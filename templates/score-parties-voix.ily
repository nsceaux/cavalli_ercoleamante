\score {
  <<
    \new Staff \with { \tinyStaff \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'basse \includeNotes "voix"
    >> \keepWithTag #'basse { \set fontSize = #-2 \includeLyrics "paroles" }
    \new StaffGroup \with { \haraKiriFirst } <<
      \new Staff <<
        $(if (*instrument-name*)
             (make-music 'ContextSpeccedMusic
               'context-type 'GrandStaff
               'element (make-music 'PropertySet
                          'value (make-large-markup (*instrument-name*))
                          'symbol 'instrumentName))
             (make-music 'Music))
        $(or (*score-extra-music*) (make-music 'Music))
        \global \includeNotes "partie1"
      >>
      \new Staff << \global \includeNotes "partie2" >>
    >>
  >>
  \layout {
    system-count = #(*system-count*)
    indent = #(if (*instrument-name*)
                  largeindent
                  (or (*score-indent*) smallindent))
  }
}
