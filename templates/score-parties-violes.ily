\score {
  \new StaffGroup <<
    \new Staff <<
      $(if (*instrument-name*)
           (make-music 'ContextSpeccedMusic
             'context-type 'GrandStaff
             'element (make-music 'PropertySet
                        'value (make-large-markup (*instrument-name*))
                        'symbol 'instrumentName))
           (make-music 'Music))
      $(or (*score-extra-music*) (make-music 'Music))
      \global \includeNotes "partie1"
    >>
    \new Staff << \global \includeNotes "partie2" >>
    \new Staff << \global \includeNotes "basse" >>
  >>
  \layout {
    system-count = #(*system-count*)
    indent = #(if (*instrument-name*)
                  largeindent
                  (or (*score-indent*) smallindent))
  }
}
