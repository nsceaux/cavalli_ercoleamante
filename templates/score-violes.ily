\score {
  \new StaffGroup <<
    \new Staff << \global \includeNotes "viole0" >>
    \new Staff <<
      $(or (*score-extra-music*) (make-music 'Music))
      \global \includeNotes "viole1"
    >>
    \new Staff << \global \includeNotes "viole2" >>
    \new Staff << \global \includeNotes "basse" >>
  >>
  \layout {
    system-count = #(*system-count*)
    indent = #(if (*instrument-name*)
                  largeindent
                  (or (*score-indent*) smallindent))
  }
}
