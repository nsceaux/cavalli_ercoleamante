\version "2.19.80"
\include "common.ily"
\paper {
  %% de la place sous la basse pour pouvoir noter les chiffrages
  score-markup-spacing.padding = #4
  system-system-spacing.padding = #4
  last-bottom-spacing.padding = #4
}

%% Title page
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \header { title = "L'Ercole" }
  \markup\null
}
%% Table of contents
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \markuplist
  \abs-fontsize-lines #8
  \override-lines #'(use-rehearsal-numbers . #t)
  \override-lines #'(column-number . 2)
  \table-of-contents
}
%% Musique
\include "pygmalion/prologo.ily"
\include "pygmalion/atto1.ily"
\include "pygmalion/atto2.ily"
\include "pygmalion/atto3.ily"
\include "pygmalion/atto4.ily"
\include "pygmalion/atto5.ily"
