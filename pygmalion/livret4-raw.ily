\livretAct ATTO QUARTO
\livretScene SCENA PRIMA
\livretDescAtt\center-column {
  \justify {
    La scena si cangia in un mare sui liti del quale sono molte torri,
    ed in una di esse Hyllo prigioniero.
  }
  Hyllo.
}
\livretPers Hyllo
\livretRef#'EAAaria
%# Ahi che pena è gelosia
%# ad un'alma innamorata
%# ch'a i sospetti abbandonata
%# teme ogn'or sorte più ria.
%# Ad Alcide allor ch'Iole
%# crudelmente in ver me pia,
%# di sperar alfin concesse;
%# io credei, che m'uccidesse,
%# solo il suon di tai parole,
%# ma il morir manco duol fia.
 
%# Ma che veggio? ecco un messo,  
%# che viene a dritta voga, è il Paggio? è desso.

\livretScene SCENA SECONDA
\livretDescAtt\center-column {
  \justify { Apparisce nel detto mare il Paggio in una barchetta. }
  Paggio, Hyllo.
}
\livretPers Paggio
\livretRef#'EBAaria
%# Zefiri che gite
%# da' vicini fiori
%# involando odori
%# e qua poi fuggite;
%# fate alla mia prora
%# ch'oggi il mar si spiani,
%# voi pur cortigiani
%# siete de l'aurora.
%# Noto è a voi Cupido
%# che d'ogn'un fa giuoco,
%# e per l'altrui fuoco
%# or me trae dal lido.
%# A voi pur convenne
%# far l'ufficio mio,
%# così avessi anch'io
%# come voi le penne.
\livretPers Hyllo
\livretRef#'EBBrecit
%# Che novella m'arrechi? è buona, o rea?
%# Ma che parlo infelice?
%# Sperar più verun bene a me non lice.
\livretPers Paggio
%# Iole alfin astretta
%# di maritarsi al furibondo Alcide
%# con questo foglio a te mi spinse in fretta.
\livretPers Hyllo
%# Porgilo dunque;
\livretDidasPPage\wordwrap { (legge il biglietto) }
%# “Alla tua fé tradita,
%# chiedo giusto perdono,
%# se per serbarti in vita
%# ad Ercole mi dono.”
%# Che per serbarmi in vita? Oh cieco errore!
%# Ah, che ciò per me fia morte peggiore.
%# Torna veloce, oh dio,
%# torna veloce, e dille,
%# ch'essendo essa fedele all'amor mio,
%# se morrò, sì contento
%# scenderà questo spirto al basso mondo,
%# ch'in alcun tempo mai
%# non ne vider gli elisei un più giocondo.
%# Ma che, s'altrui si dona, o il duol atroce
%# di sì perfida sorte,
%# o la mia destra mi darà in tal punto
%# una sì amara, e sconsolata morte,
%# ch'affannosa, e dolente
%# quest'alma in approdar le stigie arene
%# infin quivi parrà mostro di pene.
%# Saprai tu ben ridir queste querele?
\livretPers Paggio
%# Pur ch'il mar infedele
%# non mi vieti il ritorno, e di già parmi
%# che ben voglia agitarmi: o numi algosi
%# correte al mio soccorso.
\livretDidasPPage\wordwrap { Si muove la tempesta in mare. }

\livretScene SCENA TERZA
\livretDescAtt\wordwrap-center { Hyllo. }
\livretRef#'ECArecit
%# E non si trova
%# fra gl'armenti squamosi
%# un cor benché gelato,
%# che qual già d'Arione
%# di quel meschin garzone
%# senta qualche pietade, e salvi insieme
%# gl'ultimi avanzi in lui d'ogni mia speme
%# ohimè, ch'il mar con cento fauci, e cento
%# tutte rabbia spumanti
%# non par ch'ad altro furioso aneli
%# ch'a divorar quel poverello. Ah date
%# a sì mortal periglio
%# pronto soccorso o cieli;
%# ohimè, che più tardate?
\livretDidasPage\wordwrap { Il Paggio si sommerge. }
%# Ah che quella voragine l'ingoia,  
%# dunque forz'è, che disperato io moia:
%# su, su, dunque a morir, ché 'l chiaro nome
%# dell'amato mio sole
%# indorar mi potrà l'ombre più dense
%# del Tartaro profondo: Iole, Iole.
\livretDidasPage\wordwrap { Hyllo si precipita in mare. }

\livretScene SCENA QUARTA
\livretDescAtt\center-column {
  \justify {
    Apparisce nell'aria Giunone, in un gran trono e cala in soccorso
    d'Hyllo.
  }
  \wordwrap-center { Giunone, Nettuno, Hyllo. }
}
\livretPers Giunone
\livretRef#'EDArecit
%# Salva, Nettuno, ah salva
%# quel troppo ardito giovine, e sovvienti,
%# che t'acquistò non favorevol grido
%# il negato soccorso
%# all'amoroso nuotator d'Abido.
%# Salvalo, o dio triforme,
%# che d'Ercole comun nostro nemico
%# all'alma inviperita
%# far non si può da noi più grande oltraggio
%# che di salvare il di lui figlio in vita;
%# Ah tu non m'odi? o vi ripugni? adunque?
%# In quest'onde ver me già sì cortesi
%# quell'antica bontà del tutto è spenta?
\livretDidasPage\justify {
  Sorge dal mar Nettuno in una gran conchiglia tirata da cavalli marini,
  e in essa si vede Hyllo salvato.
}
\livretPers Nettuno
%# Eccoti, o dèa contenta;  
%# che nulla al tuo voler negar poss'io;
%# né fu mia negligenza
%# ma ben sua renitenza il tardar mio;
%# né credo unqua più avvenne,
%# che dall'orribil gola
%# della vorace, e non mai sazia Dite
%# fosser ritorti a forza
%# contro la lor voglia i miseri mortali
%# come or succede in questo.
\livretRef#'EDBaria
%# Amanti che tra pene
%# ogn'or gridate ohimè:
%# perché bramate di morir, perché?
%# Ah non negate mai fede alla spene.
%# Per chi vive il ciel gira,
%# e non sempre un sospira,
%# anzi lieto è tal'or chi mesto fu,
%# ma per chi more il ciel non gira più.
\livretPers Giunone
\livretRef#'EDCrecit
%# Saggiamente a te parla, Hyllo, quel nume.
\livretPers Nettuno
%# Vanne veloce, e la gran diva inchina
%# a dio forma regina.
\livretDidasPage\justify {
  Hyllo entra nella macchina di Giunone, e Nettuno s'attuffa nel mare.
}

\livretScene SCENA QUINTA
\livretDescAtt\wordwrap-center {
  Giunone, Hyllo, coro di Zefiri, che danzano, e suonano.
}
\livretPers Giunone
\livretRef#'EEArecit
%# Dunque del mio potere
%# diffiderai tu solo?
\livretPers Hyllo
%# Diva a che viver più chi vive al duolo?
%# Ma pure ossequioso
%# ti chieggio umil perdono,
%# che quantunque penoso,
%# grato il viver mi fia poich'è tuo dono.
\livretPers Giunone
%# Vanne dunque, e pur spera, e non t'annoi
%# il dar più fede a me, ch'a i sensi tuoi.
\livretRef#'EEBaria
%# Congedo a gl'orridi
%# suoi flutti altissimi
%# poi ch'il mar diè,
%# zefiri floridi
%# su festosissimi
%# volate a me,
%# e in danza lepida
%# da voi si venere
%# la mia virtù,
%# che sempre intrepida
%# contro di Venere
%# vittrice fu.
\livretPers Coro
%# Su, su lieti in nobil gara
%# ogni nume col suo lume
%# aspiri a celebrar dea si chiara.
\livretDidasP\justify {
  Scendono sul palco Hyllo e Giunone e poi questa parte e rimonta al
  cielo nella sua macchina, nella quale i Zefiri invitati da essa
  formano la 5ª danza.
}

\livretScene SCENA SESTA
\livretDescAtt\center-column {
  \justify {
    Si cangia la scena in un giardin di cipressi pieno di
    sepolcri reali.
  }
  \wordwrap-center {
    Deianira, Licco.
  }
}
\livretPers Deianira
\livretRef#'EFArecit
%# Ed a che peggio i fati ahi mi serbaro?
%# Ah che ben mi guidaro
%# gl'addolorati miei languidi passi
%# a trovare in alcun di questi sassi
%# come far sazio il mio destino avaro.
%# Ed a che peggio i fati ahi mi serbaro?
%# Alfin perduto ho il figlio
%# e già vicina è l'ora,
%# che dona ad altra sposa il mio consorte,
%# né perciò avvien ch'io mora?
%# Armi non ha da uccidermi la morte,
%# già che tanti dolor non mi sbranaro;
%# ed a che peggio i fati ahi mi serbaro?

%# Prendi Licco fedele
%# questi de' miei tesor poveri avanzi
%# per passar meno incomodi i tuoi giorni,
%# e rimira se puoi,
%# un dì questi sepolcri aprirmi in cui
%# d'ogni speranza di conforto ignuda
%# per non mirar più il sol mi colchi, e chiuda.
\livretPers Licco
%# Ah Deianira io non son tanto accorto
%# che possa in sì gran carichi servirti
%# di tesoriere insieme, e beccamorto:
%# né so s'abbi pensato,
%# ch'esser preso così quindi io potrei
%# per omicida, e ladro,
%# e con solennità condotto al posto
%# di sublime appiccato,
%# onde fora tra noi sorte ben varia,
%# tu morresti sotterra, ed io nell'aria.
%# Deh scaccia o Deianira,
%# desio sì forsennato,
%# che di quanti nell'urna abbia Pandora
%# e disastri, e ruine, e pene, e danni,
%# e dolori, ed affanni,
%# e angoscie, e crepacuori io ti so dire,
%# ch'il peggior mal di tutti è di morire.
%# Ma che pompa funebre
%# scorgo venir? tiriamoci in un lato
%# che qual lugubre aspetto a te fia grato.

\livretScene SCENA SETTIMA
\livretDescAtt\wordwrap-center {
  Iole con la pompa funebre, coro di Sacrificanti,
  ombra d’Eutyro, Deianira, Licco, coro di Damigelle d’Iole.
}
\livretPers Coro di sacrificanti
\livretRef#'EGBcoro
%# Gradisci o re,
%# il caldo pianto
%# ch'in mesto ammanto
%# afflitta gente
%# dal cor dolente
%# sparge per te!
%# Gradisci o re.
%# Tua sepoltura
%# i fior riceva
%# che selva oscura
%# germogliar fe':
%# e il sangue beva,
%# che per man monda
%# vacca infeconda
%# svenata diè,
%# gradisci o re.
\livretPers Iole
\livretRef#'EGCaria
%# E se pur negli estinti
%# di generosità pregio rimane,
%# permetti o genitore,
%# che dopo aver io tanto (ahi lassa) invano
%# per vendicarti oprato
%# ceda al voler del fato,
%# e che non già quest'alma,
%# ma sol di lei la sventurata salma
%# per l'iniquo tiranno
%# (per cui grato mi fora
%# più del talamo il rogo)
%# di sforzati imenei sottentri al giogo.
\livretPers Coro
\livretRef#'EGDcoro
%# Ah ch'il real sepolcro
%# formando entro di sé dubbi mugiti:
%# ah, ah, (ch'esser ciò puote?)
%# tutto trema, e si scuote.
\livretDidasPPage\justify {
  Rovina il sepolcro d'Eutyro, e apparisce l'ombra di lui.
}
\livretPers Eutyro
\livretRef#'EGEaria
%# Che sacrifici ingrati?
%# Che prieghi ingiuriosi?
%# Che voti obbrobriosi?
%# Porgonsi a me? così s'oltraggia Eutyro?
%# Così fia, ch'a sua voglia
%# fredda insensibil ombra ogn'un mi creda?
%# Farò ben, che s'avveda
%# l'omicida ladron, s'ancor m'adiro?
%# E se contro di lui
%# odio, rabbia, e furor più che mai spiro?
%# Dunque chi del mio sangue
%# fe' scempio ingiusto, del mio sangue ancora
%# far vorrà suo diletto? ah non fia mai:
%# e tu dar vita a i parti
%# di chi morte a me di è (figlia) potrai?
\livretPers Iole
\livretRef#'EGFaria
%# Ben resistea l'avverso mio volere
%# d'Ercole alle preghiere,
%# e alla forza di lui pur fatta avrei
%# resistenza invincibile, ma d'Hyllo,
%# d'Hyllo a te già non men, ch'a me sì caro,
%# che delle nostre offese
%# non fu complice mai:
%# anzi che ne sofferse
%# al par di noi con amorosa, e immensa
%# compassione il duolo,
%# d'Hyllo, ohimè, di lui solo
%# il periglio mortale
%# m'astrinse a consentire
%# all'aborrite nozze,
%# com'unico riparo al suo morire:
%# dunque perdona, o genitor, l'intento
%# di queste sacre pompe
%# ch'Amor, che non ha legge
%# ogni legge a sua voglia o scioglie, o rompe.
\livretPers Eutyro
\livretRef#'EGGrecit
%# Tant'ha d'Eutyro il nudo spirto ancora
%# invisibil possanza,
%# che neglette, e schernite
%# le temerarie voglie
%# del nemico fellone,
%# saprà salvare insieme
%# l'innocente garzone.
\livretPers Deianira
%# O disperata speme. Hyllo è già morto.
\livretPers Iole
%# Ohimè, che di'!
\livretPers Deianira
%# Sul più vicino scoglio
%# della di lui prigion mentre attendevo,
%# che qualche picciol legno
%# colà mi conducesse
%# a consolarlo almen col mio cordoglio,
%# lo vidi all'improvviso, ohimè, dall'alto
%# cader nel mar d'un salto.
%# E se non lo seguii,
%# fu perché dal dolore, ahi, sopra fatta
%# caddi al suol tramortita,
%# e per man degli astanti
%# con mal saggia pietà quindi fui tratta.
\livretPers Eutyro
%# Dunque a qual altro fin, che per più strano
%# mio spregio, e scorno? Or di te far vorrai
%# un esecrabil dono
%# al barbaro inumano?
%# Ch'altra moglie trafigge, altra abbandona,
%# e né meno a suoi figli empio perdona.
%# Deh con giusto coraggio
%# saggiamente pentita,
%# rinunzia a un tanto error mentr'io ritorno
%# del fumante Cocito all'aria impura
%# alle sponde infocate
%# per unire in congiura
%# l'anime ch'il crudele a morte ha date:
%# e ben vedrai ch'invano io non prefissi
%# di sollevar contro di lui gli abissi.
\livretDidasPPage\wordwrap { (l’ombra di Eutyro sparisce) }
\livretPers Iole
\livretRef#'EGHaria
%# Hyllo il mio bene è morto? altro che pianti
%# vuol da me tal dolore:
%# egli sol per mio amore
%# disperato s'uccise, ed io fra tanti
%# segni della sua fé sempre più chiari
%# fia ch'a morir dalla sua fede impari.
\livretRef#'EGIrecit
%# Attendetemi dunque, alme dilette
%# d'Hyllo, e d'Eutyro in pace,
%# ch'a raggiungervi io corro, ombra seguace.
\livretPers Licco
%# Ferma ti prego, e poiché (grazie al cielo)
%# tornò l'orribil ombra a casa sua,
%# e ch'a me così torna, e fiato, e voce;
%# vuò dar grato consiglio a tutte e dua.
%# E che miglior rimedio?
%# A' tanti vestri spasimi di quello
%# a proporvi son pronto
%# ch'è di guarire ad Ercole il cervello?
%# Dunque non ti sovviene, o Deianira,
%# che per ciò far mezzo sì raro avemo?
%# Veggio, ch'il duol estremo
%# ti rende smemorata, e quella veste,
%# che già Nesso centauro
%# in morendo a te diè, qui pur non vale?
%# Per far ch'Alcide allor che l'abbia in dosso
%# ogn'altro amor ch'il tuo ponga in non cale?
\livretPers Deianira
%#- Chi sa, che fia ben ver?
\livretPers Licco
%#= Ne farem prova.
\livretPers Iole
%# Ma ciò per ravvivare Hyllo non giova.
\livretPers Licco
%# Oh che strane domande!
%# Ma ben potrei risuscitare un morto,
%# s'a contentar due femmine mi posi,
%# ch'è d'ogni altro impossibile il più grande,
%# s'in vece, che per troppa impazienza
%# posar monte su monte
%# avesser li giganti a sasso a sasso
%# fabbricato il lor ponte;
%# al dispetto di Giove
%# sarian montati in cielo a far fracasso.
%# Si va di là dal mondo a passo a passo.
%# Né fia vano il tentare
%# di levarci un ostacolo cotanto
%# com'è d'aver con Ercole a cozzare.
%# Che poi dall'altro canto
%# chi sa? ch'Hyllo sentendosi bagnato
%# fatto più saggio non si sia pentito
%# e a nuoto salvato.
\livretPers Deianira, Iole e Licco
\livretRef#'EGJtrio
%# Una stilla di spene
%# oh che mar di dolcezza!
%# per un'anima avvezza
%# a languir sempre in pene.
%# Una stilla di spene,
%# benché tal'or mentita
%# nelle già fredde vene
%# riconduce la vita:
%# e per stupenda prova
%# fin con l'inganno giova.
\livretDidasPPage\justify {
  Le Damigelle di Iole rimaste a piangere presso le rovine del
  sepolcro d'Eutyro, alla vista di quattr'Ombre si spaventano, e
  formano così con le dett'Ombre la 6ª danza, per fine dell'atto
  quarto.
}
\sep
