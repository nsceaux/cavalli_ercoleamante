\livretAct ATTO PRIMO
\livretScene SCENA PRIMA
\livretDescAtt\justify {
  La scena si cangia ne' lati in boscareccia, e nella prospettiva in
  un gran paese contiguo alla città d’Eocalia.
}  
\livretPers Ercole
\livretRef#'BABercole
%# Come si beffa Amor del poter mio!
%# A me cui cede il mondo
%# farà contrasto una donzella? (oh dio!)
%# Come si beffa Amor del poter mio!
%# Dunque chi tanti mostri
%# vide esangui trofei di sua fortezza
%# scempio sarà di femminil fierezza,
%# e trafitto cadrà da un van desio?
%# Come si beffa Amor del poter mio!

%# O di quale empietà  
%# sacrilego tiranno ogn'or riempi
%# il credulo tuo regno?
%# Mentre ne' di lui tempi
%# l'adorate Cortine
%# di grazia, e di beltà
%# non celano altro alfine
%# ch'idoli abominevoli qua' sono
%# interesse, perfidia, orgoglio, e sdegno.
%# Così avvien per Iole
%# che l'altar del cor mio
%# sparga d'alti sospir malgrati i fumi,
%# e che vittima infausta io mi consumi.

\livretScene SCENA SECONDA
\livretDescAtt\justify {
  Cala dal cielo Venere con le Grazie in una macchina.
  Venere, Ercole, coro di Grazie.
}
\livretPers Venere
\livretRef#'BBArecit
%# Se ninfa a i pianti
%# di veri amanti
%# non mai pieghevole
%# niega mercè;
%# di ciò colpevole
%# amor non è.
\livretPers Coro
%# Se ninfa a i pianti
%# di veri amanti
%# non mai pieghevole
%# niega mercè;
%# di ciò colpevole
%# amor non è.
\livretPers Venere
%# Scoglio sì rigido
%# mostro sì frigido
%# non regge il mar
%# ch’amato al pari non deva amar.
\livretPers Coro
%# Scoglio sì rigido
%# mostro sì frigido
%# non regge il mar
%# ch’amato al pari non deva amar.
\livretPers Venere
\livretRef#'BBBrecit
%# Ogn’impero ha ribelli
%# trasgressori ogni legge
%# or come e questi, e quelli
%# giusta forza corregge,
%# sì con soave incanto
%# (ch’al dominio d’Amore
%# forza è la più conforme)
%# superare a tuo pro spero il rigore
%# che maligna fortuna,
%# sempre al mio figlio avversa
%# d’Iole in sen per tuo tormento aduna;
%# e godrai de’ miei detti
%# oggi al giardin de’ fiori i dolci effetti.
\livretPers Ercole
%# O dèa se tanto alle mie brame ottieni
%# giusto fia ch’io t’accenda
%# tutte d’Arabia l’odorate selve,
%# e che tutte a te sveni
%# dell’Erimanto le zannute belve;
\livretPers Venere
%# Vanne al loco, e m’attendi, e fa ch’Iole
%# pur vi si renda pria che manchi il sole,
%# ch’io dell’armi provvista
%# onde sua ferità vincer presumo,
%# preverrò diligente i di lei passi
%# per dispor quivi pria, ch’ella vi giunga
%# rovente acuto strale,
%# che per te l’arda, e punga.

%# Strale invisibile,  
%# ch’inevitabile
%# tal forza avrà,
%# ch’all’insensibile
%# piaga insanabile
%# imprimerà.
\livretPers Venere
%# Su dunque ogni tristezza
%# sia dal tuo cor sbandita,
%# ch’in amor l’allegrezza
%# come al ciel più gradita
%# con più felicità le gioie invita.
\livretPers Venere e Ercole
\livretRef#'BBCduoCoro
%# Fuggano a vol
%# dal bell’impero
%# del nume arciero
%# le pene, e ’l duol.
\livretPers Coro
%# E in lui così
%# gioie sol piovino,
%# e si rinnovino
%# quegli aurei dì.
\livretPers Venere e Ercole
%# Struggasi il gel
%# d’ogni fierezza
%# ogni amarezza
%# il cangi in miel.
\livretPers Coro
%# E in lui così
%# gioie sol piovino,
%# e si rinnovino
%# quegli aurei dì.
\livretDidasP\justify {
  La macchina di Venere rimonta al cielo.
}
\livretPers Ercole
\livretRef#'BBDrecit
%# Infelice, e disperato
%# mentre mestissimo
%# vo notte, e dì,
%# qual di bene inaspettato
%# raggio purissimo
%# m’apparì?

\livretScene SCENA TERZA
\livretDescAtt\justify {
  Nel resto de’ nuvoli di detta macchina essendo ascosa Giunone, questa
  si discovre assisa in un gran pavone.
}
\livretPers Giunone
\livretRef#'BCArecit
%# E vuol dunque ciprigna,
%# per far contro di me gl’ultimi sforzi
%# de’ più pungenti oltraggi,
%# favorir chi le voglie ebbe sì intese
%# ad offendermi ogn’ora,
%# che ne gli impuri suoi principi ancora
%# prima d’esser m’offese?
%# Chi pria di spirar l’aure
%# spirò desio di danneggiarmi, e dopo
%# aver dal petto mio
%# tratti i primi alimenti al viver suo,
%# con ingrata insolenza
%# d’uccidermi tentando osò ferirmi?
%# Ah ch’intesi i disegni
%# ma non sia ch’a disfarli altri m’insegni.
%# Di reciproco affetto
%# ardon Hyllo, e Iole,
%# e sol per mio dispetto
%# l’iniqua dèa non vuole,
%# ch’Imeneo li congiunga? anzi procura
%# per il mio scorno maggiore,
%# ch’il nodo maritale ond’è ristretto
%# Ercole a Deianira alfin si rompa;
%# a ciò ch’Iole a questi
%# del di lei genitore empio omicida
%# con mostruosi amplessi oggi s’innesti.
%# E con qual arte oh dio? con arti indegne
%# d’ogni anima più vil non che divina.
\livretRef#'BCBAaria
%# Ma in amor ciò ch’altri fura
%# più d’amor gioia non è
%# e un’insipida ventura
%# ciò ch’egli in dono, o ver pietà non diè.
%# In amor ciò ch’altri fura
%# più d’amor gioia non è.
%# Se non vien da grata arsura
%# volontaria all’altrui fé
%# cangia affatto di natura
%# come d’odio condita ogni mercè.

%# Ma che più con inutili lamenti  
%# il tempo scarso alla difesa io perdo?
%# Su portatemi o venti
%# alla grotta del Sonno, e d’aure infeste
%# corteggiato il mio tron versi per tutto
%# pompe del mio furor fiamme, e tempeste.
\livretDidasP\justify {
  Giunone parte e fa cader dalle nuvole della sua macchina, Tempeste e
  Fulmini che formano una danza per fine del primo atto.
}
\sep