\livretAct ATTO QUINTO
\livretScene SCENA PRIMA
\livretDescAtt\center-column {
  \justify { La scena si cangia in inferno. }
  \wordwrap-center {
    Ombra d’Eutyro, coro di Anime infernali,
    Clerica, Laomedonte, Bussiride.
  }
}
\livretPers Eutyro
\livretRef#'FABaria
%# Come solo ad un grido,
%# che giunto a pena d'Acheronte al lido
%# formai, vi radunate anime ardite?
%# Su, così pur contro il comun nemico
%# vostro furore alla mia rabbia unite,
%# che più dunque s'aspetta?
%# Pera mora il crudel, su su vendetta.
\livretPers Coro
%# Pera mora il crudel, su su vendetta.
\livretPers Clerica regina di Cos
\livretRef#'FACrecit
%# Pera mora l'indegno
%# di cui più scellerato unqua non visse,
%# che del troiano eccidio ancor fumante
%# non mai sazio di sangue
%# i miei poveri figli, e me trafisse.
%# Ah ver'un chiostro
%# più fiero mostro
%# di lui non ha.
%# E se il crudel
%# per nostro ufficio
%# oggi cadrà
%# mai sacrifizio
%# più grato al ciel
%# altri fe', né mai farà.
%# Che più dunque si aspetta?
%# Pera mora il crudel, su su vendetta.
\livretPers Coro
%# Pera mora il crudel, su su vendetta.
\livretPers Bussiride re d'Egitto
\livretRef#'FAErecit
%# Pera mora l'iniquo,
%# che dell'etereo Giove,
%# ingratissimo al pari,
%# ch'in legittimo figlio,
%# di sacerdoti, e vittime più degne,
%# con sacrilega man spogliò l'altari.
%# Che più dunque s'aspetta?
%# Pera mora il crudel, su su vendetta.
\livretPers Coro
%# Pera mora il crudel, su su vendetta.
\livretPers Eutyro
\livretRef#'FAFrecit
%# Su, su dunque ombre terribili
%# su voliam tutte in Eocalia,
%# nuova in ciel schiera stimfalia
%# contra il reo furie invisibili,
%# e con le vipere
%# onde Tesifone
%# tormenta l'anime
%# flagellamogli il cor;
%# fin ch'immenso dolor
%# con angoscie rabbiose il renda esanime.
\livretPers Coro
\livretRef#'FAGcoro
%# Su, su dunque all'armi, su, su,
%# su corriamo a vendicarci,
%# ch'altro ben non può mai darci
%# il destino di quaggiù.
%# E che giova assordar quest'antri più
%# con il vano rumor de' nostri carmi?
%# Su, su dunque all'armi, all'armi.
\livretPers Eutyro
%# Ah più val più diletta,
%# che quante gioie ha il ciel una vendetta.
\livretPers Coro
%# Ah più val più diletta,
%# che quante gioie ha il ciel una vendetta.

\livretScene SCENA SECONDA
\livretDescAtt\center-column {
  \justify {
    La scena si cangia in un portico del tempio di Giunone Pronuba.
  }
  \wordwrap-center {
    Ercole, Iole, Licco, Deianira, coro di Sacerdoti di Giunone Pronuba.
  }
}
\livretPers Ercole
\livretRef#'FBArecit
%# Alfine il ciel d'Amor
%# per me si serenò,
%# e i nembi di rigor,
%# in gioie distemprò,
%# sol nel mio cor pur sento
%# un soave martir,
%# ch'abbia per gir più lento
%# dati il tempo i suoi vanni al mio desir.
%# Ma pur l'amata Iole
%# l'adorato mio sole ecco a me viene,
%# dunque affatto il mio sen sgombrate o pene,
%# che di sì rigid'alma
%# qual si sia la vittoria io n'ho la palma,
%# e l'ardente mio spirto
%# pospon tutti i suoi lauri a un sì bel mirto.
\livretPers Licco
%# Quando com'è tuo uffizio,
%# dar quella veste ad Ercole dovrai
%# per far di nozze tali il sacrifizio,
%# quest'altra in vece, il cui valor ben sai,
%# destramente da me prender potrai.
\livretPers Iole
%# Così farò: ma che? per diffidenza
%# di rimedio sì incerto, ho il sen ripieno
%# di gelosa temenza,
%# pur quando mi tradisca ogn'altro scampo,
%# soccorso mi darà pronto veleno.
\livretPers Ercole
%# Deh non muovere Iole il piè restio,
%# ver chi dominator del mondo intero
%# solo in goder dell'alma tua l'impero
%# pon la felicità del suo desio.
%# E il sacro concento
%# sciolgasi omai, ch'a me di tali indugi
%# grado è d'immensa pena ogni momento.
\livretPers Coro
\livretRef#'FBBcoro
%# Pronuba, e casta dèa  
%# l'alme de' nuovi sposi
%# con lacci avventurosi
%# annoda, e bea.
%# E quieta, e gioconda
%# da' lor nestorea vita,
%# e gl'amplessi feconda
%# con progenie infinita.
\livretPers Ercole
\livretRef#'FBCrecit
%# E di che temi, Iole, e di che temi?
\livretPers Iole
%# Ecco il mio viver giunto
%# a un formidabil punto.
\livretPers Ercole
%# Deh su porgimi ardita
%# la veste, ond'io ben tosto
%# per i nostri imenei
%# renda olocausto a i dèi.
\livretPers Coro
\livretRef#'FBDcoro
%# Pronuba, e casta dèa
%# l'alme de' nuovi sposi
%# con lacci avventurosi
%# annoda, e bea.
\livretPers Ercole
\livretRef#'FBErecit
%# Ma qual pungente arsura
%# la mia ruvida scorza intorno assale?
%# Qual incognito male
%# d'offendermi temendo
%# serpe nascoso per le vene al core?
%# Qual immenso dolore, ahi, mi conquide?
%# E per dar morte a me tanto più dura
%# in vista de' contenti, oh dio, m'uccide?
%# E tu lo soffri, o genitore? E lasci,
%# ch'io, che con piè temuto
%# passeggiai della morte i regni illeso,
%# e che fin dalla cuna
%# di belle glorie adorni
%# tutti contai della mia vita i giorni,
%# or senz'avere a fronte
%# di me degno nemico (ah rio martire,
%# che della morte ancor vie più m'accora)
%# in ozio vil qui mora?
%# Senza che gloria alcuna
%# renda almen di me degno il mio morire.
%# Almen di nubi oscure
%# vela quest'aria in torno
%# sì che sorte maligna
%# di me grato spettacolo non faccia
%# all'implacabil mia cruda matrigna;
%# e per quando la tua
%# insensata pigrizia, (oh gran tonante)
%# il conquasso destina
%# dell'universo, ohimè, s'ora no 'l fai?
%# E a che riserbi il cielo?
%# Che nel perder Alcide a perder vai?
\livretRef#'FBFaria
%# Ma l'atroce mia doglia
%# imperversando ogn'or pochi respiri
%# mi lascia più, deh s'il morire è forza,
%# ardasi la mia spoglia
%# né della terra, i di cui figli uccisi
%# s'esponga ad un rifiuto:
%# a dio, cielo, a dio Iole, eccomi Pluto.
\livretPers Licco
\livretRef#'FBGrecit
%# Che dite? Il mio non fu rimedio tardo,
%# ma un poco più (ch'io non credea) gagliardo.
%# Pur ciascuna di voi di già rimira
%# il penoso destin per sé finito
%# d'un amante importun, d'un reo marito.
%# E non piangete già,
%# che comunque ch'avvenga a un saggio core
%# dar non si può qui giù sorte migliore,
%# che di vivere in pace, e libertà.
\livretPers Deianira
%# Ah Nesso mi tradì, deh ti perdoni
%# o Licco il ciel l'involontario errore;
%# a dolor su dolore
%# egualmente infinito
%# più resister non so, mostrami o morte
%# e del figlio la traccia, e del consorte.
%# Ma che? l'ombra del figlio
%# ecco ch'ad incontrarmi
%# ver me riede pietosa.

\livretScene SCENA TERZA
\livretDescAtt\wordwrap-center {
  Iole, Deianira, Licco, Hyllo.
}
\livretPers Iole
\livretRef#'FCArecit
%# Oh che opportun ristoro!
\livretPers Licco
%#- Oh che spavento!
\livretPers Iole
%#- Hyllo?
\livretPers Deianira
%#= Figlio?
\livretPers Deianira e Iole
%#- Sei tu?
\livretPers Hyllo
%#= Mercé di Giuno
%# son io dal mar salvato
%# acciò per gl'occhi miei
%# versi in un mar di pianto il cor stemprato.
%# Se qual ridirlo intendo
%# vero è del caro padre il fato orrendo.
\livretPers Iole
%# Pur mio ben ti consola,
%# che se perdesti il genitor crudele
%# me qui ritrovi, e l'amor mio fedele.
\livretPers Deianira
%# Saranno almen le ceneri d'Alcide
%# le più pompose de' funebri onori
%# e più sparse di lagrime, e di fiori.
\livretPers Licco
%# Or che sorte è la mia?  
%# Che senza averne voglia,
%# anch'io per compagnia
%# converrà che mi doglia.
\livretPers Deianira, Iole, Hyllo e Licco
\livretRef#'FCBquatro
%# Dall'occaso a gl'Eoi
%# ah non sia chi non pianga,
%# ch'oggi il sol de gl'eroi
%# estinto, ohimè, rimanga.
%# Dall'occaso a gl'Eoi
%# ah non sia chi non pianga.

\livretScene SCENA QUARTA
\livretDescAtt\center-column {
  \justify {
    Cala Giunone nell’ultima macchina corteggiata dall’armonia de’
    cieli, ed apparisce nella più alta parte di questi Ercole sposato
    alla Bellezza.
  }
  \wordwrap-center { Giunone, Deianira, Iole, Hyllo, Licco. }
}
\livretPers Giunone
\livretRef#'FDAaria
%# Su, su allegrezza
%# non più lamenti
%# deh non più no,
%# ch'ogni amarezza
%# il ciel cangiò
%# tutt'in contenti
%# tutt'in dolcezza
%# non più lamenti
%# su, su, allegrezza.
%# Non morì Alcide
%# tergete i lumi
%# non morì no,
%# su nel ciel ride,
%# che lo sposò
%# il re de' numi
%# alla Bellezza
%# tergete i lumi
%# su, su, allegrezza.
\livretPers Giunone
\livretRef#'FDBrecit
%# Così deposti alfin gl'umani affetti
%# così l'alma purgata
%# d'ogni rea gelosia
%# ciò che qui giù sdegnò, lassù desia.
%# Quindi ammorzati anch'io gl'antichi sdegni
%# per il vostro godere:
%# a me sì glorioso
%# consentii, ch'egli goda in su le sfere
%# un beato riposo,
%# onde a compire ogni desio celeste
%# sol de' vostri imenei mancan le feste.
%# Su dunque a i giubili
%# anime nubili
%# e felicissimi
%# i miei dolcissimi
%# nodi insolubili
%# al par d'amor v'allaccino,
%# e nelle vostre destre i cor allaccino.
\livretPers Iole e Hyllo
\livretRef#'FDCtre
%# Che dolci gioie oh dèa
%# versi nel nostro seno,
%# il ciel benigno a pieno
%# che più dar ne potea?
%# Che dolci gioie oh dèa.
\livretPers Giunone, Deianira, Iole, Hyllo e Licco
\livretRef#'FDFtutti
%# Contro due cor ch'avvampano
%# tra loro innamorati
%# in van nel ciel s'accampano
%# per guerreggiar i fati.
%# Da lega d'amore
%# fia vinto il furore
%# d'ogni contraria sorte:
%# d'un reciproco amor nulla è più forte.

\livretScene SCENA QUINTA
\livretDescAtt\wordwrap-center { Ercole, la Bellezza, coro di Pianeti. }
\livretPers Coro
\livretRef#'FEAcoro
%# Quel grand'eroe, che già
%# laggiù tanto penò
%# sposo della beltà
%# per goder nozze eterne al ciel volò!
%# Virtù, che soffre alfin mercede impetra
%# e degno campo a' suoi trionfi è l'etra.
\livretPers Bellezza e Ercole
\livretRef#'FEBduo
%# Così un giorno avverrà con più diletto,
%# che della Senna in su la riva altera
%# altro gallico Alcide arso d'affetto
%# giunga in pace a goder bellezza ibera;
%# ma noi dal ciel traem viver giocondo
%# e per tal coppia sia beato il mondo.
\livretPers Tutti
\livretRef#'FECcoro
%# Virtù che soffre alfin mercede impetra
%# e degno campo a' suoi trionfi è l'etra.	
\livretDidasP\justify {
  Le varie influenze di sette Pianeti scendono sul palco
  successivamente a danzare, e in fine anche un coro di Stelle.
}
