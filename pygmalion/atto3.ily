\newBookPart#'()
\act "Atto Terzo"
\scene "Scena Prima" "Scena I"
\sceneDescription\markup\center-column {
  \wordwrap-center {
    [Si cangia la scena in un giardino d’Eocalia, e Venere cala dal
    cielo a terra, in una nuvola, che sparisce.]
  }
  \wordwrap-center { Venere, Ercole. }
}
%% 3-1
\pieceToc "Sinf[onia]"
\includeScore "DAAsinfonia"
\newBookPart#'(full-rehearsal)
%% 3-2
\pieceToc\markup\wordwrap {
  Ercole, Venere: \italic { E per me cangi o dèa }
}
\includeScore "DABritornello"
\includeScore "DACrecit"
%% 3-3
\pieceToc "Sinf[onia]"
\includeScore "DAEsinfonia"
\newBookPart#'(full-rehearsal)

\scene "Scena Seconda" "Scena II"
\sceneDescription\markup\wordwrap-center {
  Ercole, Paggio.
}
%% 3-4
\pieceToc\markup\wordwrap {
  Ercole: \italic { Amor contar ben puoi }
}
\includeScore "DBAercole"
\newBookPart#'(full-rehearsal)
%% 3-5
\pieceToc \markup\wordwrap {
  Paggio, Ercole: \italic { Sarà com’hai disposto }
}
\includeScore "DBBrecit"
\newBookPart#'(full-rehearsal)

\scene "Scena Terza" "Scena III"
\sceneDescription\markup\wordwrap-center {
  Ercole, Iole, Paggio, Hyllo.
}
%% 3-6
\pieceToc \markup\wordwrap {
  Ercole, Iole: \italic { Bella Iole, e quando mai }
}
\includeScore "DCArecit"
%% 3-7
\pieceToc  "Sinf[onia]"
\includeScore "DCBsinfonia"
\newBookPart#'(full-rehearsal)
%% 3-8
\pieceToc \markup\wordwrap {
  Iole, Ercole, Hyllo: \italic { Ma qual? ma come io sento }
}
\includeScore "DCCrecit"
\newBookPart#'(full-rehearsal)

\scene "Scena Quarta" "Scena IV"
\sceneDescription\markup\wordwrap-center {
  Ercole, Iole, Paggio.
}
%% 3-9
\pieceToc \markup\wordwrap {
  Ercole, Iole: \italic { E tu a che pensi Iole? }
}
\includeScore "DDArecit"

\scene "[Scena Quinta]" "Scena V"
\sceneDescription\markup\center-column {
  \wordwrap-center {
    [Torna ad apparir in aria Giunone nel suo carro col Sonno.]
  }
  \wordwrap-center {
    [Giunone col Sonno, Ercole, Iole, Paggio.]
  }
}
%% 3-10
\pieceToc \markup\wordwrap {
  Giunone: \italic { Sonno potente nume }
}
\includeScore "DEArecit"
\newBookPart#'(full-rehearsal)
%% 3-11
\pieceToc  "Sinf[onia]"
\includeScore "DEBsinfonia"
%% 3-12
\pieceToc \markup\wordwrap {
  Iole, Giunone: \italic { E quale inaspettato }
}
\includeScore "DECArecit"
\newBookPart#'(full-rehearsal)
%% 3-13
\pieceToc \markup\wordwrap {
  Iole: \italic { Luci mie, voi che miraste }
  [Cavalli: \concat { \italic Xerse ] }
}
\includeScore "xerces"
\newBookPart#'(full-rehearsal)
%% 3-14
\pieceToc \markup\wordwrap {
  Giunone: \italic { Ah perché perdi Iole }
}
\includeScore "DECBrecit"
\partNoPageTurn#'(basse)
\newBookPart#'(full-rehearsal)
%% 3-15
\pieceToc  "Sinf[onia]"
\includeScore "DEDsinfonia"
\newBookPart#'(full-rehearsal)

\scene "Scena [Sesta]" "Scena VI"
\sceneDescription\markup\wordwrap-center {
  Iole, Hyllo, Ercole che dorme, [Paggio.]
}
%% 3-16
\pieceToc \markup\wordwrap {
  Iole, Hyllo: \italic { D'Eutyro anima grande }
}
\includeScore "DFArecit"
\newBookPart#'(full-rehearsal)

\scene "Scena [Settima]" "Scena VII"
\sceneDescription\markup\center-column {
  \wordwrap-center {
    [Mercurio d’un volo risveglia Ercole e parte.]
  }
  \wordwrap-center {
    Mercurio, Hyllo, Iole, Ercole, [Paggio.]
  }
}
%% 3-17
\pieceToc \markup\wordwrap {
  Mercurio, Ercole, Iole, Hyllo: \italic { Svegliati Alcide, e mira }
}
\includeScore "DGArecit"
\newBookPart#'(full-rehearsal)

\scene "Scena [Ottava]" "Scena VIII"
\sceneDescription\markup\wordwrap-center {
  Deianira, Licco, Ercole, Iole, Hyllo, Paggio.
}
%% 3-18
\pieceToc \markup\wordwrap {
  Ercole, Deianira, Iole: \italic { Più di salvarlo tenti }
}
\includeScore "DHArecit"
\newBookPart#'(full-rehearsal)

\scene "[Scena Nona]" "Scena IX"
\sceneDescription\markup\wordwrap-center {
  [Deianira, Hyllo.]
}
%% 3-19
\pieceToc \markup\wordwrap {
  Deianira, Hyllo: \italic { Figlio tu prigioniero? }
}
\includeScore "DIAduo"
\newBookPart#'(full-rehearsal)

\scene "[Scena Decima]" "Scena X"
\sceneDescription\markup\wordwrap-center {
  [Licco, Paggio.]
}
%% 3-20
\pieceToc \markup\wordwrap {
  Licco, Paggio: \italic { A dio, Paggio. }
}
\includeScore "DJArecit"
%% 3-21
\pieceToc \markup\wordwrap {
  Paggio, Licco: \italic { Amor, chi ha senno in sé }
}
\includeScore "DJBduo"
\actEnd\markup\wordwrap-center {
  Qui va il 4 Balletto
  e Finisce l’Atto \concat { 3 \super o }
}
