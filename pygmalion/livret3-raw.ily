\livretAct ATTO TERZO
\livretScene SCENA PRIMA
\livretDescAtt\center-column {
  \justify {
    Si cangia la scena in un giardino d’Eocalia, e Venere cala dal
    cielo a terra, in una nuvola, che sparisce.
  }
  Venere, Ercole.
}
\livretPers Ercole
\livretRef#'DACrecit
%# E per me cangi o dèa
%# le delizie del ciel con questo suolo
%# ed or perché non manda
%# la palude Lerneà
%# e la selva Nemeà
%# nov'idre, altri leoni a far qui meco
%# gloriosi contrasti,
%# onde a te formi o dèa grati olocausti?
\livretPers Venere
%# Pur ch'io giunga a cangiar nel crudo seno
%# d'Iole il core, e te lo renda amante
%# ne trarrò tal piacere,
%# che fia d'ogni opra mia premio bastante,
%# mira quest'è la verga onde fa Circe
%# magiche meraviglie;
%# al di cui moto ubbidienti ancelle
%# per patto inalterabile son tutte
%# de' lidi Acherontei l'anime felle.
%# Or in virtù di sì potente stelo
%# dove tocco la terra
%# nascerà seggio erboso in cui riposte,
%# da spiriti lascivi a ciò costretti
%# le mandragore oscene
%# di pallido color la Lidia pietra
%# e d'amorose rondinelle i cori
%# faran ch'Iole allor, ch'in lui s'affida
%# cangi per te il suo sdegno in dolci amori.
\livretDidasPPage\wordwrap {
  (nasce di sotto terra la sedia incantata fatta di erbe e di fiori)	
}
\livretPers Ercole
%# Diva ad opre sì rare
%# insolito tremor tutto mi scuote,
%# e poi ch'esser non puote
%# timor (da me non conosciuto ancora)
%# forz'è che sia per inspirar superno
%# di futuro gioir presagio interno.
%# Ma pur nel pensier mio sceman di pregio
%# quelli, ch'a me prometti
%# sospirati diletti,
%# qual or lasso m'avveggio
%# ch'a far miei dì giocondi
%# tratte non fian tai gioie
%# dal mar d'amor, ma da gli stigi fondi.
\livretPers Venere
%# O di questa canzon  
%# pur che tu goda
%# ch'importa a te?
%# Che sia per froda
%# o per mercé?
%# Pur che tu goda
%# ch'importa a te?
 
%# Ch'altro è l'amare?  
%# Ch'un guerreggiare,
%# ove in trionfo egual lieti se n' vanno
%# il valor, e l'inganno;
%# infelice non sai?
%# Che nel gran regno del mio figlio arciero
%# non v'è (tolto il penar) nulla di vero.
%# Prendi il crin, che fortuna
%# per mia man t'offre in dono.
%# Ma mentre a te giusta ragion m'invola
%# se d'altro uopo ti sia
%# Mercurio invierò, che ratto vola.
\livretPers Venere e Ercole
\livretRef#'DADduo
%# E perché Amor non fa,
%# ch'all'amorosa schiera
%# sol delle gioie sue sia dispensiera
%# o ragione, o pietà?
%# E perché crudeltà
%# perché il rigor,
%# in guardia ogn'or le avrà?
%# Dunque per involarle ogn'arte ancor
%# lecita altrui sarà:
%# d'un ardente desio giungerà 'l segno
%# sì, sì, gioco è d'ingegno.

\livretScene SCENA SECONDA
\livretDescAtt\wordwrap-center { Ercole, Paggio. }
\livretPers Ercole
\livretRef#'DBAercole
%# Amor contar ben puoi
%# fra tuoi non minor vanti
%# che dell'ardir, che torre a me non seppe
%# co' latrati di Cerbero, e orrendi
%# strepiti suoi lo spaventoso abisso;
%# tu disarmato m'hai, sì ch'io, che colsi
%# ad onta del terribile custode,
%# con intrepida man l'Esperie frutta,
%# quasi di sostenere or non ardisco
%# l'avvicinar del bel per cui languisco.
 	
%# O quale instillano  
%# in arso petto
%# rai, che sfavillano
%# di gran beltà,
%# umil rispetto,
%# bassa umiltà:
%# il ciel ben sa
%# a sì suprema
%# adorabil maestà,
%# s'ei pur non trema?

\livretPers Paggio
\livretRef#'DBBrecit
%# Sarà com'hai disposto
%# Iole qui ben tosto.
\livretPers Ercole
%# E dove la trovasti?
\livretPers Paggio
%# Nel cortil regio a favellar d'amore.
\livretPers Ercole
%# A favellar d'amor? con chi? deh dillo,
%# dell'amor mio?
\livretPers Paggio
%# Dell'amor suo con Hyllo.
\livretPers Ercole
%# Come? Dunque il mio figlio
%# mio rivale divenne?
%# A tal temerità sarebbe ei giunto?
%# Tu non hai ben compreso
%# semplicetto garzone.
\livretPers Paggio
%# Eccoli appunto.

\livretScene SCENA TERZA
\livretDescAtt\wordwrap-center {
  Ercole, Iole, Hyllo, coro di Damigelle, e Paggio.
}
\livretPers Ercole
\livretRef#'DCArecit
%# Bella Iole, e quando mai
%# sentirai
%# di me pietà?
%# Chi la chiede al tuo rigore
%# ha valore
%# per domare ogn'impietà
%# ma non sia, che teco impieghi
%# se non prieghi
%# e mesti lai;
%# bell'Iole, e quando mai?
\livretPers Iole
%# Quando il mio cor capace
%# fosse d'un lieve amor per chi m'uccise
%# il genitor diletto
%# aver per me dovresti
%# orrore, e non affetto.
\livretPers Ercole
%# Ah bella Iole
%# a sì gran crime, e di sì gran castigo
%# degno, qual per me fora
%# l'impossibilità dell'amor tuo:
%# imputar mi vorrai
%# una prova fatale,
%# ed un impulso senza freno, oh dio,
%# dell'infinito ardor, dell'amor mio?
%# Quand'il tonante istesso
%# negarmi com'Eutyro, avesse ardito
%# un ben sì desiato, e a me promesso,
%# come già contro il sole, e 'l dio triforme
%# stato non fora contra lui men parco
%# di strali avvelenati il mio grand'arco.
\livretPers Iole
%# Io sola fui cagion, che il re mio padre
%# rompesse a te la data fede.
\livretPers Ercole
%# Ah come
%# a ciò tu l'inducesti?
%# Dunque tu l'uccidesti.
%# Che d'un mal, che si feo,
%# chi la causa ne diè, quegli n'è reo.
%# Ma pon bella in oblio
%# sì funeste memorie, e sì noiose,
%# e qui meco t'assidi,
%# poiché depost'anch'io
%# l'innata mia ferocia, anzi cangiata
%# in conocchia la clava
%# ravisar ti farò, che quale ogn'altra
%# tua più devota ancella
%# non mai prenderò a vile
%# di renderti ogni ossequio il più servile;
%# qua gira gli occhi Atlante
%# e per somma beltà
%# mira quel, ch'oggi fa
%# Ercole amante:
%# ma non ne rider già
%# che se tale è il voler
%# del pargoletto arcier.

%# Tutte son opre gloriose, e belle  
%# tanto il filar, che sostener le stelle.
%# Sol per voler d'Amore,
%# chi in ciel Etho frenò
%# armenti ancor guidò
%# nume, e pastore:
%# e non ne riser no
%# gl'altri dèi, ch'il mirar,
%# che fan ben ch'in amar:
%# tutte son opre gloriose, e belle
%# tanto il filar, che sostener le stelle.
 	
\livretPers Iole
\livretRef#'DCCrecit
%# Ma qual? ma come io sento
%# spuntare entro il mio petto
%# per te improvviso, e involontario affetto
%# onde forz'è ch'io t'ami
%# e ch'amor mio ti chiami.
\livretPers Hyllo
%# Ohimè, ch'ascolto!
%# E non sogno? e son desto? e non già stolto?
%# Così cangiasi Iole?
%# Fragil femminea fede;
%# ben merta i tradimenti un, che ti crede.
\livretPers Ercole
%# Hyllo, di che ti offendi?
%# Che senso ha tal linguaggio?
%# (Non mal l'intese il Paggio)
%# ami tu dunque Iole?
\livretPers Hyllo
%# Io per un'empia
%# ingrata al padre, al mondo, al ciel spergiura,
%# che soffrissi nel cuor d'amor l'arsura?
%# Per una sì mutabile, ch'a un tratto
%# con subito contento
%# alla mia genitrice, a Deianira
%# tecò a far sì gran torto (ohimè) cospira?
%# Versi pria sul mio capo irato Giove
%# tutti i fulmini suoi,
%# e il più nero baratro m'ingoi.
\livretPers Iole
%# O me infelice, o misera, che fei?
%# Uccidetemi, oh dèi.
\livretPers Ercole
%# Finora a te d'Eutyro
%# ne men di Deianira unqua non calse.
%# Parti, e ringrazia il ciel; che ben ti valse,
%#- che d'esser mite oggi disposi.
\livretPers Hyllo
%#= A dio:
%# andrò morte a cercar per quelle balze.

\livretScene SCENA QUARTA
\livretDescAtt\wordwrap-center { Ercole, Iole, Paggio. }
\livretPers Ercole
\livretRef#'DDArecit
%# E tu a che pensi Iole?
\livretPers Iole
%# All'error mio,
%# se ben ciò che mia lingua
%# disse pur dianzi ah no, non lo diss'io.
%# E l'alma forsennata,
%# nel frenetico errore
%# altra parte non ebbe
%# che di gran pentimento alto dolore.
\livretPers Ercole
%# Dunque su di tua mano
%# per fermezza amorosa
%# pegno porgimi sol d'esser mia sposa.
\livretPers Iole
%# No'l rifiuto, ma lascia,
%# ch'in segrete preghiere
%# del genitore all'oltraggiato spirto
%# per addolcirlo in qualche guisa almeno
%# prima, ch'affatto a te mi doni in preda,
%# io licenza ne chieda.

\livretScene SCENA QUINTA
\livretDescAtt\center-column {
  \justify {
    Torna ad apparir in aria Giunone nel suo carro col Sonno.
  }
  \wordwrap-center {
    Giunone col Sonno, Ercole, Iole, Paggio.
  }
}
\livretPers Giunone
\livretRef#'DEArecit
%# Sonno potente nume
%# fu qui pur opportuno il nostro arrivo;
%# dunque poiché tu sei
%# dell'innocenza amico,
%# impedisci pietoso al par, che giusto
%# oggi un crime il più nero,
%# che contro amor la frode unqua tentasse,
%# e con la verga a cui fu facil prova
%# le sempre deste luci
%# tutte velare ad Argo
%# vanne veloce, e in Ercole produci
%# un più cieco letargo.
\livretPers Iole
\livretRef#'DECArecit
%# E quale inaspettato
%# sonno prodigioso
%# prevenendo Imeneo lega il mio sposo?
\livretPers Giunone
%# Iole, Iole, ah sorgi
%# sorgi rapida, e fuggi, e t'allontana
%# dall'incantato seggio, e a me t'appressa
%# che di ben tosto risanarti è d'uopo
%# dal magico veleno,
%# ond'hai l'anima oppressa.
\livretPers Iole
%# O diva, o dèa, da quali
%# orridi precipizi
%# d'infedeltà, d'iniquità risorgo?
%# Ohimè! di quali errori
%# rea, quantunque innocente ora mi scorgo!
%# Pure il mio primo, e sol gradito fuoco,
%# ch'in me pareva estinto
%# mentre il cor mi ralluma,
%# con usura di fiamme
%# più che mai mi consuma.
%# Ma che pro? s'Hyllo intanto
%# l'unico mio tesoro
%# senza mia colpa a ragion meco irato,
%# a ragion da me fugge, e a torto io moro.
\livretRef#'xerces
%# Luci mie, voi che miraste
%# quel bel sol che m’abbagliò,
%# voi che semplici cercaste
%# il crin d’or che mi legò,
%# voi che del mio penar la colpa avette
%# di dover lagrimar non vi dolete.
\livretPers Giunone
\livretRef#'DECBrecit
%# Ah perché perdi Iole
%# in superflue querele
%# tempo sì prezioso, Hyllo non lunge
%# per mio consiglio in un cespuglio ascoso
%# tutto guata, e ascolta. Arma più tosto
%# arma figlia la mano
%# di questo acuto acciaro,
%# (ch'abile a penetrare ogni riparo
%# per me temprò Vulcano)
%# e mentre imprigionato
%# da i legami del Sonno i più tenaci
%# sta quel mostro sì crudo
%# d'ogni difesa ignudo,
%# vanne, e vendica ardita
%# con la morte di lui
%# le mie offese, e i tuoi danni,
%# ch'altro scampo non ha d'Hyllo la vita.
%# Vanne, e poiché spedita al ciel'io torno
%# ad ovviare in ciò l'ire di Giove
%# fa' ch'io vi giunga il crin di lauri adorno.

\livretScene SCENA SESTA
\livretDescAtt\wordwrap-center { Iole, Hyllo, Ercole che dorme, Paggio. }
\livretPers Iole
\livretRef#'DFArecit
%# D'Eutyro anima grande
%# a questo core, a questo braccio imbelle
%# tanto furor, tanto vigor comparti
%# che possa or qui sacrarti,
%# con insigne vendetta
%# (universal di cui desio rimbomba)
%# vittima sì dovuta alla tua tomba.
%# Prendi o mio genitor dall'arso lido
%# di Flegetonte, il sangue
%# di quest'empio tiranno,
%# che nel tuo nome uccido.
\livretPers Hyllo
%# Ohimè, che fai?
%#- Cessa.
\livretPers Iole
%#- Deh lascia.
\livretPers Hyllo
%#= Ah cessa.
\livretPers Iole
%# Lascia se m'ami.
\livretPers Hyllo
%# Ah che del pari io sono
%# tuo vero amante, e di lui figlio.
%# Lo placherò, quando non basti il pianto,
%# con la mia morte.
\livretPers Iole
%# E sì poco è gradita
%# la speme a te d'esser mio sposo (oh dio)
%# che per essa non pregi
%# punto di più la vita?

\livretScene SCENA SETTIMA
\livretScene SCENA PRIMA
\livretDescAtt\center-column {
  \justify {
    Mercurio d’un volo risveglia Ercole e parte.
  }
  \wordwrap-center {
    Mercurio, Hyllo, Iole, Ercole, Paggio.
  }
}
\livretPers Mercurio
\livretRef#'DGArecit
%# Svegliati Alcide, e mira.
\livretPers Ercole
%# E dove, o bella?
%# Dove? ah qui pur di nuovo
%# temerario importuno io ti ritrovo?
%# Ed a qual fine impugni
%# ferro micidial? per tor la vita
%# a chi s'ingiustamente a te la diede?
%# Ah se cotanto eccede
%# tuo scellerato ardir, giust'è la voglia,
%# che quel viver ingrato,
%# ch'a torto a te fu dato
%# ora a ragione io toglia.
\livretPers Iole
%# Ohimè, s'amore
%# nulla in te puote, arresta.
\livretPers Hyllo
%# Ah genitore.
\livretPers Ercole
%# E con sì dolce nome ancor mi chiami?
\livretPers Hyllo
%# Non creder già, ch'io più di viver brami
%# che per mia miglior sorte
%# non so più desiar altro, che morte.
\livretPers Iole
%# Alcide, ah ch'io fui quella
%# per vendicar Eutyro,
%# e per sottrarmi alle tue insidie, io quella,
%# che sola di trafiggerti tentai.

\livretScene SCENA OTTAVA
\livretDescAtt\wordwrap-center { Deianira, Licco, Ercole, Iole, Hyllo, Paggio. }
\livretPers Ercole
\livretRef#'DHArecit
%# Più di salvarlo tenti
%# più l'accusi, e tu menti,
%# ma ch'al tuo crime, o pure
%# a mie gelose cure
%# il tuo morir s'ascriva
%# soffrir più non saprei, no che tu viva.
\livretPers Deianira
%# Ah barbaro di fé, di pietà avaro.
%# Non basta avermi l'amor tuo ritolto,
%# ch'ancor toglier mi vuoi pegno sì caro;
%# fa' pur tua sposa Iole,
%# abbandonami pure a ogni martoro,
%# ma per solo ristoro
%# lasciami la mia prole.
%# Innocente, che sia,
%# chi propizio gli sia, se ingiusto è il padre?
%# E quand'anche sia reo, concedi il vanto
%# d'impetrarli perdono
%# d'una misera madre al largo pianto.
\livretPers Ercole
%# Ambo morrete, e fra tant'altre prove
%# che fer di me già sì famoso il grido
%# dicasi ancor, ch'altri duo mostri uccisi
%# una moglie gelosa, e un figlio infido.
\livretPers Deianira
%#- Ah crudo.
\livretPers Iole
%#= Ah senti pria: s'alcuna spene
%# ch'io pieghi all'amor tuo, restar ti puote,
%# solo al viver di lui questa s'attiene;
%# s'ei mor, fia, ch'ogni speme anco a te pera,
%# e s'egli vive, spera.
\livretPers Ercole
%# E s'egli vive spera? ogni possanza
%# sovra l'anime amanti ha la speranza.
%# Vanne tu dunque, e torna al patrio nido,
%# e tu va' prigioniero
%# nella torre del mar, ch'altro riparo
%# sicuro aver non può mia gelosia,
%# e con Iole intanto io vedrò chiaro
%# del mio sperar, del viver tuo che fia?

\livretScene SCENA NONA
\livretDescAtt\wordwrap-center { Deianira, Hyllo. }
\livretPers Deianira
\livretRef#'DIAduo
%# Figlio tu prigioniero?
\livretPers Hyllo
%# Madre tu discacciata?
\livretPers Deianira
%# E vive in sen di padre un cor sì fiero?
\livretPers Hyllo
%# Ed in cor di marito alma sì ingrata.
\livretPers Deianira
%# Figlio tu prigioniero?
\livretPers Hyllo
%# Madre tu discacciata?
\livretPers Deianira
%# Non fosse a te crudele,
%# e gli perdonerei l'infedeltà.
\livretPers Hyllo
%# Non fosse a te infedele,
%# e lieve troverei sua crudeltà.
\livretPers Deianira e Hyllo
%# S'a te pietà non spero
%# ogni sorte a me fia sempre spietata.
\livretPers Deianira
%# Figlio tu prigioniero?
\livretPers Hyllo
%# Madre tu discacciata?
\livretPers Deianira
%#- Figlio…
\livretPers Hyllo
%#- Madre…
\livretPers Deianira e Hyllo
%#= Ogn'or desti
%# a me dell'amor tuo segni più espressi,
%# ah voglia il ciel, che questi
%# non sian gli ultimi amplessi.

\livretScene SCENA DECIMA
\livretDescAtt\wordwrap-center { Licco, Paggio. }
\livretPers Licco
\livretRef#'DJArecit
%#- A dio, Paggio.
\livretPers Paggio
%#- A dio, tutti.
\livretPers Licco
%#= A rivederci;
%# che della donna a cui Ercol presume
%# di far sì facilmente cangiar clima,
%# non fu mai suo costume
%# d'obbedir alla prima.
\livretPers Paggio
%# Oh che gran cose ho viste! ancor l'orrore
%# tutto mi raccapriccia.
\livretPers Licco
%# Ed è sol mastro Amore,
%# che si fatti bitumi oggi impiastriccia,
%# ma contro un sì pestifero bigatto
%# senti gentil garzone
%# impara una canzone.
\livretPers Licco e Paggio
\livretRef#'DJBduo
%# Amor, chi ha senno in sé,
%# va già d'accordo,
%# ch'il più contento è in te
%# chi è il più balordo.
%# Ogni dolce, che puoi dare
%# è d'assenzio atro sciroppo
%# e le tue gioie più rare
%# o son false, o costan troppo:
%# e così in simil frode
%# lieto è più chi men vede, e crede, e gode.
\livretDidasPPage\justify {
  La sedia incantata sparisce, e gli Spiriti ch'erano costretti in essa,
  entrano nelle statue del giardino, e animandole formano la 4ª danza
  per fine dell'atto terzo.
}
\sep
