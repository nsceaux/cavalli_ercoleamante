\notesSection "Libretto"
\markuplist\page-columns-title \act\line { LIBRETTO } {
\livretAct PROLOGO
\livretScene SCENA UNICA
\livretDescAtt\justify {
  La scena rappresenta ne’ lati montagne di scogli su li quali si
  vedono giacenti 14 fiumi, che bagnano i regni e le provincie che
  sono o furono sotto la dominazione della corona di Francia. Nella
  prospettiva si vede il mare, e nell’aria Cinzia che discende in una
  gran macchina rappresentante il di lei cielo.
}
\livretPers Coro di Fiumi
\livretRef#'AABcoro
\livretVerse#12 { Qual concorso indovino }
\livretVerse#12 { oggi al mar più vicino }
\livretVerse#12 { del festoso Parigi }
\livretVerse#12 { noi raunò dal gemino emisfero, }
\livretVerse#12 { noi, che del franco impero }
\livretVerse#12 { vantiamo il nobil giogo, o i bei vestigi? }
\livretPers Cinzia
\livretRef#'AAFArecit
\livretVerse#12 { Ed ecco o Gallia invitta }
\livretVerse#12 { i tuoi pregi più grandi, e immortali }
\livretVerse#12 { mira del primo ciel ne' puri argenti }
\livretVerse#12 { come in tempio d'onor lampe lucenti }
\livretVerse#12 { l'idee delle maggior stirpi reali. }
\livretPers Coro di Fiumi
\livretRef#'AADcoro
\livretVerse#12 { A i di lei veri accenti }
\livretVerse#12 { su dunque attenti, attenti. }
\livretPers Cinzia
\livretRef#'AAFCrecit
\livretVerse#12 { Di queste il ciel con ammirabil cura, }
\livretVerse#12 { e con stupor del tempo, e di natura, }
\livretVerse#12 { scettri a scettri innestando, e fregi, a fregi }
\livretVerse#12 { la prosapia formò de i franchi regi; }
\livretVerse#12 { che qual fiume di glorie }
\livretVerse#12 { da' monti di Corone, e fasci alteri }
\livretVerse#12 { trasse i fonti primieri }
\livretVerse#12 { ed accresciuto ogn'or da copiosi }
\livretVerse#12 { torrenti di vittorie, }
\livretVerse#12 { e da' più generosi }
\livretVerse#12 { rivi di sangue augusto oltre gli Achei }
\livretVerse#12 { per interrotto, e limpido sentiero }
\livretVerse#12 { tra margini di palme, e di trofei }
\livretVerse#12 { inondò trionfante il mondo intero. }
\livretVerse#12 { Alfin tra l'auree sponde }
\livretVerse#12 { della Senna guerriera }
\livretVerse#12 { fissò la reggia in cui benigna infonde }
\livretVerse#12 { grazie a nembi ogni sfera, }
\livretVerse#12 { ed or più che mai prodigo }
\livretVerse#12 { di contentezze eteree }
\livretVerse#12 { ad ibera beltà franco valore }
\livretVerse#12 { su talamo di pace unisce Amore. }
\livretPers Coro di Fiumi
\livretRef#'AAGcoro
\livretVerse#12 { Dopo belliche noie }
\livretVerse#12 { oh che soavi gioie! }
\livretVerse#12 { A dolcezze sì rare oltre ogni segno }
\livretVerse#12 { Gallia dilata il cor, non men, ch'il regno. }
\livretPers Cinzia
\livretRef#'AAHrecit
\livretVerse#12 { Ma voi che più tardate inclite Dee? }
\livretVerse#12 { Uscite ad inchinare }
\livretVerse#12 { Anna la gran regina, }
\livretVerse#12 { che le bell'alme onde sperar si dée }
\livretVerse#12 { che la serie divina }
\livretVerse#12 { de' vostri alti nipoti il ciel confermi }
\livretVerse#12 { ambo sono di lei rampolli, e germi. }
\livretVerse#12 { Uscite a festeggiare }
\livretVerse#12 { ch'in sì degna allegrezza a vostri balli }
\livretVerse#12 { e veda ogn'un, che desiar non sa }
\livretVerse#12 { un eroico valore }
\livretVerse#12 { qui giù premio maggiore }
\livretVerse#12 { che di godere in pace alma beltà. }
\livretPers Coro di Fiumi
\livretRef#'AAJcoro
\livretVerse#12 { Oh Gallia fortunata }
\livretVerse#12 { già per tante vittorie, }
\livretVerse#12 { di pace, e d'imenei l'ultime glorie }
\livretVerse#12 { ti fanno oltre ogni speme oggi beata. }
\livretVerse#12 { E a fin ch'a tuoi contenti }
\livretVerse#12 { gioia ogn'or s'augumenti }
\livretVerse#12 { ecco, ch'in te si vede }
\livretVerse#12 { alba di nuove glorie un regio erede; }
\livretVerse#12 { per splender più di doppio sole ornata }
\livretVerse#12 { oh Gallia fortunata. }
\livretDidasP\justify {
  Le dette Idee discendono sul palco a danzare, quindi rientrate nella
  medesima macchina, questa si chiude, e le riporta in cielo.
}
\sep
\livretAct ATTO PRIMO
\livretScene SCENA PRIMA
\livretDescAtt\justify {
  La scena si cangia ne' lati in boscareccia, e nella prospettiva in
  un gran paese contiguo alla città d’Eocalia.
}
\livretPers Ercole
\livretRef#'BABercole
\livretVerse#12 { Come si beffa Amor del poter mio! }
\livretVerse#12 { A me cui cede il mondo }
\livretVerse#12 { farà contrasto una donzella? (oh dio!) }
\livretVerse#12 { Come si beffa Amor del poter mio! }
\livretVerse#12 { Dunque chi tanti mostri }
\livretVerse#12 { vide esangui trofei di sua fortezza }
\livretVerse#12 { scempio sarà di femminil fierezza, }
\livretVerse#12 { e trafitto cadrà da un van desio? }
\livretVerse#12 { Come si beffa Amor del poter mio! }

\livretVerse#12 { O di quale empietà }
\livretVerse#12 { sacrilego tiranno ogn'or riempi }
\livretVerse#12 { il credulo tuo regno? }
\livretVerse#12 { Mentre ne' di lui tempi }
\livretVerse#12 { l'adorate Cortine }
\livretVerse#12 { di grazia, e di beltà }
\livretVerse#12 { non celano altro alfine }
\livretVerse#12 { ch'idoli abominevoli qua' sono }
\livretVerse#12 { interesse, perfidia, orgoglio, e sdegno. }
\livretVerse#12 { Così avvien per Iole }
\livretVerse#12 { che l'altar del cor mio }
\livretVerse#12 { sparga d'alti sospir malgrati i fumi, }
\livretVerse#12 { e che vittima infausta io mi consumi. }

\livretScene SCENA SECONDA
\livretDescAtt\justify {
  Cala dal cielo Venere con le Grazie in una macchina.
  Venere, Ercole, coro di Grazie.
}
\livretPers Venere
\livretRef#'BBArecit
\livretVerse#12 { Se ninfa a i pianti }
\livretVerse#12 { di veri amanti }
\livretVerse#12 { non mai pieghevole }
\livretVerse#12 { niega mercè; }
\livretVerse#12 { di ciò colpevole }
\livretVerse#12 { amor non è. }
\livretPers Coro
\livretVerse#12 { Se ninfa a i pianti }
\livretVerse#12 { di veri amanti }
\livretVerse#12 { non mai pieghevole }
\livretVerse#12 { niega mercè; }
\livretVerse#12 { di ciò colpevole }
\livretVerse#12 { amor non è. }
\livretPers Venere
\livretVerse#12 { Scoglio sì rigido }
\livretVerse#12 { mostro sì frigido }
\livretVerse#12 { non regge il mar }
\livretVerse#12 { ch’amato al pari non deva amar. }
\livretPers Coro
\livretVerse#12 { Scoglio sì rigido }
\livretVerse#12 { mostro sì frigido }
\livretVerse#12 { non regge il mar }
\livretVerse#12 { ch’amato al pari non deva amar. }
\livretPers Venere
\livretRef#'BBBrecit
\livretVerse#12 { Ogn’impero ha ribelli }
\livretVerse#12 { trasgressori ogni legge }
\livretVerse#12 { or come e questi, e quelli }
\livretVerse#12 { giusta forza corregge, }
\livretVerse#12 { sì con soave incanto }
\livretVerse#12 { (ch’al dominio d’Amore }
\livretVerse#12 { forza è la più conforme) }
\livretVerse#12 { superare a tuo pro spero il rigore }
\livretVerse#12 { che maligna fortuna, }
\livretVerse#12 { sempre al mio figlio avversa }
\livretVerse#12 { d’Iole in sen per tuo tormento aduna; }
\livretVerse#12 { e godrai de’ miei detti }
\livretVerse#12 { oggi al giardin de’ fiori i dolci effetti. }
\livretPers Ercole
\livretVerse#12 { O dèa se tanto alle mie brame ottieni }
\livretVerse#12 { giusto fia ch’io t’accenda }
\livretVerse#12 { tutte d’Arabia l’odorate selve, }
\livretVerse#12 { e che tutte a te sveni }
\livretVerse#12 { dell’Erimanto le zannute belve; }
\livretPers Venere
\livretVerse#12 { Vanne al loco, e m’attendi, e fa ch’Iole }
\livretVerse#12 { pur vi si renda pria che manchi il sole, }
\livretVerse#12 { ch’io dell’armi provvista }
\livretVerse#12 { onde sua ferità vincer presumo, }
\livretVerse#12 { preverrò diligente i di lei passi }
\livretVerse#12 { per dispor quivi pria, ch’ella vi giunga }
\livretVerse#12 { rovente acuto strale, }
\livretVerse#12 { che per te l’arda, e punga. }

\livretVerse#12 { Strale invisibile, }
\livretVerse#12 { ch’inevitabile }
\livretVerse#12 { tal forza avrà, }
\livretVerse#12 { ch’all’insensibile }
\livretVerse#12 { piaga insanabile }
\livretVerse#12 { imprimerà. }
\livretPers Venere
\livretVerse#12 { Su dunque ogni tristezza }
\livretVerse#12 { sia dal tuo cor sbandita, }
\livretVerse#12 { ch’in amor l’allegrezza }
\livretVerse#12 { come al ciel più gradita }
\livretVerse#12 { con più felicità le gioie invita. }
\livretPers Venere e Ercole
\livretRef#'BBCduoCoro
\livretVerse#12 { Fuggano a vol }
\livretVerse#12 { dal bell’impero }
\livretVerse#12 { del nume arciero }
\livretVerse#12 { le pene, e ’l duol. }
\livretPers Coro
\livretVerse#12 { E in lui così }
\livretVerse#12 { gioie sol piovino, }
\livretVerse#12 { e si rinnovino }
\livretVerse#12 { quegli aurei dì. }
\livretPers Venere e Ercole
\livretVerse#12 { Struggasi il gel }
\livretVerse#12 { d’ogni fierezza }
\livretVerse#12 { ogni amarezza }
\livretVerse#12 { il cangi in miel. }
\livretPers Coro
\livretVerse#12 { E in lui così }
\livretVerse#12 { gioie sol piovino, }
\livretVerse#12 { e si rinnovino }
\livretVerse#12 { quegli aurei dì. }
\livretDidasP\justify {
  La macchina di Venere rimonta al cielo.
}
\livretPers Ercole
\livretRef#'BBDrecit
\livretVerse#12 { Infelice, e disperato }
\livretVerse#12 { mentre mestissimo }
\livretVerse#12 { vo notte, e dì, }
\livretVerse#12 { qual di bene inaspettato }
\livretVerse#12 { raggio purissimo }
\livretVerse#12 { m’apparì? }

\livretScene SCENA TERZA
\livretDescAtt\justify {
  Nel resto de’ nuvoli di detta macchina essendo ascosa Giunone, questa
  si discovre assisa in un gran pavone.
}
\livretPers Giunone
\livretRef#'BCArecit
\livretVerse#12 { E vuol dunque ciprigna, }
\livretVerse#12 { per far contro di me gl’ultimi sforzi }
\livretVerse#12 { de’ più pungenti oltraggi, }
\livretVerse#12 { favorir chi le voglie ebbe sì intese }
\livretVerse#12 { ad offendermi ogn’ora, }
\livretVerse#12 { che ne gli impuri suoi principi ancora }
\livretVerse#12 { prima d’esser m’offese? }
\livretVerse#12 { Chi pria di spirar l’aure }
\livretVerse#12 { spirò desio di danneggiarmi, e dopo }
\livretVerse#12 { aver dal petto mio }
\livretVerse#12 { tratti i primi alimenti al viver suo, }
\livretVerse#12 { con ingrata insolenza }
\livretVerse#12 { d’uccidermi tentando osò ferirmi? }
\livretVerse#12 { Ah ch’intesi i disegni }
\livretVerse#12 { ma non sia ch’a disfarli altri m’insegni. }
\livretVerse#12 { Di reciproco affetto }
\livretVerse#12 { ardon Hyllo, e Iole, }
\livretVerse#12 { e sol per mio dispetto }
\livretVerse#12 { l’iniqua dèa non vuole, }
\livretVerse#12 { ch’Imeneo li congiunga? anzi procura }
\livretVerse#12 { per il mio scorno maggiore, }
\livretVerse#12 { ch’il nodo maritale ond’è ristretto }
\livretVerse#12 { Ercole a Deianira alfin si rompa; }
\livretVerse#12 { a ciò ch’Iole a questi }
\livretVerse#12 { del di lei genitore empio omicida }
\livretVerse#12 { con mostruosi amplessi oggi s’innesti. }
\livretVerse#12 { E con qual arte oh dio? con arti indegne }
\livretVerse#12 { d’ogni anima più vil non che divina. }
\livretRef#'BCBAaria
\livretVerse#12 { Ma in amor ciò ch’altri fura }
\livretVerse#12 { più d’amor gioia non è }
\livretVerse#12 { e un’insipida ventura }
\livretVerse#12 { ciò ch’egli in dono, o ver pietà non diè. }
\livretVerse#12 { In amor ciò ch’altri fura }
\livretVerse#12 { più d’amor gioia non è. }
\livretVerse#12 { Se non vien da grata arsura }
\livretVerse#12 { volontaria all’altrui fé }
\livretVerse#12 { cangia affatto di natura }
\livretVerse#12 { come d’odio condita ogni mercè. }

\livretVerse#12 { Ma che più con inutili lamenti }
\livretVerse#12 { il tempo scarso alla difesa io perdo? }
\livretVerse#12 { Su portatemi o venti }
\livretVerse#12 { alla grotta del Sonno, e d’aure infeste }
\livretVerse#12 { corteggiato il mio tron versi per tutto }
\livretVerse#12 { pompe del mio furor fiamme, e tempeste. }
\livretDidasP\justify {
  Giunone parte e fa cader dalle nuvole della sua macchina, Tempeste e
  Fulmini che formano una danza per fine del primo atto.
}
\sep
\livretAct ATTO SECONDO
\livretScene SCENA PRIMA
\livretDescAtt\center-column {
  \justify {
    La scena si cangia in un gran cortile del palazzo reale.
  }
  Hyllo, e Iole.
}
\livretPers Hyllo e Iole
\livretRef#'CABduo
\livretVerse#12 { Amor ardor più rari }
\livretVerse#12 { accesi mai non ha, }
\livretVerse#12 { che quelli onde del pari }
\livretVerse#12 { le nostre alme disfà }
\livretVerse#12 { d'avverso ciel le lampe }
\livretVerse#12 { contro di lui si sforzino, }
\livretVerse#12 { ch'in vece, che l'amorzino, }
\livretVerse#12 { l'arricchiran di vampe. }
\livretPers Iole
\livretRef#'CACrecit
\livretVerse#12 { Pure alfine il rispetto }
\livretVerse#12 { di figlio al genitor fia ch'in te cangi }
\livretVerse#12 { sì amoroso linguaggio. }
\livretPers Hyllo
\livretVerse#12 { Che più tosto il tuo affetto }
\livretVerse#12 { non renda anch'egli al forte Alcide omaggio. }
\livretPers Iole
\livretVerse#12 { Ah che forzar un core }
\livretVerse#12 { no 'l puote altri che amore. }
\livretPers Hyllo
\livretVerse#12 { E di rivale il titolo odioso }
\livretVerse#12 { qualunque altro bel nome, }
\livretVerse#12 { che concorra con lui, rende ozioso; }
\livretVerse#12 { una sol vita il genitor mi diede, }
\livretVerse#12 { e per te, che mia vita }
\livretVerse#12 { molto più cara sei }
\livretVerse#12 { mille vite darei. }
\livretPers Iole
\livretVerse#12 { E per te sol mio ben, }
\livretVerse#12 { all'empio usurpator contenta i' cedo }
\livretVerse#12 { il regno, e 'l mondo tutto, e te sol chiedo. }

\livretScene SCENA SECONDA
\livretDescAtt\wordwrap-center { Paggio, Iole, e Hyllo. }
\livretPers Paggio
\livretRef#'CBArecit
\livretVerse#12 { Ercole a dirti invia, ch'altro non bada, }
\livretVerse#12 { che di saper, se nel giardin de' fiori }
\livretVerse#12 { di condurti a diporto oggi t'aggrada. }
\livretPers Iole
\livretVerse#12 { Come fia, che ciò nieghi? }
\livretVerse#12 { D'un che sovra di me le stelle alzaro }
\livretVerse#12 { son comandi anco i prieghi. }
\livretPers Hyllo
\livretVerse#12 { Ahi qual torbido, e amaro }
\livretVerse#12 { velen presaga gelosia m'appresta, }
\livretVerse#12 { di cui solo il timor già mi funesta. }
\livretPers Iole
\livretVerse#12 { Non temere Hyllo caro: }
\livretVerse#12 { che non potrà mai violenza ardita }
\livretVerse#12 { togliermi a te, senza a me tor la vita. }
\livretPers Hyllo
\livretVerse#12 { E quando anche in tal guisa }
\livretVerse#12 { ogn'un meco ti perda amato bene, }
\livretVerse#12 { qual miglior sorte avrò, che cangiar pene? }
\livretPers Iole
\livretVerse#12 { Da sì grave timor l'alma disvezza, }
\livretVerse#12 { che quanto Ercol per me palesa affetto, }
\livretVerse#12 { tant'ha rispetto, ed io per te fermezza. }
\livretVerse#12 { Torna, digli, ch'io vado: Hyllo vien meco. }
\livretPers Hyllo
\livretVerse#12 { E quando io non son teco! }
\livretVerse#12 { Se dovunque il mio piè giri, o la mente }
\livretVerse#12 { t'adoro ogn'or presente. }
\livretRef#'CBBduo
\livretVerse#12 { Chi può vivere un sol istante }
\livretVerse#12 { lunge dal bello che l'invaghì, }
\livretVerse#12 { dica pur, ch'in lui morì }
\livretVerse#12 { ogni pregio di vero amante; }
\livretPers Iole
\livretVerse#12 { O gloria }
\livretVerse#12 { d'amor più nobile }
\livretVerse#12 { con fede immobile }
\livretVerse#12 { sempr'arde più; }
\livretVerse#12 { memoria }
\livretVerse#12 { non mai vi fu, }
\livretVerse#12 { che la vittoria }
\livretVerse#12 { mancassi tu. }
\livretPers Iole e Hyllo
\livretVerse#12 { D'amore il foco }
\livretVerse#12 { per ogni poco }
\livretVerse#12 { ch'intiepidiscasi ghiaccio diviene, }
\livretVerse#12 { e le di lui catene }
\livretVerse#12 { più strettamente avvolte }
\livretVerse#12 { ogni poco, che cedano, son sciolte. }

\livretScene SCENA TERZA
\livretPers Paggio
\livretRef #'CCArecit
\livretVerse#12 { E che cosa è quest'amore? }
\livretVerse#12 { Di cui parlan tanto in corte, }
\livretVerse#12 { e canzon di mille sorte }
\livretVerse#12 { di lui cantano a tutt'ore. }
\livretVerse#12 { Egli è qualche ciurmadore }
\livretVerse#12 { poi che a quel, che sento dire }
\livretVerse#12 { (senza punto intender come) }
\livretVerse#12 { mentre a stille dà il gioire }
\livretVerse#12 { e il penar dispensa a some, }
\livretVerse#12 { fassi il mondo adoratore }
\livretVerse#12 { egli è qualche ciurmadore. }
\livretVerse#12 { Di vederlo ebbi gran brame }
\livretVerse#12 { ma poi seppi, ch'è impossibile, }
\livretVerse#12 { ch'egli sia già mai visibile }
\livretVerse#12 { perché sempre è con le dame, }
\livretVerse#12 { e che queste al finger dotte }
\livretVerse#12 { se lo tengano celato, }
\livretVerse#12 { come s'ei stesse appiattato }
\livretVerse#12 { dentro le cimmerie grotte. }

\livretScene SCENA QUARTA
\livretDescAtt\wordwrap-center { Deianira, Licco, Paggio. }
\livretPers Licco
\livretRef#'CDArecit
\livretVerse#12 { Buon dì gentil fanciullo. }
\livretPers Paggio
\livretVerse#12 { E buona notte. }
\livretPers Licco
\livretVerse#12 { Ma dove in tanta fretta? }
\livretPers Paggio
\livretVerse#12 { A far da gran messaggio. }
\livretPers Licco
\livretVerse#12 { Ascolta un poco, aspetta; }
\livretVerse#12 { che so qual possa aver faccende un Paggio. }
\livretPers Paggio
\livretVerse#12 { E che tu sai? ch'Iole }
\livretVerse#12 { ad Ercole… }
\livretPers Licco
\livretVerse#12 { \transparent { ad Ercole… } T'invia. }
\livretPers Paggio
\livretVerse#12 { Sì affé m'invia… }
\livretPers Licco
\livretVerse#12 { A dirgli. }
\livretPers Paggio
\livretVerse#12 { \transparent { A dirgli. } È vero a dirgli… }
\livretPers Licco e Paggio
\livretVerse#12 { Ch'al giardino de' fiori }
\livretVerse#12 { ella si renderà com'ei desia. }
\livretPers Paggio
\livretRef#'CDBrecit
\livretVerse#12 { Sei tu qualche indovino? }
\livretPers Licco
\livretVerse#12 { E ben famoso, }
\livretVerse#12 { ch'in simil guisa a me nulla è nascoso. }
\livretPers Deianira
\livretVerse#12 { Ah crudo, ah disleale, }
\livretVerse#12 { ah traditore, ingrato, }
\livretVerse#12 { ah scellerato, ed empio. }
\livretVerse#12 { Ah Deianira ogni ristor dispera, }
\livretVerse#12 { ch'a morir di dolor sei destinata. }
\livretPers Paggio
\livretVerse#12 { Che? cotesta straniera }
\livretVerse#12 { anch'essa è innamorata? }
\livretPers Licco
\livretVerse#12 { Così mi dice, ma d'amor ben vero, }
\livretVerse#12 { come saggio io non credo, }
\livretVerse#12 { ch'a gli uomini, poco, ed alle donne un zero. }
\livretPers Paggio
\livretVerse#12 { Basta per questa corte ogn'or volare }
\livretVerse#12 { si vede un sì gran numero d'amori, }
\livretVerse#12 { che non abbiamo a fare, }
\livretVerse#12 { che ne vengan di fuori. }
\livretVerse#12 { Ama Hyllo Iole riamato, e l'ama }
\livretVerse#12 { Ercole assai malvisto, ama Nicandro }
\livretVerse#12 { Licori, e questa Oreste, e Oreste Olinda, }
\livretVerse#12 { e Olinda, e Celia scaltre }
\livretVerse#12 { aman le gemme, e l'oro, }
\livretVerse#12 { e Niso, ed Alidoro aman cent'altre. }
\livretPers Licco
\livretVerse#12 { E perché ha in odio Iole }
\livretVerse#12 { Ercole? }
\livretPers Paggio
\livretVerse#12 { \transparent { Ercole? } Perché uccise Eutyro. }
\livretPers Licco
\livretVerse#12 { \transparent { Ercole? Perché uccise Eutyro. } Ed ama }
\livretVerse#12 { il figlio poi di chi gli uccise il padre? }
\livretVerse#12 { Ha la pianta in orrore, ed ama il frutto? }
\livretVerse#12 { Che vuoi giocar ch'io so }
\livretVerse#12 { la ragion che di ciò }
\livretVerse#12 { ella in sé covane? }
\livretVerse#12 { Un d'essi è troppo adulto, e l'altro è giovane }
\livretPers Paggio
\livretVerse#12 { Fin da principio Iole ardea per Hyllo }
\livretVerse#12 { onde per compiacerla }
\livretVerse#12 { le già date promesse }
\livretVerse#12 { delle nozze di lei ritolse Eutyro }
\livretVerse#12 { ad Ercole, ch'al fin sì mal soffrillo, }
\livretVerse#12 { ch'una tal dalla figlia opra gradita }
\livretVerse#12 { all'infelice re costò la vita. }
\livretVerse#12 { E tu, ch'il tutto sai }
\livretVerse#12 { non sai, ch'Ercol' m'attende? e ch'egli è amante? }
\livretVerse#12 { E che fra quanti mai }
\livretVerse#12 { ardono al mondo d'amorosa fiamma }
\livretVerse#12 { non v'è di pazienza una sol dramma. }

\livretScene SCENA QUINTA
\livretDescAtt\wordwrap-center {
  Deianira, Licco.
}
\livretPers Deianira
\livretRef#'CEArecit
\livretVerse#12 { Misera, ohimè, ch'ascolto. }
\livretVerse#12 { Non so, se più gelosa }
\livretVerse#12 { esser dèa come madre, o come sposa; }
\livretVerse#12 { che comune è il periglio }
\livretVerse#12 { alla mia fede coniugale, e al figlio; }
\livretVerse#12 { almen con soffrir l'uno }
\livretVerse#12 { schivar l'altro potessi: oh dio qual sorte }
\livretVerse#12 { prefisse iniquo fato a i miei natali: }
\livretVerse#12 { ch'io soffra a doppio i mali, }
\livretVerse#12 { né per schivarne alcun basti mia morte. }
\livretVerse#12 { O presagi funesti: }
\livretVerse#12 { Ercol spirti non ha, se non feroci, }
\livretVerse#12 { e non serian già questi }
\livretVerse#12 { i di lui primi parricidi atroci. }
\livretVerse#12 { Come mal mi lasciai }
\livretVerse#12 { strascinar da' miei guai }
\livretVerse#12 { a queste eubee contrade, }
\livretVerse#12 { ove il destin mi fabbricò l'inferno: }
\livretVerse#12 { ora, ahi lassa, discerno }
\livretVerse#12 { quanto meglio era entro le patrie mura }
\livretVerse#12 { di Calidonia sospirar piangendo }
\livretVerse#12 { miei dubbi oltraggi, che con duol più orrendo }
\livretVerse#12 { esserne qui sicura. }
\livretRef#'CEBaria
\livretVerse#12 { Ahi ch'amarezza }
\livretVerse#12 { meschina me }
\livretVerse#12 { è la certezza }
\livretVerse#12 { di rotta fé! }
\livretVerse#12 { Ahi come, ohimè, }
\livretVerse#12 { la gelosia }
\livretVerse#12 { di furie l'Erebo impoverì. }
\livretVerse#12 { E l'alma mia }
\livretVerse#12 { ne riempì. }
\livretVerse#12 { S'in amor si raddoppiassero }
\livretVerse#12 { tutti i guai, tutti i tormenti, }
\livretVerse#12 { e ch'in lui solo mancassero }
\livretVerse#12 { i sospetti, e i tradimenti }
\livretVerse#12 { fora amor tutta dolcezza. }
\livretPers Licco
\livretRef#'CECrecit
\livretVerse#12 { Ah fu sempre in amor stolto consiglio }
\livretVerse#12 { il cercar di sapere }
\livretVerse#12 { punto di più, che quel basta a godere; }
\livretVerse#12 { copron l'indiche balze }
\livretVerse#12 { sotto aspetto villan viscere d'oro; }
\livretVerse#12 { ma ben contrario affatto }
\livretVerse#12 { l'amoroso terreno }
\livretVerse#12 { sotto una superficie preziosa }
\livretVerse#12 { sol cattiva materia ha in sé nascosa. }
\livretVerse#12 { Onde chi vuole in lui }
\livretVerse#12 { gir scavando tal'or con mesta prova }
\livretVerse#12 { più s'inoltra a cercar peggio ritrova; }
\livretVerse#12 { ben lo dicea, che noi sariam venuti }
\livretVerse#12 { a incontrar pene, e rischi: }
\livretVerse#12 { ah che d'Ercole irato }
\livretVerse#12 { qualche stral ben rotato }
\livretVerse#12 { parmi sentir, ch'intorno a me già fischi. }
\livretPers Deianira
\livretVerse#12 { Ah Licco il cor ti manca, ohimè, che fia }
\livretVerse#12 { di me senza il tuo aiuto? }
\livretPers Licco
\livretVerse#12 { Ah Deianira: }
\livretVerse#12 { dunque, dunque tu temi? }
\livretVerse#12 { Io non ho già paura. }
\livretPers Deianira
\livretVerse#12 { E in tanto tremi. }
\livretPers Licco
\livretVerse#12 { Ma ve'; poiché nel mondo }
\livretVerse#12 { ogni cosa ha misura; }
\livretVerse#12 { forz'è che l'abbia ancor la mia bravura }
\livretVerse#12 { e siccome tra quelle, }
\livretVerse#12 { che fè nemico ciel senza danari }
\livretVerse#12 { chi ha quattro soldi è ricco: }
\livretVerse#12 { così per bravo io solamente spicco }
\livretVerse#12 { fra tutti quanti li poltron miei pari. }
\livretPers Deianira
\livretVerse#12 { Dunque che far dovrem? }
\livretPers Licco
\livretVerse#12 { Avvertir ne conviene }
\livretVerse#12 { che qualche beffa, o crocchio }
\livretVerse#12 { (grazie, ch'alli stranier versa ogni corte) }
\livretVerse#12 { non c'irriti a parlare, e di tal sorte }
\livretVerse#12 { farem la guerra all'occhio. }


\livretScene SCENA SESTA
\livretDescAtt\center-column {
  \justify { La scena si cangia nella grotta del Sonno. }
  \wordwrap-center {
    Pasithea, il Sonno, coro d'Aure e Ruscelli.
  }
}
\livretPers Pasithea
\livretRef#'CFAaria
\livretVerse#12 { Mormorate }
\livretVerse#12 { o fiumicelli, }
\livretVerse#12 { sussurrate }
\livretVerse#12 { o venticelli, }
\livretVerse#12 { e col vostro sussurro, e mormorio }
\livretVerse#12 { dolci incanti dell'oblio, }
\livretVerse#12 { ch'ogni cura fugar ponno }
\livretVerse#12 { lusingate al sonno il Sonno. }
\livretVerse#12 { Chi da ver ama }
\livretVerse#12 { vie più il diletto }
\livretVerse#12 { del caro oggetto }
\livretVerse#12 { che 'l proprio brama, }
\livretVerse#12 { quind'è ch'io posi }
\livretVerse#12 { la notte, e 'l die }
\livretVerse#12 { le contentezze mie }
\livretVerse#12 { del consorte gentil ne' bei riposi. }
\livretPers Coro
\livretRef#'CFBcoro
\livretVerse#12 { Dormi, dormi, o Sonno dormi }
\livretVerse#12 { fra le braccia a Pasithea }
\livretVerse#12 { ninfa aver non ti potea }
\livretVerse#12 { più d'affetti a' tuoi conformi: }
\livretVerse#12 { dormi, dormi o Sonno dormi. }
\livretVerse#12 { Dormi, dormi o Sonno dormi }
\livretVerse#12 { sovra a te gli amori istessi }
\livretVerse#12 { lente movano le piume; }
\livretVerse#12 { e al tuo cor placido nume, }
\livretVerse#12 { gelosia mai non appressi }
\livretVerse#12 { de' suoi rei sospetti i stormi }
\livretVerse#12 { dormi, dormi o Sonno dormi. }

\livretScene SCENA SETTIMA
\livretDescAtt\center-column {
  \wordwrap-center { Cala Giunone dal cielo. }
  \wordwrap-center {
    Giunone, Pasithea, il Sonno, coro d'Aure e Ruscelli.
  }
}
\livretPers Pasithea
\livretRef#'CGArecit
\livretVerse#12 { O dèa sublime dèa, }
\livretVerse#12 { e qual nuovo desio }
\livretVerse#12 { a quest'umile albergo oggi ti mena? }
\livretPers Giunone
\livretVerse#12 { Zelo dell'onor mio }
\livretVerse#12 { e della fede altrui }
\livretVerse#12 { a me già sacra, e da sacrarsi, a cui }
\livretVerse#12 { e frodi, e violenze altri prepara, }
\livretVerse#12 { onde per fare a ciò schermo innocente }
\livretVerse#12 { sol per una breve ora }
\livretVerse#12 { di condur meco il Sonno uopo mi fora. }
\livretPers Pasithea
\livretVerse#12 { Ohimè di nuovo esporre }
\livretVerse#12 { di Giove all'ire ogni mio ben vorrai? }
\livretVerse#12 { No, ciò non fia più mai. }
\livretPers Giunone
\livretVerse#12 { Non temer Pasithea, }
\livretVerse#12 { che solo è mio pensiero }
\livretVerse#12 { di valermi di lui con men che numi }
\livretVerse#12 { di già soggetti al di lui pigro impero. }
\livretPers Pasithea
\livretVerse#12 { E di ciò m'assicuri? }
\livretPers Giunone
\livretVerse#12 { S'ancor vuoi che te 'l giuri }
\livretVerse#12 { sul germano di lui lo stigio Lete. }
\livretPers Pasithea
\livretVerse#12 { Basta Giuno: quiete }
\livretVerse#12 { son già le mie voglie al tuo desir sovrano. }
\livretPers Giunone
\livretVerse#12 { Porgilo dunque a me, diva, pian piano… }
\livretDidasPPage\justify {
  Giunone prende nel suo carro il Sonno e parte.
}
\livretRef#'CGCaria
\livretVerse#12 { Dell'amorose pene }
\livretVerse#12 { sospirato ristoro, }
\livretVerse#12 { vital dolce tesoro, }
\livretVerse#12 { ch'il mondo più che Cerere mantiene }
\livretVerse#12 { dal neghittoso speco }
\livretVerse#12 { soffri di venir meco, }
\livretVerse#12 { ch'Amore oggi dispone }
\livretVerse#12 { contro l'empia insolenza }
\livretVerse#12 { di straniera potenza }
\livretVerse#12 { della sua libertà farti campione. }

\livretPers Tutti
\livretRef#'CGDAtutti
\livretVerse#12 { Le rugiade più preziose }
\livretVerse#12 { tuoi papaveri ogn'or bagnino, }
\livretVerse#12 { e per tutto gigli, e rose }
\livretVerse#12 { co' lor aliti t'accompagnino. }
\livretDidasPPage\justify {
  Li Sogni giacenti per la grotta formano sognando la 3ª danza per
  fine del 2º atto.
}
\sep
\livretAct ATTO TERZO
\livretScene SCENA PRIMA
\livretDescAtt\center-column {
  \justify {
    Si cangia la scena in un giardino d’Eocalia, e Venere cala dal
    cielo a terra, in una nuvola, che sparisce.
  }
  Venere, Ercole.
}
\livretPers Ercole
\livretRef#'DACrecit
\livretVerse#12 { E per me cangi o dèa }
\livretVerse#12 { le delizie del ciel con questo suolo }
\livretVerse#12 { ed or perché non manda }
\livretVerse#12 { la palude Lerneà }
\livretVerse#12 { e la selva Nemeà }
\livretVerse#12 { nov'idre, altri leoni a far qui meco }
\livretVerse#12 { gloriosi contrasti, }
\livretVerse#12 { onde a te formi o dèa grati olocausti? }
\livretPers Venere
\livretVerse#12 { Pur ch'io giunga a cangiar nel crudo seno }
\livretVerse#12 { d'Iole il core, e te lo renda amante }
\livretVerse#12 { ne trarrò tal piacere, }
\livretVerse#12 { che fia d'ogni opra mia premio bastante, }
\livretVerse#12 { mira quest'è la verga onde fa Circe }
\livretVerse#12 { magiche meraviglie; }
\livretVerse#12 { al di cui moto ubbidienti ancelle }
\livretVerse#12 { per patto inalterabile son tutte }
\livretVerse#12 { de' lidi Acherontei l'anime felle. }
\livretVerse#12 { Or in virtù di sì potente stelo }
\livretVerse#12 { dove tocco la terra }
\livretVerse#12 { nascerà seggio erboso in cui riposte, }
\livretVerse#12 { da spiriti lascivi a ciò costretti }
\livretVerse#12 { le mandragore oscene }
\livretVerse#12 { di pallido color la Lidia pietra }
\livretVerse#12 { e d'amorose rondinelle i cori }
\livretVerse#12 { faran ch'Iole allor, ch'in lui s'affida }
\livretVerse#12 { cangi per te il suo sdegno in dolci amori. }
\livretDidasPPage\wordwrap {
  (nasce di sotto terra la sedia incantata fatta di erbe e di fiori)
}
\livretPers Ercole
\livretVerse#12 { Diva ad opre sì rare }
\livretVerse#12 { insolito tremor tutto mi scuote, }
\livretVerse#12 { e poi ch'esser non puote }
\livretVerse#12 { timor (da me non conosciuto ancora) }
\livretVerse#12 { forz'è che sia per inspirar superno }
\livretVerse#12 { di futuro gioir presagio interno. }
\livretVerse#12 { Ma pur nel pensier mio sceman di pregio }
\livretVerse#12 { quelli, ch'a me prometti }
\livretVerse#12 { sospirati diletti, }
\livretVerse#12 { qual or lasso m'avveggio }
\livretVerse#12 { ch'a far miei dì giocondi }
\livretVerse#12 { tratte non fian tai gioie }
\livretVerse#12 { dal mar d'amor, ma da gli stigi fondi. }
\livretPers Venere
\livretVerse#12 { O di questa canzon }
\livretVerse#12 { pur che tu goda }
\livretVerse#12 { ch'importa a te? }
\livretVerse#12 { Che sia per froda }
\livretVerse#12 { o per mercé? }
\livretVerse#12 { Pur che tu goda }
\livretVerse#12 { ch'importa a te? }

\livretVerse#12 { Ch'altro è l'amare? }
\livretVerse#12 { Ch'un guerreggiare, }
\livretVerse#12 { ove in trionfo egual lieti se n' vanno }
\livretVerse#12 { il valor, e l'inganno; }
\livretVerse#12 { infelice non sai? }
\livretVerse#12 { Che nel gran regno del mio figlio arciero }
\livretVerse#12 { non v'è (tolto il penar) nulla di vero. }
\livretVerse#12 { Prendi il crin, che fortuna }
\livretVerse#12 { per mia man t'offre in dono. }
\livretVerse#12 { Ma mentre a te giusta ragion m'invola }
\livretVerse#12 { se d'altro uopo ti sia }
\livretVerse#12 { Mercurio invierò, che ratto vola. }
\livretPers Venere e Ercole
\livretRef#'DADduo
\livretVerse#12 { E perché Amor non fa, }
\livretVerse#12 { ch'all'amorosa schiera }
\livretVerse#12 { sol delle gioie sue sia dispensiera }
\livretVerse#12 { o ragione, o pietà? }
\livretVerse#12 { E perché crudeltà }
\livretVerse#12 { perché il rigor, }
\livretVerse#12 { in guardia ogn'or le avrà? }
\livretVerse#12 { Dunque per involarle ogn'arte ancor }
\livretVerse#12 { lecita altrui sarà: }
\livretVerse#12 { d'un ardente desio giungerà 'l segno }
\livretVerse#12 { sì, sì, gioco è d'ingegno. }

\livretScene SCENA SECONDA
\livretDescAtt\wordwrap-center { Ercole, Paggio. }
\livretPers Ercole
\livretRef#'DBAercole
\livretVerse#12 { Amor contar ben puoi }
\livretVerse#12 { fra tuoi non minor vanti }
\livretVerse#12 { che dell'ardir, che torre a me non seppe }
\livretVerse#12 { co' latrati di Cerbero, e orrendi }
\livretVerse#12 { strepiti suoi lo spaventoso abisso; }
\livretVerse#12 { tu disarmato m'hai, sì ch'io, che colsi }
\livretVerse#12 { ad onta del terribile custode, }
\livretVerse#12 { con intrepida man l'Esperie frutta, }
\livretVerse#12 { quasi di sostenere or non ardisco }
\livretVerse#12 { l'avvicinar del bel per cui languisco. }

\livretVerse#12 { O quale instillano }
\livretVerse#12 { in arso petto }
\livretVerse#12 { rai, che sfavillano }
\livretVerse#12 { di gran beltà, }
\livretVerse#12 { umil rispetto, }
\livretVerse#12 { bassa umiltà: }
\livretVerse#12 { il ciel ben sa }
\livretVerse#12 { a sì suprema }
\livretVerse#12 { adorabil maestà, }
\livretVerse#12 { s'ei pur non trema? }

\livretPers Paggio
\livretRef#'DBBrecit
\livretVerse#12 { Sarà com'hai disposto }
\livretVerse#12 { Iole qui ben tosto. }
\livretPers Ercole
\livretVerse#12 { E dove la trovasti? }
\livretPers Paggio
\livretVerse#12 { Nel cortil regio a favellar d'amore. }
\livretPers Ercole
\livretVerse#12 { A favellar d'amor? con chi? deh dillo, }
\livretVerse#12 { dell'amor mio? }
\livretPers Paggio
\livretVerse#12 { Dell'amor suo con Hyllo. }
\livretPers Ercole
\livretVerse#12 { Come? Dunque il mio figlio }
\livretVerse#12 { mio rivale divenne? }
\livretVerse#12 { A tal temerità sarebbe ei giunto? }
\livretVerse#12 { Tu non hai ben compreso }
\livretVerse#12 { semplicetto garzone. }
\livretPers Paggio
\livretVerse#12 { Eccoli appunto. }

\livretScene SCENA TERZA
\livretDescAtt\wordwrap-center {
  Ercole, Iole, Hyllo, coro di Damigelle, e Paggio.
}
\livretPers Ercole
\livretRef#'DCArecit
\livretVerse#12 { Bella Iole, e quando mai }
\livretVerse#12 { sentirai }
\livretVerse#12 { di me pietà? }
\livretVerse#12 { Chi la chiede al tuo rigore }
\livretVerse#12 { ha valore }
\livretVerse#12 { per domare ogn'impietà }
\livretVerse#12 { ma non sia, che teco impieghi }
\livretVerse#12 { se non prieghi }
\livretVerse#12 { e mesti lai; }
\livretVerse#12 { bell'Iole, e quando mai? }
\livretPers Iole
\livretVerse#12 { Quando il mio cor capace }
\livretVerse#12 { fosse d'un lieve amor per chi m'uccise }
\livretVerse#12 { il genitor diletto }
\livretVerse#12 { aver per me dovresti }
\livretVerse#12 { orrore, e non affetto. }
\livretPers Ercole
\livretVerse#12 { Ah bella Iole }
\livretVerse#12 { a sì gran crime, e di sì gran castigo }
\livretVerse#12 { degno, qual per me fora }
\livretVerse#12 { l'impossibilità dell'amor tuo: }
\livretVerse#12 { imputar mi vorrai }
\livretVerse#12 { una prova fatale, }
\livretVerse#12 { ed un impulso senza freno, oh dio, }
\livretVerse#12 { dell'infinito ardor, dell'amor mio? }
\livretVerse#12 { Quand'il tonante istesso }
\livretVerse#12 { negarmi com'Eutyro, avesse ardito }
\livretVerse#12 { un ben sì desiato, e a me promesso, }
\livretVerse#12 { come già contro il sole, e 'l dio triforme }
\livretVerse#12 { stato non fora contra lui men parco }
\livretVerse#12 { di strali avvelenati il mio grand'arco. }
\livretPers Iole
\livretVerse#12 { Io sola fui cagion, che il re mio padre }
\livretVerse#12 { rompesse a te la data fede. }
\livretPers Ercole
\livretVerse#12 { Ah come }
\livretVerse#12 { a ciò tu l'inducesti? }
\livretVerse#12 { Dunque tu l'uccidesti. }
\livretVerse#12 { Che d'un mal, che si feo, }
\livretVerse#12 { chi la causa ne diè, quegli n'è reo. }
\livretVerse#12 { Ma pon bella in oblio }
\livretVerse#12 { sì funeste memorie, e sì noiose, }
\livretVerse#12 { e qui meco t'assidi, }
\livretVerse#12 { poiché depost'anch'io }
\livretVerse#12 { l'innata mia ferocia, anzi cangiata }
\livretVerse#12 { in conocchia la clava }
\livretVerse#12 { ravisar ti farò, che quale ogn'altra }
\livretVerse#12 { tua più devota ancella }
\livretVerse#12 { non mai prenderò a vile }
\livretVerse#12 { di renderti ogni ossequio il più servile; }
\livretVerse#12 { qua gira gli occhi Atlante }
\livretVerse#12 { e per somma beltà }
\livretVerse#12 { mira quel, ch'oggi fa }
\livretVerse#12 { Ercole amante: }
\livretVerse#12 { ma non ne rider già }
\livretVerse#12 { che se tale è il voler }
\livretVerse#12 { del pargoletto arcier. }

\livretVerse#12 { Tutte son opre gloriose, e belle }
\livretVerse#12 { tanto il filar, che sostener le stelle. }
\livretVerse#12 { Sol per voler d'Amore, }
\livretVerse#12 { chi in ciel Etho frenò }
\livretVerse#12 { armenti ancor guidò }
\livretVerse#12 { nume, e pastore: }
\livretVerse#12 { e non ne riser no }
\livretVerse#12 { gl'altri dèi, ch'il mirar, }
\livretVerse#12 { che fan ben ch'in amar: }
\livretVerse#12 { tutte son opre gloriose, e belle }
\livretVerse#12 { tanto il filar, che sostener le stelle. }

\livretPers Iole
\livretRef#'DCCrecit
\livretVerse#12 { Ma qual? ma come io sento }
\livretVerse#12 { spuntare entro il mio petto }
\livretVerse#12 { per te improvviso, e involontario affetto }
\livretVerse#12 { onde forz'è ch'io t'ami }
\livretVerse#12 { e ch'amor mio ti chiami. }
\livretPers Hyllo
\livretVerse#12 { Ohimè, ch'ascolto! }
\livretVerse#12 { E non sogno? e son desto? e non già stolto? }
\livretVerse#12 { Così cangiasi Iole? }
\livretVerse#12 { Fragil femminea fede; }
\livretVerse#12 { ben merta i tradimenti un, che ti crede. }
\livretPers Ercole
\livretVerse#12 { Hyllo, di che ti offendi? }
\livretVerse#12 { Che senso ha tal linguaggio? }
\livretVerse#12 { (Non mal l'intese il Paggio) }
\livretVerse#12 { ami tu dunque Iole? }
\livretPers Hyllo
\livretVerse#12 { Io per un'empia }
\livretVerse#12 { ingrata al padre, al mondo, al ciel spergiura, }
\livretVerse#12 { che soffrissi nel cuor d'amor l'arsura? }
\livretVerse#12 { Per una sì mutabile, ch'a un tratto }
\livretVerse#12 { con subito contento }
\livretVerse#12 { alla mia genitrice, a Deianira }
\livretVerse#12 { tecò a far sì gran torto (ohimè) cospira? }
\livretVerse#12 { Versi pria sul mio capo irato Giove }
\livretVerse#12 { tutti i fulmini suoi, }
\livretVerse#12 { e il più nero baratro m'ingoi. }
\livretPers Iole
\livretVerse#12 { O me infelice, o misera, che fei? }
\livretVerse#12 { Uccidetemi, oh dèi. }
\livretPers Ercole
\livretVerse#12 { Finora a te d'Eutyro }
\livretVerse#12 { ne men di Deianira unqua non calse. }
\livretVerse#12 { Parti, e ringrazia il ciel; che ben ti valse, }
\livretVerse#12 { che d'esser mite oggi disposi. }
\livretPers Hyllo
\livretVerse#12 { \transparent { che d'esser mite oggi disposi. } A dio: }
\livretVerse#12 { andrò morte a cercar per quelle balze. }

\livretScene SCENA QUARTA
\livretDescAtt\wordwrap-center { Ercole, Iole, Paggio. }
\livretPers Ercole
\livretRef#'DDArecit
\livretVerse#12 { E tu a che pensi Iole? }
\livretPers Iole
\livretVerse#12 { All'error mio, }
\livretVerse#12 { se ben ciò che mia lingua }
\livretVerse#12 { disse pur dianzi ah no, non lo diss'io. }
\livretVerse#12 { E l'alma forsennata, }
\livretVerse#12 { nel frenetico errore }
\livretVerse#12 { altra parte non ebbe }
\livretVerse#12 { che di gran pentimento alto dolore. }
\livretPers Ercole
\livretVerse#12 { Dunque su di tua mano }
\livretVerse#12 { per fermezza amorosa }
\livretVerse#12 { pegno porgimi sol d'esser mia sposa. }
\livretPers Iole
\livretVerse#12 { No'l rifiuto, ma lascia, }
\livretVerse#12 { ch'in segrete preghiere }
\livretVerse#12 { del genitore all'oltraggiato spirto }
\livretVerse#12 { per addolcirlo in qualche guisa almeno }
\livretVerse#12 { prima, ch'affatto a te mi doni in preda, }
\livretVerse#12 { io licenza ne chieda. }

\livretScene SCENA QUINTA
\livretDescAtt\center-column {
  \justify {
    Torna ad apparir in aria Giunone nel suo carro col Sonno.
  }
  \wordwrap-center {
    Giunone col Sonno, Ercole, Iole, Paggio.
  }
}
\livretPers Giunone
\livretRef#'DEArecit
\livretVerse#12 { Sonno potente nume }
\livretVerse#12 { fu qui pur opportuno il nostro arrivo; }
\livretVerse#12 { dunque poiché tu sei }
\livretVerse#12 { dell'innocenza amico, }
\livretVerse#12 { impedisci pietoso al par, che giusto }
\livretVerse#12 { oggi un crime il più nero, }
\livretVerse#12 { che contro amor la frode unqua tentasse, }
\livretVerse#12 { e con la verga a cui fu facil prova }
\livretVerse#12 { le sempre deste luci }
\livretVerse#12 { tutte velare ad Argo }
\livretVerse#12 { vanne veloce, e in Ercole produci }
\livretVerse#12 { un più cieco letargo. }
\livretPers Iole
\livretRef#'DECArecit
\livretVerse#12 { E quale inaspettato }
\livretVerse#12 { sonno prodigioso }
\livretVerse#12 { prevenendo Imeneo lega il mio sposo? }
\livretPers Giunone
\livretVerse#12 { Iole, Iole, ah sorgi }
\livretVerse#12 { sorgi rapida, e fuggi, e t'allontana }
\livretVerse#12 { dall'incantato seggio, e a me t'appressa }
\livretVerse#12 { che di ben tosto risanarti è d'uopo }
\livretVerse#12 { dal magico veleno, }
\livretVerse#12 { ond'hai l'anima oppressa. }
\livretPers Iole
\livretVerse#12 { O diva, o dèa, da quali }
\livretVerse#12 { orridi precipizi }
\livretVerse#12 { d'infedeltà, d'iniquità risorgo? }
\livretVerse#12 { Ohimè! di quali errori }
\livretVerse#12 { rea, quantunque innocente ora mi scorgo! }
\livretVerse#12 { Pure il mio primo, e sol gradito fuoco, }
\livretVerse#12 { ch'in me pareva estinto }
\livretVerse#12 { mentre il cor mi ralluma, }
\livretVerse#12 { con usura di fiamme }
\livretVerse#12 { più che mai mi consuma. }
\livretVerse#12 { Ma che pro? s'Hyllo intanto }
\livretVerse#12 { l'unico mio tesoro }
\livretVerse#12 { senza mia colpa a ragion meco irato, }
\livretVerse#12 { a ragion da me fugge, e a torto io moro. }
\livretRef#'xerces
\livretVerse#12 { Luci mie, voi che miraste }
\livretVerse#12 { quel bel sol che m’abbagliò, }
\livretVerse#12 { voi che semplici cercaste }
\livretVerse#12 { il crin d’or che mi legò, }
\livretVerse#12 { voi che del mio penar la colpa avette }
\livretVerse#12 { di dover lagrimar non vi dolete. }
\livretPers Giunone
\livretRef#'DECBrecit
\livretVerse#12 { Ah perché perdi Iole }
\livretVerse#12 { in superflue querele }
\livretVerse#12 { tempo sì prezioso, Hyllo non lunge }
\livretVerse#12 { per mio consiglio in un cespuglio ascoso }
\livretVerse#12 { tutto guata, e ascolta. Arma più tosto }
\livretVerse#12 { arma figlia la mano }
\livretVerse#12 { di questo acuto acciaro, }
\livretVerse#12 { (ch'abile a penetrare ogni riparo }
\livretVerse#12 { per me temprò Vulcano) }
\livretVerse#12 { e mentre imprigionato }
\livretVerse#12 { da i legami del Sonno i più tenaci }
\livretVerse#12 { sta quel mostro sì crudo }
\livretVerse#12 { d'ogni difesa ignudo, }
\livretVerse#12 { vanne, e vendica ardita }
\livretVerse#12 { con la morte di lui }
\livretVerse#12 { le mie offese, e i tuoi danni, }
\livretVerse#12 { ch'altro scampo non ha d'Hyllo la vita. }
\livretVerse#12 { Vanne, e poiché spedita al ciel'io torno }
\livretVerse#12 { ad ovviare in ciò l'ire di Giove }
\livretVerse#12 { fa' ch'io vi giunga il crin di lauri adorno. }

\livretScene SCENA SESTA
\livretDescAtt\wordwrap-center { Iole, Hyllo, Ercole che dorme, Paggio. }
\livretPers Iole
\livretRef#'DFArecit
\livretVerse#12 { D'Eutyro anima grande }
\livretVerse#12 { a questo core, a questo braccio imbelle }
\livretVerse#12 { tanto furor, tanto vigor comparti }
\livretVerse#12 { che possa or qui sacrarti, }
\livretVerse#12 { con insigne vendetta }
\livretVerse#12 { (universal di cui desio rimbomba) }
\livretVerse#12 { vittima sì dovuta alla tua tomba. }
\livretVerse#12 { Prendi o mio genitor dall'arso lido }
\livretVerse#12 { di Flegetonte, il sangue }
\livretVerse#12 { di quest'empio tiranno, }
\livretVerse#12 { che nel tuo nome uccido. }
\livretPers Hyllo
\livretVerse#12 { Ohimè, che fai? }
\livretVerse#12 { Cessa. }
\livretPers Iole
\livretVerse#12 { \transparent { Cessa. } Deh lascia. }
\livretPers Hyllo
\livretVerse#12 { \transparent { Cessa. Deh lascia. } Ah cessa. }
\livretPers Iole
\livretVerse#12 { Lascia se m'ami. }
\livretPers Hyllo
\livretVerse#12 { Ah che del pari io sono }
\livretVerse#12 { tuo vero amante, e di lui figlio. }
\livretVerse#12 { Lo placherò, quando non basti il pianto, }
\livretVerse#12 { con la mia morte. }
\livretPers Iole
\livretVerse#12 { E sì poco è gradita }
\livretVerse#12 { la speme a te d'esser mio sposo (oh dio) }
\livretVerse#12 { che per essa non pregi }
\livretVerse#12 { punto di più la vita? }

\livretScene SCENA SETTIMA
\livretScene SCENA PRIMA
\livretDescAtt\center-column {
  \justify {
    Mercurio d’un volo risveglia Ercole e parte.
  }
  \wordwrap-center {
    Mercurio, Hyllo, Iole, Ercole, Paggio.
  }
}
\livretPers Mercurio
\livretRef#'DGArecit
\livretVerse#12 { Svegliati Alcide, e mira. }
\livretPers Ercole
\livretVerse#12 { E dove, o bella? }
\livretVerse#12 { Dove? ah qui pur di nuovo }
\livretVerse#12 { temerario importuno io ti ritrovo? }
\livretVerse#12 { Ed a qual fine impugni }
\livretVerse#12 { ferro micidial? per tor la vita }
\livretVerse#12 { a chi s'ingiustamente a te la diede? }
\livretVerse#12 { Ah se cotanto eccede }
\livretVerse#12 { tuo scellerato ardir, giust'è la voglia, }
\livretVerse#12 { che quel viver ingrato, }
\livretVerse#12 { ch'a torto a te fu dato }
\livretVerse#12 { ora a ragione io toglia. }
\livretPers Iole
\livretVerse#12 { Ohimè, s'amore }
\livretVerse#12 { nulla in te puote, arresta. }
\livretPers Hyllo
\livretVerse#12 { Ah genitore. }
\livretPers Ercole
\livretVerse#12 { E con sì dolce nome ancor mi chiami? }
\livretPers Hyllo
\livretVerse#12 { Non creder già, ch'io più di viver brami }
\livretVerse#12 { che per mia miglior sorte }
\livretVerse#12 { non so più desiar altro, che morte. }
\livretPers Iole
\livretVerse#12 { Alcide, ah ch'io fui quella }
\livretVerse#12 { per vendicar Eutyro, }
\livretVerse#12 { e per sottrarmi alle tue insidie, io quella, }
\livretVerse#12 { che sola di trafiggerti tentai. }

\livretScene SCENA OTTAVA
\livretDescAtt\wordwrap-center { Deianira, Licco, Ercole, Iole, Hyllo, Paggio. }
\livretPers Ercole
\livretRef#'DHArecit
\livretVerse#12 { Più di salvarlo tenti }
\livretVerse#12 { più l'accusi, e tu menti, }
\livretVerse#12 { ma ch'al tuo crime, o pure }
\livretVerse#12 { a mie gelose cure }
\livretVerse#12 { il tuo morir s'ascriva }
\livretVerse#12 { soffrir più non saprei, no che tu viva. }
\livretPers Deianira
\livretVerse#12 { Ah barbaro di fé, di pietà avaro. }
\livretVerse#12 { Non basta avermi l'amor tuo ritolto, }
\livretVerse#12 { ch'ancor toglier mi vuoi pegno sì caro; }
\livretVerse#12 { fa' pur tua sposa Iole, }
\livretVerse#12 { abbandonami pure a ogni martoro, }
\livretVerse#12 { ma per solo ristoro }
\livretVerse#12 { lasciami la mia prole. }
\livretVerse#12 { Innocente, che sia, }
\livretVerse#12 { chi propizio gli sia, se ingiusto è il padre? }
\livretVerse#12 { E quand'anche sia reo, concedi il vanto }
\livretVerse#12 { d'impetrarli perdono }
\livretVerse#12 { d'una misera madre al largo pianto. }
\livretPers Ercole
\livretVerse#12 { Ambo morrete, e fra tant'altre prove }
\livretVerse#12 { che fer di me già sì famoso il grido }
\livretVerse#12 { dicasi ancor, ch'altri duo mostri uccisi }
\livretVerse#12 { una moglie gelosa, e un figlio infido. }
\livretPers Deianira
\livretVerse#12 { Ah crudo. }
\livretPers Iole
\livretVerse#12 { \transparent { Ah crudo. } Ah senti pria: s'alcuna spene }
\livretVerse#12 { ch'io pieghi all'amor tuo, restar ti puote, }
\livretVerse#12 { solo al viver di lui questa s'attiene; }
\livretVerse#12 { s'ei mor, fia, ch'ogni speme anco a te pera, }
\livretVerse#12 { e s'egli vive, spera. }
\livretPers Ercole
\livretVerse#12 { E s'egli vive spera? ogni possanza }
\livretVerse#12 { sovra l'anime amanti ha la speranza. }
\livretVerse#12 { Vanne tu dunque, e torna al patrio nido, }
\livretVerse#12 { e tu va' prigioniero }
\livretVerse#12 { nella torre del mar, ch'altro riparo }
\livretVerse#12 { sicuro aver non può mia gelosia, }
\livretVerse#12 { e con Iole intanto io vedrò chiaro }
\livretVerse#12 { del mio sperar, del viver tuo che fia? }

\livretScene SCENA NONA
\livretDescAtt\wordwrap-center { Deianira, Hyllo. }
\livretPers Deianira
\livretRef#'DIAduo
\livretVerse#12 { Figlio tu prigioniero? }
\livretPers Hyllo
\livretVerse#12 { Madre tu discacciata? }
\livretPers Deianira
\livretVerse#12 { E vive in sen di padre un cor sì fiero? }
\livretPers Hyllo
\livretVerse#12 { Ed in cor di marito alma sì ingrata. }
\livretPers Deianira
\livretVerse#12 { Figlio tu prigioniero? }
\livretPers Hyllo
\livretVerse#12 { Madre tu discacciata? }
\livretPers Deianira
\livretVerse#12 { Non fosse a te crudele, }
\livretVerse#12 { e gli perdonerei l'infedeltà. }
\livretPers Hyllo
\livretVerse#12 { Non fosse a te infedele, }
\livretVerse#12 { e lieve troverei sua crudeltà. }
\livretPers Deianira e Hyllo
\livretVerse#12 { S'a te pietà non spero }
\livretVerse#12 { ogni sorte a me fia sempre spietata. }
\livretPers Deianira
\livretVerse#12 { Figlio tu prigioniero? }
\livretPers Hyllo
\livretVerse#12 { Madre tu discacciata? }
\livretPers Deianira
\livretVerse#12 { Figlio… }
\livretPers Hyllo
\livretVerse#12 { \transparent { Figlio… } Madre… }
\livretPers Deianira e Hyllo
\livretVerse#12 { \transparent { Figlio… Madre… } Ogn'or desti }
\livretVerse#12 { a me dell'amor tuo segni più espressi, }
\livretVerse#12 { ah voglia il ciel, che questi }
\livretVerse#12 { non sian gli ultimi amplessi. }

\livretScene SCENA DECIMA
\livretDescAtt\wordwrap-center { Licco, Paggio. }
\livretPers Licco
\livretRef#'DJArecit
\livretVerse#12 { A dio, Paggio. }
\livretPers Paggio
\livretVerse#12 { \transparent { A dio, Paggio. } A dio, tutti. }
\livretPers Licco
\livretVerse#12 { \transparent { A dio, Paggio. A dio, tutti. } A rivederci; }
\livretVerse#12 { che della donna a cui Ercol presume }
\livretVerse#12 { di far sì facilmente cangiar clima, }
\livretVerse#12 { non fu mai suo costume }
\livretVerse#12 { d'obbedir alla prima. }
\livretPers Paggio
\livretVerse#12 { Oh che gran cose ho viste! ancor l'orrore }
\livretVerse#12 { tutto mi raccapriccia. }
\livretPers Licco
\livretVerse#12 { Ed è sol mastro Amore, }
\livretVerse#12 { che si fatti bitumi oggi impiastriccia, }
\livretVerse#12 { ma contro un sì pestifero bigatto }
\livretVerse#12 { senti gentil garzone }
\livretVerse#12 { impara una canzone. }
\livretPers Licco e Paggio
\livretRef#'DJBduo
\livretVerse#12 { Amor, chi ha senno in sé, }
\livretVerse#12 { va già d'accordo, }
\livretVerse#12 { ch'il più contento è in te }
\livretVerse#12 { chi è il più balordo. }
\livretVerse#12 { Ogni dolce, che puoi dare }
\livretVerse#12 { è d'assenzio atro sciroppo }
\livretVerse#12 { e le tue gioie più rare }
\livretVerse#12 { o son false, o costan troppo: }
\livretVerse#12 { e così in simil frode }
\livretVerse#12 { lieto è più chi men vede, e crede, e gode. }
\livretDidasPPage\justify {
  La sedia incantata sparisce, e gli Spiriti ch'erano costretti in essa,
  entrano nelle statue del giardino, e animandole formano la 4ª danza
  per fine dell'atto terzo.
}
\sep
\livretAct ATTO QUARTO
\livretScene SCENA PRIMA
\livretDescAtt\center-column {
  \justify {
    La scena si cangia in un mare sui liti del quale sono molte torri,
    ed in una di esse Hyllo prigioniero.
  }
  Hyllo.
}
\livretPers Hyllo
\livretRef#'EAAaria
\livretVerse#12 { Ahi che pena è gelosia }
\livretVerse#12 { ad un'alma innamorata }
\livretVerse#12 { ch'a i sospetti abbandonata }
\livretVerse#12 { teme ogn'or sorte più ria. }
\livretVerse#12 { Ad Alcide allor ch'Iole }
\livretVerse#12 { crudelmente in ver me pia, }
\livretVerse#12 { di sperar alfin concesse; }
\livretVerse#12 { io credei, che m'uccidesse, }
\livretVerse#12 { solo il suon di tai parole, }
\livretVerse#12 { ma il morir manco duol fia. }

\livretVerse#12 { Ma che veggio? ecco un messo, }
\livretVerse#12 { che viene a dritta voga, è il Paggio? è desso. }

\livretScene SCENA SECONDA
\livretDescAtt\center-column {
  \justify { Apparisce nel detto mare il Paggio in una barchetta. }
  Paggio, Hyllo.
}
\livretPers Paggio
\livretRef#'EBAaria
\livretVerse#12 { Zefiri che gite }
\livretVerse#12 { da' vicini fiori }
\livretVerse#12 { involando odori }
\livretVerse#12 { e qua poi fuggite; }
\livretVerse#12 { fate alla mia prora }
\livretVerse#12 { ch'oggi il mar si spiani, }
\livretVerse#12 { voi pur cortigiani }
\livretVerse#12 { siete de l'aurora. }
\livretVerse#12 { Noto è a voi Cupido }
\livretVerse#12 { che d'ogn'un fa giuoco, }
\livretVerse#12 { e per l'altrui fuoco }
\livretVerse#12 { or me trae dal lido. }
\livretVerse#12 { A voi pur convenne }
\livretVerse#12 { far l'ufficio mio, }
\livretVerse#12 { così avessi anch'io }
\livretVerse#12 { come voi le penne. }
\livretPers Hyllo
\livretRef#'EBBrecit
\livretVerse#12 { Che novella m'arrechi? è buona, o rea? }
\livretVerse#12 { Ma che parlo infelice? }
\livretVerse#12 { Sperar più verun bene a me non lice. }
\livretPers Paggio
\livretVerse#12 { Iole alfin astretta }
\livretVerse#12 { di maritarsi al furibondo Alcide }
\livretVerse#12 { con questo foglio a te mi spinse in fretta. }
\livretPers Hyllo
\livretVerse#12 { Porgilo dunque; }
\livretDidasPPage\wordwrap { (legge il biglietto) }
\livretVerse#12 { “Alla tua fé tradita, }
\livretVerse#12 { chiedo giusto perdono, }
\livretVerse#12 { se per serbarti in vita }
\livretVerse#12 { ad Ercole mi dono.” }
\livretVerse#12 { Che per serbarmi in vita? Oh cieco errore! }
\livretVerse#12 { Ah, che ciò per me fia morte peggiore. }
\livretVerse#12 { Torna veloce, oh dio, }
\livretVerse#12 { torna veloce, e dille, }
\livretVerse#12 { ch'essendo essa fedele all'amor mio, }
\livretVerse#12 { se morrò, sì contento }
\livretVerse#12 { scenderà questo spirto al basso mondo, }
\livretVerse#12 { ch'in alcun tempo mai }
\livretVerse#12 { non ne vider gli elisei un più giocondo. }
\livretVerse#12 { Ma che, s'altrui si dona, o il duol atroce }
\livretVerse#12 { di sì perfida sorte, }
\livretVerse#12 { o la mia destra mi darà in tal punto }
\livretVerse#12 { una sì amara, e sconsolata morte, }
\livretVerse#12 { ch'affannosa, e dolente }
\livretVerse#12 { quest'alma in approdar le stigie arene }
\livretVerse#12 { infin quivi parrà mostro di pene. }
\livretVerse#12 { Saprai tu ben ridir queste querele? }
\livretPers Paggio
\livretVerse#12 { Pur ch'il mar infedele }
\livretVerse#12 { non mi vieti il ritorno, e di già parmi }
\livretVerse#12 { che ben voglia agitarmi: o numi algosi }
\livretVerse#12 { correte al mio soccorso. }
\livretDidasPPage\wordwrap { Si muove la tempesta in mare. }

\livretScene SCENA TERZA
\livretDescAtt\wordwrap-center { Hyllo. }
\livretRef#'ECArecit
\livretVerse#12 { E non si trova }
\livretVerse#12 { fra gl'armenti squamosi }
\livretVerse#12 { un cor benché gelato, }
\livretVerse#12 { che qual già d'Arione }
\livretVerse#12 { di quel meschin garzone }
\livretVerse#12 { senta qualche pietade, e salvi insieme }
\livretVerse#12 { gl'ultimi avanzi in lui d'ogni mia speme }
\livretVerse#12 { ohimè, ch'il mar con cento fauci, e cento }
\livretVerse#12 { tutte rabbia spumanti }
\livretVerse#12 { non par ch'ad altro furioso aneli }
\livretVerse#12 { ch'a divorar quel poverello. Ah date }
\livretVerse#12 { a sì mortal periglio }
\livretVerse#12 { pronto soccorso o cieli; }
\livretVerse#12 { ohimè, che più tardate? }
\livretDidasPage\wordwrap { Il Paggio si sommerge. }
\livretVerse#12 { Ah che quella voragine l'ingoia, }
\livretVerse#12 { dunque forz'è, che disperato io moia: }
\livretVerse#12 { su, su, dunque a morir, ché 'l chiaro nome }
\livretVerse#12 { dell'amato mio sole }
\livretVerse#12 { indorar mi potrà l'ombre più dense }
\livretVerse#12 { del Tartaro profondo: Iole, Iole. }
\livretDidasPage\wordwrap { Hyllo si precipita in mare. }

\livretScene SCENA QUARTA
\livretDescAtt\center-column {
  \justify {
    Apparisce nell'aria Giunone, in un gran trono e cala in soccorso
    d'Hyllo.
  }
  \wordwrap-center { Giunone, Nettuno, Hyllo. }
}
\livretPers Giunone
\livretRef#'EDArecit
\livretVerse#12 { Salva, Nettuno, ah salva }
\livretVerse#12 { quel troppo ardito giovine, e sovvienti, }
\livretVerse#12 { che t'acquistò non favorevol grido }
\livretVerse#12 { il negato soccorso }
\livretVerse#12 { all'amoroso nuotator d'Abido. }
\livretVerse#12 { Salvalo, o dio triforme, }
\livretVerse#12 { che d'Ercole comun nostro nemico }
\livretVerse#12 { all'alma inviperita }
\livretVerse#12 { far non si può da noi più grande oltraggio }
\livretVerse#12 { che di salvare il di lui figlio in vita; }
\livretVerse#12 { Ah tu non m'odi? o vi ripugni? adunque? }
\livretVerse#12 { In quest'onde ver me già sì cortesi }
\livretVerse#12 { quell'antica bontà del tutto è spenta? }
\livretDidasPage\justify {
  Sorge dal mar Nettuno in una gran conchiglia tirata da cavalli marini,
  e in essa si vede Hyllo salvato.
}
\livretPers Nettuno
\livretVerse#12 { Eccoti, o dèa contenta; }
\livretVerse#12 { che nulla al tuo voler negar poss'io; }
\livretVerse#12 { né fu mia negligenza }
\livretVerse#12 { ma ben sua renitenza il tardar mio; }
\livretVerse#12 { né credo unqua più avvenne, }
\livretVerse#12 { che dall'orribil gola }
\livretVerse#12 { della vorace, e non mai sazia Dite }
\livretVerse#12 { fosser ritorti a forza }
\livretVerse#12 { contro la lor voglia i miseri mortali }
\livretVerse#12 { come or succede in questo. }
\livretRef#'EDBaria
\livretVerse#12 { Amanti che tra pene }
\livretVerse#12 { ogn'or gridate ohimè: }
\livretVerse#12 { perché bramate di morir, perché? }
\livretVerse#12 { Ah non negate mai fede alla spene. }
\livretVerse#12 { Per chi vive il ciel gira, }
\livretVerse#12 { e non sempre un sospira, }
\livretVerse#12 { anzi lieto è tal'or chi mesto fu, }
\livretVerse#12 { ma per chi more il ciel non gira più. }
\livretPers Giunone
\livretRef#'EDCrecit
\livretVerse#12 { Saggiamente a te parla, Hyllo, quel nume. }
\livretPers Nettuno
\livretVerse#12 { Vanne veloce, e la gran diva inchina }
\livretVerse#12 { a dio forma regina. }
\livretDidasPage\justify {
  Hyllo entra nella macchina di Giunone, e Nettuno s'attuffa nel mare.
}

\livretScene SCENA QUINTA
\livretDescAtt\wordwrap-center {
  Giunone, Hyllo, coro di Zefiri, che danzano, e suonano.
}
\livretPers Giunone
\livretRef#'EEArecit
\livretVerse#12 { Dunque del mio potere }
\livretVerse#12 { diffiderai tu solo? }
\livretPers Hyllo
\livretVerse#12 { Diva a che viver più chi vive al duolo? }
\livretVerse#12 { Ma pure ossequioso }
\livretVerse#12 { ti chieggio umil perdono, }
\livretVerse#12 { che quantunque penoso, }
\livretVerse#12 { grato il viver mi fia poich'è tuo dono. }
\livretPers Giunone
\livretVerse#12 { Vanne dunque, e pur spera, e non t'annoi }
\livretVerse#12 { il dar più fede a me, ch'a i sensi tuoi. }
\livretRef#'EEBaria
\livretVerse#12 { Congedo a gl'orridi }
\livretVerse#12 { suoi flutti altissimi }
\livretVerse#12 { poi ch'il mar diè, }
\livretVerse#12 { zefiri floridi }
\livretVerse#12 { su festosissimi }
\livretVerse#12 { volate a me, }
\livretVerse#12 { e in danza lepida }
\livretVerse#12 { da voi si venere }
\livretVerse#12 { la mia virtù, }
\livretVerse#12 { che sempre intrepida }
\livretVerse#12 { contro di Venere }
\livretVerse#12 { vittrice fu. }
\livretPers Coro
\livretVerse#12 { Su, su lieti in nobil gara }
\livretVerse#12 { ogni nume col suo lume }
\livretVerse#12 { aspiri a celebrar dea si chiara. }
\livretDidasP\justify {
  Scendono sul palco Hyllo e Giunone e poi questa parte e rimonta al
  cielo nella sua macchina, nella quale i Zefiri invitati da essa
  formano la 5ª danza.
}

\livretScene SCENA SESTA
\livretDescAtt\center-column {
  \justify {
    Si cangia la scena in un giardin di cipressi pieno di
    sepolcri reali.
  }
  \wordwrap-center {
    Deianira, Licco.
  }
}
\livretPers Deianira
\livretRef#'EFArecit
\livretVerse#12 { Ed a che peggio i fati ahi mi serbaro? }
\livretVerse#12 { Ah che ben mi guidaro }
\livretVerse#12 { gl'addolorati miei languidi passi }
\livretVerse#12 { a trovare in alcun di questi sassi }
\livretVerse#12 { come far sazio il mio destino avaro. }
\livretVerse#12 { Ed a che peggio i fati ahi mi serbaro? }
\livretVerse#12 { Alfin perduto ho il figlio }
\livretVerse#12 { e già vicina è l'ora, }
\livretVerse#12 { che dona ad altra sposa il mio consorte, }
\livretVerse#12 { né perciò avvien ch'io mora? }
\livretVerse#12 { Armi non ha da uccidermi la morte, }
\livretVerse#12 { già che tanti dolor non mi sbranaro; }
\livretVerse#12 { ed a che peggio i fati ahi mi serbaro? }

\livretVerse#12 { Prendi Licco fedele }
\livretVerse#12 { questi de' miei tesor poveri avanzi }
\livretVerse#12 { per passar meno incomodi i tuoi giorni, }
\livretVerse#12 { e rimira se puoi, }
\livretVerse#12 { un dì questi sepolcri aprirmi in cui }
\livretVerse#12 { d'ogni speranza di conforto ignuda }
\livretVerse#12 { per non mirar più il sol mi colchi, e chiuda. }
\livretPers Licco
\livretVerse#12 { Ah Deianira io non son tanto accorto }
\livretVerse#12 { che possa in sì gran carichi servirti }
\livretVerse#12 { di tesoriere insieme, e beccamorto: }
\livretVerse#12 { né so s'abbi pensato, }
\livretVerse#12 { ch'esser preso così quindi io potrei }
\livretVerse#12 { per omicida, e ladro, }
\livretVerse#12 { e con solennità condotto al posto }
\livretVerse#12 { di sublime appiccato, }
\livretVerse#12 { onde fora tra noi sorte ben varia, }
\livretVerse#12 { tu morresti sotterra, ed io nell'aria. }
\livretVerse#12 { Deh scaccia o Deianira, }
\livretVerse#12 { desio sì forsennato, }
\livretVerse#12 { che di quanti nell'urna abbia Pandora }
\livretVerse#12 { e disastri, e ruine, e pene, e danni, }
\livretVerse#12 { e dolori, ed affanni, }
\livretVerse#12 { e angoscie, e crepacuori io ti so dire, }
\livretVerse#12 { ch'il peggior mal di tutti è di morire. }
\livretVerse#12 { Ma che pompa funebre }
\livretVerse#12 { scorgo venir? tiriamoci in un lato }
\livretVerse#12 { che qual lugubre aspetto a te fia grato. }

\livretScene SCENA SETTIMA
\livretDescAtt\wordwrap-center {
  Iole con la pompa funebre, coro di Sacrificanti,
  ombra d’Eutyro, Deianira, Licco, coro di Damigelle d’Iole.
}
\livretPers Coro di sacrificanti
\livretRef#'EGBcoro
\livretVerse#12 { Gradisci o re, }
\livretVerse#12 { il caldo pianto }
\livretVerse#12 { ch'in mesto ammanto }
\livretVerse#12 { afflitta gente }
\livretVerse#12 { dal cor dolente }
\livretVerse#12 { sparge per te! }
\livretVerse#12 { Gradisci o re. }
\livretVerse#12 { Tua sepoltura }
\livretVerse#12 { i fior riceva }
\livretVerse#12 { che selva oscura }
\livretVerse#12 { germogliar fe': }
\livretVerse#12 { e il sangue beva, }
\livretVerse#12 { che per man monda }
\livretVerse#12 { vacca infeconda }
\livretVerse#12 { svenata diè, }
\livretVerse#12 { gradisci o re. }
\livretPers Iole
\livretRef#'EGCaria
\livretVerse#12 { E se pur negli estinti }
\livretVerse#12 { di generosità pregio rimane, }
\livretVerse#12 { permetti o genitore, }
\livretVerse#12 { che dopo aver io tanto (ahi lassa) invano }
\livretVerse#12 { per vendicarti oprato }
\livretVerse#12 { ceda al voler del fato, }
\livretVerse#12 { e che non già quest'alma, }
\livretVerse#12 { ma sol di lei la sventurata salma }
\livretVerse#12 { per l'iniquo tiranno }
\livretVerse#12 { (per cui grato mi fora }
\livretVerse#12 { più del talamo il rogo) }
\livretVerse#12 { di sforzati imenei sottentri al giogo. }
\livretPers Coro
\livretRef#'EGDcoro
\livretVerse#12 { Ah ch'il real sepolcro }
\livretVerse#12 { formando entro di sé dubbi mugiti: }
\livretVerse#12 { ah, ah, (ch'esser ciò puote?) }
\livretVerse#12 { tutto trema, e si scuote. }
\livretDidasPPage\justify {
  Rovina il sepolcro d'Eutyro, e apparisce l'ombra di lui.
}
\livretPers Eutyro
\livretRef#'EGEaria
\livretVerse#12 { Che sacrifici ingrati? }
\livretVerse#12 { Che prieghi ingiuriosi? }
\livretVerse#12 { Che voti obbrobriosi? }
\livretVerse#12 { Porgonsi a me? così s'oltraggia Eutyro? }
\livretVerse#12 { Così fia, ch'a sua voglia }
\livretVerse#12 { fredda insensibil ombra ogn'un mi creda? }
\livretVerse#12 { Farò ben, che s'avveda }
\livretVerse#12 { l'omicida ladron, s'ancor m'adiro? }
\livretVerse#12 { E se contro di lui }
\livretVerse#12 { odio, rabbia, e furor più che mai spiro? }
\livretVerse#12 { Dunque chi del mio sangue }
\livretVerse#12 { fe' scempio ingiusto, del mio sangue ancora }
\livretVerse#12 { far vorrà suo diletto? ah non fia mai: }
\livretVerse#12 { e tu dar vita a i parti }
\livretVerse#12 { di chi morte a me di è (figlia) potrai? }
\livretPers Iole
\livretRef#'EGFaria
\livretVerse#12 { Ben resistea l'avverso mio volere }
\livretVerse#12 { d'Ercole alle preghiere, }
\livretVerse#12 { e alla forza di lui pur fatta avrei }
\livretVerse#12 { resistenza invincibile, ma d'Hyllo, }
\livretVerse#12 { d'Hyllo a te già non men, ch'a me sì caro, }
\livretVerse#12 { che delle nostre offese }
\livretVerse#12 { non fu complice mai: }
\livretVerse#12 { anzi che ne sofferse }
\livretVerse#12 { al par di noi con amorosa, e immensa }
\livretVerse#12 { compassione il duolo, }
\livretVerse#12 { d'Hyllo, ohimè, di lui solo }
\livretVerse#12 { il periglio mortale }
\livretVerse#12 { m'astrinse a consentire }
\livretVerse#12 { all'aborrite nozze, }
\livretVerse#12 { com'unico riparo al suo morire: }
\livretVerse#12 { dunque perdona, o genitor, l'intento }
\livretVerse#12 { di queste sacre pompe }
\livretVerse#12 { ch'Amor, che non ha legge }
\livretVerse#12 { ogni legge a sua voglia o scioglie, o rompe. }
\livretPers Eutyro
\livretRef#'EGGrecit
\livretVerse#12 { Tant'ha d'Eutyro il nudo spirto ancora }
\livretVerse#12 { invisibil possanza, }
\livretVerse#12 { che neglette, e schernite }
\livretVerse#12 { le temerarie voglie }
\livretVerse#12 { del nemico fellone, }
\livretVerse#12 { saprà salvare insieme }
\livretVerse#12 { l'innocente garzone. }
\livretPers Deianira
\livretVerse#12 { O disperata speme. Hyllo è già morto. }
\livretPers Iole
\livretVerse#12 { Ohimè, che di'! }
\livretPers Deianira
\livretVerse#12 { Sul più vicino scoglio }
\livretVerse#12 { della di lui prigion mentre attendevo, }
\livretVerse#12 { che qualche picciol legno }
\livretVerse#12 { colà mi conducesse }
\livretVerse#12 { a consolarlo almen col mio cordoglio, }
\livretVerse#12 { lo vidi all'improvviso, ohimè, dall'alto }
\livretVerse#12 { cader nel mar d'un salto. }
\livretVerse#12 { E se non lo seguii, }
\livretVerse#12 { fu perché dal dolore, ahi, sopra fatta }
\livretVerse#12 { caddi al suol tramortita, }
\livretVerse#12 { e per man degli astanti }
\livretVerse#12 { con mal saggia pietà quindi fui tratta. }
\livretPers Eutyro
\livretVerse#12 { Dunque a qual altro fin, che per più strano }
\livretVerse#12 { mio spregio, e scorno? Or di te far vorrai }
\livretVerse#12 { un esecrabil dono }
\livretVerse#12 { al barbaro inumano? }
\livretVerse#12 { Ch'altra moglie trafigge, altra abbandona, }
\livretVerse#12 { e né meno a suoi figli empio perdona. }
\livretVerse#12 { Deh con giusto coraggio }
\livretVerse#12 { saggiamente pentita, }
\livretVerse#12 { rinunzia a un tanto error mentr'io ritorno }
\livretVerse#12 { del fumante Cocito all'aria impura }
\livretVerse#12 { alle sponde infocate }
\livretVerse#12 { per unire in congiura }
\livretVerse#12 { l'anime ch'il crudele a morte ha date: }
\livretVerse#12 { e ben vedrai ch'invano io non prefissi }
\livretVerse#12 { di sollevar contro di lui gli abissi. }
\livretDidasPPage\wordwrap { (l’ombra di Eutyro sparisce) }
\livretPers Iole
\livretRef#'EGHaria
\livretVerse#12 { Hyllo il mio bene è morto? altro che pianti }
\livretVerse#12 { vuol da me tal dolore: }
\livretVerse#12 { egli sol per mio amore }
\livretVerse#12 { disperato s'uccise, ed io fra tanti }
\livretVerse#12 { segni della sua fé sempre più chiari }
\livretVerse#12 { fia ch'a morir dalla sua fede impari. }
\livretRef#'EGIrecit
\livretVerse#12 { Attendetemi dunque, alme dilette }
\livretVerse#12 { d'Hyllo, e d'Eutyro in pace, }
\livretVerse#12 { ch'a raggiungervi io corro, ombra seguace. }
\livretPers Licco
\livretVerse#12 { Ferma ti prego, e poiché (grazie al cielo) }
\livretVerse#12 { tornò l'orribil ombra a casa sua, }
\livretVerse#12 { e ch'a me così torna, e fiato, e voce; }
\livretVerse#12 { vuò dar grato consiglio a tutte e dua. }
\livretVerse#12 { E che miglior rimedio? }
\livretVerse#12 { A' tanti vestri spasimi di quello }
\livretVerse#12 { a proporvi son pronto }
\livretVerse#12 { ch'è di guarire ad Ercole il cervello? }
\livretVerse#12 { Dunque non ti sovviene, o Deianira, }
\livretVerse#12 { che per ciò far mezzo sì raro avemo? }
\livretVerse#12 { Veggio, ch'il duol estremo }
\livretVerse#12 { ti rende smemorata, e quella veste, }
\livretVerse#12 { che già Nesso centauro }
\livretVerse#12 { in morendo a te diè, qui pur non vale? }
\livretVerse#12 { Per far ch'Alcide allor che l'abbia in dosso }
\livretVerse#12 { ogn'altro amor ch'il tuo ponga in non cale? }
\livretPers Deianira
\livretVerse#12 { Chi sa, che fia ben ver? }
\livretPers Licco
\livretVerse#12 { \transparent { Chi sa, che fia ben ver? } Ne farem prova. }
\livretPers Iole
\livretVerse#12 { Ma ciò per ravvivare Hyllo non giova. }
\livretPers Licco
\livretVerse#12 { Oh che strane domande! }
\livretVerse#12 { Ma ben potrei risuscitare un morto, }
\livretVerse#12 { s'a contentar due femmine mi posi, }
\livretVerse#12 { ch'è d'ogni altro impossibile il più grande, }
\livretVerse#12 { s'in vece, che per troppa impazienza }
\livretVerse#12 { posar monte su monte }
\livretVerse#12 { avesser li giganti a sasso a sasso }
\livretVerse#12 { fabbricato il lor ponte; }
\livretVerse#12 { al dispetto di Giove }
\livretVerse#12 { sarian montati in cielo a far fracasso. }
\livretVerse#12 { Si va di là dal mondo a passo a passo. }
\livretVerse#12 { Né fia vano il tentare }
\livretVerse#12 { di levarci un ostacolo cotanto }
\livretVerse#12 { com'è d'aver con Ercole a cozzare. }
\livretVerse#12 { Che poi dall'altro canto }
\livretVerse#12 { chi sa? ch'Hyllo sentendosi bagnato }
\livretVerse#12 { fatto più saggio non si sia pentito }
\livretVerse#12 { e a nuoto salvato. }
\livretPers Deianira, Iole e Licco
\livretRef#'EGJtrio
\livretVerse#12 { Una stilla di spene }
\livretVerse#12 { oh che mar di dolcezza! }
\livretVerse#12 { per un'anima avvezza }
\livretVerse#12 { a languir sempre in pene. }
\livretVerse#12 { Una stilla di spene, }
\livretVerse#12 { benché tal'or mentita }
\livretVerse#12 { nelle già fredde vene }
\livretVerse#12 { riconduce la vita: }
\livretVerse#12 { e per stupenda prova }
\livretVerse#12 { fin con l'inganno giova. }
\livretDidasPPage\justify {
  Le Damigelle di Iole rimaste a piangere presso le rovine del
  sepolcro d'Eutyro, alla vista di quattr'Ombre si spaventano, e
  formano così con le dett'Ombre la 6ª danza, per fine dell'atto
  quarto.
}
\sep
\livretAct ATTO QUINTO
\livretScene SCENA PRIMA
\livretDescAtt\center-column {
  \justify { La scena si cangia in inferno. }
  \wordwrap-center {
    Ombra d’Eutyro, coro di Anime infernali,
    Clerica, Laomedonte, Bussiride.
  }
}
\livretPers Eutyro
\livretRef#'FABaria
\livretVerse#12 { Come solo ad un grido, }
\livretVerse#12 { che giunto a pena d'Acheronte al lido }
\livretVerse#12 { formai, vi radunate anime ardite? }
\livretVerse#12 { Su, così pur contro il comun nemico }
\livretVerse#12 { vostro furore alla mia rabbia unite, }
\livretVerse#12 { che più dunque s'aspetta? }
\livretVerse#12 { Pera mora il crudel, su su vendetta. }
\livretPers Coro
\livretVerse#12 { Pera mora il crudel, su su vendetta. }
\livretPers Clerica regina di Cos
\livretRef#'FACrecit
\livretVerse#12 { Pera mora l'indegno }
\livretVerse#12 { di cui più scellerato unqua non visse, }
\livretVerse#12 { che del troiano eccidio ancor fumante }
\livretVerse#12 { non mai sazio di sangue }
\livretVerse#12 { i miei poveri figli, e me trafisse. }
\livretVerse#12 { Ah ver'un chiostro }
\livretVerse#12 { più fiero mostro }
\livretVerse#12 { di lui non ha. }
\livretVerse#12 { E se il crudel }
\livretVerse#12 { per nostro ufficio }
\livretVerse#12 { oggi cadrà }
\livretVerse#12 { mai sacrifizio }
\livretVerse#12 { più grato al ciel }
\livretVerse#12 { altri fe', né mai farà. }
\livretVerse#12 { Che più dunque si aspetta? }
\livretVerse#12 { Pera mora il crudel, su su vendetta. }
\livretPers Coro
\livretVerse#12 { Pera mora il crudel, su su vendetta. }
\livretPers Bussiride re d'Egitto
\livretRef#'FAErecit
\livretVerse#12 { Pera mora l'iniquo, }
\livretVerse#12 { che dell'etereo Giove, }
\livretVerse#12 { ingratissimo al pari, }
\livretVerse#12 { ch'in legittimo figlio, }
\livretVerse#12 { di sacerdoti, e vittime più degne, }
\livretVerse#12 { con sacrilega man spogliò l'altari. }
\livretVerse#12 { Che più dunque s'aspetta? }
\livretVerse#12 { Pera mora il crudel, su su vendetta. }
\livretPers Coro
\livretVerse#12 { Pera mora il crudel, su su vendetta. }
\livretPers Eutyro
\livretRef#'FAFrecit
\livretVerse#12 { Su, su dunque ombre terribili }
\livretVerse#12 { su voliam tutte in Eocalia, }
\livretVerse#12 { nuova in ciel schiera stimfalia }
\livretVerse#12 { contra il reo furie invisibili, }
\livretVerse#12 { e con le vipere }
\livretVerse#12 { onde Tesifone }
\livretVerse#12 { tormenta l'anime }
\livretVerse#12 { flagellamogli il cor; }
\livretVerse#12 { fin ch'immenso dolor }
\livretVerse#12 { con angoscie rabbiose il renda esanime. }
\livretPers Coro
\livretRef#'FAGcoro
\livretVerse#12 { Su, su dunque all'armi, su, su, }
\livretVerse#12 { su corriamo a vendicarci, }
\livretVerse#12 { ch'altro ben non può mai darci }
\livretVerse#12 { il destino di quaggiù. }
\livretVerse#12 { E che giova assordar quest'antri più }
\livretVerse#12 { con il vano rumor de' nostri carmi? }
\livretVerse#12 { Su, su dunque all'armi, all'armi. }
\livretPers Eutyro
\livretVerse#12 { Ah più val più diletta, }
\livretVerse#12 { che quante gioie ha il ciel una vendetta. }
\livretPers Coro
\livretVerse#12 { Ah più val più diletta, }
\livretVerse#12 { che quante gioie ha il ciel una vendetta. }

\livretScene SCENA SECONDA
\livretDescAtt\center-column {
  \justify {
    La scena si cangia in un portico del tempio di Giunone Pronuba.
  }
  \wordwrap-center {
    Ercole, Iole, Licco, Deianira, coro di Sacerdoti di Giunone Pronuba.
  }
}
\livretPers Ercole
\livretRef#'FBArecit
\livretVerse#12 { Alfine il ciel d'Amor }
\livretVerse#12 { per me si serenò, }
\livretVerse#12 { e i nembi di rigor, }
\livretVerse#12 { in gioie distemprò, }
\livretVerse#12 { sol nel mio cor pur sento }
\livretVerse#12 { un soave martir, }
\livretVerse#12 { ch'abbia per gir più lento }
\livretVerse#12 { dati il tempo i suoi vanni al mio desir. }
\livretVerse#12 { Ma pur l'amata Iole }
\livretVerse#12 { l'adorato mio sole ecco a me viene, }
\livretVerse#12 { dunque affatto il mio sen sgombrate o pene, }
\livretVerse#12 { che di sì rigid'alma }
\livretVerse#12 { qual si sia la vittoria io n'ho la palma, }
\livretVerse#12 { e l'ardente mio spirto }
\livretVerse#12 { pospon tutti i suoi lauri a un sì bel mirto. }
\livretPers Licco
\livretVerse#12 { Quando com'è tuo uffizio, }
\livretVerse#12 { dar quella veste ad Ercole dovrai }
\livretVerse#12 { per far di nozze tali il sacrifizio, }
\livretVerse#12 { quest'altra in vece, il cui valor ben sai, }
\livretVerse#12 { destramente da me prender potrai. }
\livretPers Iole
\livretVerse#12 { Così farò: ma che? per diffidenza }
\livretVerse#12 { di rimedio sì incerto, ho il sen ripieno }
\livretVerse#12 { di gelosa temenza, }
\livretVerse#12 { pur quando mi tradisca ogn'altro scampo, }
\livretVerse#12 { soccorso mi darà pronto veleno. }
\livretPers Ercole
\livretVerse#12 { Deh non muovere Iole il piè restio, }
\livretVerse#12 { ver chi dominator del mondo intero }
\livretVerse#12 { solo in goder dell'alma tua l'impero }
\livretVerse#12 { pon la felicità del suo desio. }
\livretVerse#12 { E il sacro concento }
\livretVerse#12 { sciolgasi omai, ch'a me di tali indugi }
\livretVerse#12 { grado è d'immensa pena ogni momento. }
\livretPers Coro
\livretRef#'FBBcoro
\livretVerse#12 { Pronuba, e casta dèa }
\livretVerse#12 { l'alme de' nuovi sposi }
\livretVerse#12 { con lacci avventurosi }
\livretVerse#12 { annoda, e bea. }
\livretVerse#12 { E quieta, e gioconda }
\livretVerse#12 { da' lor nestorea vita, }
\livretVerse#12 { e gl'amplessi feconda }
\livretVerse#12 { con progenie infinita. }
\livretPers Ercole
\livretRef#'FBCrecit
\livretVerse#12 { E di che temi, Iole, e di che temi? }
\livretPers Iole
\livretVerse#12 { Ecco il mio viver giunto }
\livretVerse#12 { a un formidabil punto. }
\livretPers Ercole
\livretVerse#12 { Deh su porgimi ardita }
\livretVerse#12 { la veste, ond'io ben tosto }
\livretVerse#12 { per i nostri imenei }
\livretVerse#12 { renda olocausto a i dèi. }
\livretPers Coro
\livretRef#'FBDcoro
\livretVerse#12 { Pronuba, e casta dèa }
\livretVerse#12 { l'alme de' nuovi sposi }
\livretVerse#12 { con lacci avventurosi }
\livretVerse#12 { annoda, e bea. }
\livretPers Ercole
\livretRef#'FBErecit
\livretVerse#12 { Ma qual pungente arsura }
\livretVerse#12 { la mia ruvida scorza intorno assale? }
\livretVerse#12 { Qual incognito male }
\livretVerse#12 { d'offendermi temendo }
\livretVerse#12 { serpe nascoso per le vene al core? }
\livretVerse#12 { Qual immenso dolore, ahi, mi conquide? }
\livretVerse#12 { E per dar morte a me tanto più dura }
\livretVerse#12 { in vista de' contenti, oh dio, m'uccide? }
\livretVerse#12 { E tu lo soffri, o genitore? E lasci, }
\livretVerse#12 { ch'io, che con piè temuto }
\livretVerse#12 { passeggiai della morte i regni illeso, }
\livretVerse#12 { e che fin dalla cuna }
\livretVerse#12 { di belle glorie adorni }
\livretVerse#12 { tutti contai della mia vita i giorni, }
\livretVerse#12 { or senz'avere a fronte }
\livretVerse#12 { di me degno nemico (ah rio martire, }
\livretVerse#12 { che della morte ancor vie più m'accora) }
\livretVerse#12 { in ozio vil qui mora? }
\livretVerse#12 { Senza che gloria alcuna }
\livretVerse#12 { renda almen di me degno il mio morire. }
\livretVerse#12 { Almen di nubi oscure }
\livretVerse#12 { vela quest'aria in torno }
\livretVerse#12 { sì che sorte maligna }
\livretVerse#12 { di me grato spettacolo non faccia }
\livretVerse#12 { all'implacabil mia cruda matrigna; }
\livretVerse#12 { e per quando la tua }
\livretVerse#12 { insensata pigrizia, (oh gran tonante) }
\livretVerse#12 { il conquasso destina }
\livretVerse#12 { dell'universo, ohimè, s'ora no 'l fai? }
\livretVerse#12 { E a che riserbi il cielo? }
\livretVerse#12 { Che nel perder Alcide a perder vai? }
\livretRef#'FBFaria
\livretVerse#12 { Ma l'atroce mia doglia }
\livretVerse#12 { imperversando ogn'or pochi respiri }
\livretVerse#12 { mi lascia più, deh s'il morire è forza, }
\livretVerse#12 { ardasi la mia spoglia }
\livretVerse#12 { né della terra, i di cui figli uccisi }
\livretVerse#12 { s'esponga ad un rifiuto: }
\livretVerse#12 { a dio, cielo, a dio Iole, eccomi Pluto. }
\livretPers Licco
\livretRef#'FBGrecit
\livretVerse#12 { Che dite? Il mio non fu rimedio tardo, }
\livretVerse#12 { ma un poco più (ch'io non credea) gagliardo. }
\livretVerse#12 { Pur ciascuna di voi di già rimira }
\livretVerse#12 { il penoso destin per sé finito }
\livretVerse#12 { d'un amante importun, d'un reo marito. }
\livretVerse#12 { E non piangete già, }
\livretVerse#12 { che comunque ch'avvenga a un saggio core }
\livretVerse#12 { dar non si può qui giù sorte migliore, }
\livretVerse#12 { che di vivere in pace, e libertà. }
\livretPers Deianira
\livretVerse#12 { Ah Nesso mi tradì, deh ti perdoni }
\livretVerse#12 { o Licco il ciel l'involontario errore; }
\livretVerse#12 { a dolor su dolore }
\livretVerse#12 { egualmente infinito }
\livretVerse#12 { più resister non so, mostrami o morte }
\livretVerse#12 { e del figlio la traccia, e del consorte. }
\livretVerse#12 { Ma che? l'ombra del figlio }
\livretVerse#12 { ecco ch'ad incontrarmi }
\livretVerse#12 { ver me riede pietosa. }

\livretScene SCENA TERZA
\livretDescAtt\wordwrap-center {
  Iole, Deianira, Licco, Hyllo.
}
\livretPers Iole
\livretRef#'FCArecit
\livretVerse#12 { Oh che opportun ristoro! }
\livretPers Licco
\livretVerse#12 { Oh che spavento! }
\livretPers Iole
\livretVerse#12 { \transparent { Oh che spavento! } Hyllo? }
\livretPers Deianira
\livretVerse#12 { \transparent { Oh che spavento! Hyllo? } Figlio? }
\livretPers Deianira e Iole
\livretVerse#12 { Sei tu? }
\livretPers Hyllo
\livretVerse#12 { \transparent { Sei tu? } Mercé di Giuno }
\livretVerse#12 { son io dal mar salvato }
\livretVerse#12 { acciò per gl'occhi miei }
\livretVerse#12 { versi in un mar di pianto il cor stemprato. }
\livretVerse#12 { Se qual ridirlo intendo }
\livretVerse#12 { vero è del caro padre il fato orrendo. }
\livretPers Iole
\livretVerse#12 { Pur mio ben ti consola, }
\livretVerse#12 { che se perdesti il genitor crudele }
\livretVerse#12 { me qui ritrovi, e l'amor mio fedele. }
\livretPers Deianira
\livretVerse#12 { Saranno almen le ceneri d'Alcide }
\livretVerse#12 { le più pompose de' funebri onori }
\livretVerse#12 { e più sparse di lagrime, e di fiori. }
\livretPers Licco
\livretVerse#12 { Or che sorte è la mia? }
\livretVerse#12 { Che senza averne voglia, }
\livretVerse#12 { anch'io per compagnia }
\livretVerse#12 { converrà che mi doglia. }
\livretPers Deianira, Iole, Hyllo e Licco
\livretRef#'FCBquatro
\livretVerse#12 { Dall'occaso a gl'Eoi }
\livretVerse#12 { ah non sia chi non pianga, }
\livretVerse#12 { ch'oggi il sol de gl'eroi }
\livretVerse#12 { estinto, ohimè, rimanga. }
\livretVerse#12 { Dall'occaso a gl'Eoi }
\livretVerse#12 { ah non sia chi non pianga. }

\livretScene SCENA QUARTA
\livretDescAtt\center-column {
  \justify {
    Cala Giunone nell’ultima macchina corteggiata dall’armonia de’
    cieli, ed apparisce nella più alta parte di questi Ercole sposato
    alla Bellezza.
  }
  \wordwrap-center { Giunone, Deianira, Iole, Hyllo, Licco. }
}
\livretPers Giunone
\livretRef#'FDAaria
\livretVerse#12 { Su, su allegrezza }
\livretVerse#12 { non più lamenti }
\livretVerse#12 { deh non più no, }
\livretVerse#12 { ch'ogni amarezza }
\livretVerse#12 { il ciel cangiò }
\livretVerse#12 { tutt'in contenti }
\livretVerse#12 { tutt'in dolcezza }
\livretVerse#12 { non più lamenti }
\livretVerse#12 { su, su, allegrezza. }
\livretVerse#12 { Non morì Alcide }
\livretVerse#12 { tergete i lumi }
\livretVerse#12 { non morì no, }
\livretVerse#12 { su nel ciel ride, }
\livretVerse#12 { che lo sposò }
\livretVerse#12 { il re de' numi }
\livretVerse#12 { alla Bellezza }
\livretVerse#12 { tergete i lumi }
\livretVerse#12 { su, su, allegrezza. }
\livretPers Giunone
\livretRef#'FDBrecit
\livretVerse#12 { Così deposti alfin gl'umani affetti }
\livretVerse#12 { così l'alma purgata }
\livretVerse#12 { d'ogni rea gelosia }
\livretVerse#12 { ciò che qui giù sdegnò, lassù desia. }
\livretVerse#12 { Quindi ammorzati anch'io gl'antichi sdegni }
\livretVerse#12 { per il vostro godere: }
\livretVerse#12 { a me sì glorioso }
\livretVerse#12 { consentii, ch'egli goda in su le sfere }
\livretVerse#12 { un beato riposo, }
\livretVerse#12 { onde a compire ogni desio celeste }
\livretVerse#12 { sol de' vostri imenei mancan le feste. }
\livretVerse#12 { Su dunque a i giubili }
\livretVerse#12 { anime nubili }
\livretVerse#12 { e felicissimi }
\livretVerse#12 { i miei dolcissimi }
\livretVerse#12 { nodi insolubili }
\livretVerse#12 { al par d'amor v'allaccino, }
\livretVerse#12 { e nelle vostre destre i cor allaccino. }
\livretPers Iole e Hyllo
\livretRef#'FDCtre
\livretVerse#12 { Che dolci gioie oh dèa }
\livretVerse#12 { versi nel nostro seno, }
\livretVerse#12 { il ciel benigno a pieno }
\livretVerse#12 { che più dar ne potea? }
\livretVerse#12 { Che dolci gioie oh dèa. }
\livretPers Giunone, Deianira, Iole, Hyllo e Licco
\livretRef#'FDFtutti
\livretVerse#12 { Contro due cor ch'avvampano }
\livretVerse#12 { tra loro innamorati }
\livretVerse#12 { in van nel ciel s'accampano }
\livretVerse#12 { per guerreggiar i fati. }
\livretVerse#12 { Da lega d'amore }
\livretVerse#12 { fia vinto il furore }
\livretVerse#12 { d'ogni contraria sorte: }
\livretVerse#12 { d'un reciproco amor nulla è più forte. }

\livretScene SCENA QUINTA
\livretDescAtt\wordwrap-center { Ercole, la Bellezza, coro di Pianeti. }
\livretPers Coro
\livretRef#'FEAcoro
\livretVerse#12 { Quel grand'eroe, che già }
\livretVerse#12 { laggiù tanto penò }
\livretVerse#12 { sposo della beltà }
\livretVerse#12 { per goder nozze eterne al ciel volò! }
\livretVerse#12 { Virtù, che soffre alfin mercede impetra }
\livretVerse#12 { e degno campo a' suoi trionfi è l'etra. }
\livretPers Bellezza e Ercole
\livretRef#'FEBduo
\livretVerse#12 { Così un giorno avverrà con più diletto, }
\livretVerse#12 { che della Senna in su la riva altera }
\livretVerse#12 { altro gallico Alcide arso d'affetto }
\livretVerse#12 { giunga in pace a goder bellezza ibera; }
\livretVerse#12 { ma noi dal ciel traem viver giocondo }
\livretVerse#12 { e per tal coppia sia beato il mondo. }
\livretPers Tutti
\livretRef#'FECcoro
\livretVerse#12 { Virtù che soffre alfin mercede impetra }
\livretVerse#12 { e degno campo a' suoi trionfi è l'etra. }
\livretDidasP\justify {
  Le varie influenze di sette Pianeti scendono sul palco
  successivamente a danzare, e in fine anche un coro di Stelle.
}
}
