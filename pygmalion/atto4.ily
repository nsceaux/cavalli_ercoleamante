\newBookPart#'()
\act "Atto Quarto"
\scene "Scena Prima" "Scena I"
\sceneDescription\markup\center-column {
  \wordwrap-center {
    [La scena si cangia in un mare sui liti del quale sono molte
    torri, ed in una di esse Hyllo prigioniero.]
  }
  \wordwrap-center { Hyllo. }
}
%% 4-1
\pieceToc\markup\wordwrap {
  Hyllo: \italic { Ahi che pena è gelosia }
}
\includeScore "EAAaria"
\newBookPart#'(full-rehearsal)

\scene "Scena Seconda" "Scena II"
\sceneDescription\markup\center-column {
  \wordwrap-center {
    [Apparisce nel detto mare il Paggio in una barchetta.]
  }
  \wordwrap-center { Paggio, Hyllo. }
}
%% 4-2
\pieceToc\markup\wordwrap {
  Paggio: \italic { Zefiri che gite }
}
\includeScore "EBAaria"
\newBookPart#'(full-rehearsal)
%% 4-3
\pieceToc\markup\wordwrap {
  Hyllo, Paggio: \italic { Che novella m'arrechi? è buona, o rea? }
}
\includeScore "EBBrecit"
\newBookPart#'(full-rehearsal)

\scene "Scena Terza" "Scena III"
\sceneDescription\markup\wordwrap-center { Hyllo. }
%% 4-4
\pieceToc\markup\wordwrap {
  Hyllo: \italic { E non si trova }
}
\includeScore "ECArecit"
\newBookPart#'(full-rehearsal)

\scene "Scena Quarta" "Scena IV"
\sceneDescription\markup\center-column {
  \wordwrap-center {
    [Apparisce nell'aria Giunone, in un gran trono e cala in soccorso
    d'Hyllo.]
  }
  \wordwrap-center { Giunone, Nettuno, Hyllo. }
}
%% 4-5
\pieceToc\markup\wordwrap {
  Giunone, Nettuno, Hyllo: \italic { Salva, Nettuno, ah salva }
}
\includeScore "EDArecit"
%% 4-6
\pieceToc\markup\wordwrap {
  Nettuno: \italic { Amanti che tra pene }
}
\includeScore "EDBaria"
%% 4-7
\pieceToc\markup\wordwrap {
  Giunone, Nettuno: \italic { Saggiamente a te parla, Hyllo, quel nume }
}
\includeScore "EDCrecit"
\newBookPart#'(full-rehearsal)

\scene "Scena Quinta" "Scena V"
\sceneDescription\markup\wordwrap-center {
  Giunone, Hyllo, coro di Zefiri, che danzano, e suonano.
}
%% 4-8
\pieceToc\markup\wordwrap {
  Giunone, Hyllo: \italic { Dunque del mio potere }
}
\includeScore "EEArecit"
\newBookPart#'(full-rehearsal)
%% 4-9
\pieceToc\markup\wordwrap {
  Giunone: \italic { Congedo a gl’orridi }
}
\includeScore "EEBaria"
% Ercole i Tebe de Melani 1661.
\markup { [Melani: Ercole i Tebe] } \noPageBreak
\includeScore "EEBBchoeur"
\includeScore "EEBCritornello"
\newBookPart#'(full-rehearsal)

\scene "Scena Sesta" "Scena VI"
\sceneDescription\markup\center-column {
  \wordwrap-center {
    [Si cangia la scena in un giardin di cipressi pieno
    di sepolcri reali.]
  }
  \wordwrap-center { Deianira, Licco. }
}
%% 4-10
\pieceToc "Sinf[onia]"
\includeScore "EFAAsinfonia"
%% 4-11
\pieceToc\markup\wordwrap {
  Deianira, Licco: \italic { Ed a che peggio i fati ahi mi serbaro }
}
\includeScore "EFArecit"
\newBookPart#'(full-rehearsal)

\scene "[Scena Settima]" "Scena VII"
\sceneDescription\markup\wordwrap-center {
  [Iole con la pompa funebre, coro di Sacrificanti,
  ombra d’Eutyro, Deianira, Licco, coro di Damigelle d’Iole.]
}
%% 4-12
\pieceToc "Sinf[onia]"
\includeScore "EGAsinfonia"
%% 4-13
\pieceToc\markup\wordwrap {
  Coro di sacrificanti: \italic { Gradisci o re }
}
\includeScore "EGBcoro"
\newBookPart#'(full-rehearsal)
%% 4-14
\pieceToc\markup\wordwrap {
  Iole: \italic { E se pur negli estinti }
}
\includeScore "EGCaria"
\newBookPart#'(full-rehearsal)
%% 4-15
\pieceToc\markup\wordwrap {
  Coro: \italic { Ah ch’il real sepolcro }
}
\includeScore "EGDcoro"
%% 4-16
\pieceToc\markup\wordwrap {
  Eutyro: \italic { Che sagrefici ingiusti }
}
\includeScore "EGEaria"
\newBookPart#'(full-rehearsal)
%% 4-17
\pieceToc\markup\wordwrap {
  Iole: \italic { Ben resistea l’avverso mio volere }
}
\includeScore "EGFaria"
\newBookPart#'(full-rehearsal)
%% 4-18
\pieceToc\markup\wordwrap {
  Eutyro, Deianira, Iole: \italic { Tant’ha d’Eutyro il nudo spirto ancora }
}
\includeScore "EGGrecit"
\newBookPart#'(full-rehearsal)
%% 4-19
\pieceToc\markup\wordwrap {
  Iole: \italic { Hyllo il mio bene è morto? }
}
\includeScore "EGHaria"
\newBookPart#'(full-rehearsal)
%% 4-20
\pieceToc\markup\wordwrap {
  Iole, Licco, Deianira: \italic { Attendetemi dunque, alme dilette }
}
\includeScore "EGIrecit"
\newBookPart#'(full-rehearsal)
%% 4-21
\pieceToc\markup\wordwrap {
  Iole, Deianira, Licco: \italic { Una stilla di spene }
}
\includeScore "EGJtrio"
\actEnd "Fine Dell Atto Quarto"
