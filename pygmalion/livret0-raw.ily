\livretAct PROLOGO
\livretScene SCENA UNICA
\livretDescAtt\justify {
  La scena rappresenta ne’ lati montagne di scogli su li quali si
  vedono giacenti 14 fiumi, che bagnano i regni e le provincie che
  sono o furono sotto la dominazione della corona di Francia. Nella
  prospettiva si vede il mare, e nell’aria Cinzia che discende in una
  gran macchina rappresentante il di lei cielo.
}
\livretPers Coro di Fiumi
\livretRef#'AABcoro
%# Qual concorso indovino
%# oggi al mar più vicino
%# del festoso Parigi
%# noi raunò dal gemino emisfero,
%# noi, che del franco impero
%# vantiamo il nobil giogo, o i bei vestigi?
\livretPers Cinzia
\livretRef#'AAFArecit
%# Ed ecco o Gallia invitta
%# i tuoi pregi più grandi, e immortali
%# mira del primo ciel ne' puri argenti
%# come in tempio d'onor lampe lucenti
%# l'idee delle maggior stirpi reali.
\livretPers Coro di Fiumi
\livretRef#'AADcoro
%# A i di lei veri accenti
%# su dunque attenti, attenti.
\livretPers Cinzia
\livretRef#'AAFCrecit
%# Di queste il ciel con ammirabil cura,
%# e con stupor del tempo, e di natura,
%# scettri a scettri innestando, e fregi, a fregi
%# la prosapia formò de i franchi regi;
%# che qual fiume di glorie
%# da' monti di Corone, e fasci alteri
%# trasse i fonti primieri
%# ed accresciuto ogn'or da copiosi
%# torrenti di vittorie,
%# e da' più generosi
%# rivi di sangue augusto oltre gli Achei
%# per interrotto, e limpido sentiero
%# tra margini di palme, e di trofei
%# inondò trionfante il mondo intero.
%# Alfin tra l'auree sponde
%# della Senna guerriera
%# fissò la reggia in cui benigna infonde
%# grazie a nembi ogni sfera,
%# ed or più che mai prodigo
%# di contentezze eteree
%# ad ibera beltà franco valore
%# su talamo di pace unisce Amore.
\livretPers Coro di Fiumi
\livretRef#'AAGcoro
%# Dopo belliche noie
%# oh che soavi gioie!
%# A dolcezze sì rare oltre ogni segno
%# Gallia dilata il cor, non men, ch'il regno.
\livretPers Cinzia
\livretRef#'AAHrecit
%# Ma voi che più tardate inclite Dee?
%# Uscite ad inchinare
%# Anna la gran regina,
%# che le bell'alme onde sperar si dée
%# che la serie divina
%# de' vostri alti nipoti il ciel confermi
%# ambo sono di lei rampolli, e germi.
%# Uscite a festeggiare
%# ch'in sì degna allegrezza a vostri balli
%# e veda ogn'un, che desiar non sa
%# un eroico valore
%# qui giù premio maggiore
%# che di godere in pace alma beltà.
\livretPers Coro di Fiumi
\livretRef#'AAJcoro
%# Oh Gallia fortunata
%# già per tante vittorie,
%# di pace, e d'imenei l'ultime glorie
%# ti fanno oltre ogni speme oggi beata.
%# E a fin ch'a tuoi contenti
%# gioia ogn'or s'augumenti
%# ecco, ch'in te si vede
%# alba di nuove glorie un regio erede;
%# per splender più di doppio sole ornata
%# oh Gallia fortunata.
\livretDidasP\justify {
  Le dette Idee discendono sul palco a danzare, quindi rientrate nella
  medesima macchina, questa si chiude, e le riporta in cielo.
}
\sep
