\actn "Prologo"
\sceneDescription\markup\justify {
  [La scena rappresenta ne’ lati montagne di scogli su li quali si
  vedono giacenti 14 fiumi, che bagnano i regni e le provincie che
  sono o furono sotto la dominazione della corona di Francia. Nella
  prospettiva si vede il mare, e nell’aria Cinzia che discende in una
  gran macchina rappresentante il di lei cielo.]
}
%% 0-1
\pieceToc "Sinf[onia]"
\includeScore "AAAsinfonia"
\newBookPart#'(full-rehearsal)
%% 0-2
\pieceToc\markup\wordwrap {
  Coro di Fiumi, Tevare: \italic { Qual concorso indovino }
}
\includeScore "AABcoro"
\newBookPart#'(full-rehearsal)
%% 0-3
\pieceToc\markup\wordwrap {
  Cinzia: \italic { Ed ecco o Gallia invitta }
}
\includeScore "AAFArecit"
\newBookPart#'(full-rehearsal)
%% 0-4
\pieceToc\markup\wordwrap {
  Coro di Fiumi: \italic { Ai di lei veri accenti }
}
\includeScore "AADcoro"
\newBookPart#'(full-rehearsal)
%% 0-5
\pieceToc "Sinf[onia]"
\includeScore "AAEsinfonia"
%% 0-6
\pieceToc\markup\wordwrap {
  Cinzia: \italic { Di queste il ciel con ammirabil cura, }
}
\includeScore "AAFCrecit"
\newBookPart#'(full-rehearsal)
%% 0-7
\pieceToc\markup\wordwrap {
  Coro di Fiumi: \italic { Dopo belliche noie }
}
\includeScore "AAGcoro"
\newBookPart#'(full-rehearsal)
%% 0-8
\pieceToc\markup\wordwrap {
  Cinzia, coro di Fiumi: \italic { Ma voi che più tardate inclite Dee? }
}
\includeScore "AAHrecit"
\includeScore "AAIBcoro"
%% 0-9
\pieceToc "Sinf[onia]"
\includeScore "AAICsinfonia"
%% 0-10
\pieceToc\markup\wordwrap {
  Coro di Fiumi: \italic { Oh Gallia fortunata }
}
\includeScore "AAJcoro"
\actEnd Fine del Prologo
