\newBookPart#'()
\act "Atto Primo"
\sceneDescription\markup\wordwrap-center {
  [La scena si cangia ne' lati in boscareccia, e nella prospettiva in
  un gran paese contiguo alla città d’Eocalia.]
}
\scene "Scene Prima" "Scena I"
\sceneDescription\markup\center-column { Ercole. }
%% 1-1
\pieceToc "Sinf[onia]"
\includeScore "BAAsinfonia"
\newBookPart#'(full-rehearsal)
%% 1-2
\pieceToc\markup\wordwrap {
  Ercole: \italic { Come si beffa Amor del poter mio }
}
\includeScore "BABercole"
\newBookPart#'(full-rehearsal)

\scene "Scene Seconda" "Scena II"
\sceneDescription\markup\wordwrap-center {
  [Cala dal cielo Venere con le Grazie in una macchina.
  Venere, Ercole, coro di Grazie.]
}
%% 1-3
\pieceToc\markup\wordwrap {
  Venere, coro du Gratie: \italic { Se ninfa a i pianti }
}
\includeScore "BBAAprelude"
\includeScore "BBArecit"
%% 1-4
\pieceToc\markup\wordwrap {
  Venere, Ercole: \italic { Ogn’impero ha ribelli }
}
\includeScore "BBBrecit"
\newBookPart#'(full-rehearsal)
%% 1-5
\pieceToc\markup\wordwrap {
  Venere, Ercole, coro: \italic { Fuggano a vol }
}
\includeScore "BBCduoCoro"
\newBookPart#'(full-rehearsal)
%% 1-6
\pieceToc\markup\wordwrap {
  Ercole: \italic { Infelice, e disperato }
}
\includeScore "BBDrecit"
\newBookPart#'(full-rehearsal)

\scene "Scene Terza" "Scena III"
\sceneDescription\markup\wordwrap-center {
  [Nel resto de’ nuvoli di detta macchina essendo ascosa Giunone, questa
  si discovre assisa in un gran pavone.]
}
%% 1-7
\pieceToc\markup\wordwrap {
  Giunone: \italic { E vuol dunque ciprigna }
}
\includeScore "BCArecit"
\newBookPart#'(full-rehearsal)
%% 1-8
\pieceToc\markup\wordwrap {
  Giunone: \italic { Ma in amor ciò ch’altri fura }
}
\includeScore "BCBAaria"
\actEnd\markup { Fine dell Atto Primo }
