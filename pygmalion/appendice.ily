\newBookPart#'()
\act "Annexes"

%% 4-10 Transposé en fa mineur
\pieceToc\markup\wordwrap {
  Sinf[onia] [Cavalli: \italic Ercole, IV.7]
}
\includeScore "EFAAsinfoniaTransp"

\pieceToc\markup\wordwrap {
  Damigelle à 4: \italic { Per le piagge superbe }
  [Rossi: \italic { Il Palazzo Incantato, } II.3]
}
\includeScore "palazzoIIdamigelle"
\newBookPart#'()
%%
\pieceToc\markup\wordwrap {
  Choro di Damigelle à 6: \italic { Qui pur giungesti }
  [Rossi: \italic { Il Palazzo Incantato, } II.13]
}
\markup\with-color#red {
  [VÉRIFIER LA PROSODIE QUI N’EST EXPLICITÉE QUE POUR LES VOIX 1 ET 6]
}
\includeScore "palazzoIIcoroDamigelle"
\includeScore "palazzoIIritornello"
\newBookPart#'()
%%
\pieceToc\markup\wordwrap {
  Cori: \italic { Su, su, guerrieri, all'armi! }
  [Rossi: \italic { Il Palazzo Incantato, } II.17]
}
\includeScore "palazzoIIsuSuGuerrieri"
\newBookPart#'()
%%
\pieceToc\markup\wordwrap {
  Coro di Damigelle: \italic { Via di qua vada ogni cura }
  [Rossi: \italic { Il Palazzo Incantato, } II.17]
}
\includeScore "palazzoIIcoroDamigelleB"
\newBookPart#'()
%%
\pieceToc\markup\wordwrap {
  Balletto. [Rossi: \italic { Il Palazzo Incantato, } III.6]
}
\includeScore "palazzoIIIballetto"
\newBookPart#'()
%%
\pieceToc\markup\wordwrap {
  A 3: \italic { Se con placidi sguardi }
  [Rossi: \italic { Il Palazzo Incantato, } III.7]
}
\includeScore "palazzoIIItrio"
\newBookPart#'()
%%
\pieceToc\markup\wordwrap {
  Madrigale à 10: \italic { Vincer gl’inganni }
  [Rossi: \italic { Il Palazzo Incantato, } III.8]
}
\includeScore "palazzoIIImadrigal"
\newBookPart#'()
%%
\pieceToc\markup\wordwrap {
  À 6: \italic { Vola precipite }
  [Cavalli: \italic { Le Nozze di Teti e Peleo, } I.]
}
\includeScore "nozzeIaVI"
\newBookPart#'()
%%
\pieceToc\markup\wordwrap {
  Coro: \italic { Alla caccia }
  [Cavalli: \italic { Le Nozze di Teti e Peleo, } I.]
}
\includeScore "nozzeIcaccia"
\newBookPart#'()
%%
\pieceToc\markup\wordwrap {
  À 6: \italic { Viva Bacco }
  [Cavalli: \italic { Le Nozze di Teti e Peleo, } II.]
}
\includeScore "nozzeIIaVI"
\newBookPart#'()
%%
\pieceToc\markup\wordwrap {
  Coro à 3: \italic { Già la nostra genitrice }
  [Cavalli: \italic { Le Nozze di Teti e Peleo, } III.]
}
\includeScore "nozzeIIIcoroA3"
\newBookPart#'()
%%
\pieceToc\markup\wordwrap {
  À 3: \italic { Ambrosia, è latte corri ogni fiume }
  [Cavalli: \italic { Le Nozze di Teti e Peleo, } III.]
}
\includeScore "nozzeIIItrio"
