\newBookPart#'()
\act "Atto Secondo"
\scene "Scena Prima" "Scena I"
\sceneDescription\markup\center-column {
  \wordwrap-center {
    [La scena si cangia in un gran cortile del palazzo reale.]
  }
  \wordwrap-center { Hyllo, e Iole. }
}
%% 2-1
\pieceToc "Sinf[onia]"
\includeScore "CAAsinfonia"
\newBookPart#'(full-rehearsal)
%% 2-2
\pieceToc\markup\wordwrap {
  Iole, Hyllo: \italic { Amor ardor più rari }
}
\includeScore "CABduo"
\markup "Ritor[nello] [Il Pompeo Magno]" \noPageBreak
\includeScore "CABBsinfPompeoMagno"
\newBookPart#'(full-rehearsal)
%% 2-4
\pieceTocNb "2-4" \markup\wordwrap {
  Iole, Hyllo: \italic { Pure alfine il rispetto }
}
\includeScore "CACrecit"
\newBookPart#'(full-rehearsal)

\scene "Scena Seconda" "Scena II"
\sceneDescription\markup\wordwrap-center { Paggio, Iole, e Hyllo. }
%% 2-6
\pieceTocNb "2-6" \markup\wordwrap {
  Paggio, Iole, Hyllo: \italic { Ercole a dirti invia, ch’altro non bada }
}
\includeScore "CBArecit"
%% 2-7
\pieceTocNb "2-7" \markup\wordwrap {
  Hyllo, Iole: \italic { Chi può vivere un sol istante }
}
\includeScore "CBBduo"
\newBookPart#'(full-rehearsal)

\scene "Scena Terza" "Scena III"
\sceneDescription\markup\wordwrap-center { Paggio. }
%% 2-8
\pieceTocNb "2-8" \markup\wordwrap {
  Paggio: \italic { E che cosa è quest’amore? }
}
\includeScore "CCAArecit"
\newBookPart#'(full-rehearsal)

\scene "Scena Quarta" "Scena IV"
\sceneDescription\markup\wordwrap-center { Deianira, Licco, Paggio. }
%% 2-9
\pieceTocNb "2-9" \markup\wordwrap {
  Licco, Paggio: \italic { Buon dì gentil fanciullo }
}
\includeScore "CDArecit"
\newBookPart#'(full-rehearsal)
%% 2-10
\pieceTocNb "2-10" \markup\wordwrap {
  Paggio, Deianira, Licco,: \italic { Sei tu qualche indovino? }
}
\includeScore "CDBrecit"
\newBookPart#'(full-rehearsal)

\scene "Scena Quinta" "Scena V"
\sceneDescription\markup\wordwrap-center { Deianira, Licco. }
%% 2-11
\pieceTocNb "2-11" \markup\wordwrap {
  Deianira: \italic { Misera, ohimè, ch’ascolto }
}
\includeScore "CEArecit"
\newBookPart#'(full-rehearsal)
%% 2-12
\pieceTocNb "2-12" \markup\wordwrap {
  Deianira: \italic { Ahi ch’amarezza }
}
\includeScore "CEBaria"
\newBookPart#'(full-rehearsal)
%% 2-13
\pieceTocNb "2-13" \markup\wordwrap {
  Licco, Deianira: \italic { Ah fu sempre in amor stolto consiglio }
}
\includeScore "CECrecit"
\newBookPart#'(full-rehearsal)

\scene "Scena Sesta" "Scena VI"
\sceneDescription\markup\center-column {
  \wordwrap-center {
    La scena si cangia nella grotta del Sonno.
  }
  \wordwrap-center {
    Pasithea, il Sonno, coro d'Aure e Ruscelli.
  }
}
%% 2-14
\pieceTocNb "2-14" \markup\wordwrap {
  Pasithea: \italic { Mormorate }
}
\includeScore "CFAAritornello"
\includeScore "CFAaria"
\newBookPart#'(full-rehearsal)
%% 2-15
\pieceTocNb "2-15" \markup\wordwrap {
  Aure e Ruscello: \italic { Dormi, dormi, o Sonno dormi }
}
\includeScore "CFBcoro"
\newBookPart#'(full-rehearsal)

\scene "Scena Settima" "Scene VII"
\sceneDescription\markup\center-column {
  \wordwrap-center { [Cala Giunone dal cielo.] }
  \wordwrap-center { Li sudetti e Giunone. }
}
%% 2-16
\pieceTocNb "2-16" \markup\wordwrap {
  Pasithea, Giunone: \italic { O dèa sublime dèa }
}
\includeScore "CGArecit"
%% 2-17
\pieceTocNb "2-17"  "Ritor[nello]"
\includeScore "CGBritornello"
\newBookPart#'(full-rehearsal)
%% 2-18
\pieceTocNb "2-18" \markup\wordwrap {
  Giunone: \italic { Dell’amorose pene }
}
\includeScore "CGCaria"
%% 2-19
\pieceTocNb "2-19" \markup\wordwrap {
  Tutti: \italic { Le rugiade più preziose }
}
\includeScore "CGDAtutti"
\includeScore "CGDBritornello"
\actEnd\markup\wordwrap-center {
  Qui va il Balletto \concat { 3 \super o }
  e Finisce l’Atto \concat { 2 \super o }
}
