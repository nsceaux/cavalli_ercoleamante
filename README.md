# Francesco Cavalli: L’Ercole
Matériel complet, créé pour Pygmalion.

Source : Biblioteca Nazionale Marciana, Venice (I-Vnm): Mss.It.IV.359

Version Urtext.

Pour construire le matériel :
```
$ make all
```
