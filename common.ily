\version "2.19.37"

\header {
  copyrightYear = "2018"
  composer = "Francesco Cavalli"
  poet = "Francesco Buti"
  date = 1662
}

#(ly:set-option 'original-layout (eqv? (ly:get-option 'urtext) #t))
#(ly:set-option 'use-ancient-clef (eqv? (ly:get-option 'urtext) #t))
#(ly:set-option 'show-ancient-clef (not (symbol? (ly:get-option 'part))))
#(ly:set-option 'print-footnotes (not (symbol? (ly:get-option 'part))))
#(ly:set-option 'use-rehearsal-numbers #t)
#(ly:set-option 'apply-vertical-tweaks
                (and (not (eqv? #t (ly:get-option 'urtext)))
                     (not (symbol? (ly:get-option 'part)))))
%% Staff size
#(set-global-staff-size (if (symbol? (ly:get-option 'part)) 18 16))

%% Line/page breaking algorithm
%%  optimal   for lead sheets
%%  page-turn for instruments and vocal parts
\paper {
  #(define page-breaking (if (eqv? (ly:get-option 'part) #f)
                             ly:optimal-breaking
                             ly:page-turn-breaking))
}

\language "italiano"
\include "nenuvar-lib.ily"
\setPath "ly"
\opusTitle "L'Ercole"

\header {
  maintainer = #(if (eqv? #t (ly:get-option 'pygmalion))
                    #{ \markup {
    © Edition Nicolas Sceaux - Pygmalion.
    Compositions et arrangements Miguel Henry & Raphaël Pichon.
    Tous droits réservés
  } #}
                    #{ \markup {
    Nicolas Sceaux,
    \with-url #"http://www.ensemblepygmalion.com/" \line {
      Pygmalion – Raphaël Pichon
    }
  } #})
                      
  license = "Creative Commons Attribution-ShareAlike 4.0 License"

  shortcopyright = #(if (eqv? #t (ly:get-option 'pygmalion))
                        #{ \markup { \fromproperty#'header:maintainer } #}
                        #{ \markup { \copyright — \license } #})
  longcopyright = #(if (eqv? #t (ly:get-option 'pygmalion))
                       #{ \markup\column {
    \vspace #1 \fill-line { \fromproperty#'header:maintainer }
  } #}
                       #{ \markup\column {
    \vspace #1
    \fill-line { \copyright }
    \fill-line { \license }
  }#})

  tagline = \markup\sans\abs-fontsize #8 \override #'(baseline-skip . 0) {
    \right-column\bold {
      \with-url #"http://nicolas.sceaux.free.fr" {
        \concat { Éditions \tagline-vspacer }
        \concat { Nicolas \tagline-vspacer }
        \concat { Sceaux \tagline-vspacer }
      }
    }
    \abs-fontsize #9 \with-color #(x11-color 'grey40) \raise #-0.7 \musicglyph #"clefs.petrucci.f"
   \on-the-fly #(lambda (layout props arg)
                   (if (eqv? #t (ly:get-option 'pygmalion))
                       (interpret-markup layout props #{
   \markup\column {
     \line {
       \tagline-vspacer
       Compositions et arrangements Miguel Henry & Raphaël Pichon
     }
     \line {
       \tagline-vspacer
       Edition Nicolas Sceaux - Pygmalion
     }
     \line {
       \tagline-vspacer
       Tous droits réservés
     }
   }
    #})
                       (interpret-markup layout props arg)))
    \column {
      \line { \tagline-vspacer \copyright }
      \smaller\line {
        \tagline-vspacer
        Sheet music from
        \with-url #"http://nicolas.sceaux.free.fr"
        http://nicolas.sceaux.free.fr
        typeset using \with-url #"http://lilypond.org" LilyPond
        on \concat { \today . }
      }
      \smaller\line {
        \tagline-vspacer \license
        — free to download, distribute, modify and perform.
      }
    }
  }
}

\opusPartSpecs
#`((dessus "Dessus" () (#:score-template "score-dessus"))
   (parties "Parties" () (#:score-template "score-parties"))
   (violes "Violes" ((parties #f))
           (#:score-template "score-parties-violes" #:clef "alto"))
   (basse "Basses" () (#:notes "basse" #:clef "bass")))

original =
#(define-music-function (parser location music) (ly:music?)
   (if (eqv? #t (ly:get-option 'pygmalion))
       (make-music 'Music 'void #t)
       music))

pygmalion =
#(define-music-function (parser location music) (ly:music?)
   (if (eqv? #t (ly:get-option 'pygmalion))
       music
       (make-music 'Music 'void #t)))

#(set! (clef-map 'dessus) '(petrucci-g . treble))
#(set! (clef-map 'viole0) '(petrucci-c1 . treble))
#(set! (clef-map 'viole1) '(petrucci-c3 . alto))
#(set! (clef-map 'viole2) '(petrucci-c4 . alto))

#(define-markup-command (tacet layout props) ()
   (interpret-markup layout props #{ \markup {
  \hspace#2 \pad-to-box #'(0 . 0) #'(-2 . 1) \small TACET } #}))



measure =
#(define-music-function (parser location fraction skip) (fraction? ly:music?)
   (let* ((skip-duration (ly:music-property skip 'duration))
          (extra-bars (if (eqv? (ly:music-property skip 'name) 'SkipEvent)
                          (cond (;; \measure 6/4 s2.*x avec x multiple de 2
                                 (and (equal? fraction '(6 . 4))
                                      (= (ly:duration-log skip-duration) 1)
                                      (= (ly:duration-dot-count skip-duration) 1)
                                      (= 0 (modulo (car (ly:duration-factor skip-duration)) 2)))
                                 (make-music 'UnfoldedRepeatedMusic
                                             'repeat-count (/ (car (ly:duration-factor skip-duration)) 2)
                                             'element #{ s2. \bar "'" \noBreak s2. #}))
                                 (;; \measure 6/4 s1.*x
                                  (and (equal? fraction '(6 . 4))
                                       (= (ly:duration-log skip-duration) 0)
                                       (= (ly:duration-dot-count skip-duration) 1))
                                  (make-music 'UnfoldedRepeatedMusic
                                              'repeat-count (car (ly:duration-factor skip-duration))
                                              'element #{ s2. \bar "'" \noBreak s2. #}))
                                  (;; \measure 9/4 s2.*x avec x multiple de 3
                                   (and (equal? fraction '(9 . 4))
                                      (= (ly:duration-log skip-duration) 1)
                                      (= (ly:duration-dot-count skip-duration) 1)
                                      (= 0 (modulo (car (ly:duration-factor skip-duration)) 3)))
                                   (make-music 'UnfoldedRepeatedMusic
                                              'repeat-count (/ (car (ly:duration-factor skip-duration)) 3)
                                              'element #{ s2. \bar "'" \noBreak s2. \bar "'" \noBreak s2. #}))
                                  (;; \measure 12/4 s2.*x avec x multiple de 4
                                   (and (equal? fraction '(12 . 4))
                                      (= (ly:duration-log skip-duration) 1)
                                      (= (ly:duration-dot-count skip-duration) 1)
                                      (= 0 (modulo (car (ly:duration-factor skip-duration)) 4)))
                                   (make-music 'UnfoldedRepeatedMusic
                                              'repeat-count (/ (car (ly:duration-factor skip-duration)) 4)
                                              'element #{ s2. \bar "'" \noBreak s2. \bar "'" \noBreak s2. \bar "'" \noBreak s2. #}))
                                  (;; \measure 12/4 s1.*x avec x multiple de 2
                                   (and (equal? fraction '(12 . 4))
                                      (= (ly:duration-log skip-duration) 0)
                                      (= (ly:duration-dot-count skip-duration) 1)
                                      (= 0 (modulo (car (ly:duration-factor skip-duration)) 2)))
                                   (make-music 'UnfoldedRepeatedMusic
                                              'repeat-count (/ (car (ly:duration-factor skip-duration)) 2)
                                              'element #{ s2. \bar "'" \noBreak s2. \bar "'" \noBreak s2. \bar "'" \noBreak s2. #}))
                                  (;; \measure 2/1 s1*x avec x multiple de 2
                                   (and (equal? fraction '(2 . 1))
                                      (= (ly:duration-log skip-duration) 0)
                                      (= (ly:duration-dot-count skip-duration) 0)
                                      (= 0 (modulo (car (ly:duration-factor skip-duration)) 2)))
                                   (make-music 'UnfoldedRepeatedMusic
                                              'repeat-count (/ (car (ly:duration-factor skip-duration)) 2)
                                              'element #{ s1 \bar "'" \noBreak s1 #}))
                                  (;; \measure 2/1 s\breve*x
                                   (and (equal? fraction '(2 . 1))
                                      (= (ly:duration-log skip-duration) -1)
                                      (= (ly:duration-dot-count skip-duration) 0))
                                   (make-music 'UnfoldedRepeatedMusic
                                              'repeat-count (car (ly:duration-factor skip-duration))
                                              'element #{ s1 \bar "'" \noBreak s1 #}))
                                  (;; \measure 3/1 s1*x avec x multiple de 3
                                   (and (equal? fraction '(3 . 1))
                                      (= (ly:duration-log skip-duration) 0)
                                      (= (ly:duration-dot-count skip-duration) 0)
                                      (= 0 (modulo (car (ly:duration-factor skip-duration)) 3)))
                                   (make-music 'UnfoldedRepeatedMusic
                                              'repeat-count (/ (car (ly:duration-factor skip-duration)) 3)
                                              'element #{ s1 \bar "'" \noBreak s1 \bar "'" \noBreak s1 #}))
                                  (;; \measure 3/1 s\breve.*x
                                   (and (equal? fraction '(3 . 1))
                                      (= (ly:duration-log skip-duration) -1)
                                      (= (ly:duration-dot-count skip-duration) 1))
                                   (make-music 'UnfoldedRepeatedMusic
                                              'repeat-count (car (ly:duration-factor skip-duration))
                                              'element #{ s1 \bar "'" \noBreak s1 \bar "'" \noBreak s1 #}))
                                  (;; \measure 4/1 s1*x avec x multiple de 4
                                   (and (equal? fraction '(4 . 1))
                                      (= (ly:duration-log skip-duration) 0)
                                      (= (ly:duration-dot-count skip-duration) 0)
                                      (= 0 (modulo (car (ly:duration-factor skip-duration)) 4)))
                                   (make-music 'UnfoldedRepeatedMusic
                                              'repeat-count (/ (car (ly:duration-factor skip-duration)) 4)
                                              'element #{ s1 \bar "'" \noBreak s1 \bar "'" \noBreak s1 \bar "'" \noBreak s1 #}))
                                  (;; \measure 4/1 s\breve*x avec x multiple de 2
                                   (and (equal? fraction '(4 . 1))
                                      (= (ly:duration-log skip-duration) -1)
                                      (= (ly:duration-dot-count skip-duration) 0)
                                      (= 0 (modulo (car (ly:duration-factor skip-duration)) 2)))
                                   (make-music 'UnfoldedRepeatedMusic
                                              'repeat-count (/ (car (ly:duration-factor skip-duration)) 2)
                                              'element #{ s1 \bar "'" \noBreak s1 \bar "'" \noBreak s1 \bar "'" \noBreak s1 #}))
                                  (;; \measure 4/1 s\longa*x
                                   (and (equal? fraction '(4 . 1))
                                      (= (ly:duration-log skip-duration) -2)
                                      (= (ly:duration-dot-count skip-duration) 0))
                                   (make-music 'UnfoldedRepeatedMusic
                                              'repeat-count (car (ly:duration-factor skip-duration))
                                              'element #{ s1 \bar "'" \noBreak s1 \bar "'" \noBreak s1 \bar "'" \noBreak s1 #}))
                                  (else
                                   (make-music 'Music 'void #t)))
                                 (make-music 'Music 'void #t))))
     #{<< \set Score.measureLength = $(ly:make-moment (/ (car fraction) (cdr fraction)))
           $skip $extra-bars >> #}))
