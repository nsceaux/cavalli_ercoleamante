\clef "dessus" R2.*9 |
la''4 la'' la''8 re'' |
sol''4 mi'' la''8 fad'' |
si''4. si''8 si''4 fad'' la'' mi'' |
la''2 re''4 sol''2 sol''4 |
re'' re'' re'' mi''2 fad''4 |
sol''4. \ficta fad''8 mi''4 fad''! fad'' si'' |
mi''8 mi'' fad'' sold'' la'' \ficta sol'' fad''4. \ficta sold''16[ la''] si''8 fad'' |
sold''4 mi'' la'' \ficta fad'' si''2 la''2. |
r4 la'' la'' la''8 la'' re''2 re'' |
r4 re'' re'' re''8 re'' |
re''2 re'' |
sol'' sol''4 dod'' |
fad''2 si'' fad''1 fad'' |
r4 \ficta fad''8 fad'' si''4 si''8 si'' |
si''2. | \allowPageTurn
R2.*8 |
r4 re'' re'' la''4. la''8 la''4 fad'' la'' la'' |
sol''4. sol''8 sol''4 |
sol''2. do''' |
la''4 re'' re'' sol''4. sol''8 sol''4 |
mi''2. r2*3/2 |
r4 la'' la'' sol''4. sol''8 sol''4 |
sol'' sol'' sol'' la''4. la''8 mi''4 |
re''2 fad'' re'' |
r2*3/2 r4 mi'' mi'' |
la''4. la''8 la''4 fad'' fad'' fad'' |
si''4. si''8 si''4 la''2. la''1*3/4\fermata |
