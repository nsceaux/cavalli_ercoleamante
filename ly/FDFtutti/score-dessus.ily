\score {
  <<
    \new StaffGroup <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
    >>
    \new ChoirStaff <<
      \original <<
        \new Staff \with {
          instrumentName = \markup\character Giunone
          shortInstrumentName = \markup\character Giu
        } \withLyrics <<
          \global \keepWithTag #'giunone \includeNotes "voix"
        >> \keepWithTag #'giunone \includeLyrics "paroles"
        \new Staff \with {
          instrumentName = \markup\character Iole
          shortInstrumentName = \markup\character Io
        } \withLyrics <<
          \global \keepWithTag #'iole \includeNotes "voix"
        >> \keepWithTag #'iole \includeLyrics "paroles"
      >>
      \pygmalion <<
        \new Staff \with {
          instrumentName = \markup\character Iole
          shortInstrumentName = \markup\character Io
        } \withLyrics <<
          \global \keepWithTag #'giunone \includeNotes "voix"
        >> \keepWithTag #'giunone \includeLyrics "paroles"
        \new Staff \with {
          instrumentName = \markup\character Giunone
          shortInstrumentName = \markup\character Giu
        } \withLyrics <<
          \global \keepWithTag #'iole \includeNotes "voix"
        >> \keepWithTag #'iole \includeLyrics "paroles"
      >>
      \new Staff \with {
        instrumentName = \markup\character Deianira
        shortInstrumentName = \markup\character De
      } \withLyrics <<
        \global \keepWithTag #'deianira \includeNotes "voix"
      >> \keepWithTag #'deianira \includeLyrics "paroles"
    >>
  >>
  \layout {
    indent = \largeindent
    short-indent = 10\mm
  }
}
