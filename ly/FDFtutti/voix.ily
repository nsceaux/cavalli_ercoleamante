<<
  \tag #'(giunone basse) {
    <<
      \tag #'basse { s2.*6 \ffclef "vsoprano" }
      \tag #'giunone {
        \clef "vsoprano" R2.*5 |
        r2*3/2
      }
    >> re''4 re'' re'' |
    mi''2 fad''4 sol''4. fad''8 mi''4 |
    fad'' re'' re'' |
    dod''2 re''4 |
    sol'8[\melisma \ficta fad' sol' la'] si'[ dod''] re''4\melismaEnd re'' mi'' |
    mi''2 fad''4 re''2 si'4 |
    fad''2 re''4 sol''4. sol''8 fad''4 |
    mi''8[ fad''] mi''[ re''] dod''[ si'] la'8[\melisma la' si' dod''] re''4\melismaEnd |
    r4 r dod'' fad''8[ mi''] fad''[ mi'' re'' dod''!] |
    si'[\melisma la' si' dod''] re''[ dod'']\melismaEnd re''[ mi''] si'2 dod''!2. |
    r4 mi'' mi'' mi''8 mi'' fad''2 fad'' |
    R1 |
    r4 re'' re'' re''8 re'' |
    mi''2 mi'' |
    dod''4 dod''8 re'' mi''4 si' re''2( dod''!) si'1 |
    R1 |
    r4 si' si' |
    mi''4. mi''8 mi''4 dod'' dod'' dod'' |
    fad''4. fad''8 fad''4 re''2. |
    sol'' mi'' |
    fad'' re'' |
    mi''4 re'' re'' re''2 dod''4( re''2.) |
    R2. |
    mi''2. do'' |
    re'' sol' |
    r4 si' si' mi''4. mi''8 mi''4 |
    re''2. re''4 si' si' |
    mi''4. mi''8 mi''4 dod'' dod'' dod'' |
    fad''4. fad''8 fad''4 re''2. |
    sol'' mi'' |
    fad'' re'' |
    sol''4. sol''8 fad''4 mi''2. re''1*3/4\fermata |
  }
  \tag #'iole {
    \clef "vsoprano" R2.*7 |
    la'4 la' la' si'2 dod''4 |
    re''4. dod''8 si'4 |
    mi''4 mi'' re'' |
    mi''2 sol''4 fad''8[\melisma mi'' re'' dod''] si'4 |
    dod''\melismaEnd dod'' re'' si'2 mi''4 |
    la'2 fad'4 si'( dod'') re'' |
    si'4. si'8 mi''4 re''8[ dod''] re''[ mi''] fad''4 |
    si'8[\melisma si' dod'' re''] mi''4 la' re''8[ dod''!] si'[ la'] |
    sold'2\melismaEnd la' la'4. sold'!8( la'2.) |
    r4 dod'' dod'' dod''8 dod'' re''2 re'' |
    r4 la' la' la'8 la' |
    si'2 si' |
    dod'' dod''4 dod'' |
    lad'2 si' si'2. lad'!4( si'1) |
    r4 re''8 re'' re''4 re''8 re'' |
    si'2. |
    r2*3/2 r4 la' la' |
    re''4. re''8 re''4 si' si' si' |
    re''4. re''8 re''4 do''2. |
    do'' si' |
    r2*3/2 r4 la' la' re''4. re''8 re''4 |
    si'4 si' si' |
    sol'4. sol'8 do''[ re''] mi''2. |
    r2*3/2 re''2. |
    si'4 sol' sol' do''4. do''8 si'4 |
    la'2. si' |
    R2.*2 |
    r2*3/2 re''2. |
    si' mi'' |
    do'' r4 re'' re'' |
    si'4. dod''8 re''4 re''2 dod''!4( re''1*3/4)\fermata |
  }
  \tag #'deianira {
    \clef "vsoprano" R2.*9 |
    fad'4 fad' fad' |
    sol'2 la'4 |
    si'4. la'8 sol'4 la' la' si' |
    la'2 la'4 sol' sol' sol' |
    re''2 la'4 mi''2 la'4 |
    sol'4. re''8 la'4 fad''8[ mi''] re''[ dod''] si'[ la'] |
    sold'8[\melisma sold' la' si'] la'4\melismaEnd re'' si' fad' |
    si'8[\melisma do''16 re''] mi''4\melismaEnd fad''2 mi'' mi''2. |
    r4 la' la' la'8 la' la'2 la' |
    R1 |
    r4 sol' sol' sol'8 sol' |
    sol'2 sol' |
    fad'4 fad'8 fad' sol'4. sol'8 re'[\melisma mi' re' mi'] fad'!2\melismaEnd fad'1 |
    r4 si'8 si' si'4 si'8 si' |
    re''2. |
    R2.*2 |
    r2*3/2 re''2. |
    si' sol' |
    la' r4 re'' re'' |
    si'4. si'8 si'4 la'2. la'4 re' re' |
    sol'4. sol'8 sol'4 |
    mi'4 mi' mi' la'4. la'8 la'4 |
    fad'2. si' |
    sol' la'4 sol' sol' |
    sol'2 fad'4( sol') sol' sol' |
    si'4. si'8 si'4 la' la' la' |
    re''4. re''8 re''4 si'2. |
    re'' do'' |
    r4 do'' do'' si'2 si'4 |
    re''4. sol'8 si'4 mi'( la'2) la'1*3/4\fermata |
  }
  \tag #'(licco basse) {
    \clef "valto" re'4 re' re' |
    mi'2 fad'4 sol'4. fad'8 mi'4 |
    fad'4 fad' sol' la'2 fad'4 |
    <<
      \tag #'basse {
        sol'2.
      }
      \tag #'licco {
        sol'2.\melisma
        fad'4. mi'8 re'4 |
        dod'4. si8 la4 mi'2\melismaEnd mi'4 |
        la'4 la' si' |
        mi'8[\melisma fad'] sol'4\melismaEnd fad' |
        mi'4. mi'8 mi'4 re' re' sol' |
        mi'2 re'4 re'2( mi'4) |
        fad'2 la'4 sol'2 re'4 |
        mi' si( dod') re'4. re'8 re'4 |
        mi'4 mi'8[ re'] dod'4 re'8[\melisma dod' re' mi' fad' sold'16 la'] |
        mi'2\melismaEnd la mi' mi'2. |
        r4 mi' mi' mi'8 mi' re'2 re' |
        R1 |
        r4 re' re' si8 si |
        mi'2 mi'4 dod'~ |
        dod'8 dod' fad'4 si4. si8 fad'!2( dod'!) red'1 |
        r4 fad'8 fad' fad'4 fad'8 fad' |
        sol'2. |
        sol' mi' |
        la' fad' |
        sol'4. sol'8 re'4 mi'2 mi'4 |
        r2*3/2 fad'2 fad'4 |
        sol'4. sol'8 fad'4 mi'2. fad'! |
        r4 sol sol |
        do'4. do'8 do'4 la la la |
        re'4. re'8 re'4 si2. |
        mi'2 mi'4 do'4. do'8 do'4 |
        re'2. sol4 re' re' |
        sol'4. sol'8 sol'4 mi' mi' mi' |
        la'4. la'8 la'4 fad'2. |
        r2*3/2 sol'2. |
        la' r4 fad' fad' |
        sol' re' re' la'( mi'2) fad'1*3/4\fermata |
      }
    >>
  }
  \tag #'hyllo {
    \clef "vtenor" R2. |
    la4 la la si2 dod'4 |
    re'4. do'8 si4 dod' dod' re' |
    mi'2 dod'4 re'2. |
    la r2*3/2 |
    re4 re re |
    mi2 fad4 |
    sol4. fad8 mi4 fad! fad sol |
    la2 fad4 sol2. |
    fad2 fad4 mi2 re4 |
    sol2 la4 fad4. mi8 re4 |
    r2*3/2 re4 re re |
    mi2 re mi la2. |
    r4 la la la8 la fad2 fad |
    r4 fad fad fad8 fad |
    sol2 sol |
    mi2 mi4 mi |
    fad2 mi fad1 si |
    r4 si8 si si4 si8 si |
    sol2. |
    r4 mi mi la4. la8 la4 |
    fad fad fad si4. si8 si4 |
    sol2. do' |
    la si2 si4 |
    sol4. sol8 sol4 la2. re |
    R2.*7 |
    r4 re re sol4. sol8 sol4 |
    mi mi mi la4. la8 la4 |
    fad2. si |
    sol4 sol sol do'4. do'8 do'4 |
    la2. r4 si si |
    sol4. sol8 sol4 la2. re1*3/4\fermata |
  }
>>
