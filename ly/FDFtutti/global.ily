\key do \major \midiTempo#160
\customTime\markup\vcenter { \musicglyph#"timesig.C44" \musicglyph#"three" }
\measure 3/4 s2.
\measure 6/4 s1.*4
\measure 3/4 s2.*2
\measure 6/4 s1.*5
\measure 9/4 s2.*3
\time 4/4 \measure 2/1 s1*2
\measure 4/4 s1*3
\measure 3/1 s1*3
\measure 4/4 s1
\digitTime\time 3/4 s2.
\measure 6/4 s1.*4
\measure 9/4 s2.*3
\measure 3/4 s2.
\measure 6/4 s1.*8
\measure 9/4 s2.*3 \bar "|."
