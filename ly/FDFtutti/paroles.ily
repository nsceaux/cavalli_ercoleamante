\tag #'basse {
  Con -- tro due cor ch’av -- vam -- pa -- no
  tra lo -- ro in -- na -- mo -- ra-
}
\tag #'(giunone basse) {
  Con -- tro due cor ch’av -- vam -- pa -- no
  tra lo -- ro in -- na -- mo -- ra -- ti
  in van in van nel ciel s’ac -- cam -- pa -- no
  per guer -- reg -- giar __
  per guer -- reg -- giar __ i fa -- ti.
  Da le -- ga d’a -- mo -- re
  fia vin -- to il fu -- ro -- re
  d’o -- gni con -- tra -- ria sor -- te:
  d’un re -- ci -- pro -- co A -- mor
  d’un re -- ci -- pro -- co A -- mor
  nul -- la nul -- la nul -- la è più for -- te __
  nul -- la nul -- la nul -- la nul -- la è più for -- te
  d’un re -- ci -- pro -- co A -- mor
  d’un re -- ci -- pro -- co A -- mor
  nul -- la nul -- la nul -- la è più for -- te.
}
\tag #'iole {
  Con -- tro due cor ch’av -- vam -- pa -- no
  tra lo -- ro in -- na -- mo -- ra -- ti
  in van in van nel ciel s’ac -- cam -- pa -- no
  per guer -- reg -- giar __ i fa -- ti. __
  Da le -- ga d’a -- mo -- re
  fia vin -- to il fu -- ro -- re
  d’o -- gni con -- tra -- ria sor -- te: __
  d’un re -- ci -- pro -- co A -- mor
  d’un re -- ci -- pro -- co A -- mor
  d’un re -- ci -- pro -- co A -- mor
  nul -- la
  d’un re -- ci -- pro -- co A -- mor
  d’un re -- ci -- pro -- co A -- mor
  nul -- la nul -- la nul -- la è più for -- te
  nul -- la nul -- la nul -- la nul -- la è più for -- te. __
}
\tag #'deianira {
  Con -- tro due cor ch’av -- vam -- pa -- no
  tra lo -- ro in -- na -- mo -- ra -- ti
  in van nel ciel s’ac -- cam -- pa -- no
  per guer -- reg -- giar __
  per guer -- reg -- giar __ i fa -- ti.
  Da le -- ga d’a -- mo -- re
  fia vin -- to il fu -- ro -- re
  d’o -- gni con -- tra -- ria sor -- te:
  d’un re -- ci -- pro -- co A -- mor
  nul -- la nul -- la nul -- la nul -- la è più for -- te
  d’un re -- ci -- pro -- co A -- mor
  d’un re -- ci -- pro -- co A -- mor
  nul -- la nul -- la è più for -- te
  d’un re -- ci -- pro -- co A -- mor
  d’un re -- ci -- pro -- co A -- mor
  nul -- la nul -- la nul -- la nul -- la è più for -- te.
}
\tag #'licco {
  Con -- tro due cor ch’av -- vam -- pa -- no
  tra lo -- ro in -- na -- mo -- ra -- ti
  con -- tro due cor __ ch’av -- vam -- pa -- no
  tra lo -- ro in -- na -- mo -- ra -- ti
  in van nel ciel s’ac -- cam -- pa -- no
  per guer -- reg -- giar __ i fa -- ti.
  Da le -- ga d’a -- mo -- re
  fia vin -- to il fu -- ro -- re
  d’o -- gni con -- tra -- ria sor -- te:
  d’un re -- ci -- pro -- co A -- mor
  nul -- la nul -- la nul -- la è più for -- te
  nul -- la nul -- la è più for -- te
  d’un re -- ci -- pro -- co A -- mor
  d’un re -- ci -- pro -- co A -- mor
  nul -- la nul -- la è più for -- te
  d’un re -- ci -- pro -- co A -- mor
  d’un re -- ci -- pro -- co A -- mor
  nul -- la nul -- la nul -- la è più for -- te.
}
\tag #'hyllo {
  Con -- tro due cor ch’av -- vam -- pa -- no
  tra lo -- ro in -- na -- mo -- ra -- ti
  con -- tro due cor ch’av -- vam -- pa -- no
  tra lo -- ro in -- na -- mo -- ra -- ti
  in van nel ciel s’ac -- cam -- pa -- no
  per guer -- reg -- giar i fa -- ti.
  Da le -- ga d’a -- mo -- re
  fia vin -- to il fu -- ro -- re
  d’o -- gni con -- tra -- ria sor -- te:
  d’un re -- ci -- pro -- co A -- mor
  d’un re -- ci -- pro -- co A -- mor
  d’un re -- ci -- pro -- co A -- mor
  nul -- la nul -- la nul -- la è più for -- te
  d’un re -- ci -- pro -- co A -- mor
  d’un re -- ci -- pro -- co A -- mor
  nul -- la
  d’un re -- ci -- pro -- co A -- mor
  nul -- la nul -- la è più for -- te.
}
