\clef "dessus" R2.*9 |
re''4 fad'' fad'' |
mi'' dod'' fad''8 la'' |
sol''4. sol''8 sol''4 re'' fad'' r |
la' la' la' si'2 si'8 dod'' |
re''4. mi''8 fad''4 sol'' sol''8 mi'' la''4 |
mi''2 la''4 re''2 re''4 |
r4 r mi'' re''8 mi'' re'' la' re'' la' |
mi''4 si' fad'' la'' mi''2 mi''2. |
r4 mi'' mi'' mi''8 mi'' la''2 la'' |
r4 la'' la'' la''8 la'' |
sol''2 sol'' |
r4 dod'' mi'' mi'' |
lad' dod'' sol''4. mi''8 re''4. fad''8 dod''!4. lad'!8 red''1 |
r4 re''!8 re'' fad''4 fad''8 re'' |
sol''2. |
R2.*8 |
r2*3/2 r4 mi'' mi'' re''4. re''8 fad''4 |
re''4 re'' re'' |
do''4. do''8 mi''4 mi''2. |
fad'' re'' |
r2*3/2 r4 sol' sol' |
re''4. re''8 re''4 si' si' re'' |
mi''4. mi''8 mi''4 mi'' dod'''2 |
la''2. r4 si''2 |
si''4 si'' si'' sol''4. sol''8 sol''4 |
do'''2. r2*3/2 |
re''4. re''8 re''4 re'' dod''8[ re'' mi'' la''] fad''1*3/4\fermata |
