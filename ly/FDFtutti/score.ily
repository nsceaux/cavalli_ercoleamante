\score {
  <<
    \new StaffGroup <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
      \original <<
        \new Staff << \global \includeNotes "partie1" >>
        \new Staff << \global \includeNotes "partie2" >>
      >>
    >>
    \new ChoirStaff <<
      \original <<
        \new Staff \with {
          instrumentName = \markup\character Giunone
          shortInstrumentName = \markup\character Giu
        } \withLyrics <<
          \global \keepWithTag #'giunone \includeNotes "voix"
        >> \keepWithTag #'giunone \includeLyrics "paroles"
        \new Staff \with {
          instrumentName = \markup\character Iole
          shortInstrumentName = \markup\character Io
        } \withLyrics <<
          \global \keepWithTag #'iole \includeNotes "voix"
        >> \keepWithTag #'iole \includeLyrics "paroles"
      >>
      \pygmalion <<
        \new Staff \with {
          instrumentName = \markup\character Iole
          shortInstrumentName = \markup\character Io
        } \withLyrics <<
          \global \keepWithTag #'giunone \includeNotes "voix"
        >> \keepWithTag #'giunone \includeLyrics "paroles"
        \new Staff \with {
          instrumentName = \markup\character Giunone
          shortInstrumentName = \markup\character Giu
        } \withLyrics <<
          \global \keepWithTag #'iole \includeNotes "voix"
        >> \keepWithTag #'iole \includeLyrics "paroles"
      >>
      \new Staff \with {
        instrumentName = \markup\character Deianira
        shortInstrumentName = \markup\character De
      } \withLyrics <<
        \global \keepWithTag #'deianira \includeNotes "voix"
      >> \keepWithTag #'deianira \includeLyrics "paroles"
      \new Staff \with {
        instrumentName = \markup\character Licco
        shortInstrumentName = \markup\character Li
      } \withLyrics <<
        \global \keepWithTag #'licco \includeNotes "voix"
      >> \keepWithTag #'licco \includeLyrics "paroles"
      \new Staff \with {
        instrumentName = \markup\character Hyllo
        shortInstrumentName = \markup\character Hy
      } \withLyrics <<
        \global \keepWithTag #'hyllo \includeNotes "voix"
      >> \keepWithTag #'hyllo \includeLyrics "paroles"
    >>
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2.*10\pageBreak
        s2.*9\pageBreak
        s2.*5 s1*3\pageBreak
        s1*6 s2.\pageBreak
        s2.*11\pageBreak
        s2.*11\pageBreak
      }
    >>
  >>
  \layout {
    indent = \largeindent
    short-indent = 10\mm
  }
  \midi { }
}
