\clef "basse" re2 re4 |
la la la si2 dod'4 |
re'4. do'8 si4 dod'2 re'4 |
mi'2 dod'4 re'2. |
la2 la4 mi2. |
re4 re re |
mi2 fad4 |
sol4. fad8 mi4 fad!2 sol4 |
la2 fad4 sol2. |
fad2 fad4 mi2 re4 |
sol2 la4 fad4. mi8 re4 |
mi2 dod4 re re re |
mi2 re mi la,2. |
la2. la4 fad1 |
fad |
sol |
mi2. mi4 |
fad2 mi fad1 si, |
si,2. si,4 |
sol,2. |
mi,4 mi mi la2 la4 |
fad2 fad4 si2 si4 |
sol2. do' |
la si2 si4 |
sol2 sol4 la2. re |
sol,4 sol, sol, |
do2 do4 la, la, la, |
re2 re4 si,2. |
mi2 mi4 do2 do4 |
re re re sol2 sol4 |
mi mi mi la2 la4 |
fad2. si |
sol4 sol sol do'4. do'8 do'4 |
la2. si4 si si |
sol2 sol,4 la,2. re1*3/4\fermata |
