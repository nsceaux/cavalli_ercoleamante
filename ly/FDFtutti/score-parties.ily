\score {
  \new ChoirStaff <<
    \new Staff \with {
      instrumentName = \markup\character Licco
      shortInstrumentName = \markup\character Li
    } \withLyrics <<
      \global \keepWithTag #'licco \includeNotes "voix" \clef "alto"
    >> \keepWithTag #'licco \includeLyrics "paroles"
    \new Staff \with {
      instrumentName = \markup\character Hyllo
      shortInstrumentName = \markup\character Hy
    } \withLyrics <<
      \global \keepWithTag #'hyllo \includeNotes "voix" \clef "alto"
    >> \keepWithTag #'hyllo \includeLyrics "paroles"
  >>
  \layout {
    indent = \largeindent
    short-indent = 10\mm
  }
}
