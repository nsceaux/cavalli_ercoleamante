\clef "viole2" re' |
re'2 re'4 |
re'2. |
r4 r mi' |
re'2 la4 |
mi'2 mi4 |
\ficta lad2 fad fad |
fad fad4 re sol2 |
sol fad la |
fad\breve*3/4 |
