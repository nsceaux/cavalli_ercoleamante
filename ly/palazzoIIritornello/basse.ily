\clef "basse" re4 |
re2 sol,4 |
r re mi |
fad4. sol8 la4 |
re2 re4 |
mi2. |
fad1 fad2 |
si,2 re4 sol,2. |
la,1 la,2 |
re\breve*3/4 |
