\clef "viole2" la4 |
la re'2 |
re2. |
la2. |
la2 fad4 |
si dod'2~ |
dod'4 fad fad'2 dod' |
re'2 re'4 re'2 si4 |
mi2 la la |
la\breve*3/4 |
