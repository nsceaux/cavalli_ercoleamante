\clef "dessus" la''4 |
la''2 si''4 |
la''2 sol''4 |
fad''2 mi''4 |
fad''2 la''4 |
sol''2. |
fad''2~ fad''1 |
\ficta fad''2 re''4 re''2 mi''4 |
mi''4. re''16 mi'' \ficta fad''4. sol''16 fad'' mi''4. re''16 mi'' |
\ficta fad''\breve*3/4 |
