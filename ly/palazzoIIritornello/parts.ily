\piecePartSpecs
#`((dessus #:indent 0)
   (violes #:score "score-violes")
   (basse #:indent 0)
   (silence #:on-the-fly-markup , #{ \markup\tacet #}))
