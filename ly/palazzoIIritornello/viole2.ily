\clef "viole1" fad'4 |
fad'2 sol'4 |
sol'2 mi'4 |
re' la'2 |
fad' fad'4 |
sol' mi'2 |
mi' re' fad' |
fad' fad'4 sol'2 mi'4 |
la'1 la'2 |
la'\breve*3/4 |
