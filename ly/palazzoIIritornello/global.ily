\key do \major \midiTempo#160
\beginMark "Ritornello"
\omit Staff.TimeSignature
\digitTime\time 3/4 \partial 4 s4 s2.*5
\measure 6/4 s1.*4 \bar "|."
