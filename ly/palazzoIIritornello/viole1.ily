\clef "petrucci-c1/treble" la' |
la'2 re''4 |
la'2. |
la' |
la'2 re''4~ |
re'' dod'' si' |
lad'4. \ficta sold'16 lad' si'4. dod''16 si' lad'4. sold'16 lad' |
si'2 la'4 si'2 si'4 |
dod''4. si'16 dod'' re''4. mi''16 re'' dod''4. si'16 dod'' |
re''\breve*3/4 |
