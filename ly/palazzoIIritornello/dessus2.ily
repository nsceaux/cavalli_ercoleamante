\clef "dessus" re''4 |
re''2 re''4 |
re''2. |
re''2 dod''4 |
re''2 fad''4 |
mi''2 re''4 |
dod''4. si'16 dod'' re''4. mi''16 re'' dod''4. re''16 dod'' |
si'2 la''4 sol''2 si''4 |
la''1 la''2 |
la''\breve*3/4 |
