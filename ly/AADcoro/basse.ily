\clef "basse" << \original la1 \pygmalion do' >> |
fa |
fa2. |
re2 sol4 |
do2 do4 |
la,2 re4 |
sol,2 la,4 si,2 do4 |
re2. sol,2 sol4 |
mi2 la4 re2 re4 |
si,2 mi4 la,2 si,4 |
do2 re4 mi2. |
la,2 sol,4 do'2 sol4 |
do'2 si4 |
do'2 sol4 do'2 fa4 |
re2 sol4 do2 re4 |
mi2 fa4 sol mi fa |
sol2. do2_\markup\italic Tou douxemans re4 |
mi2 fa4 sol mi fa |
sol2. do1\fermata |
