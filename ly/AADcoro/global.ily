\original\set Score.currentBarNumber = 28 \bar ""
\time 4/4 \midiTempo#80 s1*2
\digitTime\time 3/4 \midiTempo#160 s2.*4
\measure 6/4 s1.*6
\measure 3/4 s2.
\measure 6/4 s1.*5
\measure 7/4 s4*7 \bar "|."
