<<
  \tag #'(coro11 basse) {
    <>^\markup\character Coro di Fiumi
    \clef "vsoprano" r4 mi''8 mi'' mi''4 mi''8 mi'' |
    fa''1 |
    fa''2 do''4 |
    re'' re'' re'' |
    mi'' mi'' r |
    R2. |
    r4 r mi'' si'4. re''8 mi''4 |
    re''2. re''2 si'4 |
    si' si' dod'' re'' re'' r |
    r2*3/2 r4 r sol'' |
    mi''4. do''8 fa''4 mi''4.\melisma re''16[ do''] si'4\melismaEnd |
    dod''2 re''4 \ficta do''4. do''8 re''4 |
    do''4 do'' re'' |
    do''4. do''8 re''4 do'' do'' do'' |
    fa'' fa'' re'' mi'' do'' fa'' |
    mi''4. sol''8 fa''4 re'' mi'' r8 do'' |
    sol''8[ fa''16 mi'']( re''2) mi''2 fa''4 |
    mi''4. sol''8 fa''4 re'' mi'' r8 do'' |
    sol''[\melisma fa''16 mi''] re''2\melismaEnd mi''1\fermata |
  }
  \tag #'coro12 {
    \clef "valto" r4 <<
      \original { la'8 la' la'4 la'8 la' | }
      \pygmalion { sol'8 sol' sol'4 sol'8 sol' | }
    >>
    la'1 |
    la'2 fa'4 |
    fa' fa' sol' |
    sol' sol' r |
    R2. |
    r4 r la' sol'4. sol'8 sol'4 |
    sol'2 fad'4( sol'2) sol'4 |
    sol' sol' la' la' la' r |
    r2*3/2 r4 r si' |
    la'4. la'8 la'4 la'2 sold'4( |
    la'2) si'4 sol'4. sol'8 si'4 |
    sol'4 sol' si' |
    sol'4. sol'8 si'4 sol' sol' la' |
    la' la' sol' sol' sol' re' |
    sol'4. mi'8 la'4 sol' sol' la' |
    sol'2. sol'2 re'4 |
    sol'4. mi'8 la'4 sol' sol' la' |
    sol'2. sol'1\fermata |
  }
  \tag #'coro13 {
    \clef "vtenor" r4 do'8 mi' do'4 do'8 do' |
    do'1 |
    do'2 la4 |
    la la si |
    do' do' r |
    R2. |
    r4 r do' sol re' do'8[ sol] |
    re'2. si2 re'4 |
    mi' mi' mi' fad' fad' r |
    r2*3/2 r4 r sol |
    do' la re' r8 si si4( mi') |
    mi'2 sol'4 mi'4. mi'8 sol'4 |
    mi'4 mi' sol' |
    mi'4. mi'8 sol'4 mi' mi' r8 do' |
    re'4 re' re' do' do' la |
    mi' do' re'8[ la] si4 sol do'8[ la] |
    re'2. do'2 la4 |
    mi'4 do' re'8[ la] si4 sol do'8[ la] |
    re'2. do'1\fermata |
  }
  \tag #'coro14 {
    \clef "vbasse" r4 <<
      \original { la8 la la4 la8 la | }
      \pygmalion { do'8 do' do'4 do'8 do' | }
    >>
    fa1 |
    fa2 fa4 |
    re re sol |
    do do r |
    R2. |
    r4 r la, si,4. si,8 do4 |
    re2. sol,2 sol4 |
    mi mi la re re r |
    r2*3/2 r4 r si, |
    do4. do8 re4 mi2. |
    la2 sol4 do'4. do'8 si4 |
    do'4 do' sol |
    do'4. do'8 si4 do' do' fa |
    re re sol do do re |
    mi4. mi8 fa4 sol mi fa |
    sol2. do2 re4 |
    mi4. mi8 fa4 sol mi fa |
    sol2. do1\fermata |
  }
  \tag #'coro21 {
    \clef "vsoprano" r4 do''8 do'' do''4 do''8 do'' |
    do''1 |
    do''2. |
    R2. r4 r sol' |
    la' la' la' |
    si' si' do'' re''4. sol'8 do''4 |
    la'2. si' |
    r2*3/2 r4 r la' |
    si' si' si' do'' do'' re'' |
    do''4. mi''8 re''4 si'( mi''2) |
    mi'' r4 r r si' |
    mi''4. mi''8 si'4 |
    mi''4 mi'' si' mi'' mi'' r8 la' |
    re''4 re'' si' do''4 do''8 mi'' re''4 |
    do''4. mi''8 re''4 si' do''4. do''8 |
    do''2 si'4( do''4.) fa''8 re''4 |
    do''4. mi''8 re''4 si' do''4. do''8 |
    do''2 si'4\melisma do''1\fermata\melismaEnd |
  }
  \tag #'coro22 {
    \clef "valto" r4 mi'8 mi' mi'4 do'8 do' |
    fa'1 |
    fa'2. |
    R2. |
    r4 r mi' |
    mi' mi' fad' |
    sol' sol' mi' sol' re' sol' |
    re'2. re' |
    r2*3/2 r4 r fad' |
    fad' fad' sold' la' la' \ficta sol' |
    la' mi' fa'8[ la'] mi'2. |
    dod'2 r4 r r fa' |
    sol'4. sol'8 sol'4 |
    sol' sol' sol' sol'4. sol'8 fa'4 |
    fa'4. re'8 re'[ sol'] mi'4 mi' fa' |
    sol'4. sol'8 re'4 sol' do' fa' |
    re'( sol'2) mi'2 fa'4 |
    sol'4. sol'8 re'4 sol' do' fa' |
    re'( sol'2) mi'1\fermata |
  }
  \tag #'coro23 {
    \clef "vtenor" r4 <<
      \original { la8 do' la4 mi'8 mi' | }
      \pygmalion { sol8 do' sol4 mi'8 mi' }
    >>
    la1 |
    la2. |
    R2. |
    r4 r do' |
    do' do' re' |
    re' re' la re' si mi' |
    la2. sol |
    r2*3/2 r4 r re' |
    re' re' mi' mi' mi' si |
    mi' do' la r8 mi si2 |
    la r4 r r re' |
    mi'4. mi'8 re'4 |
    mi'4 mi' re' mi' do' la |
    la la si sol sol fa |
    do'4. sol8 la4 re'8([ si] mi'4) la8 do' |
    sol[\melisma la si do'] re'[ sol]\melismaEnd sol2 fa4 |
    do'4. sol8 la4 re'8([ si] mi'4) la8 do' |
    sol[\melisma la si do'] re'[ sol]\melismaEnd sol1\fermata |
  }
  \tag #'coro24 {
    \clef "vbasse" r4 <<
      \original { la8 la la4 la8 la | }
      \pygmalion { do'8 do' do'4 do'8 do' | }
    >>
    fa1 |
    fa2. |
    R2. |
    r4 r do |
    la, la, re |
    sol, sol, la, si,4. si,8 do4 |
    re2. sol, |
    r2*3/2 r4 r re |
    si, si, mi la, la, si, |
    do4. do8 re4 mi2. |
    la,2 r4 r r sol |
    do'4. do'8 si4 |
    do' do' sol do' do' fa |
    re re sol do do re |
    mi4. mi8 fa4 sol mi fa |
    sol2. do2 re4 |
    do4. do8 fa4 sol mi fa |
    sol2. do1\fermata |
  }
>>
