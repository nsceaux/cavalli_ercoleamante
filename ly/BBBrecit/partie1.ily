\clef "viole1" R1*17 R2.*12 R4*7 
<< \original R1*10 \pygmalion R1*9 >>
\original { R2.*6 R1. R4*7 } R1*13 R2.*5 R1.
R2.*4 R4*7 |
la'4 do'' sol' mi'2 mi'4 |
sib' fa' sol' do'2 fa'4 |
re' la'4. la'8 la'1\fermata |
\original R2. R1. R2.*16 R1. R2. R4*7 |
