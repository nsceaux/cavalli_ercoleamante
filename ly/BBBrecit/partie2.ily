\clef "viole2" R1*17 R2.*12 R4*7
<< \original R1*10 \pygmalion R1*9 >>
\original { R2.*6 R1. R4*7 } R1*13 R2.*5 R1.
R2.*4 R4*7 |
fa'4 la' re' la'2 la'4 |
re'4 la' mi' la2 re'4 |
sol'8 fa' mi'4. mi'8 fad'1\fermata |
\original R2. R1. R2.*16 R1. R2. R4*7 |
