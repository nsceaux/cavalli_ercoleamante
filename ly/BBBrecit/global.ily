\key la \minor
\time 4/4 \midiTempo#120 s1*17
\digitTime\time 3/4 s2.*12
\measure 7/4 s4*7 \original \bar "|."
\time 4/4 << \original s1*10 \pygmalion s1*9 >>
\original {
  \digitTime\time 3/4 s2.*6
  \measure 6/4 s1.
  \measure 7/4 s4*7
}
\bar "|."
\time 4/4 s1*13 \bar "||"
\digitTime\time 3/4 s2.*5
\measure 6/4 s1.
\measure 3/4 s2.*4
\measure 7/4 s4*7 \bar "||"
\beginMark "Ritor[nello]" \measure 6/4 s1.*2 \measure 7/4 s4*7 \bar "||"
\original { \measure 3/4 s2. }
\measure 6/4 s1.
\measure 3/4 s2.*16
\measure 6/4 s1.
\measure 3/4 s2.
\measure 7/4 s4*7 \bar "|."
