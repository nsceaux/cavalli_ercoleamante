\clef "dessus" R1*17 R2.*12 R4*7 << \original R1*10 \pygmalion R1*9 >>
\original { R2.*6 R1. R4*7 } R1*13 R2.*5 R1.
R2.*4 R4*7 |
la''4 la'' sol'' la''2 la''4 |
sol'' fa'' mi'' fa''2 fa''4 |
sib'' mi''4. mi''8 re''1\fermata |
\original R2. R1. R2.*16 R1. R2. R4*7 |
