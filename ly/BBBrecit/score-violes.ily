\score {
  <<
    \new StaffGroup \with { \haraKiriFirst } <<
      \new Staff << \global \includeNotes "partie1" >>
      \new Staff << \global \includeNotes "partie2" >>
    >>
    \new StaffGroup \with { \haraKiriFirst } <<
      \new Staff << \global \includeNotes "viole0" >>
      \new Staff << \global \includeNotes "viole1" >>
      \new Staff << \global \includeNotes "viole2" >>
    >>
    \new ChoirStaff <<
      \new Staff \with { \tinyStaff \haraKiriFirst } \withLyrics <<
        \global \keepWithTag #'basse \includeNotes "voix"
      >> \keepWithTag #'basse { \set fontSize = #-2 \includeLyrics "paroles" }
      \new Staff <<
        \global \includeNotes "basse"
        \modVersion {
          s1*17\break
          s2.*13 s1\break
          s1*22 s2.*12 s1\break
        }
      >>
    >>
  >>
  \layout {
    system-count = #(*system-count*)
    indent = #(if (*instrument-name*)
                  largeindent
                  (or (*score-indent*) smallindent))
  }
}
