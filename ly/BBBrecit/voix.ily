\clef "vsoprano" <>^\markup\character Venere
r4 do''8 si' do'' do'' r8 re''16 mi'' |
si'4 si' r mi''8 mi'' |
si' si' r si'16 do'' la'4 la' |
r do''4 do''8 do'' do'' do''16 sib' |
do''4 do'' do''8 do'' do'' do''16 sib' |
re''4 re'' r sib' |
r sib'8 sib' sib'4 sib'8 la' |
\ficta sib'2 sib' |
\ficta sib'8 sib' sib' sib'16 sib' fa'4 fa' |
sol'8 sol'16 sol' sol'8 fa' la' la' do'' do'' |
fa''4 fa''8 do'' re''2 |
re''4 << \original { sib'8 la' sol'2 } \pygmalion { mi''16[ re''] mi''[ fa''] do''2 } >> |
fa' sol'8 sol' sol' sol'16 la' |
sib'4 sib' sib'8 sib'16 sib' do''8 re'' |
la'4 la' r8 la' la' sib' |
sol'2 r8 sol' sol' lab' |
lab'4 lab'8 sol' sol'4. fad'8( |
sol'2) sib'8 sol' |
la'4 sol' la' |
fa' fa' fa''8 re'' |
mi''4 re'' mi'' |
do'' do'' re''8 la' |
sib'4 do'' do'' |
re''2 dod''4 |
mi''4 mi'' la' |
si'2 si'4 |
dod''2 dod''4 |
re''2 re''4 |
sol''4 fa''4. mi''8 |
fa''4( mi''2) <<
  \original {
    re''1\fermata |
    \clef "vbasse" <>^\markup\character Ercole
    r2 re' |
  }
  \pygmalion {
    re''2\fermata
    \clef "vbasse" <>^\markup\character Ercole
    re' |
  }
>>
la4 la r la |
la la8 la la la sol la |
re4 re re'8 do' do' do'16 sib |
sib4 sib sib sib8 la |
la la la sib do'4 sib |
do'2 fa4 fa8 fa |
mi mi mi fa re re16 re re8 dod |
dod dod dod re mi4 fa16[ mi re8] |
mi2 << \original { la,4 la } \pygmalion { la,2 } >> |
\original {
  fa4. sol8 la si |
  do'4 do' sib8 la |
  sol4 la8[ si] do'[ la] |
  sib4 sib8[\melisma la sib sol] |
  la2\melismaEnd la4 |
  r4 la8[\melisma sol fa mi] |
  re4 re8[ do sib, la,] sol,4\melismaEnd fad, sol, |
  la,2. re1\fermata |
}
\clef "vsoprano" <>^\markup\character Venere
r4 re''8 fa'' re''4 re'' |
r la'8 si' dod''4 dod'' |
r8 dod'' dod'' re'' mi'' mi'' mi'' mi''16 fa'' |
re''8 re'' do'' sib' sib'4 la'8[ sol'] |
la'8[\melisma sib'16 la'] sol'4\melismaEnd fa'2 |
fa'8 sol' la' sol'16 fa' do''8 do'' r4 |
do''8 do''16 do'' do''8 do'' do''4 fa''8 si'16 do'' |
do''8 do'' do'' do'' sol'4 sol'8 la' |
sib' sib'16 sib' do''8 re'' la'4 la' |
la'8 la' si'4 do'' do'' |
do'' fa''8 fa''16 do'' re''4 re''8 re'' |
mi''8 mi''16 mi'' mi''8 sol'' dod''8 dod''16 dod'' re''8 mi'' |
la'8 la' r re'' re''8. dod''16( re''4) |
R2. |
fa''4 re'' la' |
fa'4. mi'8 re'4 |
la' re'' re'' |
do''4. si'8 la'4 |
re''4 sol'4. fa'8 la'2. |
la'4 sib' do'' |
re''4. do''8 re''4 |
si' << \original { do''4 re'' } \pygmalion { do''8[ si'] do''[ re''] } >> |
mi''4. re''8 mi''4 |
fa'' re''4. dod''8 re''1\fermata |
R1.*2 << \original { R4*7 | r4 r la' | } \pygmalion { r2*3/2 r4*4/3 r la' } >>
re''4 re'' dod'' re'' re''8 re'' \ficta do'' sib' |
la'4 la'8 la' sib' do'' |
re''[\melisma do'' re'' do'' re'' mi''] |
fa''4\melismaEnd do''8([ sib'] la'4) |
sib'8([ la'] sol'2) |
fa' la'8 si' |
do''4 si'4. do''8 |
do''4 do'' sol'8 la' |
sib'4 do''4. sib'8 |
re''4 re'' re'' |
do''4. sib'8 la' sol' |
fad'4 la'8[ sib'] do''[ sib'] |
do''[ re''] sib'4. la'8( |
sol'2) sib'8 do'' |
re''4 dod''4. re''8 |
re''4 re'' la'8 si' |
do''4 re''4. dod''8 |
mi''4 mi'' la' si'4. dod''8 re'' mi'' |
dod''4 si'8[ dod''] re''[ mi''] |
fa''[ sol''] fa''4. mi''8( re''1)\fermata |
