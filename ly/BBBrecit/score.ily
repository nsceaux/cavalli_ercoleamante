\score {
  \new StaffGroupNoBar \with { \haraKiriFirst } <<
    \new StaffGroupNoBracket <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
      \new Staff << \global \includeNotes "partie1" >>
      \new Staff << \global \includeNotes "partie2" >>
    >>
    \pygmalion\new StaffGroupNoBracket <<
      \new Staff << \global \includeNotes "viole0" >>
      \new Staff << \global \includeNotes "viole1" >>
      \new Staff << \global \includeNotes "viole2" >>
    >>
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \original\origLayout {
        s1*2\break s1*4\break s1*5\pageBreak
        s1*5\break s1 s2.*6\break s2.*7 s1\break
        s1*5\break s1*4\pageBreak
        s1 s2.*6\break s2.*3 s1*3\break s1*4\break
        s1*3 s2 \bar "" \break s2 s1*3\pageBreak
        s2.*9\break s2.*3 s1 s2.*5 s1\break
        s2.*7\break s2.*7\break s2.*7\pageBreak
      }
      \pygmalion\modVersion {
        s1*17\break
        s2.*13 s1\break
        s1*9\break
        s1*13 s2.*12 s1\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
