\clef "basse" la1 |
sold1~ |
sold2 la |
fa1~ |
fa |
sib, |
sib,~ |
sib,~ |
sib,~ |
sib,2 fa |
la sib |
sib, do |
fa, do |
sol,1 |
re |
sib,2 do~ |
do re |
sol,2 sol4 |
fa mi2 |
re re'4 |
do' sib2 |
la fa4 |
sol la2 |
sib2. |
la2 fa4 |
re sol2 |
mi4 la2 |
fa4 sib2 |
sol2. |
sol4 la2 re1\fermata |
\original re1 |
la~ |
la |
re2 re'4 do' |
sib1 |
la4. sib8 do'4 sib |
do'2 fa |
mi re |
dod4. re8 mi4 re |
mi2 la, |
\original {
  fa4. fa8 mi re |
  do4 do' sib8 la |
  sol4 la2 |
  sib sol4 |
  la4 la8 sol fa mi |
  re2 la,4 |
  sib, fa,2 sol,2. |
  la, re1\fermata |
}
re1 |
re2 la |
la1 |
re2. sib,4 |
do2 fa, |
fa mi~ |
mi re |
do1 |
sol,2 re~ |
re la |
la sib |
sol la |
fa4. sol8 la4 re |
re4 fa la |
re2 dod4 |
re2. |
do4 sib,2 |
la,2. |
sib,4 do2 fa,2. |
fa4 sol la |
\ficta sib2. |
sol4 la si |
do'2. |
fa4 sol la re1\fermata | \allowPageTurn
re'4 do' sib la2 la4 |
sib4 la sol fa2 fa4 |
sol la2 re1\fermata |
\original { re2. | }
re2 la4 re2 mi4 |
fa4. mi8 re do |
sib,2. |
la,2 re4 |
sib, do2 |
fa, fa4 |
mi4 re2 |
do2 do4 |
sib, la,2 |
sol, sib,4 |
do2. |
re2 mib4 |
do re2 |
sol, sol4 |
fa mi2 |
re re4 |
do sib,2 |
la, fa,4 sol,2 sib,4 |
la,2 sib,4 |
sol, la,2 re1\fermata |
