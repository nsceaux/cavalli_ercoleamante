Ogn’ im -- pe -- ro ha ri -- bel -- li
tra -- sgres -- so -- ri o -- gni leg -- ge
hor co -- me e que -- sti, e quel -- li
giu -- sta for -- za cor -- reg -- ge,
sì con so -- a -- ve in -- can -- to
ch’al do -- mi -- nio d’A -- mo -- re
for -- za è la più con -- for -- me
su -- pe -- ra -- re a tuo pro spe -- ro il ri -- go -- re
che ma -- li -- gna for -- tu -- na,
sem -- pre al mio fi -- glio av -- ver -- sa
di Io -- le in sen per tuo tor -- men -- to a -- du -- na; __
e go -- drai de’ miei det -- ti
e go -- drai de’ miei det -- ti
og -- gi og -- gi al giar -- din de’ fio -- ri i dol -- ci dol -- ci dol -- ci
i dol -- ci ef -- fet -- ti.

O Dè -- a se tan -- to à le mie bra -- me ot -- tie -- ni
giu -- sto fia ch’io t’ac -- cen -- da
tut -- te d’A -- ra -- bia l’o -- do -- ra -- te sel -- ve,
e che tut -- te a te sve -- ni
del E -- ri -- man -- to le zan -- nu -- te bel -- ve;
\original {
  ch’il ciel non può ver -- sa -- re
  de i con -- ten -- ti d’A -- mor gra -- tie gra -- tie più ra -- re.
}

Van -- ne al lo -- co, e m’at -- ten -- di, e fa ch’I -- o -- le
pur vi si ren -- da pria che man -- ch’il so -- le,
ch’io dell’ ar -- mi prov -- vi -- sta
on -- de sua fe -- ri -- tà vin -- cer pre -- su -- mo,
pre -- ver -- rò di -- li -- gen -- te i di lei pas -- si
per dis -- por qui -- vi pria, ch’el -- la vi giun -- ga
ro -- ven -- te a -- cu -- to stra -- le,
che per te l’ar -- da, e pun -- ga.

Stra -- le in -- vi -- si -- bi -- le,
ch’in -- e -- vi -- ta -- bi -- le
tal forz’ ha -- vrà,
ch’all’ in -- sen -- si -- bi -- le
pia -- ga in -- sa -- na -- bi -- le
im -- pri -- me -- rà.

Su dun -- que su dun -- que o -- gni tri -- stez -- za
sia dal tuo cor __ sban -- di -- ta,
ch’in a -- mor l’al -- le -- grez -- za
co -- me al ciel più gra -- di -- ta
con più fe -- li -- ci -- tà le gio -- ie in -- vi -- ta __
ch’in a -- mor l’al -- le -- grez -- za
co -- me al ciel più gra -- di -- ta
con più fe -- li -- ci -- tà le gio -- ie in -- vi -- ta. __
