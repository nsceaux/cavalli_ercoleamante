\clef "viole2" R1*17
R2. |
\noHaraKiri r4 r sol8 mi |
fa4. fa'8 fa'4 |
mi' re'2 |
fa'4 do' fa' |
mi'2 do'4 |
fa'4 re'2 |
dod'4 la2 |
fad4 si2 |
sol4 dod'2 |
la4 re'2 |
mi'4 re'2 | \revertNoHaraKiri
re' dod'4 re'1\fermata |
R1*9 R1*13 R2.*5 R1. R2.*4 R4*7 R2.*4 R4*7 |
