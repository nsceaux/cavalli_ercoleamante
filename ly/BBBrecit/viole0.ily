\clef "viole0" R1*17 |
r4 r re'8 mi' |
fa'4 sol'2 |
la' la'8 sib' |
do''4 re''4. sib'8 |
do''4 la'4. re''8 |
re''4 do'' fa''8 mi'' |
re'' do'' sib' la' sol'4 |
la'2.~ |
la'2 sol'4~ |
sol'2 la'4~ |
la'2 sib'4 |
si'!4. dod''8 re''4~ |
re''4 la'2 la'1\fermata |
R1*9 R1*13 R2.*5 R1. R2.*4 R4*7 R2.*4 R4*7 |
