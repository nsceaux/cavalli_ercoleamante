\clef "viole1" R1*17
r4 r sib |
re'2 dod'4 |
re'2. |
la'2 sol'4 |
la' mi' re' |
sol'2 fa'4 |
sib'8 la' sol' fa' mi' re' |
mi'2 re'4~ |
re' re' mi'~ |
mi' mi' fa'~ |
fa' fa' sol' |
sol'4. la'8 sib'4~ |
sib' mi'4. mi'8 fad'1\fermata |
R1*9 R1*13 R2.*5 R1. R2.*4 R4*7 R2.*4 R4*7 |
