\tag #'(couplet1 basse) {
  Con -- ge -- do a gl’or -- ri -- di
  suoi flut -- ti al -- tis -- si -- mi
  poi ch’il mar diè, __
  ze -- fi -- ri flo -- ri -- di
  su fes -- to -- sis -- si -- mi
  vo -- la -- te a me
  e in dan -- za le -- pi -- da
  da voi si ve -- ne -- re
  la mia vir -- tù, __
  che sem -- pre in -- tre -- pi -- da
  con -- tro di Ve -- ne -- re
  vit -- tri -- ce fu.
}
\original\tag #'couplet2 {
  \override Lyrics.LyricText.font-shape = #'italic
  [Sol gl’a -- mor re -- gni -- no
  da qua -- li spie -- ghi -- si
  o -- nes -- to ar -- dor, __
  e i cie -- li sde -- gni -- no
  ch’in al -- tro im -- pie -- ghi -- si
  il lor fa -- vor:]
  [de -- sir che se -- gui -- no
  af -- fet -- ti i -- gno -- bi -- li
  stian sem -- pre in duol, __
  e si di -- le -- gui -- no
  dell’ al -- me no -- bi -- li
  qual neb -- bia al sol.]
}
