\clef "vbas-dessus" <>^\markup\character Giunone
mi''4 re'' do'' si'4. do''8 re''4 |
do'' si' la' mi''4. mi''8 mi''4 |
la' sol' fa' mi'4.\melisma re'8[ mi' fad'] |
sol'2.\melismaEnd sol'4. la'8 si'4 |
do''4. si'8 do''4 |
si' do'' re'' mi''4. fa''8 sol''4 |
fa''8[ mi''] re''[ do''] re''[ mi''] |
do''1*3/4\fermata |
R2.*13 |
mi''4 re'' do'' |
si'4. do''8 re''4 do'' si' la' |
mi''4. mi''8 mi''4 la' sol' fa' |
mi'4.\melisma re'8[ mi' fad'] sol'2.\melismaEnd |
sol'4 la' si' |
do''4. si'8 do''4 si' do'' re'' |
mi''4. fa''8 sol''4 fa''8[ mi''] re''[ do''] re''[ mi''] do''1*3/4\fermata |
R2.*13 |
