\piecePartSpecs
#(let ((breaks #{ s2.*13\break s2.*13\break s2.*13\break #}))
   `((dessus #:score-template "score-dessus-voix" #:music ,breaks)
     (parties #:score-template "score-parties-voix" #:music ,breaks)
     (violes #:score-template "score-parties-violes-voix" #:music ,breaks)
     (basse #:score-template "score-basse-continue-voix" #:music ,breaks)))
