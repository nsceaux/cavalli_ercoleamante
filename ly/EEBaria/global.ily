\key do \major \midiTempo#160
\beginMark "Aria" \digitTime\time 3/4 \measure 6/4 s1.*4
\measure 3/4 s2.
\measure 6/4 s1.
\measure 3/4 s2.*2 \bar "|."
\beginMark "Ritor[nello]" \digitTime\time 3/4 \measure 6/4 s1.*2
\measure 9/4 s2.*3
\measure 6/4 s1.*3 \bar "|."
\beginMark\markup\concat { 2 \super da } \digitTime\time 3/4 s2.
\measure 6/4 s1.*3
\measure 3/4 s2.
\measure 6/4 s1.
\measure 9/4 s2.*3 \bar "|."
\beginMark "Ritor[nello]" \digitTime\time 3/4 \measure 6/4 s1.*5
\measure 9/4 s2.*3 \bar "|."
