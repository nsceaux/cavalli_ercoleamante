\clef "dessus" sib''1 la''2 sib'' |
sol''1 fad'' |
re''4 sol'' fa'' sib''8 la'' |
sol'' fa'' mib'' re'' do''2 |
re''4 r r8 fa'' sib''4 |
la'' re'''8 do''' sib'' la'' sol'' fa'' |
mi''4 la'' fad'' sol'' |
r4 la'' fa'' sol'' |
mib'' fa'' re'' mib'' |
do'' re'' sib'8 do'' re'' mi'' |
fad''2 r8 sib' mib''4 |
re'' sol''8 fa'' mib'' re'' do'' sib' |
la' la'' sib'' do''' la''4. sol''8 sol''1\fermata |
r4 r8 sib'' la''8. sol''16 |
fa''4. fa''8 sol''4 la''4. do'''8 sib'' la'' |
sol''4. sol''8 la''4 |
sib''4. sol''8 fa'' sol'' |
mib'' fa'' re''2 do''4. mib''!8 fa''4 |
sol''2. |
r4 r8 sib' do''4 re''4. re''8 mi''4 |
fa''4. re''8 do'' re'' sib' do'' la'2 si'1\fermata |
