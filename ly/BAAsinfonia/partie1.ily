\clef "viole1" sol'1 la'2 re' |
sol'1 la'2. la'4 |
sol'4 sib' sib'8 fa' sol' re' |
mib' sib do' fa' fa'2 |
fa'4. fa'8 fa'4 sol' |
la'8 la' sib' fa' sol' la' mi' la' |
la'2 la'4 sol' |
sol' fa' fa' mib' |
\ficta mib' re' re' do' |
do' sib re'2 |
re'4 r8 sib mib'4 sol' |
sol'8 re' mib' fa' do' re' la sib |
re'4 sib re'2 re'1\fermata |
r4 r8 re' fa'4 |
fa'4. fa'8 sib'4 la'4. la'8 sol' fa' |
do''4. do''8 sol' do' |
sol'4. sol'8 lab'4 |
\ficta lab' re' sol' sol'2. |
r4 r8 sol' la'4 |
sib'2. r4 r8 sol' la' mi' |
la'4. re'8 mib'4 mib' la la' sol'1\fermata |
