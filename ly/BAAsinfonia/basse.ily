\clef "basse" sol,1 re2 sib, |
mib1 re2. re4 |
sol4 mib sib8 la sol fa |
mib re do sib, fa2 |
sib,4. fa8 sib4 sol |
re'8 do' sib la sol fa mi re |
la2 re4 sol |
mi fa re mib |
do re sib, do |
la, sib, sol,2 |
re4. re8 mib4 do |
sol8 fa mib re do sib, la, sol, |
re1 sol,\fermata |
r4 r8 sol la4 |
sib4. sib8 la sol fa4. la8 sib4 |
do'4. do'8 sib la |
sol fa sol mib lab sol |
lab fa sol2 do2. |
r4 r8 do' sib la |
sol2. r4 r8 sol fa mi |
re do re sib, mib re mib do re2 sol,1\fermata |
