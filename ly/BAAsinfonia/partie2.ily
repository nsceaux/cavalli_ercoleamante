\clef "viole2" re'1 re'2 fa' |
sib sol re'2. re'4 |
re'4 mib' fa'8 do' sib fa' |
sol' re' mib' sib do'2 |
sib4. do'8 re'4 mi' |
fa'8 do' re' la sib re' sol' re' |
mi'2 re'4 re' |
mi' do' sib sol |
la fa sol la |
fa fa sib sol |
la r8 re' do'4 do' |
re'8 fa' sol' re' mib' sib do' sol |
la4 re' la2 si1\fermata |
r4 r8 sib do'4 |
re'4. re'8 do' sib do'4. fa'8 re'4 |
sol'4. sol'8 re' la |
re'4 sib do' |
do' sol' re' mib'2. |
r4 r8 mib' re' do' |
re'2. r4 r8 sib do'4 |
re'4. fa'8 do'4 sol re'2 re'1\fermata |
