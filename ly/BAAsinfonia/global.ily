\key re \minor \midiTempo#160
\time 4/4 \measure 2/1 s\breve*2
\measure 4/4 s1*10
\measure 2/1 s1*2 \bar "|." \endMarkSmall "Segue sub[ito]"
\digitTime\time 3/4 s2.
\measure 6/4 s1.
\measure 3/4 s2.*2
\measure 6/4 s1.
\measure 3/4 s2.
\measure 6/4 s1.
\measure 10/4 s4*10 \bar "|."
\endMarkSmall\markup\column {
  Un ostre fois Replica tripla
}