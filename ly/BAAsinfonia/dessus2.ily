\clef "dessus" sol''1 fa''2 re''~ |
re'' do'' re'' la'4 re'' |
sib' mib'' re''8 do'' mib'' fa'' |
do'' re'' la' sib' sib'4 la' |
sib'4. la'8 re''4 sol'' |
fa''8 mi'' sol'' la'' mi'' fa'' dod'' re'' |
re''4 dod'' re'' sib' |
do'' la' re'' sib' |
do'' la' sib' sol' |
la' fa' sol'8 la' sib' do'' |
la' la' sib'4 sol' do''8 sol' |
sib' la' do'' re'' la' sib' fad' sol' |
fad' fad'' sol''2 fad''4 sol''1\fermata |
r4 r8 sol'' fa''8. mib''16 |
re''4. re''8 mi''4 fa''4. do''8 re''4 |
mib''4. mib''8 re'' do'' |
re''4. mib''8 re''4 |
do''4 do''4. si'8 do''4 do'' re'' |
mib''2. |
r4 r8 sol'' la''4 sib''4. sib''8 la'' sol'' |
fa'' mi'' re'' sib'' sol''4 sol'' sol''4. fad''8 sol''1\fermata |
