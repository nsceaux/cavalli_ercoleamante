\clef "dessus" r2*3/2 r4 r8 mi'' si' dod'' |
re''4. si'8 do'' re'' |
mi''4 mi''4. red''8 |
mi''2 r4 |
r4 r8 mi'' re'' mi'' |
do''4. do''8 mi'' mi'' |
fa''4. si'8 do'' re'' |
sol' do'' do''4. si'8 |
do''4. si'8 sol' la' |
si'2. |
r4 r8 do'' la' si' |
do''4. si'8 sol' la' |
si'4. mi''8 re'' mi'' do'' re'' si'2 dod''1*3/4\fermata |
