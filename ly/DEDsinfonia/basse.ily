\clef "basse" la,2. mi |
si4. sol8 la4 |
mi si2 |
mi re4 |
do4. do'8 si do' |
la4. la8 sol la |
fa4. sol8 la si |
do' sol sol2 |
do4. re8 mi do |
sol4. la8 si sol |
re'4 re8 mi fa re |
la4 la,8 si, do la, |
mi4. do8 re4 la, mi2 la,1*3/4\fermata |
