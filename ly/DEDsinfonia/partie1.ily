\clef "viole1" r2*3/2 r4 r8 si' sol' sol' |
si'4. mi'8 la'4 |
si' si'2 |
si'4. si'8 si' si' |
do''2 r4 |
r4 r8 la' mi' la' |
la'4. mi'8 la' sol' |
do'' la' sol'2 |
sol'4. fa'8 mi' fad' |
sol'2. |
r4 r8 sol' fa' sol' |
la'4. re'8 mi' do' |
mi'4. mi'8 la'4 la' mi'2 mi'1*3/4\fermata |
