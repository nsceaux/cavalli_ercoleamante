\piecePartSpecs
#`((dessus)
   (parties)
   (violes)
   (basse #:score-template "score-basse-continue" #:system-count 2)
   (silence #:on-the-fly-markup , #{ \markup\tacet #}))
