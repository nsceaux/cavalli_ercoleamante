\clef "dessus" r4 r8 la'' mi'' fad'' sol''2. |
r4 r8 si'' la'' si'' |
sol'' la'' fad''2 |
mi''4 r8 sol'' fa'' sol'' |
mi''2 r4 |
r4 r8 do''' si'' do''' |
la''4. sol''8 fa'' sol'' |
mi'' fa'' re''4. do''8 |
do''2 r4 |
r4 r8 fa'' re'' mi'' |
fa''2. |
r4 r8 sol'' mi'' fad'' |
sold''4. la''8 fad'' sold'' la''4 la''4. sold''!8 la''1*3/4\fermata |
