\clef "viole2" r2*3/2 r4 r8 sol' mi' mi' |
fad'4. sol'8 mi'4 |
mi' fad'2 |
sold' r4 |
r4 r8 sol' sol' sol' |
la'4. mi'8 sol' do' |
do'4. sol'8 do' si |
mi' re' re'2 |
mi'4. re'8 do' do' |
re'2. |
r4 r8 mi' re' re' |
mi'4. sol'8 do' fad' |
si4. do'8 la4 mi' si2 la1*3/4\fermata |
