\clef "dessus" <>^"Trombe" re''8. re''16 fad''8. fad''16 la''8. la''16 fad''8. fad''16 |
re''8. re''16 fad''8. fad''16 la''8. la''16 fad''8. fad''16 |
la''8 la''16 la'' fad''8. fad''16 la''8 la''16 la'' fad''8. fad''16 |
la''4 la'' la'' la'' |
fad'' fad'' fad'' fad'' |
re'' re'' re'' re'' |
re''1\fermata |
