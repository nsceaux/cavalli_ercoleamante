\clef "dessus" r4 re''8. re''16 fad''8. fad''16 la''8. la''16 |
fad''8. fad''16 re''8. re''16 fad''8. fad''16 la''8. la''16 |
fad''8. fad''16 la''8 la''16 la'' fad''8. fad''16 la''8 la''16 la'' |
fad''8. fad''16 r4 r2 |
la''4 la'' la'' la'' |
fad'' fad'' fad'' fad'' |
fad''1\fermata |
