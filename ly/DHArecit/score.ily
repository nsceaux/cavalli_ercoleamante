\score {
  \new StaffGroupNoBar <<
    \pygmalion\new StaffGroupNoBracket \with { \haraKiriFirst } <<
      \new Staff << \global \includeNotes "viole0" >>
      \new Staff << \global \includeNotes "viole1" >>
      \new Staff << \global \includeNotes "viole2" >>
    >>
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \original\origLayout {
        s1*4\pageBreak
        s1*4\break s1*4\break s1*4\break s1*3\break s1*2\pageBreak
        s1*4\break s1*3\break s1*5\break s1*4\break s1*5\pageBreak
        s1*6\break s1*3 s2 \bar "" \break s2 s1*2 s2 \bar "" \break
        s2 s1*2 s2 \bar "" \break s2 s1*4\pageBreak
        s1*6\break s1*5\break s1*4\break s1*5\break s1*3\pageBreak
        s1*3\break
      }
      \pygmalion\modVersion { s1*17\break s1*15\break }
    >>
  >>
  \layout { }
  \midi { }
}
