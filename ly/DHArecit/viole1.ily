\clef "viole1" R1*17 |
do'8 re' mib'4 do'8 re' mib' fa' |
sol' la' sib'4 sol'8 la' sib'4 |
sib8 do' re' mi' la si dod'4 |
fad8 sol la4 fad8 sol la4 |
fad8 sol la fad si do' re'4 |
si8 do' re' sol sol2 |
fad8 sol la4 sib8 do' re'4 |
si4. re'8 do'4. mi'8 |
dod'4. mi'8 re'4. fad'8 |
sol' la' sib'4 mib'8 fa' sol'4~ |
sol'8 sol' fad' sol' fad' mi' sol' fad'~ |
fad' sol' la'4 sol'8 la' sib'4 mib'4. mib'8 mi'[ fad'] sol'4 |
re'2. la8 re' si do' re'4 sol2\fermata |
R1*31 |
