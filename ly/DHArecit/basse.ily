\clef "basse"
\original {
  fa2 sib |
  sib,1~ |
  sib,2 fa, |
  fa,4 do fa,2 |
  fa,2 do~ |
  do sol |
  sol1 |
  sol~ |
  sol2 mib |
  fa sol4 do |
  fa1~ |
  fa2 sib,~ |
  sib,2 la,~ |
  la,4 sib, do2 |
}
fa,1~ |
fa, |
sol,4 do r2 |
sol,1 |
sol, |
fa,4 fa mib~ mib8 do |
re4 sol,~ sol,2 |
la,1 |
re2. sib,4 |
la,1 |
fa, |
sib, |
sib,4 mi! fa2 |
la, sib,~ |
sib,1 |
la,4 sib, do2 fa,1 | \allowPageTurn
do |
sol,~ |
sol, |
re~ |
re2 si,~ |
si, do~ |
do re~ |
re mi~ |
mi fad |
sib4 sol mib si, |
do1 |
re do2 dod |
re1 sol,\fermata | \allowPageTurn
\original {
  do1 |
  do2 fa, |
  fa, sib, |
  re1 |
  re |
  re4 la,~ la, re |
}
sol,1 |
sol,~ |
sol,2 do |
do1 |
re2. mi4 |
do2 re |
sol,4~ sol, mi2~ |
mi fa |
re1 | \allowPageTurn
re |
la,~ |
la,2 re |
sol, do |
fa, sib,~ |
sib, sol,~ |
sol, re |
re la,4 sol, |
la,2 re |
\original {
  sol,1 |
  do |
  do |
  do |
  do |
  fa,~ |
  fa,4 sol, do2 |
}
sol,2 re4 do |
re2 sol, |
la,1~ |
la,2 sold, |
la,1 |
fa,2~ fa,4 mi, |
fa,1~ |
fa,2 sib,~ |
sib, fa, |
do1 |
sol,2 la, |
do1 |
fa, |
