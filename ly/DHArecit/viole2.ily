\clef "viole2" R1*17 |
sol1 |
sib8 do' re'4 sib8 do' re'4 |
sol8 la sib4 sol2 |
la8 sib do'4 la4. sib8 |
do'8 sib la re' sol la si4 |
sol2 do'8 re' mib' sol |
la sib do'4 sol8 la sib4 |
fa8 sol lab sol sol lab sib do' |
sol la sib la la sib do' re' |
re'4. do'8 do' re' mib' re' |
mib'4. mib'8 re' la sib do' |
la sol re'4 re'4. sol8 lab sib do'4 sol2~ |
sol8 la sib do' re'2 re'4. la8 si2\fermata |
R1*31 |
