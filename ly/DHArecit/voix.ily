\original {
  \ffclef "vbas-dessus" <>^\markup\character Deianira
  fa''4 r8 re'' sib'4 sib' |
  r re''8 re'' sib' sib' sib' sib' |
  sib'4 sib'8 la' la'4 la' |
  r8 do''16 la' la'8 sol'16 la' fa'8 fa' r4 |
  \ffclef "valto" <>^\markup\character Licco
  r8 do' fa' sol' mib'8 mib' mib' mib' |
  mib'4 mib'8 mib' re' re' r re' |
  re' re' re'4 si si8 la |
  si4. si8 re'4 re' |
  re'8 re'16 re' re'8 mib' do'4 do'8 do' |
  do' do' fa' sol' mib'8. re'16( do'4) |
  r do'4 do'8 do' do' do' |
  fa'8 fa' do' re' sib8 sib r sib |
  re' re' re' mi' fa'8. fa'16 fa'8 la' |
  fa' fa' r4 r2 |
}
\ffclef "vbasse" <>^\markup\character Ercole
r4 fa fa8 fa fa sol |
la la la la si si r si16 do' |
sol4 do r2 |
r8 sol sol fad sol sol r sol |
sol sol16 sol sol sol sol la sib8 sib16 sib sib sib sib la |
la la r re' la la la sib do' fad sol la sol8 sol |
\ffclef "vbas-dessus" <>^\markup\character Deianira
r8 re''( sol''4) mi''16 mi'' mi'' mi'' dod'' dod'' dod'' dod'' |
la'8 la' r16 mi'' mi'' mi'' mi'' mi'' fa'' sol'' fa''8 mi'' |
fa''16 fa'' fa'' mi'' mi''8 mi''16 fa'' re''4 sol''8 sol''16 re'' |
mi''4 mi'' r2 |
r8 fa'' do'' fa'' do'' do'' do'' do''16 re'' |
sib'4 sib' re''8 fa'' re'' re''16 re'' |
sib'8 sib' r16 sib' sib' la' la'4 la' |
do''8 do'' do'' do''16 re'' re''4 re'' |
re''4 re''8 re'' fa''8. fa''16 fa''4 |
r4 sib'8 la' sol'2 fa'1 |
r4 sol'8 sol' sol'4 sol'8 la' |
sib'4 sib' re''8 re'' re'' re''16 re'' |
sib'4 sib'8 la' la'4 la'8 la' |
fad'4 fad' r re''8 re'' |
la'4 la'8 sib' sol'4 sol'8 sol' |
re''8 re''16 re'' re''8 mib'' do''4 do'' |
la'8 la' la' la'16 sib' sib'4 sib' |
si'8 si' si' si'16 do'' do''4 do'' |
dod''8 dod''16 dod'' dod''8 dod''16 re'' re''4 re''8 re'' |
sol'4 sol' r si' |
mib''(\melisma do'' la' \override AccidentalSuggestion.avoid-slur = #'outside \ficta mi'?) |
fad'2\melismaEnd sib' lab'( sol')~ |
sol'2. fad'4\melisma sol'1\fermata\melismaEnd |
\original {
  \ffclef "vbasse" <>^\markup\character Ercole
  r8 mi16 mi mi8 mi16 sol do8 do r4 |
  r8 mi16 mi sol8 sol16 la fa4 r |
  \ffclef "valto" <>^\markup\character Licco
  r8 fa' do' re' sib sib r4 |
  r8 fa'16 fa' fa'8 fa'16 fa' re'8 re' r re' |
  re' re' re' re' re' re' re' re' |
  re' re'16 mi' mi'8 mi' mi'8 mi'16 fa' re'8 re' |
}
\ffclef "vbasse" <>^\markup\character Ercole
si8 si re' re'16 re' sol8 sol r sol |
sol sol sol sol sol sol16 sol sol8 sol |
sol sol sol16 la si sol do'8 do' r4 |
do'8 do'16 do' sol4 sol8 sol16 la si8 la |
la la r re'16 re' si8 si16 si sol8 sol16 sol |
do'8 si sol sol
\ffclef "vbas-dessus" <>^\markup\character Deianira
r4 re'' |
si'8 si'
\ffclef "vbas-dessus" <>^\markup\character Iole
mi''4 do'' do'' |
do''8 sib' sib' la' la'4 la' |
r8 re'' re'' re'' re'' re'' r re'' |
re'' re'' re'' re'' re'' re'' re'' dod'' |
mi''4 mi'' mi''8 mi'' mi'' mi''16 mi'' |
mi''4 mi''8 mi''16 fa'' re''8 re'' r4 |
r re'' do''2 |
r4 do'' sib'2 |
sib'4 sib'8 do'' re''4 re'' |
re'' sib'8 re'' la'4 la' |
r8 la' la' si' dod'' re'' re'' re'' |
R1 |
\original {
  \ffclef "valto" <>^\markup\character Licco
  re'4 re' r8 re' sol' sol' |
  mi' mi' r4 r do' |
  mi' mi'8 mi' sol'4 sol' |
  sol'8 sol' sol' sol'16 fa' sol'4 sol' |
  sol'8 sol' sol' sol'16 sol' sol'8 sol' sol' sol'16 fa' |
  la'4 la' fa'8 fa'16 fa' fa'8 mi' |
  mi'8 mi'16 re' re'8 mi' do' do' r4 |
}
\ffclef "vbasse" <>^\markup\character Ercole
r8 re re mi fad sol sol sol |
R1 |
mi4 mi8 mi la4 la |
la8 la la la16 si si8 si si do'16 do' |
la4 la r2 |
la8 la16 do' la8 la sol8 sol16 sol sol8 la |
fa8 fa r fa la4 la8 la16 sib |
do' do' do' do' do'8 do'16 re' sib4 sib8 sib16 la |
sib8 sib16 sib fa8 sol la do' la16 la la sol |
sol8 sol r sol sol sol sol sol16 la |
sib8 sib16 sib sib8 la la la16 la la8 sol |
sol4 r8 la la sol sol la |
fa fa r4 r2 |
