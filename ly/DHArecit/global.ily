\key fa \major \midiTempo#100
\time 4/4
\original {
  s1*4 \bar "|."
  s1*10 \bar "|."
}
s1*15 \measure 2/1 s1*2 \measure 4/4 s1*11 \measure 2/1 s1*4 \bar "|."
\measure 4/4
\original {
  s1*2 \bar "|."
  s1*4
}
\key do \major s1*18 \bar "|."
\original { s1*7 \bar "|." }
s1*5
\key fa \major s1*8 \bar "|."
