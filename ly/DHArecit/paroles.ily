\original {
  Ah che scor -- go? il mio fi -- glio
  post’ è in gra -- ve pe -- ri -- glio?
  Forz’ è ben, ch’io mi sco -- pra.
  
  Il ciel ti guar -- di
  da co -- tan -- ta fol -- li -- a,
  che quand’ an -- cor com’ è suo stil per gio -- co
  Er -- col l’am -- maz -- zi un po -- co,
  tu ne puoi far de gli al -- tri;
  ma se n’uc -- ci -- de noi fia mol -- to peg -- gio,
  che poi chi ne re -- sus -- ci -- ti, no’l veg -- gio.
}

Più di sal -- var -- lo ten -- ti
più l’ac -- cu -- si, e tu men -- ti,
ma ch’al tuo cri -- me, o pu -- re
a mie ge -- lo -- se cu -- re
il tuo mo -- rir s’as -- cri -- va
sof -- frir più non sa -- prei, no che tu vi -- va.

Ah __ bar -- ba -- ro di fé, di pie -- tà a -- va -- ro.
Non bas -- ta ha -- ver -- mi l’a -- mor tuo ri -- tol -- to,
ch’an -- cor to -- glier mi vuoi pe -- gno sì ca -- ro;
fa’ pur fa’ pur tua spo -- sa I -- o -- le,
ab -- ban -- do -- na -- mi pu -- re a o -- gni mar -- to -- ro,
ma per so -- lo ris -- to -- ro
las -- cia -- mi las -- cia -- mi la mia pro -- le.
In -- no -- cen -- te, che si -- a,
chi pro -- pi -- zio gli si -- a, s’in -- giu -- sto el pa -- dre?
E quand’ an -- che sia re -- o, con -- ce -- di con -- ce -- di il van -- to
d’im -- pe -- trar -- li per -- do -- no
d’u -- na mi -- se -- ra ma -- dre mi -- se -- ra mi -- se -- ra ma -- dre
al lar -- go al lar -- go pian -- to. __

\original {
  In mal pun -- to giun -- ges -- ti
  e chi qua ti por -- tò?
  
  Non fu già Lic -- co;
  chi m’in -- se -- gna u -- na ta -- na?
  Che quand’ anch’ el -- la fos -- se,
  d’un gran lu -- po af -- fa -- ma -- to io mi ci fic -- co.
}

Am -- bo am -- bo mor -- re -- te, e fra tan -- te al -- tre pro -- ve
che fer di me già sì fa -- mo -- so il gri -- do
di -- ca -- si an -- cor, ch’al -- tri duo mos -- tri uc -- ci -- si
u -- na mo -- glie ge -- lo -- sa, e un fi -- glio in -- fi -- do.

Ah cru -- do.

Ah sen -- ti sen -- ti sen -- ti pri -- a: s’al -- cu -- na spe -- me
ch’io pie -- ghi a l’a -- mor tuo, res -- tar ti puo -- te,
so -- lo al vi -- ver di lui ques -- ta s’at -- tie -- ne;
s’ei mor, s’ei mor, fia, ch’o -- gni spe -- me an -- co a te pe -- ra,
e s’e -- gli vi -- ve, spe -- ra.

\original {
  Ho -- ra ch’il cre -- de -- ri -- a: quel gran -- de in -- vit -- to
  do -- ma -- tor de’ Gi -- gan -- ti,
  che i dia -- vo -- li is -- tes -- si ha tri -- on -- fa -- to
  ec -- co -- lo tra due fem -- mi -- ne in -- tri -- ga -- to!
}

E s’e -- gli vi -- ve spe -- ra? o -- gni pos -- san -- za
so -- vra l’a -- ni -- ma a -- man -- te ha la spe -- ran -- za.
Van -- ne tu dun -- que, e tor -- na al pa -- trio ni -- do,
e tu va’ pri -- gio -- nie -- ro
né la tor -- re del mar, ch’al -- tro ri -- pa -- ro
si -- cu -- ro ha -- ver non può mia ge -- lo -- si -- a,
e con I -- o -- le in -- tan -- to io ve -- drò chia -- ro
del mio spe -- rar, del vi -- ver tuo che fi -- a?
