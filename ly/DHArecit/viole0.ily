\clef "viole0" R1*17 |
mib'8 fa' sol'4 mib'8 fa' sol'4 |
re'1~ |
re'2 dod'8 re' mi'4 |
re'2 re'8 mi' fad'4 |
re'8 mi' fad'4 re'4. sol'8 |
re'8 mib' fa' sol' mib' fa' sol' mib'~ |
mib' mib' mib'8. re'16 re'4 sol'8. la'16 |
re'8 mib' fa'4 mi'8 fa' sol' lab' |
mi' fa' sol' la' fad' sol' la' sib' |
sib'8 do'' re''4 sol'4. re''8 |
do'' sib' la'4 do'' sib'8 la' |
la'8 sib' do''4 sib'8 do'' re''4 do''8 re'' mib''4 la'4. sib'8 |
sib' do'' re''4 la'4. sib'8 sol'1\fermata |
R1*31 |
