\score {
  \new StaffGroupNoBar \with { \haraKiriFirst } <<
    \pygmalion\new StaffGroupNoBracket <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
    >>
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \original\origLayout {
        s1*3\break s1*4\break s1*5\pageBreak
        s1*4\break s1*5\break s1*3\break s1*3\break s1*3\pageBreak
        s1*4 s2.\break s2.*9\break s2.*9\break s2.*12\break s1*3\pageBreak
        s1*3 s2 \bar "" \break s2 s1*4\break
        s1*5\break s1*5\break s1*6\pageBreak
        s1*7\break s1*6\break s1*5\break
      }
      \pygmalion\modVersion { s1*30\break s2.*31\break }
    >>
  >>
  \layout { }
  \midi { }
}
