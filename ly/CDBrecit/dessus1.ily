\clef "dessus" R1*30 |
r8 do'' la'4 do'' |
r8 la' do''4 sol' |
la' do''4. sol'8 |
sol'4 r8 do' mi'4 |
fa' r8 mi' sol'4 fa' do''4. si'8 |
do''4 do'2 sol'2.~ sol'8 fa' mi' re' mi'4 |
r8 do'' sol'4 r8 do'' |
sib' do'' re''4 la'8 re'' |
sib' sol' r la' mi' la' |
re'4 r8 mi' fad'4 |
r8 sib' re''4 r8 re' |
fad'4 r8 re' fad'4 |
r8 sol' fad' sol' la' mi' |
fad'4 r8 mi' fad'4 |
r8 sib' la'4 r8 do'' sib'4 r8 re'' sol'4 |
mi'4 fa'8 re' mi'4 r8 re'' do''4 r8 do'' |
fad'4 r8 re'' la'4 r8 sib' re''4 r8 do'' |
la'4 r8 sol' do''4 |
r8 la' do''4 r8 do' |
fa'4 r8 fa' do'' do' mi'4 r8 sol' mi'4 |
r8 do'' la'2 |
r8 fa' re'4 re'' do''4. sib'16 la' sol'4 la'1*3/4 | \allowPageTurn
R1*51 |
