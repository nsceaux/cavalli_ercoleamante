\piecePartSpecs
#(if (eqv? #t (ly:get-option 'pygmalion))
     '((dessus #:score-template "score-dessus-voix")
       (violes #:score-template "score-voix" #:notes "basse" #:clef "bass")
       (basse #:score-template "score-basse-continue-voix"))
     '((basse #:score-template "score-voix")))
