\clef "vsoprano" <>^\markup\character Paggio
r8 sib'16 re'' sib'8 sib'16 la' do''8 do'' r4 |
\ffclef "valto" <>^\markup\character Licco
r8 fa' sol' la' fa' fa' r do' |
do' do' fa' fa' r fa' sib4 |
r8 sol' mib'4 mib'8 re' re' do'16 fa' |
re'8 re' r4 r2 |
\ffclef "vsoprano" <>^\markup\character Deianira
r2 re'' |
sib'4 sib' mib'' mib''8 mib'' |
do''4 do'' r fa'' |
fa''8 fa'' fa'' fa'' re''4 re'' |
r sol'' mi''8 mi'' mi'' mi'' |
dod''4 dod'' r \original {
  dod''8 dod'' |
  dod''4 dod''8 si' dod''4 dod''8 dod'' |
  dod''4 dod''8 dod''16 re'' re''4 re''8 mi'' |
  re''8 re'' re'' re'' re'' re''16 re'' re''8 dod'' |
  mi''4 mi'' r
} fa''4~ |
fa'' re''8 re'' re''4 re'' |
re''8 re''16 do'' do''8 re'' sib'4 sib' |
r sib'8 la' la'2 |
r4 la'8 sol' sol'4 sol'8 sol' |
sol'4( fa') sib' sol'8 fa' |
re'4 re' r2 |
\ffclef "vsoprano" <>^\markup\character Paggio
la'4 r8 la' fad'4 fad'8 mi' |
fad'4 fad'8 fad' sol' sol' sol' fad'! |
la' la' r4
\ffclef "valto" <>^\markup\character Licco
r8 re' do' re' |
sib4 sib sib8 sib do' re' |
mib' mib' mib' mib' mib' mib' mib' re' |
fa' fa'16 fa' fa'8 mib' sol' sol' r16 fa' mib'! re' |
do'8 sol' do'8. do'16( sib2) |
\ffclef "vsoprano" <>^\markup\character Paggio
sib'8 fa' r16 fa' fa' sol' la'8 la'16 la' la'8 sib' |
do'' do'' r do'' sib' la' la' sol' |
sol'8. fa'16 fa'8 mi' sol' sol' r sol' |
do'' la' sib' sol' la' fa' sol' la' |
sib'4 la'8 sol' la'4( sol') fa'1 |
r4 fa' sol' |
la' sol'2 |
fa'4. sol'8 la' si' |
do''4 do'' sol' |
la' sol'2 la'4. la'8 sol'4 |
fa' mi'2 re'2. do' |
sol'4. sol'8 la'4 |
sib'4 la'4. la'8 |
sib'4 la'2 |
la'4 sib' la' |
sib' la'2 |
la'4 sib' la' |
sib' la'2 |
re''4 sib' la' |
sib' do''2 re'' re''4 |
do''4. do''8 sib'4 la' sol'2 |
la'2. sol'2 sol'4 |
la' sol'2 |
la'4 sol'2 |
la'2. sol' |
do''2 fa'4 |
sib'4. sib'8 la'4 sol'2. fa'1*3/4\fermata |
\ffclef "valto" <>^\markup\character Licco
r4 fa'8 la' fa' fa' fa' fa'16 sol' |
mib'4. re'8 re'8. re'16 re'4 |
\ffclef "vsoprano" <>^\markup\character Paggio
r4 sib'8 do'' lab'4 lab'8 sol' |
sol'4 sol'
\ffclef "valto" <>^\markup\character Licco
r8 sol' sol' sol' |
mib' re' mib'4. mib'8 re' mib' |
fa'4 fa'8 do' re'4 re' |
r8 re'16 re' sol'8 sol'16 re' mib'8 mib'16 mib' mib'8 re' |
fa'4 fa' r2 |
r8 fa' mib' fa' sol'4 fa' |
mib' mib'8 re' do'4 fa'8 mib' |
re'4 do' re' mi' |
fa'8. fa'16 fa'4 r sib |
sib sib sib sib |
sib2 sib4 do'8[ re'] |
mib'4 mib'8 re' do'4. sib8 sib1\fermata |
\ffclef "vsoprano" <>^\markup\character Paggio
sib'4 do''8 re'' do''4 sib' |
la' sib'8 do'' sib'4 la' |
sol'2 fa' |
la'4 fa'8 sib' sol'4 la' |
sib'2 sib'4 sib'8 la' |
sol'4 fad'8 sol' la'4 la' |
fad'4 fad' sol' fad'8 sol' |
mi'4 do'' sib' la' |
sib'2 sib'4 la' |
la'4. sol'8 sol'4 re'' |
sib'2 r4 sol' |
do''4. sib'8 re''4 re'' |
fa'4 fa' sol'2 |
la'4 la' sib' sib' |
la'2 la'4. sol'8 |
la'4 la' r re'' |
sib' la' la' sol' |
r2 r4 re'' |
sib' la' la' sol' |
sol'2 r4 la' |
mi'2 r |
r2 r4 la' |
mi'2 r |
r r4 la' |
sib' fa' fa'4. mi'8( re'1) |
r8 mi' sol'4 r8 sol' sol' fa' |
sol' sol' sol' sol' sol'4 sol'8 fa' |
la'4 la' r8 la' la' la'16 sol' |
sib'4 sib' r sib' |
sib'8 sib' sib' sib' sib'4 sib' |
sib' sib'8 sib' sib'4 sib' |
sib'8 sib' sib' la' do''4 do''8 fa' |
do''8 do'' do'' do'' do'' do'' do'' do''16 sib' |
re''8 re'' r4 r2 |
