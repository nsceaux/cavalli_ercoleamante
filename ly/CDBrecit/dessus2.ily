\clef "dessus" R1*30 |
r4 r8 do'' sol'4 |
fa' r8 sol' do''4~ |
do''8 sib' la' sol' fa'4 |
mi'8 do'' sol'4 r |
r8 la' do''4 sib' la'8 sol' fa' mi' re'4 |
r8 la' sol'4 la' si'8 re'' sol' re' sol' fa' mi'4 sol'4. sol'8 |
mi'4 r8 do'' fad' la' |
re' mib' re' sol'4 fad'8 |
sol'4. fad'8 sol' mi' |
fad' re'' sib' sol' r la' |
sol'4 r8 fad' la'4 |
r8 re'' sib'4 r8 do'' |
sib' do'' re'' re' mi' sol' |
re' la' sol'4 r8 la' |
sol'4 r8 do'' la'4 r8 re'' sib'4 r8 re'' |
sol' do'' la' fa' r sol' fad'4 r8 mib'' la'4 |
r8 do'' la'4 r8 fad' sol'4 r8 re' mi'4 |
r8 la' do''4 r8 sol' |
do''4 r8 sol' do''4 |
la'8 fa' do''4 la' r8 sol' do''4 r8 sol' |
fa'4. fa'8 do''4 |
re'' sol'4. fa'8 fa'2 mi'4 fa'1*3/4 |
R1*51 |
