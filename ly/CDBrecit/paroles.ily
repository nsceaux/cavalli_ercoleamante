Sei tu qual -- ch’in -- do -- vi -- no?

E ben fa -- mo -- so,
ch’in si -- mil gui -- sa a me a me nul -- la nul -- la è nas -- co -- so.

Ah cru -- do, ah di -- sle -- a -- le,
ah tra -- di -- to -- re, in -- gra -- to,
ah scel -- le -- rato,
\pygmalion {
  ed em -- pio.
}
\original {
  ed em -- pio
  dell’ a -- mor co -- niu -- ga -- le
  tra noi tan -- to giu -- ra -- to.
  Qui dun -- que hai scel -- to il luo -- go a far -- ne scem -- pio?
}
Ah __ De -- ia -- ni -- ra o -- gni ris -- tor dis -- pe -- ra,
ch’a mo -- rir ch’a mo -- rir di do -- lor sei des -- ti -- na -- ta.

Che? co -- tes -- ta stra -- nie -- ra
anch’ es -- sa è in -- na -- mo -- ra -- ta?

Co -- sì mi di -- ce, ma d’a -- mor ben ve -- ro,
co -- me sag -- gio io non cre -- do,
ch’a gli uo -- min, po -- co, ed al -- le don -- ne un ze -- ro.

Bas -- ta per ques -- ta cor -- te ogn’ or vo -- la -- re
si ve -- de un sì gran nu -- me -- ro d’a -- mo -- ri,
che non ab -- bia -- mo a fa -- re,
che ne ven -- gan di fuo -- ri.
A -- ma Hyl -- lo Io -- le ri -- a -- ma -- to, e l’a -- ma
Er -- co -- le as -- sai mal -- vis -- to, a -- ma Ni -- can -- dro
Li -- co -- ri, e que -- sta O -- res -- te, e O -- res -- te O -- lin -- da,
e O -- lin -- da, e Ce -- lia scal -- tre
a -- man le gem -- me, e l’o -- ro,
e Ni -- so, ed A -- li -- do -- ro a -- man a -- man cent’ al -- tre.

E per -- ché ha in o -- dio I -- o -- le
Er -- co -- le?

Per -- ché uc -- ci -- se Eu -- ty -- ro.

Ed a -- ma il fi -- glio poi di chi gli uc -- ci -- se il pa -- dre?
Ha la pian -- ta in or -- ro -- re, ed a -- ma il frut -- to?
Che vuoi gio -- car ch’io so
la ra -- gion che di ciò
el -- la in sé co -- va -- ne?
Un d’es -- si è trop -- po a -- dul -- to, e l’al -- tro è gio -- va -- ne

Fin da prin -- ci -- pio Io -- le ar -- dea per Hyl -- lo
on -- de per com -- pia -- cer -- la
le già da -- te pro -- mes -- se
del -- le noz -- ze di lei ri -- tol -- se Eu -- ty -- ro
ad Er -- co -- le, ch’al fin sì mal sof -- fril -- lo,
ch’u -- na tal dal -- la fi -- glia o -- pra gra -- di -- ta
all’ in -- fe -- li -- ce
all’ in -- fe -- li -- ce re cos -- tò cos -- tò cos -- tò la vi -- ta.
E tu, ch’il tut -- to sa -- i
non sai, ch’Er -- col’ m’at -- ten -- de? e ch’e -- gli è a -- man -- te?
E che fra quan -- ti ma -- i
ar -- do -- no al mon -- do d’a -- mo -- ro -- sa fiam -- ma
non v’è di pa -- zi -- en -- za u -- na sol dram -- ma.
