\clef "basse" sib,2 fa, |
fa,4 do fa,2~ |
fa, re |
<<
  \original { mib2 fa | }
  \pygmalion { mib4 do fa fa, | }
>>
sib,1 |
sib, |
sol, |
la,~ |
la,2 sib, |
sib,1 |
<<
  \original {
    la,1~ |
    la,~ |
    la,2 fa,~ |
    fa, sib, |
    la,1 |
  }
  \pygmalion { la,1 | }
>>
fa,1 |
fad,2 sol,~ |
sol, la,~ |
la, sib, |
sol,1~ |
sol,2 la, |
re1~ |
re2 mib |
re~ re |
sol,1 |
do |
re2 mib |
fa8 mib fa4 sib,2 |
sib,2 fa,4. sol,8 |
la,4 fa, sol, la, |
sib,4 re do sib, |
la, sol, fa mib |
re sib, do2 fa,1 |
fa2 mi4 |
fa mi2 |
fa4. mi8 re sol |
do2 do4 |
fa mi2 fa sol4 |
la mi fa sol2. do |
do2 la,4 |
sol, re2 |
sol,4 re2 |
re4 sol, re |
sol, re2 |
re4 sol, re |
sol, re2 |
re4 sol re |
sol fa2 sib sib,4 |
do2 do4 re mib2 |
re2. sol,2 do4 |
fa, mi,2 |
fa,4 mi,2 |
fa,2. do |
la,2 la,4 |
sib,2 sib,4 do2. fa,1*3/4 | \allowPageTurn
fa,1~ |
fa,2 sib, |
sib,1 |
mib |
do |
lab,2 sol, |
sol, do |
sib,1 |
sib,2 mib4 fa |
sol2 la |
sib4 la sol do |
fa2. re4 |
mib re mib re |
mib re8 do sib,4 la, |
sol, mib, fa,2 sib,1\fermata | \allowPageTurn
sib2 la4 sol |
fa mib re sib, |
do2 fa, |
fa4 re mib fa |
sib,2. re4 |
mib2 do |
re sol, |
do re |
sol,2. do4 |
re2 sol,~ |
sol,4 sol mib2 |
fa sib, |
re mi4 mi |
fa2 sol4 sol |
la2 sib |
la fa |
sol la |
sib fa |
sol la |
sib fad |
sol la |
sib fad |
sol la |
sib fad |
sol la re1 |
do~ |
do |
fa |
sib,~ |
sib, |
sib,~ |
sib,2 la,~ |
la,1 |
sib, |
