\key fa \major \midiTempo#100
\once\override Staff.TimeSignature.stencil = ##f
\time 4/4 s1*5 \bar "||" << \original s1*27 \pygmalion s1*23 >>
\measure 2/1 s1*2
\digitTime\time 3/4 \midiTempo#160 s2.*4
\measure 6/4 s1.
\measure 9/4 s2.*3
\measure 3/4 s2.*8
\measure 6/4 s1.*3
\measure 3/4 s2.*2
\measure 6/4 s1.
\measure 3/4 s2.
\measure 9/4 s2.*3 \bar "||"
\time 4/4 \midiTempo#100 s1*14
\measure 2/1 s1*2 \bar "||"
\time 4/4 s1*24
\measure 2/1 s1*2
\measure 4/4 s1*9 \bar "|."
