\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \keepWithTag #'voix1 \includeNotes "voix"
    >> \keepWithTag #'voix1 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'voix2 \includeNotes "voix"
    >> \keepWithTag #'voix2 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'voix3 \includeNotes "voix"
    >> \keepWithTag #'voix3 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'voix4 \includeNotes "voix"
    >> \keepWithTag #'voix4 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'voix5 \includeNotes "voix"
    >> \keepWithTag #'voix5 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'voix6 \includeNotes "voix"
    >> \keepWithTag #'voix6 \includeLyrics "paroles"
    \new Staff <<
      \global \keepWithTag #'voix6 \includeNotes "voix"
      \clef "petrucci-c3/bass"
      \origLayout {
        s4 s2.*5\break s2.*6\pageBreak
        s2.*6\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
