<<
  \tag #'(voix1 basse) {
    \clef "vsoprano" la'4 |
    re''2 mi''4 |
    fad''4. mi''8 re''4 |
    re'' dod''4.\melisma re''16[ mi'']\melismaEnd |
    re''4 re'' fad'' |
    mi''2 mi''4 |
    mi''4. mi''8 re''4 |
    re'' dod''2 |
    si'2 si'4 |
    R2. |
    r4 r la' |
    re'' mi''2 |
    fad''4.\melisma mi''8[ re'' do''] |
    re''4\melismaEnd la'2 |
    r la''4~ |
    la'' r sol''~ |
    sol'' fad''4. mi''8 |
    re''2 mi''4 |
    mi''2. |
    re''2 la''4~ |
    la'' r sol''~ |
    sol'' fad''4. mi''8 |
    re''2 mi''4 |
    mi''2. |
    re''2
  }
  \tag #'voix2 {
    \clef "vsoprano" re''4 |
    la'2 la'4 |
    la'4. la'8 la'4 |
    si' la'2 |
    la'4 la' la' |
    sold'2 sold'4 |
    lad'4 lad' si' |
    si'2 lad'4 |
    si'4. si'8 fad'4 |
    si'2 dod''4 |
    re''4.\melisma \ficta dod''8[ si' la'] |
    si'4\melismaEnd la'4. sol'8 |
    fad'2 fad'4 |
    fad'\melisma fad'' mi'' |
    re''\melismaEnd re'' re''~ |
    re'' r re''~ |
    re'' dod'' dod'' |
    si'2 si'4 |
    la'2. |
    la'2 re''4~ |
    re'' r4 re''~ |
    re'' dod'' dod'' |
    si'2 si'4 |
    la'2. |
    la'2
  }
  \tag #'voix3 {
    \clef "vsoprano" la'4 |
    la'2 la'4 |
    la'4. sol'8 fad'4 |
    mi'2 dod'4 |
    re' re' la' |
    mi'2 si'4 |
    lad'4 fad' si' |
    si'\melisma dod''\melismaEnd fad' |
    fad'2 fad'4 |
    r r fad' |
    si'2 dod''4 |
    re''\melisma dod''8[ re'' dod'' si'] |
    la'4. sol'8[ fad' mi'] |
    re'4\melismaEnd fad' la' |
    la' la' fad'~ |
    fad' r si'~ |
    si' la'4. sol'8 |
    fad'2 sol'4 |
    mi'2. |
    fad'2 r4 |
    fad'4 r si'~ |
    si' la'4. sol'8 |
    fad'2 sol'4 |
    mi'2. |
    fad'2
  }
  \tag #'voix4 {
    \clef "vsoprano" re''4 |
    re''2 dod''4 |
    re''4. re''8 re''4 |
    mi''4 mi''4.\melisma fad''16[ sol'']\melismaEnd |
    fad''4 re'' re'' |
    si'2 mi''4 |
    dod''4. dod''8 re''4 |
    fad''2 dod''4 |
    re''2 re''4 |
    R2.*2 |
    r4 r la' |
    re''2 mi''4 |
    fad''4.\melisma mi''8[ re'' do''] |
    re''4\melismaEnd re'' re''~ |
    re'' r si'~ |
    si' dod'' dod'' |
    re''2 re''4 |
    re''2 dod''4\melisma |
    re''2\melismaEnd r4 |
    re'' r si'~ |
    si' dod'' dod'' |
    re''2 re''4 |
    re''2 dod''4\melisma |
    re''2\melismaEnd
  }
  \tag #'voix5 {
    \clef "vsoprano" fad'4 |
    fad'2 mi'4 |
    re'4. la'8 la'4 |
    sol' mi'( la') |
    la' fad' fad' |
    sold' si'2 |
    fad'4 fad' fad' |
    fad'2 fad'4 |
    fad'2 fad'4 |
    R2.*3 |
    r4 la'2 |
    re'' mi''4 |
    fad'' re'' la'~ |
    la' r re'~ |
    re' la' la' |
    re'2 si4 |
    mi'4.\melisma fad'16[ sol'] la'4~ |
    la'\melismaEnd re' r |
    la' r re'~ |
    re' la' la' |
    re'2 si4 |
    mi'4.\melisma fad'16[ sol'] la'4~ |
    la'\melismaEnd re'
  }
  \tag #'voix6 {
    \clef "valto" re'4 |
    re'2 la4 |
    re'4. re'8 fad4 |
    sol la2 |
    re'4 re' re' |
    mi'2 mi'4 |
    fad' fad' fad |
    fad2 fad4 |
    si2 si4 |
    R2. |
    r4 r fad |
    si dod'2 |
    re'2. |
    re'2 la4 |
    re' re' fad~ |
    fad r sol~ |
    sol la la |
    la2 sol4 |
    la2. |
    re'2 fad4~ |
    fad r sol~ |
    sol la la |
    si2 sol4 |
    la2. |
    re'2
  }
>>
