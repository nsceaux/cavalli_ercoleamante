\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \keepWithTag #'voix1 \includeNotes "voix" \clef "treble"
    >> \keepWithTag #'voix1 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'voix2 \includeNotes "voix" \clef "treble"
    >> \keepWithTag #'voix2 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'voix3 \includeNotes "voix" \clef "treble"
    >> \keepWithTag #'voix3 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'voix4 \includeNotes "voix" \clef "treble"
    >> \keepWithTag #'voix4 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'voix5 \includeNotes "voix" \clef "treble"
    >> \keepWithTag #'voix5 \includeLyrics "paroles"
  >>
  \layout { }
}
