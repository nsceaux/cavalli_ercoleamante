\clef "viole2" R4*43 |
r4 | R2.*8 R4*7 |

sol4 |
la2 sol4 |
re'2 mib'4 |
re'2. |
re'4 sol8 la sib do' |
re' mi' fa' sol' la'4 |
re'4 fa'4. fa'8 |
mib'4 sib8 do' re'4 |
sib8 sol la4 sib |
sib2 sib4 |
la2 sol4 |
re'2 mib'4 |
re'2. si1\fermata |

sol4 |
la la4. fad8 fad4. sol8 la si |
do' re' mib'4 re'8 dod' dod'4. re'8 mi' fad' |
sol' sol si4 dod' re'2 sol4 |
re8 mi fa sol la4 sol fad8 sol do' sib |
la sib sol4 fad sol1\fermata |

R2*23 |
R1*5 |
R1*11 |
