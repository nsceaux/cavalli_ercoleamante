\clef "dessus" R4*43 |
sib'8 do'' |
re''4 do'' do''8 re'' re''4 re'' fa''8 sol'' |
la''4 sol'' sol''8 sol'' sol''4 fa'' sol''8 la'' |
sib''4 fa'' mi''8 fa'' re''2 sib'8 do'' |
re''4 do'' do''8 do'' do''4 sib' do''8 re'' |
mib''4 sib' la'8 sib' sol'1\fermata |

R4*41 |
r4 | R4*31 |

R2*23 |
%% Sinfonia originale
r8 sib''16 la'' sol'' fa'' mib'' re'' do'' do'' sib' do'' la'8. la'16 |
sol'2 r8 la''16 sol'' fa'' mi'' re'' do'' |
si'8 dod''16 re'' re''8. dod''16 re''2 |
r8 la'16 sib' do'' re'' mi'' fa'' sol''8 sib''16 la'' sol'' fa'' mib'' re'' |
do'' do'' sib' do'' la'8. la'16 sol'2 |
%% Sinfonia alternative
r8 sib''16 la'' sol'' fa'' mib'' re'' do'' do'' sib' do'' la'8. la'16 |
sol'2 r8 la''16 sol'' fa'' mi'' re'' do'' |
si'8 dod''16 re'' re''8. dod''16 re''2 |
r8 la'16 sib' do'' re'' mi'' fa'' sol''2 |
r8 do'''16 sib'' la'' sol'' fa'' mi'' re'' sib' la' sol' fad'8 re''16 do'' |
si' mi'' re'' mi'' dod''4 r16 si'' la'' si'' sold'' la'' si'' sold'' |
la''8 do'''!16 si'' la'' sol'' fa'' mi'' re'' mi'' fa'' sol'' la''4~ |
la''16 la'' sib'' la'' sol'' fa'' mib'' re'' mib''8. sol''16 fad'' sol'' la'' sib'' |
re'' mi'' fa'' sol'' mi''8. mi''16 re''2 |
r8 la'16 sib' do'' re'' mi'' fa'' sol''8 sib''16 la'' sol'' fa'' mib'' re'' |
do'' do'' sib' do'' la'8. la'16 sol'2 |
