\clef "basse" R4*43 R4*32

R4*41 R4*32

R2*23 |
%% Sinfonia originale
sol,4 sib, do re |
sol8 sib16 la sol la sib sol la4 re |
sol8 sol, la,4 re8 la16 sol fa mi re do |
sib,8 fa16 mi re do sib, la, sol,4 sib, |
do re sol,2 |
%% Sinfonia alternative
sol,4 sib, do re |
sol8 sib16 la sol la sib sol la4 re |
sol8 sol, la,4 re8 la16 sol fa mi re do |
sib,8 fa16 mi re do sib, la, sol,8 re'16 do' sib la sol fa |
mi fa sol mi fa8 la sib sib,16 do re8. re16 |
mi4 la, re mi |
la,2 sib,4 la, |
re,8 re mib4 do re |
sol, la, re8 la16 sol fa mi re do |
sib,8 fa16 mi re do sib, la, sol,4 sib, |
do re sol,2 |
