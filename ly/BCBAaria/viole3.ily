\clef "viole2" R4*43 |
r4 | R2.*8 R4*7 |
R4*41 |

re4 |
fa2 sol8 la sib4 la re' |
la do' sol la2 sol8 do' |
sib do' re' sol sol la fad4 re mi |
fa4 do sol8 fad la4 re8 sib la8. sol16 |
sol2 re4 re1\fermata |

R2*23 |
R1*5 |
R1*11 |
