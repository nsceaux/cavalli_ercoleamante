\clef "basse" R4*43 |
r4 | R2.*8 R4*7 |
R4*41 |

sib,4 |
la, do sol, re2 fa4 |
mi8 fad sol la sib4 mi la, re~ |
re8 mi sol4 mi8 la, la,4 sib,2 |
sib,4 la,2 re sol8 fa |
mib4 re la, si,1\fermata |

R2*23 |
R1*5 |
R1*11 |
