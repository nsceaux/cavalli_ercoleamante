\clef "basse"
sol,2 sol4 |
fa2 mib4 |
re2 do4 |
re2. |
sol,2 sol,4 |
re2 do4 |
sib,2 la,4 |
sol,2 re4 |
mib fa2 |
sib, sol4 |
fa2 mib4 |
re2 do4 |
re2. sol,1\fermata |
sol4 |
fa2 mib4 re2 re'4 |
do'2 sib4 la2 sib8 la |
sol2 la4 re2 sol4 |
fa2 mib4 re2 mib!8 re |
do2 re4 sol,1\fermata |

sol4 |
fa2 mib4 |
re2 do4 |
re2. |
sol,2 sol,4 |
re2 do4 |
sib,2 la,4 |
sol,2 re4 |
mib fa2 |
sib, sol4 |
fa2 mib4 |
re2 do4 |
re2. sol,1\fermata |

sol,4 |
fa,2 mib,4 re,2 re4 |
do2 sib,4 la,2 sib,8 la, |
sol,2 la,4 re,2 sol,4 |
fa,2 mib,4 re,4. re8 mib re |
do4 re re, sol,1\fermata | \allowPageTurn

fad1~ |
fad~ |
fad2 sol |
mi1 re |
re'2 si~ |
si1 |
do' |
do~ |
do2 re |
re mib |
do4 re |
%% Sinfonia originale
sol,4 sib, do re |
sol,4. sol8 la4 re |
sol8 sol, la,4 re do |
sib, la, sol, sib, |
do re sol,2 |
%% Sinfonia alternative
sol,4 sib, do re |
sol,4. sol8 la4 re |
sol8 sol, la,4 re do |
sib, la, sol, sib, |
do fa,8 la, sib,4 re |
mi la, re mi |
la,2 sib,4 la, |
re,8 re mib4 do re |
sol, la, re do |
sib, la, sol, sib, |
do re sol,2 |

  