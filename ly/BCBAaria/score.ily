\score {
  \new StaffGroupNoBar \with { \haraKiriFirst } <<
    \new StaffGroupNoBracket <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
      \new Staff << \global \includeNotes "partie1" >>
      \new Staff << \global \includeNotes "partie2" >>
      \new Staff << \global \includeNotes "basse1" >>
    >>
    \new StaffGroupNoBracket <<
      \new Staff << \global \includeNotes "viole1" >>
      \new Staff << \global \includeNotes "viole2" >>
      \new Staff << \global \includeNotes "viole3" >>
      \new Staff << \global \includeNotes "viole4" >>
    >>
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \modVersion {
        s4*43\break s4*32\break
        s4*41\break s4*32\break
        s1*11 s2\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
