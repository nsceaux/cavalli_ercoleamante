\piecePartSpecs
#`((violes #:score "score-violes")
   (dessus #:score-template "score-dessus-voix"
           #:music ,#{
           s4*43\break s4*32\break
           s4*41 s4*32
           s1*11 s2 \break #})
   (parties #:score-template "score-parties-voix"
           #:music ,#{
           s4*43\break s4*32\break
           s4*41\break s4*32\break
           s1*11 s2 \break #})
   (basse #:score "score-basse"))
