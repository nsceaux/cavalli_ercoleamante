\score {
  <<
    \new ChoirStaff \with { \haraKiriFirst } <<
      \new Staff \with { \tinyStaff } \withLyrics <<
        \global \includeNotes "voix"
      >> { \keepWithTag #'basse \includeLyrics "paroles" \set fontSize = -2 }
      \new Staff <<
        \global \includeNotes "dessus1"
        { s2.*12 s4*7 s4 s1.*4 s4*7 \startHaraKiri }
      >>
      \new Staff <<
        \global \includeNotes "dessus2"
        { s2.*12 s4*7 s4 s1.*4 s4*7 \startHaraKiri }
      >>
      \new Staff <<
        \global \includeNotes "partie1"
        { s2.*12 s4*7 s4 s1.*4 s4*7 \startHaraKiri }
      >>
      \new Staff <<
        \global \includeNotes "partie2"
        { s2.*12 s4*7 s4 s1.*4 s4*7 \startHaraKiri }
      >>
      \new Staff << \global \includeNotes "viole4" >>
      \new Staff << \global \includeNotes "basse1" >>
      \new Staff <<
        \global \includeNotes "basse"
        \includeFigures "chiffres"
        { % Aria
          s4*43\break
          % Rit
          s4*32\break
          % Aria
          s4*41\break
          % Rit
          s4*32\break
          % Aria
          s1*11 s2\break
          % Sinf
          s1*5\break
          % Sinf alt
        }
      >>
    >>
  >>
  \layout { }
}
