\clef "viole2" R4*43 |
sib8 sib |
la4 do' sol8 sol re'4 re' re'8 fa' |
mi'4 do' re'8 re' la'4 la8 fa' sib8 do' |
re'4 re' la'8 mi' fad'2 sib8 sib |
la4 do' sol8 sol re'4 re' sol8 re' |
sol4 mib' la8 re' si1\fermata |

R4*41 |

r4 | R4*31 |

R2*23 |
%% Sinfonia originale
re'4 re' mib' la |
sib re' la' la' |
re'8 si mi' la la2 |
r8 fa16 sol la sib do' re' sib4 re' |
la la si2 |
%% Sinfonia alternative
re'4 re' mib' la |
sib re' la' la' |
re'8 si mi' la la2 |
sib8 fa16 sol la sib do' re' sib4 re' |
sol'8 fa do'4 re'2 |
r4 mi'8 la la la' mi'4 |
mi4 la8. sol16 fa mi re8 mi4 |
la8 fad sol4 do' la |
sol mi8 la la2 |
r8 fa16 sol la sib do' re' sib4 re' |
la la si2 |
