\clef "viole1" r4 sib2 |
la4 sib la~ |
la sol4. sol8 |
sol4 fad4. sol8 |
sol4 sib4. do'8 |
re'4 la2 |
fa4 sib4. sib8 |
sib4 do'8 sol fad la |
sol4 la sib |
sib4. do'8 re'4 |
la2 la4 |
la sol2 |
sol4 fad4. sol8 sol1\fermata |

r4 | R2.*8 R4*7 |

re'4 |
re'4 la4. la8 |
la4 sol sol' |
sol'2 fad'4 |
sol'2 sol4 |
la re' mib' |
fa'4 sib do' |
sib sol sol'~ |
sol' do' fa' |
re'2 re'4 |
re' la4. la8 |
la4 sol sol' |
sol'2 fad'4 sol'1\fermata |

sib8 do' |
re'4 do' do'8 re' re'4 re' fa'8 sol' |
la'4 sol' sol'8 sol' sol'4 fa' sol'8 la' |
sib'4 fa' mi'8 fa' re'2 sib8 do' |
re'4 do' do'8 do' do'4 sib do'8 re' |
mib'4 sib la8 sib sol1\fermata | \allowPageTurn

R2*23 |
R1*5 |
R1*11 |
