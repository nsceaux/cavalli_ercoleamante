\key re \minor \midiTempo#120
\beginMark "Aria" \digitTime\time 3/4 s2.*12 \measure 7/4 s4*7 \bar "|."
\beginMark "Ritor[nello]" \digitTime\time 3/4 \measure 1/4 s4
\measure 6/4 s1.*4 \measure 7/4 s4*7 \bar "|."

\measure 1/4 s4 \measure 3/4 s2.*11 \measure 7/4 s4*7 \bar "|."
\beginMark "Ritor[nello]" \digitTime\time 3/4 \measure 1/4 s4
\measure 6/4 s1.*4 \measure 7/4 s4*7 \bar "|."

\time 4/4 \midiTempo#80 s1*3 \measure 2/1 s1*2
\measure 4/4 s1*6
\measure 2/4 s2
\beginMark "Sinf[onia]" \time 4/4 s1*5 \bar "|."

\break\beginMark "[Sinfonia alternative]" s1*5 \pygmalion s1*6 \bar "|."
