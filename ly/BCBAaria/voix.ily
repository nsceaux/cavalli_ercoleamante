\clef "vsoprano" <>^\markup\character Giunone
r4 r sib'8 do'' |
re''4. do''8 do'' do'' |
do''4 sib' sib'8 la' |
sib'4 la' la'8 sib' |
sol'2 re''8 mi'' |
fa''4. mib''8 mib'' re'' |
re''4 do''8 do'' do'' re'' |
mib''4 mib''8 sib' sib' do'' |
reb''4 do''( reb'') |
sib' r sib'8 do'' |
re''4. do''8 do'' do'' |
do''4 sib' sib'8 la' |
sib'4 la' la'8 sib' sol'1\fermata |
R4*32 |
%% 2e strophe
<>^\markup\character Giunone
sib'8 do'' |
re''4. do''8 do'' do'' |
do''4 sib' sib'8 la' |
sib'4 la' la'8 sib' |
sol'2 re''8 mi'' |
fa''4. mib''8 mib'' re'' |
re''4 do''8 do'' do'' re'' |
mib''4 mib''8 sib' sib' do'' |
reb''4 do''( reb'') |
sib' r sib'8 do'' |
re''4. do''8 do'' do'' |
do''4 sib' sib'8 la' |
sib'4 la' la'8 sib' sol'1\fermata |
R4*32 |
%%
<>^\markup\character Giunone r4 re''8 re'' la'4 la'8 la' |
la'8. la'16 la'8 sol' la' la'16 la' la'8 sib' |
do'' do'' do'' re'' sib'4 sib'8 fad' |
sol'2.( la'4) la'1 |
re''4 fa'' r8 sol'' re''8. re''16 |
re''8 sol'' re'' re''16 re'' re'' re'' re'' re'' re''8 re''16 mib'' |
do''4 do'' r8 do'' do'' si' |
do''8 do'' do'' do'' do''4 do''8 re'' |
sib'4 mib''8 sib'16 la' la'4 la' |
re''8 re''16 re'' re''8 re'' sol' sol''16[\melisma fa'' mib'' re'' do'' sib'] |
la'8\melismaEnd sib'16 do'' sib'8. la'16( sol'4) r
r2 R1*5 R1*10
