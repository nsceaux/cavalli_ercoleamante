\clef "basse" fa,2. |
do |
mi |
fa4 sol2 |
do8 si, do re mi do |
fa mi fa sol la fa |
sib4 sib,8 do re4 |
sib, do2 fa,1*3/4\fermata |
