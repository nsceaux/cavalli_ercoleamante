\clef "dessus" r8 do''' sib'' la'' sol'' fa'' |
mi'' la'' sol'' fa'' mi'' re'' |
do'' re'' mi'' fa'' sol''4 |
fa''8 mi'' re''4. re''8 |
mi'' fa'' mi'' re'' do'' sib' |
la'4. sib'8 do''4 |
sib'8 do'' re'' mi'' fa''4 |
fa'' fa''4. mi''8 fa''1*3/4\fermata |
