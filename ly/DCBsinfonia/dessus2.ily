\clef "dessus" r8 la'' sol'' fa'' mi'' re'' |
do'' fa'' mi'' re'' mi'' fa'' |
sol''4. fa''8 mi''4 |
re''8 do'' do''4. si'8 |
do''2. |
r8 sib'' la'' sol'' fa'' mi'' |
re''4. do''8 sib' do'' |
la' sib' sol'4. sol'8 la'1*3/4\fermata |
