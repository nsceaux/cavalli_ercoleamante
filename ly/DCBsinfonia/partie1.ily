\clef "viole1" la'2. |
sol' |
mi'2. |
la'4 re'2 |
do'8 re' mi' fa' sol'4 |
fa'2 fa'4 |
fa'2. |
fa'4 do'' do' do'1*3/4\fermata |
