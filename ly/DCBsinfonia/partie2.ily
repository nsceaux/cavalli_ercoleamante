\clef "viole2" do'2. |
do' |
do' |
re'4 sol'2 |
sol'4. fa'8 mi' re' |
do' re' do' sib la4 |
re'2. |
re'4 do' sol' fa'1*3/4\fermata |
