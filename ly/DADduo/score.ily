\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \keepWithTag #'venere \includeNotes "voix"
    >> \keepWithTag #'venere \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'ercole \includeNotes "voix"
    >> \keepWithTag #'ercole \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*4\pageBreak
        s1*5\break s1*5\break s1*4\pageBreak
        s1*4 s2.*5\break s2.*11\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
