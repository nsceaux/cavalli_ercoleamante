\clef "basse" sib,4 sib8 la sol4 sib |
mib8 fa sol la sib4 la |
sol2 fa |
sib, fa |
sib fad |
sol fa |
mib re |
do4 sib, do2 |
fa,2 sib,8 sib sib la |
sol4 sib fa2 |
do'4 sol sib2 |
lab sol |
mi1 |
mi2 fa |
fa4. sol8 la4. sib8 |
do'4. sib8 la4. sol8 |
fa4. mi8 re4. do8 |
si,4 do8 fa, sol,2 |
do2 fa,4 fa8 mib |
re2 sib, |
do4 la,8 sib, do2 fa,1 |
fa2 fa4 |
sib2. la |
sol fa |
mib re |
sol |
do |
re4 re' sib |
do' sol2 |
re'2. |
si do' |
la sib4 sib sol |
lab4 mib2 |
sib2. |
sol lab |
fa |
sol4 sol re mib2 sib,4 |
fa2. |
sib,4 sib fa |
sol re mib fa2. sib,1*3/4\fermata |
