<<
  \tag #'(venere basse) {
    \clef "vbas-dessus" <>^\markup\character Venere R1 |
    r8 fa'' fa'' mib'' re''4 fa'' |
    sib'8 do'' re'' mib'' fa''4 mib'' |
    re''2 do''8 la' sib' do'' |
    fa'8[ sol'] la'[ sib'] do''4. do''8 |
    mib''2 la' |
    do'' sib'4 sib'8 sib' |
    la'4 sol' sol'4. do''8 |
    la' fa'' fa'' mib'' re''4 fa'' |
    sib'8 do'' re'' mi'' fa''4 do'' |
    mib'' mib''8 mib'' re''4. mib''8 |
    do''4. si'8 re''2 |
    r2 do''4 do'' |
    r8 do'' sib' la' la'4 la' |
    r2 r8 fa' fa' sol' |
    la' la' la' sib' do''4 do''8.[\melisma sib'16] |
    la'8.[ sib'16 la'8. sol'16] fa'8.[ sol'16 fa'8. mi'16] |
    re'4\melismaEnd mi'8 la' sol'4. sol'8 |
    mi'4 do''8.[\melisma sib'16] la'8.[ sib'16 la'8. sol'16] |
    fa'8.[ sol'16 la'8. fa'16] sib'8.[ la'16 sib'8. la'16] |
    sol'4\melismaEnd fa'8 sol' sol'4. fa'8 fa'1 |
    R2. |
    r4 sib' sib' fa'' fa'' fa'' |
    mib''2. re''4. mib''!8 re''4 |
    re''2 do''4( re''2) re''4 |
    si'2. |
    r4 r do'' |
    la' la' re'' |
    do'' sib' do'' |
    la'2 la'4( |
    sol'2) sol''4 mi''2. |
    r4 r fa'' re'' re'' mib'' |
    do'' mib'' mib'' |
    mib''2 re''4( |
    mib''2) mib''4 do''2. |
    r4 r re'' |
    sib' sib' re'' sol' la' sib' |
    sib'2 la'4( |
    sib') re'' do'' |
    mib'' sib' do'' re''( do''2) sib'1*3/4\fermata |
  }
  \tag #'ercole {
    \clef "vbasse" <>^\markup\character Ercole r8 sib sib la sol4 sib |
    mib8 fa sol la sib4 la |
    sol2 fa8 re mib fa |
    sib,8[ do] re[ mi] fa4. fa8 |
    sib2 fad |
    sol fa4 fa8 fa |
    mib2 re4 re8 re |
    do4 sib, do4. do8 |
    fa,2 r8 sib sib la |
    sol4 sib fa8 sol la sib |
    do'4 sol sib sib8 sib |
    lab4. sol8 sol2 |
    r2 mi4 mi |
    r8 mi mi mi fa4 fa |
    r8 fa fa sol la la la sib |
    do'4 do'8.[\melisma sib16] la8.[ sib16 la8. sol16] |
    fa8.[ sol16 fa8. mi16] re8.[ mi16 re8. do16] |
    si,4\melismaEnd do8 fa, sol,4. sol,8 |
    do2 r4 fa8.[\melisma mib16] |
    re8.[ mib16 re8. do16] sib,8.[ do16 re8. sib,16] |
    do4\melismaEnd la,8 sib, do4. do8 fa,1 |
    r4 fa fa |
    sib sib sib la2. |
    sol2 sol4 fa4. sol8 fa4 |
    mib2. re |
    r4 r sol |
    mi2. |
    r4 re' sib |
    do' sol sol |
    re' re' re' |
    si2. r4 r do' |
    la2. r4 sib sol |
    lab mib mib |
    sib sib sib |
    sol2. r4 r lab |
    fa2. |
    r4 sol re mib mib sib, |
    fa2. |
    sib,4 sib fa |
    sol re mib fa2. sib,1*3/4\fermata |
  }
>>
