\key fa \major \midiTempo#120
\time 4/4 s1*20 \measure 2/1 s1*2
\digitTime\time 3/4 \midiTempo#160 s2.
\measure 6/4 s1.*3
\measure 3/4 s2.*5
\measure 6/4 s1.*2
\measure 3/4 s2.*2
\measure 6/4 s1.
\measure 3/4 s2.
\measure 6/4 s1.
\measure 3/4 s2.*2
\measure 9/4 s2.*3 \bar "|."
\endMarkSmall "Segue sub[ito] la Sinf[onia]"
