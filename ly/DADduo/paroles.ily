E per -- ch’A -- mor non fa,
ch’a l’a -- mo -- ro -- sa schie -- ra
sol del -- le gio -- ie sue sia dis -- pen -- sie -- ra
\tag #'ercole { o ra -- gio -- ne, }
o ra -- gio -- ne, o pie -- tà?
E per -- ché cru -- del -- tà
per -- ch’il ri -- gor,
in guar -- da o -- gn’or \tag #'(basse venere) { o -- gn’or } le ha -- vrà?
Dun -- que per in -- vo -- lar -- le ogn’ ar -- te an -- cor
ogn’ ar -- te an -- cor
le -- ci -- ta al -- trui sa -- rà
le -- ci -- ta al -- trui sa -- rà:
d’un ar -- den -- te de -- sio giun -- ger \tag #'ercole { giun -- ger } al se -- gno
sì, sì, \tag #'(venere basse) { sì, sì, }
gio -- co gio -- co è d’in -- ge -- gno
sì, sì, sì, sì,
gio -- co gio -- co è d’in -- ge -- gno
sì, sì, sì, sì,
gio -- co gio -- co è d’in -- ge -- gno
gio -- co gio -- co è d’in -- ge -- gno.
