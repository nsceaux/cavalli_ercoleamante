Ahi che pe -- na è ge -- lo -- si -- a __
ad un’ al -- ma in -- na -- mo -- ra -- ta
ch’a i sos -- pet -- ti ab -- ban -- do -- na -- ta
te -- me ogn’ or sor -- te più ri -- a.
Ahi che pe -- na è ge -- lo -- si -- a. __

Ad Al -- ci -- de al -- lor ch’I -- o -- le
cru -- del -- men -- te in ver me pi -- a,
di spe -- rar al -- fin con -- ces -- se;
io cre -- dei, che m’uc -- ci -- des -- se,
so -- lo il suon so -- lo il suon di tai pa -- ro -- le, __
ma il mo -- rir __ ma il mo -- rir __ man -- co man -- co duol fi -- a.

Ahi che pe -- na è ge -- lo -- si -- a __
ad un’ al -- ma in -- na -- mo -- ra -- ta
ch’a i sos -- pet -- ti ab -- ban -- do -- na -- ta
te -- me ogn’ or sor -- te più ri -- a.
Ahi che pe -- na è ge -- lo -- si -- a. __

Ma che veg -- gio? ec -- co un mes -- so,
che vie -- ne a drit -- ta vo -- ga, è il Pag -- gio? è des -- so.
