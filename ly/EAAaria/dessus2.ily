\clef "dessus" mi''2. re''4 do''2. si'4 |
la' si' do''1 si'2 do''1\fermata |
si'2. la'4 sol'2. re''4 |
do''2 re''4 si' la'1 si'\fermata |
r4 do'' re'' |
do'' si'2 la'4 do''2 |
do'' si'4 do'' do'' si' |
la' mi''2 la'4 do''2 |
do'' si'4 do''1*3/4\fermata |
R2.*19 |
r2*3/2 r4 do'' re'' |
do'' si'2 la'4 do''2 |
do'' si'4 do'' do'' si' |
la' mi''2 la'4 do''2 |
do'' si'4 do''1*3/4\fermata |
R2.*49 |
r2*3/2 r4 do'' re'' |
do'' si'2 la'4 do''2 |
do'' si'4 do'' do'' si' |
la' mi''2 la'4 do''2 do'' si'4 |
do''2 r |
R1*5 |
