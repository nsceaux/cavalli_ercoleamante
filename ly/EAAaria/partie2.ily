\clef "viole2" mi'2. si4 do'2 mi'4 sol' |
do'4. re'8 mi' fa' sol'4 re'1 sol\fermata |
si2. re'4 sol2. si4 |
sol4. la8 si do' re'4 la1 sol\fermata |
r4 mi' si |
do' sol'2 do'4 mi'2 |
la4 re'2 sol4 sol si |
do' do'2 fa'4 do' mi' |
re'2. do'1*3/4\fermata |
R2.*19 |
r2*3/2 r4 mi' si |
do' sol'2 do'4 mi'2 |
la4 re'2 sol4 sol si |
do' do'2 fa'4 do' mi' |
re'2. do'1*3/4\fermata |
R2.*49 |
r2*3/2 r4 mi' si |
do' sol'2 do'4 mi'2 |
la4 re'2 sol4 sol si |
do' do'2 fa'4 do' mi' re'2. |
do'2 r |
R1*5 |
