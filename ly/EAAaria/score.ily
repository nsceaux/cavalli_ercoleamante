\score {
  \new StaffGroupNoBar \with { \haraKiriFirst } <<
    \new StaffGroupNoBracket <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
      \new Staff << \global \includeNotes "partie1" >>
      \new Staff << \global \includeNotes "partie2" >>
    >>
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*10 s2.*9\pageBreak
        s2.*14\break s2.*16\break s2.*15\break s2.*15\break s2.*14\pageBreak
        s2.*13 s1\break
      }
      \modVersion { s1*10\break s2.*9\break }
    >>
  >>
  \layout { }
  \midi { }
}
