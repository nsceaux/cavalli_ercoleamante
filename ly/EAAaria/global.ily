\key do \major \midiTempo#240
\beginMark "Sinf[onia]"
\time 4/4 \measure 2/1 s1*2
\measure 3/1 s1*3
\measure 2/1 s1*2
\measure 3/1 s1*3
\beginMark "Ritor[nello]" \midiTempo#160
\digitTime\time 3/4 s2.
\measure 6/4 s1.*4 \bar "|."
\customTime\markup\vcenter { \musicglyph#"timesig.C44" \musicglyph#"three" }
\measure 6/4 s1.*4
\measure 9/4 s2.*6
\measure 3/4 s2.
<< \measure 6/4 s1.*7 { s2.*5 \beginMark "Ritor[nello]" } >> \bar "|."
\digitTime\time 3/4 s2.*2
\measure 6/4 s1.*6
\measure 3/4 s2.*2
\measure 6/4 s1.*7
\measure 3/4 s2.*2
\measure 6/4 s1.*3
\measure 3/4 s2.
<< \measure 6/4 s1.*8 { s2.*11 \beginMark "[Ritornello]" } >>
\measure 9/4 s2.*3
\time 4/4 \midiTempo#120 s1*4 \measure 2/1 s1*2 \bar "|."

