\clef "basse" do'2. si4 la2. sol4 |
fa2 mi re1 do\fermata |
sol2. fa4 mi2. si,4 |
do2 si, re1 sol,\fermata |
r4 do si, |
la, sol,2 fa,4 mi,2 |
re,2. do,4 do sol, |
la, mi,2 fa,4 mi,2 |
sol,2. do1*3/4\fermata |
do4 do' si la2 sol4 |
fa2 mi4 re2. |
do2 si,4 do2 si,4 |
do si,2 la,2. sol,4 sol fa mi re2 do4 si,2 |
la,2 sol,4 fa,2. mi, |
fa, |
fa,4 sol,2 do4 do' si |
la2 sol4 fa2 mi4 |
re2. do4 do si, |
la, sol,2 fa,4 mi,2 |
re,2. do,4 do sol, |
la, mi,2 fa,4 mi,2 |
sol,2. do1*3/4\fermata |
r4 do' si |
do'2 si4 |
la2 sol4 fa2. |
sol r4 do' si |
la2 sol4 fa2 la4 |
re2. mi |
fa mi |
do r4 re re mi2. |
fad4 fad fad |
sold2. r4 la la |
sol2. fa2 mi4 |
re2 dod mi |
la,2. re |
sol, do |
fa, mi, |
fa, sol, |
do4 do' si |
la2 sol4 |
fa2 mi4 re2. |
do2 si,4 do2 si,4 |
do si,2 la,2. |
sol,4 sol fa |
mi re2 do4 si,2 |
la, sol,4 fa,2. |
mi, fa, |
fa,4 sol,2 do4 do' si |
la2 sol4 fa2 mi4 |
re2. do4 do si, |
la, sol,2 fa,4 mi,2 |
re,2. do,4 do sol, |
la, mi,2 fa,4 mi,2 sol,2. |
do2 fa |
re1 |
re |
re2 sol, |
r4 r8 sol, la,2 re,1\fermata |

