\clef "viole1" sol'2. sol'4 la'2. mi'4 |
fa'2 sol'4 mi' la'2 fa' sol'1\fermata |
re'2. la4 mi'2. sol'4 |
mi'2 re' re'1 re'\fermata |
r4 sol' sol' |
la' mi'2 fa'4 sol'2 |
la'4 fa'2 sol'4 sol' sol' |
mi' mi'2 do'4 mi' sol' |
sol'2. sol'1*3/4\fermata |
R2.*19 |
r2*3/2 r4 sol' sol' |
la' mi'2 fa'4 sol'2 |
la'4 fa'2 sol'4 sol' sol' |
mi' mi'2 do'4 mi' sol' |
sol'2. sol'1*3/4\fermata | \allowPageTurn
R2.*49 |
r2*3/2 r4 sol' sol' |
la' mi'2 fa'4 sol'2 |
la'4 fa'2 sol'4 sol' sol' |
mi' mi'2 do'4 mi' sol' sol'2. |
sol'2 r |
R1*5 |
