\clef "dessus" sol''2. sol''4 fa''2 mi'' |
la'' sol'' fa''1 mi''\fermata |
re''2. re''4 do''2 si' |
mi''4 fad'' sol''1 fad''!2 sol''1\fermata |
r4 mi'' sol'' |
fa'' mi''2 la''4 sol''2 |
fa''2. mi''4 mi'' re'' |
do'' sol''2 fa''4 sol''2 |
re''2. mi''1*3/4\fermata |
R2.*19 |
r2*3/2 r4 mi'' sol'' |
fa'' mi''2 la''4 sol''2 |
fa''2. mi''4 mi'' re'' |
do'' sol''2 fa''4 sol''2 |
re''2. mi''1*3/4\fermata |
R2.*49 |
r2*3/2 r4 mi'' sol'' |
fa'' mi''2 la''4 sol''2 |
fa''2. mi''4 mi'' re'' |
do'' sol''2 fa''4 sol''2 re''2. |
mi''2 r |
R1*5 |
