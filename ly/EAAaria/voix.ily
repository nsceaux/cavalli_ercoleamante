\clef "vtenor" R1*10 |
R2.*9 |
<>^\markup\character Hyllo
r4 mi' re' fa' mi'2 |
la4 si do' do'2 si4( |
do') sol sol la sol2 |
la4 si2 do'2. |
si4 si re' do' si2 la4 sol2 |
fa2 mi4 la si2 do'2. |
do'4. re'8 mi'4 |
mi'( re'2) do'4 mi' re' |
fa' mi'2 la4 si do' |
do'2 si4( do'2.) |
R2.*8 |
r4 do' re' |
mi' do'2 |
do'4 re'4. mi'8 fa'2. |
re'4 sol' fa' mi'2 re'4 |
do' mi'2 la la4 |
si re'2 sold do'4 |
la2 sold4 si2 si4 |
r la la si2. |
dod'4 dod' dod' |
re'2 re'4 |
r mi' mi' fa'2. |
r4 mi' mi' re'2 do'4 |
sib2 la la4. sold8( |
la4) do'4. do'8 do'2( sib4) |
r4 sib4. sib8 sib2( la4) |
r4 la2~ la4 sol2 |
do'4 re' mi' do'2 si4 |
r mi' re' |
fa' mi'2 |
la4 si do' do'2 si4( |
do') sol4 sol la sol2 |
la4 si2 do'2. |
si4 si re' |
do' si2 la4 sol2 |
fa mi4 la si2 |
do'2. do'4. re'8 mi'4 |
mi'4( re'2) do'4 mi' re' |
fa' mi'2 la4 si do' |
do'2 si4( do'2.) |
R2.*7 |
r4 mi'8 mi' la la r4 |
r re'8 fa' re'4 re'8 re' |
re' re' re' dod' re'4 re' |
r re' sib sib |
r r8 re' la4 la r1\fermata |
