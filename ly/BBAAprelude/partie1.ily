\clef "viole1" fa' sol' fa' |
fa' mi'2 |
sol'4 do' mi' fa' do' re' |
do' re' la sib8 do' re' mi' fa'4 |
re' mi'2 fa'2. |
do'4 sib do' |
re' sib sib'~ |
sib'8 la' sol' fa' mi'4 |
fad'2. |
