\clef "viole2" la2 re'4 |
re' la2 |
sol4 la sib fa2 la4 |
sol sib do' sol2 do'4 |
sib4 sol2 la2. |
la4 sol do' |
sib re'2 |
re' dod'4 |
re'2. |
