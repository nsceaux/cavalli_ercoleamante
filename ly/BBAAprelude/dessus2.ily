\clef "dessus2" la'2 la'4 |
la'2. |
re'4 fa' sol' do' fa'2 |
sol'4 fa' mi' sol'8 la' sib'4 la' |
sol'2 sol'4 fa'2. |
fa'4 re' fa' |
fa' sib'4. do''8 |
re''4 sib' la' |
la'2. |
