\clef "dessus" re''4 mi'' re'' |
re'' dod''2 |
re''4 do'' sib' sib' la'2 |
do''4 sib' la' re''4. re''8 do''4 |
re''4 sib'4. la'8 la'2. |
la'4 sib' la' |
re''4. mi''8 fa''4 |
sol'' mi''2 |
re''2. |
