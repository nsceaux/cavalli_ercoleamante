\clef "basse" re dod re |
la2 la,4 |
sib, la, sol, fa,2 fa4 |
mi re do sib, sol, la, |
sib, do2 fa,2. |
fa4 sol la |
sib2 sol4~ |
sol4 la2 |
re2. |
