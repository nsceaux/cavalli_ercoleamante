\clef "dessus" \original {
  R1*15 |
  r2 r8 la' la' la' |
  re''2 r8 si' si' si' |
  mi''2 r8 la' la' la' |
  re''4 re'' re''4. dod''8 re''1\fermata |
}
R2.*8 R1*3 R2.*11 |
re''4 mi'' fad'' si'2 si'4 |
fad'' sol'' la'' re''2 sol''4 |
dod''2 re'' re''4. dod''8 re''1*3/4\fermata |
