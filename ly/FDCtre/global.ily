\key do \major \midiTempo#160
\original {
  \beginMark "A 3" \time 4/4 \measure 2/1 s1*2
  \measure 4/4 s1*11 \measure 2/1 s1*2 \bar "|."
  \beginMark "Rit[ornello]" \time 4/4 s1*3 \measure 2/1 s1*2 \bar "|."
}
\beginMark "A 2" \digitTime\time 3/4 \measure 6/4 s1.
\measure 3/4 s2.*2
\measure 6/4 s1.
\measure 3/4 s2.*2
\time 4/4 s1*3
\digitTime\time 3/4 s2.
\measure 9/4 s2.*3
\measure 6/4 s1.
\measure 3/4 s2.*2
\measure 6/4 s1.
\measure 3/4 s2. \bar "|."
\beginMark "Sinf[onia]" \digitTime\time 3/4 \measure 6/4 s1.*2 \measure 9/4 s2.*3 \bar "|."
