\score {
  <<
    \new StaffGroup \with { \haraKiriFirst } <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
      \new Staff << \global \includeNotes "partie1" >>
      \new Staff << \global \includeNotes "partie2" >>
      \new Staff << \global \keepWithTag #'basse \includeNotes "basse" >>
    >>
    \new ChoirStaff \with { \haraKiriFirst } <<
      \new Staff \with {
        instrumentName = \markup\character Iole
        shortInstrumentName = \markup\character Io
      } \withLyrics <<
        \global \keepWithTag #'iole \includeNotes "voix"
      >> \keepWithTag #'iole \includeLyrics "paroles"
      \original\new Staff \with {
        instrumentName = \markup\character Deianira
        shortInstrumentName = \markup\character De
      } \withLyrics <<
        \global \keepWithTag #'deianira \includeNotes "voix"
      >> \keepWithTag #'deianira \includeLyrics "paroles"
      \new Staff \with {
        instrumentName = \markup\character Hyllo
        shortInstrumentName = \markup\character Hy
      } \withLyrics <<
        \global \keepWithTag #'hyllo \includeNotes "voix"
      >> \keepWithTag #'hyllo \includeLyrics "paroles"
    >>
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*7\pageBreak
        s1*8\break s1*5\pageBreak
        s2.*8\break s1*3 s2.*4\break s2.*7\pageBreak
      }
      \modVersion {
        \original { s1*15\break s1*5\break } s2.*8 s1*3 s2.*11\break
      }
    >>
  >>
  \layout {
    indent = \largeindent
    short-indent = 10\mm
  }
  \midi { }
}
