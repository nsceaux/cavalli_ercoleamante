\clef "dessus" \original {
  R1*15 |
  r8 re'' re'' re'' la''2 |
  r8 fad'' fad'' fad'' si''2 |
  r8 sol'' sol'' sol'' la''4. sol''8 |
  fad''4 mi'' mi''2 fad''1\fermata |
}
R2.*8 R1*3 R2.*11 |
r2*3/2 mi''4 fad'' sol'' |
dod''2 dod''4 sol'' la'' si'' |
mi''2 fad'' mi''4. re''8 re''1*3/4\fermata |
