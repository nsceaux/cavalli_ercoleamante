\original {
  %% a 3
  s1*3 s4 <6> <_+>2 <\markup\dynamic\fontsize#-2 { 3 4 3 } >1 s1*7
  s4 <6> <_+>2 <\markup\dynamic\fontsize#-2 { 3 4 3 } >1 s1
  %% Rit
  <_+>1 s1*4
}
%% a 2
<_+>2. s2.*3 <_+>1. s2.*2 s1*3
s2 <_->4 s2.*5 <6>2. <6+> s2.*3
%% Rit
s2. <6> <_+> <6>
