\original {
  Oh Dè -- a co -- me n’ar -- re -- quii.
  
  Ch’a i det -- ti tuoi
  non li -- ce a noi
  \tag #'(iole basse) { fe -- de ne -- gar fe -- de ne -- gar }
  \tag #'deianira { fe -- de ne -- gar __ }
  \tag #'hyllo { fe -- de ne -- gar }
  né os -- se -- qui
  oh Dè -- a co -- me n’ar -- re -- quii.
}
\tag #'(iole basse hyllo) {
  Che dol -- ci gio -- ie
  che dol -- ci gio -- ie oh Dè -- a
  ver -- si nel nos -- tro se -- no,
  il ciel be -- ni -- gno a pie -- no
  che più dar
  che più dar ne po -- te -- a?
  Che dol -- ci gio -- ie
  che dol -- ci gio -- ie oh Dè -- a.
}
