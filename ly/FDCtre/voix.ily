<<
  \tag #'(iole basse) \clef "vsoprano"
  \tag #'deianira \clef "vsoprano"
  \tag #'hyllo \clef "vtenor"
>>
\original <<
  \tag #'(iole basse) {
    fad''1 si'2 si' |
    r4 fad' sol' la' |
    sol'2.\melisma fad'8[ sol'] |
    la'4 sol'8[ fad'] sol'4. fad'8\melismaEnd |
    fad'8 la' la' la' re''2 |
    r8 si' si' si' mi''2 |
    do''4 do''8 do'' fa''2 |
    mi''4 mi''8 re'' do''4 si'8[ do''] |
    si'4. si'8\melisma la'2\melismaEnd |
    r4 fad'' si' si' |
    r4 fad' sol' la' |
    sol'2.\melisma fad'8[ sol'] |
    la'4 sol'8[ fad'] sol'4.\melismaEnd sol'8\melisma fad'1\fermata\melismaEnd |
  }
  \tag #'deianira {
    la'1 sol'2 sol' |
    r4 re' mi' fad' |
    mi'2.\melisma re'4 |
    dod' re'2 dod'4\melismaEnd |
    re'2 r8 re' re' re' |
    sol'2 r8 mi' mi' mi' |
    la'2 fa'4 fa'8 fa' |
    do''8.[\melisma re''16 do''8 si'] la'4\melismaEnd la' |
    la'4. sold'8( la'4) la' |
    re' re' r2 |
    r4 re' mi' fad' |
    mi'2.\melisma re'4 |
    dod' re'2\melismaEnd dod'4\melisma re'1\fermata\melismaEnd |
  }
  \tag #'hyllo {
    re'1 sol2 sol |
    R1 |
    r4 sol la si |
    la2.\melisma la4\melismaEnd |
    re1 |
    r8 sol sol sol do'2 |
    r8 la la la re'2 |
    do'4 do'8 do' fa'4 re' |
    mi'4. mi'8\melisma la2\melismaEnd |
    r4 re' sol sol |
    R1 |
    r4 sol la si |
    la2. la4\melisma re1\fermata\melismaEnd |
  }
>>
\original { R1*5 | }
<<
  \tag #'(iole basse) {
    re''4 mi'' fad'' dod''2 dod''4 |
    si' dod'' re'' |
    la'4. la'8 re''4 |
    sold'2\melisma la'~ la'4\melismaEnd sold' |
    dod'' dod''8 re'' mi'' fad'' |
    re''4 re'' r |
    r8 re'' re'' re'' re''4 re''8 mi'' |
    mi''2 mi'' |
    r4 mi''8 mi'' la'2 |
    r4 r mi''8 re'' |
    dod''4 lad' si' si'2 lad'!4( si'2.) |
    si'4 dod'' re'' la'2 la'4 |
    mi'' fad'' sol'' |
    dod''4. dod''8 fad''4 |
    si'4.\melisma la'8[ si' dod''] re''2\melismaEnd dod''4( |
    re''1*3/4)\fermata |
  }
  \tag #'deianira { R2.*8 R1*3 R2.*11 }
  \tag #'hyllo {
    r2*3/2 mi'4 fad' sol' |
    re'2 re'4 |
    re' mi' fad' |
    si4. si8 fad'2 si |
    la2. |
    la4 la8 si do' re' |
    si4 si r2 |
    r8 si si si si4 si8 dod' |
    dod'2 dod'4 fad'8 fad' |
    si2 sol'8 fad' |
    mi'4 re'4. dod'8 re'4( dod'2) si2. |
    r2*3/2 re'4 mi' fad' |
    si2 si4 |
    fad' sol' la' |
    re'4. re'8 sol'2 mi' |
    re'1*3/4\fermata |
  }
>>
R2.*7 |
