\clef "viole1" \original {
  R1*15 |
  R1 |
  r8 la' la' la' sol'2 |
  r8 si' si' si' fad'4. sol'8 |
  la'4 si' la'2 la'1\fermata |
}
R2.*8 R1*3 R2.*11 |
fad'4 sol' la' sol'2 sol'4 |
fad' mi' fad' si2 re'4 |
la'2 la' la' la'1*3/4\fermata |
