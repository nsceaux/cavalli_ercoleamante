\clef "basse"
\original {
  <<
    \tag #'basse { R1*15 }
    \tag #'basse-continue {
      re1 sol |
      R1 |
      r4 sol la si |
      la1 |
      re |
      r8 sol sol sol do2 |
      r8 la la la re2 |
      do fa4 re |
      mi2 la, |
      re sol |
      R1 |
      r4 sol la si |
      la1 re\fermata |
    }
  >>
  %% Ritornello
  <<
    \tag #'basse {
      R1 |
      r8 re re re sol2 |
    }
    \tag #'basse-continue {
      re1~ |
      re8 re re re sol2 |
    }
  >>
  r8 mi mi mi fad4. mi8 |
  re4 sol, la,2 re1\fermata |
}
<<
  \tag #'basse {
    R2.*8 R1*3 R2.*11 
  }
  \tag #'basse-continue {
    re2. lad |
    si |
    fad2 re4 |
    mi2 re mi |
    la,2. |
    fad |
    sol1 |
    sold |
    la |
    si2 mi4 |
    mi2. fad si, |
    si, fad |
    sol |
    la |
    si2 sol la |
    re1*3/4\fermata |
  }
>>
re2 re4 sol2 sol4 |
la2 la4 si la sol |
la2 re la re1*3/4\fermata |
