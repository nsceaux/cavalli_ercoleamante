\clef "viole2" \original {
  R1*15 |
  R1 |
  r8 re' re' re' re'2 |
  r8 mi' mi' mi' re'2 |
  re'4 sol' mi'2 re'1\fermata |
}
R2.*8 R1*3 R2.*11 |
la4 si do' si2 mi'4 |
la'2 la'4 sol'2 si4 |
mi'2 re' mi' fad'1*3/4\fermata |
