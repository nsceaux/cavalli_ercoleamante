\clef "basse" do'2 si |
la2. sol4 |
fa2. mi8 re |
do4 si,8 la, sol,2 |
mi fa |
la2. mi4 sol1 do |
sol~ |
sol |
mi |
la2 sol |
fa re4 dod |
mi1 la,\fermata |
