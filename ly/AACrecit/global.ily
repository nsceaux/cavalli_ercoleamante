\set Score.currentBarNumber = 16 \bar "" 
\key do \major \midiTempo#120
\time 4/4 s1*5
\measure 3/1 s1*3
\measure 4/4 s1*5
\measure 2/1 s1*2 \bar "|."
