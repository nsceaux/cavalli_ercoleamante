Ah che men -- tre la ter -- ra
di lun -- ga or -- ri -- da guer -- ra
già di -- le -- gua -- ti am -- mi -- ra i fa -- ti re -- i
ne’ be -- a -- ti i -- me -- ne -- i
di Ma -- ri -- a di Lu -- i -- gi
a -- dor -- na Cin -- zia di più bei can -- do -- ri
noi tes -- ti -- mo -- ni e -- les -- se
di quei, ch’a spi -- rar va’, gal -- li -- ci ho -- no -- ri.
