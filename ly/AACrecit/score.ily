\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout { s1*2\break s1*3\break s1*4\break }
    >>
  >>
  \layout {
    indent = \noindent
    system-count = 3
  }
  \midi { }
}
