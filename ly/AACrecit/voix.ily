\clef "basse" <>^\markup\character Tevare
r4 do' r8 si si si16 do' |
la4 la8 la la sol sol sol16 la |
fa4 fa8 fa fa fa mi re |
do do16 do si,8 la, sol, sol, sol fa |
mi4 re8 do fa fa fa sol |
la2 la4 mi8 fa sol1 do |
r8 sol sol sol sol4 sol |
sol8 sol sol fad sol4 sol |
mi4 mi8 mi mi4 mi8 mi |
la4 la8 la sol sol sol la |
fa2 re4 dod8 re |
mi1 la,\fermata |
