\key re \minor \midiTempo#120
\time 4/4 s1*8
\measure 2/1 s1*2
\measure 4/4 s1*8
\measure 2/1 s1*2
\measure 4/4 s1*6
\measure 2/1 s1*2
\measure 4/4 \pygmalion s1*3 s1*4
\measure 2/1 s1*2
\measure 4/4 s1*8
\measure 2/1 s1*2 \bar "|."
