\clef "viole0" sol'1 |
sol' |
sol' |
do'2 re'4 do' |
re' mib'2 re'4 |
re'2 r4 r |
sol'1 |
fa'2. mib'4 |
lab'4. lab'8 mib'8. fa'16 sol'4 sol'1 |
R1*8 |
r1 r4 do' fa'2 |
r4 sol' fad'4. fad'8 |
re'1 |
fad'1~ |
fad'2 sol'~ |
sol' mib'~ |
mib'1 |
fa'4 mib' re'4. la8 si1\fermata |
r4 sol'4. re'8 re'4 |
r lab'4. sol'8 sol' fa' |
sol'1 | \allowPageTurn
R1*7 |
reb'4. mib'8 do'4 do' |
fa'1 |
fa'2. re'4 |
mib'1 |
re'2. re'4 |
sol'1 |
lab'2 fa' |
sol'4 lab' sol'2 sol'1\fermata |
