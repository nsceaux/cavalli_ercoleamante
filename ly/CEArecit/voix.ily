\clef "vsoprano" <>^\markup\character Deianira
r4 mib''4. do''8 do''4 |
r8 fa'' re'' re'' sol'4 sol' |
r mib'' do''8 sib' sib' lab' |
lab' lab' lab' lab' lab'4 lab'8 lab' |
lab'4 sol' sol' fad'8 sol' |
sol'4 sol' r si'8 si' |
si'4 si'8 do'' do''4 do'' |
r8 do'' do'' do'' do'' do'' do'' do'' |
do'' do'' r mib'' do''4. si'8( do''1) |
r8 fa'' re''4 r8 re'' re'' do'' |
re'' re'' do'' do'' do''4 do''8 re'' |
sib'4 sib' r re'' |
sol' sol' r mib'' |
do'' do''8 do'' do'' sib' sib' sib' |
sib' sib' sib' la' la'4 la'8 la' |
do'' do'' do'' sib' re''4 re''8 re'' |
re'' re'' re'' mi'' fa''2 |
do''4 re''8 do'' sib'2 la'1 |
mib''4. la'8 la'4 la'8 la' |
fad'4 fad' r2 |
re''4 do'' do'' do''8 do'' |
do'' do'' do'' re'' sib'4 sib'8 sib' |
sib' sib' do'' re'' mib''4 mib'' |
r8 lab' lab' lab' lab' lab' lab' lab' |
\ficta lab'4 sol'8 sib' sol'4. fad'8( sol'1)\fermata |
\pygmalion R1*3 |
r4 sol'8 sol' sol'4 sol'8 sib' |
fa'8 fa' fa' fa' fa'4 fa'8 sol' |
lab'4 lab'8 lab' lab' lab' lab' sol' |
sol'4 sol' sib' sib'8 lab' |
do'' do'' do'' re'' mib'' sol' mib'2 re'4( mib'2) |
fa'4 fa' r sib'8[ do''] |
lab'4 lab'8 sol' lab'4 lab' |
lab' lab'8 sib' do''4 do'' |
lab'8 lab'16 lab' lab'8 lab' lab' lab'16 lab' fa'8 sol' |
sol' sol' sol' sol' la'4 r8 si' |
si'2 si'8 si' si' do'' |
do''4 do'' do''8 do'' do'' reb'' |
reb''4 reb'' reb'' reb''8 do'' |
do''4. do''8 do''4. si'8( do''1)\fermata |
