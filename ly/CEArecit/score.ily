\score {
  \new StaffGroupNoBar \with { \haraKiriFirst } <<
    \pygmalion\new StaffGroupNoBracket <<
      \new Staff << \global \includeNotes "viole0" >>
      \new Staff << \global \includeNotes "viole1" >>
      \new Staff << \global \includeNotes "viole2" >>
    >>
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \original\origLayout {
        s1*3\pageBreak
        s1*5\break s1*5\break s1*4\break s1*6\break s1*5\pageBreak
        s1*4\break s1*5\break s1*3\break
      }
      \pygmalion\origLayout {
        s1*4\break s1*4\pageBreak
        s1*5\break s1*4\break s1*5\pageBreak
        s1*4\break s1*5\pageBreak
        s1*3\break s1*3\pageBreak
        s1*4\break s1*3\pageBreak
        
      }
    >>
  >>
  \layout { }
  \midi { }
}
