\clef "basse" do1 |
si, |
do |
do |
sib,2 lab, |
<<
  \original { sol,1 | }
  \pygmalion { sol,2 r4 r | }
>>
sol2 mib |
fa2. sol4 |
lab fa sol2 do1 |
sib,~ |
sib,2 la, |
sib,1 |
mib |
mi!~ |
mi2 fa~ |
fa sib,~ |
sib, la,~ |
la,4 sib, do2 fa,1 |
do |
re |
re~ |
re2 sol~ |
sol do~ |
do1 |
si,4 do re2 sol,1\fermata |
\pygmalion {
  do2 si, |
  do sib,4 lab, |
  sol,1 |
}
mib1~ |
mib |
re |
mib |
lab,2 sol,4. lab,!8 sib,2 mib |
sib,1~ |
sib,2 fa |
fa1 |
fa |
do |
<<
  \original { sol1 | }
  \pygmalion { sol2 fa | }
>>
mib2 mi |
fa1 |
mi4 fa sol2 do1\fermata |
