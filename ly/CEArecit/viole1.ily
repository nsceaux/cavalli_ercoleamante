\clef "viole1" mib'1 |
re' |
mib' |
fa' |
fa'4 sib do'4. sol8 |
sol2 r4 r |
re'2 do' |
do' re'4 do' |
mib'4 re'8 do' sol'4 re' mib'1 |
R1*7 |
r2 r4 do' |
fa'4. sol'8 mi'4 fa' fa'2 do'4 re' |
mib'2 do' |
la1 |
re'1~ |
re'2 re'~ |
re' do'~ |
do'1 |
re'4 do'8 sol sib4 la sol1\fermata |
mib'2 re'4 sol'~ |
sol'8 mib' mib'4 re' do' |
si1 |
R1*6 |
r2 reb'4. mib'8 |
sib4 sib lab4. fa8 |
lab4 fa do'2 |
do'1 |
do'2. sol4 |
si2 sol |
sol2. sol'4 |
fa'2 reb'4. mib'8 |
do'4 fa' re'2 mi'1\fermata |
