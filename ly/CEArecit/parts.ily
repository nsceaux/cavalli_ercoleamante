\piecePartSpecs
#(if (eqv? #t (ly:get-option 'pygmalion))
     `((violes #:score-template "score-violes-voix"
               #:music , #{ s1*10 \allowPageTurn #})
       (basse #:score-template "score-basse-continue-voix"))
     '((basse #:score-template "score-voix")))
