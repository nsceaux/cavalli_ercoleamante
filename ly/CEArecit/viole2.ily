\clef "viole2" sol1 |
sol |
sol2. sol4 |
lab2. lab4 |
sib4 sol2 do'8 si |
si2 r4 r |
si4 sol sol2 |
lab2. sol4 |
do'8. sib16 lab4 sol2 sol1 |
R1*8 |
r1 r2 la |
sol la~ |
la4 fad fad2 |
la1~ |
la2 sib~ |
sib sol |
mib1 |
re4 sol~ sol re re1\fermata |
sol1 |
mib4 do re mib8 fa |
re1 |
R1*7 |
r2 r4 lab |
do'4. reb'8 lab4 lab |
lab1 |
sol |
r4 sol re'2 |
do'2. sib4 |
lab2. lab4 |
sol do'4. re'8 sol4 sol1\fermata |
