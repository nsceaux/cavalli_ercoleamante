Mi -- se -- ra, ohi -- mè, ch’as -- col -- to.
Non so, se più ge -- lo -- sa
es -- ser dèa co -- me ma -- dre, o co -- me spo -- sa;
che co -- mu -- ne è’l pe -- ri -- glio
al -- la mia fe -- de co -- niu -- ga -- le, e al fi -- glio;
al -- men con sof -- frir l’u -- no
schi -- var l’al -- tro po -- tes -- si: oh Di -- o qual sor -- te
pre -- fis -- se i -- ni -- quo fa -- to a miei na -- ta -- li:
ch’io sof -- fra a dop -- pio i ma -- li,
né per schi -- var -- ne al -- cun bas -- ti mia mor -- te.
O pre -- sa -- gi fu -- nes -- ti:
Er -- col spir -- ti non ha, se non fe -- ro -- ci,
e non se -- rian già ques -- ti
i di lui pri -- mi par -- ri -- ci -- di a -- tro -- ci.
Co -- me mal mi la -- scia -- i
stras -- ci -- nar da’ miei gua -- i
a ques -- te eu -- bee con -- tra -- de,
o -- ve il des -- tin mi fab -- bri -- cò l’in -- fer -- no:
ho -- ra, ahi las -- sa, dis -- cer -- no
quan -- to me -- gl’e -- ra en -- tro le pa -- trie mu -- ra
di Ca -- li -- do -- nia sos -- pi -- rar pian -- gen -- do
miei dub -- bi ol -- trag -- gi, che con duol più or -- ren -- do es -- ser -- ne qui si -- cu -- ra.
