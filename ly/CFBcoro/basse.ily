\clef "basse"
\transpose do #(if (eqv? #t (ly:get-option 'pygmalion))
                   (ly:make-pitch -2 0)
                   (ly:make-pitch -1 0))
{
  fa2.~ |
  fa |
  fa~ fa |
  sib fa |
  sol la2 si4 |
  do'2. do' |
  sib la |
  sol2. sol4 do'2 |
  re'2. do' |
  re' sol |
  fa~ fa |
  fa fa sib |
  fa sol |
  la2 si4 do'2. |
  la4 sib2 do'2. fa1*3/4\fermata |
  fa2.~ fa~ |
  fa fa4 fa mi |
  re2. do |
}
do~ do~ do |
la,4 la, sib, do2. fa,1*3/4\fermata |
\transpose do #(if (eqv? #t (ly:get-option 'pygmalion))
                   (ly:make-pitch -2 0)
                   (ly:make-pitch -1 0))
{
  fa2.~ fa~ |
  fa fa |
  sib fa |
  sol la2 si4 |
  do'2. do'2 sib4 |
  la2. sol2 fa4 sol2. do' |
  r4 do'2 sib2. |
  la sol |
  r4 sol la sib2. |
  do'2 do'4 re'2. |
  r4 re'2 do'4 re'2 |
  sib2. do'4 re'2 sol2. |
  fa2.~ fa |
  fa fa sib |
  fa sol |
  la2 si4 do'2. |
  la4 sib2 do'2. fa1*3/4\fermata |
  fa2.~ fa~ |
  fa fa4 fa mi |
  re2. do |
}
do~ do~ do |
la,4 la, sib, do2. fa,1*3/4\fermata |
