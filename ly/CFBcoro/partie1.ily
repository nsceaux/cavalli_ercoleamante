\clef "viole1" R2.*30 |
fa'2. fa' |
fa' fa'4 fa' sol' |
la'2 fa'4 sol'2. |
do'2. do' do' |
do'4 do' re' sol2. fa1*3/4\fermata |
R2.*39 |
fa'2. fa' |
fa' fa'4 fa' sol' |
la'2 fa'4 sol'2. |
do'2. do' do' |
do'4 do' re' sol2. fa1*3/4\fermata |
