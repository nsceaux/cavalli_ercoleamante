\clef "dessus" R2.*30 |
la'2 sib'4 la'2 sib'4 |
la'2 la'4 la' si' do'' |
do''2 si'4 do''2. |
mi'2 fa'4 mi'2 fa'4 mi'2 mi'4 |
la' sol' fa' fa'2 mi'4 fa'1*3/4\fermata |
R2.*39 |
la'2 sib'4 la'2 sib'4 |
la'2 la'4 la' si' do'' |
do''2 si'4 do''2. |
mi'2 fa'4 mi'2 fa'4 mi'2 mi'4 |
la' sol' fa' fa'2 mi'4 fa'1*3/4\fermata |
