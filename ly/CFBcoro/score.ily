\score {
  \new StaffGroupNoBar \with { \haraKiriFirst } <<
    \new StaffGroupNoBracket <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
      \new Staff << \global \includeNotes "partie1" >>
      \new Staff << \global \includeNotes "partie2" >>
    >>
    \original <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'coro1 \includeNotes "voix"
      >> \keepWithTag #'coro1 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'coro2 \includeNotes "voix"
      >> \keepWithTag #'coro2 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'coro3 \includeNotes "voix"
      >> \keepWithTag #'coro3 \includeLyrics "paroles"
    >>
    \pygmalion \notemode <<
      \new Staff \with { instrumentName = \markup\character Aura 1 }
      \withLyrics <<
        \global \transpose do' do { \keepWithTag #'coro1 \includeNotes "voix" }
        \clef "petrucci-c1/G_8"
      >> \keepWithTag #'coro1 \includeLyrics "paroles"
      \new Staff \with { instrumentName = \markup\character Aura 2 }
        \withLyrics <<
        \global \transpose do' do { \keepWithTag #'coro2 \includeNotes "voix" }
        \clef "petrucci-c1/G_8"
      >> \keepWithTag #'coro2 \includeLyrics "paroles"
      \new Staff \with { instrumentName = \markup\character Ruscello }
      \withLyrics <<
        \global \transpose do' do { \keepWithTag #'coro3 \includeNotes "voix" }
        \clef "petrucci-c4/bass"
      >> \keepWithTag #'coro3 \includeLyrics "paroles"
    >>
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2.*12\pageBreak
        s2.*18\break s2.*12\pageBreak
        s2.*14\break s2.*13\pageBreak
        s2.*12\break
      }
      \modVersion { s2.*30\break s2.*12\break s2.*39\break }
    >>
  >>
  \layout {
    indent = #(if (eqv? #t (ly:get-option 'pygmalion))
                  largeindent
                  smallindent)
  }
  \midi { }
}
