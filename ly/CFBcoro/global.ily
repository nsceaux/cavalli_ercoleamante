\key fa \major \midiTempo#240
\digitTime\time 3/4 s2.*2
\measure 6/4 s1.*9
\measure 9/4 s2.*3
\measure 6/4 s1.*2
\measure 9/4 s2.*3
\beginMark "Rit[ornello]" \tempo\markup\normal-text "Tou douxemans"
\digitTime\time 3/4 \measure 6/4 s1.*3
\measure 9/4 s2.*6 \bar "||"
\digitTime\time 3/4 \measure 6/4 s1.*5
\measure 12/4 s2.*4
\measure 6/4 s1.*5
\measure 9/4 s2.*3
\measure 6/4 s1.
\measure 9/4 s2.*3
\measure 6/4 s1.*2
\measure 9/4 s2.*3 \bar "||"
\beginMark "Rit[ornello]" \tempo\markup\normal-text  "Tou douxemans"
\digitTime\time 3/4
\measure 6/4 s1.*3
\measure 9/4 s2.*6 \bar "|."
