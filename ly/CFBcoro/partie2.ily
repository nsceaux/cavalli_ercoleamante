\clef "viole2" R2.*30 |
do'2 sib4 do'2 sib4 |
do'2. la4 re' sol |
re'2 re'4 sol2. |
sol2 fa4 sol2 fa4 sol2. |
fa4 sib fa do'2. do'1*3/4\fermata |
R2.*39 |
do'2 sib4 do'2 sib4 |
do'2. la4 re' sol |
re'2 re'4 sol2. |
sol2 fa4 sol2 fa4 sol2. |
fa4 sib fa do'2. do'1*3/4\fermata |
