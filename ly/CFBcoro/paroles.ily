Dor -- mi, \tag #'(coro1 coro2 basse) { dor -- mi, dor -- mi, }
o Son -- no dor -- mi
fra le brac -- cia a Pa -- si -- the -- a
nin -- fa a -- ver non ti po -- te -- a
più d’af -- fet -- ti a’ tuoi con -- for -- mi:
dor -- mi, \tag #'(coro1 coro2 basse) { dor -- mi, dor -- mi, }
o Son -- no dor -- mi
o Son -- no o Son -- no dor -- mi.

Dor -- mi, \tag #'(coro1 coro2 basse) { dor -- mi, dor -- mi, }
o Son -- no dor -- mi
so -- vra a te gli a -- mo -- ri is -- tes -- si
len -- te mo -- va -- no le piu -- me;
e al tuo cor pla -- ci -- do nu -- me,
ge -- lo -- si -- a mai non ap -- pres -- si
de’ suoi rei sos -- pet -- ti i stor -- mi
dor -- mi, \tag #'(coro1 coro2 basse) { dor -- mi, dor -- mi, }
o Son -- no dor -- mi
o Son -- no o Son -- no dor -- mi.
