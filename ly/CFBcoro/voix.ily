<<
  \tag #'(coro1 basse) {
    \clef "vsoprano" do''2 re''4 |
    do''2 re''4 |
    do''2 do''4 do'' sib' do'' |
    re''2. do''4 do'' do'' |
    sib' sib' sib' la'2 sol'4 |
    sol'2. sol'4 sol' la' |
    sib'2 sib'4 do''2 do''4 |
    re''2. re''4 do'' sib' |
    la' la' sib' do''2 sib'4 |
    la'2. sol' |
    do''2 re''4 do''2 re''4 |
    do''2 do''4 do'' sib' do'' re''2. |
    do''2 do''4 sib'2. |
    la' r2*3/2 |
    do''4 sib' la' sol'2. la' |
  }
  \tag #'coro2 {
    \clef "vsoprano" la'2 sib'4 |
    la'2 sib'4 la'2 la'4 la' sol' la' |
    sib'2. la'4 la' la' |
    sol' sol' sol' fa'2 fa'4 |
    mi'2. mi'4 mi' fad' |
    sol'2 sol'4 la'2 la'4 |
    sib'2. sib'4 la' sol' |
    fad' fad' sol' la'2 sol'4 |
    sol'2 fad'4( sol'2.) |
    la'2 sib'4 la'2 sib'4 |
    la'2 la'4 la' sol' la' sib'2. |
    la'2 la'4 sol'2. |
    fa' r2*3/2 |
    la'4 sol' fa' fa'2 mi'4( fa'2.) |
  }
  \tag #'coro3 {
    \clef "vtenor" fa2.~ |
    fa |
    fa fa4 fa fa |
    sib2. fa4 fa fa |
    sol sol sol la2 si4 |
    do'2. do'4 do' do' |
    sib2 sib4 la2 la4 |
    sol2. sol4 do' do' |
    re' re' re' do'2 do'4 |
    re'2. sol |
    fa~ fa |
    fa fa4 fa fa sib2. |
    fa r4 r sol |
    la2( si4) do'2. |
    la4 sib sib do'2. fa1*3/4\fermata |
  }
>>
R2.*12 |
<<
  \tag #'(coro1 basse) {
    do''2 re''4 do''2 re''4 |
    do''2 do''4 do'' sib' do'' |
    re''2. do''4 do'' do'' |
    sib'2 sib'4 la'2 sol'4 |
    sol'2. sol'4 sol' sol' |
    la'2 la'4 si'2 do''4 do''2 si'4( do''2.) |
    r4 sol' la' sib'2. |
    do''4. sib'8 do''4 re''2 re''4 |
    r re'' do'' sib'2 sib'4 |
    mib'' sib' la' la'2 la'4 |
    r4 la' la' sib' la'2 |
    re'' re''4 mib'' la'2 sol'2. |
    do''2 re''4 do''2 re''4 |
    do''2 do''4 do'' sib' do'' re''2. |
    do''2 do''4 sib'2. |
    la' r2*3/2 |
    do''4 sib' la' sol'2. la' |
  }
  \tag #'coro2 {
    la'2 sib'4 la'2 sib'4 |
    la'2 la'4 la' sol' la' |
    sib'2. la'4 la' la' |
    sol'2 sol'4 fa'2 fa'4 |
    mi'2. mi'4 mi' mi' |
    fa'2 fa'4 fa'2 mi'4 re'2. mi' |
    r4 mi' fad' sol'2. |
    la'4. sol'8 la'4 sib'2 sib'4 |
    r sib' la' sol'2 sol'4 |
    la' sol' fad' fad'2 fad'4 |
    r fad' fad' sol' fad'2 |
    sib'2 sib'4 sol' sol'4. fad'8( sol'2.) |
    la'2 sib'4 la'2 sib'4 |
    la'2 la'4 la' sol' la' sib'2. |
    la'2 la'4 sol'2. |
    fa' r2*3/2 |
    la'4 sol' fa' fa'2 mi'4( fa'2.) |
  }
  \tag #'coro3 {
    fa2.~ fa |
    fa fa4 fa fa |
    sib2. fa4 fa fa |
    sol2 sol4 la2 si4 |
    do'2. do'4 do' sib |
    la2 la4 sol2 fa4 sol2. do' |
    r4 do' do' sib2. |
    la4. la8 la4 sol2 sol4 |
    r sol la sib2 sib4 |
    do'4 do' do' re'2 re'4 |
    r4 re' re' do' re'2 |
    sib2 sib4 do' re'2 sol2. |
    fa2.~ fa |
    fa fa4 fa fa sib2. |
    fa r4 r sol |
    la2( si4) do'2. |
    la4 sib sib do'2. fa1*3/4\fermata |
  }
>>
R2.*12 |
