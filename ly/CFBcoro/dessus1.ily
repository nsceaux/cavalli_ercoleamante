\clef "dessus" R2.*30 |
do''2 re''4 do''2 re''4 |
do''2 do''4 do'' re'' mi'' |
fa''2. mi'' |
sol'2 la'4 sol'2 la'4 sol'2 sol'4 |
do'' sib' la' sol'2. la'1*3/4\fermata |
R2.*39 |
do''2 re''4 do''2 re''4 |
do''2 do''4 do'' re'' mi'' |
fa''2. mi'' |
sol'2 la'4 sol'2 la'4 sol'2 sol'4 |
do'' sib' la' sol'2. la'1*3/4\fermata |
