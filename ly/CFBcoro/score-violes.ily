\score {
  \new StaffGroupNoBar \with { \haraKiriFirst } <<
    \new StaffGroupNoBracket <<
      \new Staff << \global \includeNotes "partie1" >>
      \new Staff << \global \includeNotes "partie2" >>
    >>
    \notemode <<
      \new Staff
      \withLyrics <<
        \global \transpose do' do { \keepWithTag #'coro1 \includeNotes "voix" }
        \clef "alto"
      >> \keepWithTag #'coro1 \includeLyrics "paroles"
      \new Staff
        \withLyrics <<
        \global \transpose do' do { \keepWithTag #'coro2 \includeNotes "voix" }
        \clef "alto"
      >> \keepWithTag #'coro2 \includeLyrics "paroles"
      \new Staff
      \withLyrics <<
        \global \transpose do' do { \keepWithTag #'coro3 \includeNotes "voix" }
        \clef "bass"
      >> \keepWithTag #'coro3 \includeLyrics "paroles"
    >>
    \new Staff <<
      \global \includeNotes "basse"
      \modVersion { s2.*30\break s2.*12\break s2.*39\break }
    >>
  >>
  \layout { }
}
