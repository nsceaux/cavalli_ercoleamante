\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \keepWithTag #'coro1 \includeNotes "voix"
    >> \keepWithTag #'coro1 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'coro2 \includeNotes "voix"
    >> \keepWithTag #'coro2 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'coro3 \includeNotes "voix"
    >> \keepWithTag #'coro3 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'coro4 \includeNotes "voix"
    >> \keepWithTag #'coro4 \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout { s2.*9\break }
    >>
  >>
  \layout { }
  \midi { }
}
