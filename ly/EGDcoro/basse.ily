\clef "basse" do'2. |
sib |
lab |
lab |
sol fa2 mib4 |
sib2 sol4 do'2. |
si |
si2 do'4 sol2. |
r4 r sol |
do2 r4 |
do8 sol, re4 sol, |
