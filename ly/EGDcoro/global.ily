\key re \minor \midiTempo#160
\customTime\markup\vcenter { \musicglyph#"timesig.C44" \musicglyph#"three" }
\measure 3/4 s2.*4
\measure 6/4 s1.*2
\measure 3/4 s2.
\measure 6/4 s1.
\measure 3/4 s2.*3 \bar "|."
