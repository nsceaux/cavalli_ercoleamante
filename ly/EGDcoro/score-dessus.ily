\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \keepWithTag #'coro1 \includeNotes "voix"
    >> \keepWithTag #'coro1 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'coro2 \includeNotes "voix"
    >> \keepWithTag #'coro2 \includeLyrics "paroles"
  >>
  \layout { }
}
