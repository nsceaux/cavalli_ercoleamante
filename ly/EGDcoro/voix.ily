<<
  \tag #'(coro1 basse) {
    \clef "vbas-dessus" do''2. |
    r4 re''8 re'' re'' re'' |
    do''4 do'' do'' |
    do'' do'' do''8 do'' |
    sib'2. lab'4. lab'8 sib'4 |
    sib' sib' si'( do''2.) |
    R2. |
    re''4. re''8 do''4 si'2 si'4 |
    r r si'8 si' |
    do''4 do'' r |
    do''8 sib' la'8. la'16( si'4) |
  }
  \tag #'coro2 {
    \clef "valto" sol'2. |
    r4 sol'8 sol' sol' sol' |
    lab'4 lab' lab' |
    lab' lab' lab'8 lab' |
    mib'2. fa'4. fa'8 sol'4 |
    fa' fa' r4 r r do'( |
    sol'2.) |
    sol'4. sol'8 sol'4 sol'2 sol'4 |
    r r sol'8 sol' |
    sol'4 sol' r |
    sol'8 sol' sol'8. fad'16( sol'4) |
  }
  \tag #'coro3 {
    \clef "vtenor" mib'2. |
    r4 re'8 re' re' re' |
    mib'4 mib' mib' |
    mib' mib' mib'8 fa' |
    sol'2. do'4. do'8 mib'4 |
    re' re' re'( mib'2.) |
    re'2 re'4 |
    re'4. re'8 mib'4 re'2 re'4 |
    r r re'8 re' |
    mib'4 mib' r |
    mib'8 re' re'4 re' |
  }
  \tag #'coro4 {
    \clef "vbasse" do'2. |
    r4 sib8 sib sib sib |
    lab4 lab lab |
    lab lab lab8 lab |
    sol2. fa4. fa8 mib4 |
    sib sib sol( do'2.) |
    si2 si4 |
    si4. si8 do'4 sol2 sol4 |
    r r sol8 sol |
    do4 do r |
    do8 sol, re4 sol, |
  }
>>
