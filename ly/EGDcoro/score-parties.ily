\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \keepWithTag #'coro2 \includeNotes "voix" \clef "alto"
    >> \keepWithTag #'coro2 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'coro3 \includeNotes "voix" \clef "alto"
    >> \keepWithTag #'coro3 \includeLyrics "paroles"
  >>
  \layout { }
}
