\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*5\break s1*5\break s1*5\break s1*6\pageBreak
        s1*4\break s1*4\break s1*2 s2.*4\break s2.*8\break s2.*10\pageBreak
        s2.*12\break s2.*10\break s2.*6 s1*2\break s1*5\break s1*4\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
