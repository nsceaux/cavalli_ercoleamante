\ffclef "vbas-dessus" <>^\markup\character Giunone
do''4 do'' r8 do'' do'' re'' |
mi''4 mi''8 mi'' mi'' mi'' do''8. si'16 |
re''4 re'' r2 |
\ffclef "vtenor" <>^\markup\character Hyllo
si4 si si8 si si re' |
si4. dod'8 dod'4 dod'8 dod' |
la4 la r2 |
r8 mi' do' do' r la la la |
fa4 fa8 mi mi4 mi |
la4 la8 si sold4 sold |
r sold8 sold sold4 sold8 sold |
do'4 do' re' sib |
\ficta sib sib8 la la2 |
la4 si8 do' la4. sold8( la1)\fermata |
\ffclef "vbas-dessus" <>^\markup\character Giunone
\original {
  r4 mi'' do'' do'' |
  r8 do'' do'' do'' sol'4 sol' |
  sol'8 sol' sol' sol'16 la' la'2 |
  re''4 re''8 re'' si'4 si' |
  si' si'8 si' mi''4 mi'' |
  sol'8 sol' mi' sol' sol'4. fad'8( sol'1) |
  r4 sol' si'8 si' si' si' |
  si'4 si' si'8 re'' sol' sol'16 sol' |
  re'4 re' re'8 re' sol' sol'16 sol' |
  sol'8 sol'16 sol' sol'8 sol' do''4 do'' |
  do'' sib' sib' sib'8 sib' |
  \ficta sib'4 sib' sib'8 sib' sib' sib'16 sib' |
  \ficta sib'8 sib' sib' la' la'4 la' |
  la'8 la' la' la'16 si' si'4 si' |
  si'8 si' si' si'16 do'' do''4 do''8 do'' |
  la'4. do''8 sol' sol' r4 |
  r r mi''8 re'' |
  do''4 si'4. do''8 |
  la'4 la' do'' |
  la'4 la'8 sol' la' sol' |
  la'4. sol'8 la' si' do''2 do''4 |
  re'' re'' re'' do'' do'' do'' |
  si'4. la'8 si' do'' |
  la'4 la' re' |
  mi'4 mi'8 mi' fad' fad' |
  sol'2 sol'4 |
  la'4 la'8 re'' do'' do'' |
  si'2 si'4 |
  sol' sol' sol' sol'2 sol'4 |
  sol' sol' sol' sol'2 fad'4 |
  sol'2 sol'4 |
  r4 la' la' la' la' la' la'2 la'4 |
  si'4 si' la' |
  la'2 la'4 la'2. |
  sol'4. la'8 si'4 si'( la'2) sol'2. |
  do''4 do'' do'' |
  do''2 do''4 do''2 do''4 |
  r4 re'' re'' mi'' mi'' re'' |
  re'' re'' re'' |
  do'' do'' do'' |
  si'4. si'8 mi''4 la'2 la'4 |
  r4 do'' do'' fa' fa'8[ sol'] la'[ si'] |
  do''2 do''4 re''2 sol'4 |
  sol'4. fad'8 sol'4 sol'2. |
  sol'4 sol'' mi'' |
  do'' fa'' re'' |
  mi'' do'' la' |
  re''8[\melisma do'' si' la'] sol'4 |
  sol''8[ fa'' mi'' re''] do''4\melismaEnd |
  re''4 re''2 do''2. |
}
r4 la'8 do'' la'4 la' |
r do''8 re'' sib'4 sib' |
r8 do'' fad' sol' sol'4. fad'!8( |
sol'4) r8 sol' do'' do'' si' do'' |
la'4. re''8 si' sol' do''4~ |
do''2. si'4\melisma do''1\fermata\melismaEnd |
\original {
  \ffclef "vtenor" <>^\markup\character Hyllo
  si4 si r8 si si si16 la |
  si4 si8 sol si si si si |
  re'4 re'8 re' la4 la8 si |
  sol4 sol8 si sol sol sol fad |
  fad fad si si si4 si8 si |
  mi'2 re'4 do'8 si |
  si2( la) sol1\fermata | 
}
