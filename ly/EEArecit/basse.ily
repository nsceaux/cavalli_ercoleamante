\clef "basse" do1~ |
do2 la, |
sol,1 |
sol, |
sol, |
re |
la, |
la,2 sol, |
fa, mi, |
mi1 |
mi2 re~ |
re dod~ |
dod4 re mi2 la,1\fermata |
\original {
  do1~ |
  do |
  do2 fa,~ |
  fa, sol,~ |
  sol, do~ |
  do re sol,1 |
  sol,1 |
  sol,~ |
  sol,~ |
  sol,2 mi, |
  mi,1~ |
  mi,~ |
  mi,2 fa,~ |
  fa, sol,~ |
  sol, la, |
  fa,4 mi, sol,2 |
  do do'8 sol |
  la4 mi2 |
  fa mi4 |
  fa4. mi8 fa mi |
  fa4. mi8 do sol do2. |
  si, la, |
  sol,2 do4 |
  re2 si,4 |
  do2 re4 |
  mi2. |
  fad |
  sol |
  sol mi |
  si, do |
  sol, |
  fad, mi, re, |
  si, |
  do re |
  mi2 do4 re2. sol, |
  mi |
  fa mi |
  re do |
  si, |
  la, |
  sol, fa, |
  mi, re, |
  do, si, |
  la, sol,2 sol4 |
  mi4 do do' |
  la fa sib |
  sol la fa |
  re sol8 fa mi re |
  do4 do'8 sib la sol |
  fa4 sol2 do2. |
}
fa1 |
fa2 re |
do re |
sol, mi, |
fa, sol,4. fa,8 |
sol,1 do\fermata |
\original {
  sol1 |
  sol |
  fad |
  mi |
  re |
  do2 si,4 do |
  re1 sol,\fermata |
}
