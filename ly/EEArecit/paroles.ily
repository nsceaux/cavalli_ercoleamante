Dun -- que del mio po -- te -- re
dif -- fi -- de -- rai tu so -- lo?

Di -- va a che vi -- ver più chi vi -- ve al duo -- lo?
Ma pu -- re os -- se -- qui -- o -- so
ti chieg -- gio hu -- mil per -- do -- no,
che quan -- tun -- que pe -- no -- so,
gra -- to il vi -- ver mi fia poi -- ch’è tuo do -- no. __

\original {
  Non li -- ce a voi mor -- ta -- li
  del des -- tin pre -- ve -- der gli al -- ti de -- cre -- ti
  quan -- to più stra -- ni tan -- to più se -- gre -- ti. __
  
  Quin -- di è che nel mi -- ra -- re
  de’ fu -- tu -- ri nas -- cos -- ti
  i pre -- lu -- di tal -- vol -- ta al fi -- ne op -- pos -- ti,
  spes -- so cie -- chi las -- cia -- te
  con i vos -- tri giu -- di -- zi in -- fer -- mi, e mon -- chi,
  che d’i -- gno -- te ven -- tu -- re
  dis -- pe -- ra -- ta i -- gno -- ran -- za il fil vi tron -- chi.
  Ma se a scor -- ger giun -- ges -- te
  in que -- gli in -- es -- pli -- ca -- bi -- li vo -- lu -- mi
  scrit -- ti in zaf -- fi -- ri a let -- te -- re di stel -- le:
  so -- ven -- te am -- mi -- ra -- res -- te
  es -- ser in lor pre -- fis -- so,
  ch’in -- a -- ri -- dis -- ca a len -- te piog -- ge un pra -- to
  e lo ren -- da fe -- con -- do
  di Si -- rio, e d’A -- qui -- lon l’a -- ri -- do fia -- to;
  che res -- ti in pic -- ciol sta -- gno
  d’un Gia -- so -- ne, e d’un Ti -- si il le -- gno il le -- gno ab -- sor -- to,
  ch’a i nau -- fra -- gi con -- du -- ca au -- ra au -- ra tran -- quil -- la,
  et av -- ver -- sa tem -- pes -- ta al lie -- to por -- to.
}
Van -- ne dun -- que, e pur spe -- ra, e non t’an -- no -- i __
il dar più fe -- de a me, ch’a i sen -- si tuo -- i. __

\original {
  Di -- va do -- vun -- que io si -- a
  non so se po -- si in cie -- lo, o in ter -- ra il pie -- de,
  co -- sì di sue for -- tu -- ne
  pur in -- cer -- ta se n’ va l’a -- ni -- ma mi -- a.
}
