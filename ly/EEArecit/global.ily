\key do \major
\time 4/4 \midiTempo#100 s1*3 \bar "|."
s1*9 \measure 2/1 s1*2 \bar "|."
%% Giunone
\original {
  \measure 4/4 s1*5 \measure 2/1 s1*2
  \measure 4/4 s1*10
  \digitTime\time 3/4 \midiTempo#160 s2.*4
  \measure 6/4 s1.*2
  \measure 3/4 s2.*6
  \measure 6/4 s1.*2
  \measure 3/4 s2.
  \measure 9/4 s2.*3
  \measure 3/4 s2.
  \measure 6/4 s1.
  \measure 9/4 s2.*3
  \measure 3/4 s2.
  \measure 6/4 s1.*2
  \measure 3/4 s2.*2
  \measure 6/4 s1.*4
  \measure 3/4 s2.*5
  \measure 6/4 s1.
}
\time 4/4 \midiTempo#100 s1*5 \measure 2/1 s1*2 \bar "|."
\original {
  \measure 4/4 s1*6 \measure 2/1 s1*2 \bar "|."
}
