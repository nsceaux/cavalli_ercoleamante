\clef "dessus" R1*4 |
si'1 |
si'2. si'4 sol'1 |
la'2. la'4 re'2. sol'4 |
sol'2 la'4 sol' |
re''1 re''\fermata |
R2.*21\allowPageTurn R1\allowPageTurn
R2.*21\allowPageTurn R1*14\allowPageTurn
R2.*33\allowPageTurn R4*7
