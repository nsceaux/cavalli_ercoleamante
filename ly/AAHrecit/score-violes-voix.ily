\score {
  \new StaffGroupNoBar \with { \haraKiriFirst } <<
    \new StaffGroupNoBracket <<
      \new Staff <<
        { s1*4 <>^\markup\italic Piano violi }
        \global \includeNotes "partie1"
      >>
      \new Staff << \global \includeNotes "partie2" >>
    >>
    \new StaffGroupNoBracket <<
      \new Staff << \global \includeNotes "viole0" >>
      \new Staff << \global \includeNotes "viole1" >>
      \new Staff << \global \includeNotes "viole2" >>
    >>
    \new Staff \with { \tinyStaff } \withLyrics <<
      \global \keepWithTag #'basse \includeNotes "voix"
    >> \keepWithTag #'basse { \set fontSize = #-2 \includeLyrics "paroles" }
    \new Staff <<
      \global \includeNotes "basse"
      \modVersion {
        s1*4\break s1*8\break s2.*21 s1\break s2.*21\break
        s1*10\allowPageTurn
      }
    >>
  >>
  \layout {
    system-count = #(*system-count*)
    indent = #(if (*instrument-name*)
                  largeindent
                  (or (*score-indent*) smallindent))
  }
}
