\key do \major \midiTempo#120
\time 4/4 s1*5
\measure 2/1 s1*4
\measure 4/4 s1
\measure 2/1 s1*2
\digitTime\time 3/4 s2.*9
\measure 6/4 s1.
\measure 3/4 s2.*10
\time 4/4 s1
\digitTime\time 3/4 s2.*11
\measure 6/4 s1.
\measure 3/4 s2.*6
\measure 6/4 s1.
\time 4/4 s1*14
\digitTime\time 3/4
\measure 3/4 s2.*2
\measure 6/4 s1.
\measure 3/4 s2.*19
\measure 6/4 s1.
\measure 3/4 s2.
\measure 6/4 s1.
\measure 3/4 s2.*5
\measure 7/4 s4*7 \bar "|."
\endMarkSmall "Segue subito il Coro"
