\clef "basse" sol1~ |
sol2 fad |
sol1~ |
sol |
sol~ |
sol do |
la, sol, |
<<
  \original { do1 | re sol, | }
  \pygmalion { do,2 do | re re, sol,1 | }
>>
sol4 mi la |
re2 mi4 |
fa2 sol4 |
la2 la4 |
re2 re4 |
do2 do'4 |
la2 la4 |
sol2 sol4 |
la2 la4 |
sib2 sib4 do'2. |
dod' |
re'8 do' re'4 la8 do' |
sib2. |
la8 sol la4 mi8 sol |
fa2 mi4 |
re do8 re sib,4 |
la, sol,8 la, fa,4 |
sol,2 la,4 |
sib, fa,8 sol, la,4 |
sol, la,2 |
re1 |
re2. |
si,4 do re |
sol,2. |
mi4 fa sol |
do2. |
la,4 sib, do |
fa2 mi4 |
re do si,! |
do2 mi4 |
re2 re4 |
do2 la,4 |
sib,4 do2 re4 mi2 |
fa2 mi4 |
re2 do4 |
re la, si, do2. |
la, |
sib,2 sib,4~ |
sib, do2 fa,2. |
fa,1 | \allowPageTurn
fa,~ |
fa,~ |
fa,2 do |
sol,1~ |
sol, |
sol, |
dod |
re |
sib, |
la, |
fa, |
sib,2 sol, |
la,4 sol, la,2 |
re2.~ |
re4 re8 do si,4 |
do re2 sol,2.~ |
sol,4 sol8 fa mi4 |
fa sol2 |
do la,4 |
sib, do2 |
fa,4 fa mi |
re do sib, |
do4. sib,8 la,4 |
re do sib, |
do2. |
re4 la, sib, |
do2. |
fa,2 fa4 |
mi re2 |
do4 si, la, |
sol, sol8 la sol fa |
mi4. si,8 do re |
do4. do8 re mi |
re4. re8 mi fa |
mi4. fa8 mi re |
do4 re2 sol,2 la4 |
sol fa2 |
mi4 re2 do4 si, la, |
sol,4. la,8 sol, fa, |
mi,4. mi,8 fa, sol, |
fa,4. fa,8 sol, la, |
sol,4. sol,8 la, si, |
la,4. sib,8 la, sol, |
fa,4 sol,2 do1\fermata |
