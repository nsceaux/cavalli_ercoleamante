\clef "viole1" R1*12 |
%{re'4 sol' mi' |
fa'2 sol'4 |
re' fa' sib' la' mi' la' |
fa'2 re'4 |
mi'2 mi'4 |
fa'2 mi'4 |
sol'8 la' sol'2 |
fa' la'4~ |
la' sol' fa' do''4. re''8 do'' sib' |
la'2 mi'4 |
la'2. |
re'2 re'4 |
la'2 la4 |
re'8 mi' fa'4 dod' |
re' la re' |
dod'8 re' mi'4 re' |
sib2 la4 |
re' re'4. dod'8 |
re'4 re' dod' |
re' r r2 | \allowPageTurn%}
R2.*21 R1 |
R2. |
si4 mi' re' |
re' sol'4. la'8 |
si' do'' la'4 si' |
do'' do'2 |
fa'8 la' sol' fa' mi'4 |
do' re' mi' |
fa'2 re'4 |
mi'2 mi'4 |
fa'2 la'4 |
do'' do'2 |
sol'4 mi' do' fa' mi'4. re'8 |
do'2 sol'4 |
fa'4. sol'8 la'4 |
fa'2 re'4 |
do'2 do'4 |
fa'2.~ |
fa'4 fa' re'~ |
re'8 re' do'2 do'2. |
R1*14 R2. |
fa'4 fa'8 mi' re'4 |
mi'4 re'2 re' sol'4 |
sol'2 sol'4 |
la' sol'2 |
mi'2 do'4 |
re' sol2 |
la sol4 |
sib do' re' |
do' sol do' |
re' mi' re' |
mi'8 fa' sol'4 do' |
fa'4. mi'8 re'4 |
mi'2 do'4 |
do' re'2 |
mi'4 fa'2 |
mi' do'4 |
re'2 re'4 |
mi'2 mi'4 |
la8 si do'4 si8 la |
si do' re'4 do'8 si |
do' re' mi' re' sol' fa' |
mi'4 la re' re'2.~ |
re'2 re'4 |
mi' si2 sol do'4 |
si4. do'8 si la |
sol2 do'4 |
do'2 si8 la |
re'4. re'8 do' si |
mi'4. mi'8 fa' sol' |
re'2 re'4 mi'1\fermata |
