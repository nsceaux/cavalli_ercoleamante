\clef "viole2" R1*4 |
sol1 |
sol sol2 mi |
la2. la4 si2. si4 |
sol4 mi la8 do' sol4 |
re re' re'2 re'1\fermata |
R2.*21\allowPageTurn R1\allowPageTurn
R2.*21\allowPageTurn R1*14\allowPageTurn
R2.*33\allowPageTurn R4*7
