\clef "viole2" R1*12 |
%{si2 dod'4 |
re'2 do'4 |
fa' re' sol' |
fa' la' mi' |
re' la fa |
do'2.~ |
do' |
sib8 do' re'2 |
do'2 do'4 |
re'2 re'4 fa' do'4. re'8 |
mi'2. |
fa'8 mi' fa'4 fad' |
sol'8 la' sib'4 sol' |
mi' dod' mi' |
la2 sol4 |
la8 si do' fa re4 |
la2 la4 |
sol2 do'4 |
sib la2 |
mi'4 la4. sol8 |
fad4 r r2 |%}
R2.*21 R1 |
R2. |
re'4 do'8 si la4 |
sol8 la si do' re'4 |
sol' fa' re' |
mi'2. |
la4 re' sol |
la2 sol4 |
fa4 la sol |
sol2 do'4 |
re'2 fa'4 |
mi'2 la4 |
re' do' mi' re' sol' sol |
la do'2 |
re'2 mi'4 |
re' do' sol |
sol' mi'2 |
do'2. |
re'2 re4~ |
re sol2 la2. |
R1*14 R2. |
la2 si4 |
sol la re' si2 si4 |
re'8 do' si la sol4 |
re'2 do'4 |
do'2 fa'4 |
fa'2 mi'4 |
fa' do'2 |
re'4. do'8 do' sib |
la4 do' fa |
sib sol2 |
do'8 re' mi'4 la |
la la4. sol8 |
do'2 sol4 |
la fa2 |
do'4 la fa |
do' re' la |
si4. do'8 si la |
sol2 sol4 |
do'4. la8 fa sol~ |
sol8 la si4 sol8 la~ |
la si do' sol sol4~ |
sol re' la si2 do'4~ |
do'8 la si4 si |
do' fa re mi re do |
sol2 re'4 |
do'4. do'8 la sol |
la4. la8 sol fad |
sol4 si sol |
do'4. sol8 do' sib |
la do' do'4 si do'1\fermata |
