\clef "vsoprano" <>^\markup\character Cinzia
r8 sol'' re''4 r8 sol'' mi'' re'' |
do''4 do'' do'' re''8 la' |
si'4 si'8 sol' re''4 re''8 sol'' |
re''4 re'' r2 |
r4 re'' re''2 |
re''4 sol'' fa''4. mi''8 mi''2 mi'' |
fad''2 fad'' sol'' sol'' |
r4 mi'' fad'' sol'' |
sol'2. fad'4( sol'1) |
si'4 mi'' dod'' |
fa''4 fa''8 sol'' fa'' mi'' |
re''2 mi''4 |
fa'' mi'' mi''8 mi'' |
fa''4. mi''8 fa''4 |
sol''4 sol'' do''8 do'' |
fa''4. fa''8 do''4 |
re'' re'' mi'' |
fa''2 fa''4 |
re''2 fa''4 fa''2 mi''4 |
la''8 sol'' la''4 mi''8 sol'' |
fa''2 r4 |
sol''8 fad'' sol''4 re''8 mi'' |
dod''2 dod''4 |
re''\melisma do''8[ re''] mi''4 |
fa'' mi''8[ fa''] sol''4\melismaEnd |
mi''2 la'4 |
sib'4\melisma la'8[ sib'] do''4 |
re'' re''8[ mi''] fa''4\melismaEnd |
mi'' mi''2 |
re''4 r8 re'' fad''4 fad''8 re'' |
fad''4 fad'' r |
re''4 mi''8[ re''] mi''[ fad''] |
sol''2 sol''4 |
sol'' la''8[ sol''] fa''[ sol''] |
mi''2 mi''4 |
fa''8 do'' re''4 do''8 sib' |
la'4 la' do'' |
fa''2 sol''4 |
mi''4.\melisma re''8 do''4 |
fa''4. mi''8 fa''4 |
sol''\melismaEnd sol'' do'' |
re''4 mi''2 fa''4 sol''2 |
do''4 do'' sol''4 |
r4 fa'' mi'' |
fa''4 do''( re'') mi''2 mi''4 |
fa'' mi''4. re''8 |
re''4( do'') sib'( |
la') sib'8[ la' sib' do''] la'2. |
r4 la'8 la' do''4 r8 do'' |
do'' do''16 do'' do''8 sib' do''4 do''8 do'' |
do'' do'' do'' do'' do''4 do'' |
do''8. do''16 do''8 re'' mi''4 mi'' |
r re''4 re''8 re'' re'' do'' |
re''4 re'' re''8 re'' re'' re''16 re'' |
re''8 re'' re'' re'' re''4 re''8 mi'' |
mi''4 mi''8 la' mi'' la' mi''8. fa''16 |
re''4 re''8 re'' re'' dod'' re''4 |
re''8 re'' mi'' fa'' sol''4 sol''8 re'' |
mi''4 mi'' dod''8 dod'' dod'' dod''16 re'' |
re''4 re''8 re'' re'' re'' re'' re''16 re'' |
re''4 re'' sol''8. sol''16 fa''8 mi'' |
fa''2( mi'') |
re''2 la'4 |
re''4. re''8 re''4 |
do'' la'( si') si'2 re''4 |
sol''4. sol''8 sol''4 |
fa'' re''( mi'') |
mi''4. mi''8 fa'' mi'' |
re''4 do''8[ re'' do'' sib'] |
la'2 do''8 re'' |
sib'4. la'8 la' sol' |
la'4 sol' do'' |
<< \original sib'2 \pygmalion fa'2 >> fa''4 |
mi''2. |
fa''4. do''8 re''4 |
sib' sol' do'' |
la' si'2 |
do''4 do''( re'') |
mi''4.\melisma re''8[ mi'' fad''] |
sol''2\melismaEnd sol''4 |
sol'4.\melisma sol'8[ la' si'] |
la'4. la'8[ si' do''] |
si'4. si'8[ do'' re''] |
do''4. si'8[ do'' re''] |
mi''4\melismaEnd re''4. do''8 si'2 r4 |
r r re'' |
<<
  \original { do''4 si'2 do''4 sol'( la') | }
  \pygmalion { sol''4 fa''2 mi''4 re''8[\melisma mi''] do''[ re'']\melismaEnd | }
>>
si'4.\melisma sol'8[ la' si'] |
do''4\melismaEnd do''8 sol'[\melisma la' sib'] |
la'4. la'8[ si' do''] |
si'4. si'8[ do'' re''] |
do''4. do''8[ re'' mi''] |
fa''4\melismaEnd re''4. do''8 do''1\fermata |
