\clef "dessus" R1*4 |
re''1 |
re''2. re''4 do''1 |
do''2. do''4 re''2. re''4 |
mi''2 do''4 si' |
la'1 si'\fermata |
R2.*21\allowPageTurn R1\allowPageTurn
R2.*21\allowPageTurn R1*14\allowPageTurn
R2.*33\allowPageTurn R4*7
