\clef "viole1" R1*4 |
sol'1 |
re'2. si4 do'1 |
do'2. do'4 sol'2. re'4 |
do'2. mi'4 |
la2 la' sol'1\fermata |
R2.*21\allowPageTurn R1\allowPageTurn
R2.*21\allowPageTurn R1*14\allowPageTurn
R2.*33\allowPageTurn R4*7
