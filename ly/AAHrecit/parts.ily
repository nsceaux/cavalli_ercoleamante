\piecePartSpecs
#(if (eqv? #t (ly:get-option 'pygmalion))
     `((dessus #:score-template "score-dessus-voix")
       (parties #:score-template "score-parties-voix")
       (violes #:score "score-violes-voix")
       (basse #:score-template "score-basse-continue-voix"))
     `((dessus #:score-template "score-dessus-voix")
       (parties #:score-template "score-parties-voix")
       (basse #:score-template "score-basse-voix")))
