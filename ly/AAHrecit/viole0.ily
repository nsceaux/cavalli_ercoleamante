\clef "viole0" R1*12 |
%{sol'4 si' la' |
la'4. si'8 do''4 |
la' re''2 |
re'' dod''4 |
re''4. do''8 sib'4 |
sol' do'' la'8 la' |
do''2 la'4 |
sib'4 sib'4. la'8 |
la'2 do''4 |
sib'2 re''4 sol'2 sol''4 |
mi''2 la'4 |
re''2. |
re''4 re' re'' |
mi'' mi' la' |
la'8 sol' la'4 mi'8 sol' |
fa' sol' la'4 sol' |
la'2. |
re'2 fa'4~ |
fa'8 sol' la'4. la'8 |
sib'4 la'2 |
la'4 r r2 |%}
R2.*21 R1 |
r4 r fad'4 |
sol' sol'4. la'8 |
si' do'' re''4 si' |
mi'' re''4. do''8 |
do''2 la'4 |
do'' sib' la'8 sol' |
fa'2 sol'4 |
la'2 re''4 |
do'' sol'4. sol'8 |
si'4. do''8 re''4 |
sol'2 fa'4 |
sib' sol' do'' sib'16 do'' re''8 do''4. sib'8 |
la'2 do''4 |
la'8 sol' la' sib' do''4 |
la'2 sol'4 |
do''2 do''4 |
do''4 do''4. sib'8 |
sib'4 la' sol'~ |
sol'8 fa' fa'4 mi' fa'2. |
R1*14 R2. |
la'4. la'8 sol'4 |
sol' fad' sol' sol'2 si'4 |
si'2 do''4 |
re'' si' do'' |
do''4. la'8 la'4~ |
la'4 sol'2 |
fa' sol'4 |
fa' mi' fa'~ |
fa' mi' la' |
fa' sol'4. la'8 |
sol'4 do''2 |
la'4 fa'4. mi'8 |
sol'2 mi'4 |
fa' sol'2 |
sol'4 la' si'! |
do'' sol' do'' |
si' sol'2~ |
sol'2. |
fa'4 mi' fa' |
sol' fad' sol' |
sol'4. sol'8 la' si' |
do'' sol' sol'4 fad' sol'2 fad'4 |
sol' re' sol'~ |
sol' fa' sol' mi'2 fad'4 |
sol'2.~ |
sol'2 fa'8 mi' |
fa'2 mi'8 do' |
sol'2 re'4 |
la'4. sol'8 fa' mi' |
la'4 sol'2 sol'1\fermata |
