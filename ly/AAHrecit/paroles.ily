Ma voi che più tar -- da -- te in -- cli -- te De -- e?
Us -- ci -- te us -- ci -- te us -- ci -- te ad in -- chi -- na -- re
An -- na An -- na la gran re -- gi -- na, __
che le bell’ al -- me on -- de spe -- rar si dé -- e
che la se -- rie di -- vi -- na
de’ vostr’ al -- ti ne -- po -- ti il ciel il ciel con -- fer -- mi
am -- bo so -- no di lei
am -- bo so -- no di lei ram -- pol -- li, ram -- pol -- li, e ger -- mi.
Us -- ci -- te us -- ci -- te a fe -- steg -- gia -- re
a fe -- steg -- gia -- re
ch’in sì degn’ al -- le -- grez -- za a vo -- stri bal -- li
nel -- le ce -- ru -- bee val -- li
già ce -- de ce -- de il cam -- po os -- se -- qui -- o -- so il ma -- re,
e poi -- ché qual do -- po guer -- rie -- ri ho -- no -- ri
del -- la bel -- tà fu spo -- so Er -- co -- le al fi -- ne,
tal do -- po mil -- le al -- lo -- ri
e nel pri -- mo con -- fi -- ne
di sua flo -- ri -- da e -- ta -- de il Re il Re de’ Gal -- li,
su que -- ste sce -- ne a i lie -- ti Fran -- chi in -- nan -- te
per ac -- cres -- cer di -- let -- ti
ri -- pren -- da og -- gi i co -- tur -- ni Er -- co -- le a -- man -- te,
e ve -- da e ve -- da ogn’ un,
e ve -- da e ve -- da ogn’ un, che de -- si -- ar non sa
un e -- ro -- i -- co va -- lo -- re
qui giù qui giù pre -- mio mag -- gio -- re
che di go -- der in pa -- ce al -- ma bel -- tà
che di go -- der in pa -- ce al -- ma bel -- tà.
