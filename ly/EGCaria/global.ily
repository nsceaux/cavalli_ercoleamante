\key re \minor
\digitTime\time 3/4 \midiTempo#160 s2.*3
\measure 6/4 s1.
\measure 3/4 s2.*2
\measure 6/4 s1.
\measure 3/4 s2.
\measure 6/4 s1.*2
\measure 3/4 s2.*4
\measure 6/4 s1.*7
\measure 3/4 s2.
\time 4/4 \midiTempo#100 s1*3
\digitTime\time 3/4 \midiTempo#160 s2.*5 \measure 6/4 s1. \bar "|."
\endMarkSmall\markup { Segue sub[ito] il coro a 4 }
