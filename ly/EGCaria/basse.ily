\clef "basse" r4 do' si |
do' sib la |
sib2 sib4 |
lab sol2 lab4 sol2 |
lab4. sol8 fa4 |
sol fa mib |
sib2. mib |
re |
do sib, |
sib la sol |
fa |
mib |
re |
do sib, |
do re |
mib2 do re |
sol, sol4 mi re mi |
fa2. re4 do re |
mib2. do4 si, do |
re2. sol,2 do4 |
re2. |
si,1 |
do2 mib |
fa sol |
r4 do' sib |
lab2. |
sol2 mib4 |
fa2 fa4 |
sol2 fa4 |
sol2. do1*3/4\fermata |
