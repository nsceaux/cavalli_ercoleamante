\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2.*10\break s2.*10\break s2.*12\break s2. s1*3\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
