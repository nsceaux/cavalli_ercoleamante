E se pur ne -- gli es -- tin -- ti
di ge -- ne -- ro -- si -- tà pre -- gio ri -- ma -- ne,
per -- met -- ti o ge -- ni -- to -- re,
che do -- po ha -- ver io tan -- to ahi las -- sa in -- va -- no
per ven -- di -- car -- ti o -- pra -- to
ce -- da ce -- da al vo -- ler del fa -- to, __
e che non già quest’ al -- ma,
ma sol di lei la sven -- tu -- ra -- ta sal -- ma
per l’i -- ni -- quo l’i -- ni -- quo ti -- ran -- no
per cui gra -- to mi fo -- ra
più del ta -- la -- mo il ro -- go
di sfor -- za -- ti i -- me -- ne -- i sot -- ten -- tri sot -- ten -- tri al gio -- go.
