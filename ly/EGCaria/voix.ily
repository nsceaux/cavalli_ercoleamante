\clef "vbas-dessus" <>^\markup\character Iole
r4 mib'' re'' |
mib'' re'' do'' |
re'' sol' sol' |
do'' sib'2 do''4 sib'2 |
do''2. |
sib'4. lab'8 sol'4 |
sol'( lab'2) sol'2 sib'4 |
sib'2 sib'4 |
mib''4 la'4. sib'8 sib'2 sib'4 |
re'' re'' mib'' do''2 re''4 |
sib' sib' mib'' |
la' la' sib' |
sol'2 sol'4 |
fa' sib' sib' |
mib'4. mib'8 re'4 re'2 re'4 |
mi'2 mi'4 fad'4. fad'8 sib'4 |
sol'2 sol' sol'4. fad'8( |
sol'2) si'4 do''2. |
la'4 sol' la' sib'2 sib'4 |
sol' fad' sol' la'2. |
fad'4 mi' fad' sol'2 sol'4 |
fad'2 fad'4 |
r4 sol'8 sol' re'' re''16 re'' re''8 re''16 do'' |
mib''8 mib'' r mib'' do''4 do''8 do''16 do'' |
do'' do'' do'' re'' re''8 re''16 re'' si'8 si' r4 |
r do'' re'' |
mib'' re''4. do''8 |
si'4 sol' do'' |
la' la' fa'' |
si'4 do''4. re''8 |
mib''4( re''2) do''1*3/4\fermata |
