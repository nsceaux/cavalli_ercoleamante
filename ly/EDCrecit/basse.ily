\clef "basse" fa,1~ |
fa, |
sib, |
sib4 la8 sol fa2 |
do4 si, <<
  \original {
    do2 |
    do la, |
    sib, do fa,1\fermata |
  }
  \pygmalion {
    do8 sib,16 la, sol, fa, mi, re, |
    do,8 re,16 mi, fa,8 sol, la, fa, sol, la, |
    sib, la, sib, sol, do4 do, fa,1\fermata |
  }
>>
