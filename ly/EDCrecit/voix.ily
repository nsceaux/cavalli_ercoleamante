\clef "vbas-dessus" <>^\markup\character Giunone
r4 la'8 la' la' la' la' fa' |
do''4 do'' do'' do''8 re'' |
sib'4 sib' r2 |
\ffclef "vbasse" <>^\markup\character Nettuno
sib8 sib la la16 sol fa8 fa r16 fa mi re |
do4 si,8 do do4 do |
r4 sol fa2 |
re4 mi8 fa fa4. mi8( fa1\fermata) |
