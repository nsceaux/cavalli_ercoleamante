\tag #'(iole deianira licco) {
  Dall’ oc -- ca -- so
  dall’ oc -- ca -- so a gl’E -- o -- i
}
\tag #'hyllo {
  Dall’ oc -- ca -- so a gl’E -- o -- i
}
Ah non sia chi non pian -- ga,
ch’og -- gi il sol de gl’e -- ro -- i
\tag #'deianira { ohi -- mè, ohi -- mè, }
\tag #'(iole licco hyllo) { es -- tin -- to, }
es -- tin -- to, 
ohi -- mè,
\tag #'iole { ohi -- mè, }
\tag #'hyllo { ohi -- mè, ohi -- mè, }
ri -- man -- ga.

\tag #'(iole deianira licco) {
  Dall’ oc -- ca -- so
  dall’ oc -- ca -- so a gl’E -- o -- i
}
\tag #'hyllo {
  Dall’ oc -- ca -- so a gl’E -- o -- i
}
Ah non sia chi non pian -- ga,
\tag #'(iole deianira hyllo) { non sia chi } non pian -- ga.
