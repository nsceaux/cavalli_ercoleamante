<<
  \tag #'iole {
    \clef "vsoprano" r2 mi''4 mi'' mi'1 |
    mi' r |
    r4 mi''8 re'' do''4 re''8 mi'' |
    si'2 si' |
    R1 |
    re''1 |
    r4 do'' do'' do''8 si' |
    si'2 si' |
    re''4 re'' re'' re''8 mi'' |
    do''2 do''4 mi'' |
    la' la' r la' |
    si' si' r sol'' |
    do''2 r4 re'' |
    sol'2. la'4 sol'1 sol' |
    r2 mi''4 mi'' mi'1 |
    mi' r |
    r4 mi''8 re'' do''4 re''8 mi'' |
    si'2 si' |
    R1 |
    re''1 |
    r4 do'' do'' do''8 re'' |
    si'2 si' |
    r2 r4 re'' |
    re'' re''8 mi'' do''2( si'1) dod''\fermata |
  }
  \tag #'deianira {
    \clef "vsoprano" r2 do''4 do'' do'1 |
    do' r |
    r4 do''8 si' la'4 si'8 do'' |
    sold'4 sold' r2 |
    mi''1 |
    r4 sold' sold' sold'8 la' |
    la'2 la' |
    R1 |
    la'4 la' la' fa'8 sol' |
    sol'2 sol'4 do'' |
    fa'2 r4 re'' |
    sol'2 r4 mi'' |
    la'4 la' r fa'' |
    si' sol' do''1 si'2( do''1) |
    r2 do''4 do'' do'1 |
    do' r |
    r4 do''8 si' la'4 si'8 do'' |
    sold'4 sold' r2 |
    do''1 |
    r4 sold' sold' sold'8 la' |
    la'2 la' |
    R1 |
    r4 la' la' la'8 si' |
    sold'2\melisma la'1\melismaEnd sold'2( la'1)\fermata |
  }
  \tag #'licco {
    \clef "valto" R1*2 |
    r2 la'4 la' la1 |
    la2 r |
    r4 mi'8 fad' sold'4 fad'8 mi' |
    la'2 la' |
    R1 |
    fa' |
    r4 mi' mi' mi'8 fa' |
    re'2 re' |
    mi'4 mi' mi' do'8 re' |
    re'2 re'4 la' |
    mi' mi' r mi' |
    fa'4 fa' r la' |
    re'2 r4 fa' re'1 mi' |
    R1*2 |
    r2 la'4 la' la1 |
    la2 r |
    r4 mi'8 fad' \ficta sold'4 fad'8 mi' |
    la'2 la' |
    R1 |
    fa'1 |
    r4 mi' mi' mi'8 fa' |
    re'2 re'4 la |
    mi'\breve mi'1\fermata |
  }
  \tag #'hyllo {
    \clef "vtenor" R1*4 |
    r4 la8 si do'4 si8 la |
    mi'2 mi' |
    do'1 |
    r4 si si si8 do' |
    la2 la |
    sol4 sol sol sol8 la |
    fa2 fa4 la |
    mi4 mi r mi |
    fa fa r fa |
    sol2 r4 sol |
    la2 r4 fa |
    sol2 r4 fa sol1 do' |
    R1*4 |
    r4 la8 si do'4 si8 la |
    mi'2 mi' |
    do'1 |
    r4 si si si8 do' |
    la2 la |
    r4 sol sol sol8 la |
    fa1\melisma |
    mi\breve\melismaEnd la1\fermata |
  }
>>