\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \keepWithTag #'iole \includeNotes "voix"
    >> \keepWithTag #'iole \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'deianira \includeNotes "voix"
    >> \keepWithTag #'deianira \includeLyrics "paroles"
  >>
  \layout { }
}
