\clef "basse" la,1~ la,~ |
la,~ la, |
la, |
mi |
do |
si, |
la, |
sol, |
fa,2 fa |
mi1 |
fa2. fa4 |
sol2 r4 sol |
la2 r4 fa |
sol2. fa4 sol1 do |
la,1~ la,~ |
la,~ la, |
la, |
mi |
do |
si, |
la, |
sol,2 sol |
fa1 |
mi\breve la,1\fermata |
