\score {
  \new ChoirStaff <<
    \new Staff \with {
      instrumentName = \markup\character Iole
    } \withLyrics <<
      \global \keepWithTag #'iole \includeNotes "voix"
    >> \keepWithTag #'iole \includeLyrics "paroles"
    \new Staff \with {
      instrumentName = \markup\character Deianira
    } \withLyrics <<
      \global \keepWithTag #'deianira \includeNotes "voix"
    >> \keepWithTag #'deianira \includeLyrics "paroles"
    \new Staff \with {
      instrumentName = \markup\character Licco
    } \withLyrics <<
      \global \keepWithTag #'licco \includeNotes "voix"
    >> \keepWithTag #'licco \includeLyrics "paroles"
    \new Staff \with {
      instrumentName = \markup\character Hyllo
    } \withLyrics <<
      \global \keepWithTag #'hyllo \includeNotes "voix"
    >> \keepWithTag #'hyllo \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*8\break s1*6\pageBreak
        s1*9\break
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
