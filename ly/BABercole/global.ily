\key re \minor \midiTempo#120
\digitTime\time 3/4 s2.*3
\time 4/4 s1*2
\measure 2/1 s1*2
\digitTime\time 3/4
<<
  \original { s2.*2 \measure 7/4 s4*7 \bar "|." }
  \pygmalion { s2.*3 }
>>
\beginMark "Ritor[nello]"
\digitTime\time 3/4 s2.*4
\measure 6/4 s1.*2
\measure 10/4 s4*10 \bar "|."
\digitTime\time 3/4 s2.*13
\measure 6/4 s1.
\measure 3/4 <<
  \original { s2.*2 \measure 6/4 s1. \bar "|." }
  \pygmalion { s2.*3 }
>>
\beginMark "Ritor[nello]"
\digitTime\time 3/4 s2.*4
\measure 6/4 s1.*2
\measure 10/4 s4*10 \bar "|."
\original {
  \digitTime\time 3/4 s2.*8
  \measure 7/4 s4*7 \bar "|."
  \beginMark "Sinf[onia]" \digitTime\time 3/4 s2.*11 \measure 7/4 s4*7 \bar "|."
}
\time 4/4 s1*18
\measure 2/1 s1*2
\digitTime\time 3/4 s2.*9 \beginMark "Ritor[nello]" s2.*6
\measure 6/4 s1.*2
\measure 3/4 s2.
\measure 7/4 s4*7 \bar "|."
