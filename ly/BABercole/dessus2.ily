\clef "dessus" R2.*3 R1*4 << \original R4*13 \pygmalion R2.*3 >> |
re''4 re''8 do'' re'' la' |
sib'4. la'8 sib' do'' |
la'4. la'8 re'' do'' |
sib' sib' fa''4 mi'' |
re'' re''4. dod''8 re''2. |
r2*3/2 sol''4 sol''8 fa'' sol'' re'' |
mib''4. re''8 do''4 sib' la'2 sol'1\fermata |
<< \original R2.*19 \pygmalion R2.*18 >> |
re''4 re''8 do'' re'' la' |
sib'4. la'8 sib' do'' |
la'4. la'8 re'' do'' |
sib' sib' fa''4 mi'' |
re'' re''4. dod''8 re''2. |
r2*3/2 sol''4 sol''8 fa'' sol'' re'' |
mib''4. re''8 do''4 sib' la'2
sol'1\fermata |
\original {
  R4*31 |
  r4 r re''8 re'' |
  sib'4 do'' re''8 sol' |
  do''4 do'' mi''8 mi'' |
  do''4. re''8 mi'' la' |
  re''4 re'' fa''8 fa'' |
  mi''4 mi''8 re'' dod'' re'' |
  re''2. |
  r4 r re''8 re'' |
  sol'4 do'' sol'8 do'' |
  do''4 do'' do''8 do'' |
  sib'4 do'' re''8 re'' |
  do''4 do''8 sib' la'8. sib'16 sol'1\fermata |
}
R1*20 R2.*9 |
r4 r re''8 re'' |
si'4 do'' re''8 sol' |
do''4 do'' mi''8 mi'' |
do''4. re''8 mi'' la' |
re''4 re'' fa''8 fa'' |
mi''4 mi''8 re'' dod'' re'' |
re''2. r4 r re''8 re'' |
sol'4 do'' sol'8 do'' do''4 do'' do''8 do'' |
sib'4 do'' re''8 re'' |
do''4 do''8 sib' la'8. sib'16 sol'1\fermata |
