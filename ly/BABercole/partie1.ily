\clef "viole1" R2.*3 R1*4 << \original R4*13 \pygmalion R2.*3 >> |
R2.*2 |
re''4 re''8 la' la' la' |
fa'4. fa'8 sib'4 |
la' sol'2 la'2. |
sol'4 sol'8 la' sol' fa' mib'2 sol'4 |
sol'2. sol'4 re'2 re'1\fermata |
<< \original R2.*19 \pygmalion R2.*18 >> |
R2.*2 |
re''4 re''8 la' la' la' |
fa'4. fa'8 sib'4 |
la' sol'2 la'2. |
sol'4 sol'8 la' sol' fa' mib'2 sol'4 |
sol'2. sol'4 re'2
re'1\fermata |
\original {
  R4*31 |
  R2.*2 |
  r4 r sol'8 sol' |
  fa'4. sib'8 la' do'' |
  sib'4 sib' la'8 la' |
  sib'4 mi' la'8 la' |
  fad'4. sol'8 la' re' |
  sol'4 sol' sol'8 si' |
  do''4 sol' do''8 sol' |
  la'4 la' la'8 la' |
  fa'4 mi' re'8 sol' |
  do'4 do'8 mib' la la si1\fermata |
}
R1*20 R2.*9 |
R2.*2 |
r4 r sol'8 sol' |
fa'4. sib'8 la' do'' |
sib'4 sib' la'8 la' |
sib'4 mi' la'8 la' |
fad'4. sol'8 la' re' sol'4 sol' sol'8 si' |
do''4 sol' do''8 sol' la'4 la' la'8 la' |
fa'4 mi' re'8 sol' |
do'4 do'8 mib' la la si1\fermata |
