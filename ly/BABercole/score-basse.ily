\score {
  \new ChoirStaff \with { \haraKiriFirst } <<
    \new Staff << \global \includeNotes "basse1" >>
    \new Staff \with { \tinyStaff } \withLyrics <<
      \global \keepWithTag #'basse \includeNotes "voix"
    >> \keepWithTag #'basse { \set fontSize = #-2 \includeLyrics "paroles" }
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
    >>
  >>
  \layout { }
}
