\clef "basse" sol2 sol8 re |
mib2. |
re |
sib,1 |
sol2 mib |
sib2 la sol1 |
sol2 sol8 re |
mib2 do4 |
re2. \original { sol,1\fermata | }
sol,2.~ |
sol, |
re'4 re'8 do' re' la |
sib4. sib8 sol4 |
re la2 re2. |
sol4 sol8 fa sol re mib2 si,4 |
do4. re8 mib4 do re2 sol,1\fermata |
sol2 la4 |
sib2. |
re |
mi |
fa |
sol |
la |
sib~ |
sib~ |
sib |
la |
re'2~ re'8 la |
sib2 sol4 |
la2. re4 re8 mi fad re |
sol2 sol8 re |
mib2 do4 |
re2. \original sol, |
sol,2.~ |
sol, |
re'4 re'8 do' re' la |
sib4. sib8 sol4 |
re la2 re2. |
sol4 sol8 fa sol re mib2 si,4 |
do4. re8 mib4 do re2 sol,1\fermata |
\original {
  sol2 re4 |
  mib4. re8 do fa |
  sib,2 la,4 |
  sol,2 fad,4 |
  sol, la,2 |
  re sol4 |
  do4. re8 mi do |
  fa2 sib,4 |
  mib do re sol,1\fermata |
  sol,2. |
  sol |
  do2 do'8 do' |
  la4. sib8 do' fa |
  sib4 sib fa8 fa |
  sol4 sol8 sol la la, |
  re2. |
  sol, |
  do |
  fa, |
  sib,2 sib,8 sib, |
  do4. do8 re re, sol,1\fermata |
}
sib2 la |
sol1 |
fa2 mib~ |
mib4 re do2 |
sib, fa~ |
fa re |
mib do |
sol~ sol |
do~ do |
fa sol |
lab sib |
do'4. fa8 sol4 do |
fa1~ |
fa | \allowPageTurn
sib2 la~ |
la1 |
sol2. fa4 |
mib2 do4 sib, |
la,1 sol,\fermata |
sol2 re4 |
mib4. re8 do fa |
sib,2 la,4 |
sol,2 fad,4 |
sol, la,2 |
re sol,4 |
do4. re8 mi do |
fa2 re4 |
mib do re |
sol,2. |
sol |
do2 do'8 do' |
la4. sib8 do' fa |
sib4 sib fa8 fa |
sol4 sol8 sol la la, |
re2. sol, |
do fa, |
sib,2 sib,8 sib, |
do4. do8 re re, sol,1\fermata |
