\clef "basse" R2.*3 R1*4 << \original R4*13 \pygmalion R2.*3 >> |
R2.*2 |
re'4 re'8 do' re' la |
sib4. sib8 sol4 |
re la2 re2. |
sol4 sol8 fa sol re mib2 si,4 |
do4. re8 mib4 do re2 sol,1\fermata |
<< \original R2.*19 \pygmalion R2.*18 >> |
R2.*2 |
re'4 re'8 do' re' la |
sib4. sib8 sol4 |
re la2 re2. |
sol4 sol8 fa sol re mib2 si,4 |
do4. re8 mib4 do re2
sol,1\fermata |
\original {
  R4*31 |
  R2.*2 |
  r4 r do'8 do' |
  la4. sib8 do' fa |
  sib4 sib fa8 fa |
  sol4 sol8 sol la la, |
  re2 re'8 re' |
  si4 do' re'8 sol |
  do'4 do' do'8 do' |
  la4 sib do'8 fa |
  sib4 sib sib,8 sib, |
  do4. do8 re re, sol,1\fermata |
}
R1*20 R2.*9 |
R2.*2 |
r4 r do'8 do' |
la4. sib8 do' fa |
sib4 sib fa8 fa |
sol4 sol8 sol la la, |
re2 re'8 re' si4 do' re'8 sol |
do'4 do' do'8 do' la4 sib do'8 fa |
sib4 sib sib,8 sib, |
do4. do8 re re, sol,1\fermata |
