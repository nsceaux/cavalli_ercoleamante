Co -- me si bef -- fa A -- mor del po -- ter mi -- o!
A me cui ce -- de il mon -- do
fa -- rà con -- tra -- sto u -- na don -- zel -- la? oh di -- o!
Co -- me si bef -- fa A -- mor del mio de -- si -- o!

Dun -- que chi tan -- ti mo -- stri
vi -- de e -- san -- gui tro -- fei di sua for -- tez -- za
scem -- pio sa -- rà di fem -- mi -- nil fie -- rez -- za,
e tra -- fit -- to ca -- drà da un van de -- si -- o?
Co -- me si bef -- fa A -- mor del pian -- to mi -- o!
Co -- me si bef -- fa A -- mor del pian -- to mi -- o!

\original {
  Ah Cu -- pi -- do io non so già
  per -- ch’il ciel sof -- frir ti deg -- gia?
  Di Plu -- ton l’or -- ri -- da reg -- gia
  un di te più reo non ha.
}

Oh di qua -- le em -- pie -- tà
sa -- cri -- le -- go ti -- ran -- no ogn’ or ri -- em -- pi
il cre -- du -- lo tuo re -- gno?
Men -- tre ne’ di lui tem -- pi
l’a -- do -- ra -- te Cor -- ti -- ne
di gra -- zia, e di bel -- tà
non ce -- lan al -- tro al -- fi -- ne
ch’i -- do -- li a -- bo -- mi -- ne -- vo -- li quai so -- no
in -- te -- res -- se, per -- fi -- dia, or -- go -- glio, e sde -- gno.
Co -- sì av -- vien per Io -- le
che l’al -- tar del cor mi -- o
spar -- ga d’al -- ti so -- spir mal -- gra -- ti i fu -- mi,
e che vit -- ti -- ma in -- fau -- sta io mi con -- su -- mi.

Ah Cu -- pi -- do io non so già
per -- ch’il ciel sof -- frir ti deg -- gia?
Di Plu -- ton l’or -- ri -- da reg -- gia
un di te più reo non ha.
