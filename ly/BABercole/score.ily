\score {
  <<
    \new StaffGroup \with { \haraKiriFirst } <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
      \new Staff << \global \includeNotes "partie1" >>
      \new Staff << \global \includeNotes "partie2" >>
      \new Staff << \global \includeNotes "basse1" >>
    >>
    \new ChoirStaff \with { \haraKiriFirst } <<
      \new Staff \withLyrics <<
        \global \includeNotes "voix"
      >> \includeLyrics "paroles"
      \new Staff <<
        \global \includeNotes "basse"
        \includeFigures "chiffres"
        \origLayout {
          s2.*3 s1 s2 \bar "" \break
          s2 s1*2 s2.*3 s1\break
          s2.*10 s1\pageBreak
          s2.*7\break s2.*9\break s2.*3 s2.*10 s1\break
          s2.*8\break s2. s1\pageBreak
          s2.*12 s1\break
          s1*4 s2 \bar "" \break s2 s1*4\pageBreak
          s1*3\break s1*5\break s1*3 s2.*3\break s2.*8\break
        }
        \modVersion {
          s2.*3 s1*4 s2.*3 \original s1 \break
          %% ritornello
          s2.*10 s1\break
          s2.*18 \original s2. \break
          %% ritornello
          s2.*10 s1\break
          s2.*9 s1\break
        }
      >>
    >>
  >>
  \layout { }
  \midi { }
}
