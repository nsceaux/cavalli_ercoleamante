\clef "viole2" R2.*3 R1*4 << \original R4*13 \pygmalion R2.*3 >> |
R2.*2 |
fa'4 fa'8 do' fa' fa' |
re'4. re'8 mi'4 |
fa'8 re' mi'2 fad'2. |
re'4 re'8 re' re' re' sib!2 re'4 |
do'2. mib'4 la2 si1\fermata |
<< \original R2.*19 \pygmalion R2.*18 >> | \allowPageTurn
R2.*2 |
fa'4 fa'8 do' fa' fa' |
re'4. re'8 mi'4 |
fa'8 re' mi'2 fad'2. |
re'4 re'8 re' re' re' sib!2 re'4 |
do'2. mib'4 la2
si1\fermata |
\original {
  R4*31 |
  R2.*2 |
  r4 r mi'8 mi' |
  la'4 fa' mi'8 fa' |
  re'4 re' do'8 la |
  mi'4 sib' mi'8 re' |
  re'2 fad'8 fad' |
  re'4. mi'8 si re' |
  do'4 do' mi'8 mi' |
  fa'4 fa' fa'8. do'16 |
  re'4. do'8 sib sib |
  sol4 sol8 sol re' re' re'1\fermata | \allowPageTurn
}
R1*20 R2.*9 |
R2.*2 |
r4 r mi'8 mi' |
la'4 fa' mi'8 fa' |
re'4 re' do'8 la |
mi'4 sib' mi'8 re' |
re'2 fad'8 fad' re'4. mi'8 si re' |
do'4 do' mi'8 mi' fa'4 fa' fa'8. do'16 |
re'4. do'8 sib sib |
sol4 sol8 sol re' re' re'1\fermata |
