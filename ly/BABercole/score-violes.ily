\score {
  <<
    \new StaffGroup \with { \haraKiriFirst } <<
      \new Staff << \global \includeNotes "partie1" >>
      \new Staff << \global \includeNotes "partie2" >>
      \new Staff << \global \includeNotes "basse1" >>
    >>
    \new ChoirStaff \with { \haraKiriFirst } <<
      \new Staff \with { \tinyStaff } \withLyrics <<
        \global \keepWithTag #'basse \includeNotes "voix"
      >> \keepWithTag #'basse { \set fontSize = #-2 \includeLyrics "paroles" }
      \new Staff <<
        \global \includeNotes "basse"
        \includeFigures "chiffres"
        \modVersion {
          s2.*3 s1*4 s2.*3 \original s1 \break
          %% ritornello
          s2.*10 s1\break
          s2.*18 \original s2. \break \allowPageTurn
          %% ritornello
          s2.*10 s1\break
          s2.*9 s1\break
        }
      >>
    >>
  >>
  \layout { }
  \midi { }
}
