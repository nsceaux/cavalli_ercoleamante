\clef "dessus" R2.*3 R1*4 << \original R4*13 \pygmalion R2.*3 >> |
R2. |
sol''4 sol''8 fa'' sol'' mi'' |
fa''4. mi''8 fa'' la'' |
re''4. re''8 sol''4 |
fa'' mi''2 re''4 re''8 do'' re'' la' |
sib'4. la'8 sib' re'' sol'4. sol'8 re'' sol'' |
do'' re'' mib'' fa'' sol''4 sol'' sol''4. fad''8 sol''1\fermata |
<< \original R2.*19 \pygmalion R2.*18 >> |
R2. |
sol''4 sol''8 fa'' sol'' mi'' |
fa''4. mi''8 fa'' la'' |
re''4. re''8 sol''4 |
fa'' mi''2 re''4 re''8 do'' re'' la' |
sib'4. la'8 sib' re'' sol'4. sol'8 re'' sol'' |
do'' re'' mib'' fa'' sol''4 sol'' sol''4. fad''8 sol''1\fermata |
\original {
  R4*31 |
  R2. |
  r4 r sol''8 sol'' |
  mi''4 fa'' sol''8 do'' |
  fa''4 fa'' la''8 la'' |
  fa''4. sol''8 la'' re'' |
  sol''4 sol''8 fa'' mi'' fa'' |
  re''2. |
  r4 r sol''8 sol'' |
  mi''4 fa'' sol''8 do'' |
  fa''4 fa'' fa''8 fa'' |
  re''4 mi'' fa''8 sib' |
  mib''4 fa''8 sol'' fad''8. sol''16 sol''1\fermata |
}
R1*20 R2.*9 |
R2. |
r4 r sol''8 sol'' |
mi''4 fa'' sol''8 do'' |
fa''4 fa'' la''8 la'' |
fa''4. sol''8 la'' re'' |
sol''4 sol''8 fa'' mi'' fa'' |
re''2. r4 r sol''8 sol'' |
mi''4 fa'' sol''8 do'' fa''4 fa'' fa''8 fa'' |
re''4 mi'' fa''8 sib' |
mib''4 fa''8 sol'' fad''8. sol''16 sol''1\fermata |
