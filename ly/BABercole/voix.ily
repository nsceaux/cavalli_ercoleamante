\clef "vbasse" <>^\markup\character Ercole
sol4 sol8 fad sol re |
mib4. re8 re do |
re2 re4 |
r8 fa sib4 r8 sib sib sib |
sol sol16 sol sol8 sol mib mib mib mib |
sib4 sib8 do' la2 sol1 |
sol4 sol8 fad sol re |
mib4. mib8 fa sol |
re2. <<
  \original { sol,1\fermata | R4*34 | }
  \pygmalion { sol,2.\fermata | R4*31 }
>>
sol4 sol8 sol la sib |
sib4 sib re8 re |
re4 re mi |
mi4. mi8 mi fa |
fa2 fa4 |
sol4. sol8 sol4 |
la8 la la la la sib |
sib4 sib sib8 re' |
sib4 sib4. sib8 |
sol4. sol8 fad sol |
la2 la4 |
re' re'8 do' re' la |
sib4. sib8 do' re' |
la2. re |
sol4 sol8 fad sol re |
mib4. mib8 mib fa |
re2. sol, |
<< \original R4*34 \pygmalion R4*31 >> |
\original {
  r4 sib4. re'8 |
  sol4 sol8 fa sol la |
  sib2 fad8 fad |
  sol4. sol8 la4 |
  sib8[ sol] la2 |
  re sol8 sol |
  mi4 fa sol8 do |
  fa4 fa re8 re |
  mib4. do8 re re sol,1\fermata |
  R4*40 |
}
r4 sib r8 la la la16 sib |
sol4. sol8 sol sol sol fa |
fa fa16 fa fa8 sol mib4 mib8 mib |
mib8. re16 re8 mib do2 |
sib, fa8 fa16 fa fa8 mi |
fa fa fa fa re4 re8 re |
mib4 mib8 mib do do16 re mib8 do |
sol4. sol8 sol sol mib sol |
do4 do do'8 do'16 sib lab8 sol |
fa8. fa16 fa8 mi sol sol r sol16 mib |
lab8 lab r fa sib sib r sol |
do' do' r fa sol4 do |
r4 r8 fa fa fa fa fa |
fa4 fa fa8 fa fa fa16 fa |
sib4 sib fad8 fad fad fad16 sol |
la4 r8 do' la4 la8 sib |
sol4 sol sol8 fa fa fa16 sol |
mib4 mib do sib,8 la, |
la,1 sol,\fermata |
r4 sib4. re'8 |
sol4 sol8 fa sol la |
sib2 fad8 fad |
sol4. sol8 la4 |
sib8[ sol] la2 |
re sol8 sol |
mi4 fa sol8 do |
fa4 fa re8 re |
mib4. do8 re re |
sol,2.\fermata |
R4*37 |

