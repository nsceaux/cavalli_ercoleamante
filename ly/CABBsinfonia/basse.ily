\clef "basse" do4 la, fa,2 |
do4. sib,8 la,4 |
re re2 sib,2 do4 |
fa,4. sol,8 la,4 sib, do2 |
fa,2. |
