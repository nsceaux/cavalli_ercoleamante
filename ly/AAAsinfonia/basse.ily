\clef "basse" do1 mi2 sol do1 |
do'4 si la |
si! la sol la sol fa |
sol fa mi fa mi re |
mi re do re do si, |
do si, do re2. sol, |
sol,1 si,2 re sol,1 |
sol4 fa mi fa mi re |
mi re do re do si, |
do si, la, si, la, sol, |
la, sol, fa, sol, fa, mi, |
fa, mi, fa, sol,2. do, |
r8 do' si8. do'16 la4 re' |
sol4. fa8 mi re do si, |
la,4 re sol,2 |
r8 sol fa8. sol16 mi4 la |
re4. do8 sib, la, sol, fa, |
sol,4 la, re,2 |
r r8 sol fa8. sol16 |
mi2 r8 do' si8. do'16 |
la2 r8 sib la8. sib16 |
sol2 r8 la sol8. la16 |
fa2 r8 sol fa8. sol16 |
mi8 mi re8. mi16 do8 do si,8. do16 |
la,8 la, sol, la, fa, fa, mi,8. fa,16 |
sol,4 do, sol,2 do,1\fermata |
do1 |
do |
do |
do4 do do do |
do do do do |
do do do do |
do1\fermata |
