\key do \major
\time 4/4 \midiTempo#240 \measure 3/1 s1*3
\digitTime\time 3/4 s2.
\measure 6/4 s2.*6
\measure 9/4 s2.*3
\time 4/4 \measure 3/1 s1*3
\digitTime\time 3/4 \measure 6/4 s2.*8
\measure 9/4 s2.*3 \bar ".|:"
\segnoMark \time 4/4 \midiTempo#120 s1*13
\measure 2/1 s\breve \bar ":|."
\measure 4/4 s1*7 \bar "|."
