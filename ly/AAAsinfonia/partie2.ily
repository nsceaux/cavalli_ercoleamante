\clef "viole2" do'1 do'2 re' do'1 |
do'4 re' mi' |
si do' mi' la si re' |
sol la do' fa sol si |
mi fa la re' la si |
la sol do' la2. sol |
sol1 sol2 la sol1 |
si4 re' mi' la do' re' |
sol si do' fa la si |
la si do' si do' mi' |
do' mi' fa' si re' mi' |
re' mi' la re'2. do' |
r8 do' re'8. do'16 mi'4 re' |
re'4. fa'8 si re' mi' re' |
la4 la sol2 |
r8 re' la8. re'16 mi'4 mi' |
re'4. la8 re' la sib fa |
sib4 la la2 |
r r8 si la8. si16 |
sol2 r8 mi' re'8. mi'16 |
do'2 r8 re' do'8. re'16 |
sib2 r8 do' sib8. do'16 |
la2 r8 si la8. si16 |
sol8 sol si8. sol16 do'8 mi' sol'8. mi'16 |
la'8 do' mi'8. do'16 fa'8 fa' do'8. fa'16 |
re'4 do' re'2 do'1\fermata |
sol1 |
sol |
sol |
sol4 sol sol sol |
sol sol sol sol |
sol sol sol sol |
sol1\fermata |
