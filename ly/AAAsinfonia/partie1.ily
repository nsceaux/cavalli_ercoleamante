\clef "viole1" sol'1 sol'2 sol' sol'1 |
sol'4 sol' la' |
re' fa' sol' do' mi' fa' |
si re' mi' la do' re' |
sol si do' si do' re' |
do' re' sol re'2. re' |
sol'1 sol'2 re' re'1 |
re'4 fa' sol' re' mi' fa' |
do' re' mi' si do' re' |
do' re' fa' re' fa' sol' |
fa' sol' la' mi' fa' sol' |
fa' sol' do' sol'2. sol' |
r8 sol' si'8. sol'16 la'4 la' |
sol'4. la'8 mi' fa' sol' la' |
mi'4 re' re'2 |
r8 sol' re'8. sol'16 sol'4 la' |
la'4. la'8 sib' fa' sol' la' |
sol'4 mi' fad'2 |
r2 r8 re' re'8. re'16 |
do'2 r8 sol' sol'8. sol'16 |
fa'2 r8 fa' fa'8. fa'16 |
mi'2 r8 mi' mi'8. mi'16 |
re'2 r8 re' re'8. re'16 |
mi'8 mi' fa'8. mi'16 sol'8 do' re'8. do'16 |
mi'8 la si8. la16 do'8 do' mi'8. do'16 |
sol'4 sol' sol'2 sol'1\fermata |
do'1 |
do' |
do' |
do'4 do' do' do' |
do' do' do' do' |
do' do' do' do' |
do'1\fermata |
