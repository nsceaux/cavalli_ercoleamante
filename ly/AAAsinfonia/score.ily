\score {
  \new StaffGroup <<
    \new Staff << \global \includeNotes "dessus1" >>
    \new Staff << \global \includeNotes "dessus2" >>
    \new Staff << \global \includeNotes "partie1" >>
    \new Staff << \global \includeNotes "partie2" >>
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      { s1*3 s2.*10 s1*3 s2.*11 s1*15
        \endMark\markup { une autre fois }
        \modVersion\break }
      \origLayout {
        s1*3 s2.*10 s1*3\break s2.*11 s1*2\pageBreak
        s1*7\break s1*6\pageBreak
        s1*4\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
