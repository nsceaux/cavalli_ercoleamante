\clef "dessus" mi''1 sol''2 re'' mi''1 |
mi''4 re'' do'' |
re'' do'' si' do'' si' la' |
si' la' sol' la' sol' fa' |
sol' fa' mi' fa' mi' re' |
mi' sol'2 sol' fad'4 sol'2. |
re''1 si'2 la' si'1 |
si'4 la' sol' la' sol' fa' |
sol' fa' mi' fa' mi' re' |
mi'' re'' do'' re'' do'' si' |
do'' si' la' si' la' sol' |
la' do''2 do'' si'4 do''2. |
r8 mi'' re''8. mi''16 mi''4 fad'' |
sol''4. re''8 mi'' si' do'' re'' |
do''4 la' si'8 re'' do''8. re''16 |
si'8 si' la'8. si'16 si'4 dod'' |
re''4. mi''8 re'' do'' sib' la' |
re''4 dod'' re''2 |
r8 re'' do''8. re''16 si'2 |
r8 mi'' re''8. mi''16 do''2 |
r8 la'' sol''8. la''16 fa''2 |
r8 sol'' fa''8. sol''16 mi''2 |
r8 fa'' mi''8. fa''16 re''2 |
r8 si' si'8. si'16 do''8 sol'' sol''8. sol''16 |
la''8 mi'' mi''8. mi''16 fa''8 fa'' sol''8. la''16 |
re''4 mi'' re''2 mi''1\fermata |
r4 do''8. do''16 mi''8. mi''16 sol''8. sol''16 |
mi''8. mi''16 do''8. do''16 mi''8. mi''16 sol''8. sol''16 |
mi''8. mi''16 sol''8 sol''16 sol'' mi''8. mi''16 sol''8 sol''16 sol'' |
mi''8. mi''16 r4 r2 |
sol''4 sol'' sol'' sol'' |
mi'' mi'' mi'' mi'' |
mi''1\fermata |
