\clef "dessus" sol''1 mi''2 si' do''1 |
R2. |
sol''4 fa'' mi'' fa'' mi'' re'' |
mi'' re'' do'' re'' do'' si' |
do'' si' la' si' la' sol' |
la' re'' mi'' la'2. si' |
si''1 sol''2 fad'' sol''1 |
r2*3/2 re''4 do'' si' |
do'' si' la' si' la' sol' |
la'' sol'' fa'' sol'' fa'' mi'' |
fa'' mi'' re'' mi'' re'' do'' |
re'' sol'' la'' re''2. mi'' |
r8 sol'' sol''8. sol''16 do'''4 la'' |
si''4. la''8 sol'' fa'' mi'' sol'' |
sol''4 fad'' sol''2 |
r8 re'' fa''8. re''16 sol''4 mi'' |
fa''4. la''8 sol'' fa'' mi'' re'' |
mi''4 mi'' re''8 la'' sol''8. la''16 |
fad''2 r |
r8 sol'' fa''8. sol''16 mi''2 |
r8 fa'' mi''8. fa''16 re''2 |
r8 mi'' re''8. mi''16 do''2 |
r8 re'' do''8. re''16 si'2 |
r8 sol'' fa''8. sol''16 mi''8 mi'' re''8. mi''16 |
do''8 do'' si'8. do''16 la'4 do'' |
si'8 sol' do''2 si'4 do''1\fermata |
<>^"Trombe" do''8. do''16 mi''8. mi''16 sol''8. sol''16 mi''8. mi''16 |
do''8. do''16 mi''8. mi''16 sol''8. sol''16 mi''8. mi''16 |
sol''8 sol''16 sol'' mi''8. mi''16 sol''8 sol''16 sol'' mi''8. mi''16 |
sol''4 sol'' sol'' sol'' |
mi'' mi'' mi'' mi'' |
do'' do'' do'' do'' |
do''1\fermata |
