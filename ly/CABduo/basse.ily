\clef "basse" \original { r4 r } fa4 |
fa2. la4 sib do' |
fa2. mi |
re mi4 fa2 |
sol4 la fa |
fa4 sol2 |
do do'4 la2. |
sol mib2 do4 |
re2. sol |
do re |
mib4 do2 re2. sol, |
sol4 fa mib fa2. |
sib, |
la,4 sol, fa, do2. |
fa, |
fa4 re do sol2. |
do |
sib,4 mib2 fa2. |
re4 mib2 fa2. |
sib,2. |
do4 la, sol, re2. |
sol, |
fa,4 sib,2 do2. |
la,4 sib,2 do2. |
fa,1*3/4\fermata |
