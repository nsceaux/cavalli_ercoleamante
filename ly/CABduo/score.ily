\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \keepWithTag #'iole \includeNotes "voix"
    >> \keepWithTag #'iole \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'hyllo \includeNotes "voix"
    >> \keepWithTag #'hyllo \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2.*9\break s2.*11\break s2.*9\pageBreak
        s2.*10\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
