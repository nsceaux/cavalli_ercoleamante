\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \keepWithTag #'iole \includeNotes "voix"
    >> \keepWithTag #'iole \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'hyllo \includeNotes "voix" \clef "alto"
    >> \keepWithTag #'hyllo \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
    >>
  >>
  \layout { }
}
