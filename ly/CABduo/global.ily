\key fa \major \midiTempo#180
\beginMark "a 2" \digitTime\time 3/4
<<
  \original s2.
  \pygmalion { \partial 4 s4 }
>>
\measure 6/4 s1.*3
\measure 3/4 s2.*2
\measure 6/4 s1.*4
\measure 9/4 s2.*3
\measure 6/4 s1.
\measure 3/4 s2.
\measure 6/4 s1.
\measure 3/4 s2.
\measure 6/4 s1.
\measure 3/4 s2.
\measure 6/4 s1.*2
\measure 3/4 s2.
\measure 6/4 s1.
\measure 3/4 s2.
\measure 6/4 s1.*2
\measure 3/4 s2. \bar "|."
