A -- mor ar -- dor più ra -- ri
ac -- ce -- si ac -- ce -- si mai non ha,
che quel -- li on -- de del pa -- ri
le nos -- tre al -- me dis -- fà
d’av -- ver -- so ciel le lam -- pe
con -- tro di lui si sfor -- zi -- no,
ch’in ve -- ce, che l’a -- mor -- zi -- no,
l’ar -- ric -- chi -- ran
l’ar -- ric -- chi -- ran di __ vam -- pe
ch’in ve -- ce, che l’a -- mor -- zi -- no,
l’ar -- ric -- chi -- ran
l’ar -- ric -- chi -- ran di __ vam -- pe.
