<<
  \tag #'(iole basse) {
    \clef "vsoprano" \original { r4 r } la'4 |
    la'2. r2*3/2 |
    fa'4 la' sib' do'' sol'2 |
    r4 r re'' do'' la' la' |
    si' do''2 |
    do''4 si'( do'') |
    do''2 sol'4 la'2 la'4 |
    sib'4 do'' re'' sol'2 sol'4 |
    r re'' do'' sib'2.~ |
    sib'4\melisma la'4. sol'8 fad'4 sib'4. la'8 |
    sol'4 lab' sol'~ sol'\melismaEnd fad' sol' sol'2. |
    sol'4 la' sib' sib'2 la'4 |
    sib'2 sib'4 |
    do'' sib' la' sib'2 sib'8[ la'] |
    la'4. la'8 la'4 |
    la'4 si' do'' do'' si'( do''8[ re'']) |
    do''4. do''8 do''4 |
    re''4 do''4. sib'8 la'2. |
    re''4 do''4. sib'8 sib'4 la'( sib') |
    sib'2 sib'4 |
    mi' fad' sol' sol' fad'!\melisma sol'8[ la']\melismaEnd |
    sol'4. sol'8 sol'4 |
    la' sol'4. fa'8 mi'2. |
    la'4 sol'4. fa'8 fa'4 mi'( fa') |
    fa'2 fa'4 |
  }
  \tag #'hyllo {
    \clef "vtenor" \original { r4 r } do'4 |
    do'2. do'4 re' mi' |
    fa' do'2 r4 r sol' |
    fa' re' r r r fa' |
    mi' do'( re') |
    mi' re'( do') |
    do'2. r4 r do' |
    re'2 re'4 do' re' mib' |
    la2 la4 r sol' fa' |
    mib'2.~ mib'4\melisma re'4. do'8 |
    sib4 do'4. re'8 sib4\melismaEnd la sib sol2. |
    sib4 do' re' do'2 fa'4 |
    re'2 re'4 |
    do' re' mi' fa'2 mi'8[ fa'] |
    fa'4. fa'8 fa'4 |
    do' re' mib' mib' re'\melisma mib'8[ fa']\melismaEnd |
    mib'4. mib'8 mib'4 |
    fa' mib'4. re'8 do'2. |
    fa'4 mib'4. re'8 re'4 do'( re') |
    re'2 re'4 |
    sol la sib sib la\melisma sib8[ do']\melismaEnd |
    sib4. sib8 sib4 |
    do' sib4. la8 sol2. |
    do'4 sib4. la8 la4 sol( la) |
    la2 la4 |
  }
>>
