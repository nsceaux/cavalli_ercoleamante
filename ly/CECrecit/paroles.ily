Ah fu sem -- pre in a -- mor stol -- to con -- si -- glio
il cer -- car di sa -- pe -- re
pun -- to di più, che quel ba -- sta a go -- de -- re;
co -- pron l’in -- di -- che bal -- ze
sot -- to as -- pet -- to vil -- lan vis -- ce -- re d’o -- ro;
ma ben con -- tra -- ri -- o af -- fat -- to
l’a -- mo -- ro -- so ter -- re -- no
sot -- to u -- na su -- per -- fi -- cie pre -- zi -- o -- sa
sol cat -- ti -- va ma -- te -- ria ha in sé nas -- co -- sa.
On -- de chi vuo -- le in lui
gir sca -- van -- do tal’ or con mes -- ta mes -- ta pro -- va
più s’in -- ol -- tra a cer -- car
più s’in -- ol -- tra a cer -- car peg -- gio peg -- gio peg -- gio ri -- tro -- va;
ben lo di -- cea, che noi sa -- riam ve -- nu -- ti
a in -- con -- trar pe -- ne, e ri -- schi:
ah che d’Er -- co -- le i -- ra -- to
qual -- che stral ben ro -- ta -- to
par -- mi sen -- tir, ch’in -- tor -- no a me già fi -- schi.

Ah Lic -- co il cor ti man -- ca, ohi -- mè, che fia
di me sen -- za il tuo a -- iu -- to?

Ah De -- ia -- ni -- ra:
dun -- que, dun -- que tu tre -- mi?
Io non ho già pa -- u -- ra.

E in tan -- to tre -- mi.

Ma ve’; poi -- ché nel mon -- do
o -- gni co -- sa ha mi -- su -- ra;
forz’ è che l’ab -- bia an -- cor la mia bra -- vu -- ra
e sic -- co -- me tra quel -- le,
che fè ne -- mi -- co ciel sen -- za da -- na -- ri
chi ha quat -- tro sol -- di è ric -- co:
co -- sì per bra -- vo io so -- la -- men -- te spic -- co
fra tut -- ti quan -- ti li pol -- tron miei pa -- ri.

Dun -- que che far do -- vrem?

\original {
  N’han già can -- gia -- ti
  in gui -- sa tal que -- sti a -- bi -- ti vil -- la -- ni,
  che se guar -- din -- ghi an -- dre -- mo
  ad al -- tro non po -- trà, ch’al -- la fa -- vel -- la
  Er -- co -- le ri -- co -- no -- scer -- ne: per tan -- to
  av -- ver -- tir
}
\pygmalion {
  Av -- ver -- tir
}
ne con -- vie -- ne
che qual -- che bef -- fa, o croc -- chio
gra -- zie, ch’al -- li stra -- nier ver -- sa o -- gni cor -- te
non c’ir -- ri -- ti non c’ir -- ri -- ti a par -- la -- re, e di tal sor -- te
fa -- rem la guer -- ra la guer -- ra
fa -- rem la guer -- ra all’ oc -- chio.
