\clef "dessus"
\transpose do #(if (eqv? #t (ly:get-option 'pygmalion))
                   (ly:make-pitch -1 1)
                   (ly:make-pitch -1 0))
{
R1*6 |
R2. |
r4 sol' sib' fa' lab' sib' |
do'' lab'2 r4 fa' lab'! |
mib' sol' lab' sib'2 sib'4 la'4. sol'8 la'4 sib'2. |
r4 re' mib' fa'2 sib4 |
do'2 r4 r2. |
r4 do'' re'' mib''2 sib'4 |
do''4. mib''8 re''4 re'' mib'' fa'' |
do''4 re''2 |
mib''4 do'' re'' |
do''4. sib'8 la'4 sib' fa'4 sol'8. lab'16 |
sib'4 sib'4. sib'8 do''4 lab' do'' |
fa' sol' mib' lab'2 sol'4 |
sol' sib'4. fa'8 sol'2 sib'8 do'' |
re''8 do'' re'' mib'' fa''4 fa''8 fa' sol' la' sib' do'' |
re''4 sib' fa''~ |
fa''8 fa' la'4. la'8 |
sib'4 re'' sib'~ |
sib' mib'' do''~ |
do'' si'4. si'8 do''4 mib'' fa'' |
re''2. r4 sol' sib' |
lab' fa'2 r4 sib' mib'' |
do''4 la'4. do''8 sib'4 sol'4. sol'8 |
sol'4 lab' sol' do''4. sib'8 lab'4 |
do'' sol'2 sol'2. |
}
R1*30 R2.*3 R1*7 |
