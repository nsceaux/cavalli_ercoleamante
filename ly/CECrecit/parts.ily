\piecePartSpecs
#(if (eqv? #t (ly:get-option 'pygmalion))
     '((dessus #:score-template "score-dessus-voix")
       (violes #:score "score-violes")
       (basse #:score "score-basse"))
     '((basse #:score-template "score-voix")))
