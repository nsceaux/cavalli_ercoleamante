\clef "basse"
do1~ |
do2 sol |
sol, re~ |
re mib |
do re sol,1 |
<< \original sol,2. \pygmalion R2. >> |
\transpose do #(if (eqv? #t (ly:get-option 'pygmalion))
                   (ly:make-pitch -1 1)
                   (ly:make-pitch -1 0))
{
r4 do' sib lab4. lab8 sol4 |
fa2. r4 sib lab |
sol4. sol8 fa4 mib2. fa sib, |
sib,~ sib, |
fa,4 fa, sol, la,4. la,8 si,4 |
do2. r4 do re |
mib4. mib8 fa4 sol2 fa4 |
mib re2 |
do4 do' sib |
lab2 do'4 sib2. |
sib4 sol2 lab2. |
sib2 sol4 lab2. |
sib2. mib |
sib, sib |
sib |
sib4 la fa |
sib4 sib4. lab8 |
sol4 mib4. mib8 |
lab2 sol4 lab2. |
sol2.~ sol4 mib2 |
r4 fa4. fa8 sol4 sol sol |
la la4. la8 sib4 si4. si8 |
do'4 do' sib lab2 fa4~ |
fa4 sol2 do2. |
}
\pygmalion\allowPageTurn
sib,1 |
sol,2 fa,~ |
fa,4 mi, fa,2 |
do1~ |
do |
do2~ do4 re |
sol,1 |
sol2 lab |
la! sib |
re mib |
fa sib, |
si, do |
mib lab |
sol1 |
mib2 fa2 |
sol do |
do1~ |
do2 fa |
re mib |
fa sib, |
sib,1 |
sol,2 fa,~ |
fa, sol, |
sol1 |
mib |
fa |
fa2 sol |
do1 |
<<
  \original {
    fa2~ fa |
    fa1~ |
    fa2 sib, |
    sib,1 |
    sib, |
    sib, |
    mib |
    lab |
  }
  \pygmalion {
    fa2 lab |
  }
>>
sol1 |
do2. |
do'2 sib4 |
lab2. |
sol2 do4. re8 |
mib4 do re2 |
sol, r8 do' do' sib |
la4 fa8 fa sib4 sol8 sol |
do' do' do' sib lab4 fa |
sol fa sol2 do1\fermata |

