\clef "dessus"
\transpose do #(if (eqv? #t (ly:get-option 'pygmalion))
                   (ly:make-pitch -1 1)
                   (ly:make-pitch -1 0))
{
R1*6 |
R2. |
r4 mib'' sol'' do'' fa''4. mi''8 |
fa''2. r4 re'' fa'' |
sib' mib''4. re''8 mib''2 sol''4 fa''4. mib''!8 do''4 re''2. |
r2. r4 fa' sol' |
la'2 sol'4 fa'2 fa'4 |
mib'2 r4 r mib'' fa'' |
sol''2.~ sol''4 do'' re'' |
mib'' sib'2 |
do''4 mib'' re'' |
mib'' re'' do'' re'' sib'4. do''8 |
re''4 mib''4. fa''8 mib''!4 fa''4. sol''8 |
fa''4 re'' mib''2 fa''4 sol'' |
sol'' fa''2 mib'' sol'8 la' |
sib'8 la' sib' do'' re'' mib'' re'' la' sib' do'' re'' mib'' |
fa''4. fa''8 re''4~ |
re''8 re'' do''4. do''8 |
re''4 fa'' re'' |
sol''4. sol''8 mib''4~ |
mib''4 re'' mib''~ mib'' do'' re'' |
si'2. r |
r4 lab' do'' sib' sol'2 |
r4 do'' fa'' re'' re''4. re''8 |
mib''4 do'' sol'' fa'' do''4. sib'8 |
la'16 si' do''8 do''4. si'8 do''2. |
}
R1*30 R2.*3 R1*7
