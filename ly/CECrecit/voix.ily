\ffclef "valto" <>^\markup\character Licco
r4 sol' r8 fa' fa' fa'16 sol' |
mib'4 mib'8 fa'16 sol' re'4 re' |
re'8 re' re' re'16 mi' fad'4 fad' |
fad' fad'8 sol' sol'4 sol'8 sol' |
mi'4 fad'8 sol' sol'4. fad'8( sol'1) |
\transpose do #(if (eqv? #t (ly:get-option 'pygmalion))
                   (ly:make-pitch -1 1)
                   (ly:make-pitch -1 0))
{
r4 sol' fa' |
mib'4. mib'8 re'4 do'2 do'4 |
r fa' mib' re'4. re'8 do'4 |
sib2. sol'4. mib'8 re'4 do'2. sib |
sib4 sib do' re'4. re'8 mi'4 |
fa'2 fa'4 r do' re' |
mib'4. mib'8 fa'4 sol'2. |
sol' sol'4. sib'8 lab'4 |
sol' fa'2 |
lab'4 sol'2~ |
sol'4 fa'4. mib'8 fa'2. |
fa'4 mib'4. re'8 do'4 re'4. mib'!8 |
re'4 sib sol' do'( re') mib' |
mib'2 re'4( mib'!2.) |
fa'2. fa' |
fa'4 re' sib |
fa' fa'4. mib'8 |
re'4 sib4. sib8 |
mib'2 mib'4 |
fa'2 sol'4 sol'2 fa'4 |
sol'2 sol'4 \sug r4 do'4. do'8 |
re'4 re' re' mib' mib'4. mib'8 |
fa'4 fa' fa' sol' sol'4. fa'8 |
mib'2 re'4 fa'4. fa'8 mib'!4 |
\override AccidentalSuggestion.avoid-slur = #'outside
\ficta mib'( re'2) do'2. |
}
fa'4 fa'8 fa' re'4. re'8 |
mi'8 mi' mi' fa' fa' fa'16 fa' fa'8 sol' |
sol'4 sol'8 la' fa'4 fa' |
r sol' r8 mib' mib' mib'16 mib' |
do' do' do' do' do'8 do'16 re' mib'8 mib' mib' mib'16 fa' |
sol'8. sol'16 sol' sol' la' sib' sol'8 sol' r4 |

\ffclef "vsoprano" <>^\markup\character Deianira
re''2 si'4 si' |
r8 re'' si' do'' do''4 do'' |
r fa'' re''2 |
r8 re'' sib' sib' sol' sol' la' sib' |
sib'4 sib'
\ffclef "valto" <>^\markup\character Licco
r4 sol' |
r re'8 mib' do'4 do' |
r4 do'8 do' fa'4 fa'8 do' |
re'4 re' r8 re'16 re' si8 si16 si |
sol8 sol r4
\ffclef "vsoprano" <>^\markup\character Deianira
r8 do'' re'' mib'' |
do'' do'' r4
\ffclef "valto" <>^\markup\character Licco
r8 sol' mi'4 |
r8 mi' mi' mi' mi' mi' mi' mi' |
mi' mi' mi' fa' fa'4 fa'8 fa' |
fa'8 fa' fa' sol' sol'8. sol'16 la'8 sib' |
fa' fa' r4 r8 re'16 re' re'8 re'16 do' |
re'4 re'8 re' re' re' re' mi' |
mi'4 mi'8 mi'16 fa' fa'4 fa'8 fa' |
do'8 re' mib' mib'16 re' re'4. re'8 |
r8 re' sol' sol' sol' sol' r sol' |
sol' sol' sol' sol' do'4 do'8 do' |
do' do' do' do' do' do' fa' mib' |
do' do' r4 r2 |
\ffclef "vsoprano" <>^\markup\character Deianira
sol'4 sol' r8 do'' sib' la' |
la'4 <<
  \pygmalion { \ffclef "valto" <>^\markup\character Licco }
  \original {
    r4
    \ffclef "valto" <>^\markup\character Licco
    r8 do' fa' mi' |
    fa'4 fa'8 fa' fa' fa' do' do' |
    do'8. do'16 do'8 sib re'4 re'8 re' |
    re' re' re' mib' fa' fa' r sib |
    fa' fa' fa' fa' fa' fa' fa' mib' |
    fa'4 fa' fa'8. fa'16 fa'8 fa'16 sol' |
    mib'8. mib'16 mib'4 r mib' |
    do'8 do'
}
>> do'8 do' fa'4 fa'8 do' |
re' re' r re' sol' fa' fa' fa'16 sol' |
mib'4 mib' r |
do'4 do'8 do' do' do' |
do'4 fa' fa'8 do' |
re' re' sol' fa' mib' mib' mib' fa' |
sol' sol' mib' sol' re' re' r4 |
r8 sol' sol' fa' mib'4 mib'8 re' |
do' do' fa'4 re'8 re' sol'4 |
mib'8 mib' mib' re' fa'4 fa'8 sol' |
mib'2( re') do'1 |
