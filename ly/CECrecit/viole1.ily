\clef "alto"\transpose do #(if (eqv? #t (ly:get-option 'pygmalion))
                   (ly:make-pitch -1 1)
                   (ly:make-pitch -1 0))
{
R1*6 |
R2. |
r2. r4 do' sib |
lab2 do'4 sib2 r4 |
r sib lab sol2 sol4 do' fa2 fa2.~ |
fa4 sol sol fa2 r4 |
r la sib do'2 sol4 |
sol lab lab sol2 re'4 |
do'4 sol la si do' lab |
sol sib2 |
mib4 mib' sib |
do' re' mib' re'2. |
re'4 r r r fa' do' |
re'2 sib4 mib' re' do' |
sib2. sib |
r4 r sib8 do' re'4. mib'8 fa'4 |
sib fa'4. fa'8 |
sib4 do'2 |
fa'4 re'4. do'8 |
sib2 sol4 |
do'4 lab mib' do' lab re' |
re'2 sol sol4. sol8 |
fa4 lab4. lab8 sol4 sib4. sib8 |
do'4 do' do' re'2 re'4 |
do' mib' sib do'2 mib'4 |
do'4. sol'8 re'4 mib'2. |
}
R1*30 R2.*3 R1*7 |
