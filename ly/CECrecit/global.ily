\key re \minor
\midiTempo#120
\time 4/4 s1*4
\measure 2/1 s1*2
\digitTime\time 3/4 \midiTempo#160 s2.
\measure 6/4 s1.*2
\measure 12/4 s1.*2
\measure 6/4 s1.*4
\measure 3/4 s2.*2
\measure 6/4 s1.*5
\measure 3/4 s2.*4
\measure 6/4 s1.*6
\time 4/4 \midiTempo#120 s1*30 \original s1*7
\digitTime\time 3/4 \midiTempo#160 s2.*3
\time 4/4 \midiTempo#120 s1*5
\measure 2/1 s1*2 \bar "|."
