\score {
  \new StaffGroupNoBar <<
    \pygmalion\new StaffGroupNoBracket \with { \haraKiriFirst } <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
      \new Staff << \global \includeNotes "viole1" >>
    >>
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*4\break s1*2 s2.*9\break s2.*12\break
        s2.*11\break s2.*11\pageBreak
        s2.*2 s1*4\break s1*3\break s1*5\break
        s1*4\break s1*3 s2 \bar "" \pageBreak
        s2 s1*3\break s1*4\break s1*3 s2 \bar "" \break
        s2 s1*3\break s1*3 s2.\pageBreak
        s2.*2 s1*2\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
