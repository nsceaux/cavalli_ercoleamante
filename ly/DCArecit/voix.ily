\ffclef "vbasse" <>^\markup\character Ercole
r4 r do'8 do' |
sib4 sib8 sib lab sib |
sol4 sol fa8 sol |
mib4 mib8 fa sol la |
sib2 sib8 do' |
lab4 lab8 lab sol lab |
fa4 fa mib8 fa |
re4 re do8 re |
si,4 si,8 do re re |
<< \original sol2 \pygmalion sol, >> sol8 sol |
do4 do8 do fa fa |
sib,4 sib, mib8 mib |
lab,4 lab,8 lab, sol,4 |
lab,4 sib,2 mib do'8 do' |
sib4 sib8 do' lab sib |
sol4 sol8 fa mib4 |
fa sol2 do1*3/4\fermata |
\ffclef "vbas-dessus" <>^\markup\character Iole
la'4 la'8 sol' la'4. la'8 |
la' la' la'4 la'8 la' la' sib' |
sib' sib' sib' sib' sib'4 sib'8 sib' |
sib' sib' sib' la' la'4 la'8 la' |
do''8 do'' do'' do'' sol'4 sol'8 do'' |
fa'4 fa' r8 sol' fa' mi' |
mi'2( re') do'1\fermata |
\ffclef "vbasse" <>^\markup\character Ercole
r4 do'2 la8 la |
la4 la r8 mi mi mi |
la4 la8 sol sol sol sol sol |
sol fa sol4 sol8 sol sol sol |
sol4 sol sol8 sol sol sol16 la |
la8 la mi re fa4 fa |
fa8 fa fa fa16 mi sol8 sol sol sol |
sol4 sol8 fad la4 la8 la |
la la la la la sol la la |
r4 do'8[ sib] sib4 sib8 sib |
sib sib la sib sol sol fad sol |
la4 la r2 |
sol8 sol16 sol re'8 do' re'4 re'8 re' |
sol8 sol sol sol sol sol sol fa |
sol4 sol8 sol re re re re |
re re16 re sol8 re mi4 mi |
do8 do16 do mi8 mi sol sol16 do' sib8 la |
la8 la la sib16 do' fa fa fa fa fa8 mi |
sol8 sol16 sol sol sol la si do'8 do'16 sol mi8 do |
sol4 do r2 |
\ffclef "vbas-dessus" <>^\markup\character Iole
r4 sol' mi'8 mi' mi' fa' |
sol'4. sol'8 sol' la' la' la' |
r la' la' sol' sol' sol' sol' la' |
fa'4 fa'
\ffclef "vbasse" <>^\markup\character Ercole
do'4 la8 la |
r4 r8 la si4 si8 si16 do' |
do'8 do' r4 r2 |
r4 mi8 mi re4 re8 mi |
do4 do r8 do16 do sol8 sol16 fa |
la4 la la8 do' la la16 sib |
sib4 do'8 sib16 la sol8 sol( fa4) |
re r8 fa re4 re8 mi |
fa8 fa sib fa lab4 lab8 sol |
sol4 sol r8 fa fa sol |
mib mib mib sol re4 re8 mib! |
do4 do8 do do sib, sib, do |
lab,4 lab,8 lab lab lab sol lab |
fa4 fa fa sol8 lab |
mi8 mi mi mi mi4 mi8 fa |
sol8 sol sol la si4 la8 sol |
do'8 do' do' sib sib4 sib8 sib |
sib sib sib lab lab4 lab8 do' |
sol4 sol8 sol16 lab fa4 fa8 fa |
mib8 mib re do do4 do8 do |
sib,2 lab, sol,1 do\fermata |
fa4 fa8 sol la4 |
sib do'2 fa fa8 fa |
sib4 la4. sol8 sol2. |
sol4 sol8 sol fa sol mib2. |
do4. si,8 do4 re2. sol, |
fa4 fa8 sol la4 |
sib do'2 fa4 r8 fa mi fa |
re4 re mi8 fa |
sol4 sol,2 do do'8 do' |
la4 la la8 la |
sib4. sib8 la sib |
sol4 sol8[ la sib sol] |
re'2 re8 re sol4 fa4. sol8 |
mib4. fa8 sol la |
sib4 re( mib) fa2 fa4 |
sib2. la |
sol fa |
fa mi |
re do |
do' do' |
do' do'~ |
do'~ |
do'4 sol2 sol2. fa |
