\key fa \major \midiTempo#160
%% Ercole
\customTime\markup\vcenter { \musicglyph#"timesig.C44" \musicglyph#"three" }
\measure 3/4 s2.*13
\measure 6/4 s1.
\measure 3/4 s2.*2
\measure 6/4 s1. \bar "|."
%% Iole
\time 4/4 \midiTempo#120 s1*6 \measure 2/1 s1*2 \bar "|."
\measure 4/4
%% Ercole
s1*20 \bar "|."
%% Iole
s1*10
s1*13 \measure 3/1 s1*3 \bar ".|:"
\beginMark "Aria" \digitTime\time 3/4 \midiTempo#160 s2.
\measure 6/4 s1.*3
\measure 9/4 s2.*3
\measure 3/4 s2.
\measure 6/4 s1.
\measure 3/4 s2.
\measure 6/4 s1.
\measure 3/4 s2.*3
\measure 6/4 s1.
\measure 3/4 s2.
\measure 6/4 s1.*7
\measure 3/4 s2.
\measure 9/4 s2.*3 \bar ":|."

