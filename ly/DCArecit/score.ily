\score {
  \new ChoirStaff <<
    \new Staff \withLyricsB <<
      \global \includeNotes "voix"
    >>
    \keepWithTag #'couplet1 \includeLyrics "paroles"
    \keepWithTag #'couplet2 \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2.*6\break s2.*7\break s2.*6\break s1*4\pageBreak
        s1*4\break s1*4\break s1*4\break s1*4\break s1*4\pageBreak
        s1*2 s2 \bar "" \break s2 s1*3\break s1*4\break s1*4\break s1*4\pageBreak
        s1*4\break s1*3 s2 \bar "" \break s2 s1*4 s2.*3\break
        s2.*10\break s2.*8\pageBreak
        s2.*13\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
