\clef "basse" do2 do'4 |
sib2 lab4 |
sol2 fa4 |
mib2 do4 |
sib,2 sib4 |
lab2 sol4 |
fa2 mib4 |
re2 do4 |
si,4. do8 re4 |
sol,2 sol4 |
do2 fa4 |
sib,2 mib4 |
lab,2 sol,4 |
lab, sib,2 mib, do'4 |
sib2 lab4 |
sol2 mib4 |
fa sol2 do1*3/4\fermata |
fa,1~ |
fa, |
sol,~ |
sol,2 fa, |
fa, mi, |
re, do,4 fa, |
sol,1 do\fermata |
do2~ do |
dod1 |
\ficta dod~ |
dod~ |
dod1~ |
dod2 re~ |
re sib,~ |
sib, la,~ |
la,~ la,~ |
la, sol,~ |
sol,4 fa, mib,2 |
re,1 |
si,1~ |
si, |
si, |
si,2 do~ |
do mi |
fa la |
sol mi4 do |
sol, do r2 | \allowPageTurn
do1~ |
do |
sib,2. do4 |
fa,2~ fa, |
fa re |
do1 |
do2 sol, |
do1 |
fa, |
sib,4 la,8 sib, do4 fa, |
sib,1 |
sib,~ |
sib, |
mib2 si, |
do2. sib,4 |
lab,2 lab4 sol |
fa1 |
mi2 mi4 fa |
sol1 |
do'2 sib~ |
sib lab |
sol fa |
mib4 re do2 |
sib,2 lab, sol,1 do\fermata | \allowPageTurn
fa4. sol8 la4 |
sib do'2 fa2. |
sib4 la2 sol2. |
sol2 fa4 mib2. |
do re sol, |
fa4. sol8 la4 |
sib do'2 fa mi8 fa |
re2 mi8 fa |
sol2. do2 do'4 |
la2. |
sib2 la4 |
sol2. |
re2 re4 sol4 fa2 |
mib2. |
re2 mib4 fa2. |
sib4 la sol fa la, sib, |
do sib, do fa,2. |
fa4 mi re do mi, fa, |
sol, fa, sol, do,2. |
do'8 sol la sib do' sib la mi fa sol la sol |
fa la sol fa mi re do fa mi re do sib, |
la, re do sib, la, sol, |
fa,4 sib,2 do2. <<
  \original fa,
  \pygmalion { \custosNote fa,2 \stopStaff }
>>
