\tag #'(couplet1 basse) {
  Bel -- la I -- o -- le, e quan -- do ma -- i
  sen -- ti -- ra -- i
  di me pie -- tà?
  Chi la chie -- de al tuo ri -- go -- re
  ha va -- lo -- re
  per do -- ma -- re ogn’ im -- pie -- tà
  ma non si -- a, che te -- co im -- pie -- ghi
  se non prie -- ghi
  e mes -- ti la -- i;
  bel -- la I  -- o -- le, e quan -- do ma -- i?
  e quan -- do ma -- i?

  Quan -- do il mio cor ca -- pa -- ce
  fos -- se d’un lie -- ve a -- mor per chi m’uc -- ci -- se
  il ge -- ni -- tor di -- let -- to
  a -- ver per me do -- vres -- ti
  or -- ro -- re, e non af -- fet -- to.

  Ah bel -- la I -- o -- le
  a sì gran cri -- me, e di sì gran cas -- ti -- go
  de -- gno, qual per me fo -- ra
  l’im -- pos -- si -- bi -- li -- tà dell’ a -- mor tu -- o:
  im -- pu -- tar mi vor -- ra -- i
  u -- na pro -- va fa -- ta -- le,
  et un im -- pul -- so sen -- za fre -- no, oh di -- o,
  dell’ in -- fi -- ni -- to ar -- dor, dell’ a -- mor mi -- o?
  Quan -- do il to -- nan -- te is -- tes -- so
  ne -- gar -- mi co -- me Eu -- ty -- ro, ha -- ver ar -- di -- to
  un ben sì de -- si -- a -- to, e a me pro -- mes -- so,
  co -- me già con -- tro il so -- le, e’l dio tri -- for -- me
  sta -- to non fo -- ra con -- tro lui men par -- co
  di stra -- li av -- ve -- le -- na -- ti il mio gran’ ar -- co.
  
  Io so -- la fui ca -- gion, ch’il Ré mio pa -- dre
  rom -- pes -- se a te la da -- ta fe -- de.
  
  Ah co -- me
  a ciò tu l’in -- du -- ces -- ti?
  Dun -- que tu l’uc -- ci -- des -- ti.
  Che d’un mal, che si fe -- o,
  chi la cau -- sa ne diè, que -- gli n’è re -- o.

  Ma pon bel -- la in o -- bli -- o
  sì fu -- nes -- te me -- mo -- rie, e sì no -- io -- se,
  e qui me -- co t’as -- si -- di,
  poi -- ché de -- post’ an -- ch’i -- o
  l’in -- na -- ta mia fe -- ro -- cia, an -- zi can -- gia -- ta
  in co -- noc -- chia la cla -- va
  ra -- vi -- sar ti fa -- rò, che qua -- le ogn’ al -- tra
  tua più di -- vota an -- cel -- la
  non mai pren -- de -- rò a vi -- le
  di ren -- der -- ti ogn’ os -- se -- quio il più ser -- vi -- le;

  Qua gi -- ra gli oc -- chi A -- tlan -- te
  e per som -- ma bel -- tà
  mi -- ra quel, ch’og -- gi fa
  Er -- co -- le a -- man -- te:
  qua gi -- ra gli oc -- chi A -- tlan -- te
  ma non ne ri -- der non ne ri -- der già
  che se ta -- le e il vo -- ler
  del par -- go -- let -- to ar -- cier.

  Tut -- te tut -- te son o -- pre glo -- ri -- o -- se, e bel -- le
  tan -- to il fi -- lar,
  tan -- to il fi -- lar, che sos -- te -- ner __ le stel -- le.
}
\tag #'couplet2 {
  \ru#391 _
  Sol per vo -- ler d’A -- mo -- re,
  che in ciel E -- tho frenò
  ar -- men -- ti an -- cor gui -- dò
  nu -- me, e pas -- to -- re:
  sol per vo -- ler d’A -- mo -- re,
  e non ne ri -- ser non ne ri -- ser no
  gl’al -- tri dè -- i, ch’il mi -- rar,
  che fan ben ch’in a -- mar:
}
