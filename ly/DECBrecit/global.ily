\key do \major \time 4/4 \midiTempo#80
%% Giunone
\measure 4/4 s1*7
\measure 2/1 s1*2
\measure 4/4 s1*15
\measure 2/1 s1*2
\digitTime\time 3/4 \midiTempo#160 s2.*6
\measure 6/4 s1.
\measure 3/4 s2.*6
\measure 6/4 s1. \bar "|."
