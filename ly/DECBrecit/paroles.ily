Ah per -- ché per -- di Io -- le
in su -- per -- flue que -- re -- le
tem -- po sì pre -- ci -- o -- so, Hyl -- lo non lun -- ge
per mio con -- si -- glio in un cis -- pu -- glio as -- co -- so
tut -- to gua -- ta, et as -- col -- ta. __ Ar -- ma più tos -- to
ar -- ma ar -- ma fi -- glia la ma -- no
di ques -- to a -- cu -- to ac -- cia -- ro,
ch’a -- bi -- le a pe -- ne -- tra -- re o -- gni ri -- pa -- ro
per me tem -- prò Vul -- ca -- no
e men -- tre im -- pri -- gio -- na -- to
da i le -- ga -- mi del Son -- no i più te -- na -- ci
sta quel mos -- tro sì cru -- do
d’o -- gni di -- fe -- sa i -- gnu -- do,
van -- ne, van -- ne, e ven -- di -- ca ar -- di -- ta
con la mor -- te di lui
le mie of -- fe -- se, e i tuoi dan -- ni,
ch’al -- tro scam -- po non ha d’Hyl -- lo la vi -- ta.
Van -- ne, e poi -- ché spe -- di -- ta al ciel’ io tor -- no
ad ov -- vi -- ar in ciò l’i -- re di Gio -- ve __
fa’ ch’io vi giun -- ga
fa’ ch’io vi giun -- ga il crin de lau -- ri a -- dor -- no. __
