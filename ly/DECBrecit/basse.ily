\clef "basse" la,1 |
la, |
sol,2 fa, |
mi,1 |
mi |
mi2 la,~ |
la, si, |
do4 la,8 sold, si,2 mi1 |
do1 |
do2 sol, |
sol,1 |
mi,2. fa,4 |
sol,2 do |
fa,1~ |
fa, |
fa,~ |
fa, |
fa,2 do |
sol, sol,~ |
sol,1 |
re, |
sol, |
do |
re sol, | \allowPageTurn
la2 la4 |
sold2. |
la4. sol8 fa4 |
mi2 mi4 |
do4 si,2 |
la, sol,4 |
fa,2 mi,4 re,2. |
do, |
sol,4. sol8 re mi |
fa2. |
mi4. re8 do si, |
la,4. sol,8 fa, mi, |
re,4. mi,8 fa,4 |
re, mi,2 <<
  \original { la,1*3/4\fermata | }
  \pygmalion { \custosNote la,2 \stopStaff }
>>
