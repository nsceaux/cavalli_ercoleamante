\ffclef "vbas-dessus" <>^\markup\character Giunone
r4 do'' r8 do''16 mi'' do''8 do'' |
la'4 la' la'8 la' la' la'16 la' |
mi'4 mi' la'8 la' la' la'16 si' |
sold'4 sold' r2 |
si'4 si'8 la' si'4 si' |
r8 si' do'' re'' do''4 do''8 do'' |
la' la' la' la' fad' fad' sol' sol' |
sol' sol' la' si' sol'4. fad'8( mi'1) |
sol'8 sol'16 sol' do''8 do'' mi''16 do'' mi'' do'' do''8 do''16 si' |
do''8 do''16 do'' do'' do'' do'' si' re''8 re'' r4 |
re''8 re''16 re'' re''8 re'' re''4 re''8 re''16 do'' |
mi''4 mi''8 mi'' mi'' re'' re'' fa'' |
re''4. re''8( do''4) r8 do'' |
la'4 la' r8 la' la' sol' |
la' la' la' la' la'4 la'8 la' |
fa' fa' fa' mi' fa'4 fa' |
fa'8 fa' fa' fa'16 sol' la'4 la' |
la'8 la'16 la' fa'8 mi' sol'4 sol' |
sol'8 la' si' si'16 sol' re''8 re''16 re'' si'8 si' |
si' si' si' si'16 si' sol'4 sol'8 fad' |
la'4 la' r la'8 sol' |
si'4 si' si'8 si' si' si'16 la' |
do''2 la'4 si'8 la' |
si'2( la') sol'1 |
do''4 la' mi'' |
si' do''4. re''8 |
do''4 do''8 si' do'' re'' |
mi''4 mi''8 si' si' si' |
do''4 re''( mi'') |
fa'' mi''2 |
re''4 do''2 do'' si'4( |
do''4.) do''8 sol' la' |
si'4 si' r |
r r8 re'' la' si' |
do''4 si' mi''~ |
mi''8 re'' do''[ si']\melisma la'4~ |
la'8 la'8[ si' do''] re''[ do'']\melismaEnd |
re''[ mi''] do''4. si'8( la'1*3/4)\fermata |
