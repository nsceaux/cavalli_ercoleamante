\clef "basse" mi1~ |
mi~ |
mi |
do~ |
do2 si,~ |
si, la,~ |
la,1 |
mi2. do4 |
si,2 mi |
\original {
  re1 |
  re~ |
  re |
  re~ |
  re~ |
  re |
  re4 dod re2~ |
  re la,~ |
  la,4 sol, fad,2 |
  sol,1 |
  la,2 sold, |
  la,1 re |
  re |
  re |
  sol |
  sold~ |
  sold |
  la |
}
si1 |
sol~ |
sol |
sol,2 la, |
re1\fermata |
re1 |
re |
re~ |
re |
re |
si,~ |
si,2 la,~ |
la, fad,~ |
fad,1~ |
fad, |
mi,2 mi4 red |
mi2 dod~ |
dod re |
fad,1~ |
fad, |
sol,2~ sol, |
sold,1 |
la, |
la,2 re,~ |
re, sol,2~ |
<<
  \original { sol,2 la, | }
  \pygmalion { sol,2 la,4 re, | }
>>
re1 |
re~ |
re | \allowPageTurn
re~ |
re |
sol |
fad |
mi |
re2 do |
la, si, mi,1 |
mi~ |
mi |
mi |
la, |
si,~ |
si,2 do~ |
do re~ |
re mi~ |
mi do~ |
do re |
sol,1~ |
sol, |
re |
la,2 si, |

