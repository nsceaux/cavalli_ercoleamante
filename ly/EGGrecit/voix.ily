\clef "vbasse" <>^\markup\character Eutyro
sol4 sol8 sol sol4 sol8 sol |
sol sol sol fad sol sol sol la |
la4 la8 si sol4 sol |
la8 la la la16 sol la4 la8 la |
la la la si si si si si |
si4 do'8 re' do'4 do'8 la |
mi4 r8 la mi mi mi fad |
sol sol sol sol sol4 sol8 la |
si2 mi |
\ffclef "vsoprano" <>^\markup\character Deianira
<<
  \original {
    r4 fad'' re''8 re'' r4 |
    fad''4 fad''8 mi'' mi'' mi'' mi'' mi'' |
    mi'' mi'' mi'' mi'' mi'' mi'' mi'' fad'' |
    re''4 re'' r8 re'' re'' do'' |
    do''4 do'' do''8 do'' do'' si' |
    do''4 do'' si' si'8 re'' |
    sol'4 sol'8 fad' fad'4 fad' |
    sold' sold'8 la' la'4 la'8 la' |
    la' la' la' la' re''4 fad''~ |
    fad'' mi'' dod''2~ |
    dod''4 re''8 mi'' fa''!2( |
    mi''1) re'' |
    r4 r8 la' la' la' la' sol' |
    la'4 la'8 re'' la' la' la' sol' |
    si'4 si' r mi''~ |
    mi''2 si'8 si' si' si' |
    si'4 si' si'8. si'16 si'8 dod'' |
    la'4 la' r fad''~ |
    fad''2
  }
  \pygmalion { r4 fad'' }
>> re''8 re'' re'' re'' |
si'4 si' mi'' mi'' |
r2 sol''4 sol''8 fad'' |
re''4 re''
\ffclef "vsoprano" <>^\markup\character Iole
r8 mi'' dod'' fad'' |
re''4 r r2 |
\ffclef "vsoprano" <>^\markup\character Deianira
r4 la' la'8 la' la' sol' |
la'4 la' si'8 si'16 si' si'8 la' |
la'2 la'4 la'8 la' |
la'4 la'8 la' la' la' la' la' |
la'4 la'8 la' re'' sold' sold' sold' |
sold'4 sold'8 sold' sold' sold' sold' sold' |
sold' sold' sold' la' la'4 la'8 dod'' |
dod''4 dod'' r r8 red'' |
red''4 red'' r8 red''! red'' red'' |
red''4 red''8 \ficta fad'' red''!4 r8 mi'' |
mi''4 mi''8. mi''16 sold'8 fad' fad' si' |
mi'4 mi' r la' |
la'8 sol' sol' fad' fad' fad' la'4 |
r la' la' la'8 la' |
do''4 do'' re'' la'8 si' |
si'4 si' mi'' si' |
r si' mi' re'8 dod' |
dod'4 dod' r mi'8 mi' |
mi'4 mi'8 re' fad' fad' fad' fad' |
la'4 la'8 sol' si'2 |
si'4 sol'8 fad' mi'4 re' |
\ffclef "vbasse" <>^\markup\character Eutyro
fad4 fad la8 la la la |
fad fad fad fad fad fad16 fad fad8 mi |
fad8 fad fad fad fad4 fad8 sol |
la4 la8 la la la la la |
la4 la8 re' la8. la16 la8 la16 si |
sol4 sol si8 la la la16 la |
red4 red la la8 si |
sol4 sol r sol8 fad |
fad4 fad8 sol mi4 mi |
do'8 do' r mi mi4. red8( mi1) |
sold4 r8 si mi4 mi8 mi |
mi4 mi mi8 mi mi mi16 fa |
re4 re8 re re re re mi |
do2 mi4 do8 si, |
re4 re re8 re re re16 re |
re8 re16 re sol8 re mi mi mi mi |
la4 la8 mi fad fad fad fad |
fad4 fad8 sol sol4 sol |
sol8. sol16 sol8 la16 si do'4 do'8 mi |
fad4 fad8 sol sol4. fad8( |
sol2) r8 sol si re' |
sol4. sol8 re re16 re re8 mi |
fad8 fad16 fad mi8 re la4 la8 la16 si |
do'4. mi8 si,4 si, |
