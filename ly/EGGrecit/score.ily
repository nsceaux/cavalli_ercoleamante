\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*4\break s1*5\break s1*4\break s1*4 s2 \bar "" \break s2 s1*6\pageBreak
        s1*6\break s1*5\break s1*4\break s1*4\break s1*5\pageBreak
        s1*5\break s1*4\break s1*3\break s1*5\break s1*4\pageBreak
        s1*3\break s1*4\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
