Tan -- to ha d’Eu -- ty -- ro il nu -- do spir -- to an -- co -- ra
in -- vi -- si -- bil pos -- san -- za,
che ne -- glet -- te, e scher -- ni -- te
le te -- me -- ra -- rie vo -- glie
del ne -- mi -- co fel -- lo -- ne,
sa -- prà sa -- prà sal -- va -- re in -- sie -- me
l’in -- no -- cen -- te gar -- zo -- ne.

O \original { Di -- o dun -- que las -- cia -- te,
  che dà me di chi v’of -- fe -- se of -- fe -- sa mo -- glie
  e di chi tan -- to fa -- vo -- rir bra -- ma -- te
  ma -- dre, ohi -- mè, se -- mi -- vi -- va or sia con -- ces -- so
  d’ac -- co -- mu -- nar con voi l’as -- pre l’as -- pre mie do -- glie.
  Per con -- ser -- var -- mi il fi -- glio
  pri -- var -- mi di ma -- ri -- to,
  o __ di ri -- me -- dio re -- o mi -- se -- ro a -- bor -- to;
  o __
} dis -- pe -- ra -- ta spe -- me. Hyl -- lo Hyl -- lo è già mor -- to.

Ohi -- mè, che di’!

Sul più vi -- ci -- no sco -- glio
del -- la di lui pri -- gion men -- tre at -- ten -- de -- vo,
che qual -- che pic -- ciol le -- gno
co -- là mi con -- du -- ces -- se
a con -- so -- lar -- lo al -- men col mio cor -- do -- glio,
lo vi -- di lo vi -- di all’ im -- prov -- vi -- so, ohi -- mè, dall’ al -- to
ca -- der nel mar d’un sal -- to.
E se non lo se -- gui -- i,
fu per -- ché dal do -- lo -- re, ahi, so -- pra fat -- ta
cad -- di al suol tra -- mor -- ti -- ta,
e per man de -- gli as -- tan -- ti
con mal sag -- gia pie -- tà quin -- di fui trat -- ta.

Dun -- que a qual al -- tro fin, che per più stra -- no
mio spre -- gio, e scor -- no? Hor di te far vor -- ra -- i
un e -- se -- cra -- bil do -- no
al bar -- ba -- ro in -- hu -- ma -- no?
Ch’al -- tra mo -- glie tra -- fig -- ge, al -- tra ab -- ban -- do -- na,
e né me -- no a suoi fi -- gli em -- pio per -- do -- na. __
Deh con gius -- to co -- rag -- gio
sag -- gia -- men -- te pen -- ti -- ta,
ri -- nun -- cia a un tan -- to er -- ror men -- tr’io ri -- tor -- no
del fu -- man -- te Co -- ci -- to all’ a -- ria im -- pu -- ra
al -- le spon -- de in -- fo -- ca -- te
per u -- ni -- re in con -- giu -- ra
l’a -- ni -- me ch’il cru -- de -- le a mor -- te ha da -- te: __
e ben ve -- drai ch’in -- va -- no io non pre -- fis -- si
di sol -- le -- var con -- tro di lui gli a -- bis -- si.

