\key la \minor \midiTempo#100
\time 4/4 s1*9 \bar "|."
<<
  \original { s1*11 \measure 2/1 s1*2 \measure 4/4 s1*32 }
  \pygmalion s1*26
>>
s1*9 \measure 2/1 s1*2
\measure 4/4 s1*14
\endMarkSmall "Segue sub[ito] la Sinf[onia] Infernale"
\bar "|."
