\key fa \major
\time 4/4 \midiTempo#160 \measure 3/1 s1*6
\measure 2/1 s1*2
\digitTime\time 3/4 \measure 6/4 s1.*2
\measure 3/4 s2.*4
\measure 6/4 s1.
\time 4/4 \measure 2/1 s1*2
\digitTime\time 3/4 \measure 9/4 s2.*3
\measure 3/4 s2.*4
\measure 6/4 s1.
\measure 3/4 s2.*4
\measure 9/4 s2.*3 \bar "|."
\digitTime\time 3/4 \measure 6/4 s1.*2
\measure 9/4 s2.*3
\measure 6/4 s1.*4
\measure 9/4 s2.*3
\time 4/4 s1*4
\measure 3/1 s1*3 \bar "|."
