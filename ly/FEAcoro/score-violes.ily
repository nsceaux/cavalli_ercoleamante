\score {
  <<
    \new ChoirStaff \with { \haraKiri } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix13 \includeNotes "voix" \clef "alto"
      >> \keepWithTag #'voix13 \includeLyrics "paroles"
    >>
    \new ChoirStaff \with { \haraKiri }  <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix22 \includeNotes "voix" \clef "alto"
      >> \keepWithTag #'voix22 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix23 \includeNotes "voix" \clef "alto"
      >> \keepWithTag #'voix23 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix24 \includeNotes "voix" \clef "alto"
      >> \keepWithTag #'voix24 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix25 \includeNotes "voix"
      >> \keepWithTag #'voix25 \includeLyrics "paroles"
    >>
    \new Staff <<
      \global \includeNotes "basse"
      \modVersion { s1*8 s2.*10 s1*2 s2.*16\break }
    >>
  >>
  \layout { }
}
