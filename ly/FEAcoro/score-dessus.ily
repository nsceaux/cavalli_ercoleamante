\score {
  <<
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix11 \includeNotes "voix"
      >> \keepWithTag #'voix11 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix12 \includeNotes "voix"
      >> \keepWithTag #'voix12 \includeLyrics "paroles"
    >>
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix21 \includeNotes "voix"
      >> \keepWithTag #'voix21 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix22 \includeNotes "voix"
      >> \keepWithTag #'voix22 \includeLyrics "paroles"
    >>
  >>
  \layout { }
}
