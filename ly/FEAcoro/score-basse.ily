\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \keepWithTag #'voix13 \includeNotes "voix" \clef "alto"
    >> \keepWithTag #'voix13 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'voix25 \includeNotes "voix"
    >> \keepWithTag #'voix25 \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
    >>
  >>
  \layout { }
}
