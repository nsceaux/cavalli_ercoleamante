\clef "basse"
\sugNotes {
  \clef "treble" sol'1 r4 sol' sol' re' sol'2 sol' |
  \clef "bass" sol1 r4 sol sol re sol2 sol |
  \clef "treble" r4 sol' sol' fa' mib'2 mib'4 mib' |
  mib'2 mib'4 re' re' re'8 do' |
  sib2. re'4 re'8 re' re' mi' |
  fa'2. |
  do'8 do' do'4 do'8 re' |
  mib'4 mib' mib' |
  re'2 re'4 |
  mib' fa'2 sib2. |
}
\clef "bass" r4 sib sib fa sib2 sib |
la2. sol2 do4 fa2. |
\sugNotes {
  la4 la8 la la sib |
  do'2. |
  sib8 sib sib4 sib8 do' |
  re'4 re' re' |
  la2 fa4 sol la2 |
}  
re4 re8 re re mi |
fa2. |
mib2 mib8 fa |
sol2 sol4 |
re2 sib,4 do re2 sol,1*3/4\fermata | \allowPageTurn
\sugNotes {
  \clef "treble" sol'4 sol' sol' re'2 re'4 |
  mib' mib' mib' fa'2 fa'4 |
  re' re' re' mib'2 fa' sol' |
}  
\clef "bass" do'4 do' do' sol2 sol4 |
la la la sib2 sib4 |
mi!2. fa |
sol4 sol sol re2 re4 |
mi2 mi4 fa2 sol la |
re2 \sugNotes {
  r8 re' do' sib |
  sib2 sib8 sib la la |
  sol2 sol8
} sol8\noBeam fa4 |
mib2. re4 |
do2. sol,4 re1 sol,\fermata |
