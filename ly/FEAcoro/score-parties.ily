\score {
  <<
    \new Staff \withLyrics <<
      \global \keepWithTag #'voix13 \includeNotes "voix" \clef "alto"
    >> \keepWithTag #'voix13 \includeLyrics "paroles"
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix22 \includeNotes "voix" \clef "alto"
      >> \keepWithTag #'voix22 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix23 \includeNotes "voix" \clef "alto"
      >> \keepWithTag #'voix23 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix24 \includeNotes "voix" \clef "alto"
      >> \keepWithTag #'voix24 \includeLyrics "paroles"
    >>
  >>
  \layout { }
}
