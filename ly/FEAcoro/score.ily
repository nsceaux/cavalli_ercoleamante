\score {
  <<
    \new ChoirStaff \with {
      instrumentName = \markup\character Coro di 3 voci
    } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix11 \includeNotes "voix"
      >> \keepWithTag #'voix11 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix12 \includeNotes "voix"
      >> \keepWithTag #'voix12 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix13 \includeNotes "voix"
      >> \keepWithTag #'voix13 \includeLyrics "paroles"
    >>
    \new ChoirStaff \with {
      instrumentName = \markup\character Coro di 5 voci
    } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix21 \includeNotes "voix"
      >> \keepWithTag #'voix21 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix22 \includeNotes "voix"
      >> \keepWithTag #'voix22 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix23 \includeNotes "voix"
      >> \keepWithTag #'voix23 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix24 \includeNotes "voix"
      >> \keepWithTag #'voix24 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix25 \includeNotes "voix"
      >> \keepWithTag #'voix25 \includeLyrics "paroles"
    >>
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*8 s2.*2\pageBreak
        s2.*8\pageBreak
        s1*2 s2.*6\pageBreak
        s2.*6\pageBreak
        s2.*11\pageBreak
        s2.*11\pageBreak
      }
      \modVersion { s1*8 s2.*10 s1*2 s2.*16\break }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
