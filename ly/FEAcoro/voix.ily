<<
  \tag #'voix11 {
    \clef "vsoprano" re''1 r4 re'' re'' re'' sib'8.([ do''16 sib'8. do''16] re''4) re'' |
    R1*3 |
    r4 re'' re'' re'' mib''2 mib''4 sib' |
    sib'2 sib'4 sib' sib' sib'8 do'' |
    re''2. r2*3/2 |
    do''4 do''8 do'' do'' re'' |
    mib''2. |
    sib'8 sib' sib'4 sib'8 do'' |
    re''4 re'' fa'' |
    mib'' do''8[ re'' mib'' fa''] re''2. |
    R1*2 R2.*9 |
    re''4 re''8 re'' re'' do'' |
    do''2. |
    sol''8 fa'' mib''4 mib''8 re'' |
    re''4 re'' sol'' |
    fad''2 re''4 mib'' la'( re'') re''1*3/4\fermata | \allowPageTurn
    R2.*2 |
    sol''4 sol'' sol'' do''2 do''4 |
    fa'' fa'' fa'' mib''2 re'' re'' |
    do''2. r2*3/2 |
    R2.*4 |
    sol''4 sol'' re'' fa''2 fa''4 |
    sol'' sol'' sol'' fa''2 mi'' mi'' |
    re''2 r8 fa'' mib'' re'' |
    re''2 re''8 sib' do''8. re''16 |
    sib'2 sib'8 sib' fa'' fa'' |
    sol''2 sol''8 sol'' re'' fa'' |
    mib''[\melisma fa'' mib'' fa''] sol''4\melismaEnd sol'' r sol'' re''2 re''1\fermata |
  }
  \tag #'voix12 {
    \clef "vsoprano" sib'1 r4 sib' sib' fad' sol'8.([ la'16 sol'8. la'16] sib'4) sib' |
    R1*3 |
    r4 sib' sib' la' sol'2 sol'4 sol' |
    sol'2 sol'4 fa' fa' sol'8 la' |
    sib'2. fa'4 fa'8 fa' fa' sol' |
    la'2. |
    R2. |
    sol'8 sol' sol'4 sol'8 la' |
    sib'4 sib' re'' |
    sib' la'8[ sib' do'' re''] sib'2. |
    R1*2 R2.*9 |
    sib'4 sib'8 sib' sib' sib' |
    la'2. |
    mib''8 re'' do''4 do''8 do'' |
    sib'4 sib' re'' |
    re''2 sol'4 do''8[ la'] re''2 si'1*3/4\fermata |
    r2*3/2 re''4 re'' re'' |
    sol'2 sol'4 la' la' la' |
    si' si' si' do''2 do'' do''4. si'!8( |
    do''2.) r2*3/2 |
    R2.*4 |
    re''4 re'' sib' re''2 re''4 |
    sol' do'' do'' la'2 sib'! la' |
    la'2 r8 la' la' sib' |
    sib'2 sib'8 sol' fad' fad' |
    sol'2 sol'8 sol' re'' la' |
    sib'2 sib'8 sib' sib' sib' |
    do''2 do''4 sib' re''\melisma do''8[ sib'] la'4 re''\melismaEnd si'1\fermata |
  }
  \tag #'voix13 {
    \clef "valto" sol'1 r4 sol' sol' re' sol'2 sol' |
    R1*3 |
    r4 sol' sol' fa' mib'2 mib'4 mib' |
    mib'2 mib'4 re' re' re'8 do' |
    sib2. re'4 re'8 re' re' mi' |
    fa'2. |
    do'8 do' do'4 do'8 re' |
    mib'4 mib' mib' |
    re'2 re'4 |
    mib' fa'2 sib2. |
    R1*2 R2.*9 |
    fa'4 fa'8 fa' fa' sol' |
    do'2. |
    sol'8 sol' sol'4 sol'8 do' |
    sol'4 sol' sib' |
    la'8.[\melisma sol'16] fad'4\melismaEnd sib' sol' re'2 sol'1*3/4\fermata |
    sol'4 sol' sol' re'2 re'4 |
    mib' mib' mib' fa'2 fa'4 |
    re' re' re' mib'2 fa' sol' |
    do'2. r2*3/2 |
    R2.*4 |
    sol'4 sol' sol' re'2 re'4 |
    mi' mi' mi' fa'2 sol' la' |
    re'2 r8 re' do' sib |
    sib2 sib8 sib la la |
    sol2 sol8 sol' fa' fa' |
    mib'2 mib'8 mib' re' re' |
    do'2 do'4 sol' re'1 sol'\fermata |
  }
  %%
  \tag #'voix21 {
    \clef "vsoprano" R1*3 |
    re''1 r4 re'' re'' re'' sib'8.([ do''16 sib'8. do''16] re''4) re'' |
    R1*2 R2.*10 |
    re''2 fa''4 fa'' re''2 re''4. re''8 |
    do''2 do''4 sib' sib' do''8 do'' do''2. |
    R2. |
    sol'4 sol'8 sol' sol' la' |
    sib'2. |
    la'8 la' la'4 la'8 si' |
    dod''4 dod'' re'' re'' dod''( re'') |
    re''2 r4 |
    la'4 la'8 la' la' sib' |
    do''2. |
    sib'8 sib' sib'4 sib'8 do'' |
    re''4 re'' re'' do'' la'2 si'1*3/4\fermata |
    R2.*9 |
    r2*3/2 re''4 re'' re'' |
    sol'2 sol'4 la' la' la' |
    sib'2 sib'4 la'2 la'4 |
    do'' do'' do'' re''2 re'' re''4. dod''8( |
    re''2) r |
    R1 |
    r2 r8 re'' re'' re'' |
    mib''2 mib''8 mib'' fa'' re'' |
    mib''2 mib''4 re'' re''1 re''\fermata |
  }
  \tag #'voix22 {
    \clef "valto" R1*3 |
    sol'1 r4 sol' sol' fad' sol'2 sol' |
    R1*2 R2.*10 |
    sib'2 fa'4 la' sib'2 sib'4. fa'8 |
    la'2 fa'4 sol' sol' sol'8 sol' la'2. |
    R2. |
    mi'4 mi'8 mi' mi' fad' |
    sol'2. |
    fa'8 fa' fa'4 fa'8 sol' |
    la'4 mi' fa' fa' mi'( la') |
    re'2 r4 |
    fa'4 fa'8 fa' fa' fa' |
    sol'2. |
    re'8 re' re'4 re'8 mi' |
    fad'4 fad' sol' sol' fad'!2 sol'1*3/4\fermata |
    R2.*13 |
    sol'4 sol' sol' fa'2 fa'4 |
    mi' mi' mi' la'2 re' la' |
    fad'2 r |
    R1 |
    r2 r8 sol' la' la' |
    sol'2 sol'8 sol' fa' fa' |
    sol'2 sol'4 sol' fad'\melisma sol'2\melismaEnd fad'4( sol'1)\fermata |
  }
  \tag #'voix23 {
    \clef "vtenor" R1*3 |
    re'1 r4 re' re' re' re'2 re' |
    R1*2 R2.*10 |
    fa'2 re'4 fa' fa'2 fa'4. re'8 |
    fa'2 do'4 re' re' mi'8 mi' fa'2. |
    R2.*6 |
    fa4 fa8 fa fa do' |
    la8.[\melisma sib16 la8. sib16] do'4\melismaEnd |
    sol8 sol sol4 sol8 la |
    sib4 sol re' |
    re'2 re'4 la la2 sol1*3/4\fermata |
    R2.*7 |
    r2*3/2 sol'4 sol' sol' |
    do'2 do'4 re' re' re' |
    mi'2 mi'4 do'2 do'4 |
    sib sib re' re'2 re'4 |
    do' do' do' fa'2 re'4 sol' mi'( la) |
    la2 r |
    R1 |
    r2 r8 re' fa' fa' |
    sib2 sib8 sib re' re' |
    sol2 sol4 re' re'1 re'\fermata |
  }
  \tag #'voix24 {
    \clef "vtenor" R1*3 |
    sib1 r4 sib sib la sib2 sib |
    R1*2 R2.*10 |
    r4 re' re' do' re'2 re'4. sib8 |
    do'2 la4 sib sib do'8 do' do'2. |
    la4 la8 la la sib |
    do'2. |
    sib8 sib sib4 sib8 do' |
    re'4 re' re' |
    la2 fa4 sol la2 |
    re' r4 |
    do'4 do'8 do' do' re' |
    mib'2. |
    re'8 re' sib4 sib8 sol |
    la4 la sib mib' re'2 re'1*3/4\fermata |
    R2.*9 |
    fa'4 fa' fa' sib2 sib4 |
    do' do' do' la2 la4 |
    re' re' sol la2 la4 |
    sol sol sol la2 re' la |
    re'2 r |
    R1 |
    r2 r8 sib re' re' |
    sol2 sol8 sol sib fa |
    do'2 do'4 sib la\melisma sol la2\melismaEnd si1\fermata |
  }
  \tag #'voix25 {
    \clef "vbasse" R1*3 |
    sol1 r4 sol sol re sol2 sol |
    R1*2 R2.*10 |
    r4 sib sib fa sib2 sib4. sib8 |
    la2 la4 sol sol do8 do fa2. |
    R2.*6 |
    re4 re8 re re mi |
    fa2. |
    mib8 mib mib4 mib8 fa |
    sol4 sol sol |
    re2 sib,4 do re2 sol,1*3/4\fermata |
    R2.*7 |
    do'4 do' do' sol2 sol4 |
    la la la sib2 sib4 |
    mi!2. fa |
    sol4 sol sol re2 re4 |
    mi mi mi fa2 sol la |
    re2 r |
    R1 |
    r2 r8 sol fa fa |
    mib2 mib8 mib re re |
    do2 do4 sol, re1 sol,\fermata |
  }
>>
