\tag #'(voix11 voix12 voix13) {
  Quel quel grand’ e -- ro -- e,
  quel grand’ e -- ro -- e, che già
  lag -- giù tan -- to pe -- nò
  spo -- so del -- la bel -- tà
  per go -- der noz -- ze e -- ter -- ne al ciel \tag #'voix13 { al ciel } vo -- lò!
  spo -- so del -- la bel -- tà
  per go -- der noz -- ze e -- ter -- ne al ciel al ciel vo -- lò!

  Vir -- tù, che sof -- fre
  \tag #'voix13 { al -- fin mer -- ce -- de }
  al -- fin \tag #'voix12 { mer -- ce -- de } mer -- ce -- de im -- pe -- tra
  Vir -- tù, che sof -- fre al -- fin mer -- ce -- de im -- pe -- tra
  e de -- gno cam -- po a’ suoi tri -- on -- fi
  e de -- gno cam -- po a’ suoi tri -- on -- fi è l’e -- tra.
}
\tag #'(voix21 voix22 voix23 voix24 voix25) {
  Quel quel grand’ e -- ro -- e,
  quel grand’ e -- ro -- e, che già
  lag -- giù tan -- to pe -- nò
  \tag #'(voix21 voix22 voix24) {
    spo -- so del -- la bel -- tà
    per go -- der noz -- ze e -- ter -- ne al ciel \tag #'voix24 { al ciel } vo -- lò!
  }
  \tag #'(voix21 voix22 voix24 voix25) { spo -- so del -- la bel -- tà }
  \tag #'voix23 { spo -- so del -- la bel -- tà __ }
  per go -- der noz -- ze e -- ter -- ne al ciel
  \tag #'(voix23 voix25) { al ciel }
  vo -- lò!

  \tag #'voix21 {
    Vir -- tù, che sof -- fre al -- fin mer -- ce -- de im -- pe -- tra
    al -- fin mer -- ce -- de im -- pe -- tra __
  }
  \tag #'voix22 {
    Vir -- tù, che sof -- fre al -- fin mer -- ce -- de im -- pe -- tra
  }
  \tag #'voix23 {
    Vir -- tù, che sof -- fre al -- fin mer -- ce -- de im -- pe -- tra
    vir -- tù, che sof -- fre al -- fin mer -- ce -- de im -- pe -- tra
  }
  \tag #'voix24 {
    Vir -- tù, che sof -- fre al -- fin mer -- ce -- de
    vir -- tù, che sof -- fre al -- fin mer -- ce -- de im -- pe -- tra
  }
  \tag #'voix25 {
    Vir -- tù, che sof -- fre al -- fin mer -- ce -- de im -- pe -- tra
    vir -- tù, che sof -- fre al -- fin mer -- ce -- de im -- pe -- tra
  }
  e de -- gno cam -- po a’ suoi tri -- on -- fi è l’e -- tra.
}
