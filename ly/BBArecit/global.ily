\key la \minor \midiTempo#180
\digitTime\time 3/4 s2.*2
\measure 6/4 s1.*3
\measure 3/4 s2.*2
\measure 7/4 s4*7 \bar "||"
\beginMark "a 3" \digitTime\time 3/4 \measure 6/4 s1.*5
\measure 7/4 s4*7 \bar "||"
\beginMark "Ritor[nello]" \digitTime\time 3/4 \measure 6/4 s1.*2
\measure 7/4 s4*7 \bar "||"
\digitTime\time 3/4 s2.*5
\measure 9/4 s2.*3
\measure 3/4 s2.*2
\measure 7/4 s4*7 \bar "||"
\beginMark "a 3" \digitTime\time 3/4 \measure 6/4 s1.*5
\measure 7/4 s4*7 \bar "||"
\beginMark "Ritor[nello]" \digitTime\time 3/4 s2.
\measure 9/4 s2.*3
\measure 7/4 s4*7 \bar "|."
