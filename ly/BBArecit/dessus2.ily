\clef "dessus" R4*74 |
fa''4 mi'' re'' do''2 do''4 |
re'' do'' sib' la'2 la'4 |
re'' re''4. dod''8 re''1\fermata |
R4*74 |
fa''4 mi'' re'' |
do''2 do''4 re'' do'' sib' la'2 la'4 |
re'' re''4. dod''8 re''1\fermata |
