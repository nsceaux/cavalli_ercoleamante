\clef "viole2" R4*74 |
fa'4 la' re' la'2 la'4 |
re'4 la' mi' la2 re'4 |
sol'8 fa' mi'4. mi'8 fad'1\fermata |
R4*74 |
fa'4 la' re' |
la'2 la'4 re'4 la' mi' la2 re'4 |
sol'8 fa' mi'4. mi'8 fad'1\fermata |
