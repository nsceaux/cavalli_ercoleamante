\clef "basse" re' dod' re' |
la2. |
sib4 la sol fa2. |
mi4 re do sib,2 la,4 |
sib, do2 fa,2. |
fa4 sol la |
sib2. |
sol4 la2 re1\fermata |
\clef "alto" re'4 mi'2 fa'2. |
sib4 do'2 re'2. |
la4 sib2 do' sib4 |
do' re'2 sol2. |
sol'4 fa' mi' re'2. |
sol4 la2 re'1\fermata |
\clef "bass" re'4 do' sib la2 la4 |
sib la sol fa2 fa4 |
sol la4. la8 re1\fermata |
re'4 dod' re' |
la2. |
sib4 la sol |
fa2. |
mi4 re do |
sib,2 la,4 sib,! do2 fa,2. |
fa4 sol la |
sib2. |
sol4 la2 re1\fermata |
\clef "alto" re'4 mi'2 fa'2. |
sib4 do'2 re'2. |
la4 sib2 do' sib!4 |
do' re'2 sol2. |
sol'4 fa' mi' re'2. |
sol4 la2 re'1\fermata |
\clef "bass" re'4 do' sib |
la2 la4 sib la sol fa2 fa4 |
sol la4. la8 re1\fermata |
