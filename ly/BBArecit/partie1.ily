\clef "viole1" R4*74 |
la'4 do'' sol' mi'2 mi'4 |
sib' fa' sol' do'2 fa'4 |
re' la'4. la'8 la'1\fermata |
R4*74 |
la'4 do'' sol' |
mi'2 mi'4 sib' fa' sol' do'2 fa'4 |
re' la'4. la'8 la'1\fermata |
