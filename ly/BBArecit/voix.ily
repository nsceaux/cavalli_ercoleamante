<<
  \tag #'(venere basse) {
    \clef "vsoprano" <>^\markup\character Venere
    re''4 mi'' re'' |
    re'' dod''2 |
    re''4 do'' sib' sib' la'2 |
    do''4 sib' la' re''4. re''8 do''4 |
    re'' sib'4. la'8 la'2. |
    la'4 sib' la' |
    re''4. re''8 re''4 |
    fa''4 re''4. dod''8 re''1\fermata |
  }
  \tag #'(coro1 coro2) \clef "vsoprano"
  \tag #'coro3 \clef "valto"
  \tag #'(coro1 coro2 coro3) { R4*37 }
>>
<<
  \tag #'(coro1 basse) {
    \clef "vsoprano" <>^\markup\character Coro di Gratie
    fa''4 mi'' re'' re'' do''2 |
    re''4 do'' sib' sib' la'2 |
    do''4 sib' la' sol'4. la'8 sib'4 |
    mib'' la'4. \ficta sib'8 sib'2. |
    re''4 re'' mi'' fa''4. fa''8 fa''4 |
    mi''4 mi''4. re''8 re''1\fermata |
  }
  \tag #'coro2 {
    \clef "vsoprano" re''4 do'' sib' sib' la'2 |
    sib'4 la' sol' sol' fa'2 |
    la'4 sol' fa' mi'4. fad'8 sol'4 |
    sol'4 sol'4. fad'8 sol'2. |
    sib'4 la' sol' la'4. la'8 re''4 |
    re'' re''4. dod''8 re''1\fermata |
  }
  \tag #'coro3 {
    \clef "valto" re'4 mi' mi' fa' fa'2 |
    sib4 do' do' re' re'2 |
    la4 sib sib do'4. do'8 sib!4 |
    do' re'4. re'8 sol'2. |
    sol'4 fa' mi' re'4. re'8 re'4 |
    sol' la'4. la'8 re'1\fermata |
  }
  \tag #'venere { R4*37 }
>>
R4*19 |
<<
  \tag #'(venere basse) {
    <>^\markup\character Venere
    re''4 mi'' re'' |
    re''4. dod''8 dod''4 |
    re''4 do'' sib' |
    sib'4. la'8 la'4 |
    do''4 sib' la' |
    re''4\melisma mi''8([ re''] do''4)\melismaEnd re'' sib'4. la'8 la'2. |
    la'4 sib' la' |
    re''2 re''4 |
    fa''4 re''4. dod''8 re''1\fermata |
  }
  \tag #'(coro1 coro2) \clef "vsoprano"
  \tag #'coro3 \clef "valto"
  \tag #'(coro1 coro2 coro3) { R4*37 }
>>

<<
  \tag #'(coro1 basse) {
    <>^\markup\character Coro di Gratie
    fa''4 mi'' re'' do''4. sib'8 do''4 |
    re'' do'' sib' la'4. sol'8 la'4 |
    do'' sib' la' sol'\melisma sol'8([ la'] sib'!4)\melismaEnd |
    mib''4 la'4. \ficta sib'8 sib'2. |
    re''4 re'' mi'' fa''4. fa''8 fa''4 |
    fa'' mi''2 re''1\fermata |
  }
  \tag #'coro2 {
    re''4 do'' sib' la'4. sol'8 la'4 |
    sib' la' sol' fa'4. mi'8 fa'4 |
    la' sol' fa' mi'\melisma mi'8[ fad'] sol'4\melismaEnd |
    sol' sol'4. fad'8 sol'2. |
    sib'4 la' sol' la'4. la'8 re''4 |
    re'' dod''2 re''1\fermata |
  }
  \tag #'coro3 {
    re'4 mi' mi' fa'4. fa'8 fa'4 |
    sib do' do' re'4. re'8 re'4 |
    la sib sib do'\melisma re'8[ do'] sib4\melismaEnd |
    do' re'4. re'8 sol2. |
    sol'4 fa' mi' re'4. re'8 re'4 |
    sol' la'2 re'1\fermata |
  }
  \tag #'venere { R4*37 }
>>
R4*19 |
