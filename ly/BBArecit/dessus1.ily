\clef "dessus" R4*74 |
la''4 la'' sol'' la''2 la''4 |
sol'' fa'' mi'' fa''2 fa''4 |
sib'' mi''4. mi''8 re''1\fermata |
R4*74 |
la''4 la'' sol'' |
la''2 la''4 sol'' fa'' mi'' fa''2 fa''4 |
sib'' mi''4. mi''8 re''1\fermata |
