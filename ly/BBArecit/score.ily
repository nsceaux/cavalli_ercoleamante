\score {
  \new StaffGroupNoBar \with { \haraKiriFirst } <<
    \new StaffGroupNoBracket <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
      \new Staff << \global \includeNotes "partie1" >>
      \new Staff << \global \includeNotes "partie2" >>
    >>
    \new Staff \withLyrics <<
      \global \keepWithTag #'coro1 \includeNotes "voix"
    >> \keepWithTag #'coro1 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'coro2 \includeNotes "voix"
    >> \keepWithTag #'coro2 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'coro3 \includeNotes "voix"
    >> \keepWithTag #'coro3 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'venere \includeNotes "voix"
    >> \keepWithTag #'venere \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \origLayout {
        s2.*6\break s2.*5 s1\break s2.*11 s1\pageBreak
        s2.*5 s1\break s2.*5\break s2.*6 s1\pageBreak
        s2.*11 s1\break
      }
      \modVersion { s4*93\break }
    >>
  >>
  \layout { }
  \midi { }
}
