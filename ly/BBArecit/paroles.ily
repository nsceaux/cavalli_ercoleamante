\tag #'(venere basse) {
  Se nin -- fa a i pian -- ti
  di ve -- ri a -- man -- ti
  non mai pie -- ghe -- vo -- le
  nie -- ga mer -- cè;
  di ciò col -- pe -- vo -- le
  a -- mor non è.
}
\tag #'(coro1 coro2 coro3 basse) {
  Se nin -- fa a i pian -- ti
  di ve -- ri a -- man -- ti
  non mai pie -- ghe -- vo -- le
  nie -- ga mer -- cè;
  di ciò col -- pe -- vo -- le
  a -- mor non è.
}
\tag #'(venere basse) {
  Sco -- glio sì ri -- gi -- do
  mo -- stro sì fri -- gi -- do
  non reg -- ge il mar __
  non reg -- ge il mar
  ch’a -- ma -- to al pa -- ri non de -- va a -- mar.
}
\tag #'(coro1 coro2 coro3 basse) {
  Sco -- glio sì ri -- gi -- do
  mo -- stro sì fri -- gi -- do
  non reg -- ge il mar __
  non reg -- ge il mar
  ch’a -- ma -- to al pa -- ri non de -- va a -- mar.
}
