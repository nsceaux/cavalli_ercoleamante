\piecePartSpecs
#(if (eqv? #t (ly:get-option 'pygmalion))
     '((dessus #:score "score-dessus")
       (parties #:score-template "score-parties-voix")
       (violes #:score-template "score-parties-violes-voix")
       (basse #:score "score-basse"))
     '((dessus #:score "score-dessus")
       (parties #:score-template "score-parties-voix")
       (basse #:score-template "score-voix")))