\clef "vbasse" <>^\markup\character Ercole
r4 r sol |
sol sol8 la sib sol |
re'2 sib4 do'4. sol8 re' re sol2. |
r4 r re |
re4. mi8 fa re la2 fa4 |
sol4. re8 la la, re2. |
r4 r8 re' re' do' |
sib2 la4 sol sol fa8 fa |
mib4 mib4. sol8 |
si,2 si,8 do do2. |
re4 re re mib2. |
fa2. sol2 sol4 |
la2 la4 sib2 sib4 |
r do' sib |
la sol la fad re sol |
sib2. la sol1*3/4\fermata |
r4 r8 re la la la la |
fa fa fa fa fa4 sol8 la |
mi4 mi fa mi8 re |
la4 la r2 |
la8 si dod' si16 la re'8 re' do'16[\melisma sib do' la] |
sib8\melismaEnd sib16 sib la[\melisma sol la fa] sol8\melismaEnd sol16 sol fa[\melisma mi fa mi] |
la8\melismaEnd sol la4 re2 |
r4 sol sol8 sol sol fad |
sol4 sol sol8 sol sol sol16 sol |
do'4 do' mi fa8 mi sol2 do |
r4 fa8 fa fa4 fa8 fa |
sib2 sib4 sib |
mib2 re4 re8 mib |
do4 do8 do si,4 do re1 sol,\fermata |
\ffclef "valto" <>^\markup\character Licco
<<
  \original {
    do'4 do' r8 do' do' si |
    do'4 do' do' do'8 do' |
    do'4 do'8 do' do'8. do'16 do'8 si |
  }
  \pygmalion {
    mi'4 mi' r8 mi' mi' re' |
    mi'4 mi' mi' mi'8 mi' |
    mi'4 mi'8 mi' mi'8. mi'16 mi'8 re' |
  }
>>
re'4 re'8 re' re' re' re' re' |
re' re' re' do' mi'4 mi'8 do' |
do' do' do'4 do'4. do'8 |
do'8 do' do' do' la4 la |
la8 la la la16 si do'2 |
do'4 fa'8 mi' re'2 do'1 |
\ffclef "vsoprano" <>^\markup\character Iole
r8 sol' sol' sol' mi'4 r8 do'' |
si'4 r r8 sol' sol' sol' |
sol' sol' sol' sol' sol'4 sol'8 la' |
la'4 la' r8 la' la' si' |
do'' do'' si' la' sib'4 la'8 sib' |
sol'4 sol' r re'' |
si'8 si' si' si' si' si' si' do'' |
do''4 do''8 sol' do'' sib' sib' la' |
la'4 la'8 si'16 do'' do''8 do'' r4 |
\ffclef "vbasse" <>^\markup\character Ercole
sol4 r8 la fa4 fa8 mi |
fa4 fa r8 fa sol re |
mi4 mi8 mi mi mi mi mi |
mi mi mi fad sol4 sol |
sol sol8 sol sol4. sol8 |
sol8 sol la si do'4 do'8 do' |
do' do' do' si si4. la8 |
la4 la la2 |
sol re8 re re re16 do |
re4 re sol8. sol16 sol8 sol |
mi4. mi8 sol sol sol fa |
la la la4 fa8 fa fa fa |
fa4 mi sol mi8 re |
mi2( re) do1\fermata |
