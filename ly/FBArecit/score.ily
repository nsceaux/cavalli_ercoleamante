\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2.*8\break s2.*8\break s2.*12\break s1*4 s2 \bar "" \pageBreak
        s2 s1*4\break s1*5\break s1*5\break s1*4\break s1*4\pageBreak
        s1*4\break s1*4\break s1*5\break s1*5\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
