Al -- fi -- ne il ciel d’A -- mor
per me si se -- re -- nò,
e i nem -- bi di ri -- gor,
in gio -- ie dis -- tem -- prò,
sol nel mio cor pur sen -- to
un so -- a -- ve so -- a -- ve mar -- tir,
ch’ab -- bia per gir più len -- to
da -- ti il tem -- po da -- ti il tem -- po i suoi van -- ni al mio de -- sir.
Ma pur l’a -- ma -- ta I -- o -- le
l’a -- do -- ra -- to mio so -- le ec -- co a me vie -- ne,
dun -- que af -- fat -- to il mio sen sgom -- bra -- te sgom -- bra -- te
sgom -- bra -- te o pe -- ne,
che di sì ri -- gid’ al -- ma
qual si sia la vit -- to -- ria io n’ho la pal -- ma,
e l’ar -- den -- te mio spir -- to
pos -- pon tut -- ti i suoi lau -- ri a un sì bel mir -- to.

Quan -- do com’ è tuo uf -- fi -- zio,
dar quel -- la ves -- te ad Er -- co -- le do -- vra -- i
per far di noz -- ze ta -- li il sa -- cri -- fi -- zio,
quest’ al -- tra in ve -- ce, il cui va -- lor ben sa -- i,
des -- tra -- men -- te da me pren -- der po -- tra -- i.

Co -- sì fa -- rò: ma che? per dif -- fi -- den -- za
di ri -- me -- dio s’in -- cer -- to, ho il sen ri -- pie -- no
di ge -- lo -- sa te -- men -- za,
pur quan -- do mi tra -- dis -- ca ogn’ al -- tro scam -- po,
soc -- cor -- so mi da -- rà pron -- to ve -- le -- no.

Deh non muo -- ve -- re I -- o -- le il piè res -- ti -- o,
ver chi do -- mi -- na -- tor del mon -- do in -- te -- ro
so -- lo in go -- der dell’ al -- ma tua l’im -- pe -- ro
pon la fe -- li -- ci -- tà del suo de -- si -- o.
Et il sa -- cro con -- cen -- to
sciol -- ga -- si o -- mai, ch’a me di ta -- li in -- du -- gi
gra -- do è d’im -- men -- sa pe -- na o -- gni mo -- men -- to.
