\key re \minor \midiTempo#160
\digitTime\time 3/4 s2.*2 \measure 9/4 s2.*3 \measure 3/4 s2.
\measure 6/4 s1.*2 \measure 3/4 s2.
\measure 6/4 s1. \measure 3/4 s2.
\measure 6/4 s1.*4 \measure 3/4 s2.
\measure 6/4 s1. \measure 9/4 s2.*3
\time 4/4 \midiTempo#100 s1*9 \measure 2/1 s1*2
\measure 4/4 s1*3 \measure 3/1 s1*3 \bar "|."
\key do \major \measure 4/4 s1*8 \measure 2/1 s1*2 \bar "|."
\measure 4/4 s1*22 \measure 2/1 s1*2 \bar "|."
