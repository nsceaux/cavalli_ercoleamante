\clef "basse" r4 r sol |
sol2 sol4 |
re re'8 do' sib4 do'4. sol8 re'4 sol2. |
r4 r re |
re2. la4 la8 sol fa4 |
sol4. re8 la4 re2. |
re2 re'4 |
sib2 la4 sol2 fa4 |
mib2. |
re do |
si, do4 do do |
re2. mib |
fa sol |
mib |
do re2 sib,4 |
do2. re sol,1*3/4\fermata |
re1~ |
re |
do2 sib, |
la,1 |
la2 re'4 do' |
sib la sol fa8 re |
la sol la4 re2 |
sol1 |
sol |
mi2. fa4 sol2 do |
fa1 |
sib |
mib2 re |
do si,4 do re1 sol,\fermata | \allowPageTurn
do1~ |
do |
do |
sol,~ |
sol,2 do |
do1~ |
do2 fa,~ |
fa, mi,~ |
mi,4 fa, sol,2 do1 |
do1 |
sol, |
sol, |
re |
mi4 do re2 |
sol,1 |
sol, |
mi, |
fa,2 sol, |
do1~ |
do2 si, |
do1~ |
do |
si,~ |
si,2 la,~ |
la, sol,4 do |
re do re2 |
sol,1 |
sol, |
do |
fa, |
sol,~ |
sol, do1\fermata |
