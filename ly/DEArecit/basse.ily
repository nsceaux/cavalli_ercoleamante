\clef "basse" sib,1~ |
sib, |
sib,2 mib |
fa1 sib, |
fa~ |
fa2 mi <<
  \original {
    fa1 |
    fa1 |
    sib, |
    sib, |
    fa,2~ fa, |
    sib4 fa sol re8 fa |
    mib4 re mib do |
    sib,4. la,8 sib,4 sol, |
    fa, sol, la, fa, |
  }
  \pygmalion { fa4 sol la fa | }
>>
sib,4 do re sib, |
mib fa sol mib |
fa mib fa re |
mib re do re |
mib do fa fa, sib, do re mib |
re mib re do |
sib, do sib, la, |
sol, la, sib, do |
fa, fa re4. do8 |
sib,4 sib sol4. fa8 |
mib4 fa sol mib |
fa re mib sib, |
fa mib fa fa, |
sib,1 |
sib, |
sib,~ |
sib, |
fa2 fa,8 sol, |
la,4 sib,2 |
do la,4 |
sib,2 do4 |
fa2 mib4 |
re la, sib, |
do2. fa,2 sib,8 do |
re4 mib2 |
fa re4 |
mib2 fa4 |
sib2 la4 |
sol re mib fa2. |
sib,2 sib8 la sol4 re mib \sug fa2. \sug sib, |
