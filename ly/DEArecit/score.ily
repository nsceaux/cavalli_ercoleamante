\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*2\break
        s1*6\break s1*3\break s1*6\break s1*9\break s1*7\pageBreak
        s1 s2.*4\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
