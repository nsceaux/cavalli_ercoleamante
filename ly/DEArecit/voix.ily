\ffclef "vbas-dessus" <>^\markup\character Giunone
sib'4 sib' r8 sib' re'' la' |
sib'2 sib'4 r8 sib' |
fa' sib' lab' sol' sol' sol'16 sol' \ficta la'8 sib' |
fa'2 fa' r1 |
la'4 la' r8 la' la' la' |
do''4 do''8 do'' do'' do'' do'' do''16 sol' la'4 la' <<
  \original {
    r4 la' |
    la'8 la' la' sib' do'' fa' do'' sib' |
    re''4 re'' re''8 re'' re'' do''16 re'' |
    sib'8 sib'16 sib' sib' sib' sib' sib' sib'4 sib'8 sib'16 la' |
    do''4 do'' fa'' do'' |
    re'' la'8 do'' sib'4 la'8 la' |
    sol'4 fa' sol' la' |
    sib' sib'8 do'' re''4 mi'' |
    fa''2 fa'' |
  }
  \pygmalion { r2 | }
>>
sib'2 sib' |
sib' sib'4 do'' |
lab'1 |
sol'2. fa'4 |
sol'2 la' sib'1 |
sib'2 sib'4 do'' |
re''2 re''4 do'' |
sib'2 sol'4 do'' |
la'4. sol'8 fa'4 fa'' |
re''4. do''8 sib'4 re'' |
sol'( la') sib'( do'') |
la'( sib') do''( re'') |
re''\melisma mib''8[ re''] do''2\melismaEnd |
sib'2 r8 fa' fa' fa' |
fa'4 fa'8 fa' fa' fa' fa' mib' |
fa'4 fa'8 sib' fa' fa' fa' fa' |
fa'4 fa' fa'8 fa'16 fa' sol'8 la' |
la'4 la' la'8 sib' |
do''4 re''8[ do''] sib'[ la'] |
sol'4 sol' do'' |
sib'4. sib'8 sib' la' |
la'4 la' sol'8 la' |
sib'4 fa' sol' |
la'( sol'2) fa' re''8 mib'' |
fa''4 sol''8[ fa''] mib''[ re''] |
do''4 do'' fa'' |
mib''4. mib''8 mib'' re'' |
re''4 re'' do''8 re'' |
mib''4 sib' do'' re''( do''2) |
sib'2.~ sib'~ sib'~ \sug sib' |
