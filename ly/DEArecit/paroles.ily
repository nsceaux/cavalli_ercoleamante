Son -- no po -- ten -- te nu -- me
fu qui pur op -- por -- tu -- no il nos -- tro ar -- ri -- vo;
dun -- que poi -- ché tu se -- i
dell’ in -- no -- cen -- za a -- mi -- co,
\original {
  e de’ mis -- fat -- ti rei co -- tan -- to schi -- vo,
  che da lo -- ro fug -- gen -- do
  d’in -- e -- so -- ra -- bil vol sa -- zi tue piu -- me,
  co’ più for -- ti le -- ga -- mi,
  che mai tua fred -- da suo -- ra a te pres -- tas -- si
}
im -- pe -- dis -- ci pie -- to -- so al par, che gius -- to
og -- gi un cri -- me il più ne -- ro,
che con -- tro a -- mor che con -- tro a -- mor la fro -- de un -- qua ten -- tas -- se,
e con la ver -- ga a cui fu fa -- cil pro -- va
le sem -- pre des -- te lu -- ci
tut -- te ve -- la -- re ad Ar -- go
van -- ne van -- ne ve -- lo -- ce, e in Er -- co -- le pro -- du -- ci
un più sor -- do le -- tar -- go.
Van -- ne van -- ne ve -- lo -- ce, e in Er -- co -- le pro -- du -- ci
un più sor -- do le -- tar -- go. __
