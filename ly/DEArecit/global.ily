\key fa \major \midiTempo#120
\time 4/4 s1*3
\measure 2/1 s1*2
\measure 4/4 s1
\measure 2/1 s1*2
\measure 4/4 << \original s1*12 \pygmalion s1*4 >>
\measure 2/1 s1*2
\measure 4/4 s1*12
\digitTime\time 3/4 \midiTempo#160 s2.*6
\measure 6/4 s1.
\measure 3/4 s2.*4
\measure 6/4 s1.
\measure 12/4 s2.*4 \bar "|."
