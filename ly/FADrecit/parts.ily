\piecePartSpecs
#`((dessus #:score "score-dessus"
           #:music , #{ s2.*5 s1*12 s2.*12 s1*4 s1.*9\break #})
   (parties #:score "score-parties"
           #:music , #{ s2.*5 s1*12 s2.*12 s1*4 s1.*9\break #})
   (basse #:score "score-basse"
           #:music , #{ s2.*5 s1*12 s2.*12 s1*4 s1.*9\break #}))
