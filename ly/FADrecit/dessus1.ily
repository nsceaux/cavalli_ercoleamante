\clef "dessus" R2.*5 R1*12 R2.*12 R1*4 R1.*9
s1.*4 |
s2. r4 sib'' sol'' |
la'' la'' fa'' sol'' mib''2 |
fa''4 fa'' fa''8 sol'' la''2 la''4 |
sib''2 fa''4 sol'' sol'' sol'' |
la'' fa'' sib'' fa''2. fa''1*3/4\fermata |
