\score {
  \new ChoirStaff \with { \haraKiriFirst } <<
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'vtenor \includeNotes "voix" \clef "tenor"
    >> \keepWithTag #'vtenor { \includeLyrics "paroles" }
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      { s2.*5 s1*12 s2.*12 s1*4 s1.*9 s2 \noHaraKiri }
      \global \keepWithTag #'vbasse \includeNotes "voix"
    >> \keepWithTag #'vbasse { \includeLyrics "paroles" }
    \new Staff << \global \keepWithTag #'basse \includeNotes "basse" >>
    \new Staff \with { \tinyStaff \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'basse \includeNotes "voix"
    >> \keepWithTag #'basse { \set fontSize = #-2 \includeLyrics "paroles" }
    \new Staff <<
      $(or (*score-extra-music*) (make-music 'Music))
      \global \keepWithTag #'basse-continue \includeNotes "basse"
    >>
  >>
  \layout { }
}
