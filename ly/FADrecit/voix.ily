<<
  \tag #'(laomedonte basse) {
    \clef "vtenor" <>^\markup\character-text Laomedonte Re di Troia
    r4 re' sib |
    fa' fa' r |
    r4 re'4. mib'8 |
    fa'4 mib' re' |
    do'2. |
    sib8 re' re' mib' do' do' do' re' |
    mib'4. re'8 sol4 sol8 sol |
    sol sol la sib do' do' la8. sol16 |
    la4 la8 la la la re'8 la |
    sib sib sib sib sib4 sib8 la |
    do'4 do' do'8 do' do' do'16 sib |
    re'4 re' re'8 re'16 re' mi'8 fa' |
    sol'4 mi'8 fa'16 sol' dod'8 dod' mi' mi' |
    dod'4 dod'8 dod' la4 la8 fa' |
    mib'4 re' dod' re' |
    re'2. dod'4( re'1) |
    fa'4 mi' fa' |
    re'8. re'16 re'4 r |
    re'4 do' re' |
    sib8. sib16 sib4 r |
    do'4 sib do' la2. |
    do'4 do' do' fa'2. |
    la4 la la re'2. |
    lab4 sol4. lab8 fa2. |
    r4 la8 la do'4 do'8 do' |
    la4 la r2 |
    r4 re'8 re' fa'4 fa'8 fa' |
    re'4 re' r2 |
    r4 fa' re' mib' do'2 |
    re'4 re' sib do'2 re'4 |
    sib2 do'4 la2 la4 |
    re' mi' fa' |
    la( sol2) |
    fa2. r4 sol' mib' |
    fa' re'2 mib'4 mib' do' |
    re'2 mib'4 do'2 re'4 |
    sib2 sib4 fa' mib' re' |
    re'( do'2) sib2. |
    R2.*19
  }
  \tag #'vsoprano {
    \clef "vsoprano" R2.*5 R1*12 R2.*12 R1*4 R1.*9
    <>^\markup\character Coro
    r4 fa'' re'' mib'' do''2 |
    re''4 re'' sib' do''2 re''4 |
    sib'2 do''4 la'2 sib'4 |
    sol' sol' la' re'' sib' sib' |
    sol' do'' la' sol'2. |
    la'4 do'' re'' sib' do''2 |
    la'4 la' re'' do''2 re''4 |
    sib'2 sib'4 sol'2 do''4 |
    la' sib' sib' sib'2 la'4( sib'1*3/4)\fermata |
  }
  \tag #'valto {
    \clef "valto" R2.*5 R1*12 R2.*12 R1*4 R1.*9
    r2*3/2 r4 do'' la' |
    sib' sol'2 la'4 la' fa' |
    sol'2 la'4 fa'2 sol'4 |
    mi'2 fa'4 re' re' sol' |
    mi' do' fa' fa'2 mi'4( |
    fa') la' sib' sol' la'2 |
    fa'4 fa' sib' la'2 la'4 |
    sol'2 fa'4 mib'2 sol'4 |
    fa' fa' fa' fa'2. fa'1*3/4\fermata |
  }
  \tag #'vtenor {
    \clef "vtenor" R2.*5 R1*12 R2.*12 R1*4 R1.*9
    R2.*2 |
    r2*3/2 r4 fa' re' |
    mib' do'2 re'4 re' sib |
    do'2 la4 sib2 sib4 |
    do' la fa do'2. |
    fa4 fa' fa' mib' mib'2 |
    re'4 re' fa' fa'2 fa'4 |
    re'2 re'4 sib2 mib'4 |
    do' re' re' do'2. re'1*3/4\fermata |
  }
  \tag #'vbasse {
    \clef "vbasse" R2.*5 R1*12 R2.*12 R1*4 R1.*9
    R2.*10 |
    r4 fa re mib do2 |
    re4 re sib, fa2 re4 |
    sol2 re4 mib2 mib4 |
    fa re sib, fa2. sib,1*3/4\fermata |
  }
>>
