\tag #'(laomedonte basse) {
  Pe -- ra pe -- ra mo -- ra mo -- ra il per -- ver -- so
  che d’un sol at -- to di pie -- tà, che ma -- i
  tra le bar -- ba -- rie sue con -- tar po -- tes -- se,
  qual mer -- ce -- na -- rio vi -- le
  ri -- chie -- den -- do -- ne il prez -- zo,
  ne’ con -- ten -- ti as -- sai tos -- to
  gl’a -- vi -- di suoi de -- sir quan -- to mal -- va -- gi,
  si pa -- gò col mio san -- gue, e mil -- le mil -- le stra -- gi. __
  Su su sbra -- nia -- mo -- lo,
  su la -- ce -- ria -- mo -- lo
  gius -- ti -- tia il vol,
  pa -- ghi e -- gli an -- cor
  l’al -- trui do -- lor
  col pro -- prio duol.
  Che più dun -- que s’as -- pet -- ta?
  Che più dun -- que s’as -- pet -- ta?
  
  Pe -- ra pe -- ra mo -- ra il cru -- del, sù sù sù sù ven -- det -- ta ven -- det -- ta.
  Pe -- ra pe -- ra mo -- ra il cru -- del, sù sù sù sù ven -- det -- ta ven -- det -- ta.
}
\tag #'(vsoprano valto vtenor) {
  Pe -- ra pe -- ra mo -- ra il cru -- del, sù sù
  \tag #'(vsoprano valto) { sù sù ven -- det -- ta }
  \tag #'vsoprano { ven -- det -- ta }
  ven -- det -- ta ven -- det -- ta.
}
\tag #'(vsoprano valto vtenor vbasse) {
  Pe -- ra pe -- ra mo -- ra il cru -- del, sù sù sù sù ven -- det -- ta ven -- det -- ta.
}
