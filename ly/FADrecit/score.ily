\score {
  \new StaffGroupNoBar \with { \haraKiriFirst } <<
    \new StaffGroupNoBracket <<
      \new Staff <<
        { s2.*5 s1*12 s2.*12 s1*4 s1.*9
          s1. \override MultiMeasureRest.transparent = ##t 
          \noHaraKiri R1.*7 }
        \global \includeNotes "dessus1"
      >>
      \new Staff <<
        { s2.*5 s1*12 s2.*12 s1*4 s1.*9
          s1. \override MultiMeasureRest.transparent = ##t 
          \noHaraKiri R1.*7 }
        \global \includeNotes "dessus2"
      >>
      \new Staff <<
        { s2.*5 s1*12 s2.*12 s1*4 s1.*9
          s1. \override MultiMeasureRest.transparent = ##t 
          \noHaraKiri R1.*7 }
        \global \includeNotes "partie1"
      >>
      \new Staff <<
        { s2.*5 s1*12 s2.*12 s1*4 s1.*9
          s1. \override MultiMeasureRest.transparent = ##t 
          \noHaraKiri R1.*7 }
        \global \includeNotes "partie2"
      >>
      \new Staff <<
        { s2.*5 s1*12 s2.*12 s1*4 s1.*9
          s1. \override MultiMeasureRest.transparent = ##t 
          \noHaraKiri R1.*7 }
        \global \keepWithTag #'basse \includeNotes "basse"
      >>
    >>
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'vsoprano \includeNotes "voix"
      >> \keepWithTag #'vsoprano \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'valto \includeNotes "voix"
      >> \keepWithTag #'valto \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vtenor \includeNotes "voix"
      >> \keepWithTag #'vtenor \includeLyrics "paroles"
      \new Staff \withLyrics <<
        { s2.*5 s1*12 s2.*12 s1*4 s1.*9\break
          s2\noHaraKiri }
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new Staff \withLyrics <<
      \global \keepWithTag #'laomedonte \includeNotes "voix"
    >> \keepWithTag #'laomedonte \includeLyrics "paroles"
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2.*5 s1\pageBreak
        s1*4\break s1*4\break s1*3 s2.*6\break s2.*6 s1*4\break s1.*6\pageBreak
        s1.*3\break s1.*5\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
