\clef "basse"
<<
  \tag #'basse-continue {
    \clef "basse" sib2. |
    r4 la fa |
    sib2. |
    re4 mib2 |
    fa2. |
    sib,2 fad,~ |
    fad, sol,~ |
    sol, mib |
    re1 |
    sol |
    fa |
    sib |
    sol2 la~ la fa |
    sol1 |
    la re |
    re4 la2 |
    sib2. |
    sib,4 fa2 |
    sol2. |
    mi fa |
    mi re |
    do sib, |
    sib,4 do2 fa,2. |
    fa1~ |
    fa |
    sib,~ |
    sib, |
    r2*3/2 r4 do' la |
    sib sol2 la4 la fa |
    sol2 mi4 fa2 la,4 |
    sib,2 sib,4 |
    do2. |
    fa,4 fa re mib do2 |
    re4 sib,2 do4 do' la |
    sib sol2 lab4 fa2 |
    sol2 sol4 re mib2 |
    fa2. sib, |
    %%
    \clef "alto" r2*3/2 r4 do'' la' |
    sib' sol'2 la'4 \clef "tenor" fa' re' |
    mib' do'2 re'4 re' sib |
    do'2 la4 sib2 sib4 |
    do' la fa do'2. |
    fa4 \clef "bass" fa re mib do2 |
    re4 re sib, fa2 re4 |
    sol2 re4 mib2 mib4 |
    fa re sib, fa2. sib,1*3/4\fermata |
  }
>>