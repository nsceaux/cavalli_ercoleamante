\clef "dessus" R2.*5 R1*12 R2.*12 R1*4 R1.*9
s1.*5 |
r4 fa'' sib'' sib'' la''2 |
re''4 re'' re''8 mi'' fa''2 fa''4 |
sol''2 sib''4 mib'' mib'' sib' |
fa'' sib' fa'' do''4. re''8 mib'' fa'' re''1*3/4\fermata |
