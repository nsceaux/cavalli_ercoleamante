\key fa \major
\digitTime\time 3/4 \midiTempo#160 s2.*5
\time 4/4 \midiTempo#80 s1*10 \measure 2/1 s1*2
\digitTime\time 3/4 \midiTempo#160 s2.*4
\measure 6/4 s1.*4
\time 4/4 \midiTempo#80 s1*4
\time 6/4 \midiTempo#160 s1.*3 \measure 3/4 s2.*2 \measure 6/4 s1.*5 \bar "|."
\time 6/4 s1.*8 \measure 9/4 s2.*3 \bar "|."
