\clef "dessus" R1*9 R2.*12 R1*12 |
r2 mi'' mi''2. mi''4 |
do''2. re''4 do''2 do''~ do''4 si'8 la' si'2 do''1\fermata |
R1*3 R2.*8 R1*10 |
