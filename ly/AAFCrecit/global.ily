\key do \major
\time 4/4 \midiTempo#120 s1*9
\digitTime\time 3/4 \midiTempo#180 s2.*12
\time 4/4 \midiTempo#92 s1*10
\measure 2/1 s\breve \bar "|."
\beginMark "Ritor[nello]" \midiTempo#240 \time 4/4 \measure 2/1 s\breve
\measure 4/1 s\longa \bar "|."
\time 4/4 \midiTempo#120 s1*3
\digitTime\time 3/4 \midiTempo#180 s2.*8
\time 4/4 \midiTempo#120 s1*10 \bar "|."
\endMarkSmall "Segue il Coro"
