Di que -- ste il ciel con am -- mi -- ra -- bil cu -- ra,
e con stu -- por del tem -- po, e di na -- tu -- ra,
scet -- tri a scet -- tri in -- nes -- tan -- do, e fre -- gi, a fre -- gi
la pro -- sa -- pia for -- mò de i fran -- chi re -- gi;
che qual fiu -- me di glo -- rie
da’ mon -- ti di Co -- ro -- ne, e fa -- sci al -- te -- ri
tras -- se i fon -- ti
tras -- se i fon -- ti pri -- mie -- ri
ed ac -- cres -- ciu -- to o -- gn’or da co -- pi -- o -- si
tor -- ren -- ti di vit -- to -- rie,
e da’ più ge -- ne -- ro -- si
ri -- vi di san -- gue au -- gu -- sto ol -- tre gli A -- che -- i
per in -- ter -- rot -- to, e lim -- pi -- do sen -- tie -- ro
tra mar -- gi -- ni di pal -- me, e di tro -- fe -- i
in -- on -- dò tri -- on -- fan -- te il mon -- do il mon -- do in -- te -- ro.

Al -- fin tra l’au -- ree spon -- de
del -- la Sen -- na guer -- rie -- ra
fis -- sò fis -- sò la reg -- gia in cui be -- ni -- gna in -- fon -- de
gra -- zie a nem -- bi o -- gni sfe -- ra,
et hor più che mai pro -- di -- go
di con -- ten -- tez -- ze e -- te -- re -- e
ad i -- be -- ra bel -- tà fran -- co va -- lo -- re
su ta -- la -- mo di pa -- ce u -- ni -- sce u -- ni -- sce A -- mo -- re.
