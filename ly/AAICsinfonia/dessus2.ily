\clef "dessus" r4 do''8. do''16 mi''8. mi''16 sol''8. sol''16 |
mi''8. mi''16 do''8. do''16 mi''8. mi''16 sol''8. sol''16 |
mi''8. mi''16 sol''8 sol''16 sol'' mi''8. mi''16 sol''8 sol''16 sol'' |
mi''8. mi''16 r4 r2 |
sol''4 sol'' sol'' sol'' |
mi'' mi'' mi'' mi'' |
mi''1\fermata |
