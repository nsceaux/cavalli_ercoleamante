\clef "dessus" <>^"Trombe" do''8. do''16 mi''8. mi''16 sol''8. sol''16 mi''8. mi''16 |
do''8. do''16 mi''8. mi''16 sol''8. sol''16 mi''8. mi''16 |
sol''8 sol''16 sol'' mi''8. mi''16 sol''8 sol''16 sol'' mi''8. mi''16 |
sol''4 sol'' sol'' sol'' |
mi'' mi'' mi'' mi'' |
do'' do'' do'' do'' |
do''1\fermata |
