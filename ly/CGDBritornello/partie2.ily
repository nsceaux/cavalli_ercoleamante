\clef "alto" r4 do' do' |
sol'2 sol'4 mi'2 mi'4 |
la'2 la'4 r2*3/2 |
r4 fa' fa' |
sol'2 sol'4 |
mib'2. r4 do' do' re'2 re'4 |
do' do' re' fa'2\melisma sol'4 |
do' do' re' sol'2\melismaEnd sol'4 fa'1*3/4\fermata |
