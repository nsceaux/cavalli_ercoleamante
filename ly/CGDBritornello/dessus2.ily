\clef "treble" R2.*3 |
r2*3/2 r4 fa' fa' |
sib'2 sib'4 |
sol'2 sol'4 |
do''2 do''4 r do'' do'' sib'2 sib'4 |
sol' la' sib' do''2.~ |
do''2\melisma sib'8[ la'] sol'2\melismaEnd sol'4 la'1*3/4\fermata |
