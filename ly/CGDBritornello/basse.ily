\clef "bass" R2. |
sol do' |
la re' |
sib |
mib'2 sib4 |
do'2. la sib2 sib4 |
do'2 sib4 la2 sol4 |
fa la sib do'2. fa1*3/4\fermata |
