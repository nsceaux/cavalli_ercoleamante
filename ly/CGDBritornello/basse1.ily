\clef "bass" r4 la la |
re'2 re'4 do'2 do'4 |
r4 la la re'2 re'4 |
sib sib sib |
mib'2 sib4 |
do'2. r2*3/2 r4 sib sib |
do'2\melisma sib4 la2\melismaEnd sol4 |
fa la sib do'2 do'4 fa1*3/4\fermata |
