\clef "alto" r4 fa' fa' |
sib'2 sib'4 sol'2 sol'4 |
do''2 do''4 r la' la' |
re''2 re''4 |
sib'2. |
r4 sol' sol' la'2 la'4 fa' fa' fa' |
do''2\melisma sol'4 la'2\melismaEnd mi'4 |
la' la' re'' do''2 do''4 do''1*3/4\fermata |
