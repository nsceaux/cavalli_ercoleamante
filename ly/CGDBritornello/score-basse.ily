\score {
  \new StaffGroup <<
    \new Staff << \global \includeNotes "basse1" >>
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
    >>
  >>
  \layout { indent = 0 }
}
