\clef "treble" R2.*3 |
r4 do'' do'' fa''2 fa''4 |
re''2 re''4 |
sol''2 sol''4 |
r4 do'' do'' fa''2 fa''4 re'' re'' re'' |
mi''2 re''4 do'' re'' mi'' |
fa''2.~ fa''2 mi''4 fa''1*3/4\fermata |
