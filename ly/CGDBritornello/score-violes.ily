\score {
  \new StaffGroup <<
    \new Staff << \global \includeNotes "partie1" >>
    \new Staff << \global \includeNotes "partie2" >>
    \new Staff << \global \includeNotes "basse1" >>
    \new Staff << \global \includeNotes "basse" >>
  >>
  \layout {
    system-count = #(*system-count*)
    indent = #(if (*instrument-name*)
                  largeindent
                  (or (*score-indent*) smallindent))
  }
}
