\piecePartSpecs
#`((dessus #:score "score-dessus")
   (parties #:indent 0)
   (violes #:score "score-violes" #:indent 0)
   (basse #:score "score-basse" #:indent 0)
   (silence #:on-the-fly-markup , #{ \markup\tacet #}))
