\newBookPart#'()
\act "Atto Quinto"
\scene "Scena Prima" "Scena I"
\sceneDescription\markup\center-column {
  \wordwrap-center {
    [La scena si cangia in inferno.]
  }
  \wordwrap-center {
    [Ombra d’Eutyro, coro di Anime infernali,
    Clerica, Laomedonte, Bussiride.]
  }
}
%% 5-1
\pieceToc "[Sinf]onia"
\includeScore "FAAsinfonia"
%% 5-2
\pieceToc\markup\wordwrap {
  Eutyro, coro: \italic { Come solo ad un grido }
}
\includeScore "FABaria"
%% 5-3
\pieceToc\markup\wordwrap {
  Clerica Regina: \italic { Pera mora l'indegno }
}
\includeScore "FACrecit"
%% 5-4
\pieceToc\markup\wordwrap {
  Laomedonte Re di Troia: \italic { Pera mora il perverso }
}
\includeScore "FADrecit"
%% 5-5
\pieceToc\markup\wordwrap {
  Bussiride re d’Egitto: \italic { Pera mora l’iniquo }
}
\includeScore "FAErecit"
%% 5-6
\pieceToc\markup\wordwrap {
  Eutyro: \italic { Se nel terrestre mondo }
}
\includeScore "FAFrecit"
%% 5-7
\pieceToc\markup\wordwrap {
  Coro: \italic { Su, su dunque all’armi }
}
\includeScore "FAGcoro"
%% 5-8
\pieceToc "Sinf[onia]"
\includeScore "FAHsinfonia"

\scene "Scena Seconda" "Scena II"
\sceneDescription\markup\center-column {
  \wordwrap-center {
    [La scena si cangia in un portico del tempio di Giunone Pronuba.]
  }
  \wordwrap-center {
    Iole, Licco, Deianira, Ercole, coro di Sacerdoti.
  }
}
%% 5-9
\pieceToc\markup\wordwrap {
  Ercole, Licco, Iole: \italic { Alfine il ciel d’Amor }
}
\includeScore "FBArecit"
%% 5-10
\pieceToc\markup\wordwrap {
  Coro: \italic { Pronuba, e casta dèa }
}
\includeScore "FBBcoro"
%% 5-11
\pieceToc\markup\wordwrap {
  Ercole, Iole: \italic { E di che temi, Iole, e di che temi? }
}
\includeScore "FBCrecit"
%% 5-12
\pieceToc\markup\wordwrap {
  Coro: \italic { Pronuba, e casta dèa }
}
\includeScore "FBDcoro"
%% 5-13
\pieceToc\markup\wordwrap {
  Ercole: \italic { Ma qual pungente arsura }
}
\includeScore "FBErecit"
%% 5-14
\pieceToc\markup\wordwrap {
  Ercole: \italic { Ma l’atroce mia doglia }
}
\includeScore "FBFaria"
%% 5-15
\pieceToc\markup\wordwrap {
  Licco, Iole, Deianira: \italic { Che dite? Il mio non fu rimedio tardo }
}
\includeScore "FBGrecit"

\scene "Scena Terza" "Scena III"
\sceneDescription\markup\wordwrap-center {
  Iole, Deianira, Licco, Hyllo.
}
%% 5-16
\pieceToc\markup\wordwrap {
  Iole, Deianira, Licco, Hyllo: \italic { Veggio, o di veder parmi }
}
\includeScore "FCArecit"
%% 5-17
\pieceToc\markup\wordwrap {
  Iole, Deianira, Licco, Hyllo: \italic { Dall’occaso a gl’Eoi }
}
\includeScore "FCBquatro"

\scene "Scena Quarta" "Scena IV"
\sceneDescription\markup\center-column {
  \justify {
    [Cala Giunone nell’ultima macchina corteggiata dall’armonia de’
    cieli, ed apparisce nella più alta parte di questi Ercole sposato
    alla Bellezza.]
  }
  \wordwrap-center {
    Giunone, Deianira, Iole, Hyllo, Licco.
  }
}
%% 5-18
\pieceToc\markup\wordwrap {
  Giunone: \italic { Su, su allegrezza }
}
\includeScore "FDAaria"
%% 5-19
\pieceToc\markup\wordwrap {
  Giunone: \italic { Così deposti alfin gl’umani affetti }
}
\includeScore "FDBrecit"
%% 5-20
\pieceToc\markup\wordwrap {
  Iole, Deianira, Hyllo: \italic { Oh dèa come n’arrequii. }
}
\includeScore "FDCtre"
%% 5-21
\pieceToc\markup\wordwrap {
  Licco: \italic { Come a tante ruine }
}
\includeScore "FDDrecit"
%% 5-22
\pieceToc "Sinf[onia]"
\includeScore "FDEsinfonia"
%% 5-23
\pieceToc\markup\wordwrap {
  Giunone, Deianira, Iole, Hyllo e Licco: \italic { Contro due cor ch’avvampano }
}
\includeScore "FDFtutti"

\scene "Scena Quinta" "Scena V"
\sceneDescription\markup \wordwrap-center { Ercole, la Bellezza, coro di Pianeti. }
%% 5-24
\pieceToc\markup\wordwrap {
  Coro di Pianeti a 8: \italic { Quel grand’eroe }
}
\includeScore "FEAcoro"
%% 5-25
\pieceToc\markup\wordwrap {
  Bellezza e Ercole: \italic { Così un giorno avverrà con più diletto }
}
\includeScore "FEBduo"
%% 5-26
\pieceToc\markup\wordwrap {
  Coro: \italic { Virtù che soffre alfin mercede impetra }
}
\includeScore "FECcoro"
\actEnd "Fine Dell’ Oppera"
