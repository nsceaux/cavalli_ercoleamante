\clef "basse" re'2 la sib |
fa2. sol4 la2 sol1 fa2 |
do'2. do'4 sib2 do'1 do'2 |
re' do'4 si la2 sib2. la4 sol2 |
fa4 mi fa sol la2 |
\blackNotation { sib2 do'1 } fa1. |
fa |
do'2 sib la |
sib4 do' sib la sol2 la1 la2 |
la fa mi re re re |
mi1 mi2 la1 la2 |
la1. re2 dod re |
sol2. sol4 sol2 do1. |
do'2 do' do' fa2. fa4 fa2 |
sib sib sib sol1 sol2 |
la1. |
re |
la2 sol la re1. |
la1 la2 |
fa1 fa2 sol1 sol2 |
la1. re |
