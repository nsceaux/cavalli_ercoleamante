Am -- bro -- sia, è lat -- te cor -- ri o -- gni fiu -- me
stil -- li o -- gni
\tag #'(voix1 basse) { fon -- te. }
\tag #'voix2 { fon -- te still’ o -- gni fon -- te. __ }
\tag #'voix3 { fon -- te still’ o -- gni fon -- te. }

Di ro -- se in tat -- te
s’a -- dor -- ni il bo -- sco s’ad -- dob -- bi il mon -- te
\tag #'(voix1 voix2 basse) { fat -- ti lu -- cen -- ti }
gl’an -- tri di let -- ti -- no
le Ti -- gri al -- le -- ti -- no con lie -- ta fron -- te.
Di ro -- se in tat -- te
s’a -- dor -- ni il bo -- sco s’ad -- dob -- bi il mon -- te.
