<<
  \tag #'(voix1 basse) {
    \clef "vsoprano" re''2 do'' sib' |
    la'2. sib'4 do''2 sib'!1 la'2 |
    sol'2. la'4 sib'2 la' sol'( la') |
    fa'4\melisma mi' fa' sol' la'2 sol'4 fa' sol' la' sib'2 |
    la'4 sol' la' si' do''2 |
    sib'4 la' sib' la' sol'2\melismaEnd fa'1. |
    la'2 sol' fa' |
    sol'4\melisma re'4 mi' fa'8[ sol'] la'2 |
    fa'4 do' re' mi'8[ fa'] sol'2\melismaEnd mi'1. |
    mi'2 fa' sol' la' si' la' |
    la'1 sold'2 la'1 la'2 |
    mi'2 re' mi' fad'1 fad'2 |
    re'2 do' re' mi'2. mi'4 mi'2 |
    sol' fa' sol' la'2. la'4 la'2 |
    fa' mi' fa' sol'1 sol'2 |
    mi' re' mi' |
    fa'4\melisma sol' fa' mi' re'2 |
    mi'4 fa' mi' re' do'2 re'4 mi' re' do' si2 |
    dod'1\melismaEnd dod'2 |
    la' sol' la' si' dod'' re'' |
    re''1 dod''2 re''1 re''2 |
  }
  \tag #'voix2 {
    \clef "vtenor" fa'2 mi' re' |
    do'2. sib4 la2 re' mi'( fa') |
    mi'2. fa'4 sol'2 fa'1 mi'2 |
    re'4\melisma do' re' mi' fa'2~ fa'1\melismaEnd mi'2 |
    fa' mi'4( re' do'2) |
    fa'2 fa'2. mi'4\melisma fa'1.\melismaEnd |
    fa'2 mi' re' |
    mi'4\melisma fa' mi' re' do'2 |
    re'4 mi' re' do' si2 dod'1\melismaEnd dod'2 |
    \ficta dod' re' mi' fa' re' \ficta do' |
    si1 si2 la1 la2 |
    dod'2 si dod' re'1 re'2 |
    si2 la si do'2. do'4 do'2 |
    mi'2 re' mi' fa'2. fa'4 fa'2 |
    re'2 dod' re' mi'1 mi'2 |
    R1. |
    re'2 do' si |
    dod'4\melisma re' dod' re' mi'2 fa'4 sol' fa' mi' re'2 |
    mi'1\melismaEnd mi'2 |
    fa' mi' fa' re' mi' fa' |
    mi'1 mi'2 re'1 re'2 |
  }
  \tag #'voix3 {
    \clef "vtenor" re'2 la sib |
    fa2. sol4 la2 sol1 fa2 |
    do'2. do'4 sib2 do'1 do'2 |
    re'\melisma do'4 si la2 sib2.\melismaEnd la4 sol2 |
    fa4\melisma mi fa sol la2\melismaEnd |
    \blackNotation { sib2 do'1 } fa1. |
    R1. |
    do'2 sib la |
    sib4\melisma do' sib la sol2 la1\melismaEnd la2 |
    la fa mi re re re |
    mi1 mi2 la1 la2 |
    r1*3/2 re2 dod re |
    sol2. sol4 sol2 r1*3/2 |
    do'2 do' do' fa2. fa4 fa2 |
    sib2 sib sib sol1 sol2 |
    R1.*2 |
    la2 sol la re2.\melisma mi4 fa re |
    la1\melismaEnd la2 |
    fa fa fa sol sol sol |
    la1 la2 re'1 re'2 |
  }
>>
