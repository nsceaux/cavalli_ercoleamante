\score {
  <<
    \new GrandStaff <<
      \new Staff << \global \clef "dessus" >>
      \new Staff << \global \clef "dessus" >>
    >>
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix1 \includeNotes "voix"
      >> \keepWithTag #'voix1 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix2 \includeNotes "voix"
      >> \keepWithTag #'voix2 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix3 \includeNotes "voix"
      >> \keepWithTag #'voix3 \includeLyrics "paroles"
    >>
    \new Staff <<
      \global \includeNotes "basse"
      \origLayout {
        s1.*8\pageBreak
        s1.*10\pageBreak
        s1.*9\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
