A di -- o, Pag -- gio.

A di -- o, tut -- ti.

A ri -- ve -- der -- ci;
che de la don -- na a cui Er -- col pre -- su -- me
di far sì fa -- cil -- men -- te can -- giar cli -- ma,
non fu non fu mai suo cos -- tu -- me
d’ob -- be -- dir al -- la pri -- ma.

Oh che gran co -- se ho vi -- ste! an -- cor l’or -- ro -- re
tut -- to mi rac -- ca -- pric -- cia.

Et è sol mas -- tro A -- mo -- re,
che si fat -- ti bi -- tu -- mi og -- gi im -- pas -- tri -- ca,
ma con -- tro un sì pes -- ti -- fe -- ro bi -- gat -- to
sen -- ti gen -- til gar -- zo -- ne
im -- pa -- ra u -- na can -- zo -- ne.
