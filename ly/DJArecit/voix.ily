\ffclef "valto" <>^\markup\character Licco
r8 fa' re' re' sib4 sib |
\ffclef "vbas-dessus" <>^\markup\character Paggio
r8 re'' do'' do'' la'4 la' |
\ffclef "valto" <>^\markup\character Licco
r8 do' do' do' fa'4 fa' |
r8 do' do' do' do' do' do' do' |
do'4 do'8 sib do'4 do'8 do' |
do'8 do' do' do' do' do' do' sib |
re'4 re'8 sib re'4 re'8 sib |
fa'4 fa'8 sol' mib' mib' re' do' |
do'4 do'8 re' sib4 sib |
\ffclef "vbas-dessus" <>^\markup\character Paggio
sib'2 fa'8 fa' fa' sol' |
mib'4 mib' r2 |
r8 mib' mib' sol' mib'4 mib' |
mib'8 mib'16 mib' mib'8 re' re' re' r4 |
\ffclef "valto" <>^\markup\character Licco
r4 r8 <<
  \original { si8 si si si do' | }
  \pygmalion { sol'8 re' re' re' do' | }
>>
re'4 re' re'8 re' re' re'16 re' |
re'8 re' re' re'16 mi' do'8 do' r4 |
<<
  \original {
    r4 do' fa'8 fa' fa' fa' |
    fa'8. fa'16 fa'8 mi' fa'4 fa' |
    fa'8 fa'16 fa' fa'8 fa' re' re' r do' |
    do' do' do' do'16 re' sib8 sib r4 |
  }
  \pygmalion {
    r4 re' sol'8 sol' sol' sol' |
    sol'8. sol'16 sol'8 fad' sol'4 sol' |
    sol'8 sol'16 sol' sol'8 sol' mi' mi' r sol' |
    la'8 la'16 la' la'8 do'' do''8. si'16 do''4 |
  }
>>
