\clef "basse" sib,1 |
sib,4 do fa2 |
fa,1 |
fa, |
fa, |
fa, |
sib,~ |
sib,2 mib |
fa sib, |
sib,1 |
mib |
mib |
do2 re |
sol,1~ |
sol, |
sol,2 do |
<<
  \original {
    fa,1~ |
    fa, |
    la,2 sib,4. mib8 |
    fa2 sib, |
  }
  \pygmalion {
    sol,1~ |
    sol, |
    si,2 do4 r8 mi |
    fa2 sol4 do |
  }
>>
