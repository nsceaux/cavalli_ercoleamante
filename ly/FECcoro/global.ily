\key fa \major \midiTempo#160
\time 4/4 \measure 2/1 s1*6
\digitTime\time 3/4 \measure 9/4 s2.*3
\measure 12/4 s2.*4
\measure 6/4 s1.*2
\measure 12/4 s2.*4
\measure 6/4 s1.
\measure 12/4 s2.*4
\measure 9/4 s2.*3
\measure 6/4 s1.*5
\measure 9/4 s2.*3
\measure 6/4 s1.*2
\time 4/4 s1 \measure 3/1 s1*3 \bar "|."
