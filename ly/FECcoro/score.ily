\score {
  <<
    \new GrandStaff <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
    >>
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix11 \includeNotes "voix"
      >> \keepWithTag #'voix11 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix12 \includeNotes "voix"
      >> \keepWithTag #'voix12 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix13 \includeNotes "voix"
      >> \keepWithTag #'voix13 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix14 \includeNotes "voix"
      >> \keepWithTag #'voix14 \includeLyrics "paroles"
    >>
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix21 \includeNotes "voix"
      >> \keepWithTag #'voix21 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix22 \includeNotes "voix"
      >> \keepWithTag #'voix22 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix23 \includeNotes "voix"
      >> \keepWithTag #'voix23 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix24 \includeNotes "voix"
        \origLayout {
          s1*6 s2.*7\pageBreak
          s2.*14\pageBreak
          s2.*11\pageBreak
          s2.*9 s1\pageBreak
        }
      >> \keepWithTag #'voix24 \includeLyrics "paroles"
    >>
  >>
  \layout { }
  \midi { }
}
