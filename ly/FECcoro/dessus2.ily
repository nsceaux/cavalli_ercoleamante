\clef "dessus" r4 fa'' fa'' fa'' fa''2 fa'' |
r4 fa'' fa'' sib'' sol''2 mib'' |
r4 sib' fa'' re'' sol''1 |
mib''4 fa'' mib'' mib'' do''2 re''2. |
re''4 fa'' sib' re''2 mi''4 do''2. la''4 fa'' sib'' |
sol''4. sol''8 fa'' mi'' fa''4. fa''8 sol'' re'' |
sol''4 sol'' re''2 sol'' |
fa''2. do''4 sol'' fa'' fa''2 re''4 sol''2. |
mi''4 do'' re'' si'2 si'4 |
do'' do'' re''8 la' re''4 re'' la''2 re'' do''2. |
mi''4 la'' fa'' sol''2 sol'4 do''2. |
do''4 fa'' mib'' mib''4. re''8 do'' mib'' |
sib''2. r2*3/2 |
<>^\markup\concat { 2 \super o }
fa''4 re'' do'' do''2 do''4 |
fa'' re'' sib' <>^\markup\concat { 2 \super o } fa''4 sib'' la'' |
fa'' re'' sol''2 do'' |
sib'4 fa'' mi'' do'' do'' fa''2 do'' |
do''4 sol'' fa''8 do''' sib''4 fa''8 sib' mib''4 |
do''2 fa''4 sib'' sib' fa'' |
fa''2 do'' |
r4 fa'' do'' la' re''\breve\fermata |
