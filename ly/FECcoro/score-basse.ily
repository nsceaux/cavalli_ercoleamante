\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \keepWithTag #'voix14 \includeNotes "voix"
    >> \keepWithTag #'voix14 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'voix24 \includeNotes "voix"
    >> \keepWithTag #'voix24 \includeLyrics "paroles"
  >>
  \layout { }
}
