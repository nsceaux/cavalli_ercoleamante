\clef "dessus" <>^\markup { Suonano tutti due i cori }
r4 sib'' sib''8 do'''16 re''' do'''8 la'' r fa'' sol'' la'' sib''4 sib'' |
r sib'' sib'' fa''8 sib'' sib''2 sib''4 lab''8 do''' |
fa''2 sib'4. sib''8 sib''1 |
<>^\markup { con il \concat { 2 \super o } Choro }
sol''4 sib'' sol'' sol''2 la''4 fa''2. |
<>^\markup\concat { P \super o }
fa''4 la'' fa'' sib'' sol''2 la''2. do'''4 la'' sol'' |
do'''2. do'''2 do'''4 |
do''' do''' fa''2 do''' |
do'''2. <>^\markup\concat { 2 \super o } la''4 do''' la'' la''2 fa''4 do'''2. |
sol''4 mi'' la'' re''2 sol'4 |
sol'4 sol''4. fad''8 sol''4 sol'' do''2 sol'' sol''2. |
<>^\markup\concat { P \super o }
sol''4 do''' sib''! sib''8 fa'' sol'' la'' sib'' sol'' la''2. |
<>^\markup\concat { 2 \super o } fa''4 sib'' sib'' do'''2. |
fa'' <>^\markup\concat { P \super o } la'4 sib' do'' |
re'' sib' mib'' la'2 la'4 |
re'' fa'' sol'' do'' mib'' fa'' |
sib'' fa'' sib''2 fa'' |
<>^\markup\concat { 2 \super o }
fa''4 sib'' sol'' la'' la'' re''2 sol''2 |
<>^\markup\concat { P \super o }
fa''4 mib'' do'' <>^\markup { Tutti due } fa'' sib'' sol'' |
la''2. re''8 mi'' fa'' sol'' la'' re'' |
la'' sib'' do'''4 la'' sol'' |
do''4 re''8 mi'' fa''2 fa''\breve\fermata |
