\tag #'(voix11 voix12 voix13 voix14) {
  Vir -- tù che sof -- fre al -- fin mer -- ce -- de im -- pe -- tra
  e de -- gno cam -- po a suoi tri -- om -- fi
  \tag #'voix13 { a suoi tri -- om -- fi }
  è l’e -- tra.
  e de -- gno cam -- po
  a suoi tri -- om -- fi
  a suoi tri -- om -- fi è l’e -- tra
  a suoi tri -- om -- fi
  a suoi tri -- om -- fi è l’e -- tra.
}
\tag #'(voix21 voix22 voix23 voix24) {
  Vir -- tù che sof -- fre al -- fin mer -- ce -- de
  \tag #'voix23 { mer -- ce -- de }
  im -- pe -- tra
  e de -- gno cam -- po
  e de -- gno cam -- po a suoi tri -- om -- fi
  \tag #'voix21 { a suoi tri -- om -- fi }
  è l’e -- tra.
  e de -- gno cam -- po
  a suoi tri -- om -- fi
  a suoi tri -- om -- fi è l’e -- tra
  \tag #'voix22 { a suoi tri -- om -- fi }
  a suoi tri -- om -- fi è l’e -- tra.
}
