<<
  \tag #'voix11 {
    \clef "vsoprano" r4 fa'' fa'' fa'' fa''2 fa'' |
    r4 fa'' fa'' re'' sol''2 sol''4 fa'' |
    fa''1 sol'' |
    R2.*3 |
    re''4 do'' re'' sib'2. la' r2*3/2 |
    sol'4 la' sib' do''2.~ |
    do''4 do'' re''2 do'' |
    do''2. r1*3/2 r2*3/2 |
    R2.*6 |
    do''4 do'' re'' sib'2. la' |
    R2.*2 |
    sib'4 re'' mi'' fa''4.\melisma sol''8[ fa'' mib''] |
    re''4\melismaEnd re'' r r2*3/2 |
    r la'4 sib' do'' |
    sib'4 sib' sib'2 sib'4. la'8( |
    sib'2.) r1*3/2 |
    do''4 sib' la' re''4.\melisma re''8[ do'' sib'] |
    fa''2\melismaEnd fa''4 fa'' fa'' fa'' |
    fa''2 fa''4 sib' |
    fa''1 fa''\breve\fermata |
  }
  \tag #'voix12 {
    \clef "valto" r4 sib' sib' do'' sib'2 sib' |
    r4 sib' sib' sib' sib'2 sib'4 do'' |
    sib'1 sib' |
    R2.*3 |
    sib'4 fa' re' sol'2. do' r2*3/2 |
    mi'4 fa' sol' la'4.\melisma la'8[ sol' fa'] |
    sol'4\melismaEnd sol' sib'2 sol' |
    la'2. r1*3/2 r2*3/2 |
    R2.*6 |
    sol'4 la' re' sol'2. do' | \allowPageTurn
    R2.*2 |
    fa'4 fa' sol' la'4.\melisma fa'8[ sol' la'] |
    sib'4\melismaEnd sib' r r2*3/2 |
    r fa'4 mib' do' |
    fa'4 fa' sol'2 fa' |
    fa'2. r1*3/2 |
    do'4 mib' fa' fa'2. |
    fa' fa'4 sib' sib' |
    la'2 la'4 sib' |
    sib'4\melisma la'8[ sol'] la'2\melismaEnd sib'\breve\fermata |
  }
  \tag #'voix13 {
    \clef "vtenor" r4 re' re' fa' re'2 re' |
    r4 re' re' re' mib'2 mib'4 mib' |
    sib8[\melisma do' re' mib'] fa'2\melismaEnd mib'1 |
    R2.*3 |
    fa'4 do' fa' fa'2\melisma mi'4\melismaEnd fa'2. la4 do' re' |
    do'4 do' r fa' do' re' |
    mi' mi' fa'2 fa'4. mi'8( |
    fa'2.) r1*3/2 r2*3/2 |
    R2.*6 |
    mi'4 fa' fa' fa'2 mi'4( fa'2.) |
    r2*3/2 sol4 sib do' |
    re'4.\melisma re'8[ do' sib] do'4. do'8[ re' mi'] |
    fa'4\melismaEnd fa' r r2*3/2 |
    r do'4 sol' fa' |
    re'4 re' mib'2 do' |
    re'2. r1*3/2 |
    la4 sib do' re'8([ do'] sib2) |
    la r4 re' re' re' |
    fa'8([ mi'16 re'] do'4) do' sol |
    do'2.( fa'4) re'\breve\fermata |
  }
  \tag #'voix14 {
    \clef "vbasse" r4 sib sib fa sib2 sib |
    r4 sib sib sib mib2 mib4 mib |
    sib1 mib |
    R2.*3 |
    sib4 la sib sol2. fa fa4 la sib |
    do'4.\melisma sib8[ la sol] fa4. fa8[ mi re] |
    do4\melismaEnd do sib,2 do |
    fa,2. r1*3/2 r2*3/2 |
    R2.*6 |
    do'4 la sib sol2. fa |
    r2*3/2 mib4 sol la |
    sib4.\melisma sib8[ la sol] fa4. mib8[ re do] |
    sib,4\melismaEnd sib, r r2*3/2 |
    r fa4 sol la |
    sib4 sib mib2 fa2 |
    sib,2. r1*3/2 |
    fa4 sol la sib4.\melisma sib8[ la sol] |
    fa4\melismaEnd fa r sib, sib, sib, |
    fa2 fa4 sib |
    fa1 sib,\breve\fermata |
  }
  %%
  \tag #'voix21 {
    \clef "vsoprano" r4 re'' re'' do'' re''2 re'' |
    r4 re'' re'' fa'' mib''2 mib''4 mib'' |
    mib''4\melisma re''8[ do''] re''2\melismaEnd mib''1 |
    mib''4 sib' sib' sib'2 la'4( sib'2.) |
    R2.*8 |
    r2*3/2 fa''4 do'' do'' do''2 si'4( do''2.) |
    mi'4 sol' la' sol' sol' r |
    do'' sol' la' si' si' do''2 do''4. si'8( do''2.) |
    R2.*3 |
    la'4 sib' sib' sib'2 la'4 |
    R1. |
    r2*3/2 fa'4 sol' la' |
    sib'4.\melisma re''8[ do'' sib'] la'4\melismaEnd la' r |
    R2.*2 |
    sib'4 sib' do'' la' la' sib'2 sol' |
    la'2. r2*3/2 |
    la'4 sib' do'' re''4.\melisma re''8[ do'' sib'] |
    do''2\melismaEnd do''4 mib'' |
    do''1 re''\breve\fermata |
  }
  \tag #'voix22 {
    \clef "valto" r4 fa' fa' la' fa'2 fa' |
    r4 fa' fa' fa' sol'2 sol'4 lab' |
    fa'2( sib'2) sol'1 |
    sol'4 fa' sol' mib'2. re' |
    R2.*8 |
    r2*3/2 la'4 sol' la' fa'2. mi' |
    r2*3/2 re'4 mi' fa' |
    sol'2.~ sol'4 sol' la'2 sol' sol'2. |
    R2.*3 |
    fa'4 fa' sol' mib'2. |
    re' r2*3/2 |
    re'4 fa' sol' do'\melisma re'8[ mib'] fa'[ mib'] |
    re'4. do'8[ re' mi'] fa'4\melismaEnd fa' r |
    R2.*2 |
    fa'4 fa' sol' fa' fa' fa'2 fa'4. mi'8( |
    fa'2.) re'4 fa' sol' |
    do'2 do'4 fa' fa' fa' |
    fa'2 fa'4 sol' |
    fa'1 fa'\breve\fermata |
  }
  \tag #'voix23 {
    \clef "vtenor" r4 sib fa do' fa2 fa |
    r4 fa sib sib sib sib8 sib sol4 do'8 do |
    fa2.\melisma sib4\melismaEnd sib1 |
    sib4 re' sib do'2. fa |
    R2.*8 |
    r2*3/2 do'4 mi' do' fa'2. sol |
    r2*3/2 si4 do' re' |
    mi'4.\melisma mi'8[ re' do'] re'4\melismaEnd re' fa'2 re' mi'2. |
    R2.*3 |
    do'4 re' sol do'2. |
    fa r2*3/2 |
    r la4 sib do' |
    fa4.\melisma sol8[ la sib] do'4\melismaEnd do' r |
    R2.*2 |
    re'4 re' sol do' do' re'2 do' |
    do'2. r2*3/2 |
    fa4 sol la sib4.\melisma sib8[ fa sol] |
    la2\melismaEnd la4 r |
    r4 fa do'2 sib\breve\fermata |
  }
  \tag #'voix24 {
    \clef "vbasse" r4 sib, sib, la, sib,2 sib, |
    r4 re re sib, mib2 mib4 lab, |
    sib,1 mib |
    mib4 re mib do2. sib, |
    R2.*8 |
    r2*3/2 fa4 mi fa re2. do |
    do4 mi fa sol4.\melisma fa8[ mi re] |
    do4. do8[ si, la,] sol,4\melismaEnd sol, fa,2 sol, do2. |
    R2.*3 |
    fa4 re mib do2. |
    sib, r2*3/2 |
    sib,4 re mib fa4.\melisma mib8[ re do] |
    sib,4. sib,8[ la, sol,] fa,4\melismaEnd fa, r |
    R2.*2 |
    sib,4 re mi fa fa sib,2 do |
    fa,2. sib,4 re mib |
    fa4.\melisma mib8[ re do] sib,4. sib,8[ la, sol,] |
    fa,2\melismaEnd fa,4 mib, |
    fa,1 sib,\breve\fermata |
  }
>>
