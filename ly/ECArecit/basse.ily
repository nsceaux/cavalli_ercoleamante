\clef "basse" mi1~ |
mi |
mi |
mi |
la,~ |
la,2 fa,~ |
fa,1~ |
fa,2 mi, |
mi,1~ |
mi,~ |
mi,2 do~ |
do1 |
la,2 si, |
mi mi |
la1 |
si2 do'8. fa16 sol4 |
do1 |
do2 sol, |
sol,1 |
mi2 la4 sol~ |
sol la re2 |
\original {
  la,1~ |
  la, |
  sol,~ |
  sol,2 fa, |
  mi,1 |
  do |
  do~ |
  do4 fad, sol,2 |
  re1~ |
  re |
  re |
  re~ |
  re~ |
  re~ |
  re~ |
  re |
  la,2 sol, la, re, |
  sol,1~ |
  sol, |
  sol, |
  do~ |
  do |
  do |
  fa, |
  fa,8 sol, la, si, do re mi fa |
  sol4 do r2 |
}
la,2. |
la,4 sold,2 |
la,4 mi,2 |
la,1 |
sol,2 fa,~ |
fa, sib, |
la,1~ |
la,2 re, |
