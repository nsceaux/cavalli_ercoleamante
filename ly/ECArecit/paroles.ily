E non si tro -- va
fra gl’ar -- men -- ti squa -- mo -- si
un cor ben -- ché ge -- la -- to,
che qual già d’A -- ri -- o -- ne
di quel mes -- chin gar -- zo -- ne
sen -- ta qual -- che pie -- ta -- de, e sal -- vi in -- sie -- me
gli ul -- ti -- mi a -- van -- zi in lui d’o -- gni mia spe -- me!
ohi -- mè, ch’il mar con cen -- to fau -- ci, e cen -- to
tut -- te rab -- bia spu -- man -- ti
non par ch’ad al -- tro fu -- ri -- o -- so a -- ne -- li
ch’a di -- vo -- rar quel po -- ve -- rel -- lo. Ah da -- te
a sì mor -- tal pe -- ri -- glio
pron -- to soc -- cor -- so o cie -- li;
ohi -- mè, che più tar -- da -- te?

Ah che quel -- la vo -- ra -- gi -- ne l’in -- goi -- a,
dun -- que forz’ è, che dis -- pe -- ra -- to io moi -- a:
\original {
  e chi chi fia più che vie -- ti
  al -- la mia bel -- la d’e -- se -- gui -- re i suo -- i
  mal’ ac -- cor -- ti de -- cre -- ti? a che più pen -- so?
  Che più tar -- do a fi -- ni -- re
  con un bre -- ve mo -- ri -- re un duol im -- men -- so?
  Ce -- ru -- le -- i hu -- mi -- di nu -- mi,
  ri -- ce -- ve -- te pro -- pi -- zi un sven -- tu -- ra -- to,
  che dal ciel, da la ter -- ra, e da gl’a -- bis -- si,
  sem -- pre a ga -- ra ol -- trag -- gia -- to
  vie -- ne a cer -- car tra le vostr’ ac -- que in sor -- te
  per gran fa -- vor la mor -- te.
  Hyl -- lo, su su al mar t’av -- ven -- ta;
  che te -- mi, or -- che, e ba -- le -- ne?
  O pur di, di! ti spa -- ven -- ta
  l’i -- ma -- gin del mo -- rir squal -- li -- da, e te -- tra;
  chi fug -- ge ge -- lo -- sia nul -- la l’ar -- re -- tra:
}
su, su, dun -- que dun -- que a mo -- rir, ch’el chia -- ro no -- me
dell’ a -- ma -- to mio so -- le
in -- do -- rar mi po -- trà l’om -- bre più den -- se
del Tar -- ta -- ro pro -- fon -- do: I -- o -- le, I -- o -- le.
