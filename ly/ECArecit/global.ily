\key do \major
\time 4/4 \midiTempo#100 s1*21
\original {
  s1*16
  \measure 2/1 s1*2
  \measure 4/4 s1*9
}
\digitTime\time 3/4 s2.*3
\time 4/4 s1*5 \bar "|."
