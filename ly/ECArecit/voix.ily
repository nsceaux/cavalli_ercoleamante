\clef "vtenor" <>^\markup\character Hyllo
r8 mi' mi' re' mi' mi' mi' mi' |
mi'4 mi'8 fa' re' re' r re' |
re'4 re'8 re'16 do' re'8 re' r re' |
re'4 re'8 re'16 re' re'8 re' r mi' |
do' do' do' si do' do' do' do' |
do'4 do'8 si re' re'16 re' re'8 re' |
la4 la la8 la16 la si8 do' |
re'4 re'8 re'16 la si8 si r4 |
r8 mi' si si si4. si8 |
si8 si si la si4 si |
mi'8 mi' mi' mi'16 mi' la4 la8 la |
mi'8 mi' mi' mi' mi' mi' mi' fad' |
fad' fad'16 fad' fad'8 fad' fad' fad' fad' sol' |
mi'4 mi' r sol' |
fa'4 fa'8 fa' fa' fa' fa' fa' |
fa' fa' fa' fa'16 mi' mi'8 mi'16 la' re'4 |
do' r8 sol' mi' mi' mi' mi' |
do'4 do'
\once\override TextScript.self-alignment-X = #RIGHT
<>^\markup\italic { [Il Paggio si sommerge.] }
r16 re'( sol'4) re'8 |
re'8 re'16 re' re' re' re' re' sol8 sol r4 |
sol'8 mi'16 sol' dod'4 r8 fa' fa'16 fa' fa' fa' |
re'8 re' r4 r2 |
\original {
  r8 mi'8 do'4 mi'8 do' do' si |
  do'4 do' r8 do' do' si |
  si4 si si8 si si si |
  si si si do' re'4 re'8 la |
  si4 si r2 |
  r8 mi' mi' mi' do' do' r4 |
  r8 sol'16 sol' sol'8 sol'16 sol' mi' mi' mi' mi' mi'8 mi'16 re' |
  mi'8 mi'16 mi' mi'8 re' re'4 re' |
  r4 r8 re' fa'8. fa'16 fa'4 |
  fa' fa'8 mi' fa'4 fa' |
  fa'8 fa' fa' fa'16 fa' fa'8 mi'16 mi' mi'8 fa' |
  re'4 re' la8 la re'4 |
  r4 re'8 re' la la16 la la8 la |
  fa4 fa fa8 fa fa fa16 sol |
  la4 la la la8 si |
  si4. si8 si si si dod' |
  dod' dod'16 dod' dod'8 re' re'4 r8 re' la4 la r2 |
  re'8 si r4 si re' |
  r8 re' si re' sol sol r4 |
  r r8 sol' re' re' re' re'16 do' |
  mi'8 mi' r do' mi'4 mi' |
  sol'4 mi'8 sol' do'4 do'8 do' |
  do' do' do' re' sib4 sib8 sib16 la |
  la4 la r r8 do' |
  la si do' re' mi' fa' sol' la' |
  re'8. re'16( do'4) r2 |
}
r4 r la |
mi' mi' mi' |
do' do' do' |
la8 la mi' mi' mi' mi' mi' mi' |
mi'4 mi'8 fa' fa' fa' re' re' |
re'4 re'8 re' re'4 mi'8 mi'16 mi' |
dod'4 dod'8 dod' re'8. re'16 re'8 re' |
la8 la r mi' fa' re'16 re' fa'8
\once\override TextScript.self-alignment-X = #RIGHT
re'^\markup\italic { [Hyllo si precipita in mare.] }
