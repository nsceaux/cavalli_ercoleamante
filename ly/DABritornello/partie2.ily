\clef "viole2"
fa'4 la' re' re' |
sol' mib' fa' re' do' do' sib2 |
do'4 mi' fa' do' |
re' sol' do' sol' |
do' re' sol' fa' re' do' do'2 |
re'4 la' re' re' |
sol' mib' fa' re' |
do' do' sib2 |
