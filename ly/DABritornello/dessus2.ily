\clef "dessus"
re''4 do'' re''8 mib'' fa''4 |
mib'' mib'' re'' fa'' do''4. fa''8 re''2 |
do''4 do'' re'' fa'' |
re'' mi'' fa'' re'' |
do'' fa'' mi'' fa'' fa''4. mi''8 fa''2 |
re''4 do'' re''8 mib'' fa''4 |
mib'' mib'' re'' fa'' |
do''4. do''8 re''2 |
