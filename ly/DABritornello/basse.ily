\clef "basse"
sib4 la sol re |
mib do re sib, mib! fa sib,2 |
fa4 mi re la, |
sib, sol, fa, sol, |
la, sib, do la, sib, do fa,2 |
sib4 la sol re |
mib do re sib, |
mib fa sib,2 |
