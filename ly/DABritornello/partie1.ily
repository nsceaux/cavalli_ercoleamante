\clef "viole1"
sib'4 fa' sib' sib' |
sib' do'' la' sib' sol' fa' fa'2 |
fa'4 sol' la' do'' |
sib' sib' fa' sib' |
la' fa' do'' do'' sib' sol' fa'2 |
fa'4 fa' sib' sib' |
sib' do'' la' sib' |
sol' fa' fa'2 |
