\clef "dessus"
fa''4 fa'' sol'' sib'' |
sol'' la'' fa'' sib'' sib''4. la''8 sib''2 |
la''4 sol'' fa'' la'' |
fa'' sol'' la'' sol'' |
fa'' sib'' sol'' la'' sol''4. sol''8 la''2 |
fa''4 fa'' sol'' sib'' |
sol'' la'' fa'' sib'' |
sib''4. la''8 sib''2 |
