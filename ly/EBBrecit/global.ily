\key do \major \midiTempo#100
\time 4/4 s1*5
\measure 2/1 s1*2
\measure 4/4 s1*18
\measure 2/1 s1*2
\measure 4/4 s1*10
\measure 2/1 s1*2
\measure 4/4 s1*16
\measure 2/1 s1*2
\measure 4/4 << \original s1*11 \pygmalion s1*2 >> \bar "|."
%% Paggio
s1*8 \bar "|."
