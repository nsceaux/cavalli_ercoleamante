\ffclef "vtenor" <>^\markup\character Hyllo
r8 re'16 re' re'8 re'16 re' si8 si r si |
dod' dod' r8 mi' la la r4 |
r do'8 do' la la r si16 re' |
sold4 sold r mi' |
mi'8 mi' mi' fa' fa'4 fa' |
r8 re' re' do' la la r4 r1 |
\ffclef "vbas-dessus" <>^\markup\character Paggio
r8 si' si' si' r si' si' do'' |
do'' do'' r do'' do'' do'' do'' do'' |

do'' do'' do'' si' re'' re'' r si' |
si' si' si' si' la' la' la' si' |
sol' sol' r4
\ffclef "vtenor" <>^\markup\character Hyllo
r8 re' re' mi' |
do' do' r4 r2 |
<>^\markup\italic { [legge il biglietto] }
r4 r8 re' re' re' re' re' |
re'4 re' r re'8 re' |
re'4 re'8 << \original mi' \pygmalion dod' >> mi'4 mi' |
r mi' mi'8 mi' mi' mi' |
mi'4 mi'8 mi' mi'8. mi'16 mi'8 fa' |
re'4 re' r2 |
r4 r8 si si si si la |
do'4 do' r mi'~ |
mi' sib8 la la4 la |
r4 sol'4~ sol'8 mi' mi' fa' |
re' dod' re'2 mi'4 |
r8 mi' fa'2 sol'4 |
sol' dod'8 re' re'4. dod'8( re'1)\fermata |
si8 si16 si re'8 re' r4 mi' |
la4 la re'8 la re' re'16 re' |
si8 si r si re'4 re'8 re' |
re' re' re' re'16 re' re'8 re' re' mi' |
do'4 do' r do'8 re' |
sib2 r4 sol8 sol |
do' do' do' do' do'4 do'8 re' |
sib sib sib la

la4 la8 la |
la8 la la la re'4 re' |
si8 si si si16 si mi'4 mi' |
r8 mi' la do' do'4. si8( do'1) |
la4 r la8 la la sol |
la4 la r8 dod' dod' dod' |
dod'4 dod' dod'8 dod' dod' dod'16 dod' |
fa'4 fa' r8 fa' fa' mi' |
mi'4 mi' mi'8 mi' mi' fa' |
re'4 re' r8 re' re' re' |
re'4 re'8 do' do' do' do' re' |
sib sib r4 r sib!8 la |
la4 la r la8 sib |
sol4 sol8 sol sol sol sol sol |
sol sol sol la fa4 fa |
r mi'8 sol' dod'4 dod'8 si |
dod'2 fa'~ |
fa' re' |
r4 sol' fa' mib' |
re'1~ |
re'2 dod'( re'1) |
\original {
  mi'4 do' r do' |
  do'4 do'8 si do' do' do' do' |
  do'4 do'8 do' do' do' do' re' |
  mi' mi' mi' mi' si4 si8 si |
  si4 si si si8 do' |
  la4 la r do'8 do' |
  do'4 re'8 mi' fa'4 fa'8 mi' re' do' sib[ la] la4 r |
  R1 |
}
r4 r8 si si si si si |
re'4 re'8 re'16 la si8 si r4 |
\ffclef "vbas-dessus" <>^\markup\character Paggio
r4 sol'8 sol' sol'4 sol'8 sol' |
sol' sol' sol' sol' sol'4 sol'8 la' |
la'4 la' r8 la' la' sol' |
la' la' la' la' la'4 la'8 sol' |
si'4 si' r mi'' |
r4 re''8 re'' si'4 si'8 mi'' |
si'4 si'8 mi'' si' si' si' si' |
mi' mi' r4 r2 |
