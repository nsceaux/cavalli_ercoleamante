\clef "basse" sol,1 |
mi,2 re, |
la, fa, |
mi,1 |
dod2 re~ |
re mi la,1\fermata |
sol,1~ |
sol,2~ sol,~ |

sol,~ sol,~ |
sol,2. fad,4 |
sol,2~ sol, |
do1\fermata |
re1~ |
re~ |
re |
dod~ |
dod |
re |
sold, |
la, |
sol,2 fa, |
mi, dod |
fa sol |
la sib |
sol la re1\fermata |
sol,1~ |
sol,2 fad, |
sol,1~ |
sol, |
mi,~ |
mi,~ |
mi,~ |
mi,2 fa, |
fa,1 |
sol, |
la,4. fa,8 sol,2 do1 |
fa1~ |
fa~ |
fa2 mi |
re1~ |
re2 do~ |
do sib, |
la,2~ la, |
sol, sol~ |
sol fa~ |
fa mi |
mi re |
sol,1 |
la, |
sib, |
sol,~ |
sol,4 sol, fad, sol, |
la,1 re |
\original {
  la,1~ |
  la,~ |
  la, |
  sold,~ |
  sold, |
  la,1~ |
  la,2 re,~ |
  re, re,4 mi, |
  la,1 |
}
sol,1~ |
sol,4 fad, sol,2 |
sol,1 |
sol, |
re |
re |
sol, |
fa,2 mi,~ |
mi,1~ |
mi, |
