Che no -- vel -- la m’ar -- re -- chi? è buo -- na, o re -- a?
Ma che par -- lo in -- fe -- li -- ce?
Spe -- rar più ve -- run be -- ne a me non li -- ce.

I -- o -- le al -- fin as -- tret -- ta
di ma -- ri -- tar -- si al fu -- ri -- bon -- do Al -- ci -- de
con ques -- to fo -- glio a te mi spin -- se in fret -- ta.

Por -- gi -- lo dun -- que;
“Al -- la tua fé tra -- di -- ta,
chie -- do gius -- to per -- do -- no,
se per ser -- bar -- ti in vi -- ta
ad Er -- co -- le mi do -- no.”
Che per ser -- bar -- mi in vi -- ta? Oh cie -- co er -- ro -- re!
Ah, che ciò per me sia mor -- te sia mor -- te mor -- te peg -- gio -- re. __
Tor -- na ve -- lo -- ce, oh di -- o,
tor -- na tor -- na ve -- lo -- ce, e dil -- le,
ch’es -- sen -- do es -- sa fe -- de -- le all’ a -- mor mi -- o,
se mor -- rò, sì con -- ten -- to
scen -- de -- rà ques -- to spir -- to al bas -- so mon -- do,
ch’in al -- cun tem -- po ma -- i
non ne vi -- der gli e -- li -- si un più gio -- con -- do. __
Ma che, s’al -- trui si do -- na, o il duol a -- tro -- ce
di sì per -- fi -- da sor -- te,
o la mia des -- tra mi da -- rà in tal pun -- to
u -- na sì a -- ma -- ra, e scon -- so -- la -- ta mor -- te,
ch’af -- fan -- no -- sa, e do -- len -- te
quest’ al -- ma in ap -- pro -- dar le sti -- gie a -- re -- ne
in -- fin qui -- vi par -- rà mos -- tro mos -- tro di pe -- ne. __
\original {
  Dil -- le, che s’el -- la al -- me -- no
  per cos -- tan -- za d’a -- mor sa -- rà pur mi -- a
  non fa -- rà di me strag -- gi al -- tri ch’Al -- ci -- de,
  ma che s’el -- la mi las -- sia, el -- la m’uc -- ci -- de.
}
Sa -- prai tu ben ri -- dir ques -- te que -- re -- le?

Pur ch’il mar in -- fe -- de -- le
non mi vie -- ti il ri -- tor -- no, e di già par -- mi
che ben vo -- glia a -- gi -- tar -- mi: o nu -- mi al -- go -- si
cor -- re -- te cor -- re -- te al mio soc -- cor -- so.
