\clef "basse"
\pygmalion {
  sol,2 sol,4 fad, |
  sol,2 sol,4 fad, |
  sol,2 sol8 fa mi re |
  do2 re sol,1 |
  re2 re4 la, |
  re2 re4 dod |
  re2 re4 la, |
  re2 re8 do si, la, |
  sol,2 sol8 fa mi re |
  do2 re |
  sol2 r |
}
sol2 sol4 fad |
sol2 sol4 re |
sol2 sol4 fad |
mi2 re |
re2 re4 la, |
re2 re4 dod |
re2 re4 la, |
re2 re8 do si, la, |
sol,2 sol8 fa mi re |
do2 re |
sol, sol,4 fad, |
sol,2 sol,4 fad, |
sol,2 sol8 fa mi re |
do2 re sol,1\fermata |
