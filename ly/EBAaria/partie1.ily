\clef "viole1"
\pygmalion {
  re'2 re'4 re' |
  re'2 re'4 re' |
  re'2 re'4 sol' |
  sol'4. sol'8 re'2 re'1 |
  la'2 la'4 la' |
  la'2 la'4 la' |
  la'8 sol' la' si' la'4 la' |
  la'2 fad'8 mi' re' dod' |
  re'2 sol'4 sol' |
  sol'4. sol'8 re'2 |
  re'2 r |
}
re'2 re'4 re' |
sol'2 sol'4 fad' |
re'2 re'4 re' |
re'4. dod'8 la2 |
la'2 la'4 la' |
la'2 la'4 la' |
la'8 sol' la' si' la'4 la' |
la'2 fad'8 mi' re' dod' |
re'2 sol'4 sol' |
sol'4. sol'8 re'2 |
re' re'4 re' |
re'2 re'4 re' |
re'2 re'4 sol' |
sol'4. sol'8 re'2 re'1\fermata |
