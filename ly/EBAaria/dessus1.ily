\clef "dessus"
\pygmalion {
  sol'8 fad' sol' la' sol'4 la' |
  si'8 la' si' do'' si'4 re'' |
  re''8 do'' si' la' sol'4 si' |
  do''8 si' la' sol' sol'4. fad'8 sol'1 |
  fad''8 mi'' fad'' sol'' fad''4 la'' |
  la''2 la''4 mi'' |
  re''8 dod'' re'' mi'' re''4 dod'' |
  fad''8 mi'' fad'' sol'' la''4 re'' |
  si'8 la' si' do'' re''4 sol'' |
  sol''8 fa'' mi'' mi'' re''4 la' |
  sol'2 r |
}
R1
sol'8 fad' sol' la' sol'4 la' |
si'8 la' si' do'' si'4 re'' |
mi''8 dod'' re'' mi'' la'4 la' |
fad''8 mi'' fad'' sol'' fad''4 la'' |
la''2 la''4 mi'' |
re''8 dod'' re'' mi'' re''4 dod'' |
fad''8 mi'' fad'' sol'' la''4 re'' |
si'8 la' si' do'' re''4 sol'' |
sol''8 fa'' mi'' mi'' re''4 la' |
sol'8 fad' sol' la' sol'4 la' |
si'8 la' si' do'' si'4 re'' |
re''8 do'' si' la' sol'4 si' |
do''8 si' la' sol' sol'4. fad'8 sol'1\fermata |
