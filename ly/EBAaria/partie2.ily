\clef "viole2"
\pygmalion {
  si2 si4 la |
  sol2 si4 la |
  sol2 sol4 sol |
  do' mi la2 sol1 |
  re'2 re'4 dod' |
  re'2 la4 dod' |
  la2 la4 mi' |
  re'2 re'4. la8 |
  si2 si4 do'8 re' |
  mi'4 mi la2 |
  si2 r |
}
si2 si4 la |
re'2 re'4 re' |
si2 si4 la |
sol4. sol8 re'2 |
re'2 re'4 dod' |
re'2 la4 dod' |
la2 la4 mi' |
re'2 re'4. la8 |
si2 si4 do'8 re' |
mi'4 mi la2 |
si2 si4 la |
sol2 si4 la |
sol2 sol4 sol |
do' mi la2 sol1\fermata |
