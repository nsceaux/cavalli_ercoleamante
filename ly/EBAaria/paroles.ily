\tag #'(couplet1 basse) {
  Ze -- fi -- ri che gi -- te
  da’ vi -- ci -- ni fio -- ri
  in -- vo -- lan -- do o -- do -- ri
  e qua poi fug -- gi -- te;
  fa -- te al -- la mia pro -- ra
  ch’ogg’ il mar si spia -- ni,
  voi pur cor -- te -- gia -- ni
  voi pur cor -- te -- gia -- ni
  sie -- te dell’ au -- ro -- ra.
}
\tag #'couplet2 {
  No -- to è a voi Cu -- pi -- do
  che d’ogn’ un fa gio -- co,
  e per l’al -- trui fuo -- co
  or me trae dal li -- do.
  A voi pur con -- ven -- ne
  far l’uf -- fi -- cio mi -- o,
  co -- sì a -- ves -- si an -- ch’i -- o
  co -- sì a -- ves -- si an -- ch’i -- o
  co -- me voi le pen -- ne.
}
