\clef "dessus"
\pygmalion {
  R1 |
  sol'8 fad' sol' la' sol'4 la' |
  si'8 la' si' do'' si'4 re'' |
  mi''8 re'' do'' si' la'2 si'1 |
  re''8 dod'' re'' mi'' re''4 mi'' |
  fad''8 mi'' fad'' sol'' fad''4 la'' |
  la''2 la''4 la'' |
  re''8 dod'' re'' mi'' fad''4 fad'' |
  sol''8 fad'' sol'' la'' si''4 si'' |
  do'''8 si'' la'' sol'' sol''4 fad'' |
  sol''2 r |
}
R1*2
sol'8 fad' sol' la' sol'4 la' |
si'8 la' sol' mi' fad'4 fad' |
re''8 dod'' re'' mi'' re''4 mi'' |
fad''8 mi'' fad'' sol'' fad''4 la'' |
la''2 la''4 la'' |
re''8 dod'' re'' mi'' fad''4 fad'' |
sol''8 fad'' sol'' la'' si''4 si'' |
do'''8 si'' la'' sol'' sol''4 fad'' |
sol''2 r |
sol'8 fad' sol' la' sol'4 la' |
si'8 la' si' do'' si'4 re'' |
mi''8 re'' do'' si' la'2 si'1\fermata |
