\clef "vbas-dessus"
\pygmalion R1*12
<>^\markup\character Paggio
<< \original { sol'8. fad'16 } \pygmalion { sol'8 fad' } >> sol'8 la' sol'4 la' |
si'8 la' si' do'' si'4 re'' |
sol'8 la' sol' fad' sol'4 re' |
sol'8 la' si' dod'' re''4 re'' |
R1 |
re'8 dod' re' mi' re'4 mi' |
fad'8 mi' fad' sol' fad'4 la' |
la'8 sol' fad' mi' re'4 re' |
re''8 do'' si' la' sol'4 sol' |
mi''8 re'' do'' si' la'2 |
sol'2 r |
R1*4 |
