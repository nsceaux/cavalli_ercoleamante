\transpose do #(if (eqv? #t (ly:get-option 'pygmalion))
                   (ly:make-pitch -2 4)
                   (ly:make-pitch -1 0)) {
\clef "basse" \tag #'basse-continue {
  do4 mib do sol2. |
  mib4 fa sol do2. |
  do'4 si do' sol2. |
  lab4 fa sol do2. |
  do4 re2 mib2. |
  sib,4 do2 re2. |
  re4 mi fad sol2. |
  fa4 mib2 re sib,4 |
  do re2 sol,2.\fermata |
  sol4 fad sol re2. |
  mib4 do re sol,2. |
  sol4 la2 sib2. |
  re4 mi2 fa2. |
  la,4 si,2 do2. |
  re4 mib fa sib,2. |
  sib4 la sib fa2. |
  sol4 mib fa sib,2. |
  sib, fa |
  do sol |
  fa lab sol |
  sol4 sol2 do2. |
  do'4 si do' sol2. |
  lab4 fa sol do1*3/4\fermata |
}
}
