\transpose do #(if (eqv? #t (ly:get-option 'pygmalion))
                   (ly:make-pitch -2 4)
                   (ly:make-pitch -1 0))
<<
  \tag #'coro1 {
    \original\clef "vsoprano"
    \pygmalion\clef "petrucci-c1/G_8"
    R2.*4 |
    do''4 re'' do'' si'2. |
    do''4 do'' si' do''2. |
    <<
      \original {
        do''4 sib' la' sol'2 sol'4 |
        sib' la' sol' fad'2 fad'4 |
        fad' sol' la' re'2 re'4 |
        fa' sol'4. sol'8 fad'2 sol'4 |
        sol' sol'4. fad'8 sol'2. |
      }
      \pygmalion R1.*5
    >>
    sib'4 re'' sib' la'2. |
    sol'4 do'' la' si'2. |
    R2.*8 |
    sib'4 do'' sib' la'2. |
    sib'4 sib' la' sib'2. |
    <<
      \original {
        sib'4 fa'4. sol'8 lab'2 lab'4 |
        sol'4 sol' la' sib'2 sib'4 |
        lab'4 lab'4. sib'8 do''4.\melisma re''8[ do'' re''] si'4\melismaEnd sol'2 |
        do''4 do''4. si'8 do''2. |
      }
      \pygmalion R2.*9
    >>
    do''4 re'' do'' si'2. |
    do''4 do'' si' do''1*3/4\fermata |
  }
  \tag #'coro2 {
    \original\clef "valto"
    \pygmalion\clef "petrucci-c3/G_8"
    R2.*4 |
    sol'4 sol' sol' sol'2. |
    mib'4 lab' sol' sol'2. |
    <<
      \original { R2.*10 | }
      \pygmalion {
        do''4 sib' la' sol'2 sol'4 |
        sib' la' sol' fad'2 fad'4 |
        fad' sol' la' re'2 re'4 |
        fa' sol'4. sol'8 fad'2 sol'4 |
        sol' sol'4. fad'8 sol'2. |
      }
    >>
    sol'4 la' sol' fad'2. |
    sol'4 sol' fad' sol'2. |
    R2.*8 |
    fa'4 fa' fa' fa'2. |
    re'4 sol' fa' fa'2. |
    <<
      \original {
        r2*3/2 fa'4 do'4. re'8 |
        mib'2 mib'4 re' re' mib'! |
        fa'2 fa'4 mib' mib'4. fa'8 sol'2 re'4 |
        lab'4 re'4. re'8 do'2. |
      }
      \pygmalion {
        sib'4 fa'4. sol'8 lab'2 lab'4 |
        sol'4 sol' la' sib'2 sib'4 |
        lab'4 lab'4. sib'8 do''4.\melisma re''8[ do'' re''] si'4\melismaEnd sol'2 |
        do''4 do''4. si'8 do''2. |
      }
    >>
    sol'4 sol' sol' sol'2. |
    mib'4 lab' sol' sol'1*3/4\fermata |
  }
  \tag #'coro3 {
    \clef "vtenor" R2.*4 |
    mib'4 re' mib' re'2. |
    do'4 fa' re' mi'2. |
    mib'4 re' do' sib2 sib4 |
    re' do' sib la2 la4 |
    re'4 re' do' sib2 sib4 |
    re' re'4. do'8 re'2 re'4 |
    mib'4 la4. la8 si2. |
    re'4 re' re' re'2. |
    sib4 mib' re' re'2. |
    R2.*8 |
    re'4 do' re' do'2. |
    sib4 mib' do' re'2. |
    <<
      \original R2.*9
      \pygmalion {
        r2*3/2 fa'4 do'4. re'8 |
        mib'2 mib'4 re' re' mib'! |
        fa'2 fa'4 mib' mib'4. fa'8 sol'2 re'4 |
        lab'4 re'4. re'8 do'2. |
      }
    >>
    mib'4 re' mib' re'2. |
    do'4 fa' re' mi'1*3/4\fermata |
  }
  \tag #'coro4 {
    \clef "vbasse" do4 mib do sol2. |
    mib4 fa sol do2. |
    do'4 si do' sol2. |
    lab4 fa sol do2. |
    do4 re re mib2 mib4 |
    sib,4 do do re2 re4 |
    re4 mi fad sol2 sol4 |
    fa4 mib4. mib8 re2 sib,4 |
    do re4. re8 sol,2.\fermata |
    sol4 fad sol re2. |
    mib4 do re sol,2. |
    sol4 la la sib2 sib4 |
    re4 mi mi fa2 fa4 |
    la, si, si, do2 do4 |
    re mib fa sib,2. |
    sib4 la sib fa2. |
    sol4 mib fa sib,2. |
    R2.*9 |
    do'4 si do' sol2. |
    lab4 fa sol do1*3/4\fermata |
  }
>>
