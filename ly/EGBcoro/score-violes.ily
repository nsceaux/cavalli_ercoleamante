\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \keepWithTag #'coro1 \includeNotes "voix" \clef "alto"
    >> \keepWithTag #'coro1 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'coro2 \includeNotes "voix" \clef "alto"
    >> \keepWithTag #'coro2 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'coro3 \includeNotes "voix" \clef "alto"
    >> \keepWithTag #'coro3 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'coro4 \includeNotes "voix"
    >> \keepWithTag #'coro4 \includeLyrics "paroles"
    \new Staff << \global \includeNotes "basse" >>
  >>
  \layout { }
}
