\score {
  <<
    \new StaffGroup <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
      \original <<
        \new Staff << \global \includeNotes "partie1" >>
        \new Staff << \global \includeNotes "partie2" >>
        \new Staff << \global \keepWithTag #'basse \includeNotes "basse" >>
      >>
    >>
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'coro1 \includeNotes "voix"
      >> \keepWithTag #'coro1 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'coro2 \includeNotes "voix"
      >> \keepWithTag #'coro2 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'coro3 \includeNotes "voix"
      >> \keepWithTag #'coro3 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'coro4 \includeNotes "voix"
      >> \keepWithTag #'coro4 \includeLyrics "paroles"
    >>
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2.*12\pageBreak
        s2.*12\pageBreak
        s2.*12\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
