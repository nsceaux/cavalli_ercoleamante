\transpose do #(if (eqv? #t (ly:get-option 'pygmalion))
                   (ly:make-pitch -2 4)
                   (ly:make-pitch -1 0)) {
\clef "dessus" R2.*4 |
mib''4 sol'' do'' re''4. do''8 \ficta si'?4 |
mib'' fa''8 lab'' re''4 do''2. |
R2.*10 |
re''4 fad'' re'' re''8. mi''16 fad''!8 sol'' la''4 |
mib''4 mib'' la'' sol''2. |
sol''4 fa'' mib'' re''2 re''4 |
re''4 do'' sib' la'2 la'4 |
la'4 sol' fa' do''2 sol'4 |
sib' sib'4. la'8 sib'2. |
re''4 fa'' re'' fa''8. mi''16 fa''8 sol'' la''4 |
re''4 mib''8 sib' fa''4 re''2. |
R2.*9 |
mib''4 sol'' do'' re''8. mib''16 re''8 do'' si'4 |
mib''4 fa''8 lab'' re''4 do''1*3/4\fermata |
}
