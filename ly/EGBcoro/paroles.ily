\tag #'coro4 {
  Gra -- di -- sci ò Re,
  Gra -- di -- sci ò Re,
}
Gra -- di -- sci ò Re,
Gra -- di -- sci ò Re,
\tag #`(,(if (eqv? #t (ly:get-option 'pygmalion)) 'coro2 'coro1) coro3 coro4) {
  il cal -- do pian -- to
  ch’in mes -- to am -- man -- to
  af -- flit -- ta gen -- te
  dal cor do -- len -- te
  spar -- ge per te!
}
Gra -- di -- sci ò Re,
Gra -- di -- sci ò Re.

\tag #'coro4 {
  Tua se -- pol -- tu -- ra
  i fior ri -- ce -- va
  che sel -- va os -- cu -- ra
  ger -- mo -- gliar fe’:
}
Gra -- di -- sci ò Re,
Gra -- di -- sci ò Re.
\tag #(if (eqv? #t (ly:get-option 'pygmalion))
          '(coro2 coro3)
          '(coro1 coro2)) {
  e’l san -- gue be -- va,
  che per man mon -- da
  vac -- ca in -- fe -- con -- da
  sve -- na -- ta diè,
}
Gra -- di -- sci ò Re,
Gra -- di -- sci ò Re.
