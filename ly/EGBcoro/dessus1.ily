\transpose do #(if (eqv? #t (ly:get-option 'pygmalion))
                   (ly:make-pitch -2 4)
                   (ly:make-pitch -1 0)) {
\clef "dessus" R2.*4 |
sol''4 si'' sol'' sol''2. |
do'''4 do''' sol'' sol''2. |
R2.*10 |
sol''4 re'' sol'' la''8. sol''16 fa''8 mi'' re''4 |
sol'' sol'' re'' re''2. |
sib''4 la'' sol'' fa''2 fa''4 |
fa''4 mi'' re'' do''2 do''4 |
do'' re'' re'' mib''2 mib''4 |
fa''4 do''4. re''8 re''2. |
fa''4 la'' fa'' la''8. sol''16 la''8 sib'' do'''4 |
sol'' sol'' do''8 do''' sib''2. |
R2.*9 |
sol''4 si'' sol'' sol''8. la''16 sol''8 la'' si'' sol'' |
do'''4 do''' sol'' sol''1*3/4\fermata |
}
