<<
  \tag #'voix1 {
    \clef "vsoprano" R1.*2 |
    r2 do'' mi'' mi'' do'' mi'' |
    mi'' mi'' mi'' la' la' mi'' |
    la' re'' r |
    r2 r sol''4 sol'' |
    mi''2 do'' r |
    r r sol''4 sol'' |
    mi''2 do'' do'' |
    re'' re'' sol' |
    re'' re'' la' |
    re''4\melisma do'' re'' mi'' re'' mi''\melismaEnd do''1. |
    mi''2 mi'' la' re''1 re''2 |
    mi'' mi'' la' \blackNotation { re''2 sol'1 } |
    re''1. re'' |
    re''2 re'' si' |
    mi''1 do''2 do'' la' re'' |
    mi''1 fa''2 |
    re''1. |
    re''2 re'' mi'' do'' do'' re'' |
    si' si' mi'' re''1 la'2 mi''1. mi''\fermata |
    R1.*2 |
    r2 do'' mi'' mi'' do'' mi'' |
    mi'' mi'' mi'' la' la' mi'' |
    la' re'' r |
    r r sol''4 sol'' |
    mi''2 do'' r |
    r2 r sol''4 sol'' |
    mi''2 do'' do'' |
    re'' re'' sol' |
    re'' re'' la' |
    re''4( do'' re'' mi'' re'' mi'') do''1. |
  }
  \tag #'voix2 {
    \clef "vsoprano" R1.*2 |
    r2 sol' sol' do'' do'' do'' |
    do'' do'' do'' do'' do'' r4 sol' |
    re''2 la' do'' |
    si'4\melisma la' si' do'' re'' si' |
    do''2\melismaEnd do'' do'' |
    re''4\melisma sol'' fa'' mi'' re'' do''8[ si'] |
    do''2\melismaEnd sol' sol'4 sol' |
    sol'2 sol' do'' |
    la' la' do'' |
    sol'1. sol' |
    do''2 do'' re'' sol'1 sol'2 |
    do'' do'' re'' sol'1 do''2 |
    la'1. si' |
    si'2 si' re'' |
    do''1 mi''2 la' do'' si' |
    do''1 do''2 |
    si'1. |
    si'2 si' do'' la' la' si' |
    sol' sol' la' la'1 la'2 |
    la'1 sold'2( la'1.)\fermata |
    R1.*2 |
    r2 sol' sol' do'' do'' do'' |
    do'' do'' do'' do'' do'' r4 sol' |
    re''2 la' do'' |
    si'4\melisma la' si' do'' re'' si' |
    do''2\melismaEnd do'' do'' |
    re''4\melisma sol'' fa'' mi'' re'' do''8[ si'] |
    do''2\melismaEnd sol' sol'4 sol' |
    sol'2 sol' do'' |
    la' la' do'' |
    sol'1. sol' |
  }
  \tag #'voix3 {
    \clef "vhaute-contre" r2 mi' sol' mi' mi' sol' |
    mi' mi' mi' sol' sol' sol' |
    sol' sol' sol' la' la' sol' |
    fa' fa' mi' |
    re' re' sol' |
    sol'4\melisma la' sol' fa' mi' fa' |
    re'2\melismaEnd re' sol' |
    sol'4\melisma la' sol' fa' mi' fa' |
    re'2\melismaEnd re' sol' |
    fa' fa' mi' |
    re'1. mi' |
    sol'2 mi' re' re'1 re'2 |
    sol' mi' re' re'1 sol'2 |
    sol'1 fad'2( sol'1.) |
    sol'2 sol' sol' |
    sol'1 sol'2 fa' fa' fa' |
    mi'1 la'2 |
    sol'1. |
    sol'2 sol' sol' fa' fa' fa' |
    mi' mi' mi' fa'1 fa'2 |
    mi'1. mi'\fermata |
    r2 mi' sol' mi' mi' sol' |
    mi'2 mi' mi' sol' sol' sol' |
    sol' sol' sol' la' la' sol' |
    fa' fa' mi' |
    re' re' sol' |
    sol'4\melisma la' sol' fa' mi' fa' |
    re'2\melismaEnd re' sol' |
    sol'4\melisma la' sol' fa' mi' fa' |
    re'2\melismaEnd re' sol' |
    fa' fa' mi' |
    re'1. mi' |
  }
  \tag #'voix4 {
    \clef "vtenor" r2 do' mi' do' do' mi' |
    do' do' do' mi' mi' mi' |
    mi' mi' mi' fa' fa' do' |
    re' re' sol' |
    sol' sol' r4 re' |
    mi'4\melisma fa' mi' re' do' re' |
    si2\melismaEnd si re' |
    mi'4\melisma fa' mi' re' do' re' |
    si2\melismaEnd si mi' |
    re' re' do' |
    do'1 si2( do'1.) |
    mi'2 do' do' si1 si2 |
    mi' do' do' si1 mi'2 |
    re'1. re' |
    re'2 re' re' |
    mi'1 mi'2 do' do' re' |
    do'1 la2 |
    re'1. |
    re'2 re' do' do' do' si |
    si si la re'1 re'2 |
    si1. dod'\fermata |
    r2 do' mi' do' do' mi' |
    do'2 do' do' mi' mi' mi' |
    mi' mi' mi' fa' fa' do' |
    re' re' sol' |
    sol' sol' r4 re' |
    mi'\melisma fa' mi' re' do' re' |
    si2\melismaEnd si re' |
    mi'4\melisma fa' mi' re' do' re' |
    si2\melismaEnd si mi' |
    re' re' do' |
    do'1 si2( do'1.) |
  }
  \tag #'voix5 {
    \clef "vtenor" R1.*2 |
    r2 sol sol sol sol sol |
    do' do' do' do' fa sol |
    la la r |
    re'4 re' si2 sol |
    r2 r sol' |
    sol' re' r |
    r r sol4 sol |
    sol2 sol sol |
    la la la |
    \blackNotation { re2( sol1) } sol1. |
    do'2 sol la sol1 sol2 |
    sol2 sol la sol4\melisma la si2\melismaEnd sol |
    la1. sol |
    si2 sol si |
    do'1 sol2 la la r |
    la la2. do'4 |
    sol4\melisma fad sol la si2\melismaEnd |
    si2 si sol la la fa |
    sol sol do' la1 la2 |
    mi1. mi\fermata |
    R1.*2 |
    r2 sol sol sol sol sol |
    do' do' do' do' fa sol |
    la la r |
    re'4 re' si2 sol |
    r r sol' |
    sol'2 re' r |
    r r sol4 sol |
    sol2 sol sol |
    la la la |
    \blackNotation { re2( sol1) } sol1. |
  }
  \tag #'voix6 {
    \clef "vbasse" R1.*2 |
    r2 do do do do do |
    do do do fa fa mi |
    re re do |
    sol2.\melisma la4 si sol |
    do'2\melismaEnd do' do |
    sol2.\melisma la4 si sol |
    do'2\melismaEnd do' do4 do |
    sol2 sol mi |
    fa fa fa, |
    sol,1. do |
    do2 mi fa sol1 sol2 |
    do mi fa sol1 do2 |
    re1. sol, |
    sol2 sol sol |
    mi1 mi2 fa fa re |
    la1 fa2 |
    sol1. |
    sol2 sol mi fa fa re |
    mi mi do re1 re2 |
    mi1. la,\fermata |
    R1.*2 |
    r2 do do do do do |
    do do do fa fa mi |
    re re do |
    sol2.\melisma la4 si sol |
    do'2\melismaEnd do' do |
    sol2.\melisma la4 si sol |
    do'2\melismaEnd do' do4 do |
    sol2 sol mi |
    fa fa fa, |
    sol,1. do |
  }
>>
