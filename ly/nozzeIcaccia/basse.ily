\clef "basse" r2 do do do do do |
do1 do2 do do do |
do do do fa1 mi2 |
re re do |
sol1. |
do1 do2 |
sol1. |
do1 do2 |
sol sol mi |
fa fa fa, |
sol,1. do |
s1.*17 |
r2 do do do do do |
do1 do2 do do do |
do do do fa1 mi2 |
re re do |
sol1. |
do1 do2 |
sol1. |
do1 do2 |
sol sol mi |
fa fa fa, |
sol,1. do |
