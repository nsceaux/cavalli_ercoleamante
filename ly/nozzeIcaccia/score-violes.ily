\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \keepWithTag #'voix3 \includeNotes "voix" \clef "alto"
    >> \keepWithTag #'voix3 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'voix4 \includeNotes "voix" \clef "alto"
    >> \keepWithTag #'voix4 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'voix5 \includeNotes "voix" \clef "alto"
    >> \keepWithTag #'voix5 \includeLyrics "paroles"
  >>
  \layout { }
}
