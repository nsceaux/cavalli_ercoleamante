\tag #'voix1 {
  Al -- la cac -- cia al -- la cac -- cia al -- la pre -- da,
  al suo -- no
  al -- la pre -- da
  al -- la pre -- da
  al suo -- no
  al suo -- no
  al cor -- so.
}
\tag #'voix2 {
  Al -- la cac -- cia al -- la cac -- cia al -- la pre -- da,
  al suo -- no
  al cor -- so
  al cor -- so
  al -- la pre -- da,
  al suo -- no,
  al cor -- so.
}
\tag #'voix3 {
  Al -- la cac -- cia al -- la cac -- cia al -- la cac -- cia al -- la cac -- cia al -- la pre -- da,
  al suo -- no
  al cor -- so
  al cor -- so
  al cor -- so,
  al suo -- no,
  al cor -- so.
}
\tag #'voix4 {
  Al -- la cac -- cia al -- la cac -- cia al -- la cac -- cia al -- la cac -- cia al -- la pre -- da,
  al suo -- no
  al cor -- so
  al cor -- so
  al cor -- so,
  al suo -- no,
  al cor -- so. __
}
\tag #'voix5 {
  Al -- la cac -- cia al -- la cac -- cia al -- la pre -- da,
  al suo -- no
  al -- la pre -- da
  al suo -- no
  al -- la pre -- da,
  al suo -- no,
  al cor -- so.
}
\tag #'voix6 {
  Al -- la cac -- cia al -- la cac -- cia al -- la pre -- da,
  al suo -- no
  al cor -- so
  al cor -- so
  al -- la pre -- da,
  al suo -- no,
  al cor -- so.
}

Sù cac -- cia -- tri -- ce
sù cac -- cia -- tri -- ce schie -- ra
las -- si per que -- ste sel -- ve or -- ri -- bil fe -- rà
è le zam -- pe, e le zan -- ne, e’l cef -- fo, e’l dor -- so.

\tag #'voix1 {
  Al -- la cac -- cia al -- la cac -- cia al -- la pre -- da,
  al suo -- no
  al -- la pre -- da
  al -- la pre -- da
  al suo -- no
  al suo -- no
  al cor -- so.
}
\tag #'voix2 {
  Al -- la cac -- cia al -- la cac -- cia al -- la pre -- da,
  al suo -- no,
  al cor -- so
  al cor -- so
  al -- la pre -- da,
  al suo -- no,
  al cor -- so.
}
\tag #'voix3 {
  Al -- la cac -- cia al -- la cac -- cia al -- la cac -- cia al -- la cac -- cia al -- la pre -- da,
  al suo -- no,
  al cor -- so
  al cor -- so
  al cor -- so,
  al suo -- no,
  al cor -- so.
}
\tag #'voix4 {
  Al -- la cac -- cia al -- la cac -- cia al -- la cac -- cia al -- la cac -- cia al -- la pre -- da,
  al suo -- no,
  al cor -- so
  al cor -- so
  al cor -- so,
  al suo -- no,
  al cor -- so. __
}
\tag #'voix5 {
  Al -- la cac -- cia al -- la cac -- cia al -- la pre -- da,
  al suo -- no
  al -- la pre -- da
  al suo -- no
  al -- la pre -- da,
  al suo -- no,
  al cor -- so.
}
\tag #'voix6 {
  Al -- la cac -- cia al -- la cac -- cia al -- la pre -- da,
  al suo -- no
  al cor -- so
  al cor -- so
  al -- la pre -- da,
  al suo -- no,
  al cor -- so.
}
