\key do \major \midiTempo#300
\time 3/2 \measure 6/2 s1.*6
\measure 3/2 s1.*7
\measure 6/2 s1.*2 \bar ":|."
s1.*6
\measure 3/2 s1.
\measure 6/2 s1.*2
\measure 3/2 s1.*2
\measure 6/2 s1.*12
\measure 3/2 s1.*7
\measure 6/2 s1.*2 \bar "|."
