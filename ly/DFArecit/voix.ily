<<
  \tag #'(voix1 basse) {
    \ffclef "vbas-dessus" <>^\markup\character Iole
    r4 la' la' la' |
    la' la'8 la' mi''4 mi'' |
    r8 mi' mi' mi' mi'4 mi' |
    r8 mi' mi' mi' mi'4 mi'8 re' |
    mi'4 mi' mi' mi'8 mi' |
    si'2 mi''8 re''16 re'' re''8 mi'' |
    do''4 do''8 do'' do'' do'' do'' do'' |
    la' la' la' la' la'4 la'8 la' |
    fa'4 fa'8 re' la'8 la' la' la' |
    la' la' la' si' do''4 do'' |
    mi'4. mi'8 mi'4 la'8 si' |
    sold'4 sold' r8 mi' la' la' |
    mi'4 mi' r2 |
    r4 re''8 si' re'' sol'16 sol' sol'8 sol' |
    sol' sol' sol' sol' sol' sol'16 sol' sol'8 la' |
    la'4 la'8 re'' la' la' la' la' |
    re''4 re''8 re'' si'4 si'8 si' |
    si' si' si' si' sol' sol' <<
      \tag #'basse { s4 s2. \ffclef "vbas-dessus" <>^\markup\character Iole }
      \tag #'voix1 { r4 | r2 r4 }
    >> mi''4 |
    do''8 do'' <<
      \tag #'basse { s2 \ffclef "vbas-dessus" <>^\markup\character Iole }
      \tag #'voix1 { r4 r }
    >> fa''8 fa''16 mi'' |
    re''8 re'' r4 \original <<
      \tag #'basse { s2 s1*2 s2. \ffclef "vbas-dessus" }
      \tag #'voix1 { r2 | R1*2 | r2 r4 }
    >>
    \pygmalion <<
      \tag #'basse { s2 s1*3 }
      \tag #'voix1 { r2 | R1*3 }
    >>
    \original {
      <>^\markup\character Iole mi''4 |
      do'' do'' sol'8 sol' sol' sol'16 sol' |
      sol'8 sol' sol' sol' sol'4 sol'8 sol' |
      sol'8 sol' sol' la' fa' fa' do'' la' |
      do''8 do''16 do'' la'8 la' fa' fa' fa' mi' |
      sol' sol' sol' sol' sol'4 sol'8 la' |
      si'4 si' si'8 si' si' si'16 do'' |
      re''4 re'' mi'' do''8 si' |
      la'2 sol' |
    }
  }
  \tag #'(voix2 basse) {
    <<
      \tag #'basse { s1*17 s2. \ffclef "vtenor" }
      \tag #'voix2 { \ffclef "vtenor" R1*17 r2 r4 }
    >> <>^\markup\character Hyllo
    r8 re' |
    si re' si si re' sol <<
      \tag #'basse { s2 \ffclef "vtenor" <>^\markup\character Hyllo }
      \tag #'voix2 { r4 | r }
    >> mi'4 la <<
      \tag #'basse { s4 s2 \ffclef "vtenor" <>^\markup\character Hyllo }
      \tag #'voix2 { la4 | r2 }
    >> r4 sol' |
    r8 mi'16 mi' mi'8 mi'16 mi' do'8 do'16 do' do'8 re' |
    re'4 re'8 re' mi' si do'4~ |
    do'4. si8( <<
      \tag #'basse { do'4) }
      \tag #'voix2 { do'2) | \original R1*8 }
    >>
  }
>>
\tag #`(,(if (eqv? #t (ly:get-option 'pygmalion)) 'voix2 'voix1) basse) {
  \original { \ffclef "vtenor" <>^\markup\character Hyllo }
  r8 si si re' si2 |
  si8 si16 la la8 sold sold4 sold |
  r8 do' sold la la4. sold8( |
  la2)
  \ffclef "vbas-dessus" <>^\markup\character Iole
  do''8 do'' do'' do''16 si' |
  do''4 do''8 do'' do'' do'' do''4 |
  do'' do''8 re'' mi''4 mi''8 mi'' |
  la'4 la' re''8 re'' re'' re''16 mi'' |
  do''4 do'' do''8 do''16 do'' do''8 si' |
  re''4 re'' r2 |
}
