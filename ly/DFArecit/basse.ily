\clef "basse" la,1~ |
la, |
sold, |
sold,~ |
sold,~ |
sold, |
<<
  \original { la,1 | fa, | }
  \pygmalion { la,2 sol, | fa, mi, | }
>>
re,1~ |
re,2 la,~ |
la,4 sold, la, fa, |
mi,4. re,8 do,4 re, |
mi,2 la, |
sol,1 |
sol, |
fad,~ |
fad,2 sol,~ |
sol,4 do re2 |
sol,1 |
mi,2 fa,~ |
fa, sol,~ |
sol, la, |
fa, sol,4 fa, |
sol,2 do |
\original {
  do1 |
  do~ |
  do2 fa, |
  fa,1 |
  do |
  sol,~ |
  sol,2 do |
  re sol, |
}
sol,1 |
sol,4 fa, << \original mi,2 \pygmalion { mi,4 re, } >> |
do,4 re, mi,2 |
la,~ la, |
la,1~ |
la,2 sol, |
fa,1 |
mi,2 la, |
sol,1 |
