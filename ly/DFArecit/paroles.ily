\tag #'(voix1 basse) {
  D’Eu -- ty -- ro a -- ni -- ma gran -- de
  a ques -- to co -- re, a ques -- to brac -- cio im -- bel -- le
  tan -- to vi -- gor, tan -- to fu -- ror com -- par -- ti
  che pos -- sa or qui sa -- crar -- ti,
  con in -- si -- gne ven -- det -- ta
  u -- ni -- ver -- sal di cui de -- sio rim -- bom -- ba
  vit -- ti -- ma sì dov -- u -- ta a la tua tom -- ba.
  Pren -- di pren -- di o mio ge -- ni -- tor da l’ar -- so li -- do
  di Fle -- ge -- ton -- te, il san -- gue
  di quest’ em -- pio ti -- ran -- no,
  che nel tuo no -- me uc -- ci -- do.
}
\tag #'(voix2 basse) {
  Ohi -- mè, che fa -- i?
  Ces -- sa.
}
\tag #'(voix1 basse) {
  Deh las -- sia.
}
\tag #'voix2 {
  Ah ces -- sa.
}
\tag #'basse {
  Ah ces -
}
\tag #'(voix1 basse) {
  Las -- sia se m’a -- mi.
}
\tag #'(voix2 basse) {
  Ah che del pa -- ri io so -- no
  tuo ve -- ro a -- man -- te, e di lui fi -- glio.
}
\original\tag #'(voix1 basse) {
  Ah sen -- ti:
  io non l’o -- dio già più com’ uc -- ci -- so -- re
  del ca -- ro pa -- dre mi -- o sen -- ti sen -- ti che di -- co
  che co -- me av -- ver -- so al co -- mun nos -- tro ar -- do -- re
  on -- de tuo più che pa -- dre e -- gli è ne -- mi -- co.
}
\tag #`(,(if (eqv? #t (ly:get-option 'pygmalion)) 'voix2 'voix1) basse) {
  Lo pla -- che -- rò, quan -- do non bas -- ti il pian -- to,
  con la mia mor -- te. __
  
  E sì po -- co è gra -- di -- ta
  la spe -- me a te d’es -- ser mio spo -- so oh Di -- o
  che per es -- sa non pre -- gi
  pun -- to di più la vi -- ta?
}
