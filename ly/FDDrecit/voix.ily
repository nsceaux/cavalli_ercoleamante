\clef "valto" <>^\markup\character Licco
r4 fad'8 la' fad'4 fad'8 mi' |
fad'4 fad' fad'8 fad' fad' mi'16 fad' |
re'8 re'16 dod' re'8 mi' si2 la1 |
r4 la mi' mi'8 fad' |
re'2 la'4 |
fad' re' mi'8 fad' |
sol'4 sol' mi'8 re' |
dod'4 re' mi' |
la2 la'4 |
sol'2 fad' mi' re'1*3/4\fermata |
