\clef "basse" re1~ |
re2 re4. mi8 |
fad4 re mi2 la,1 |
la, |
re2. |
re'4 si do'8 re' |
sol4 la8 si do'4 |
la8 sol fad4 sol |
la re fad |
sol4. la8 si4 sol la2 re1*3/4\fermata |
