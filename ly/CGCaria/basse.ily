\clef "basse" fa4. sol8 la4 |
sib2 la4 |
sib la sol |
fa2 re4 |
do2 fa4 |
sol2. mib4 re2 |
do fa4 |
sol2. |
mi |
fa2 sol4 lab2. |
sol |
do' |
sib lab sol |
fa |
sol4 mib fa |
sol2. do |
fa re |
mib2 fa4 sib,2. |
sib2 la4 sol2. |
fa |
fa2 mi4 |
re2. |
do2. la,4 sib, do |
re mi2 |
fa4 sol la |
sib la2 |
sib sib,4 |
do2. fa,1*3/4\fermata |
