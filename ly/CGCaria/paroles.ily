Dell’ a -- mo -- ro -- se pe -- ne
sos -- pi -- ra -- to ris -- to -- ro,
vi -- tal dol -- ce te -- so -- ro,
ch’il mon -- do più che Ce -- re -- re man -- tie -- ne
dal ne -- ghit -- to -- so spe -- co
sof -- fri sof -- fri di ve -- nir me -- co, __
ch’A -- mo -- re og -- gi og -- gi dis -- po -- ne
con -- tro l’em -- pia in -- so -- len -- za
di stra -- nie -- ra po -- ten -- za
del -- la sua li -- ber -- tà __ far -- ti cam -- pio -- ne.
