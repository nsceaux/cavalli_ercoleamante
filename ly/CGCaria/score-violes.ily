\score {
  \new ChoirStaff <<
    \new Staff << \global \includeNotes "viole1" >>
    \new Staff \with { \tinyStaff \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'basse \includeNotes "voix"
    >> \keepWithTag #'basse { \set fontSize = #-2 \includeLyrics "paroles" }
    \new Staff << \global \includeNotes "basse" >>
  >>
  \layout { }
}
