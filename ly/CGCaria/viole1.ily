\clef "viole1" R2.*7 |
r4 sol la |
si do' re' |
r sol'4. do'8 |
do'4 re' si r8 mib' re'4. do'8 |
do'2 si4 |
r r r8 sol' |
sol'4 re'4. re'8 mib'4 do'4. si8 si2. |
r4 lab'4. sol'8 |
sol'4. fa'8 mib' re' |
do' la si4 do' do'2. |
r2. r4 r fa |
sol4 do'4. la8 sib4 re'4. mi'8 |
fa'2.~ fa'4 mi'2 |
r4 do' re' |
la do' sol |
la si4. do'8 |
do'2. r8 do' re'4 mi' |
fa' do'2 |
r4 re' do' |
fa'2.~ |
fa'4 sol'4. la'8 |
fa'2 mi'4 fa'1*3/4\fermata |
