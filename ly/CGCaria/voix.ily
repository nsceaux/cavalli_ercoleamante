\clef "vsoprano" <>^\markup\character Giunone
fa''4 fa'' do'' |
re'' do''2 |
re''4 do''4.( sib'8 |
do''4) do''4. re''8 |
mib''4. re''8 mib''4 |
mib''2 re''4 do'' re''2 |
mib''4. mib''8 re''4 |
mib''2 re''4 |
sol' do'' sol' |
lab'2 sol'4 do''4. do''8 si' do'' |
re''2 re''4 |
do'' re'' mib'' |
re'' sib'( sol') do'' mib'4.( fa'8 sol'4) re''4. mib''8 |
fa''4 do'' re'' |
si' sol'( do'') |
mib'' re''4.( mib''8 do''2) sol'4 |
la'2 la'4 sib'2 sib'4 |
do''4. re''8 mib''4 re''2 re''4 |
r re'' fa'' sib'4. sib'8 sib'4 |
fa''2 fa''4 |
r la' do'' |
fa'4. fa'8 fa'4 |
do''2 do''4 fa' sol' la' |
sib' sol'( do'') |
la'4\melisma sib'4. do''8 |
re''4 do''2\melismaEnd |
re''4. mi''8 fa''4 |
la'4( sol'2) fa'1*3/4\fermata |
