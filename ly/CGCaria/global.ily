\key fa \major \midiTempo#160
\digitTime\time 3/4 s2.*5
\measure 6/4 s1.
\measure 3/4 s2.*3
\measure 6/4 s1.
\measure 3/4 s2.*2
\measure 9/4 s2.*3
\measure 3/4 s2.*2
\measure 6/4 s1.*4
\measure 3/4 s2.*3
\measure 6/4 s1.
\measure 3/4 s2.*4
\measure 6/4 s1. \bar "|."

