\score {
  \new ChoirStaff <<
    \pygmalion\new Staff << \global \includeNotes "viole1" >>
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2.*8\break s2.*10\pageBreak
        s2.*11\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
