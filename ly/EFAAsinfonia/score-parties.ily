\score {
  \new StaffGroup <<
    \new Staff << \global \includeNotes "dessus2" >>
    \new Staff << \global \includeNotes "partie1" >>
  >>
  \layout { system-count = 1 }
}
