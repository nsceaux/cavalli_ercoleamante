\transpose do' fa {
\clef "basse" do1 |
fa, do\breve |
sol1 do |
sol\breve |
mib2 fa sol do |
sol\breve do\breve\fermata |
}
