\transpose do' fa {
\clef "petrucci-g/alto" mib'1 |
fa'1. mi'4 re' mi'1\fermata |
si'1 do''~ |
do''2 si'4 la' si'!1\fermata |
mib''2 re'' si' do''~ |
do'' si'4 la' si'!1 do''\breve\fermata |
}
