\piecePartSpecs
#`((dessus #:score-template "score" #:notes "dessus1" #:system-count 1)
   (parties #:score "score-parties")
   (violes #:score "score-violes")
   (basse #:score-template "score-basse-continue" #:system-count 1)
   (silence #:on-the-fly-markup , #{ \markup\tacet #}))
