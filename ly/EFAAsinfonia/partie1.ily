\transpose do' fa {
\clef "viole1" do'1 |
do' do'\breve\fermata |
sol'1 sol' |
sol'\breve\fermata |
sol'2 lab' sol' sol' |
sol'\breve sol'\fermata |
}
