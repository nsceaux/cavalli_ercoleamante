\transpose do' fa {
\clef "dessus" sol'1 |
lab' sol'\breve\fermata |
re''1 mib'' |
re''\breve\fermata |
sol''2 fa'' re'' mib'' |
re''\breve mi''\breve\fermata |
}
