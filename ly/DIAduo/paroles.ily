\tag #'(deianira basse) {
  Fi -- glio fi -- glio tu pri -- gio -- nie -- ro?
}
\tag #'hyllo {
  Ma -- dre ma -- dre tu tu dis -- cac -- cia -- ta?
}
\tag #'basse { tu tu dis -- cac -- cia - }
\tag #'(deianira basse) {
  E vi -- ve in sen di pa -- dre un cor sì fie -- ro?
}
\tag #'hyllo {
  Et in cor di ma -- ri -- to al -- ma s’in -- gra -- ta.
}
\tag #'basse {
  - ri -- to al -- ma s’in -- gra -
}
\tag #'(deianira basse) {
  Fi -- glio fi -- glio fi -- glio fi -- glio tu pri -- gio -- nie -- ro?
}
\tag #'hyllo {
  Ma -- dre ma -- dre tu tu tu dis -- cac -- cia -- ta?
}
\tag #'basse { tu tu tu dis -- cac -- cia - }
\tag #'(deianira basse) {
  Non fos -- se a te cru -- de -- le,
  e gli per -- do -- ne -- rei
  \tag #'deianira { l’in -- fe -- del -- tà. }
  \tag #'basse { l’in -- fe - }
}
\tag #'(hyllo basse) {
  Non fos -- se a te in -- fe -- de -- le,
  e lie -- ve tro -- ve -- rei sua cru -- del -- tà.
}
\tag #'(deianira basse) {
  S’a te pie -- tà non spe -- ro
  o -- gni sor -- te a me fi -- a
  sem -- pre sem -- pre sem -- pre spie -- ta -- ta.
  
  Fi -- glio fi -- glio tu pri -- gio -- nie -- ro?
}
\tag #'hyllo {
  S’a te pie -- tà non spe -- ro
  o -- gni sor -- te a me fi -- a
  sem -- pre sem -- pre spie -- ta -- ta.
  
  Ma -- dre ma -- dre tu tu tu dis -- cac -- cia -- ta?
}
\tag #'basse {
  tu tu tu dis -- cac -- cia -- ta?
}
\tag #'(deianira basse) { Fi -- glio… }
\tag #'(hyllo basse) { Ma -- dre… }
\tag #'(deianira basse) { Fi -- glio… }
\tag #'(hyllo basse) { Ma -- dre… }
\tag #'(deianira basse) {
  O -- gn’or des -- ti
  a me dell’ a -- mor tu -- o se -- gni se -- gni più es -- pres -- si, __
  ah vo -- glia il ciel, che ques -- ti
  non sian gli ul -- ti -- mi
  non sian gli ul -- ti -- mi gli ul -- ti -- mi am -- ples -- si. __
}
\tag #'hyllo {
  O -- gn’or des -- ti
  a me dell’ a -- mor tu -- o se -- gni più es -- pres -- si,
  ah vo -- glia il ciel, che ques -- ti
  non sian gli ul -- ti -- mi
  non sian gli ul -- ti -- mi am -- ples -- si. __
}
