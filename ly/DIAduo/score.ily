\score {
  \new StaffGroupNoBar \with { \haraKiriFirst } <<
    \pygmalion\new StaffGroupNoBracket <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
    >>
    \pygmalion\new StaffGroupNoBracket <<
      \new Staff << \global \includeNotes "viole0" >>
      \new Staff << \global \includeNotes "viole1" >>
      \new Staff << \global \includeNotes "viole2" >>
    >>
    \new Staff \withLyrics <<
      \global \keepWithTag #'deianira \includeNotes "voix"
    >> \keepWithTag #'deianira \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'hyllo \includeNotes "voix"
    >> \keepWithTag #'hyllo \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \original\origLayout {
        s2.*7\break s2.*11\break s2.*10\pageBreak
        s2.*9\break s2.*9\break s2.*11\pageBreak
        s2.*12\break s2.*13\break
      }
      \pygmalion\modVersion {
        s2.*24\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
