\score {
  <<
    \new Staff \with { \tinyStaff \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'basse \includeNotes "voix"
    >> \keepWithTag #'basse { \set fontSize = #-2 \includeLyrics "paroles" }
    \new StaffGroup \with { \haraKiriFirst } <<
      \new Staff << \global \includeNotes "viole1" >>
      \new Staff <<
        \global \includeNotes "viole2"
        { s2.*24\break }
      >>
    >>
  >>
  \layout {
    system-count = #(*system-count*)
    indent = #(if (*instrument-name*)
                  largeindent
                  (or (*score-indent*) smallindent))
  }
}
