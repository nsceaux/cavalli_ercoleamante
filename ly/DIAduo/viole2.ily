\clef "viole2" R2.*28 |
r4 re' fa' do'2 fa'4 |
sib2 sib4 la do'4. re'8 |
re'2 re'4 |
do'2 la4 |
sib2 sol4 |
la2 la4 sib sol2 |
la2. |
sol2 do'4 |
fa sib2~ |
sib4 la sol8 la |
sol2. |
sol2 sib4 la2 la4 |
re'2 mi'4 do'2. |
r4 r mi' fa'2 do'4 |
fa' sib4. do'8 do'2 fa'4 |
sib2. |
fa'4 do'2 re'4 sib2 |
do'4 la2 |
sib2. r4 fa'4. mi'8 re'4 mi'2 |
fa'2. r4 r re' fa'2 do'4 |
re'2 sol'4 do'2. |
re'4 sib mib' la fa' re'~ |
re' sib sol do'2. |
R2.*2 |
r2*3/2 r4 do' la |
re'2. do'2 re'4 |
sib2.~ |
sib4 la2 r4 re' sib |
fa'2.~ fa'4 re' sol' |
do'2 la4 re'2 mi'4 do'2 fa'4 |
mib'2. r4 re' la |
do'2 sol4 r sib fa |
do'2. |
re'2 re4 sol2. |
fa |
R1. |
r4 mib' do' do' re'2 |
re' sib4 do'2. |
r4 sib do' |
do' la2 re'2. |
r4 r do' |
re'2. fa'2 do'4 |
re' sib2~ sib4 do' fa' |
mib'2 sib4 do'2. |
r4 sol' sib' fa' la' fa' |
re'2 mi'4 do' fa'2 |
re' sib4 re' fa' do' |
r mib' sib~ sib fa fa' |
fa'2. re'4 la do' |
sib2 sib4 re' fa'2 |
r4 r sol' do'2. |
sib4 mib'2 la4 re'2 |
sol4 do' la fa sib2 |
fa2. fa |
