\clef "viole0" R2.*33 |
r4 do' fa' |
re'4 mi'4. fa'8 |
fa'2. sol'4 mi'( fa') |
fa'2. |
sol'4. sol'8 sol' la' |
sib'4 fa'4. sol'8 |
sol'4 la'2 |
sol'2 re'4 |
mi'2 mi'4 fa'2 la'4 |
sib'2 sol'4 la'2 fa'4 |
sol'2. r4 r fa' |
re'2 mi'4 fa' la'4. la'8 |
sib'2 sib'4 |
do''2. sib' |
r4 do''4. re''8 |
sib'4 sol'2 r4 la'4. do''8 sib'2 sol'4 |
la'4 fa'2 r2*3/2 r4 la'2 |
sib'2. do''2 la'4 |
sib'2 sol'4 la'2 fa'4 |
sol'2 do''4~ do''2 fa'4 |
R2. | \allowPageTurn
do'2. re'4. do'8 sib4 la2. |
R1. |
r4 sib'4. sol'8 |
do''2 fa'4 fa'2 sib'4 |
do'' la' re'' sib' sol' do'' |
la'2. |
R1. |
r4 sib' sol' la' fa'2 |
sol'4 mib'2 fa'4 re'2 |
sol'2.~ |
sol'4. mi'8 fa'4 do''2. |
la'2 la'4 |
sol' r mi' fa'2 sol'8 la' |
sib'2. r2*3/2 |
fa'2 sol'4 la'8 sib' do''4. re''8 |
sib'4 sol'2 |
la'4 fa'2 r2*3/2 |
r4 la' fa' |
sib' sol'2 r4 fa'2~ |
fa'2.~ fa' |
sib'2.~ sib'2 la'4 |
sib'2 sol'4 do''2. |
re''4 sib'2 do''4 la'2 |
sib'2. fa'4 la'2 |
re'4 sol' do' re' fa'2 |
r4 sib'4. sib'8 fa'4 la'4. sib'8 |
sib'2 sib'4 fa''2. |
r4 sib' sib' do''2. |
re''4 sib' sib' do'' la' la' |
sib' sol'2 la'4 fa' sol' |
fa'2. fa' |
