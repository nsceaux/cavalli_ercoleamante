s2. <6> s2.*2
s2. <6> s2.*2
s2. <6>2. <7>4 <6>2 s2.
\ru#5 s2.*4
s2. <6> <7>4 <6>2 s2.*6 <4>4 <4> <3> s2.*2
s2.*4
s2. <6> s2.*2
<6>2. s2.*5
s2.*4
s2.*3 \pygmalion s2.*8 <4>4 <3>2
\ru#2 s2.*4
s2.*6
<6>2. <6> s2.*2
\ru#2 s2.*4
\original { s2.*3 <4>4 <3>2 }
