\clef "dessus" R1.*2 R2.*3 R2. R1.*2 R2.*2 R1.*5 R2.*2 R1.*3 R2.*3
R1. R2.*5 R1.*4 R2. R1. R2. R2.*3 |
r2*3/2 r r4 fa'' la'' |
re''2. r4 do'' fa'' |
re''2 mib''4 do''2 re''4 |
sib'2.~ sib'4 la'2 |
R2.*34 |
re''2.~ re''4 do'' do'' |
mib''2. r4 do'' do'' |
re'' mi''! mi'' fa''2 la''4 |
sib''2 sol''4~ sol'' fa''4. do''8 |
re''2 mib''4 re'' do''4. sib'8 |
sib'2 do''4 sib'2 la'4 |
sib'4 re''4. re''8 re''2 do''4 |
mib''4 re''4. re''8 re''2 do''4 |
re'' mi''4. fa''8 fa''2.~ |
fa''4 mib''4. mib''8 mib''4 re''4. re''8 |
re''4 do''4. do''8 do''4 sib'4. sib'8 |
sib'2 la'4 sib'2. |
