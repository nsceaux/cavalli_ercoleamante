\clef "viole1" R2.*24 |
r4 re' fa' |
do'2 la4 |
sib2 sol4 la fa fa' |
fa'2.~ fa'2 do'4 |
re' sol'2 do'4 fa'2 |
fa' sol'4 |
sol' fa' r |
r4 r sol' |
do'2 fa'4 re' do'2 |
do'4. do'8 do' re' |
mib'2 mib'4 |
sib re'2 |
mib'2 do'4 |
do'2 si4 |
do'2.~ do' |
r2*3/2 r4 r do' |
re'2 sib4 do'2 la4 |
sib2. r4 do'4. do'8 |
re'4 mi' fa' |
do' fa'2 re'2. |
r4 fa'2 |
re'2 sib4 do'2 do'4 sol2 do'4 |
do'2 la4 re'2. r4 do' fa' |
sib2. fa'~ |
fa'4 re' sib do' la2 |
sib2. fa' |
R2. |
fa'2. |
sib fa'~ |
fa'~ fa' |
R2. |
r4 do' la re'2. |
do'2 fa'4 re'2 mi'4 |
fa'2.~ |
fa'2 sib4 fa'2 la'4 |
sib' sib2 re'4 la2 |
r4 sol mib sib2. |
mi!2 la4 |
fa sib2 r4 do'2 |
do'2 do'4 |
re'2 sol'4 do'2. |
R1. |
r2*3/2 r4 la'4. la'8 |
re'2 sol'4 |
fa'2. r4 re' sol' |
do'4. re'8 la4 |
sib2.~ sib4 do'2 |
r4 re'2 r4 r do' |
sol'2. r4 do' fa' |
re' sib2 do' do'4 |
sib2 sib4 fa'2.~ |
fa'4 re' sol' r do' la |
sib2 mib'4 fa' do'4. re'8 |
re'2. la2 r4 |
r sib re' fa'2 do'4 |
fa' sib sib fa'2 fa'4 |
re' sol'4. sol'8 do'4 fa'2 |
sib4 mib'4. mib'8 la4 re' do' |
re'8 mib' fa'4 do' re'2. |
