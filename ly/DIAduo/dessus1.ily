\clef "dessus" R1.*2 R2.*3 R2. R1.*2 R2.*2 R1.*5 R2.*2 R1.*3 R2.*3
R1. R2.*5 R1.*4 R2. R1. R2. R2.*3 |
r2*3/2 r4 re'' fa'' do''2. |
r4 sol'' sib''~ sib'' la''2 |
r4 sol''2~ sol''4 do'' fa''~ |
fa'' mi''4. fa''8 fa''2. |
R2.*32 |
r2*3/2 fa''2.~ |
fa''4 re'' re'' fa''2. |
r4 sib' sib' fa''2. |
r4 sol'' sol'' la'' fa'' fa'' |
sol'' sol'' sib'' la''2 la''4~ |
la'' sol''4. fa''8 fa''2 sol''4 |
fa''4 mib''4. re''8 re''4 do''4. fa''8 |
re''2 fa''4 fa'' fad''4. sol''8 |
sol''2 sib''4 sib'' la''4. la''8 |
la''2 sol''4 la''2 sib''4 |
sib''2 sib''4 la''2 la''4 |
sol''2 sol''4 fa''2 mib''8 re'' |
re''4 do''4. sib'8 sib'2. |
