\key fa \major \midiTempo#160
\digitTime\time 3/4
\measure 6/4 s1.*2
\measure 9/4 s2.*3
\measure 3/4 s2.
\measure 6/4 s1.*2
\measure 3/4 s2.*2
\measure 6/4 s1.*5
\measure 3/4 s2.*2
\measure 6/4 s1.*3
\measure 3/4 s2.*3
\measure 6/4 s1.
\measure 3/4 s2.*5
\measure 6/4 s1.*4
\measure 3/4 s2.
\measure 6/4 s1.
\measure 3/4 s2.
\measure 9/4 s2.*3
<<
  \original { \measure 6/4 s1. }
  \pygmalion { s2.*3 \measure 6/4 s1.*3 \measure 3/4 s2. }
>>
\measure 3/4 s2.
\measure 6/4 s1.*2
\measure 3/4 s2.
\measure 6/4 s1.*2
\measure 3/4 s2.
\measure 6/4 s1.*3
\measure 3/4 s2.
\measure 6/4 s1.
\measure 3/4 s2.
\measure 6/4 s1.*3
\measure 3/4 s2.
\measure 6/4 s1.
\measure 3/4 s2.
<<
  \original { \measure 6/4 s1.*3 }
  \pygmalion { \measure 6/4 s1.*13 }
>>
\bar "|."

