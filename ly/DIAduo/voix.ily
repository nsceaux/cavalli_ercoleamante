<<
  %% Deianira
  \tag #'(deianira basse) {
    \clef "vbas-dessus" <>^\markup\character Deianira
    R2.*7 |
    r4 do'' la' |
    re'' re'' r fa'' do'' re'' |
    sib'\melisma mi'( fa')\melismaEnd fa'2. |
    <<
      \tag #'basse { s2.*2 s2 \ffclef "vbas-dessus" <>^\markup\character Deianira }
      \tag #'deianira { R2.*2 r4 r }
    >> mi''4 fa''2 fa''4 |
    re'' do''4. sib'8 do''2 do''4 |
    re''4 sol'4. fa'8 la'2 la'4 |
    <<
      \tag #'basse { s1. s2 \ffclef "vbas-dessus" <>^\markup\character Deianira }
      \tag #'deianira { R1. | r4 r }
    >> sib'8 sol' do''4 do'' do''8 sib' |
    re''4 re'' r |
    fa'' do'' re'' |
    sib'\melisma mi'( fa')\melismaEnd fa'2. |
    <<
      \tag #'basse { s2.*3 s2 \ffclef "vbas-dessus" <>^\markup\character Deianira }
      \tag #'deianira { R2.*2 | r2*3/2 r4 r }
    >> fa''4 |
    re'' re''8 re'' mi'' fa'' |
    do''4 do'' r |
    sib'4 sib'8 sib' sib' la' |
    la'2. re''4 <<
      \tag #'basse {
        sol'4 s s2.*5 s2
        \ffclef "vbas-dessus" <>^\markup\character Deianira
      }
      \tag #'deianira {
        sol'4. fa'8 |
        fa'2. |
        R2.*4 |
        r4 r
      }
    >> sol'4 la'2 fa'4 |
    sol'2 mi'4 fa'2.~ |
    fa'4 mi'2 r4 do'' do'' |
    re'' re'' sol' do''2 do''4 |
    r sib'2~ |
    sib'4 la'2 r4 sol'2~ |
    sol'4 fa'2 |
    r4 sib'4.\melisma do''8 la'2\melismaEnd la'4 sol' sol'2 |
    <<
      \original { fa'4 }
      \pygmalion { fa'2. r2*3/2 r | R1.*2 | r2*3/2 r4 }
    >> do''4. la'8 re''2 re''4 |
    fa''4 do'' re'' |
    sib'\melisma mi'( fa')\melismaEnd fa'2. |
    <<
      \tag #'basse { s2.*4 s4 \ffclef "vbas-dessus" <>^\markup\character Deianira }
      \tag #'deianira { R2.*3 | r2*3/2 r4 }
    >> sib'4 sol' |
    <<
      \tag #'basse { s2. \ffclef "vbas-dessus" <>^\markup\character Deianira }
      \tag #'deianira { r2*3/2 }
    >> re''4 sib' r |
    <<
      \tag #'basse { s2. \ffclef "vbas-dessus" <>^\markup\character Deianira }
      \tag #'deianira { R2. }
    >>
    r4 re'' sol' do'' do''4. re''8 |
    sib'2. r4 la'2 |
    sol'4 sol'2 sol'4 fa'2 |
    mi'2 mi'4 |
    sib'4. sol'8 fa'4 fa'2 mi'4( |
    fa') fa''2 |
    r4 re'' do'' do''2. |
    r4 r sib' sib'2 la'4 |
    r re'' mi'' fa''4. fa''8 fa''4 |
    r sib'2 |
    r4 la'2 r4 sol'2~ |
    sol'4 fa' fa' |
    sol'4. la'8 sib'4 sib'2 la'4( |
    sib'2.)~ sib' |
    R2.*2 |
  }
  %% Hyllo
  \tag #'(hyllo basse) {
    <<
      \tag #'basse { s2.*12 \ffclef "vtenor" <>^\markup\character Hyllo }
      \tag #'hyllo {
        \clef "vtenor" <>^\markup\character Hyllo
        R2.*10 |
        r4 sib sol do' do' r |
      }
    >>
    re'2 r4 |
    fa' fa' la |
    <<
      \tag #'basse { sib2 s4 s2.*5 \ffclef "vtenor" <>^\markup\character Hyllo }
      \tag #'hyllo {
        sib2( do'4) do'2. |
        R2.*2 |
        r4 mi' fa' fa' mib'4. re'8 |
      }
    >>
    re'2 re'4 fa' do' re' |
    <<
      \tag #'basse { sib2 s4 s2.*5 \ffclef "vtenor" <>^\markup\character Hyllo }
      \tag #'hyllo {
        sib2. la |
        R2.*2 |
        r4 r sib8 sol do'4 do' r |
      }
    >>
    re'2 r4 fa'2 r4 |
    sol'4 mib' re' <<
      \tag #'basse {
        mib'4 s2 s2.*4 s2
        \ffclef "vtenor" <>^\markup\character Hyllo
      }
      \tag #'hyllo {
        mib'4( la sib) |
        sib2. |
        R2.*2 |
        r2*3/2 r4 r
      }
    >> do'4 |
    la4 la8 la la sib |
    do'4 do' do' |
    fa'4. fa'8 fa' mi' |
    mib'2. |
    re'4 re'4. do'8 |
    <<
      \tag #'basse {
        do'2 s4 s2.*19 \pygmalion s2.*8
        \ffclef "vtenor" <>^\markup\character Hyllo
      }
      \tag #'hyllo {
        do'2. r4 r do' |
        re'2 sib4 do'2 la4 |
        sib2.~ sib4 la2 |
        r2*3/2 r4 fa' fa' |
        sol' sol' re' |
        fa'2 fa'4 r re'2~ |
        re'4 do'2 |
        r4 re'4.\melisma mi'8 fa'4\melismaEnd do'2 re'4 sib2 |
        la2. r2*3/2
        \pygmalion { r2*3/2 R1.*3 R2. }
        R2. |
        r4 sib4. sol8 do'2 do'4 |
      }
    >>
    r4 re'2 fa' r4 |
    sol' mib' re' |
    mib'2. <<
      \tag #'basse { re'4 s2 \ffclef "vtenor" <>^\markup\character Hyllo }
      \tag #'hyllo { re'2. | }
    >>
    r4 do' la <<
      \tag #'basse { s2. \ffclef "vtenor" <>^\markup\character Hyllo }
      \tag #'hyllo { r2*3/2 }
    >>
    fa'4 do' r |
    \tag #'hyllo {
      r2*3/2 r4 fa' do' |
      mib' mib'4. fa'8 re'2. |
      r4 do'2 sib4 sib2 |
      sib4 la2 |
      re'4. sib8 la4 sol2. |
      la |
      r4 sol'2 r4 fa' mib' |
      mib'2. r4 r re' |
      re'2. do' |
      r4 re' mi' |
      fa'4. fa'8 fa'4 r re'2 |
      r4 do'2 |
      sib4. do'8 re'4 re'( do'2) |
      sib2.~ sib |
      R2.*2 |
    }
  }
>>
\pygmalion R2.*16
