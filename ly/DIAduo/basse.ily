\clef "basse" \ru#8 { sib2. la sol fa  }
sib la sol fa4. mib8 re do sib,4 do2 |
fa2. mib re |
do4. re8 mi! fa |
sol4 sol,2 |
do2. fa |
\ru#3 { sib2. la sol fa }
sib,4 do2 |
fa2.
\pygmalion\ru#2 { sib2. la sol fa }
\ru#4 { sib2. la sol fa }
mib re |
do |
sib, do |
fa |
\ru#4 { sib2. la sol fa }
\pygmalion {
  \ru#4 { sib la sol fa }
  mib2. re2 mib4 |
  fa2 fa,4 sib,2. |
}

