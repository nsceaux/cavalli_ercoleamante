\clef "dessus" R1*52 |
r2 r8 fa''32[ sol'' la'' sib''] do'''16[ sol''] mi''32[ fa'' sol''16] |
fa''32[ sol'' la''16] re''32[ mi'' fa''16] mi''8 do''32[ re'' mi'' fa''] sol''[ la'' sib''16] la''32[ sib'' do'''16] sib''32[ do''' re'''16] sol''32[ la'' sib''16] |
la''32[ si''! do''' re'''] si''[ do''' re''' mi'''] do'''8. la''16 si''16[ sol''32 la''] si''16[ do'''] la''32[ si'' do''' re'''] sol''16.[ la''32] |
si''16[ sol''32 la''] si''16[ do'''] la''32[ sol'' la'' si''] do'''[ si'' do''' la''] si''[ la'' sol'' fa'' mi'' re'' do'' si'] do''4 |
R1*15 |
