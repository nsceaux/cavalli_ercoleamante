\score {
  \new ChoirStaff <<
    \pygmalion\new Staff \with { \haraKiriFirst } <<
      \global \includeNotes "dessus"
    >>
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*4\break s1*4\break s1*4\break s1*5\pageBreak
        s1*4\break s1*4\break s1*5\break s1*5\break s1*4\pageBreak
        s1*4 s2 \bar "" \break s2 s1*5\break s1*4\break s1*5\pageBreak
        s1*2 s2 \bar "" \break s2 s1*3\break s1*4 s2 \bar "" \break
        s2 s1*3 s2 \bar "" \break s2 s1*5\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
