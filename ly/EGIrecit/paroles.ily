At -- ten -- de -- te -- mi
at -- ten -- de -- te -- mi dun -- que, al -- me di -- let -- te
d’Hyl -- lo, e d’Eu -- ty -- ro in pa -- ce,
ch’a rag -- giun -- ger -- vi io cor -- ro, om -- bra se -- gua -- ce.

Fer -- ma fer -- ma ti pre -- go, e poi -- ché gra -- tie al cie -- lo
tor -- nò l’hor -- ri -- bil om -- bra a ca -- sa su -- a,
e ch’a me co -- sì tor -- na, e fia -- to, e vo -- ce;
vuò dar gra -- to con -- si -- glio a tut -- te du -- a.
E che mi -- glior ri -- me -- dio?
A’ tan -- ti vos -- tri spa -- si -- mi di quel -- lo
a pro -- por -- vi son pron -- to
ch’è di gua -- ri -- re
ch’è di gua -- ri -- re ad Er -- co -- le il cer -- vel -- lo?
\original {
  Quand’ e -- gli si rac -- cen -- da
  per te del con -- giu -- gal do -- vu -- to af -- fet -- to,
  e che non cu -- ri più nuo -- vi i -- me -- ne -- i,
  di -- te -- mi ciò non par -- vi
  as -- sai per con -- so -- lar -- vi?
}
Dun -- que non ti sov -- vie -- ne, o Dei -- a -- ni -- ra,
che per ciò far mez -- zo sì ra -- ro ha -- ve -- mo?
Veg -- gio, ch’il duo -- lo es -- tre -- mo
ti ren -- de sme -- mo -- ra -- ta, e quel -- la ves -- te,
che già Nes -- so cen -- tau -- ro
in mo -- ren -- do a te diè, qui pur non va -- le?
Per far ch’Al -- ci -- de al -- lor che l’ab -- bia in dos -- so
ogn’ al -- tro a -- mor ch’il tuo pon -- ga in non ca -- le?

Chi sa, che fia ben ver?

Ne fa -- rem pro -- va.

Ma ciò per rav -- vi -- va -- re Hyl -- lo non gio -- va.

Oh che stra -- ne di -- man -- de!
Ma ben po -- trei ri -- sus -- ci -- tar un mor -- to,
s’a con -- ten -- tar due fe -- mi -- ne mi po -- si,
ch’è d’ogn’ al -- tro im -- pos -- si -- bi -- le il più gran -- de,
s’in ve -- ce, che per trop -- pa im -- pa -- zien -- za
po -- sar mon -- te su mon -- te
ha -- ves -- ser li gi -- gan -- ti a sas -- so a sas -- so
fab -- bri -- ca -- to il lor pon -- te; al dis -- pet -- to
al dis -- pet -- to di Gio -- ve
sa -- rian mon -- ta -- ti in cie -- lo à far fra -- cas -- so
à far fra -- cas -- so à far fra -- cas -- so.
Si va di là dal mon -- do a pas -- so a pas -- so a pas -- so a pas -- so.
Né fia va -- no il ten -- ta -- re
di le -- var -- ci un os -- ta -- co -- lo co -- tan -- to
com’ è di ha -- ver con Er -- co -- le a coz -- za -- re. __
Che poi dall’ al -- tro can -- to
chi sa chi sa? ch’Hyl -- lo sen -- ten -- do -- si ba -- gna -- to
fat -- to più sag -- gio non si sia pen -- ti -- to
et a nuo -- to sal -- va -- to.
