\ffclef "vsoprano" <>^\markup\character Iole
r4 mi''8 sol'' mi''8. mi''16 mi''4 |
r mi''8 sol'' mi''4 mi''8 mi'' |
do''4 do'' do'' do''8 do'' |
la'4 la' la'8 la'16 si' si'8 si'16 do'' |
do''4 do'' r8 sol'16 la' si'8 si'16 do'' |
re''8 re'' fa'' fa''16 mi'' do''8 do''
\ffclef "valto" <>^\markup\character Licco
sol'8 re' |
r << \original mi' \pygmalion sol' >> re' mi' do'4 do' |
r do'8 do' do'4 do'8 do' |
mi'4 mi'8 do' do' do' do' do' |
do' do'16 do' do'8 si re' re' re' re' |
re'4 re'8 re' re' re'16 re' re'8 do' |
mi'4 mi'8 sol' mi'4 mi'8 mi'16 re' |
fa'8 fa'16 fa' re'8 mi' re'2 do'1 |
r4 r8 mi' mi' mi' re' mi' |
do'4 do'8 do' do' do' do' si |
do'8. do'16 do'8 si re'8 re' re' re' |
re'4 re'8 la si4 si |
si mi'8 si do'4 do' |
re'4 sol'8 re' mi'4 mi'8 mi' |
la'8. sol'16 fad'8 sol' mi' mi' r4 |
\original {
  r4 si mi'8 mi' mi' red' |
  mi'4 mi'8 fa' re' re' re' re' |
  re' re' re' mi' do'4 do'8 do' |
  do' do' do' do' sol'4 fa'8 fa'16 mi' |
  mi'4 mi' sol'8. do'16 do'4 |
  r mi'8 sol' mi'4 mi'8 mi' |
  do'8 do' do' si re'4 re' |
}
r2 fa'4 re' |
r8 la' sol' la' fa' fa'16 fa' fa'8 mi' |
fa'4 fa' r8 fa' mi' fa' |
re'4 re'8 mi'16 fa' sol'4 sol'8 re' |
mi'4 mi' r2 |
do'8 do'16 do' do'8 si si4 si8 si |
si si si do' la la r4 |
r8 do' do' do' fa'4 fa'8 fa' |
fa'4 fa'8 fa'16 mi' fa'8 fa' fa' fa' |
fa'4 fa'8 fa' do' fa' do' re' |
mi'4 mi'8 mi' do' si do'4 |
do'4 r8 do' do' do' do' re' |
mi'4 mi'8 mi' mi' mi' re' mi' |
do'4 do'8 do'16 si re'4 re' |
\ffclef "vsoprano" <>^\markup\character Deianira
r4 r8 si' sol' la' la' la' fad'2
\ffclef "valto" <>^\markup\character Licco
r8 re' fad' mi' |
sol'4 sol'
\ffclef "vsoprano" <>^\markup\character Iole
r4 re'' |
si' r8 si' si' si' si' la' |
la' la' r mi'' la'2 |
<< { \voiceTwo sol'2*1/2 \oneVoice } \new Voice { \voiceOne r4 } >>
\ffclef "valto" <>^\markup\character Licco
sol'8 fa' mi'4 mi'8 re' |
do'4 do' r8 << \original la \pygmalion fa' >> do' re' |
re'4. re'8 re' re' re' mi' |
fa'4 fa'8 fa' fa' mi' mi' fa' |
re'8. do'16 do'8 si si si si si |
si4 si8 si mi' mi'16 mi' fa'8 la' |
si2 la |
<<
  \original {
    r4 sol do'8 do' do' do' |
    do' do' do' si do'4 do'8 do' |
  }
  \pygmalion {
    r4 sol' mi'8 mi' mi' mi' |
    mi' mi' mi' re' mi'4 mi'8 mi' |
  }
>>
mi'4 mi'8 mi'16 mi' sol'4 sol'8 sol' |
sol' sol' sol' sol' sol' sol'16 sol' sol'8 fa' |
sol'8 sol' sol' sol' sol'4 sol'8 fa' |
la'4 la' la'8 fa' sol' sol'16 mi' |
fa'8 fa'16 re' mi'8 mi'16 do' re'8 mi' fa' sol' |
la' la'16 sol' fa'8 mi' re' re'16 sol' fa'8 mi' |
re' re'16 sol' fa'8 mi' re'4 do' |
r mi' mi'8 mi' mi' mi' |
la4 la8 la si4 do' |
si si r << \original la \pygmalion la' >> |
si do' si2 |
la4 mi'8 mi' mi'4 mi'8 mi' |
la' la' la' la' la'4 la'8 sol' |
sol'8. sol'16 sol'8 la' fa'4 fa'8 fa' |
fa' fa' mi' fa' re' re'16 re' sol'8 la' |
fa'4. mi'8( re'4) r8 re' |
fad' fad' fad' mi' sol'4 sol'8 sol' |
re' sol' re'4 re' re'8 re' |
re'8. re'16 re'8 do' mi'4 mi' |
mi' mi'8 fa' sol'4 sol' |
mi'8 mi' mi' fa' fa' fa' fa' fa' |
mi'4 re'8 << \original mi' \pygmalion sol' >> do'4 do' |
