\clef "basse" <<
  \original { do1 | do | do | }
  \pygmalion { do1 | si, | la,2 sol, | }
>>
fa,1 |
do |
sol,4 fa,~ fa, sol, |
do1 |
do |
do~ |
do2 sol, |
sol,1 |
do |
fa,2 sol, do1 |
la, |
la,~ |
la,2 fa,~ |
fa, mi, |
mi la |
sol do' |
<<
  \original { la2~ la4 si | }
  \pygmalion { la4 si mi r | }
>>
\original {
  mi1 |
  mi~ |
  mi2 la,~ |
  la, sol, |
  do1 |
  do~ |
  do2 sol, |
}
re1 |
re~ |
re |
sib, |
la, |
la, |
sold,2 la, |
fa,1~ |
fa,~ |
fa, | \allowPageTurn
do~ |
do~ |
do |
la,2 sol, |
sol,1 re2~ re |
si,2~ si, |
si,2. do4 |
re do re2 |
sol,1 |
mi,2 fa,~ |
fa, mi, |
re,1 |
re,2 mi,~ |
mi, do,4 re, |
mi,2 la, |
do1~ |
do |
do~ |
do |
do |
fa,2 fa4 mi |
re do8 la, sib, do re mi |
fa sol la fa sol8. mi16 fa8 do |
sol8. mi16 fa8 do sol4 do |
do1 |
fa2 mi4 re |
mi2. fa4 |
mi4 re mi2 |
la,1 |
fa,2 fa |
mi re~ |
re4 do sib, sol, |
la,2 re,~ |
re, sol,~ |
sol,1 |
sol,2 do |
do1~ |
do2 fa, |
sol, do |
