\score {
  <<
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'coro11 \includeNotes "voix"
      >> \keepWithTag #'coro11 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'coro12 \includeNotes "voix"
      >> \keepWithTag #'coro12 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'coro13 \includeNotes "voix"
      >> \keepWithTag #'coro13 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'coro14 \includeNotes "voix"
      >> \keepWithTag #'coro14 \includeLyrics "paroles"
    >>
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'coro21 \includeNotes "voix"
      >> \keepWithTag #'coro21 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'coro22 \includeNotes "voix"
      >> \keepWithTag #'coro22 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'coro23 \includeNotes "voix"
      >> \keepWithTag #'coro23 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'coro24 \includeNotes "voix"
      >> \keepWithTag #'coro24 \includeLyrics "paroles"
    >>
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*6 s2.*3\pageBreak
        s2.*11 s1\pageBreak
        s1*7\pageBreak
        s1*4\pageBreak
        s1*3 s2.*3\pageBreak
      }
    >>
  >>
  \layout { indent = \noindent }
  \midi { }
}
