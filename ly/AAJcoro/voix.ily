<<
  \tag #'(coro11 basse) {
    \clef "vsoprano" mi''1\fermata |
    mi''\fermata |
    r8 mi'' mi'' re'' do''4 fa'' |
    re''2 mi''8 mi'' mi'' re'' |
    do''4 fa'' re''2 mi''1 |
    r4 mi'' mi'' |
    re''4. re''8 re''4 re'' re'' mi'' |
    do''4 do'' do'' do''2 fa''4 |
    mi''2. mi''4. mi''8 re''4 re'' re'' re'' |
    do''2 si'4 mi''4. mi''8 re''4 |
    re''2 re''4 mi''4. mi''8 re''4 |
    re''2. re'' |
    re''1\fermata |
    re''\fermata |
    do''8.([ re''16 mi''8. fa''16] sol''2) |
    do''8.([ re''16 mi''8. fa''16] sol''2) |
    r8 mi'' mi'' re'' do''4 fa'' |
    re''2 mi''8 mi'' mi'' re'' |
    do''4 fa'' re''2 mi''1\fermata |
    r4 fa'' sol''8 sol'' mi'' mi'' |
    fa'' fa'' fa'' mi'' re''4 re''8 fa'' |
    mi''4 mi'' mi''8 mi''16 mi'' mi''8 mi'' |
    fa''4 fa'' fa''8 fa''16 fa'' fa''8 fa'' |
    fa''4 fa''8 re'' re''4 re''8 re'' |
    mi''2( la') la'1 |
    si'4 si' re'' |
    re''4. re''8 re'' re'' |
    si'4 si'8 si' si' si' |
    mi''2 mi''4 re''2 re''4 |
    do''8.[\melisma re''16 mi''8. fa''16] sol''2\melismaEnd |
    do''8.[\melisma re''16 mi''8. fa''16] sol''2\melismaEnd |
    r8 sol'' mi'' re'' do''4 fa'' |
    re''2 mi''8 sol'' mi'' re'' |
    do''4 fa'' re''2 mi''1\fermata |
  }
  \tag #'coro12 {
    \clef "valto" sol'1\fermata |
    sol'\fermata |
    r8 sol' sol' fa' mi'4 la' |
    sol'2 sol'8 sol' sol' fa' |
    mi'4 la' sol'2 sol'1 |
    r4 sol' sol' |
    sol'4. sol'8 la'4 sol' sol' sol' |
    fa'4 fa' sol' fa'2 la'4 |
    sol'2. sol'4. sol'8 sol'4 fad'4 fad' sol' |
    sol'2 fad'4 mi'4. mi'8 la'4 |
    sol'2 sol'4 sol'4. sol'8 sol'4 |
    sol'2 fad'4( sol'2.) |
    sol'1\fermata |
    sol'\fermata |
    mi'\fermata |
    mi'\fermata |
    r8 sol' sol' fa' mi'4 la' |
    sol'2 sol'8 sol' sol' fa' |
    mi'4 la' sol'2 sol'1\fermata |
    r4 la'4 mi'8 mi' sol' sol' |
    la' la' la' sol' fa'4 fa'8 la' |
    la'4 la' la'8 la'16 la' la'8 la' |
    la'4 la' la'8 la'16 la' la'8 la' |
    sib'4 sib'8 sib' sib'4 sib'8 la' |
    la'4.\melisma sol'16[ fa'] mi'2\melismaEnd fad'1 |
    sol'4 sol' la' |
    sol'4. sol'8 la' la' |
    sol'4 sol'8 sol' sol' sol' |
    sol'2 sol'4 sol'2 sol'4 |
    sol'1\fermata |
    sol'\fermata |
    r8 do''8 sol' fa' mi'4 la' |
    sol'2 sol'8 do'' sol' fa' |
    mi'4 la' sol'2 sol'1\fermata |
  }
  \tag #'coro13 {
    \clef "vtenor" do'1\fermata |
    do'\fermata |
    r8 do' sol re' sol4 la |
    re'2 do'8 do' sol re' |
    sol4 la re'2 do'1 |
    r4 do' do' |
    re'4. re'8 la4 re' re' do' |
    do'4 do' do' do'2 re'4 |
    sol2. do'4. do'8 re'4 la la re' |
    mi'2 si4 do'4. do'8 re'4 |
    si2 re'4 do'4. do'8 re'4 |
    re'2. si |
    re'4.( do'8 si2) |
    re'4.( do'8 si2) |
    do'( mi') |
    do'( mi') |
    r8 do' sol re' sol4 la |
    re'2 do'8 sol sol re' |
    sol4 la re'2 do'1\fermata |
    r4 do' sol8 sol sol sol |
    re'8 re' re' sol re'4 re'8 re' |
    mi'4 mi' dod'8 dod'16 dod' dod'8 la |
    re'4 re' fa'8 fa'16 fa' re'8 re' |
    re'4 re'8 re' sol'4 sol'8 fa' |
    mi'4( la mi'2) re'1 |
    re'4 re' re' |
    si4. re'8 re' re' |
    sol4 sol8 sol re' sol |
    do'2 sol4 si2 si4 |
    do'1\fermata |
    do'\fermata |
    r8 sol sol re' sol4 la |
    re'2 do'8 sol sol re' |
    sol4 la re'2 do'1\fermata |
  }
  \tag #'coro14 {
    \clef "vbasse" do'1\fermata |
    do'\fermata |
    r8 do do re mi4 fa |
    sol2 do8 do do re |
    mi4 fa sol2 do1 |
    r4 do do |
    sol4. sol8 fad4 sol sol mi |
    fa4 fa mi fa2 re4 |
    mi2. do4. do8 si,4 re re si, |
    do2 re4 mi4. mi8 fad4 |
    sol2 sol4 do4. do8 sol,4 |
    re2. sol, |
    sol1\fermata |
    sol\fermata |
    do'\fermata |
    do'\fermata |
    r8 do do re mi4 fa |
    sol2 do8 do do re |
    mi4 fa sol2 do1\fermata |
    r4 fa mi8 mi mi mi |
    re re re mi fa4 fa8 re |
    la4 la la8 la16 la la8 la |
    fa4 fa fa8 fa16 fa fa8 fa |
    sib4 sib8 sib sol4 sol8 re |
    la1 re |
    sol4 sol fad |
    sol4. sol8 re re |
    mi4 mi8 mi si, si, |
    do2 do4 sol2 sol4 |
    do'1\fermata |
    do'\fermata |
    r8 do do re mi4 fa |
    sol2 do8 do do re |
    mi4 fa sol2 do1\fermata |
  }
  \tag #'coro21 {
    \clef "vsoprano" do''1\fermata |
    do''\fermata |
    r8 do'' do'' la' sol'4 do'' |
    do''4. si'8( do'') do'' do'' la' |
    sol'4 do'' do''4. si'8( do''1) |
    r4 do'' do'' |
    si'4. si'8 la'4 si' si' do'' |
    la'4 la' mi'' la'2 re''4 |
    si'2. do''4. do''8 sol'4 la' la' si' |
    mi''2 re''4 do''4. do''8 la'4 |
    si'2 si'4 do''4. do''8 si'4 |
    la'2. si' |
    si'1\fermata |
    si'1\fermata |
    mi''4.( re''8 do''2) |
    mi''4.( re''8 do''2) |
    r8 do'' do'' la' sol'4 do'' |
    do''4. si'8( do'') do'' do'' la' |
    sol'4 do'' do''4. si'8( do''2) r4 do'' |
    do''4. do''8 dod'' dod'' dod'' dod'' |
    re'' re'' re'' do'' la'4 la'8 re'' |
    dod''4 dod'' dod''8 dod''16 dod'' dod''8 dod'' |
    re''4 re'' re''8 re''16 re'' re''8 re'' |
    re''4 re''8 re'' sib'4 sib'8 re'' |
    re''2. dod''4( re''1) |
    re''4 re'' la' |
    si'4. si'8 re'' la' |
    mi''4 mi''8 mi'' re'' re'' |
    do''2 do''4 si'2 si'4 |
    mi''1\fermata |
    mi''\fermata |
    r8 mi'' do'' la' mi''4 do'' |
    do''4. si'8( do'') mi'' do'' la' |
    mi''4 do'' do''4. si'8( do''1)\fermata |
  }
  \tag #'coro22 {
    \clef "valto" sol'1\fermata |
    sol'\fermata |
    r8 mi' mi' fa' sol'4 fa'8[ do'] |
    sol'2 mi'8 mi' mi' fa' |
    sol'4 fa'8[ do'] sol'2 mi'1 |
    r4 mi' mi' |
    sol'4. sol'8 re'4 sol' sol' do' |
    fa'4 do' mi' la'2 fa'4 |
    sol'2. mi'4. mi'8 sol'4 re' re' sol' |
    sol'2 re'4 sol'4. sol'8 re'4 |
    sol'2 sol'4 mi'4. mi'8 si4 |
    re'2. re' |
    re'2( sol') |
    re'2( sol') |
    sol'1\fermata |
    sol'\fermata |
    r8 mi' mi' fa' sol'4 fa'8[ do'] |
    sol'2 mi'8 mi' mi' fa' |
    sol'4 fa'8[ re'] sol'2 mi' r4 mi' |
    fa'4. la'8 sol' sol' sol' sol' |
    fa' fa' fa' do' fa'4 fa'8 fa' |
    la'4 mi' mi'8 mi'16 mi' mi'8 dod' |
    fa'4 fa' fa'8 fa'16 fa' fa'8 fa' |
    fa'4 fa'8 fa' sol'4 sol'8 la' |
    mi'2( la') fad'1 |
    re'4 re' re' |
    sol'4. re'8 fad' fad' |
    mi'4 mi'8 la' re' re' |
    mi'2 mi'8[ sol'] sol'2 sol'4 |
    sol'1\fermata |
    sol'\fermata |
    r8 mi' mi' fa' sol'4 fa'8[ do'] |
    sol'2 mi'8 mi' mi' fa' |
    sol'4 fa'8[ do'] sol'2 mi'1\fermata |
  }
  \tag #'coro23 {
    \clef "vtenor" mi'1\fermata |
    mi'\fermata |
    r8 sol do' fa do'4 do' |
    sol2 sol8 sol do' fa |
    do'4 do' sol2 sol1 |
    r4 sol sol |
    si4. si8 re'4 si4. la8 sol4 |
    la4 la sol la2 la4 |
    mi'2 mi8[\melisma fad] sol4.\melismaEnd sol8 si4 fad! fad si8[ sol] |
    do'2 fad4 sol4. sol8 la4 |
    re'2 si4 sol4. sol8 sol4 |
    la2. sol |
    sol8.([ la16 si8. do'16] re'2) |
    sol8.([ la16 si8. do'16] re'2) |
    do'1\fermata |
    do'\fermata |
    r8 sol do' fa do'4 do' |
    sol2 sol8 sol do' fa |
    do'4 do' sol2 sol r4 do' |
    la4. la8 mi' mi' mi' mi' |
    la la la mi' la4 la8 la |
    la4 la la8 la16 la la8 la |
    la4 la la8 la16 la la8 re' |
    sib4 sib8 sib re'4 re' |
    r8 la mi' la la2 la1 |
    sol4 si la |
    re'4. si8 la fad |
    si4 si8 si si si |
    sol2 do'4 re'2 re'4 |
    mi'1\fermata |
    mi'1\fermata |
    r8 do' do' fa do'4 do' |
    sol2 sol8 do' do' fa |
    do'4 do' sol2 sol1\fermata |
  }
  \tag #'coro24 {
    \clef "vbasse" do'1\fermata |
    do'\fermata |
    r8 do do re mi4 fa |
    sol2 do8 do do re |
    mi4 fa sol2 do1 |
    r4 do do |
    sol4. sol8 fad4 sol sol mi |
    fa4 fa mi fa2 re4 |
    mi2. do4. do8 si,4 re re si, |
    do2 re4 mi4. mi8 fad4 |
    sol2 sol4 do4. do8 sol,4 |
    re2. sol, |
    sol1\fermata |
    sol\fermata |
    do'\fermata |
    do'\fermata |
    r8 do do re mi4 fa |
    sol2 do8 do do re |
    mi4 fa sol2 do r4 do |
    fa4. fa8 mi mi mi mi |
    re re re mi fa4 fa8 re |
    la4 la la8 la16 la la8 la |
    fa4 fa fa8 fa16 fa fa8 fa |
    sib4 sib8 sib sol4 sol8 re |
    la1 re |
    sol4 sol fad |
    sol4. sol8 re re |
    mi4 mi8 mi si, si, |
    do2 do4 sol2 sol4 |
    do'1\fermata |
    do'1\fermata |
    r8 do do re mi4 fa |
    sol2 do8 do do re |
    mi4 fa sol2 do1\fermata |
  }
>>
