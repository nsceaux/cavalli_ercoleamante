\clef "basse" do1\fermata |
do1\fermata_\markup\italic piano |
r8 do do re mi4 fa |
sol2 do8 do[_\markup\italic piano do re] |
mi4 fa sol2 do1 |
r4 do do |
sol2 fad4 sol2 mi4 |
fa4 fa mi fa2 re4 |
mi2. do2 si,4 re2 si,4 |
do2 re4 mi2 fad4 |
sol2 sol4 do2 sol,4 |
re2. sol, |
sol,1\fermata |
sol,\fermata _\markup\italic piano |
do\fermata |
do\fermata _\markup\italic piano |
r8 do do re mi4 fa |
sol2 do8 do do re |
mi4 fa sol2 do\fermata r4 do |
fa4. fa8 mi2 |
re4. mi8 fa4. re8 |
la2. la4 |
fa2. fa4 |
sib4. sib8 sol4. re8 |
la1 re |
sol4 sol fad |
sol4. sol8 re4 |
mi4. mi8 si,4 |
do2 do4 sol2. |
do1\fermata |
do\fermata _\markup\italic piano |
r8 do do re mi4 fa |
sol2 do4. _\markup\italic piano re8 |
mi4 fa sol2 do1\fermata |
