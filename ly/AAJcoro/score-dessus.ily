\score {
  <<
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'coro11 \includeNotes "voix"
      >> \keepWithTag #'coro11 \includeLyrics "paroles"
      \new Staff \withLyrics <<
      { \override Staff.Script.outside-staff-priority = #10
        s1 s^\markup\italic piano s1 s2 s8 s4.^\markup\italic piano s1*2
        s2.*14
        s1 s^\markup\italic piano s1 s^\markup\italic piano s1*11
        s2.*5
        s1 s^\markup\italic piano s1 s2 s8 s4.^\markup\italic piano }
        \global \keepWithTag #'coro12 \includeNotes "voix"
      >> \keepWithTag #'coro12 \includeLyrics "paroles"
    >>
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'coro21 \includeNotes "voix"
      >> \keepWithTag #'coro21 \includeLyrics "paroles"
      \new Staff \withLyrics <<
      { \override Staff.Script.outside-staff-priority = #10
        s1 s^\markup\italic piano s1 s2 s8 s4.^\markup\italic piano s1*2
        s2.*14
        s1 s^\markup\italic piano s1 s^\markup\italic piano s1*11
        s2.*5
        s1 s^\markup\italic piano s1 s2 s8 s4.^\markup\italic piano }
        \global \keepWithTag #'coro22 \includeNotes "voix"
      >> \keepWithTag #'coro22 \includeLyrics "paroles"
    >>
  >>
  \layout { indent = \noindent }
}
