Ò ò ò Gal -- lia for -- tu -- na -- ta
ò Gal -- lia for -- tu -- na -- ta
già per tan -- te vit -- to -- rie,
di pa -- ce, ed i -- me -- nei l’ul -- ti -- me glo -- rie
ti fan -- no ol -- tre o -- gni spe -- me og -- gi be -- a -- ta.

Ò ò ò ò ò Gal -- lia for -- tu -- na -- ta
ò Gal -- lia for -- tu -- na -- ta.

E a fin
\tag #'(coro21 coro22 coro23 coro24) { e a fin }
ch’a tuoi con -- ten -- ti
gio -- ia o -- gn’hor s’au -- gu -- men -- ti
ec -- co, ch’in te si ve -- de
al -- ba di nuo -- ve glo -- rie un re -- gio
\tag #'coro23 { un re -- gio e -- re -- de; }
\tag #'(coro11 basse coro12 coro13 coro14 coro21 coro22 coro24) { e -- re -- de; }
per splen -- der più di dop -- pio so -- le
di dop -- pio so -- le or -- na -- ta

ò ò ò Gal -- lia for -- tu -- na -- ta
ò Gal -- lia for -- tu -- na -- ta.
