\key do \major \midiTempo#120
\override Score.MetronomeMark.self-alignment-X = #-0.5
\time 4/4 s1 \bar "||" \tempo\markup\normal-text "Tou douxemans" s1*3
\measure 2/1 s1*2
\digitTime\time 3/4 s2.
\measure 6/4 s1.*2
\measure 9/4 s2.*3
\measure 6/4 s1.*3
\time 4/4 s1 \tempo\markup\normal-text "Tou douxemans" s1*5
\measure 2/1 s1*2
\measure 4/4 s1*5
\measure 2/1 s1*2
\digitTime\time 3/4 s2.*3
\measure 6/4 s1.
\time 4/4 s1 \bar "||" \tempo\markup\normal-text "Tou douxemans" s1*3
\measure 2/1 s1*2 \bar "|."
