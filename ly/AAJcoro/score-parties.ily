\score {
  <<
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'coro12 \includeNotes "voix" \clef "alto"
      >> \keepWithTag #'coro12 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        { \override Staff.Script.outside-staff-priority = #10
          s1 s^\markup\italic piano s1 s2 s8 s4.^\markup\italic piano s1*2
          s2.*14
          s1 s^\markup\italic piano s1 s^\markup\italic piano s1*11
          s2.*5
          s1 s^\markup\italic piano s1 s2 s8 s4.^\markup\italic piano }
        \global \keepWithTag #'coro13 \includeNotes "voix" \clef "alto"
      >> \keepWithTag #'coro13 \includeLyrics "paroles"
      %\new Staff \withLyrics <<
      %  { \override Staff.Script.outside-staff-priority = #10
      %    s1 s^\markup\italic piano s1 s2 s8 s4.^\markup\italic piano s1*2
      %    s2.*14
      %    s1 s^\markup\italic piano s1 s^\markup\italic piano s1*11
      %    s2.*5
      %    s1 s^\markup\italic piano s1 s2 s8 s4.^\markup\italic piano }
      %  \global \keepWithTag #'coro14 \includeNotes "voix"
      %>> \keepWithTag #'coro14 \includeLyrics "paroles"
    >>
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'coro22 \includeNotes "voix" \clef "alto"
      >> \keepWithTag #'coro22 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        { \override Staff.Script.outside-staff-priority = #10
          s1 s^\markup\italic piano s1 s2 s8 s4.^\markup\italic piano s1*2
          s2.*14
          s1 s^\markup\italic piano s1 s^\markup\italic piano s1*11
          s2.*5
          s1 s^\markup\italic piano s1 s2 s8 s4.^\markup\italic piano }
        \global \keepWithTag #'coro23 \includeNotes "voix" \clef "alto"
      >> \keepWithTag #'coro23 \includeLyrics "paroles"
      %\new Staff \withLyrics <<
      %  { \override Staff.Script.outside-staff-priority = #10
      %    s1 s^\markup\italic piano s1 s2 s8 s4.^\markup\italic piano s1*2
      %    s2.*14
      %    s1 s^\markup\italic piano s1 s^\markup\italic piano s1*11
      %    s2.*5
      %    s1 s^\markup\italic piano s1 s2 s8 s4.^\markup\italic piano }
      %  \global \keepWithTag #'coro24 \includeNotes "voix"
      %>> \keepWithTag #'coro24 \includeLyrics "paroles"
    >>
  >>
  \layout { }
}
