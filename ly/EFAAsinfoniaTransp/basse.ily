\transpose do fa, {
\clef "bass" do1 |
fa, do\breve |
sol1 do |
sol\breve |
mib2 fa sol do |
sol\breve do1*2\fermata |
}