\transpose do fa, {
\clef "bass" sol1 |
fa sol\breve\fermata |
re'1 do' |
re'\breve\fermata |
do'2 re' re' sol |
re'\breve do'\fermata |
}