\transpose do fa \key fa \major \midiTempo#240
\tempo "Douxemans" \time 4/4 s1
\measure 3/1 s1*3
\measure 2/1 s1*6
\measure 4/1 s1*4 \bar "|."
