Che sa -- gre -- fi -- ci in -- gra -- ti?
Che prie -- ghi in -- giu -- rio -- si?
Che vo -- ti ob -- bro -- bri -- o -- si?
Por -- gon -- si a me? co -- sì s’ol -- trag -- gia Eu -- ty -- ro?
Co -- sì co -- sì co -- sì fia, ch’a sua vo -- glia
fred -- da in -- sen -- si -- bil om -- bra ogn’ un mi cre -- da?
Fa -- rò fa -- rò ben, che s’av -- ve -- da
l’o -- mi -- ci -- da la -- dron, s’an -- cor m’a -- di -- ro?
E se con -- tro di lui
o -- dio, rab -- bia, fu -- ror più che mai spi -- ro?
Dun -- que chi del mio san -- gue
fe’ scem -- pio in -- gius -- to, del mio san -- gue an -- co -- ra
far vor -- rà suo di -- let -- to? ah non fia ma -- i non fia ma -- i:
e tu dar vi -- ta a i par -- ti
di chi mor -- te a mi di è fi -- glia po -- tra -- i?
