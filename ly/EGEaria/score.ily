\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket \with { \haraKiri } <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
      \new Staff << \global \includeNotes "partie1" >>
      \new Staff << \global \includeNotes "partie2" >>
      \new Staff <<
        \global \keepWithTag #'basse \includeNotes "basse"
      >>
    >>
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2.*5 s1 s2.*2 s1 s2.*2\pageBreak
        s1 s2.*2 s1 s2.*3\pageBreak
        s2. s1*2 s2.*2 s1\pageBreak
        s1*4\pageBreak
        s2.*3 s1*3\pageBreak
        s1*3 s2.*2 s1\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
