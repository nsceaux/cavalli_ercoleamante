\clef "vbasse" <>^\markup\character Eutyro
R2.*5 |
r8 r16 si sol sol sol sol mi8 mi r4 |
R2.*2 |
r8 r16 si sol sol sol sol mi8 mi r4 |
R2.*2 |
r16 si sol sol sol sol mi mi mi8 fad16 sol red4 |
R2.*2 |
r8 r16 si fad fad fad mi sol8 sol r4 |
R2.*2 |
r8 sol si la si4 |
r8 re' la la la sol |
la4 la la8 la16 la la8 la |
la la16 la la8 sol si si r4 |
R2.*2 |
r8 re' si4 r8 si16 re' sol8 sol16 fad |
sol16 sol sol sol sol8 sol16 la fa8 fa fa mi |
mi mi r4 mi8 mi mi mi16 fad |
sol4 re8 re sol sol16 sol si si la sol |
re'4 sol r2 |
R2.*3 |
sold4 sold r8 sold sold sold |
sold sold16 sold si8 fad sold4 sold |
sold8 sold sold la la la la la |
mi4 mi8 red fad4 fad |
r si r mi8 mi |
la la r la16 sol mi8 mi r4 |
R2.*2 |
r8 mi sol4 r8 sol sol sol |
sol sol sol sol sol4 sol8 fa |
la2 fa4 fa8 mi sol4 sol r2 |
