\clef "basse" mi,2 mi,4 mi,2 mi,4 mi,2 r4 |
mi,2 mi,4 mi,2 mi,4 |
<<
  \tag #'basse { mi,2 r | }
  \tag #'basse-continue { mi,1 | }
>>
mi,2 mi,4 mi,2 mi,4 |
<<
  \tag #'basse { mi,2 r | }
  \tag #'basse-continue { mi,1 | }
>>
mi,2 mi,4 mi,2 mi,4 |
mi,2 <<
  \tag #'basse { r2 }
  \tag #'basse-continue { do4 si, | }
>>
si,2 si,4 si,2 si,4 |
si,2 <<
  \tag #'basse {
    r2 |
    mi,2 mi,4 mi,2 mi,4 |
    mi,2 r4 |
    R2. R1*2 |
  }
  \tag #'basse-continue {
    mi2 |
    mi2 mi4 mi2 mi4 |
    mi2. |
    re |
    re1~ |
    re2 sol, |
  }
>>
sol,2 sol,4 sol,2 sol,4 |
<<
  \tag #'basse {
    sol,2 r |
    R1*4 |
  }
  \tag #'basse-continue {
    sol,1~ |
    sol, |
    do |
    sol, |
    re,4 sol, r2 |
  }
>>
mi,2 mi,4 mi,2 mi,4 |
mi,2 r4 |
<<
  \tag #'basse { R1*6 | }
  \tag #'basse-continue {
    mi1~ |
    mi |
    mi2 do~ |
    do si, |
    sol, do |
    la,2. si,4 |
  }
>>
mi,2 mi,4 mi,2 mi,4 |
<<
  \tag #'basse {
    mi,2 r |
    R1*3 |
  }
  \tag #'basse-continue {
    mi,1 |
    do |
    fa, do |
  }
>>
