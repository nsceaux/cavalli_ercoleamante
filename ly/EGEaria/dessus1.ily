\clef "dessus" sol'2 sol'4 sol'2 sol'4 sol'2 r4 |
sol'2 sol'4 sol'2 sol'4 |
sol'2 r |
sol'2 sol'4 sol'2 sol'4 |
sol'2 r |
sol'2 sol'4 sol'2 sol'4 |
sol'2 r |
si'2 si'4 si'2 si'4 |
si'2 r |
sol'2 sol'4 sol'2 sol'4 |
sol'2 r4 |
R2. R1*2 |
sol'2 sol'4 sol'2 sol'4 |
sol'2 r |
R1*4 |
sol'2 sol'4 sol'2 sol'4 |
sol'2 r4 |
R1*6 |
sol'2 sol'4 sol'2 sol'4 |
sol'2 r |
R1*3 |
