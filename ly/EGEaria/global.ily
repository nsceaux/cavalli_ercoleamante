\key la \minor
\beginMark "Sinf[onia] Infernale"
\tempo "Bien fort messieurs"
\digitTime\time 3/4 \midiTempo#160 \measure 9/4 s2.*3
\measure 6/4 s1.
\time 4/4 \midiTempo#80 s1
\digitTime\time 3/4 \midiTempo#160 \measure 6/4 s1.
\time 4/4 \midiTempo#80 s1
\digitTime\time 3/4 \midiTempo#160 \measure 6/4 s1.
\time 4/4 \midiTempo#80 s1
\digitTime\time 3/4 \midiTempo#160 \measure 6/4 s1.
\time 4/4 \midiTempo#80 s1
\digitTime\time 3/4 \midiTempo#160 \measure 6/4 s1.
\measure 3/4 s2.*2
\time 4/4 \midiTempo#80 s1*2
\digitTime\time 3/4 \midiTempo#160 \measure 6/4 s1.
\time 4/4 \midiTempo#80 s1*5
\digitTime\time 3/4 \midiTempo#160 \measure 6/4 s1.
\measure 3/4 s2.
\time 4/4 \midiTempo#80 s1*6
\digitTime\time 3/4 \midiTempo#160 \measure 6/4 s1.
\time 4/4 \midiTempo#80 s1*2
\measure 2/1 s1*2 \bar "|."

