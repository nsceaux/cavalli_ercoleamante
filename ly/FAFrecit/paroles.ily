\original {
  Se nel ter -- res -- tre mon -- do
  per i -- ni -- quo fa -- vor d’in -- gius -- to cie -- lo
  il suo cor -- po -- reo ve -- lo
  al -- la nos -- tra mor -- tal spo -- glia pre -- val -- se,
  ad on -- ta del suo or -- go -- glio al fi -- ne im -- pa -- ri,
  che di sde -- gno, e di for -- za ogn’ al -- ma è pa -- ri.
  Che? se più lo las -- cia -- mo
  res -- pi -- rar im -- pu -- ni -- to
  in pa -- ce, e ti -- ran -- nia l’au -- re vi -- ta -- li,
  cre -- de -- rà con ra -- gio -- ne,
  che sian di ti -- mid’ om -- bre, e ne -- ghit -- to -- se
  i re -- gni di Plu -- ton ta -- ne o -- ti -- o -- se. __
}
Sù, sù \original { dun -- que sù, sù }
dun -- que om -- bre ter -- ri -- bi -- li
su vo -- liam tut -- te tut -- te in Eo -- cha -- li -- a,
nuo -- va in ciel schie -- ra stim -- fa -- li -- a
con -- tra il reo fu -- rie in -- vi -- si -- bi -- le,
e con le vi -- pe -- re
on -- de Te -- si -- fo -- ne
tor -- men -- ta l’a -- ni -- me
fla -- gel -- la -- mo -- gli
fla -- gel -- la -- mo -- gli il cor;
fin ch’im -- men -- so do -- lor
con an -- go -- scie rab -- bio -- se il ren -- da e -- sa -- ni -- me.
