\clef "vbasse" <>^\markup\character Eutyro
\original {
  r4 sib sib8 sib sib la |
  sib4 sib sib8 sib sib sib16 sib |
  sib8 sib do' re' la4 la8 la |
  la la la sol la la la la |
  la la la la la4 la8 sib |
  sol4 sol8 sib sol sol sol fa |
  fa fa16 fa fa8 sol mib mib r mib16 sol |
  re8 re r si16 la do'8 do' r do |
  si,4 si,8 do re2 |
  sol, r4 si |
  r8 si si si16 re' sol8 sol sol sol |
  sol4 sol8 fad sol4 sol8 la |
  fa8 fa16 fa fa8 fa fa4 fa8 fa16 mi |
  mi8 mi do do mi4 mi8 fa |
  sol4 sol8 sol sol sol sol sol |
  sol sol16 sol sol8 fa la4 la8 la |
  la la la la re4 re8 mi16 fa |
  fa4. mi8( fa2) |
  r4 fa8 fa sib4 sib |
}
r4 sib sib |
mib4 re2 |
mib4 fa2 sib,4. sib,8 sib,4 |
r4 sib sol |
la la sib do' sib do' |
la4. la8 la4 |
sib2 sib4 sol2. |
lab4 sol lab fa4. fa8 fa4 |
sol2 sol4 mib2. |
fa4 sol sol, do4. do8 do4 |
do' do' do' |
fa4. fa8 fa4 |
sib sib sib mib4. mib8 mib4 |
sol sol sol lab4. lab8 lab4 |
fa2 fa4 sol4. fa8 mib4 |
sol2 sol4 lab4. lab8 sib4 |
mib mib mib lab lab lab |
la!2 la8 la la la16 sib |
sib4 sib8 sib re4 re8 mib |
fa2. fa4 sib,1\fermata |
