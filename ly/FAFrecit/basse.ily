\clef "basse"
\original {
  sib1~ |
  sib~ |
  sib2 la~ |
  la1 |
  la |
  sol |
  fa2 mib |
  re do |
  si,4. do8 re2 |
  sol,1 |
  sol, |
  sol,~ |
  sol, |
  do |
  do~ |
  do2 la,~ |
  la, sib, |
  do fa, |
  fa sib |
}
sib2 sib4 |
mib4 re2 |
mib4 fa2 sib,2. |
sib2 sol4 |
la2 sib4 do'2. |
la |
sib sol |
lab fa |
sol mib |
fa4 sol2 do2. |
do |
fa |
sib mib |
sol lab |
fa sol |
sol2 sol4 lab2 sib4 |
mib2. lab |
la!2~ la |
sib2 re4. mib8 |
fa1 sib,\fermata |
