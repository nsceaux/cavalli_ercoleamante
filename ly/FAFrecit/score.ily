\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*4\break s1*3\break s1*4\break s1*4\break s1*4 s2.\pageBreak
        s2.*11\break s2.*10\break s2.*6 s1\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
