\ffclef "vbas-dessus" <>^\markup\character Iole
r4 fa'' re'' re''8 re'' |
re'' re'' re'' re'' re'' re''16 re'' re''8 fa'' |
do''4 do'' do''8 do'' do'' do''16 re'' |
sib'4 sib' do'' do''8 sol' |
la'4 la' r2 |
\ffclef "vbas-dessus" <>^\markup\character Giunone
r8 re'' fa'' fa''16 re'' fa''8 fa'' r4 |
r mi'' la' la' |
r8 la'16 la' mi''8 mi''16 fa'' re''8 re'' r16 fa'' re'' la' |
fa'8 fa'16 mi' fa' mi' fa' re' la'8 la' r16 la' dod'' la' |
re''8 re'' r16 re'' re'' re'' re'' re'' re'' re'' re''8. re''16 |
si'8 si'16 sol'' mi'' mi'' mi'' mi'' dod'' dod'' dod'' dod'' dod''8 dod''16 re'' |
re''8 re'' r4 r2 |
\ffclef "vbas-dessus" <>^\markup\character Iole
r4 do'' la' la' |
r re'' sold' sold' |
r r8 mi'' si'4 si' |
si'8 si'16 si' sold' si' mi' mi' r8 mi' mi' fad' |
fad'2 r8 sold' sold' la' |
la'4. re''8 si'4 si' |
r mi'' la' r |
r8 la' la' si' sol'4 sol' |
sol'8 sol' sol' sol'16 fa' fa'4 fa' |
mi'4 mi'8 fa' mi'4. red'8( mi'1) |
mi'4 mi'8 mi' sol'4 sol'8 sol' |
sol' sol' sol' fa' sol'4 sol'8 sol' |
sol' sol' sol' fa' la'4 la' |
la'8 la' la' la'16 la' do''8 do'' do'' do'' |
fa'4 fa'8 mi' sol' sol' sol' fa' |
mi'4 mi'8 re' mi'4( mib' re'1) do' | \allowPageTurn
r4 la'8 do'' la'4 la'8 la' |
mi'4 mi' mi'8 mi'16 mi' mi'8 fa' |
fa'4 fa' fa' fa'8 sol' |
sol'2 sol' |
sol'4 sol'8 la' la' la' la' la' |
si'4 si'8 do'' do''4 do'' |
r do''8 do'' re''4 re''8 do'' |
mi''4 mi''8 mi'' do''4 la' |
fa'2\melisma mi'~ |
mi'4 fad'8[ sold'] la'2~ |
la'4 si'8[ do''] re'' do''4\melismaEnd si'8( la'1) r |
