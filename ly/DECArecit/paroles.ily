E qua -- le in -- as -- pet -- ta -- to
son -- no pro -- di -- gi -- o -- so
pre -- ve -- nen -- do I -- me -- ne -- o le -- ga il mio spo -- so?

I -- o -- le, I -- o -- le, ah sor -- gi
sor -- gi ra -- pi -- da, fug -- gi, e t’al -- lon -- ta -- na
dall’ in -- can -- ta -- to seg -- gio, e a me t’ap -- pres -- sa
che di ben tos -- to ri -- sa -- nar -- ti è d’uo -- po
dal ma -- gi -- co ve -- le -- no,
ond’ hai l’a -- ni -- ma op -- pres -- sa.

O Di -- va, o Dè -- a, dà qua -- li
or -- ri -- di pre -- ci -- pi -- zi
d’in -- fe -- del -- tà, d’in -- i -- qui -- tà ri -- sor -- go?
Ohi -- mè! di qua -- li er -- ro -- ri
rea, quan -- tun -- que in -- no -- cen -- te o -- ra mi scor -- go! __
Pu -- re il mio pri -- mo, e sol gra -- di -- to fo -- co,
ch’in me pa -- re -- va es -- tin -- to
men -- tre il cor mi ral -- lu -- ma,
con u -- su -- ra di fiam -- me
più che mai mi con -- su -- ma.
Ma che pro? s’Hyl -- lo in -- tan -- to
l’u -- ni -- co mio te -- so -- ro
sen -- za mia col -- pa
sen -- za mia col -- pa a ra -- gion me -- co i -- ra -- to,
a ra -- gion da me fug -- ge, e a tor -- to io mo -- ro. __
