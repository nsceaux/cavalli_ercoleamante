\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*5\break s1*3\break s1*2\break s1*4\break s1*4\pageBreak
        s1*3\break s1*5\break s1*3 s2 \bar "" \break s2 s1*5\break s1*5\pageBreak
        s1*5\break s1*8\break s1*5\break s1*5\break s1*4\pageBreak
        s1*4 s2 \bar "" \break s2 s1*4\break s1*3 s2.*4\break s2.*9\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
