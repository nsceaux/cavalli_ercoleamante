\clef "basse" sib,2 sib |
la sol |
fa la, |
sol,2 mib, |
re,1 |
re |
dod |
dod2 re~ |
re4 sib, la, sol, |
fa,1 |
sol,~ |
sol,4 la, re,2 |
la,1 |
fa,2 mi, |
mi1 |
mi |
la,~ |
la,4 fa, mi,2 |
mi do~ |
do si,~ |
si, la, |
sold,4 la, si,2 mi,1 |
do~ |
do | \allowPageTurn
do2 fa, |
fa,1 |
re,2 do,4 do,8 re, |
mi,4 fa, sol, fa, sol,1 do | \allowPageTurn
la, |
sol, |
fa, |
mi,~ |
mi,2 re, |
re do~ |
do si, |
la,1~ |
la,4 la sol mi |
do2~ do4 re8 mi |
fa2 re~ re mi la,1\fermata |
