%% Iole
\key fa \major \time 4/4 \midiTempo#80 s1*5 \bar "|."
%% Giunone
\key do \major s1*7 \bar "|."
%% Iole
\measure 4/4 s1*9
\measure 2/1 s1*2
\measure 4/4 s1*5
\measure 3/1 s1*3
\measure 4/4 s1*10
\measure 3/1 s1*3 \bar "|."
