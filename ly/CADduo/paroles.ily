Ga -- re ga -- re d’af -- fet -- to ar -- den -- ti
deh non ce -- de -- te non ce -- de -- te
\tag #'hyllo { deh non ce -- de -- te }
non ce -- de -- te a i gua -- i,
\tag #'(iole basse) { e nel go -- der } non vi stan -- ca -- te
e nel go -- der non vi stan -- ca -- te ma -- i,
che de’ vos -- tri ar -- gu -- men -- ti
nell’ u -- gua -- glian -- za sol tut -- ta si sta
l’a -- mo -- ro -- sa fe -- li -- ci -- tà
\tag #'(iole basse) {
  tut -- ta tut -- ta si sta
  l’a -- mo -- ro -- sa fe -- li -- ci -- tà
  tut -- ta tut -- ta si sta
  l’a -- mo -- ro -- sa
  l’a -- mo -- ro -- sa fe -- li -- ci -- tà
  l’a -- mo -- ro -- sa
  l’a -- mo -- ro -- sa fe -- li -- ci -- tà
  l’a -- mo -- ro -- sa fe -- li -- ci -- tà.
}
\tag #'hyllo {
  l’a -- mo -- ro -- sa fe -- li -- ci -- tà
  l’a -- mo -- ro -- sa
  l’a -- mo -- ro -- sa fe -- li -- ci -- tà
  tut -- ta tut -- ta si sta
  l’a -- mo -- ro -- sa
  l’a -- mo -- ro -- sa
  l’a -- mo -- ro -- sa fe -- li -- ci -- tà
  l’a -- mo -- ro -- sa fe -- li -- ci -- tà.
}
