\clef "basse" fa2. |
mi2 fa4 sol2 mi4 |
re2. do |
la2 sib4 do'2 la4 |
sol2. fa |
re do |
sib, sib4 la2 |
sol2. |
fa2 fa4 |
sol fa2 sol2. |
do |
re |
mib2 re4 |
do2. |
re |
mib4 do2 re2. sol, |
<<
  \original {
    sol1~ |
    sol~ |
    sol2 mi~ |
    mi fa |
    fa fa4 sol2 mib4 |
    fa sol2 |
    do4 fa2 |
    sol2. |
    la2 sol4 |
    la2. |
    sib2 sol4 |
    la2. |
    re2 sib,4 |
    do2. re2 do4 |
    re2. sol,2 mib4 |
    fa2. sol2 fa4 |
    sol2. do |
    fa |
    sib, do2 sib,4 |
    do2. fa2 sib,4 |
    do2 sib,4 do2. fa,1\fermata |
  }
  \pygmalion {
    sol,1~ |
    sol,~ |
    sol,2 mi,~ |
    mi, fa, |
    fa, fa,4 sol,2 mib,4 |
    fa, sol,2 |
    do4 fa,2 |
    sol,2. |
    la,2 sol,4 |
    la,2. |
    sib,2 sol,4 |
    la,2. |
    re2 sib,4 |
    do2. re2 do4 |
    re2. sol,2 mib,4 |
    fa,2. sol,2 fa,4 |
    sol,2. do |
    fa, |
    sib, do2 sib,4 |
    do2. fa,2 sib,4 |
    do2 sib,4 do2. fa,1\fermata |
  }
>>
