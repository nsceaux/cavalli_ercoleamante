\clef "viole2" R2.*25 |
sol1 |
sol |
sol |
sol2 la |
do2 fa4 re sol do |
lab re4. sol8 |
mi4 do do' |
sol sol4. sib!8 |
la2 sib4 |
la2 sol4 |
sol2 sib4 |
mi mi fa |
re la4. sib8 |
sol4 la sib la fad sol |
sol2 fad4 sol2 do'4 |
fa4 fa4. lab8 sol4 do do' |
do'2 si4 do' sol sol |
la2. |
re4 re4. mi8 do4. do'8 reb'4 |
lab8 fa do'2 do' fa4 |
mi fa2 do2. do1\fermata |
