\piecePartSpecs
#(if (eqv? #t (ly:get-option 'pygmalion))
     `((violes #:score "score-violes")
       (basse #:score "score-basse"))
     '((basse #:score "score-basse")))
