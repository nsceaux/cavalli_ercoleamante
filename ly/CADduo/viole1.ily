\clef "viole1" R2.*25 |
sib2 sib4. re'8 |
re'1 |
re'4. do'8 do'2~ |
do' do' |
la2 do'4 si4. do'16 re' mib'4 |
do'4 sol' sol |
sol la4. fa'8 |
re'4 mi' fa' |
mi' la'4. dod'8 |
re'4 dod'4. dod'8 |
re'4 sib re' |
la2.~ |
la4 re' re' |
do'2 mib'4 re'2 mib'4 |
re'2 re4 re2 mib4 |
do re mib re sol lab |
sol2 re4 mi2. |
r4 do' do' |
sib4. la8 sol4 sol fa2 |
do'4 sol4. la8 la2 sib4 |
sol do' reb' do' sol do' la1\fermata |
