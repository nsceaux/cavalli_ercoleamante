<<
  \tag #'(iole basse) {
    \clef "vsoprano" <>^\markup\character Iole
    R2.*3 |
    r2*3/2 sol'2 sol'4 |
    do''4. sib'8 la'4 sol' la'2 |
    sib'2. la' |
    r4 sib'2 r4 la'4. sib'8 |
    sib'2 sib'4 r do''4. sib'8 |
    sib'2. |
    la'4 la'4. la'8 |
    si'4 do''2 do'' si'!4( |
    do'') r8 do'' \ficta sib'! do'' |
    la'2. |
    r4 r8 sol' fa' sol' |
    mi'4 mi'8 la' sol' la' |
    fad'4. sib'8 la' sib' |
    sol'4 fad'( sol') sol'2 fad'!4( sol'2.) |
    r4 sol'8 sol' sol'4 sol'8 fad' |
    sol'2 sol'4. sol'8 |
    sol'8 sol' sol' la' sib'2 |
    sol'4 sol'8 la' fa'2 |
    r4 la'4. la'8 si'4 sol' do'' |
    do'' si'( do'') |
    do'' la'4. la'8 |
    si'4 dod'' re'' |
    dod''2. |
    r4 la'4. la'8 |
    re''4 mi''4. fa''8 |
    re''4 dod''( re'') |
    re'' re'4. re'8 |
    mi'4 fad' sol' fad'!2 r4 |
    r4 re''4. do''8 si'4 si' r |
    r fa''4. fa''8 fa''4 mib''4. re''8 |
    mib''4 re''2 do''4 do''4. sib'8 |
    la'2 la'4 |
    r sib'4. sib'8 sib'4 lab'4. sol'8 |
    lab'4 sol'2 fa' sib'8 sib' |
    sib'4 lab'4. sol'8 lab'4 sol'2 fa'1\fermata |
  }
  \tag #'hyllo {
    \clef "vtenor" <>^\markup\character Hyllo
    do'2 do'4 |
    sol'4. fa'8 mi'4 re' mi'2 |
    fa'2. mi' |
    r2*3/2 r4 fa'2 |
    r4 mi'4. fa'8 fa'2 fa'4 |
    r fa'4. mib'8 mib'2. |
    re' r4 fa'2 |
    r4 mi'4. fa'8 |
    fa'4 fa' fa'8 mib' |
    re'4 mib'2 re' re'4( |
    do'2.) |
    r4 r8 re' do' re' |
    sib4 sib r |
    r r8 do' sib do' |
    la4. re'8 do' re' |
    sib4 la( sib) la2. sol |
    r4 sib8 sib sib4 sib8 la |
    sib2 sib4. sib8 |
    sib8 sib sib la sol2 |
    do'4 do'8 do' la2 |
    r4 fa'4. fa'8 fa'4 mib'4. re'8 |
    mib'4 re'2 |
    do'2. |
    r4 sol'4. sol'8 |
    sol'4 fa'4. mi'8 |
    fa'4 mi'2 |
    re'2. |
    r4 la4. sol8 fad4 fad r |
    r4 do'4. do'8 do'4 sib4. la8 |
    sib4 la2 sol sol8 sol |
    la4 si do' si!2 r4 |
    r sol'4. fa'8 mi'2 mi'4 |
    r4 fa'4. mib'8 |
    re'4 re' re'8 re' mi'4 do' fa' |
    fa' mi'( fa') fa'2 re'8 re' |
    mi'4 do' fa' fa' mi'( fa') fa'1\fermata |
  }
>>
