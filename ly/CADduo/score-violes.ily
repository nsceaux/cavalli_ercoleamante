\score {
  <<
    \new ChoirStaff \with { \tinyStaff } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'iole \includeNotes "voix"
        >> \keepWithTag #'iole \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'hyllo \includeNotes "voix" \clef "alto"
      >> \keepWithTag #'hyllo \includeLyrics "paroles"
    >>
    \pygmalion\new StaffGroup \with { \haraKiriFirst } <<
      \new Staff << \global \includeNotes "viole1" >>
      \new Staff << \global \includeNotes "viole2" >>
      \new Staff << \global \includeNotes "viole3" >>
    >>
  >>
  \layout { }
}
