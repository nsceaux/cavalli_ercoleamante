\score {
  \new StaffGroupNoBar <<
    \pygmalion\new StaffGroupNoBracket \with { \haraKiriFirst } <<
      \new Staff << \global \includeNotes "viole1" >>
      \new Staff << \global \includeNotes "viole2" >>
      \new Staff << \global \includeNotes "viole3" >>
    >>
    \new Staff \withLyrics <<
      \global \keepWithTag #'iole \includeNotes "voix"
    >> \keepWithTag #'iole \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'hyllo \includeNotes "voix"
    >> \keepWithTag #'hyllo \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2.*11\break s2.*8\pageBreak
        s2.*6\break s1*4 s2.*2\break s2.*8\pageBreak
        s2.*9\break
      }
      \pygmalion\modVersion {
        s2.*25\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
