\key fa \major \midiTempo#180
\beginMark "a 2" \digitTime\time 3/4 s2.
\measure 6/4 s1.*6
\measure 3/4 s2.*2
\measure 6/4 s1.
\measure 3/4 s2.*5
\measure 9/4 s2.*3
\time 4/4 s1*4
\digitTime\time 3/4 \measure 6/4 s1.
\measure 3/4 s2.*8
\measure 6/4 s1.*4
\measure 3/4 s2.
\measure 6/4 s1.*2
\measure 10/4 s2.*2 s1 \bar "|."
