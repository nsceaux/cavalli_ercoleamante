<<
  \tag #'(coro11 basse) {
    <>^\markup\character Coro di Fiumi
    \clef "vsoprano"
    R2.*7 |
    r4 r do'' re'' re'' mi'' |
    fa'' re''2 |
    do''4 fa''2 |
    fa''4 mi''2 fa''4 fa''( sol'') |
    do''( re''2 mi''4) re''2 |
    mi''4 re''( la') si' dod''2 |
    re''2. r4 re''4. re''8 |
    mi''4 si'4. si'8 do''4 re'' mi'' |
    la'2 do''4~ |
    do'' do'' si' si' la' re'' |
    do'' do''2 si'4 si'2 |
    la'2 do''4 si' re''2 |
    do''4. si'8 la'4 |
    si'2 re''4 |
    do''4 do'' si'4 mi'' do''2 |
    r4 r do'' |
    re''4 re'' mi''4 fa'' re''2 |
    do'' mi''4 re'' re''2 |
    si'4 re''2 do''4 do''4. do''8 |
    re''4 la'8[ si'] do''4 |
    re''4 re''4. sol''8 mi''2 re''4~ |
    re''4 re'' mi'' re'' re'' mi'' |
    re'' mi''2 fa''4 sol''2 |
    do''2 fa''4 mi''2 mi''4 |
    re''4. re''8 re''4 mi''1\fermata |
  }
  \tag #'coro12 {
    \clef "valto" r4 r do' |
    re' re' mi' fa' re'2 |
    do'4 fa'2 fa'4 mi'2 |
    re'4 sol'2 mi'4(\melisma fa'2) |
    mi'4 fad'2\melismaEnd r4 sol'2 |
    re'4 re'2 |
    mi'4 fa'2 |
    sol'2 mi'4 re' re' sol' |
    fa' fa'2 sol'4 sol'2 |
    do'4 fa'2 mi'4 la'2 |
    la'4 fad'4. fad'8 sol'4 sol'4. \ficta fa'8 |
    mi'4( fad' sol' la') re' sol' |
    fad' fad' mi' |
    re' re' mi' fad' fad' re' |
    mi' fad'2 sol' re'4 |
    mi' fad'2 re' sol'4 |
    sol'4. sol'8 fad'4 |
    sol'2. |
    r4 r re' mi' mi' fad' |
    sol' mi'2 |
    re'4 sol'2 fa'8([ sol'] la'4) sol' |
    mi' mi'2 fa'2. |
    r4 sol'4. sol'8 mi'4 mi'4. do'8 |
    fa'4 fa'4. mi'8 |
    fa'2 sol'4 sol'2 fa'4~ |
    fa' fa' mi' sol' sol' sol' |
    fa' mi'2 la'4 sol'2 |
    fa' la'4 sol'2 sol'4 |
    la'4. la'8 sol'4 sol'1\fermata |
  }
  \tag #'coro13 {
    \clef "vtenor"
    R2.*5 |
    r4 r sol la la si |
    do' la2 sol do'4~ |
    do' si si |
    do'4 re'2 |
    sol2 do'4 fa' fa' mi' |
    la re'2 sol re'4 |
    la re'2 sol'4 mi'2 |
    fad'4 re'4. re'8 mi'4 si4. si8 |
    do'4 re' mi' la si4.\melisma do'8\melismaEnd |
    re'4 re'\melisma la~ |
    la\melismaEnd fad sol re' re' si |
    do' re'2 mi'4 si2 |
    do'4 la2 si si4 |
    do'4. do'8 re'4 |
    sol2 sol4 |
    do' do' fa do' do'2 |
    re'4 do'2 |
    sol4 re'( do') la re'( sol) |
    sol2. r4 re'4. re'8 |
    mi'4 si4. si8 do'4. re'8 mi'4 |
    la2. |
    r4 re'2 do'4 do' la4 |
    sol sol do' re' re' do' |
    re' sol'2 do'4 do'2 |
    do' re'4 sol2 do'4 |
    fa'4. fa'8 re'4 do'1\fermata |
  }
  \tag #'coro14 {
    \clef "vbasse"
    R2.*11 |
    r4 r do re re mi |
    fa re2 do4 sol2 |
    sol4 fa2 sol4 la2 |
    re2. r2*3/2 |
    R2.*10 |
    r4 r sol,4 |
    la, la, si, do la,2 |
    sol,4 do2 |
    do4 si,( do) re re( fa) |
    do4 do4. do8 re2 re4 |
    r4 sol4. sol8 la4 mi4. mi8 |
    fa4. sol8 la4 |
    re2 sol4 do2 re4~ |
    re si, do sol sol mi |
    fa sol2 la4 mi2 |
    fa re4 mi2 mi4 |
    fa4. fa8 sol4 do1\fermata |
  }
  \tag #'coro21 {
    \clef "vsoprano"
    R2. |
    r4 r sol' la' la' si' |
    do'' la'2 sol'4 do''2 |
    do''4 si'2 do''4 do''( re'') |
    sol'4(\melisma la'2) si'\melismaEnd do''4 |
    re'' sol'2 |
    la'4 la'2 |
    si' do''4 la' la' do'' |
    la' si'2 do''4 si'2 |
    do''4 la'2 mi''4 mi''( la') |
    la'2. r2*3/2 |
    R2.*10 |
    r4 r si'4 |
    la' la' sol' sol' la'2 |
    si'4 sol'2 |
    sol'4 si'( sol') re'' re''( si') |
    mi''2. r4 la'4. la'8 |
    sol'4 si'4. si'8 la'4 sol'4. sol'8 |
    la'4 re'' la' |
    la'2 si'4 mi' mi' la' |
    si'4. si'8 sol'4 si' si' do'' |
    fa'' si'2 do''4 mi''2 |
    la' re''4 do''2 do''4 |
    do''4. do''8 si'4 do''1\fermata |
  }
  \tag #'coro22 {
    \clef "valto"
    R2.*11 |
    r4 r sol' fa' fa' do' |
    re' fa'2 mi'4 sol'2 |
    la'4 re'2 si4 mi'2 |
    re'2. r2*3/2 |
    R2.*10 |
    r4 r sol'4 |
    mi'4 mi' sol'4 mi' mi'( do') |
    sol' sol'2 |
    re'4 re'( sol') re'2 re'8[ sol'] |
    sol'4 sol'4. sol'8 fa'2 fa'4 |
    r4 re'4. sol'8 do'4 mi'4. sol'8 |
    fa'4 la'4. la'8 |
    fa'2 re'4 sol'2 re'4~ |
    re' re' do' si si mi' |
    la' sol'2 fa'4 do'2 |
    fa'2 fa'4 do'2 do'4 |
    do'4. do'8 sol'4 mi'1\fermata |
  }
  \tag #'coro23 {
    \clef "vtenor"
    R2.*11 |
    r4 r sol re' re' sol |
    la fa2 do'4 re'( si) |
    mi' fa'( re') re' dod'2 |
    la2. r2*3/2 |
    R2.*10 |
    r4 r re4 |
    la la re' sol do'( la) |
    re mi2 |
    sol4 sol( mi) la4 sol( si) |
    do'4 do'4. do'8 la2 la4 |
    r2*3/2 r4 sol4. sol8 |
    re'4 re' do' |
    la la sol sol do' fa |
    sol4. sol8 sol4 sol sol sol |
    la si( sol) do' sol2 |
    la la4 mi'2 mi'4 |
    la4. la8 si4 sol1\fermata |
  }
  \tag #'coro24 {
    \clef "vbasse"
    R2.*11 |
    r4 r do re re mi |
    fa re2 do4 sol2 |
    sol4 fa2 sol4 la2 |
    re2. r2*3/2 |
    R2.*10 |
    r4 r sol,4 |
    la, la, si, do la,2 |
    sol,4 do2 |
    do4 si,( do) re re( sol) |
    do4 do4. do8 re2 re4 |
    r4 sol4. sol8 la4 mi4. mi8 |
    fa4. sol8 la4 |
    re2 sol4 do2 re4~ |
    re si, do sol sol mi |
    fa sol2 la4 mi2 |
    fa re4 mi2 mi4 |
    fa4. fa8 sol4 do1\fermata |
  }
>>
