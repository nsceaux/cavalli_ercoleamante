\score {
  <<
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'coro12 \includeNotes "voix" \clef "alto"
      >> \keepWithTag #'coro12 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'coro13 \includeNotes "voix" \clef "alto"
      >> \keepWithTag #'coro13 \includeLyrics "paroles"
      %\new Staff \withLyrics <<
      %  \global \keepWithTag #'coro14 \includeNotes "voix"
      %>> \keepWithTag #'coro14 \includeLyrics "paroles"
    >>
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'coro22 \includeNotes "voix" \clef "alto"
      >> \keepWithTag #'coro22 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'coro23 \includeNotes "voix" \clef "alto"
      >> \keepWithTag #'coro23 \includeLyrics "paroles"
      %\new Staff \withLyrics <<
      %  \global \keepWithTag #'coro24 \includeNotes "voix"
      %>> \keepWithTag #'coro24 \includeLyrics "paroles"
    >>
    %\new Staff << \global \includeNotes "basse" >>
  >>
  \layout { indent = \noindent }
}
