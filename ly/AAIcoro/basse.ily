\clef "petrucci-c4/alto" r4 r do' |
re' re' mi' fa' re'2 |
do'4 fa'2 fa'4 mi'2 |
re'4 sol' sol la la si |
do' la2 sol do'4 |
do' si si |
do' re'2 |
sol \clef "bass" <>_"Tutti" do4 re re mi |
fa re2 do4 sol2 |
sol4 fa2 sol4 la2 |
re4 \clef "alto" re'4. re'8 mi'4 si4. si8 |
do'4 re' mi' la si do' |
re'2 la4 |
fad2 sol4 re'2 si4 |
do' re'2 mi'4 si2 |
do'4 la2 si2. |
do'2 re'4 |
sol2 \clef "bass"
<>_"Tutti" sol,4 |
la, la, si, do la,2 |
sol,4 do2~ |
do4 si, do re re sol |
do do4. do8 re4 \clef "G_8" re'4. re'8 |
mi'4 \clef "bass" sol4. sol8 la4 mi4. mi8 |
fa4. sol8 la4 |
re2 sol4 do2 re4 |
si,2 do4 sol2 mi4 |
fa sol2 la4 mi2 |
fa2 re4 mi2. |
fa2 sol4 do1\fermata |
