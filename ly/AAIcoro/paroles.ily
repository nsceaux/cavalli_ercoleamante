\tag #'(coro11 coro12 coro21 basse) {
  E ve -- da e ve -- da o -- gn’un, che de -- si -- ar non sa __
  che de -- si -- ar non sa
}
\tag #'coro13 {
  E ve -- da e ve -- da o -- gn’un, che de -- si -- ar non sa
}
\tag #'(coro12 coro13 coro14 coro21 coro22 coro23 coro24) {
  E ve -- da e ve -- da o -- gn’un, che de -- si -- ar non sa
}

\tag #'(coro11 basse coro12 coro13) {
  un e -- roi -- co va -- lo -- re
  qui giù pre -- mio \tag #'(coro12 coro13) { pre -- mio } mag -- gio -- re
  che di ve -- der in pa -- ce al -- ma al -- ma bel -- tà
}

\tag #'(coro11 basse) {
  e ve -- da e ve -- da
  e ve -- da e ve -- da o -- gn’un, che de -- si -- ar non sa
}
\tag #'(coro12 coro13 coro14 coro21 coro22 coro23 coro24) {
  e ve -- da e ve -- da o -- gn’un, che de -- si -- ar non sa
}
un e -- roi -- co
\tag #'(coro14 coro22 coro23 coro24) { un e -- roi -- co }
\tag #'(coro21) { e -- roi -- co }
va -- lo -- re
\tag #'(coro12 coro13 coro14 coro21 coro22 coro24) { qui giù }
qui giù pre -- mio \tag #'(coro13 coro21 coro23) { pre -- mio } mag -- gio -- re
che di ve -- der in pa -- ce al -- ma al -- ma bel -- tà.
