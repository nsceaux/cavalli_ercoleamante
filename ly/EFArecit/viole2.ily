\clef "viole2" fa'2 mi'4 |
fa'4. mi'8 re' do' |
re'4 do'2 |
do'2. |
mib' |
fa'2 fa4 |
sib sol2 |
la4 sol fa |
sol mi la |
re2 sol4 |
la2. |
re2 re4 |
sol2. |
la2 fa4 |
mi sol re |
mi fa' mi' |
fa'4. mi'8 re' do' |
re'4 do'2 |
do'2 do'8 re' |
sib4 re' do' |
re' fa' mi'8 fa' |
fa'2 do'4 |
re'2. |
sol2 do'4 |
re'2. |
sol2 la4 |
sib2 sol4 |
la2. |
sib |
la2 sol4 |
fa mi2 |
r4 fa'8 re' mib'4 |
r8 fa' re'4 sib |
fa'2 do'4 |
la2. sol |
lab2 do'4 |
sol2. sol2 sib4 |
la do' sib |
fa'4. mi'8 re' do' |
re'4 do'2 do'1*3/4\fermata |
R1*36 R2.*11 R1*10
