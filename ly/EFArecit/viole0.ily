\clef "viole0" la'2 sol'4 |
do''2 sib'8 la' |
sol' fa' fa'4 mi' |
fa'2. |
sib'2 do''4 |
la'2 sib'4 |
sol'2 la'4 |
fa' sol' la' |
mi'2 fa'4 |
re'2 mi'4~ |
mi' do' re'~ |
re' re' sol'~ |
sol'2 sol'4 |
fa'4. mi'8 re' do' |
do'2 si4 |
do'2 sol'4 |
do''2 sib'8 la' |
sol' fa' fa'4 mi' |
fa'2 do''4~ |
do''8 re'' sib' la' la'4 |
fa' sol'2 |
do''2. |
sib' |
sib'2 la'4 |
la'2 sib'4 |
mi'2 fad'4 |
fad' sol' mi' |
mi'2 re'4 |
sol'2. |
la'2 re'4~ |
re' la'2 |
fa' r8 fa' |
fa'4 sib'2 |
r4 r8 sol' lab'4 |
la'!2 la'4 si'2. |
do''4 re'' lab' |
sol'2 re'4 mi'4. mi'8 fa' sol' |
la'2 sol'4 |
do''2 sib'8 la' |
sol' fa' fa'4 mi' fa'1*3/4\fermata |
R1*36 R2.*11 R1*10
