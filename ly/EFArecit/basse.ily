\clef "basse" fa2 sol4 |
la2 sib4~ |
sib do'2 |
fa2. |
sol |
fa |
mib |
re |
do |
sib, |
la, |
sol, |
mi, |
fa, |
sol, |
do4 fa sol |
la2 sib4~ |
sib do'2 |
fa4. sol8 la4 |
sib4. sib8 la4 |
sib4 sol2 |
fa2. |
sib |
mib |
re |
do |
sib, |
la, |
sol, |
fad,2 sol,4 |
la,2. |
re4 re' do' |
sib2 sol4 |
fa2. |
fad sol |
fa |
sol do |
fa2 sol4 |
la2 sib4~ |
sib do'2 fa1*3/4\fermata |
sib,1~ |
sib,2 mib |
mib1 |
re |
do |
sib,2 lab, |
sol,1~ |
sol, |
fa, |
mi, |
mi, |
fa, |
sol,2. fa,4 |
sol,1 do\fermata |
do1~ |
do~ |
do~ |
do |
sol |
re2 mib4 do |
re2 sol, |
mib1 |
mib~ |
mib |
sib, |
fa~ |
fa2 re |
mib2 fa4 sib, |
fa2 do |
sol1 |
lab2 fa |
sol do |
do1~ |
do |
do2 fa~ |
fa fa1 |
fa2. |
re |
do |
sib, |
sol |
fa |
mib |
fa |
sol |
do'1 |
lab |
sib mib |
do~ |
do |
sol |
do |
si,4 do re2 sol,1\fermata |
