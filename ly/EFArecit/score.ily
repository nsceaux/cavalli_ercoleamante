\score {
  \new StaffGroupNoBar <<
    \pygmalion\new StaffGroupNoBracket \with { \haraKiriFirst } <<
      \new Staff << \global \includeNotes "viole0" >>
      \new Staff << \global \includeNotes "viole1" >>
      \new Staff << \global \includeNotes "viole2" >>
    >>
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \original\origLayout {
        s2.*5\break s2.*7\break s2.*9\pageBreak
        s2.*7\break s2.*8\break s2.*7 s1 s2 \bar "" \break s2 s1*4\break s1*4\pageBreak
        s1*5\break s1*5\break s1*4\break s1*4\break s1*4\pageBreak
        s1*4 s2 \bar "" \break s1 s2.*6\break s2.*3 s1*4\break
      }
      \pygmalion\origLayout {
        s2.*5\break s2.*6\pageBreak
        s2.*6\break s2.*6\pageBreak
        s2.*6\break s2.*7\pageBreak
        s2.*7\pageBreak
      }
      \pygmalion\modVersion {
        s2.*43\break
        s1*15\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
