\key fa \major
\digitTime\time 3/4 \midiTempo#160 s2.*34
\measure 6/4 s1.
\measure 3/4 s2.
\measure 6/4 s1.
\measure 3/4 s2.*2
\measure 6/4 s1. \pygmalion\bar "|."
\time 4/4 \midiTempo#80 s1*13 \measure 2/1 s1*2 \bar "|."
\measure 4/4 s1*21 \measure 3/2 s1.
\digitTime\time 3/4 \midiTempo#160 s2.*9
\time 4/4 \midiTempo#80 s1*2 \measure 2/1 s1*2
\measure 4/4 s1*4 \measure 2/1 s1*2 \bar "|."
