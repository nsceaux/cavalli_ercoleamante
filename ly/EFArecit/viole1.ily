\clef "viole1" do' la sib |
do'2 fa'4 |
sol'2 sol'4 |
la'2. |
sol'2 mib'4 |
do' la re' |
sol2 do'4 |
fa2 fa'4 |
do'2. |
fa'4. mi'8 re' do' |
do'4 la2 |
si2 si4 |
do'2.~ |
do'4 re'8 do' si la |
sol2. |
sol4 la sib |
do'2 fa'4 |
sol'2 sol'4 |
r8 la' la' sib' sol' fa' |
fa' re' sol' fa' fa'4~ |
fa' sib'2~ |
sib'4 la'4. sib'8 |
sib'4 fa' sol' |
sol'2. |
fad'2 sol'4 |
sol'2 do'4 |
re'2 re'4 |
re' dod' fa' |
fa' mib' re' |
re'2 sib4 |
la2.~ |
la4 la'4. la'8 |
la'4 sol'8. fa'16 mi'4 |
do'2.~ |
do'2 do'4 sol'2. |
re'2 mib'4 |
mib' re'2 do'2. |
do'4 fa'4. mi'8 |
do'4 fa'2 |
sol'2 sol'4 la'1*3/4\fermata |
R1*36 R2.*11 R1*10
