\clef "vbas-dessus" <>^\markup\character Deianira
r8 do'' do'' re'' sib' la' |
la' la' re''2 |
sib'8 la' sib'2 |
la' fa''4 |
r8 mib'' mib''4 mib''8 mib'' |
mib''4 re''4. re''8 |
re'' re'' re'' do'' do''4~ |
do'' si' si'8 do'' |
do''4 do'' la'8 la' |
la'4 la' sol'8 sol' |
sol'4. fad'8 fad' sol' |
sol'4 sol'8 sol' sol' sol' |
do''4 do''8 mi' mi' mi' |
la'4 la'4. re'8 |
mi'4( re'2) |
do'8 do'' do'' re'' sib' la' |
la' la' re''2 |
sib'8 la' sib'2 |
la'2. |
R2. |
r4 r sib' |
fa''4. mib''8 mib'' re'' |
re''4 re''8 re'' re'' re'' |
re''4 re'' do'' |
do'' do'' sib' |
sib' sib'8 la' la' la' |
la'4 la'8 sol' sol' sol' |
sol'4 sol'8 fa' fa' fa' |
sib'4 r8 sib' sib' sib' |
re''2 fa'4 |
re'2 dod'4 |
fa''8 re'' fa''4 mib''8 re'' |
re'' do'' do'' do'' do'' sib' |
do''4 do'' do''8 re'' |
mib''4 mib'' mib'' mib''2. |
fa''4 si' do'' |
do''2 si'4( do''2.) |
r8 la' sib' do'' sib' la' |
la' la' re''2 |
sib'8 la' sib'2 la'1*3/4\fermata |
re''4 sib' r re''8 sib' |
lab'4 lab'8 sol' sol'4 sol' |
do'' do'' r8 do'' do'' do'' |
do''2 si'8. si'16 si'8 do'' |
do''8 do'' lab' lab' lab'4 lab'8 sol' |
sol'4. sol'8 sol'4 fa'8 sol' |
sol'2 sol'4 si'8 si' |
re''4 re''8 re'' si' si' si' si' |
si'4 si'8 si' si'4 si'8 do'' |
do''4 do''8 do'' sol'4 sol' |
sol'8 sol'16 sol' sol'8 sol' sol' sol' sol' lab' |
lab'4 lab'8 re'' re'' re'' re'' mib'' |
mib''4 r8 mib' mib'4 mib'8 lab' |
mib'2( re') do'1\fermata |
\ffclef "valto" <>^\markup\character Licco
r4 sol'2 mib'8 mib' |
do'4 do' r mib'8 mib' |
\ficta mib'4 mib'8 re' mib'!4 mib'8 mib' |
mib' mib' mib' mib' mib'8. mib'16 mib'8 re' |
re'4 re'8 re' re' re' re' mi' |
fad'4 fad' r8 fad' fad' sol' |
sol'4. fad'8( sol'2) |
r8 sol' mib' sol' mib'4 mib'8 mib'16 re' |
mib'4 mib' mib'8 mib' mib' mib'16 mib' |
sib2 do'4 do'8 sib |
re'4 re'8 re' re' re' re' re'16 do' |
do'4 do'8 do' do' do' do' do' |
do' do' fa' do' re' re' fa' sol' |
sol' sol' mib' re' sib sib r4 |
<< \original do'8 \pygmalion fa' >> do' do' do'16 re' mib'4 sol'8 mib'!16 re' |
re'4 re' sol'8 sol' sol' sol'16 sol' |
do'4 do' r8 fa' sol' lab' |
sol'4 sol' r2 |
mi'2. sol'8 mi' |
sol'8 mi' mi' re' mi'4 mi'8 mi' |
mi' mi' mi' fa' fa'4 fa' |
do'8 do' do' do'16 do' do'4 do' do' do'8 sib |
do'4 do' la8 la sib4 sib sib8 sib |
do'4 do' r8 do' |
re'4 re' r8 re' |
mib'4 mib' mib'8 fa' |
fa'4 fa' fa'8 sol' |
sol'4 sol' sol' |
lab' lab'8 lab' lab' sol' |
sib'4 sib' r |
r8 mib' mib' mib' mib'4 mib'8 fa' |
fa' fa' fa' fa' fa'4 fa' |
r8 fa' fa' sol' mib'4 mib' r1 |
r4 sol'8 sol' mib'4 mib'8 mib' |
do'4 do' re' re'8 re' |
si4 r8 re' sol' sol' sol' sol' |
mib'4 mib'8 mib' mib' mib' mib' fa' |
sol' sol'16 re' mib'8 sol' re'4 re' r1\fermata |
