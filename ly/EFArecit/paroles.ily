Ed a che peg -- gio i fa -- ti ahi mi ser -- ba -- ro?
Ah che ben mi gui -- da -- ro
gli a -- do -- lo -- ra -- ti miei lan -- gui -- di pas -- si
a tro -- va -- re in al -- cun di ques -- ti sas -- si
co -- me far sa -- zio il mio des -- ti -- no a -- va -- ro.
Ed a che peg -- gio i fa -- ti ahi mi ser -- ba -- ro?
Al -- fin per -- du -- to ho il fi -- glio
e già vi -- ci -- na è l’ho -- ra,
che do -- na ad al -- tra spo -- sa il mio con -- sor -- te,
né per -- ciò av -- vien
né per -- ciò av -- vien ch’io mo -- ra?
Ar -- mi ar -- mi non ha da uc -- ci -- der -- mi la mor -- te,
già che tan -- ti do -- lor non mi sbra -- na -- ro; __
ed a che peg -- gio i fa -- ti ahi mi ser -- ba -- ro?

Pren -- di pren -- di Lic -- co fe -- de -- le
ques -- ti de’ miei te -- sor po -- ve -- ri a -- van -- zi
per pas -- sar me -- no in -- co -- mo -- di i tuoi gior -- ni,
e ri -- mi -- ra se puo -- i,
un dì ques -- ti se -- pol -- cri a -- prir -- mi in cu -- i
d’o -- gni spe -- ran -- za di con -- for -- to i -- gnu -- da
per non mi -- rar più il sol mi col -- chi, e chiu -- da.

Ah Dei -- a -- ni -- ra io non son tan -- to ac -- cor -- to
che pos -- sa in sì gran ca -- ri -- ca ser -- vir -- ti
di te -- so -- rie -- ro in -- sie -- me, e bec -- ca -- mor -- to: __
né so né so s’hab -- bi pen -- sa -- to,
ch’es -- ser pre -- so co -- sì quind’ io po -- tre -- i
per o -- mi -- ci -- da, e la -- dro,
e con so -- len -- ni -- tà con -- dut -- to al pos -- to
d’un su -- bli -- me ap -- pic -- ca -- to,
on -- de fo -- ra tra noi sor -- te ben va -- ria,
tu mor -- res -- ti sot -- ter -- ra, et io nell’ a -- ria.
Deh scac -- cia scac -- cia o Dei -- a -- ni -- ra,
de -- sio sì for -- sen -- na -- to,
che di quan -- ti nell’ ur -- na hab -- bia Pan -- do -- ra
e di -- sas -- tri, e ru -- i -- ne, e pe -- na, e dan -- ni,
e do -- lo -- ri, et af -- fan -- ni,
e an -- gos -- cie, e cre -- pa -- cuo -- ri io ti so di -- re,
ch’il peg -- gio mal di tut -- ti è di mo -- ri -- re.
Ma che pom -- pa fu -- ne -- bre
scor -- go ve -- nir? ti -- ria -- mo -- si in un la -- to
che qual lu -- gu -- bre as -- pet -- to a te fia gra -- to.
