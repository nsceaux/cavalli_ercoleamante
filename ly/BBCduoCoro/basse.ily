\clef "basse" r4 r re mi2 mi4 |
fa2 fa4 sol2 sol4 |
la2 fa4 sol2 sol4 |
la4. sol8 fa4 sol la2 re1\fermata |
re2 fa4 |
sol2 la4 |
re2 la4 la sold2 |
la2 fa4 fa mi2 |
re mi4 |
fa re2 |
mi2 do4 |
re mi2 la,2. |
\clef "alto" r2*3/2 re'4 mi' fad' |
sol'2. do'4 re' mi' |
fa'2. re'4 mi' fa' |
sol'2 fa'4 mi'2 fa'4 |
sol la2 re'1\fermata | \allowPageTurn
\clef "bass" re4 mi fad sol r r |
do re mi fa r r |
re2. re4 mi2 |
la,4 si, dod re r r |
sol, la, sib, do r r |
fa, sol, la, sib,2 la,4 |
sol,2. sol,4 la,2 re,1\fermata | \allowPageTurn
re2 fa4 sol2 la4 |
re2 la4 la sold2 |
la2 fa4 fa mi2 |
re mi4 |
fa re2 |
mi do4 re mi2 la,1\fermata | \allowPageTurn
\clef "alto" r2*3/2 re'4 mi' fad' |
sol'2. do'4 re' mi' |
fa'4. fa'8 fa'4 re' mi' fa' |
sol'4. la'8 sol' fa' mi'2 fa'4 |
sol la2 re'1\fermata | \allowPageTurn
\clef "bass" re4 mi fad sol r r |
do re mi fa r r |
re2. re4 mi2 |
la,4 si, dod re r r |
sol, la, sib, do r r |
fa, sol, la, sib,2 la,4 |
sol,2. sol,4 la,2 re,1\fermata |
