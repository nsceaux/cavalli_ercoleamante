\score {
  \new StaffGroupNoBar \with { \haraKiriFirst } <<
    \new StaffGroupNoBracket <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
      \new Staff << \global \includeNotes "partie1" >>
      \new Staff << \global \includeNotes "partie2" >>
    >>
    \new Staff \withLyrics <<
      \global \keepWithTag #'coro1 \includeNotes "voix"
    >> \keepWithTag #'coro1 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'coro2 \includeNotes "voix"
    >> \keepWithTag #'coro2 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'coro3 \includeNotes "voix"
    >> \keepWithTag #'coro3 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'venere \includeNotes "voix"
    >> \keepWithTag #'venere \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'ercole \includeNotes "voix"
    >> \keepWithTag #'ercole \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2.*8 s1\break s2.*8\pageBreak
        s2.*3\break s2.*9 s1\pageBreak
        s2.*14 s1\break s2.*10 s1\pageBreak
        s2.*9 s1\break
      }
      \modVersion {
        %% sinfonia
        s1.*4 s1 \break s2.
        % v&e
        s2. s1.*2 s2.*3 s1.\break
        %% coro
        s1.*4 s4*7\break
        %% rit
        s1.*7 s1 \break
        %% v&e
        s2.*10 s1\break
        %% coro
        s2.*9 s1\break
        %% rit
      }
    >>
  >>
  \layout { }
  \midi { }
}
