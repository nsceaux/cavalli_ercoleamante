\clef "dessus" r4 r fa'' mi''4. re''8 do'' sib' |
la'2 la''4 sol''4. fa''8 mi'' re'' |
do''2 do'''4 sib''4. la''8 sol'' fa'' |
mi''4 fa''8 sol'' la''4 sib'' mi''4. mi''8 re''1\fermata |
R4*64 |
r2*3/2 si'4 do'' re'' |
mi''4 r r do'' re'' mi'' |
fa''4. mi''8 re'' mi'' do'' re'' si'4. la'8 |
la'2. fad'4 sol' la' |
sib'4 r r mi'4 fa' sol' |
la' r r fa'' sol'' la'' |
sib''4. la''8 sol'' la'' fa'' sol'' mi''4. re''8
re''1\fermata |
R4*65 |
r2*3/2 si'4 do'' re'' |
mi''4 r r do'' re'' mi'' |
fa''4. mi''8 re'' mi'' do'' re'' si'4. la'8 |
la'2. fad'4 sol' la' |
sib'4 r r mi'4 fa' sol' |
la' r r fa'' sol'' la'' |
sib''4. la''8 sol'' la'' fa'' sol'' mi''4. re''8 re''1\fermata |
