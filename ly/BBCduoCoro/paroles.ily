\tag #'(venere ercole basse) {
  Fug -- ga -- no fug -- ga -- no a vol
  dal bell’ im -- pe -- ro
  del nu -- me ar -- cie -- ro
  \tag #'ercole { le pe -- ne el duol }
  le pe -- ne el duol.
}
\tag #'(coro1 coro2 coro3 basse) {
  E in lui co -- sì
  gio -- ie sol pio -- vi -- no,
  e si rin -- no -- vi -- no
  que -- gli au -- rei dì.
}
\tag #'(venere ercole basse) {
  Strug -- ga -- si strug -- ga -- si il giel
  d’o -- gni fie -- rez -- za
  o -- gni a -- ma -- rez -- za
  \tag #'ercole { si can -- gi in miel __ }
  si can -- gi in miel.
}
\tag #'(coro1 coro2 coro3 basse) {
  E in lui co -- sì
  gio -- ie sol pio -- vi -- no,
  e si rin -- no -- vi -- no
  que -- gli au -- rei dì.
}
%{
In -- fe -- lice, e dis -- pe -- ra -- to
men -- tre mes -- tis -- si -- mo
vo notte, e dì,
qual di bene i -- nas -- pet -- ta -- to
rag -- gio pu -- ris -- si -- mo
m’ap -- pa -- rì?

Ah che s’ac -- ceso un cor
av -- vien mai che dis -- pe -- ri,
non sa come i -- n a -- mor
con so -- vra -- no po -- ter for -- tuna im -- pe -- ri,
di tal nume al -- la pos -- san -- za
nulla in -- vin -- ci -- bi -- le
già mai si dà
e -- gli o -- gn’or con gran bal -- dan -- za
fin l’im -- pos -- si -- bi -- le
ce -- der fa.
%}