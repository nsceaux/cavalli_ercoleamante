\clef "viole2" r4 r re' sol' sol2 |
la la4 sib2 sib4 |
do'2 do'4 sol'2 si'4 |
mi'2 re'4 sol' mi'2 fad'1\fermata |
R4*64 |
re'4 do' la si r r |
sol re' sol la r r |
la2 re'4 fa' si2 |
dod'4 re' mi' la r r |
re' la sol do' r r |
la sib do' fa2 fa'4 |
sib2. sib4 la2
la1\fermata |
R4*65 |
re'4 do' la si r r |
sol re' sol la r r |
la2 re'4 fa' si2 |
dod'4 re' mi' la r r |
re' la sol do' r r |
la sib do' fa2 fa'4 |
sib2. sib4 la2 la1\fermata |
