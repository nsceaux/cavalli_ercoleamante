\score {
  \new StaffGroupNoBar \with { \haraKiriFirst } <<
    \new StaffGroupNoBracket <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
      \new Staff << \global \includeNotes "partie1" >>
      \new Staff << \global \includeNotes "partie2" >>
    >>
    \new Staff \with { \tinyStaff \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'basse \includeNotes "voix"
    >> \keepWithTag #'basse { \set fontSize = #-2 \includeLyrics "paroles" }
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \modVersion {
        %% sinfonia
        s1.*4 s1 \break s2.
        % v&e
        s2. s1.*2 s2.*3 s1.\break
        %% coro
        s1.*4 s4*7\break
        %% rit
        s1.*7 s1 \break
        %% v&e
        s2.*10 s1\break
        %% coro
        s2.*9 s1\break
        %% rit
      }
    >>
  >>
  \layout { }
  \midi { }
}
