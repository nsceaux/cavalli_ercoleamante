<<
  \tag #'(venere basse coro1 coro2) \clef "vsoprano"
  \tag #'coro3 \clef "valto"
  \tag #'ercole \clef "vbasse"
>>
R4*28
<<
  \tag #'(venere basse) {
    \original { \clef "vsoprano" <>^\markup\character Venere }
    R2.*2 |
    \pygmalion <>^\markup\character Venere
    la'4. si'8 do''4 re''4. re''8 mi''4 |
    la'2 re''4 re'' dod''2 |
    re''4. do''8 do''4 |
    do'' si'2 |
    si'4. si'8 la'4 |
    do'' si'2 la'2. |
  }
  \tag #'ercole {
    \original\clef "vbasse" <>^\markup\character Ercole
    re4. mi8 fa4 |
    sol4. sol8 la4 |
    re2 la4 la sold2 |
    la4. sol8 fa4 fa mi2 |
    re4. re8 mi4 |
    fa re2 |
    mi2 do4 |
    re mi2 la,2. |
  }
  \tag #'(coro1 coro2 coro3) { R2.*11 }
>>
<<
  \tag #'(coro1 basse) {
    <>^\markup\character Coro di Gratie
    la'4 si' dod'' re''2. |
    si'4 do'' re'' mi''4. mi''8 mi''4 |
    do'' re'' mi'' fa''4.\melisma sol''8[ fa'' mi''] |
    re''4 mi'' fa'' sol''4.\melismaEnd sol''8 fa''4 |
    mi'' mi''4. re''8 re''1\fermata |
  }
  \tag #'coro2 {
    R1. |
    sol'4 la' si' do''2. |
    la'4 si' do'' re''4. re''8 re''4 |
    si' do'' re'' mi''4. mi''8 re''4 |
    re'' re''4. dod''8 re''1\fermata |
  }
  \tag #'coro3 {
    r2*3/2 re'4 mi' fad' |
    sol'2. do'4 re' mi' |
    fa'4. fa'8 fa'4 re' mi' fa' |
    sol'4.\melisma la'8[ sol' fa'] mi'4.\melismaEnd mi'8 fa'4 |
    sol' la' la' re'1\fermata |
  }
  \tag #'(venere ercole) R4*31
>>
R4*46 |
<<
  \tag #'(venere basse) {
    <>^\markup\character Venere
    R1. |
    la'4. si'8 do''4 re''4. re''8 mi''4 |
    la'2 re''4 re'' dod''2 |
    re''4. re''8 do''4 |
    do'' si'2 |
    si'4. si'8 la'4 do'' si'2 la'1\fermata |
  }
  \tag #'ercole {
    <>^\markup\character Ercole
    re4. mi8 fa4 sol4. sol8 la4 |
    re2 la4 la sold2 |
    la4. sol8 fa4 fa mi2 |
    re4. re8 mi4 |
    fa re2 |
    mi4.\melisma re8 do4\melismaEnd re mi4. mi8 la,1\fermata |
  }
  \tag #'(coro1 coro2 coro3) { R4*34 }
>>
<<
  \tag #'(coro1 basse) {
    <>^\markup\character Coro di Gratie
    la'4 si' dod'' re''2. |
    si'4 do'' re'' mi''4. mi''8 mi''4 |
    do'' re'' mi'' fa''4.\melisma sol''8[ fa'' mi''] |
    re''4 mi'' fa'' sol''4.\melismaEnd sol''8 fa''4 |
    mi'' mi''4. re''8 re''1\fermata |
  }
  \tag #'coro2 {
    R1. |
    sol'4 la' si' do''2. |
    la'4 si' do'' re''4. re''8 re''4 |
    si' do'' re'' mi''4. mi''8 re''4 |
    re'' re''4. dod''8 re''1\fermata |
  }
  \tag #'coro3 {
    r2*3/2 re'4 mi' fad' |
    sol'2. do'4 re' mi' |
    fa'4. fa'8 fa'4 re' mi' fa' |
    sol'4.\melisma la'8[ sol' fa'] mi'4.\melismaEnd mi'8 fa'4 |
    sol' la' la' re'1\fermata |
  }
  \tag #'(venere ercole) R4*31
>>
R4*46 |
