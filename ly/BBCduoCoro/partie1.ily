\clef "viole1" r4 r la' mi'2 mi'4 |
do'2 do''4 \ficta sib'?2 mi'4 |
mi'2 fa'4 re'2 re'4 |
la'2 la'4 re' la'2 la'1\fermata |
R4*64 |
fa'4 sol' la' re' r sol' |
mi' fa' sol' do' r la' |
la'2 fa'4 la' mi'2 |
mi'4 si la re' r r |
sib do' re' sol r r |
do' sol fa sib2 do'4 |
re'2 sol'8 dod' re'4 la'4. la'8
fad'1\fermata |
R4*65 |
fa'4 sol' la' re' r sol' |
mi' fa' sol' do' r la' |
la'2 fa'4 la' mi'2 |
mi'4 si la re' r r |
sib do' re' sol r r |
do' sol fa sib2 do'4 |
re'2 sol'8 dod' re'4 la'4. la'8 fad'1\fermata |
