\score {
  <<
    \new ChoirStaff \with { \haraKiriFirst } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'coro1 \includeNotes "voix"
      >> \keepWithTag #'coro1 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'coro2 \includeNotes "voix"
      >> \keepWithTag #'coro2 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'coro3 \includeNotes "voix"
      >> \keepWithTag #'coro3 \includeLyrics "paroles"
    >>
    \new Staff \with { \tinyStaff \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'venere \includeNotes "voix"
    >> \keepWithTag #'venere { \set fontSize = #-2 \includeLyrics "paroles" }
    \new GrandStaff \with { \haraKiriFirst } <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff <<
        \global \includeNotes "dessus2"
        { %% sinfonia
          s1.*4 s1 \break s2.
          % v&e
          s2. s1.*2 s2.*3 s1.\break
          %% coro
          s1.*4 s4*7\break
          %% rit
          s1.*7 s1 \break
          %% v&e
          s2.*10 s1\break
          %% coro
          s2.*9 s1\break
          %% rit
        }
      >>
    >>
  >>
  \layout { }
}
