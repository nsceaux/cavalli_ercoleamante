\clef "dessus" r4 r re'' do''4. sib'8 la' sol' |
fa'2 fa''4 mi''4. re''8 do'' sib' |
la'2 la''4 sol''4. fa''8 mi'' re'' |
dod''4 re''8 mi'' fa''4 re'' re''4. dod''8 re''1\fermata |
R4*64 |
r2*3/2 sol'4 la' si' |
do'' r r la' si' do'' |
re''4. do''8 si' do'' la'4 la'4. sold'8 |
la'2. re'4 mi' fad' |
sol' r r do' re' mi' |
fa' r r re'' mi'' fa'' |
sol''4. fa''8 mi'' fa'' re''4 re''4. dod''8
re''1\fermata |
R4*65 |
r2*3/2 sol'4 la' si' |
do'' r r la' si' do'' |
re''4. do''8 si' do'' la'4 la'4. sold'8 |
la'2. re'4 mi' fad' |
sol' r r do' re' mi' |
fa' r r re'' mi'' fa'' |
sol''4. fa''8 mi'' fa'' re''4 re''4. dod''8 re''1\fermata |
