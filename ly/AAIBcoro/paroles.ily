\pygmalion\tag #'coro11 {
  E ve -- da e ve -- da o -- gn’un, che de -- si -- ar non sa
}
\pygmalion\tag #'(coro22 coro23) {
  E ve -- da e ve -- da o -- gn’un, che de -- si -- ar non sa __
  o -- gn’un, che de -- si -- ar non sa
}
\tag #'(coro11 coro12 coro21 basse) {
  E ve -- da e ve -- da o -- gn’un, che de -- si -- ar non sa __
  \tag #'coro12 { o -- gn’un, }
  che de -- si -- ar non sa
}
\tag #'coro13 {
  E ve -- da e ve -- da o -- gn’un, che de -- si -- ar non sa,
}
\tag #'(coro12 coro13 coro14 coro21 coro22 coro23 coro24) {
  e ve -- da, e ve -- da o -- gn’un, che de -- si -- ar non sa,
}
un e -- roi -- co
\tag #'(coro11 coro12 coro13) { va -- lo -- re, }
va -- lo -- re
\tag #'(coro12) { qui giù, }
qui giù \tag #'(coro13 coro21 coro23) { pre -- mio, } pre -- mio mag -- gio -- re
che di ve -- der in pa -- ce al -- ma, al -- ma bel -- tà.
