\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \keepWithTag #'coro14 \includeNotes "voix"
    >> \keepWithTag #'coro14 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'coro24 \includeNotes "voix"
    >> \keepWithTag #'coro24 \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
    >>
  >>
  \layout { indent = \noindent }
}
