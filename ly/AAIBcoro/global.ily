\key do \major \midiTempo#200
\digitTime\time 3/4
\set Score.currentBarNumber = #95 \bar ""
s2.
\measure 6/4 s1.*4
\measure 3/4 s2.*2
\measure 6/4 s1.*4
\measure 3/4 s2.*2
\measure 6/4 s1.*4
\measure 7/4 s4*7 \bar "|."
\endMarkSmall\markup { Qui viene il Balletto Reale, e poi Finito segue }
