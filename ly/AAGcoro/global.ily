\key do \major
\digitTime\time 3/4 \midiTempo#120 s2.*2
\measure 6/4 s1.*2
\measure 3/4 s2.
\measure 9/4 s2.*3
\time 4/4 s1*2
\measure 2/1 s1*2
\digitTime\time 3/4 s2.
\measure 6/4 s1.
\measure 3/4 s2.*2
\measure 6/4 s1.
\measure 3/4 s2.*4
\time 4/4 \measure 2/1 s1*4 \bar "|."
