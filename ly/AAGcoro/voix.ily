<<
  \tag #'(coro11 basse) {
    <>^\markup\character Coro di Fiumi
    \clef "vsoprano" r4 mi'' mi'' |
    mi''4. mi''8 mi''4 |
    re''2. re'' |
    do'' r4 r8 do'' sol' la' |
    si'4 si' re''8 do'' |
    re''2 mi''4 re''2. re'' |
    r4 mi''8 mi'' si'4 si'8 si' |
    si'2 si' |
    si'4 si'8 si' do''2 si'1 |
    sold'4 sold'8 la' si' sold' |
    do''2 do''4 si'4. si'8 do''4 |
    si'4 si' mi'' |
    mi'' mi''8 re'' dod'' mi'' |
    re''2 re''4 dod''4. dod''8 re''4 |
    re'' re'' dod'' |
    re'' re''8 re'' re'' re'' |
    si'2. |
    mi''4 do''8 do'' do'' do'' |
    la'2 r4 la' mi''2 do'' |
    si'4\melisma do''2 si'4\melismaEnd do''1\fermata |
  }
  \tag #'coro12 {
    \clef "valto" r4 sol' sol' |
    sol'4. sol'8 sol'4 |
    sol'2. sol' |
    sol' r4 r8 sol' si' fad' |
    sol'4 sol' la'8 mi' |
    la'2 mi'4 la'2. sol' |
    r4 sold'8 sold' sold'4 sold'8 sold' |
    sold'2 sold' |
    sold'4 sold'8 sold' la'2 sold'1 |
    si'4 si'8 la' sold' si' |
    la'2 la'4 sold'4. sold'8 la'4 |
    la'4 la' sold' |
    la' la'8 la' la' la' |
    la'2 la'4 la'4. la'8 la'4 |
    sib'4 la' la' |
    fad'4 fad'8 sol' la' fad' |
    sol'2. |
    sol'4 mi'8 fa' sol' mi' |
    fa'2 r4 fa' sol'2 la' |
    sol'1 mi'\fermata |
  }
  \tag #'coro13 {
    \clef "vtenor" r4 do' do' |
    do'4. do'8 do'4 |
    si2. si2 si4( |
    mi'2.) r4 r8 mi' re' do' |
    re'4 re'4. sol8 |
    fad[ sol] la2 la4 la2 si2. |
    r4 si8 si mi'4 mi'8 mi' |
    mi'2 mi' |
    mi'4 mi'8 mi' do'4( re') mi'1 |
    mi'4 mi'8 mi' mi' mi' |
    mi'2 mi'4 mi'4. mi'8 mi'4 |
    fa'4 si si |
    dod'4 dod'8 re' mi' dod' |
    fa'2 fa'4 mi'4. mi'8 re'4 |
    sol' mi' mi' |
    re' la8 la la re' |
    re'4 re' sol |
    sol4 sol8 sol sol do' |
    do'2 r4 do' do'2 fa' |
    re'4 do' re'( sol) sol1\fermata |
  }
  \tag #'coro14 {
    \clef "vbasse" r4 do do |
    do4. do8 do4 |
    sol2. sol2 sol4( |
    do'2.) r4 r8 do' si la |
    sol4 sol fa8 mi |
    re2 do4 re2. sol |
    r4 mi8 mi mi4 mi8 mi |
    mi2 mi |
    mi4 mi8 mi la,2 mi1 |
    mi4 mi8 fad sold mi |
    la2 la4 mi4. mi8 la4 |
    re4 mi mi |
    la la8 si dod' la |
    re'2 re'4 la4. la8 fa4 |
    sol la la |
    re re8 mi fad re |
    sol2. |
    do4 do8 re mi do |
    fa2 r4 fa do2 do |
    sol1 do\fermata |
  }
  \tag #'coro21 {
    \clef "vsoprano" r4 do'' do'' |
    do''4. do''8 sol'4 |
    si'2. si' |
    R1. |
    r4 r8 si' la' sol' |
    la'2 do''4 la'( re''2) si'2. |
    r4 si'8 si' mi''4 mi''8 mi'' |
    mi''2 mi'' mi''4 mi''8 mi'' mi''2 mi''1 |
    R2. |
    do''4 do''8 re'' mi'' mi'' mi''4. mi''8 mi''4 |
    re''8[ la'] mi''2 |
    dod''2. |
    re''4 re''8 do'' la' re'' mi''4. la'8 re''4 |
    mi'' mi''( la') |
    la'8 la' re''2 |
    re''4 re''8 do'' si' re'' |
    do''2 do''4 |
    do''2 r4 do'' do''2 r4 la' |
    re'' mi'' re''2 mi''1\fermata |
  }
  \tag #'coro22 {
    \clef "valto" r4 mi' mi' |
    mi'4. mi'8 mi'4 |
    sol'4.\melisma fad'8[ sol' la'] sol'2\melismaEnd sol'4 |
    r4 r do'( sol'2.) |
    r4 r8 sol' re' mi' |
    fad'2 sol'4 sol'2 fad'!4( sol'2.) |
    r4 mi'8 mi' mi'4 mi'8 mi' |
    mi'2 mi' |
    si4 si8 mi' mi'4( fad') sold'1 |
    R2. |
    mi'4 mi'8 re' do' mi' mi'4 si mi'8[ do'] |
    fa'4 mi'2 |
    mi'8 mi' la'2 |
    la'4 la'8 sol' fa' la' la'4. mi'8 fa'4 |
    mi' mi'2 |
    fad'8 fad' la'2 |
    sol'4 sol'8 sol' sol' sol' |
    mi'2 mi'4 |
    do'2 r4 do' mi'2 la |
    si4 sol sol'2 sol'1\fermata |
  }
  \tag #'coro23 {
    \clef "vtenor" r4 sol sol |
    sol4. sol8 do'4 |
    re'2. re' |
    <<
      \original { R1. | }
      \pygmalion { r4 r do'( sol'2.) | }
    >>
    r4 r8 re' fa' do' |
    do'([ si16 la] re'4) sol re'2. re' |
    r4 sold8 sold si4 si8 si |
    si2 si |
    sold4 sold8 si la2 si1 |
    R2. |
    do'4 do'8 si la la si4 sold do'8[ la] |
    re'8[ do'] si2 |
    la8 dod' mi'2 |
    fa'4 fa'8 mi' re' fa' dod'4. dod'8 la4 |
    re' la2 |
    la r4 |
    si4 si8 do' re' si |
    do'2 sol4 |
    la2 r4 la sol2 do' |
    sol sol8 re re'4 do'1\fermata |
  }
  \tag #'coro24 {
    \clef "vbasse" r4 do do |
    do4. do8 do4 |
    sol2. sol |
    R1. |
    r4 r8 sol fa mi |
    re2 do4 re2. sol, |
    r4 mi8 mi mi4 mi8 mi |
    mi2 mi |
    mi4 mi8 mi la,2 mi1 |
    R2. la,4 la,8 si, do la, mi4. mi8 do4 |
    re4 mi2 |
    la,2. |
    re4 re8 mi fa re la4. la8 fa4 |
    sol la2 |
    re r4 |
    sol,4 sol,8 la, si, sol, |
    do2 do4 |
    fa,2 r4 fa,4 mi,2 fa, |
    sol,1 do\fermata |
  }
>>
