Do -- po bel -- li -- che no -- ie
\tag #'(coro11 coro12 coro13 coro14 basse) {
  oh oh che so -- a -- vi
  che so -- a -- vi gio -- ie!
}
\tag #'(coro21 coro22 coro23 coro24) {
  \pygmalion \tag #'coro23 oh
  \tag #'coro22 oh
  oh che so -- a -- vi gio -- ie!
}
A dol -- cez -- ze sì ra -- re ol -- tre o -- gni se -- gno
Gal -- lia di -- la -- ta il cor, non men,
\tag #'(coro11 coro12 coro13 coro14 basse) { non men, }
ch’il re -- gno,
\tag #'(coro22 coro23) { non men, }
Gal -- lia di -- la -- ta il cor, non men,
\tag #'(coro11 coro12 coro13 coro14 basse) { non men, }
ch’il re -- gno,
\tag #'(coro21 coro22) { non men, }
Gal -- lia di -- la -- ta il cor,
\tag #'(coro11 coro12 coro13 coro14 basse) {
  Gal -- lia \tag #'coro13 { Gal -- lia } di -- la -- ta il cor,
}
\tag #'(coro13 coro21 coro22 coro23 coro24) { non men, }
\tag #'(coro21 coro22) { non men, }
non men
\tag #'coro23 { ch’il re -- gno, }
ch’il re -- gno.
