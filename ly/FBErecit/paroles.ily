Ma qual pun -- gen -- te ar -- su -- ra
la mia ru -- vi -- da scor -- za in -- tor -- no as -- sa -- le?
Qual in -- co -- gni -- to ma -- le
d’of -- fen -- der -- mi te -- men -- do
ser -- pe nas -- co -- so per le ve -- ne al co -- re?
Qual im -- men -- so do -- lo -- re, ahi, mi con -- qui -- de?
E per dar mor -- te a me tan -- to più du -- ra
in vis -- ta de’ con -- ten -- ti, oh dio, m’uc -- ci -- de?
E tu lo sof -- fri, o ge -- ni -- to -- re? E las -- ci,
ch’io, che con piè te -- mu -- to
pas -- seg -- giai del -- la mor -- te i re -- gni il -- le -- so,
e che fin dal -- la cu -- na
di bel -- le glo -- rie a -- dor -- ni
tut -- ti con -- tai del -- la mia vi -- ta i gior -- ni,
hor sen -- za ha -- ve -- re a fron -- te
di me de -- gno ne -- mi -- co (ah rio mar -- ti -- re,
che del -- la mor -- te an -- cor vie più m’ac -- co -- ra)
in o -- zio vil qui mo -- ra?
Sen -- za che glo -- ria al -- cu -- na
ren -- da al -- men di me de -- gno il mio mo -- ri -- re. __
Al -- men al -- men di nu -- bi os -- cu -- re
ve -- la quest’ a -- ria in tor -- no
sì che sor -- te ma -- li -- gna
di me gra -- to spet -- ta -- co -- lo non fac -- cia
all’ im -- pla -- ca -- bil mia cru -- da ma -- tri -- gna;
e per quan -- do la tu -- a
in -- sen -- sa -- ta pi -- gri -- zia, oh gran to -- nan -- te
il con -- quas -- so des -- ti -- na
dell’ u -- ni -- ver -- so, ohi -- mè, s’ho -- ra no’l fa -- i?
E a che ri -- ser -- bi il cie -- lo?
Che nel per -- der Al -- ci -- de a per -- der va -- i?
