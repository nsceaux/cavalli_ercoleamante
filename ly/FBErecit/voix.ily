\clef "vbasse" <>^\markup\character Ercole
r2 sol4 r |
do'8 sol sol sol mi4 mi |
mi8 mi mi fa16 sol do8 do16 re mi8 do |
sol4 sol r2 |
re'8 la la la16 la fa8 fa16 fa fa fa fa la |
re8 re re do16 re sib,8 sib, sib,16 sib, do re |
la,4 la, r2 |
r4 la8 do' la4 la8 la |
la[ sold] sold4 re'4 sold!8 la |
la4 la r2 |
r4 sib sib8 sib sib la |
la2 la4 la8 si |
dod'4 dod'8 dod' dod' dod' dod' re' |
re'4 re'8 re' sol4. fa8 |
re re r4 r2 |
r8 la la sib sib4 sib |
do' do'8 sol la4 la8 re' |
sib4 sib sib r |
sib8 sib sib la sib sib sib sib |
sib4 sib8 sib sib sib16 sib sib8 la |
do'4 do' do'8 do' do' do'16 do' |
do'4 do'8 do' do' do' do' sib |
do'4 do' do' do'8 re' |
sib4. sib8 sib sib sib la |
la4 la8 re' la la la la |
la la la la la4 la8 la |
fa fa sib2 lab8 lab |
lab4 lab8 lab lab lab lab lab |
lab lab lab sol sol4 sol8 \ficta la |
la la la la fad4 fad |
sib8 sol16 sol sol8 sol sol4 sol |
lab4 lab lab lab8 lab |
lab4 lab r8 sol sol sol |
sol2. fad4( sol1) |
r8 sol do' si do' do' do' do' |
sol4 sol sol8 sol16 sol sol8 sol |
mi4 mi mi r8 mi |
mi4 mi8 mi do4 do8 do |
do4 do8 do16 do do8. do16 do8 re |
re4 re8 re re re re re |
sol4 mi8 mi16 mi do8 do r4 |
r2 r4 do'8 re' |
sib4 sib8 sib sib sib sib sib |
sib4 sib8 la sib sib do'4~ |
do' la8 sib sib4 sib |
re'8 sib sol fad16 la re8 re r16 do sib, re |
sol,4 sol,8 sol mib4 re8 re16 do |
re4 re r r8 la |
do' do' do' re' sib4 sib |
sib8 sib sib sib16 do' do'4 do'8 re' |
re'4. sol8 la4 la |
