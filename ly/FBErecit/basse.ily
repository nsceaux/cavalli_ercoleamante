\clef "basse" do1 |
do~ |
do |
sol, |
re~ |
re2 sib, |
la,1 |
la |
sib |
la |
sol~ |
sol2 fa~ |
fa mi |
re sib,~ |
sib,4 sol, la,2 |
re, sol |
mib re |
sol1 |
sol |
sol |
mi~ |
mi |
mi~ |
mi |
fa~ |
fa |
re~ |
re~ |
re2 mib |
do re |
sib,1 |
do~ |
do2 si,4 do |
re1 sol, |
do~ |
do~ |
do~ |
do |
la, |
fa, |
sol, |
do |
sol, |
sol, |
fad,2 sol, |
sol,1 |
sol,2. mib4 |
re1~ |
re2 sol~ |
sol mib~ |
mib re |
