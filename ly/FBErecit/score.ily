\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*5\break s1*4\pageBreak
        s1*5\break s1*5\break s1*4\break s1*4\break s1*4\pageBreak
        s1*5\break s1*4\break s1*4\break s1*4\break s1*4\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
