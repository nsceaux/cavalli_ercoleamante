E tu a che pen -- si Io -- le?

A l’er -- ror mi -- o,
se ben ciò che mia lin -- gua
dis -- se pur dian -- zi ah non, non lo dis -- s’i -- o.
E l’al -- ma for -- sen -- na -- ta,
nel fre -- ne -- ti -- co er -- ro -- re
al -- tra par -- te non eb -- be
che di gran pen -- ti -- men -- to al -- to do -- lo -- re. __

\original {
  Deh non vo -- le -- re, o bel -- la,
  far con tai sen -- ti -- men -- ti
  d’Hyl -- lo più gra -- ve il fal -- lo,
  e le giu -- ste i -- re mie tan -- to più ar -- den -- ti;
  di nuo -- vo qui me -- co t’af -- fi -- di, e pen -- sa,
  pen -- sa me -- glio al tuo di -- re,
  ch’or con ri -- gi -- de vo -- glie, or con in -- fi -- de,
  trop -- po trop -- po è ten -- tar di sof -- fe -- ren -- za Al -- ci -- de.

  Ah chi sì tos -- to in -- vo -- la
  all’ at -- to -- ni -- ta men -- te
  l’im -- pres -- si -- on più ca -- re? e del mio se -- no
  la più te -- ne -- ra par -- te
  per te di stra -- no af -- fet -- to
  con re -- ci -- di -- va d’in -- cos -- tan -- za im -- pri -- me?
  Chi l’av -- ver -- so mio cor suol -- ge ad a -- ma -- re?
  Ah che tra miei pen -- sie -- ri
  più non ne tro -- vo al -- cu -- no
  ch’i -- do -- la -- tra non sia de’ tuoi de -- si -- ri,
  ah che non spi -- ro più chi tuoi res -- pi -- ri. __
  
  E pur po -- tran -- no in bre -- ve
  dell’ is -- ta -- bil tuo spir -- to
  le so -- li -- te vi -- cen -- de
  ri -- can -- giar tan -- to a -- mo -- re
  in più cru -- do ri -- go -- re. __
  
  Ciò non te -- mer, che so -- no
  sì for -- te -- men -- te ran -- no -- da -- ti, e stret -- ti
  i lac -- ci ond’ è di nuo -- vo
  per te quest’ al -- ma av -- vol -- ta,
  che più co -- me scam -- par -- ne el -- la non ve -- de, __
  chie -- di chie -- di qual pe -- gno vuoi del -- la mia fe -- de.
}

Dun -- que su dun -- que su di tua ma -- no
per fer -- mez -- za a -- mo -- ro -- sa
pe -- gno por -- gi -- mi por -- gi -- mi sol d’es -- ser mia spo -- sa.

No’l ri -- fiu -- to, ma las -- cia,
ch’in se -- gre -- te pre -- ghie -- re
del ge -- ni -- to -- re a l’ol -- trag -- gia -- to spir -- to
per ad -- dol -- cir -- lo in qual -- che gui -- sa al -- me -- no
pri -- ma, ch’af -- fat -- to a te mi do -- ni in pre -- da,
io li -- cen -- za ne chie -- da.
\original {
  Pur che ciò sia sol ce -- ri -- mo -- nia al ven -- to
  sì, sì, sì, sì, ne son con -- ten -- to
  sì, sì, sì, sì, ne son con -- ten -- to.
}

