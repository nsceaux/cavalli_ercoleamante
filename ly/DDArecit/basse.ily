\clef "basse" re2. sib,4 |
la,2~ la, |
re1 |
re |
la,2 mi |
la,1 |
fa,~ |
fa,2 sib,~ |
sib,4 la, sol,2~ |
sol, la, |
fa,4 sol,2 sold,4 |
la,1 re\fermata |
\original {
  re1~ |
  re |
  re |
  re |
  sol2 fa |
  mib2. do4 |
  re2 sol, |
  re1~ |
  re |
  dod |
  re~ |
  re2 sol,~ |
  sol, fad,~ |
  fad,4 sol, la,2 re1\fermata |
  sol1 |
  sol2 fa~ |
  fa mi |
  mi do |
  la,1 |
  sol, |
  fa,~ |
  fa,2 mi, |
  mi1 |
  fa |
  mi |
  dod |
  dod |
  \ficta dod |
  re |
  fad,2 sol,~ |
  sol, la, re1\fermata | \allowPageTurn
  re1 |
  do |
  sib, |
  la, |
  sol,2 fa,4 sib, |
  do2 fa, |
  fa1~ |
  fa |
  fa |
  re~ |
  re |
  do2. si,4 |
  la,2 fa,4 re, |
  mi,1 |
  la,2 fad, |
  sol,4 la, sib,4. sol,8 |
  la,1 re\fermata |
}
re2 re4 |
la2 la4 |
re'2. do' |
sib1~ |
sib |
fad2 fad4 sol2 fad!4 |
sol4. fad8 mi4 |
re2. do2 do4 re2. sol,1*3/4\fermata |
sol,1 |
sol |
fa~ |
fa2 mi! |
mib1~ |
mib2 re~ |
re do |
sib, la, |
sol, la, re1\fermata |
\original {
  re1~ |
  re2 sol4. sol8 |
  do2 do4 |
  fa4. do8 re4 |
  la, mi2 |
  la, la4 |
  re2 re4 |
  la4. fa8 sol4 |
  re la2 re1*3/4\fermata | 
}
