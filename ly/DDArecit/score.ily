\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*4\pageBreak
        s1*3 s2 \bar "" \break s2 s1*5\break s1*4\break
        s1*5\break s1*3 s2 \bar "" \pageBreak
        s2 s1*3 s2 \bar "" \break s2 s1*3\break s1*5\break s1*4\break s1*4\pageBreak
        s1*4\break s1*4\break s1*4\break s1*6\break s2.*4 s1*2 s2.*2\pageBreak
        s2.*5 s1*2\break s1*4\break s1*4\break s1*2 s2.*4\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
