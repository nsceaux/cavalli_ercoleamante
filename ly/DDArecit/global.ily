\key do \major
\time 4/4 \midiTempo#80 s1*11 \measure 2/1 s1*2 \bar "|."
\original {
  \measure 4/4 s1*13 \measure 2/1 s1*2 \bar "|."
  \measure 4/4 s1*16 \measure 2/1 s1*2 \bar "|."
  \measure 4/4 s1*6 \bar "|."
  \measure 4/4 s1*10 \measure 2/1 s1*2 \bar "|."
}
\digitTime\time 3/4 \midiTempo#160 s2.*2 \measure 6/4 s1.
\time 4/4 s1*2
\digitTime\time 3/4 \measure 6/4 s1.
\measure 3/4 s2.
\measure 12/4 s2.*4 \bar "|."
\time 4/4 \midiTempo#80 s1*8 \measure 2/1 s1*2 \bar "|."
\original {
  \measure 4/4 s1*2
  \digitTime\time 3/4 s2.*6 \measure 6/4 s1. \bar "|."
}
