\ffclef "vbasse" <>^\markup\character Ercole
r4 r8 la fa sol16 la16 re8. mi16 |
mi8 mi r4
\ffclef "vbas-dessus" <>^\markup\character Iole
r8 la' sol' la' |
fa'4 fa' r re' |
la'4 la'8 la'16 la' la'8 la' la' la'16 si' |
do''4 do''8 mi'' do''4 si'8 si'16 do'' |
la'4 la' r do'' |
do''8 do'' do'' do'' do''4 do'' |
do''8 do'' do'' do''16 re'' sib'8 sib' sib' sib' |
sib'4 do''8 sib' re'' re'' sib' sib' |
sol'4 mi'8 mi' dod'4 dod' |
fa''4 mi''8 sol'' dod''4\melisma re''~ |
re''2.\melismaEnd dod''4\melisma re''1\fermata\melismaEnd |
\original {
  \ffclef "vbasse" <>^\markup\character Ercole
  r4 la2 fad8 fad |
  fad4 fad8 mi fad4 fad |
  fad8 fad fad fad16 sol la4 la |
  la4 la8 do' sib4 sib8 la |
  sib4 sib r16 la la la la8 sib! |
  do'2 sol4 la8 sib |
  sol4 sol r2 |
  r8 fa fa4 fa8 fa fa fa16 fa |
  re4 re r re |
  la la la8 mi mi mi16 fa |
  fa4 fa fa8 fa fa fa16 sol |
  la8 la la sib16 do' sib4 sib |
  sib8 sib sib sib16 do' re'8 la la sol |
  fad8 fad r re la2 re1\fermata |
  \ffclef "vbas-dessus" <>^\markup\character Iole
  r4 re'' r8 do''16 re'' si'8 la' |
  si'16 si' si' si' si'8 si'16 do'' la'4 la'8 la' |
  la'8 la' la' sold' sold'4 sold' |
  r8 sold' sold' la' la' la' la' la' |
  fa'4 fa'8 mi' fa'4 fa'8 la' |
  mi'8 mi' mi' mi' mi'4 mi'8 mi' |
  mi' mi' mi'4 mi' mi'8 re' |
  re'4 re'8 mi' mi'4 mi' |
  r4 si'8 si' si'4 si'8 do'' |
  re''2 la'4 la'8 sold' |
  si'4 si' r mi'' |
  r8 la'16 la' la'8 la' la'4 la' |
  la'8 la'16 la' la'8 sib' sol' sol' sol' sol' |
  sol'4 sol'8 sol' sol' sol' sol' la' |
  fa'4 fa' r fa'' |
  r8 re''16 do'' do''8 re'' si' si' dod'' re'' |
  re''2. dod''4( re''1) |
  \ffclef "vbasse" <>^\markup\character Ercole
  r4 re re8 re re re |
  sol la mi mi mi4 mi8 fa |
  sol4 sol8 sol sol8. sol16 sol8 fa |
  fa fa fa fa fa4 fa8 fa |
  sib sib sib la la4 mi8 fa |
  fa4. mi8( fa2) |
  \ffclef "vbas-dessus" <>^\markup\character Iole
  la'4 la'8 do'' la'4. la'8 |
  la'8 la'16 la' la'8 sol' la' la' la' la' |
  la'4 la'8 si' si'4 si'8 re'' |
  si'4 si' si' si'8 si' |
  si'4 si'8 si' si' si' si'8. do''16 |
  do''4 do''8 do'' do''4 re''8 do''16 re'' |
  mi''4 mi'' re'' do''8 la' |
  la'2. sold'4( |
  la') la'8 si' do'' do''16 do'' do''8 re'' |
  sib'4. la'8 sol' re' fa'4( |
  mi'1) re'\fermata |
}
\ffclef "vbasse" <>^\markup\character Ercole
r4 re4. re8 |
la4 la4. la8 |
re'4 re'4. re'8 do'2 do'4 |
sib4 sib sib sib8 sib |
\ficta sib2 sib |
r4 fad fad sol4. sol8 fad!4 |
sol4. fad8 mi4 |
re2. do4. si,8 do4 re2. sol,1*3/4\fermata |
\ffclef "vbas-dessus" <>^\markup\character Iole
r4 sib'8 sib' sol' sol' r sol' |
mib'4 mib' mib'8 mib' mib' mib'16 re' |
re'4 re'8 re' re' re' re' re' |
re' re' re' re' sol'4 sol'8 sol' |
sol' fad' sol'4 sol'8 la' la' la' |
la'4 la'8 la' fad'4 fad' |
fad'8 fad'16 fad' fad'8 sol' la' fad'! fad' fad'16 sol' |
sol'4 sol' la'4. sib'8 |
sib'4 sib'8 la' la'2 la'1\fermata |
\original {
  \ffclef "vbasse" <>^\markup\character Ercole
  re4 re8 re re4. re8 |
  re re re re16 re sol8 sol r sol |
  do'2 do4 |
  fa4. do8 re4 |
  la, mi2 |
  la, la4 |
  re'2 re4 |
  la4. fa8 sol4 |
  re la2 re1*3/4\fermata |
}
