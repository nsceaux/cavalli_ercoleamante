\clef "dessus" R2. |
la'8 la' re''4 mi'' |
fad'' sol'' re'' |
mi''2 sol'' sol''4. fad''8 |
sol''2 mi''8 re'' |
dod''4 re'' mi'' |
la'2 la''4 |
sol''2 fad'' mi'' fad''1*3/4\fermata |
