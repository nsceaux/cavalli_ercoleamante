\clef "viole2" R2. |
re'8 do' si4 do' |
re' sol sol |
do'2 do' re' |
sol2. |
mi'8 mi' fad'4 mi' |
mi' re' la |
si2 si mi' re'1*3/4\fermata |
