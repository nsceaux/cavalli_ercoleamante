\clef "dessus" r4 r la''8 sol'' |
fad''4 sol'' la'' |
re''2 si''4 |
la''2 la'' la'' |
sol''2. |
mi''8 mi'' la''4 sol'' |
mi'' fad'' re'' |
si'2 re'' re''4. dod''8 re''1*3/4\fermata |
