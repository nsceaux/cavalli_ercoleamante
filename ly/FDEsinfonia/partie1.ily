\clef "viole1" R2.*3 |
r2*3/2 r4 r re'8 do' |
si4 dod' re' |
la2 si8 si |
dod'4 la fad' |
re'2 re' la' la'1*3/4\fermata |
