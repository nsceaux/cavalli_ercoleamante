\tag #'(voix1 voix2 voix3 voix4) {
  Su, su, guer -- rie -- ri,
  all' ar -- mi all' ar -- mi!
  all' ar -- mi all' ar -- mi!
  all' ar -- mi all' ar -- mi!
  all' ar -- mi!  
  Su, su, all' ar -- mi, su, su!
  Su, su, all' ar -- mi, su, su!
  all' ar -- mi all' ar -- mi!
  Su, su, all' ar -- mi, su, su!
  Su, su, all' ar -- mi, all' ar -- mi, su, su!
  Su, su!
  
  La vostr’ al -- ta vir -- tù
  hog -- gi non si
  \tag #'(voix1 voix3 voix4) { ri -- spar -- mi. }
  \tag #'voix2 { ri -- spar -- mi. __ }
}
\tag #'(voix5 voix6 voix7 voix8) {
  Su, su, guer -- rie -- ri,
  all' ar -- mi all' ar -- mi!
  all' ar -- mi all' ar -- mi!
  all' ar -- mi!
  all' ar -- mi!
  Su, su, all' ar -- mi, su, su!
  Su, su, all' ar -- mi, su, su!
  all' ar -- mi all' ar -- mi!
  Su, su, all' ar -- mi, su, su!
  Su, su!
  Su, su, all' ar -- mi, su, su!
  Su, su!
  
  La vostr’ al -- ta vir -- tù
  hog -- gi non si ri -- spar -- mi.
}
\tag #'(voix9 voix10 voix11 voix12) {
  Su, su, all' ar -- mi, su, su!
  Su, su, all' ar -- mi, su, su!
  Su, su, all' ar -- mi, su, su!
  all' ar -- mi all' ar -- mi!
  Su, su, all' ar -- mi, su, su!
  Su, su, all' ar -- mi, su, su!
  
  Quell' em -- pio si dis -- ar -- mi,
  deh, non s'in -- du -- gi più!
  
  Su, su, all' ar -- mi, su, su!
  La vo -- stra al -- ta vir -- tù
  og -- gi non si ri -- spar -- mi.
  Su, su, guer -- rie -- ri, all' ar -- mi!
}
