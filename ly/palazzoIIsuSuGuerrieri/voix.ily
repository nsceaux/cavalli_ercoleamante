<<
  \tag #'voix1 {
    \clef "vsoprano" sib'2 mib'' mib'' |
    \ficta mib''1 mib''2 |
    R1.*2 |
    sol''2 mib'' mib'' |
    \ficta mib''1 mib''2 |
    sol''2 mib'' mib'' |
    \ficta mib''1 mib''2 |
    sol'' \ficta mib'' mib'' |
    \ficta mib''1. |
    \ficta mib''1 mib''2 |
    \ficta mib''\breve*3/4 |
    \ficta mib''1. |
    R1. |
    r2 sib' sib' |
    mib'' mib'' sol'' |
    mib''1. |
    r2 mib'' mib'' |
    sol'' sol'' mib'' |
    sib'1. |
    R1. |
    mib''2 sol'' mib'' |
    mib''2.\melisma re''4 mib'' fa'' |
    sol''1.\melismaEnd |
    mib'' |
    r2 mib'' mib'' |
    sol'' sol'' mib'' |
    sib'1. |
    r2 mib''1 |
    sol''1. |
    mib''2 mib'' mib'' |
    \ficta mib''1. |
    sib'2 mib''1 |
    sol''1. |
    R1.*4 |
    r1*3/2 r2 sib' sib' |
    R1.*3 |
    r2 re'' fa'' |
    fa''2. fa''4 fa''2 |
    mib''1. |
    re''1 re''2 |
    mib'' mib'' mib'' |
    fa''1. |
    mib'' |
  }
  \tag #'voix2 {
    \clef "vsoprano" mib''2 sib' sib' |
    sib'1 sib'2 |
    R1.*2 |
    mib''2 sib' sib' |
    sib'1 sib'2 |
    mib''2 sib' sib' |
    sib'1 sib'2 |
    mib'' sib' sib' |
    sib'1. |
    sib'1 sib'2 |
    sib'\breve*3/4 |
    sib'1. |
    R1. |
    r2 mib'' mib'' |
    sol'' sol'' mib'' |
    sib'1. |
    r2 sib' sib' |
    mib'' mib'' sol'' |
    mib''1. |
    R1. |
    sib'2 mib'' sol'' |
    sol''2.\melisma fa''4 \ficta mib'' re'' |
    \ficta mib''1.\melismaEnd |
    sib' |
    r2 sib' sib' |
    mib'' mib'' sol'' |
    mib''1. |
    r2 sol''1 |
    mib''1. |
    sib'2 sib' sib' |
    sib'1. |
    sol'2 sol''\melisma mib''\melismaEnd |
    \ficta mib''1. |
    R1.*4 |
    r1*3/2 r2 \ficta mib'' mib'' |
    R1.*3 |
    r2 fa'' re'' |
    sib'2. sib'4 do''2 |
    do''1. |
    sib'1 sib'2 |
    sib' sib' mib'' |
    \ficta mib''1 re''2\melisma |
    mib''1.\melismaEnd |
  }
  \tag #'voix3 {
    \clef "vsoprano" sol'2 sol' sol' |
    sol'1 sol'2 |
    R1.*2 |
    sib'2 sol' sol' |
    sol'1 sol'2 |
    sib'2 sol' sol' |
    sol'1 sol'2 |
    sib' sol' sol' |
    sol'1. |
    sol'1 sol'2 |
    sol'\breve*3/4 |
    sol'1. |
    R1. |
    r2 sol' sol' |
    sib' sib' sib' |
    sol'1. |
    r2 sol' sol' |
    sib' sib' sib' |
    sol'1. |
    R1. |
    sol'2 sib' sib' |
    sib'2.\melisma la'4 sol' la' |
    sib'1.\melismaEnd |
    sol' |
    r2 sol' sol' |
    sib' sib' sib' |
    sol'1. |
    r2 sib'1 |
    sib'1. |
    sol'2 sol' sol' |
    sol'1. |
    \ficta mib'2 sib'1 |
    sib'1. |
    R1.*4 |
    r1*3/2 r2 sol' sol' |
    R1.*3 |
    r2 sib' sib' |
    do''2. do''4 la'2 |
    sol'1. |
    sol'1 sol'2 |
    sol' sol' sol' |
    sib'1. |
    sol' |
  }
  \tag #'voix4 {
    \clef "vhaute-contre" mib'2 mib' mib' |
    mib'1 mib'2 |
    R1.*2 |
    \ficta mib'2 mib' mib' |
    \ficta mib'1 mib'2 |
    \ficta mib'2 mib' mib' |
    \ficta mib'1 mib'2 |
    \ficta mib' mib' mib' |
    \ficta mib'1. |
    \ficta mib'1 mib'2 |
    \ficta mib'\breve*3/4 |
    \ficta mib'1. |
    R1. |
    r2 mib' mib' |
    \ficta mib' mib' mib' |
    \ficta mib'1. |
    r2 \ficta mib' mib' |
    \ficta mib' mib' mib' |
    \ficta mib'1. |
    R1. |
    mib'2 mib' mib' |
    \ficta mib'2.\melisma fa'4 sol' fa' |
    mib'1.\melismaEnd |
    \ficta mib' |
    r2 \ficta mib' mib' |
    \ficta mib' mib' mib' |
    \ficta mib'1. |
    r2 mib'1 |
    \ficta mib'1. |
    \ficta mib'2 mib' mib' |
    \ficta mib'1. |
    \ficta mib'1 mib'2 |
    \ficta mib'1. |
    R1.*4 |
    r1*3/2 r2 mib' mib' |
    R1.*3 |
    r2 sib2 sib |
    fa'2. fa'4 fa'2 |
    do'1. |
    sol1 sol2 |
    sol' mib' mib' |
    sib1. |
    mib' |
  }
  
  \tag #'voix5 {
    \clef "vsoprano" R1.*2 |
    sib'2 mib'' mib'' |
    \ficta mib''1 mib''2 |
    R1. |
    sol''2 mib'' mib'' |
    \ficta mib''1 mib''2 |
    sol'' \ficta mib'' mib'' |
    \ficta mib''1 mib''2 |
    \ficta mib'' mib''1 |
    \ficta mib'' mib''2 |
    \ficta mib''\breve*3/4 |
    \ficta mib''1. |
    R1.*2 |
    r2 sib' sib' |
    mib'' mib'' sol'' |
    mib''1. |
    r2 sib' sib' |
    mib'' mib'' sol'' |
    mib''1. |
    R1. |
    mib''2 sib' mib'' |
    \ficta mib''2.\melisma re''4 mib'' fa'' |
    sol''1.\melismaEnd |
    mib'' |
    r2 mib'' mib'' |
    sol'' sol'' mib'' |
    sib'1. |
    r2 mib''1 |
    sol''1. |
    r2 mib'' mib'' |
    sol'' mib'' sib' |
    mib''1. |
    R1.*6 |
    r2 sib' sib' |
    R1. |
    r2 mib'' mib'' |
    re''2. re''4 re''2 |
    do''1. |
    R1. |
    re''1 re''2 |
    sol'' sol'' sol'' |
    fa''1\melisma sib'2\melismaEnd |
    sib'1. |
  }
  \tag #'voix6 {
    \clef "vsoprano" R1.*2 |
    \ficta mib''2 sib' sib' |
    sib'1 sib'2 |
    R1. |
    mib''2 sib' sib' |
    sib'1 sib'2 |
    \ficta mib'' sib' sib' |
    sib'1 sib'2 |
    sib' sib'1 |
    sib' sib'2 |
    sib'\breve*3/4 |
    sib'1. |
    R1.*2 |
    r2 mib'' mib'' |
    sol'' sol'' mib'' |
    sib'1. |
    r2 mib'' mib'' |
    sol'' sol'' mib'' |
    sib'1. |
    R1. |
    sib'2 mib'' mib'' |
    sol''2.\melisma fa''4 \ficta mib'' re'' |
    \ficta mib''1.\melismaEnd |
    sib' |
    r2 sib' sib' |
    mib'' mib'' sol'' |
    mib''1. |
    r2 sib'1 |
    mib''1. |
    r2 sol'' sol'' |
    mib'' sib' sol' |
    sib'1. |
    R1.*6 |
    r2 mib'' mib'' |
    R1. |
    r2 sib' sib' |
    sib'2. sib'4 sib'2 |
    la'1. |
    R1. |
    sib'1 sib'2 |
    mib'' mib'' sib' |
    sib'2.\melisma do''4 re''2\melismaEnd |
    sib'1. |
  }
  \tag #'voix7 {
    \clef "vsoprano" R1.*2 |
    sol'2 sol' sol' |
    sol'1 sol'2 |
    R1. |
    sib'2 sol' sol' |
    sol'1 sol'2 |
    sib' sol' sol' |
    sol'1 sol'2 |
    sol' sol'1 |
    sol' sol'2 |
    sol'\breve*3/4 |
    sol'1. |
    R1.*2 |
    r2 sol' sol' |
    sib' sib' sib' |
    sol'1. |
    r2 sol' sol' |
    sib' sib' sib' |
    sol'1. |
    R1. |
    sol'2 sol' sib' |
    sib'2.\melisma la'4 sol' la' |
    sib'1.\melismaEnd |
    sol' |
    r2 sol' sol' |
    sib' sib' sib' |
    sol'1. |
    r2 sol'1 |
    sib'1. |
    r2 sib' sib' |
    sib' sol' sol' |
    sol'1. |
    R1.*6 |
    r2 sol' sol' |
    R1. |
    r2 sol' sol' |
    fa'2. fa'4 fa'2 |
    fa'1. |
    R1. |
    sol'1 sol'2 |
    sib' sib' sib' |
    sib'2.\melisma la'8[ sol'] fa'2\melismaEnd |
    sol'1. |
  }
  \tag #'voix8 {
    \clef "vbasse" R1.*2 |
    mib2 mib mib |
    \ficta mib1 mib2 |
    R1. |
    \ficta mib2 mib mib |
    \ficta mib1 mib2 |
    \ficta mib mib mib |
    \ficta mib1 mib2 |
    \ficta mib mib1 |
    \ficta mib mib2 |
    \ficta mib\breve*3/4 |
    \ficta mib1. |
    R1.*2 |
    r2 \ficta mib mib |
    \ficta mib mib mib |
    \ficta mib1. |
    r2 mib mib |
    \ficta mib mib mib |
    \ficta mib1. |
    R1. |
    mib2 mib mib |
    \ficta mib2.\melisma fa4 sol fa |
    mib1.\melismaEnd |
    \ficta mib1. |
    r2 mib mib |
    \ficta mib mib mib |
    \ficta mib1. |
    r2 mib1 |
    \ficta mib1. |
    r2 \ficta mib mib |
    \ficta mib mib mib |
    \ficta mib1. |
    R1.*6 |
    r2 mib mib |
    R1. |
    r2 mib mib |
    sib2. sib4 sib2 |
    fa1. |
    R1. |
    sol1 sol2 |
    mib mib mib |
    sib1. |
    mib1. |
  }
  
  \tag #'voix9 {
    \clef "vsoprano" R1.*12 |
    r2 sib' sib' |
    sib' sib' sib' |
    sib'1. |
    R1. |
    r2 sib' sib' |
    \ficta mib'' mib'' sol'' |
    mib''1. |
    r2 sib' sib' |
    \ficta mib'' mib'' sol'' |
    mib''1. |
    R1. |
    mib''2 sib' mib'' |
    \ficta mib''2.\melisma re''4 mib'' fa'' |
    sol''1.\melismaEnd |
    mib'' |
    r2 sib' sib' |
    mib'' mib'' sol'' |
    mib''1. |
    \ficta mib''2 sib' sib' |
    sib'\melisma do''4 sib' do'' re'' |
    mib''2\melismaEnd mib'' sol'' |
    mib''1. |
    r2 r sib' |
    sib'2. sib'4 sib' re'' |
    do''2. do''4 sib'1 lab'2 sib' |
    sib'1 sib'2 sib'1. |
    R1. |
    r2 sib' sib' |
    
  }
  \tag #'voix10 {
    \clef "valto" R1.*12 |
    r2 sol' sol' |
    sol' sol' fa' |
    sol'1. |
    R1. |
    r2 mib' mib' |
    sol' sol' sol' |
    sol'1. |
    r2 mib' mib' |
    sol' sol' sol' |
    sol'1. |
    R1. |
    sol'2 sol' sol' |
    sol'1.~ |
    sol' |
    sol' |
    r2 mib' mib' |
    sol' sol' sol' |
    sol'1. |
    sol'2 sol' mib' |
    \ficta mib'2.\melisma re'4 mib' fa' |
    sol'2\melismaEnd sol' sol' |
    sol'1. |
    r2 r sol' |
    fa'2. fa'4 fa' sib' |
    sol'2. sol'4 sol'1 mib'2 sol' |
    fa'1 fa'2 sol'1. |
    R1. |
    r2 mib' mib' |
    
  }
  \tag #'voix11 {
    \clef "vtenor" R1.*12 |
    r2 mib' mib' |
    \ficta mib' mib' re' |
    mib'1. |
    R1. |
    r2 sol sol |
    sib sib mib' |
    sib1. |
    r2 sol sol |
    sib sib mib' |
    sib1. |
    R1. |
    sib2 sol sib |
    sib2\melisma do'4 sib do' re' |
    mib'1.\melismaEnd |
    sib |
    r2 sol sol |
    sib sib mib' |
    sib1. |
    sib2 mib' sib |
    sol2.\melisma fa4 sol la |
    sib2\melismaEnd sib mib' |
    sib1. |
    r2 r \ficta mib' |
    re'2. re'4 re' fa' |
    mib'2. mib'4 re'1 do'2 mib'! |
    \ficta mib'1 re'2 mib'1. |
    R1. |
    r2 sol sol |
    
  }
  \tag #'voix12 {
    \clef "vbasse" R1.*12 |
    r2 mib mib |
    sol sol sib |
    mib1. |
    R1. |
    r2 mib mib |
    \ficta mib mib mib |
    \ficta mib1. |
    r2 \ficta mib mib |
    \ficta mib mib mib |
    \ficta mib1. |
    R1. |
    mib2 mib mib |
    \ficta mib2.\melisma fa4 sol fa |
    \ficta mib1.\melismaEnd |
    \ficta mib |
    r2 \ficta mib mib |
    \ficta mib mib mib |
    \ficta mib1. |
    \ficta mib2 mib mib |
    \ficta mib1. |
    \ficta mib1 mib2 |
    \ficta mib1. |
    r2 r \ficta mib |
    sib2. sib4 sib sib |
    do'2. do'4 sol1 lab2 \ficta mib |
    sib1 sib2 mib1. |
    R1. |
    r2 mib mib |
    
  }
>>