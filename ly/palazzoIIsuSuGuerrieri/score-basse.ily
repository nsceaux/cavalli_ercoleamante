\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \keepWithTag #'voix4 \includeNotes "voix" \clef "alto"
    >> \keepWithTag #'voix4 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'voix8 \includeNotes "voix"
    >> \keepWithTag #'voix8 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'voix12 \includeNotes "voix"
    >> \keepWithTag #'voix12 \includeLyrics "paroles"
  >>
  \layout { }
}
