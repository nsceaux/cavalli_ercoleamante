\score {
  <<
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix3 \includeNotes "voix"
      >> \keepWithTag #'voix3 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix4 \includeNotes "voix" \clef "alto"
      >> \keepWithTag #'voix4 \includeLyrics "paroles"
    >>
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix7 \includeNotes "voix"
      >> \keepWithTag #'voix7 \includeLyrics "paroles"
    >>
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix10 \includeNotes "voix" \clef "alto"
      >> \keepWithTag #'voix10 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix11 \includeNotes "voix" \clef "alto"
      >> \keepWithTag #'voix11 \includeLyrics "paroles"
    >>
  >>
  \layout { }
  \midi { }
}
