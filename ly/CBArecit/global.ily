\key fa \major \midiTempo#100
\time 4/4 s1*4 \bar "||" s1*3
\measure 2/1 s1*2 \bar "||"
\measure 4/4 s1*12
\measure 2/1 s1*2 \bar "||"
\time 4/4 s1*5 \bar "||" s1*6
\measure 2/1 s1*2
\measure 4/4 s1*8 \bar "|."
