\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*2\pageBreak
        s1*2 s2 \bar "" \break s2 s1*4\break
        s1*4\break s1*4\break s1*6\pageBreak
        s1*4\break s1*4\break s1*6\break s1*5\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
