Er -- co -- le a dir -- ti in -- vi -- a, ch’al -- tro non ba -- da,
che di sa -- per, se nel giar -- din de’ fio -- ri
di con -- dur -- ti a di -- por -- to og -- gi t’ag -- gra -- da.

Co -- me fia, che ciò nie -- ghi?
D’un che so -- vra di me le stel -- le al -- za -- ro
son co -- man -- di an -- co i prie -- ghi. __

Ahi qual tor -- bi -- do, e a -- ma -- ro
ve -- len pre -- sa -- ga ge -- lo -- sia m’ap -- pres -- ta,
di cui so -- lo il ti -- mor già mi fu -- nes -- ta.

Non te -- me -- re Hyl -- lo ca -- ro:
che non po -- trà mai vi -- o -- len -- za ar -- di -- ta
to -- glier -- mi a te, sen -- za a me tor la vi -- ta. __

E quand’ anch’ in tal gui -- sa
ogn’ un me -- co ti per -- da a -- ma -- to be -- ne,
qual mi -- glior sor -- te hav -- rò, che can -- giar pe -- ne?

Da sì gra -- ve ti -- mor l’al -- ma dis -- vez -- za,
che quant’ Er -- col per me pa -- le -- sa af -- fet -- to,
tan -- to ha ris -- pet -- to, ed io per te fer -- mez -- za.
Tor -- na, di -- gli, ch’io va -- do: Hyl -- lo vien me -- co.

E quand’ io non son te -- co!
Se dov -- un -- que il mio piè gi -- ri, o la men -- te
t’a -- do -- ro t’a -- do -- ro ogn’ or pre -- sen -- te. __
