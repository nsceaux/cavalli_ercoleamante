\clef "vsoprano" <>^\markup\character Paggio
do''8 sib'16 la' sib'8 sib'16 la' la'8 la' sol' sol'16 la' |
fa'8 fa' r16 mi' mi' fa' re'8. re'16 re'16 re' mi' fa' |
sol' sol' sol' sol' do''8 do''16 sib' la'8 sol' fa' fa'16 mi' |
re'2 do' |
\ffclef "vsoprano" <>^\markup\character Iole
sol'8 sol' do'' do''16 re'' sib'8 sib' r4 |
la'4 r8 la' la'4 la'8 la' |
la' la' re'' la' sib' sib' sib' do'' |
re'' re'' mi'' fa'' re''4. dod''8( re''1)\fermata |
\ffclef "vtenor" <>^\markup\character Hyllo
r4 fa'4. sol'8 re' re'16 re' |
re'4 re'8 re' re'4. do'8 |
sib sib sib sib sib4. la8 |
la4 la8 la do'2 |
fa'4 fa'8 do' re'2 |
sib4 lab8 lab fa4 fa |
\ffclef "vsoprano" <>^\markup\character Iole
r4 la'8. la'16 do''4 do'' |
r sol'8. fad'16 fad'4 fad' |
fad'8 fad' fad' sol' la'4 la'8 la' |
re''4 re''8 la' sib'4 sib' |
sib' do''8 re'' mib''2 |
la'4 la'8 la' fad'4 sib'16([ la' sol'8]) |
sol'2. fad'4( sol'1)\fermata |
\ffclef "vtenor" <>^\markup\character Hyllo
r4 sib8 re' sib4 sib8 la |
sib sib sib sib sib4 sib8 la |
do'4 do'8 re' sib4. la8 |
la4 la fa'8 fa'16 fa' fa'8 sol' |
mib'8 re' re' re' sib sib r4 |
\ffclef "vsoprano" <>^\markup\character Iole
r4 fa'8 fa' fa'4 fa'8 fa' |
fad'2 do''4 fad'!8 sol' |
sol'4 sol' la'8 la' la' la'16 la' |
la'4. la'8 do''4 do''8 re'' |
sib'4 sib' la' la'8 sib' |
sol'4 sol' r8 la' re'' la' |
sib'4. re''8 la'2 la'1\fermata |
la'8 fa' la' la'16 la' re'8 re' r4 |
do''8 la' r re'' do'' do'' r4 |
\ffclef "vtenor" <>^\markup\character Hyllo
r4 do'8 fa' mib'4 mib'8 re' |
sib4 sib r sib8 do' |
lab4 lab8 lab lab2 |
lab4 lab8 sol sol4 sol8 sol |
la4 la8 la si si do' mib' |
do'4. si8( do'2) |
