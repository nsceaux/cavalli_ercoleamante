\clef "basse" fa4 mi fa do |
re mi fa sol |
mi do fa la |
fa sol do2 |
do sol |
fad1~ |
fad2 sol4. la8 |
sib4 sol la2 re1\fermata |
re2 si, |
mib1 |
mi! |
fa |
la,2 sib,~ |
sib, do |
fa,4 fa mib2~ |
mib re~ |
re fad~ |
fad sol~ |
sol do~ |
do re4 do |
re1 sol,\fermata | \allowPageTurn
sol1~ |
sol |
mi |
fa |
sol2~ sol8 mib fa4 |
sib,1 |
la, |
sol, |
fad, |
sol,2 la, |
sib, fad, |
sol, la, re1\fermata |
re |
la,4. sib,8 do4 fa, |
fa1 |
sib,~ |
sib,~ |
sib,2 mib |
fa sol4 fa |
sol2 do |
