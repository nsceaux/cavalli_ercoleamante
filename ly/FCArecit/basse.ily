\clef "basse"
\original {
  sol,1~ |
  sol, |
  re |
  si, |
  do |
  re2 sol, |
}
sol,2 do4~ |
do re2 |
fad sol4~ |
sol la2 |
re2. |
sol, |
do |
la,1~ |
la, |
sol, |
fa, |
mi, |
re, |
la,2 fa, |
mi, re, |
mi, la, |
\original {
  la2 si~ |
  si do' |
  mi fa |
  sol la |
  si do'~ |
  do'4 si8 la si2 do'1 |
}
do |
sol, |
sol,2. mi,4 |
re,2 re |
do si, |
do re sol,1\fermata |
\original {
  sol1 |
  do'2 si |
  la sol |
  fa1~ |
  fa2 mi |
  mi la, |
  la,1 |
  re'2 do' |
  sib la |
  sol fa |
  sol la4 sol |
  la1 re\fermata |
  re1 |
  sol, |
  sol,2 do |
  la,1~ |
  la, |
  la,2 sold, |
  la,1 |
  mi2 mi4 |
  fa2 fa4 |
  la2. |
  si |
  do' |
  do2 fa4 |
  sol2~ sol4 sol |
  la sol8 fa mi do re mi |
  fa fa, sol,4 do2 |
}
do1 |
si, |
la,1 |
sol,2 fa, |
mi, re, |
mi,1 la,\fermata |
\original {
  la,1 |
  sold,2 la,4 si, |
  mi,2 mi |
  la sol |
  fa1 |
  mi2 do |
  re sol, |
  do mi, |
  fa, la, |
  sol,1 do |
}
\transpose do #(if (eqv? #t (ly:get-option 'pygmalion))
                   (ly:make-pitch -1 1)
                   (ly:make-pitch -1 0))
{
  do1 |
  sol |
  mi2 re |
  si, do~ |
  do re sol,1\fermata |
}
