\key do \major
\original { \time 4/4 \midiTempo#100 s1*6 \bar "|." }
\digitTime\time 3/4 \midiTempo#160 s2.*7 \bar "|."
%% Hyllo
\time 4/4 \midiTempo#100 s1*9 \bar "|."
\original {
  %% Deianira
  s1*5 \measure 2/1 s1*2
}
%% Iole
\measure 4/4 s1*5 \measure 2/1 s1*2 \bar "|."
\original {
  %% Hyllo, Licco, Hyllo
  \measure 4/4 s1*11 \measure 2/1 s1*2 \bar "|."
  %% Deianira, Licco
  \measure 4/4 s1*7
  \digitTime\time 3/4 \midiTempo#160 s2.*6
  \time 4/4 \midiTempo#100 s1*3
}
\pygmalion\measure 4/4
%% Deianira
s1*5 \measure 2/1 s1*2 \bar "|."
\original {
  \measure 4/4 s1*9 \measure 2/1 s1*2 \bar "|."
}
\pygmalion \key sol \major
\measure 4/4 s1*4 \measure 2/1 s1*2 \bar "|."
