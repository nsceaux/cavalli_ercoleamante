\tag #'(recit0 basse) {
  \original {
    Veg -- gio, o di ve -- der par -- mi?
    Non at -- te -- so con -- ten -- to!
    Ah che dar fe -- de a gl’oc -- chi il cor non o -- sa.
  }
  
  Oh che op -- por -- tun ris -- to -- ro!
  
  Oh che spa -- ven -- to!
  
  Hyl -- lo?
  Hyl -- lo?
}
\tag #'(recit1 basse) {
  Fi -- glio?
  Fi -- glio?
}
\tag #'(recit0 recit1 basse) {
  Sei tu?
}
\tag #'(recit0 basse) {
  Mer -- cé di Giu -- no
  son io dal mar sal -- va -- to
  ac -- ciò per gl’oc -- chi mie -- i
  ver -- si in un mar di pian -- to il cor stem -- pra -- to.
  Se qual ri -- dir -- lo in -- ten -- do
  ve -- ro è del ca -- ro pa -- dre il fa -- to hor -- ren -- do.

  \original {
    Ah fi -- glio ahi trop -- po è ver, che mi ri -- ve -- di
    ve -- do -- va af -- flit -- ta, e so -- la.
  }
  
  Pur mio ben ti con -- so -- la,
  che se per -- des -- ti il ge -- ni -- tor cru -- de -- le
  me qui ri -- tro -- vi, e l’a -- mor mio fe -- de -- le.
  
  \original {
    Ah dun -- que il ciel non sep -- pe
    far -- mi te -- co fe -- li -- ce?
    Sen -- za mi -- se -- ro far -- mi, e sven -- tu -- ra -- to
    con la mia ge -- ni -- tri -- ce?
    
    Oh ben tor -- na -- to.
    
    Ahi che con for -- za e -- gua -- le a un tem -- po is -- tes -- so
    da gio -- ia, e da do -- lo -- re
    trat -- to in con -- tra -- rie par -- ti
    sen -- to squar -- ciar -- mi il co -- re.
    
    Ohi -- mè dun -- que che fi -- a?
    
    For -- za è ch’io ri -- da
    quel ch’è sta -- to mai sem -- pre
    da che mor -- te im -- pu -- gnò fal -- ce ho -- mi -- ci -- da,
    ch’al -- tri av -- vien, che si stem -- pre
    in po -- chi, et al -- tri in co -- pi -- o -- si lut -- ti.
    Ma chi muo -- re suo dan -- no,
    che pres -- to, o tar -- di si con -- so -- lan si con -- so -- lan tut -- ti.
  }

  Sa -- ran -- no al -- men le ce -- ne -- ri d’Al -- ci -- de
  le più pom -- po -- se de’ fu -- ne -- bri ho -- no -- ri
  e più spar -- se di la -- gri -- me, e di fio -- ri. __
  
  \original {
    Cer -- to è che i miei sin -- gul -- ti
    non ha -- vran fin.
    
    Ma non fia già che so -- lo
    tu pian -- ga a -- ma -- to ben, che se co -- mu -- ne
    ho te -- co il cor fia pur co -- mu -- ne il duo -- lo.
  }

  Hor che sor -- te è la mi -- a?
  Che sen -- za ha -- ver -- ne vo -- gli -- a,
  an -- ch’io per com -- pa -- gni -- a
  con -- ver -- rà che mi do -- glia.
}
