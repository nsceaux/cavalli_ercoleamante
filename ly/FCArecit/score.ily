\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \keepWithTag #'recit0 \includeNotes "voix"
    >> \keepWithTag #'recit0 \includeLyrics "paroles"
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'recit1 \includeNotes "voix"
    >> \keepWithTag #'recit1 \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*2\break s1*4\pageBreak
        s2.*7\break s1*4\break s1*4\break s1*5\pageBreak
        s1*6\break s1*4\break s1*4\break s1*3 s2 \bar "" \break s2 s1*5\pageBreak
        s1*5\break s1*2 s2.*3\break s2.*3 s1*2\break s1*4\break s1*4\pageBreak
        s1*5\break s1*6\break s1*3 s2 \bar "" \break s2 s1*2\pageBreak
      }
      \modVersion {
        \original s1*6 s2.*4 \ru#2 { s2.\noBreak }
      }
    >>
  >>
  \layout { }
  \midi { }
}
