<<
  \tag #'(recit0 basse) {
    \original {
      \clef "vsoprano" <>^\markup\character Iole
      re''8 si' r4 si'8 si' si' re'' |
      si' si' si' si' si'4 dod''8 re'' |
      la'4 la' r2 |
      re''4 r8 sol' fa'4 fa'8 mi' |
      mi'4 mi'8 mi' fad'4. sol'8 |
      sol'4. fad'8( sol'2) |
    }
    \ffclef "vsoprano"
    \original <>^\markup\character Deianira
    \pygmalion <>^\markup\character Iole
    re''2 do''8 si' |
    si' la' la'4 la' |
    \ffclef "valto" <>^\markup\character Licco
    r4 la'4 sol'8 fad' |
    re' re' r4
    \ffclef "vsoprano" <>^\markup\character Iole
    mi''8 dod'' |
    fad''4 fad'' <<
      \tag #'basse { s4 s2 \ffclef "vsoprano" <>^\markup\character Iole }
      \tag #'recit0 { r4 | r r }
    >> sol''4 |
    mi''2. |
    \ffclef "vtenor" <>^\markup\character Hyllo
    r8 do' do' do' do'4 do'8 do' |
    la8 la la sold la4 la8 si |
    si si si si si4 si |
    do'4 do' do'8 do' do' do' |
    dod'4 dod'8 dod' dod'4. re'8 |
    re'4 re'8 re' la la la si |
    do'4 do' do'8 do'16 do' re'8 do' |
    mi'4 mi'8 sold sold4 sold8 do' |
    la4. sold8( la2) |
    \original {
      \ffclef "vsoprano" <>^\markup\character Deianira
      r4 mi'' si' si' |
      sol'' re''8 do'' do''2 |
      r8 do'' sib' la' la'4 la' |
      sol'8. sol'16 sol'4 r do'' |
      fa'4 fa' r mi' |
      fa'1 mi' |
    }
    \ffclef "vsoprano" <>^\markup\character Iole
    r4 do''8 do'' sol'4 sol'8 la' |
    si'4 si' r8 si' si' re'' |
    si'4 si'8 si' si' si' dod''8. re''16 |
    re''4 re'' la' la'8 la' |
    la'4 la' r8 la' la' la' |
    la'4 la'8[ si'16 do''] la'2 sol'1\fermata |
    \original {
      \ffclef "vtenor" <>^\markup\character Hyllo
      re'2 sol'8 fa' fa' mi' |
      mi'4 mi' re'8 re' re' re'16 mi' |
      fa'4 fa' mi'8 mi' mi' mi'16 mi' |
      la8 la la sold la la si si |
      si4 si8 la si4 si |
      \ffclef "valto" <>^\markup\character Licco
      mi'4 si8 do' la4 la |
      \ffclef "vtenor" <>^\markup\character Hyllo
      mi'2 sol'8 sol' sol' fa' |
      fa' fa'16 fa' fa'8 mi' mi'4 mi'8 re' |
      re'8 re'16 re' re'8 do' do'4 do' |
      sib8 sib16 sib sib8 la la4 la |
      si4 si8 mi' dod'4 dod'8 re' |
      re'2. dod'4( re'1) |
      \ffclef "vsoprano" <>^\markup\character Deianira
      re''4 la' do'' do''8 si' |
      sol'4 sol' r2 |
      \ffclef "valto" <>^\markup\character Licco
      re'4 si8 do' do' do' r4 |
      mi' r8 mi' mi'4 mi'8 mi' |
      do'4 do' do'8 do' do' do'16 do' |
      do'2 si4 si8 do' |
      la4 la la'8 la' la' la'16 sol' |
      sol'4 sol' sol' |
      fa' fa' r8 la' |
      fa'2 fa'4 |
      fa'4 fa'8 fa' fa' fa' |
      mi'4 mi' mi'8 sol' |
      mi'4 mi' r8 re' |
      re'4 re' r8 re' re' mi' |
      do' re' mi' fa' sol' sol' fa' sol' |
      la' re' re'4 do'2 |
    }
    \ffclef "vsoprano" <>^\markup\character Deianira
    r8 sol' sol' sol' sol'4 r8 sol' |
    sold'8. sold'16 sold'8 sold' sold' sold'16 si' sold'8 la' |
    la'8 la' la' la' la'4 la'8 si' |
    si'4 si' do''8 do'' do'' do''16 do'' |
    dod''8. dod''16 dod''4 r re''8 \ficta do'' |
    la'2. sold'4( la'1)\fermata |
    \original {
      \ffclef "vtenor" <>^\markup\character Hyllo
      r4 mi' do'8 do' do' re' |
      mi'4 mi' r8 fad' fad' sol' |
      mi'2
      \ffclef "vsoprano" <>^\markup\character Iole
      r8 si' mi'' si' |
      do''4. mi''8 si'4 si'8 do'' |
      la'4 la' r8 la' la' si' |
      sol'2 r8 sol' sol' la' |
      fa'4 fa'8 mi' fa'4 fa'8 mi' |
      mi'2 r8 sol' sol' sol' |
      la'4 la'8 si' do''4.\melisma re''8 |
      mib''2( re'')\melismaEnd do''1 |
    }
    \ffclef "valto" <>^\markup\character Licco
    \transpose do #(if (eqv? #t (ly:get-option 'pygmalion))
                   (ly:make-pitch -1 1)
                   (ly:make-pitch -1 0))
    {
      mi'4 r8 sol' do' do' do' si |
      si4 si r si |
      dod'8 dod'16 dod' dod'8 re' re'4 re'8 re' |
      sol'8 fa' fa' mi' mi' mi' mi' mi' |
      fad'4 fad'8 sol' re'4 re' r1\fermata |
    }
  }
  \tag #'(recit1 basse) {
    <<
      \tag #'basse { \original s1*6 s2.*4 s2 \ffclef "vsoprano" }
      \tag #'recit1 { \clef "vsoprano" \original R1*6 R2.*4 | r4 r }
    >> <>^\markup\character Deianira
    re''8 la' |
    si'4 si'
    \tag #'recit1 { re''4 | do''2. | R1*13 }
  }
>>
