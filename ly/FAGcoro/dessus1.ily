\clef "dessus" R2.*4 |
r4 fa'' fa'' sol'' re'' mib'' |
re'' fa''4. fa''8 fa''2. |
R2.*6 |
r1*3/4 r4 la'' la'' re'' re'' sol'' |
do'''2 fa''4 sib'' sib'' sol'' la''2 fa''4 do'''2. |
do'''4 do'''8 do''' sib''8 sib'' do''' do''' |
fa'' fa'' sib'' sib'' sib'' la'' la'' la'' |
sol''2 sib''8 sib'' sol'' sol'' |
re'' fa'' sol'' sol'' sol''4 do'' |
re''8. mi''16 re''8. mi''16 fa''4 fa''8 re'' |
mib''4. do''8 fa''4 fa''8 fa'' |
sib''4 mib'' mib'' lab'' re'' re'' sol'' do'' do'' |
fa''2 sol''4 sol''2 fa''4 sib''2. sib'' |
R1 |
r4 sol''8 sol'' sib'' fa'' sol'' re'' |
fa'' sib'' sib'' sib'' do'''2 sib''1\fermata | \allowPageTurn
R1*3 |
sol''2 sib''4 la'' la'' la'' fa'' fa'' fa'' |
sol''2 sol''4 fa''2 fa''4 |
sib''2. fa''2 fa''4 |
fa''4. fa''8 sol''4 do''4 re''4. mi''8 fa''2. fa''1*3/4\fermata |
