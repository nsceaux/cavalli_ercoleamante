\score {
  \new ChoirStaff \with { \haraKiriFirst } <<
    \new Staff \withLyrics <<
      \global \keepWithTag #'valto \includeNotes "voix" \clef "alto"
    >> \keepWithTag #'valto \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'vtenor \includeNotes "voix" \clef "alto"
    >> \keepWithTag #'vtenor \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'vbasse \includeNotes "voix"
    >> \keepWithTag #'vbasse \includeLyrics "paroles"
    \new Staff \with { \tinyStaff } \withLyrics <<
      \global \keepWithTag #'eutyro \includeNotes "voix"
    >> \keepWithTag #'eutyro { \set fontSize = -2 \includeLyrics "paroles" }
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      { s2.*21 s1*6 s2.*7 s1*4 \allowPageTurn }
    >>
  >>
  \layout { }
}
