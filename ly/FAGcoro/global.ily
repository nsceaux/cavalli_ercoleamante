\key fa \major
\customTime\markup\vcenter { \musicglyph#"timesig.C44" \musicglyph#"three" }
\midiTempo#240
\measure 6/4 s1.*4
\measure 9/4 s2.*9
\measure 12/4 s1.*2
\time 4/4 %\midiTempo#120
s1*6
\digitTime\time 3/4 \measure 9/4 s2.*3 %\midiTempo#240
\measure 12/4 s1.*2
\time 4/4 %\midiTempo#120
s1*2 \measure 2/1 s1*2 \bar "|."
\measure 4/4 s1*3 \bar "|." \endMarkSmall "Segue il Coro"
\digitTime\time 3/4 \measure 9/4 s2.*3
\measure 6/4 s1.*2
\measure 12/4 s2.*4 \bar "|."
