\clef "basse"
<<
  \tag #'basse-continue {
    sib2 la4 sol fa mib |
    fa2. sib, |
    sib2 la4 sol fa mib |
    fa2. sib, |
    sib la sol |
    fa sol la2 sib4 |
    do'2. la2 la4 sib2 sib4 |
    do'2 la4 sib2. do'2 sib4 do'2. |
    fa2 re4 do |
    sib, re mib8 do re4 |
    sol,2 sol4 mib |
    sib sol sol la |
    sib1 |
    sib4. la8 sib2 |
    sol4 lab2 fa4 sol2 mib4 fa2 |
    re2 mib4 sol2 lab4 sib2. mib |
    sib2 la4 sol |
    fa mib re do8 sib, |
    fa re mib sib, fa2 sib,1\fermata |
    %%
    sib2 la |
    sol fa |
    mib4 do re sol, |
    %%
    sol2. la2 la4 sib2 re4 |
    mib2. fa |
    sol re |
    re2 mib4 fa1. sib,1*3/4\fermata |
  }
>>
