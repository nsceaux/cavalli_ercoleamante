<<
  \tag #'vsoprano {
    \clef "vsoprano"
    \pygmalion <>^\markup\character Clerica
    r4 re'' fa'' mib'' re'' do'' |
    re'' do'' fa'' re''2. |
    \pygmalion <>^\markup\character Coro
    r4 sib' do'' sol' la' sib' |
    sib' la'4. sib'8 sib'2. |
    R2.*3 |
    r4 do'' do'' sib' do'' sib' la' sib' la' |
    sol' sol' sol' do''2 do''4 sib' sib' re'' |
    do''2 do''4 sib' sib' re'' do''2 re''4 do''2. |
    do''4 la'8 la' re'' sib' sib' la' |
    sib' sib' sib' sib' sol' do'' la' la' |
    si'2 r |
    r4 re''8 re'' sib' sib' fa'' fa'' |
    re'' re'' re'' re'' re''4 sib' |
    do''4. do''8 sib'4 re''8 re'' |
    mib''4 do'' do'' re'' sib' sib' do'' la' la' |
    sib'2 sib'4 sib'2 do''4 sib'2. sib' |
    r2 fa''8 fa'' mib'' mib'' |
    re'' re'' do'' do'' sib' sib' do'' re'' |
    la' sib' sol' sib' sib'4. la'8( sib'1)\fermata |
    R1*3 |
    sib'2. r4 r do'' sib' sib' lab' |
    sol' sol' sol' la'2 la'4 |
    sib' sib' sol' re''2. |
    re''4. do''8 sib'4 la'\melisma sib'2 sib'\melismaEnd la'4( sib'1*3/4)\fermata |
  }
  \tag #'valto {
    \clef "valto" R2.*4 |
    r4 fa' la' sib' fa' sol' |
    fa' fa' do' re'2. |
    R2.*3 |
    r4 la' la' sol' la' sol' fa' sol' fa' |
    mi' mi' mi' la'2 la'4 sol' sol' sib' |
    sol'2 la'4 fa' fa' sib' sol'2 sib'4 sol'2. |
    la'4 fa'8 fa' sib' fa' sol' do' |
    fa' fa' fa' fa' mib' sol' sol' fad' |
    sol'2 r4 sol'8 sol' |
    fa' fa' sib' sib' sol' sol' fa' fa' |
    sib'4 sib' sib' fa'8 fa' |
    mib'4. fa'8 fa'4 sib'8 sib' |
    sib'4 lab' lab' lab' sol' sol' sol' fa' fa' |
    fa'2 mib'4 mib'2 lab'4 fa'2. sol' |
    r2 r4 sib'8 sib' |
    la' la' sol' sol' fa' fa' mib' fa' |
    fa' fa' mib' fa' fa'2 fa'1\fermata |
    R1*3 |
    sol'2 sol'4 fa' fa' mib' re' re' re' |
    sib2 sib4 fa' fa' re' |
    sol'2. fa'2 fa'4 |
    fa'4. mib'8 re'4 do'\melisma sib2 fa'2.\melismaEnd fa'1*3/4\fermata |
  }
  \tag #'vtenor {
    \clef "vtenor" R2.*4 |
    r4 re' fa' mib' re' do' |
    re' do' fa' fa'2. |
    r4 re' re' do' re' do' sib do' sib |
    la la r4 r\breve*3/4 |
    r1*3/4 r4 fa' fa' re' re' re' |
    mi'2 fa'4 re' re' re' mi'2 fa'4 fa'2 mi'4( |
    fa') do'8 do' fa' re' mib' mib' |
    re' re' re' re' do' mib' re' re' |
    re'4 re'8 re' sib sib mib' mib' |
    re' re' sol' sol' re'4 do' |
    fa'2 fa'4 re' |
    do'4. do'8 re'4 fa'8 fa' |
    sol'4 mib' mib' fa' re' re' mib'! do' do' |
    re'2 sol'4 sol'2 mib'4 mib'2 re'4( mib'!2.) |
    r4 re'8 re' do' do' sib sib |
    fa' fa' do' do' re' re' sol sib |
    do' do' sib re' do'2 re'1\fermata |
    R1*3 |
    re'2. r1*3/4 r4 r fa' |
    mib' mib' re' do' do' fa' |
    re'2 re'4 sib2 sib4 |
    sib2. fa'4. mib'8 re'4 do'2. re'1*3/4\fermata |
  }
  \tag #'vbasse {
    \clef "vbasse" R2.*4 |
    r4 sib la sol fa mib |
    fa fa fa, sib,2. |
    r4 sib sib la sib la sol la sol |
    fa fa fa sol2 sol4 la2( sib4) |
    do' do' do' la la la sib2 sib4 |
    do' do' la sib2 sib4 do'2\melisma sib4( do'2.)\melismaEnd |
    fa4 fa8 fa re re do do |
    sib, sib, re re mib do re re |
    sol,2 sol8 sol mib mib |
    sib sib sol sol sol4 la |
    sib2 sib |
    sib4. la8 sib4 sib8 sib |
    sol4 lab lab fa sol sol mib fa fa |
    re2 mib4 sol2 lab4 sib2. mib |
    r4 sib8 sib la la sol sol |
    fa fa mib mib re re do sib, |
    fa re mib sib, fa2 sib,1\fermata |
    R1*3 |
    sol2 sol4 la la la sib sib re |
    mib2 mib4 fa2 fa4 |
    sol2. re2 re4 |
    re4. re8 mib4 fa2.~ fa sib,1*3/4\fermata |
  }
  \tag #'(eutyro basse) {
    \clef "vbasse" R2.*21 R1*6 R2.*7 R1*4 |
    <>^\markup\character Eutyro
    sib4 r8 sib la4 la8 sib |
    sol4 sol8 sol fa fa fa sol |
    mib4 mib8 fa16 sol re4 << \original sol \pygmalion sol, >> |
  }
>>
