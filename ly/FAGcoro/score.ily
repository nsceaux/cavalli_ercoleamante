\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket \with {
      \override VerticalAxisGroup.remove-empty = #(eqv? #t (ly:get-option 'pygmalion))
    } <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
      \original <<
        \new Staff << \global \includeNotes "partie1" >>
        \new Staff << \global \includeNotes "partie2" >>
        \new Staff << \global \keepWithTag #'basse \includeNotes "basse" >>
      >>
    >>
    \new ChoirStaff \with { \haraKiri } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'vsoprano \includeNotes "voix"
      >> \keepWithTag #'vsoprano \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'valto \includeNotes "voix"
      >> \keepWithTag #'valto \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vtenor \includeNotes "voix"
      >> \keepWithTag #'vtenor \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'eutyro \includeNotes "voix"
    >> \keepWithTag #'eutyro \includeLyrics "paroles"
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2.*11\pageBreak
        s2.*10 s2 \bar "" \pageBreak
        s2 s1*3\pageBreak
        s1*2 s2.*7 s2 \bar "" \pageBreak
        s2 s1*3\pageBreak
        s1*3\pageBreak
      }
      \modVersion {
        s2.*21 s1*6 s2.*7 s1*4\break s1*3\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
