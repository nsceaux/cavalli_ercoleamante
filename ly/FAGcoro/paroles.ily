\tag #'(vsoprano valto vtenor vbasse) {
  \tag #'vsoprano {
    Su, su dun -- que all’ ar -- mi, su, su,
  }
  Su, su dun -- que all’ ar -- mi, su, su,
  su cor -- ria -- mo
  cor -- ria -- mo
  \tag #'(vsoprano valto) {
    cor -- ria -- mo a ven -- di -- car -- ci
    a ven -- di -- car -- ci
    a ven -- di -- car -- ci,
  }
  \tag #'vtenor {
    cor -- ria -- mo
    su cor -- ria -- mo a ven -- di -- car -- ci
    a ven -- di -- car -- ci, __
  }
  \tag #'vbasse {
    cor -- ria -- mo
    a ven -- di -- car -- ci
    su cor -- ria -- mo a ven -- di -- car -- ci
    a ven -- di -- car -- ci,
  }
  ch’al -- tro ben non può mai dar -- ci
  il des -- ti -- no di quag -- giù.
  E che gio -- va
  e che gio -- va as -- sor -- dar quest’ an -- tri
  \tag #'(valto vtenor) { quest’ an -- tri }
  più
  con il va -- no ru -- mor
  con il va -- no ru -- mor
  de’ nos -- tri car -- mi?
  Su, su dun -- que all’ ar -- mi, all’ ar -- mi
  su, su dun -- que all’
  \tag #'(vsoprano vtenor vbasse) { ar -- mi, all’ }
  \tag #'(vtenor vbasse) { ar -- mi, all’ }
  ar -- mi, all’ ar -- mi.
}
\tag #'(eutyro basse) {
  Ah più val più di -- let -- ta,
  che quan -- te gioie ha il ciel u -- na ven -- det -- ta.
}
\tag #'(vsoprano valto vtenor vbasse) {
  Ah più val più di -- let -- ta,
  che quan -- te
  \tag #'(vsoprano valto) { gio -- ie }
  \tag #'(vtenor vbasse) { gioie }
  ha il ciel
  \tag #'(valto vbasse) { u -- na }
  u -- na ven -- det -- ta.
}
