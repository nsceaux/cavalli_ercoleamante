\clef "dessus" R2.*4 |
r4 re'' do'' mib'' fa'' sib' |
fa'' do''4. do'''8 sib''2. |
R2.*6 |
r1*3/4 r4 fa'' fa'' sol'' sol'' sol'' |
sol''2 do'''4 fa'' fa'' re'' sol''2 re''4 sol''2. |
fa''4 fa''8 fa'' fa'' fa'' mib'' mib'' |
sib'' sib'' fa'' fa'' sol'' sol'' re'' re'' |
re''2 re''8 re'' mib'' sib' |
sib' sib' re''4 re'' la'' |
fa''8. sol''16 fa''8 sol''16 la'' sib''4 sib''8 fa'' |
sol''4. fa''8 re''4 re''8 re'' |
sol''4 do'' do'' fa'' sib'' sib' mib'' la' la' |
re''2 sib'4 mib''2 do''4 fa''2. mib''! |
R1 |
r4 mib''8 mib'' fa'' re'' mib'' sib'' |
la'' fa'' sol'' fa'' fa''2 fa''1\fermata |
R1*3 |
re''2 sib'4 do'' do'' do'' re'' re'' r |
r r sib'' la''2 la''4 |
sol''2. sib''2 sib''4 |
sib''4. sib''8 sib''4 fa''2 sib'4 do'' do'''2 sib''1*3/4\fermata |
