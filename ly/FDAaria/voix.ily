\clef "vsoprano" <>^\markup\character Giunone
R1.*2 |
fad''4 re'' mi'' fad'' re'' mi'' |
fad'' re'' mi'' fad'' re'' fad'' |
si'2 dod''4 re''4. mi''8 fad''4 |
fad'' mi''2 re''2. r2*3/2 |
R2. |
r2*3/2 mi''4 re'' mi'' |
fad''2 fad''4 si' dod'' re'' |
mi''2. lad'4 si' dod'' |
re''2 re''4 si'2 si'4 |
mi'' fad'' sol'' re''( dod''2) si'2. |
re''4 dod''4. re''8 si'2 si'4 |
dod''4 re'' mi'' fad'' re'' mi'' |
fad'' re'' mi'' fad'' re'' fad'' |
si'2 dod''4 re'' mi''4. fad''8 |
fad''4( mi''2) re''2. |
R2.*4 |
r4 la'8 la' re''4 r |
re''8 re'' re'' fad'' re''4 re'' |
r r fad'' |
si' si' fad'' |
si' dod''2 re''4 mi'' fad'' fad'' mi''2 re''2. |
R2.*3 |
mi''4 re'' mi'' fad''2 fad''4 |
si' dod'' re'' mi''2. lad'4 si' dod''! re''2. |
si'2. mi''4. fad''8 sol''4 |
re''( dod''2) si'2. |
re''4 dod''4. re''8 si'2 si'4 |
dod'' re'' mi'' fad'' re'' mi'' |
fad'' re'' mi'' fad'' re'' fad'' |
si'2 dod''4 re'' mi''4. fad''8 |
fad''4( mi''2) re''1*3/4\fermata |
