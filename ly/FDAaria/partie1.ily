\clef "viole1" la'4 la' si' la' la' si' |
la' la' la' si' la'2 |
la'2. r2*3/2 |
R2.*4 |
r2*3/2 la'4 la' si' la' la' si' |
la' la' la' |
fad' mi'2 mi'2. |
re'2 la'4 sol'2 sol'4 |
sol'2 sol'4 fad'2. |
fad'4 mi' re' re'2 sol'4 |
sol' fad' mi' re' fad'2 red'2. |
fad'4 fad'4. fad'8 sol'2 sol'4 |
dod'4 si dod' re' re' sol' |
fad' fad' si' la' la' la' |
sol' si' la' re'2 re'4 |
la'2. fad'4 la' si' |
la' la' si' |
la' la' la' si' la'2 la'2. |
R1*2 R2.*2 |
r1*3/2 r2*3/2 la'4 la' si' |
la' la' si' |
la' la' la' fad' mi'2 |
mi'2. re'2 la'4 |
sol'2 sol'4 sol'2 sol'4 fad'2. fad'4 mi' re' |
re'2 sol'4 sol' fad' mi' |
re' fad'2 red'2. |
fad'4 fad'4. fad'8 sol'2 sol'4 |
dod'4 si dod' re' re' sol' |
fad' fad' si' la' la' la' |
sol' si' la' re'2 re'4 |
la'2. fad'1*3/4\fermata |
