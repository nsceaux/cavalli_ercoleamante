\key do \major \midiTempo#200
\beginMark "Sinf[onia]"
\customTime\markup\vcenter { \musicglyph#"timesig.C44" \musicglyph#"three" }
\measure 6/4 s1.*5
\measure 9/4 s2.*3
\measure 3/4 s2.
\measure 6/4 s1.*4
\measure 9/4 s2.*3
\measure 6/4 s1.*5
\measure 3/4 s2.
\measure 9/4 s2.*3
\time 4/4 \midiTempo#100 s1*2
\digitTime\time 3/4 \midiTempo#200 s2.*2
\measure 12/4 s2.*4
\measure 3/4 s2.
\measure 6/4 s1.*2
\measure 12/4 s2.*4
\measure 6/4 s1.*7 \bar "|."
