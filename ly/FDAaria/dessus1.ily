\clef "dessus" la''4 fad'' sol'' la'' fad'' sol'' |
la'' fad'' la'' sol'' mi''2 |
fad''2. r2*3/2 |
R2.*4 |
r2*3/2 la''4 fad'' sol'' la'' fad'' sol'' |
la'' fad'' la'' |
la'' sold''2 la''2. |
la''4 sol'' la'' si''2 si''4 |
sol''4 la'' si'' fad''2. |
fad''4 sol'' la'' re''2 re''4 |
sol'' la'' si'' si''2 lad''4 si''2. |
fad''4 fad''4. fad''8 re''2 re''4 |
la'' sol''4. la''8 la''4 fad'' sol'' |
la'' fad'' sol'' la'' fad'' la'' |
re''2 la''4 sol'' si''2 |
la''2. la''4 fad'' sol'' |
la'' fad'' sol'' |
la'' fad'' la'' sol'' mi''2 fad''!2. |
R1*2 |
R2.*2 |
r1*3/2 r2*3/2 la''4 fad'' sol'' |
la'' fad'' sol'' |
la'' fad'' la'' la'' sold''2 |
la''2. la''4 sol'' la'' |
si''2 si''4 sol'' la'' si'' fad''2. fad''4 sol'' la'' |
re''2 re''4 sol'' la'' si'' |
si''2 lad''4 si''2. |
fad''4 fad''4. fad''8 re''2 re''4 |
la'' sol''4. la''8 la''4 fad'' sol'' |
la'' fad'' sol'' la'' fad'' la'' |
re''2 la''4 sol'' si''2 |
la''2. la''1*3/4\fermata |
