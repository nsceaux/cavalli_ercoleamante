\clef "viole2" re'4 re' si re' re' si |
re' re' re' si mi'2 |
re'2. r2*3/2 |
R2.*4 |
r2*3/2 re'4 re' si re' re' si |
re' re' la |
si si2 la2. |
la4 la4. re'8 re'2 si4 |
mi'2 si4 lad2. |
si4 si si si2 si4 |
si2 si4 fad2. si |
si4 dod' la re'2 re'4 |
la2 la4 la la si |
la la sol fad la re' |
re'2 dod'4 si2 si4 |
fad la2 la4 re' si |
re' re' si |
re' re' re' si mi'2 re'2. |
R1*2 R2.*2 |
r1*3/2 r2*3/2 re'4 re' si |
re' re' si |
re' re' la si si2 |
la2. la4 la4. re'8 |
re'2 si4 mi'2 si4 lad2. si4 si si |
si2 si4 si2 si4 |
fad2. si |
si4 dod' la re'2 re'4 |
la2 la4 la la si |
la la sol fad la re' |
re'2 dod'4 si2 si4 |
fad la2 la1*3/4\fermata |
