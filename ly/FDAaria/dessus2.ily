\clef "dessus" fad''4 re'' mi'' fad'' re'' mi'' |
fad'' re'' fad'' re'' dod''2 |
re''2. r2*3/2 |
R2.*4 |
r2*3/2 fad''4 re'' mi'' fad'' re'' mi'' |
fad'' re'' mi'' |
re'' si'2 dod''2. |
fad''4 mi'' fad'' sol''2 sol''4 |
si'' mi'' mi'' dod''2. |
si''4 sol'' \ficta fad'' sol''2 sol''4 |
si'' la'' sol'' fad''2. fad''4 mi''4. fad''8 |
re''2 re''4 sol'' fad''4. sol''8 |
mi''2 mi''4 re'' la'' mi'' |
re'' la'' mi'' re'' la'' re'' |
sol''2 mi''4 re''2 re''4 |
re''2 dod''4 re'' re'' mi'' |
fad'' re'' mi'' |
fad'' re'' fad'' re'' dod''2 re''2. |
R1*2 |
R2.*2 |
r1*3/2 r2*3/2 fad''4 re'' mi'' |
fad'' re'' mi'' |
fad'' re'' mi'' re'' si'2 |
dod''2. fad''4 mi'' fad'' |
sol''2 sol''4 si'' mi'' mi'' dod''2. si''4 sol'' \ficta fad'' |
sol''2 sol''4 si'' la'' sol'' |
fad''2. fad''4 mi''4. fad''8 |
re''2 re''4 sol'' fad''4. sol''8 |
mi''2 mi''4 re'' la'' mi'' |
re'' la'' mi'' re'' la'' re'' |
sol''2 mi''4 re''2 re''4 |
re''2 dod''4 re''1*3/4\fermata |
