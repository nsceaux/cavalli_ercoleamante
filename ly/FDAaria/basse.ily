\clef "basse" re4 re sol, re re sol, |
re re fad, sol, la,2 |
re2. <<
  \tag #'basse { r2*3/2 | R2.*4 | r2*3/2 }
  \tag #'basse-continue {
    re2. |
    re re2 re4 |
    sol,2 la,4 si, la, sol, |
    la,2.
  }
>> re4 re sol, re re sol, |
re re dod |
re mi2 la,2. |
<<
  \tag #'basse {
    re2 re4 sol2 sol4 |
    mi2 mi4 fad2. |
    re4 re re sol2 sol4 |
    mi2 mi4
  }
  \tag #'basse-continue {
    re2. sol |
    mi fad |
    re sol |
    mi
  }
>> fad si, |
si4 la2 sol2. |
<<
  \tag #'basse { la2 la4 }
  \tag #'basse-continue { la2. }
>> re4 re sol, |
re re sol, re re fad, |
sol,2 la,4 si, sol,2 |
la,2. re4 re sol, |
re re sol, |
re re fad, sol, la,2 re2. |
<<
  \tag #'basse { R1*2 R2.*2 r1*3/2 r2*3/2 }
  \tag #'basse-continue {
    re1 |
    re |
    re2 re4 |
    sol2 re4 |
    sol4 la2 si4 la sol la2.
  }
>> re4 re sol, |
re re sol, |
re re dod re mi2 |
la,2. <<
  \tag #'basse {
    re2 re4 |
    sol2 sol4 mi2 mi4 fad2. re4 re re |
    sol2 sol4 mi2 mi4 |
    fad2. si, |
    si4 la2 sol sol4 |
    la2 la4
  }
  \tag #'basse-continue {
    re2. |
    sol mi fad re |
    sol mi |
    fad si, |
    si4 la2 sol2. |
    la
  }
>> re4 re sol, |
re re sol, re re fad, |
sol,2 la,4 si, sol,2 |
la,2. re1*3/4\fermata |
