\score {
  <<
    \new StaffGroup <<
      \new Staff << \global \includeNotes "partie1" >>
      \new Staff << \global \includeNotes "partie2" >>
      \new Staff << \global \keepWithTag #'basse \includeNotes "basse" >>
    >>
    \new Staff \with { \tinyStaff } \withLyrics <<
      \global \includeNotes "voix"
    >> { \set fontSize = -2 \includeLyrics "paroles" }
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
    >>
  >>
  \layout { }
}
