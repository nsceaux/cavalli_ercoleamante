\score {
  <<
    \new StaffGroup <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
      \new Staff << \global \includeNotes "partie1" >>
      \new Staff << \global \includeNotes "partie2" >>
      \new Staff << \global \keepWithTag #'basse \includeNotes "basse" >>
    >>
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2.*13\pageBreak
        s2.*12\pageBreak
        s2.*10\pageBreak
        s2.*4 s1*2 s2.*2\pageBreak
        s2.*13\pageBreak
        s2.*10\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
