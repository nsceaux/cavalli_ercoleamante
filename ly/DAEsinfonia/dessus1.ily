\clef "dessus" \original { r4 r } fa''4 |
re''2. r4 r sol'' |
mi''2. r4 r la'' |
fa'' sol'' la'' sib''2 sib''4 |
la''2 do'''4 la''2. |
r4 r sib'' sol''2. |
r4 r fa'' sol'' la'' sib'' |
sib''2 la''4 sib''1*3/4\fermata |
