\clef "basse" << \original R2. \pygmalion r4 >> |
r4 r sib sol2. |
r4 r do' la2 la4 |
sib2 la4 sol2. |
fa r4 r fa |
re2. r4 r mib |
do2 re4 mib mib sib, |
fa2. sib,1*3/4\fermata |
