\clef "viole2" << \original R2. \pygmalion r4 >> |
r4 r sib re'2. |
r4 r do' fa'2 fa'4 |
fa' sib do' re'2 sib4 |
fa'2. r4 r do' |
re'2. r4 r sib |
do'2 fa'4 mib' do' sib |
do'2 do'4 sib1*3/4\fermata |
