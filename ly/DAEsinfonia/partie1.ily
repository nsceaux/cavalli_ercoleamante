\clef "viole1" << \original R2. \pygmalion r4 >> |
r4 r fa' sol'2. |
r4 r sol' la'2 do''4 |
sib' sib' fa' sol'2 sol'4 |
do'2. r4 r la' |
sib'2. r4 r sol' |
la'2 sib'4 sol' mib' fa' |
fa'2 fa'4 fa'1*3/4\fermata |
