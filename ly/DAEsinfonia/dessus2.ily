\clef "dessus" << \original R2. \pygmalion r4 >> |
r4 r re'' sib'2. |
r4 r mi'' do''2 fa''4 |
re'' mi'' fa'' fa''2 mi''4 |
fa''2. r4 r la'' |
fa''2. r4 r sol'' |
mib''2 re''4 sib' do'' re'' |
do''2 do''4 re''1*3/4\fermata |
