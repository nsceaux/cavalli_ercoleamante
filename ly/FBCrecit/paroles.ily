E di che te -- mi, I -- o -- le, e di che te -- mi?

Ec -- co il mio vi -- ver giun -- to
a un for -- mi -- da -- bil pun -- to.

Deh sù sù por -- gi -- mi ar -- di -- ta
la ves -- te, ond’ io ben tos -- to
per i nos -- tri i -- me -- ne -- i
ren -- da o -- lo -- caus -- to a i Dè -- i.
