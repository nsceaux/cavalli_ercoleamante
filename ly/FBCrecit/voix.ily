\clef "vbasse" <>^\markup\character Ercole
r4 r8 la la la fa fa16 mi |
fa4 fa r8 fa re sib, |
fa4 fa
\ffclef "vsoprano" <>^\markup\character Iole
la'8 la'16 la' la'8 la' |
fa'4 fa'8 sib' sol' sol' sol' fa' |
re'4. dod'8( re'2) |
\ffclef "vbasse" <>^\markup\character Ercole
sol4 r8 si re'4 r |
re'8 sol16 sol sol8 sol16 fad sol8 sol16 sol sol8 la |
fa8 fa fa fa fa4 fa8 mi |
mi4 mi sol8 sol la \ficta sib |
la4 si8 do' mi( re4.) do1\fermata |
