Co -- sì co -- sì un gior -- no av -- ver -- rà con più di -- let -- to,
che del -- la Sen -- na in su la ri -- va al -- te -- ra
al -- tro gal -- li -- co Al -- ci -- de ar -- so d’af -- fet -- to
giun -- ga in pa -- ce a go -- der
\tag #'(bellezza basse) { bel -- lez -- za i -- be -- ra; }
\tag #'ercole { bel -- lez -- za bel -- lez -- za i -- be -- ra; }
ma noi dal ciel tra -- em vi -- ver vi -- ver vi -- ver gio -- con -- do
e per tal cop -- pia sia
\tag #'(bellezza basse) { be -- a -- to il mon -- do. __ }
\tag #'ercole { be -- a -- to be -- a -- to il mon -- do. }
