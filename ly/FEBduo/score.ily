\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \keepWithTag #'bellezza \includeNotes "voix"
    >> \keepWithTag #'bellezza \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'ercole \includeNotes "voix"
    >> \keepWithTag #'ercole \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*4\break s1*3 s2 \bar "" \break s2 s1*2 s2.*5\pageBreak
        s2.*8\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
