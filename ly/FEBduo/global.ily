\key fa \major \beginMark "A 2"
\time 4/4 \midiTempo#120 s1*8 \measure 2/1 s1*2
\digitTime\time 3/4 \midiTempo#160 s2.*6
\measure 6/4 s1. \measure 3/4 s2.*7 \measure 9/4 s2.*3
\pygmalion { \measure 12/4 s2.*4 }
\bar "|."
