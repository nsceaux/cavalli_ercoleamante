\clef "basse" sib1~ |
sib2 la |
sol fa |
mib re |
do sib, |
fa la4. si8 |
do'2 sol |
sib la |
sib4 sib, do2 fa,1 |
r4 fa re |
sib, mib do |
fa re2 |
mib2 do4 |
re2 sib,4 |
do2 do4 |
re2. sol, |
sol4 la2 |
sib4 sol2 fa2 re4 |
mib2 fa4 |
sol2 la4 |
sib2. |
re2 mib4 |
fa2 mib fa <<
  \original { sib,1*3/4\fermata | }
  \pygmalion {
    sib,1*3/4 |
    re2 sib,4 mi2. fa sib,1*3/4\fermata |
  }
>>
