<<
  \tag #'(bellezza basse) {
    \clef "vsoprano" <>^\markup\character Bellezza
    r2 r4 fa'' |
    fa''2 r4 fa''8 fa'' |
    mib''4 mib''8 re'' re'' re'' re'' do'' |
    do'' do''16 do'' do''8 re'' sib'4 sib'8 sib' |
    sib'8 la' la' sib' sib'4 sib' |
    fa'8 sol' la' la'16 sib' do''4 do'' |
    sol'4 sol'8 la' sib'4 sib' |
    fa'8 fa' fa' fa'16 sol' la'4. do''8 |
    re'' sol' la'4( sol'2) fa'1 |
    do''4 la' fa' |
    sib' sol'( do'') |
    la' sib'2 |
    sol'4 la'2 |
    fad'4 re''2~ |
    re''4 do''8[ sib'] la'[ sol'] |
    sib'4( la'2) sol'2. |
    R2. |
    re''4 mi'' mi'' fa''2 fa''4 |
    do''2 la'4 |
    sib'4.\melisma la'8[ sib' do''] |
    re''[ do'' sib' do'' re'' mi''] |
    fa''4 mib''8[ re'' do'' sib'] |
    la'4\melismaEnd fa' sib'2~ sib'4. la'8( sib'1*3/4)\fermata |
  }
  \tag #'ercole {
    \clef "vbasse" <>^\markup\character Ercole
    r4 sib sib2 |
    r4 sib8 sib la4 la8 la |
    sol sol sol fa fa fa16 fa fa8 sol |
    mib4 mib8 fa re re re mib! |
    do4 do sib,8 do re re16 mi |
    fa4 fa la la8 si |
    do'4 do' sol8 sol sol sol16 la |
    sib4. sib8 la4 la8 la |
    sib4 sib8 sib, do2 fa1 |
    r4 fa re |
    sib, mib do( |
    fa) re2 |
    mib do4 |
    re2 sib,4 |
    do4 do do |
    re2. sol, |
    sol4 la la |
    sib2 sib4 |
    fa2 re4 |
    mib4.\melisma re8[ mib fa] |
    sol[ fa mib fa sol la] |
    sib4 la8[ sol fa mib] |
    re4\melismaEnd re mib |
    fa2 mib fa sib,1*3/4\fermata |
  }
>>
\pygmalion s2.*4
