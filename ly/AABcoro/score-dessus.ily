\score {
  <<
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'coro11 \includeNotes "voix"
      >> \keepWithTag #'coro11 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'coro12 \includeNotes "voix"
      >> \keepWithTag #'coro12 \includeLyrics "paroles"
    >>
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'coro21 \includeNotes "voix"
      >> \keepWithTag #'coro21 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'coro22 \includeNotes "voix"
      >> \keepWithTag #'coro22 \includeLyrics "paroles"
    >>
  >>
  \layout { }
}
