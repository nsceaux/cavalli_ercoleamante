Qual
qual con -- cor -- so in -- do -- vi -- no
og -- gi al mar più vi -- ci -- no
del fes -- to -- so
\tag #'(coro11 basse coro12 coro13 coro14) { fes -- to -- so }
fes -- to -- so Pa -- ri -- gi
noi ra -- u -- nò dal ge -- mi -- no e -- mis -- fe -- ro,
noi, che del fran -- co im -- pe -- ro
van -- tia -- mo il no -- bil gio -- go, o i bei ves -- ti -- gi?
