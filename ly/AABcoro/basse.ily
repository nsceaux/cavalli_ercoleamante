\clef "basse" do1 |
r4 do sol |
la mi2 |
fa2. fa2 mi4 |
fa mi4. re8 |
do2. do2 sol,4 |
do2 si,4 |
do2 si,4 do2 sol,4 |
do2 re4 |
mi2 mi4 fad2. |
sol2. sol, |
sold2~ sold |
la2 sol |
fa re4 dod |
mi1 la,\fermata |
