<<
  \tag #'(coro11 basse) {
    <>^\markup\character Coro di Fiumi
    \clef "vsoprano" mi''1 |
    r4 mi'' re'' |
    do''4 si'4. do''8 |
    la'2. la'4 do'' do'' |
    fa'' do''4. re''8 |
    mi''2. mi''4 do'' re'' |
    do''4. mi''8 si'4 |
    mi''4. mi''8 re''4 do'' do'' re'' |
    mi'' mi''8 mi'' re'' re'' |
    do''2 mi''4 re''4. re''8 re'' re'' |
    si'2. si' |
    mi''2 mi''8 mi'' mi'' mi'' |
    do''4 do''8 do'' mi'' mi'' mi'' mi'' |
    fa''4 fa''8 fa'' fa''4 mi'' |
    mi''1 mi''\fermata |
  }
  \tag #'coro12 {
    \clef "valto" sol'1 |
    r4 sol' si' |
    la' sol'4. sol'8 |
    fa'2. fa'4 la' mi' |
    la' mi'4. fa'8 |
    sol'2. sol'4 sol' sol' |
    sol'4. sol'8 sol'4 |
    sol'4. sol'8 sol'4 sol' sol' si' |
    sol' sol'8 sol' fa' la' |
    sol'2 mi'4 la'4. la'8 la' la' |
    sol'2. sol' | \allowPageTurn
    si'2 si'8 si' si' si' |
    la'4 la'8 mi' sol' sol' sol' sol' |
    la'4 la'8 la' la'4 la' |
    sold'\melisma la'2 sold'4\melismaEnd la'1\fermata |
  }
  \tag #'coro13 {
    \clef "vtenor" do'1 |
    r4 mi' si |
    do' mi'4. do'8 |
    do'2. do'4 do' do' |
    do' do'4. fa8 |
    do'2. do'4 do' si |
    mi'4. mi'8 re'4 |
    sol4 sol4. re'8 mi'4. mi'8 si4 |
    do'4 do'8 sol re' re' |
    sol2 do'4 la4. la8 re' re' |
    re'2. re' |
    r4 si si8 si mi' mi' |
    mi'4 mi'8 do' si si si sol |
    re'2 re'4 mi' |
    si la si( mi') dod'1\fermata |
  }
  \tag #'coro14 {
    \clef "vbasse" do1 |
    r4 do sol |
    la mi4. mi8 |
    fa2. fa4 fa mi |
    fa mi4. re8 |
    do2. do4 mi sol |
    mi do si, |
    do4 do sol mi do sol, |
    do do8 do re re |
    mi2 mi4 fad4. fad8 fad fad |
    sol2. sol |
    sold2 sold8 sold sold sold |
    la4 la8 la sol sol sol sol |
    fa4 fa8 fa re4 dod |
    mi1 la,\fermata |
  }
  \tag #'coro21 {
    \clef "vsoprano" do''1 |
    r4 do'' si' |
    mi'' mi''4. mi''8 |
    do''2. do''4 la' mi'' |
    do'' mi''4. si'8 |
    do''2. do''2 r4 |
    r do'' re'' |
    do''4. mi''8 si'4 mi''4. mi''8 re''4 |
    do'' do''8 do'' la' la' |
    mi''2 mi''4 la'4. la'8 la' la' |
    re''2. re'' |
    si'2 si'8 si' si' si' |
    mi''4 mi''8 mi'' si' si' si' si' |
    re''4 re''8 re'' re''[ la'] dod''4 |
    si'4\melisma do'' si'2\melismaEnd dod''1\fermata |
  }
  \tag #'coro22 {
    \clef "valto" mi'1 |
    r4 mi' sol' |
    mi' mi'4. mi'8 |
    la'2. la'4 fa' sol' |
    fa' sol'4. fa'8 |
    mi'2. mi'2 r4 |
    r sol' sol' |
    sol'4. sol'8 sol'4 do' do' sol' |
    mi'4 mi'8 mi' fa' fa' |
    mi'2 sol'4 fad'4. fad'8 fad' fad' |
    re'2. re' |
    mi'2 sold'8 sold' sold' sold' |
    mi'4 mi'8 mi' mi' mi' mi' mi' |
    re'8.[\melisma mi'16] fa'4\melismaEnd fa' r8 la' |
    mi'4 do' mi'2 mi'1\fermata |
  }
  \tag #'coro23 {
    \clef "vtenor" sol1 |
    r4 do' re' |
    la si4. sol8 |
    la2. la4 la re' |
    la sol4. re'8 |
    sol2. sol2 r4 |
    r sol sol |
    do'4. do'8 re'4 sol sol sol |
    sol sol8 do' fa fa |
    do'2 sol4 re'4. re'8 la la |
    si2. si |
    si2 mi'8 mi' si si |
    do'4 do'8 la si mi mi mi |
    la2 la4 la |
    si mi' si2 la1\fermata |
  }
  \tag #'coro24 {
    \clef "vbasse" do1 |
    r4 do sol |
    la mi4. mi8 |
    fa2. fa4 fa mi |
    fa4 mi4. re8 |
    do2. do2 r4 |
    r mi sol |
    mi4 do si, do do sol, |
    do do8 do re re |
    mi2 mi4 fad4. fad8 fad fad |
    sol2. sol |
    sold2 sold8 sold sold sold |
    la4 la8 la sol sol sol sol |
    fa4 fa8 fa re4 dod |
    mi1 la,\fermata |
  }
>>
