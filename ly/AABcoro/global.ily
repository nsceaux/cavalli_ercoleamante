\key do \major
\time 4/4 \midiTempo#160 s1
\digitTime\time 3/4 s2.*2
\measure 6/4 s2.*2
\measure 3/4 s2.
\measure 6/4 s2.*2
\measure 3/4 s2.
\measure 6/4 s2.*2
\measure 3/4 s2.
\measure 6/4 s1.*2
\time 4/4 s1*3
\measure 2/1 s1*2 \bar "|."
