A -- mor con -- tar ben puo -- i
fra tuoi non mi -- nor van -- ti
che dell’ ar -- dir, che tor -- re a me non sep -- pe
co’ la -- tra -- ti di Cer -- be -- ro, et or -- ren -- di
stre -- pi -- ti suoi lo spa -- ven -- to -- so a -- bis -- so;
tu di -- sar -- ma -- to m’hai, sì ch’io, che col -- si
ad on -- ta del ter -- ri -- bi -- le cus -- to -- de,
con in -- tre -- pi -- da man l’Es -- pe -- rie frut -- ta,
qua -- si di sos -- te -- ner or non ar -- dis -- co
l’av -- vi -- ci -- nar del bel per cui per cui per cui lan -- guis -- co.

O qua -- le ins -- til -- la -- no
in ar -- so pet -- to
rai, che sfa -- vil -- la -- no
di gran bel -- tà,
u -- mil ris -- pet -- to,
bas -- sa bas -- sa u -- mil -- tà:
il ciel ben sa
a sì su -- pre -- ma
a -- do -- ra -- bi -- le ma -- es -- tà,
s’ei pur non tre -- ma?
