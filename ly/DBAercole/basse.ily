\clef "basse" sol1 |
sol4 fa mib2~ |
mib4 re do2 |
do1~ |
do2 sib,~ |
sib, sol,~ |
sol, \sug mi, |
\sug fa,1 |
fa2 re~ |
re1~ |
re |
mib~ |
mib2 fa~ |
fa1 |
sol2 do |
fa la, |
sib, re, |
mib, do, |
fa, mi,! |
fa,1 \sug sib, |
sib4 la2 |
sol2. |
sol4 fa2 mib2. |
mib4 re2 |
do2. |
sib, |
sib,4 re mi fa2. |
sib, |
la, |
sol, |
fad,4. sol,8 la,4 << \original re2. \pygmalion re,2. >> |
re, |
sol,4 sol sol |
do2 do4 |
fa mib2 re4 do2 |
sib, la,4 sib, sol,2 |
re2. |
si, do |
do4 re2 sol,1*3/4\fermata |
