\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*4 s2 \bar "" \break s2 s1*3\pageBreak
        s1*4\break s1*4\break s1*5 s2.*5\break s2.*10\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
