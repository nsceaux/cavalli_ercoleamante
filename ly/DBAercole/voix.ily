\clef "vbasse" <>^\markup\character Ercole
r4 sol sol2 |
r8 sol fa sol mib4 mib8 mib |
\ficta mib mib re mib do4 do |
r8 do do re mib4. mib8 |
mib fa sol la sib sib sib, fa |
re mib16 fa sib,8. sib,16 sib,8 sib,16 do re8 re |
mi8 mi16 sol mi8 mi mi mi mi fa |
fa,4 fa, fa r |
fa8 fa fa fa sib2 |
lab4 lab8 lab lab4 lab8 sib |
fa8 fa fa fa sib8. sib16 lab8 sol |
sol4 sol sol8 sol sol sol16 sol |
do'8 do' do' sol la4 la |
la8 la16 la la8 la re'4 re'8 re'16 la |
sib4 sib8 sib sib sib sib la |
la2 r4 fa |
re2 r4 sib |
sol2 r4 mib |
mib re reb2( |
do1) sib, |
sib4 la sib |
sol4. fad8 sol4 |
sol4 fa sol mib2 mib4 |
mib re4. mib8 |
do[\melisma re mib fa sol la] |
sib4.\melismaEnd sib8 sib4 |
sib, re mi fa2. |
sib,4 sib, la, |
la,2 la,4 |
sol,2 sol,4 |
fad,4. sol,8 la,4 << \original re2. \pygmalion re,2. >> |
la4 re' re' |
sol2. |
sol4 do' sib |
la2 sol4 fa2 mib4 |
re4. re8 do4 re mi2 |
fad2. |
sol mib |
sol4 re2 sol,1*3/4\fermata |
