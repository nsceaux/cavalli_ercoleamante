\clef "basse" r4 do re
mi4. re8 do4 |
sol sol,2
do2. |
r4 do' si
la2 mi4 |
fa2 do4
sol2. |
sol
do |
fa2 do4
sol la si |
do'2 si4
la2 fa4~ |
fa mi re~
re mi2 |
la,2.
la4 fa mi |
re4 sol,2
do4 do'4. si8 |
la4 sol4. fa8
mi2 fa4 |
sol la2
sol mi4 |
do sol,2
do2. |
