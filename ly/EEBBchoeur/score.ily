\score {
  <<
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix11 \includeNotes "voix"
      >> \keepWithTag #'voix11 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix12 \includeNotes "voix"
      >> \keepWithTag #'voix12 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix13 \includeNotes "voix"
      >> \keepWithTag #'voix13 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix14 \includeNotes "voix"
      >> \keepWithTag #'voix14 \includeLyrics "paroles"
    >>
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix21 \includeNotes "voix"
      >> \keepWithTag #'voix21 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix22 \includeNotes "voix"
      >> \keepWithTag #'voix22 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix23 \includeNotes "voix"
      >> \keepWithTag #'voix23 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix24 \includeNotes "voix"
      >> \keepWithTag #'voix24 \includeLyrics "paroles"
    >>
    \new Staff <<
      \global \includeNotes "basse"
      \origLayout {
        s2.*9\break s2.*9\pageBreak
      }
    >>
  >>
  \layout { indent = \noindent }
  \midi { }
}
