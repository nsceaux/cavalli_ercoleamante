<<
  \tag #'voix11 {
    \clef "treble" r4 mi'' fa''
    sol''4. fa''8 mi''4 |
    mi'' re''( mi''8)[ fa'']
    mi''2 do''4 |
    r mi'' re''
    do''2 si'4 |
    la'2 do''4
    si'2\trill si'4 |
    sol'' fa'' sol''
    mi''( fa'') sol'' |
    do''\melisma re'' mi''
    re''2.\melismaEnd |
    sol'4 la' si'
    do''2 do''4~ |
    do'' mi'' fa''~
    fa'' mi''2 |
    mi''2.
    mi''4 fa'' sol'' |
    fa''4 re''2
    mi''4.\melisma fa''8[ mi'' re''] |
    do''4 sol''2~
    sol''\melismaEnd fa''8[ mi'']( |
    re''4) do''2
    re''2. |
    mi''4( re''2)
    do''2. |
  }
  \tag #'voix12 {
    \clef "treble" r4 sol' re''
    do''4. fa'8 sol'4 |
    sol' sol'2
    sol' sol'4 |
    r do'' sol'
    la'2 sol'4 |
    fa'2 sol'4
    sol'2 sol'4 |
    r2*3/2
    sol'4 fa' mi' |
    la'2 sol'4
    sol'2.~ |
    sol'4 fa' re'
    mi'2 la'4~ |
    la' sold' la'~
    la' la'4. sold'!8\melisma |
    la'2.\melismaEnd
    la'4 la' do'' |
    fa'4 sol'2 sol'\melisma do''8[ sol'] |
    la'4\melismaEnd si'4. la'8 sol'4 do''8([ si'] la'4) |
    sol'2 fad'4 sol'2.~ |
    sol' sol' |
  }
  \tag #'voix13 {
    \clef "G_8" r4 do' fa' mi'4. la8 do'4~ |
    do'8 do' re'4( do'8)[ si] do'2 do'4 |
    r4 do' re' mi'( do') sol |
    la2 mi'4 re'2 re'4 |
    R1. |
    r2*3/2 re'4 do' si |
    mi' re'2 do'2. |
    r4 r re' re'~ re'8[\melisma do'] si4\melismaEnd |
    dod'2. do'!4 fa' do' |
    re'2\melisma sol'8[ fa']\melismaEnd mi'[ re'] do'2~ |
    do'4 sol4. la8 do'2 re'4~ |
    re' la do' si4.\melisma la8\melismaEnd sol4 |
    sol2. sol |
  }
  \tag #'voix21 {
    \clef "treble" r4 do''  la'
    do''4. do''8 do''4 |
    do''4 si'( do''8)[ re'']
    do''2 do''4 |
    r do'' sol''
    mi''2 mi''4 |
    do''2 mi''4
    re''2 re''4 |
    sol' la' si'
    do''2 si'4 |
    la'\melisma si' do''\melismaEnd
    si' do'' re'' |
    do''2.
    r4 r la'~ |
    la' si' do''
    re''8[ do'']( si'2)\trill |
    la'2.
    do''4 do'' do'' |
    do''2 si'4
    do''4.\melisma re''8[ do'' re''] |
    mi''2.~
    mi''2\melismaEnd re''8[ do'']( |
    si'4) la'2
    si'2. |
    do''2 si'4(
    do''2.) |
  }
  \tag #'voix22 {
    \clef "treble" r4 mi' re' sol'4. la'8 mi'4 |
    sol'4 re'2 mi'2 mi'4 |
    r sol' re'' la'2 mi'4 |
    la'2 sol'4 sol'2 sol'4 |
    r2*3/2 do'4 re' mi' |
    fa'2 mi'4 re'2. |
    mi'4 fa' sol' la'2. |
    r4 r fa' la'\melisma mi'2\melismaEnd |
    mi'2. mi'4 la' sol' |
    la'4 sol'2 sol'\melisma la'8[ si'] |
    do''4\melismaEnd si'8[ la'] sol'4 sol'2 re'4\melisma |
    sol'8[ fa']\melismaEnd mi'4 do' sol'8[\melisma fa'16 mi'] re'2\melismaEnd |
    sol'4 sol'4.\melisma re'8\melismaEnd mi'2. |
  }
  \tag #'voix23 {
    \clef "G_8" r4 sol fa do'4. re'8 sol4 |
    sol4 sol2 sol2 sol4 |
    r4 mi' si do'2 sol'4 |
    do' do'2 re' re'4 |
    R1. |
    do'4 si sol si( la) do' |
    do'4.\melisma do'8 re'4 la2\melismaEnd do'8([ si] |
    la4) mi' la la2( mi4) |
    mi2. la4 do' do' |
    la8[ re'] re'2 do'4\melisma mi'8[ re'] mi'4 |
    la4\melismaEnd mi' mi' mi'2 la4 |
    si do'( la) re'2\melisma sol'4\melismaEnd |
    do' re'( sol) sol2. |
  }
  \tag #'voix14 {
    \clef "bass" r4 do re
    mi4. re8 do4 |
    sol sol,2
    do2 do4 |
    r4 do' si
    la2 mi4 |
    fa2 do4
    sol2 sol4 |
    R1. |
    r2*3/2
    sol4 la si |
    do'2 si4
    la2 fa4~ |
    fa mi re~
    re mi2 |
    la,2.
    la4 fa mi |
    re4 sol2
    do4\melisma do'4. si8 |
    la4\melismaEnd sol4. fa8
    mi2 fa4 |
    sol la2
    sol4.\melisma fa8[ mi re] |
    do4 sol2\melismaEnd
    do2. |
  }
  \tag #'voix24 {
    \clef "bass" r4 do re
    mi4. re8 do4 |
    sol sol,2
    do2 do4 |
    r4 do' si
    la2 mi4 |
    fa2 do4
    sol2 sol4 |
    R1. |
    r2*3/2
    sol4 la si |
    do'2 si4
    la2 fa4~ |
    fa mi re~
    re mi2 |
    la,2.
    la4 fa mi |
    re4 sol,2
    do4\melisma do'4. si8 |
    la4\melismaEnd sol4. fa8
    mi2 fa4 |
    sol la2
    sol4.\melisma fa8[ mi re] |
    do4 sol,2\melismaEnd
    do2. |
  }
>>