Su, su lie -- ti in
\tag #'(voix11 voix13 voix21) { no -- bil __ }
\tag #'(voix12 voix14 voix22 voix23 voix24) { no -- bil }
ga -- ra
o -- gni nu -- me col suo lu -- me 
as -- pi -- ri a ce -- le -- brar __
\tag #'(voix11 voix21 voix22) { a ce -- le -- brar }
\tag #'(voix12) { ce -- le -- brar }
\tag #'(voix11 voix12 voix14 voix21 voix23 voix24) { de -- a }
si chia -- ra
as -- pi -- ri a ce -- le -- brar __
\tag #'(voix12 voix13 voix14 voix22 voix23 voix24) { ce -- le -- brar }
de -- a si \tag #'(voix13 voix22 voix23) { chia -- ra, } chia -- ra.
