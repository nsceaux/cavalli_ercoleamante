\clef "basse" do2 sol, |
do re |
sol, sol, |
do1 |
fa, |
sol, |
la,2 la, |
re1 |
re2 sol,~ |
sol, sol, |
do1 |
re2 sol, |
re1 |
sol,\breve*1/2 |
