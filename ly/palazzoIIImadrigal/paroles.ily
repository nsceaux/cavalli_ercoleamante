\tag #'(voix1 basse) {
  Vin -- cer gl’in -- gan -- ni,
  e tri -- on -- far __ d’A -- ver -- no,
  e tri -- on -- far __
  e tri -- on -- far d’A -- ver -- no,
  e tri -- on -- far __ d’A -- ver -- no,
  d’A -- ver -- no.
}
\tag #'voix2 {
  Vin -- cer gl’in -- gan -- ni,
  e tri -- on -- far __
  e tri -- on -- far __ d’A -- ver -- no,
  e tri -- on -- far __ d’A -- ver -- no,
  d’A -- ver -- no.
}
\tag #'voix3 {
  Vin -- cer gl’in -- gan -- ni,
  e tri -- on -- far __ d’A -- ver -- no,
  e tri -- on -- far d’A -- ver -- no,
  e tri -- on -- far
  e tri -- on -- far __ d’A -- ver -- no.

}
\tag #'voix4 {
  Vin -- cer gl’in -- gan -- ni,
  e tri -- on -- far __ d’A -- ver -- no,
  e tri -- on -- far
  e tri -- on -- far __ d’A -- ver -- no,
  e tri -- on -- far __ d’A -- ver -- no,
  e tri -- on -- far __ d’A -- ver -- no.
}
\tag #'voix5 {
  Vin -- cer gl’in -- gan -- ni,
  e tri -- on -- far __ d’A -- ver -- no,
  e tri -- on -- far __
  e tri -- on -- far __ d’A -- ver -- no,
  d’A -- ver -- no.
}
\tag #'voix6 {
  Vin -- cer gl’in -- gan -- ni,
  e tri -- on -- far __
  e tri -- on -- far __ d’A -- ver -- no,
  e tri -- on -- far __
  e tri -- on -- far __ d’A -- ver -- no,
  d’A -- ver -- no.
}
\tag #'voix7 {
  Vin -- cer gl’in -- gan -- ni,
  e tri -- on -- far __
  e tri -- on -- far __ d’A -- ver -- no,
  e tri -- on -- far __
  e tri -- on -- far __ d’A -- ver -- no,
  e tri -- on -- far __ d’A -- ver -- no.

}
\tag #'voix8 {
  Vin -- cer gl’in -- gan -- ni,
  e tri -- on -- far __ d’A -- ver -- no,
  e tri -- on -- far __ d’A -- ver -- no, __
  e tri -- on -- far
  e tri -- on -- far __ d’A -- ver -- no,
  d’A -- ver -- no.
}
\tag #'voix9 {
  Vin -- cer gl’in -- gan -- ni,
  e tri -- on -- far __ d’A -- ver -- no,
  d’A -- ver -- no,
  e tri -- on -- far __
  e tri -- on -- far __
  e tri -- on -- far __ d’A -- ver -- no.
}
\tag #'voix10 {
  Vin -- cer gl’in -- gan -- ni,
  e tri -- on -- far __ d’A -- ver -- no,
  d’A -- ver -- no,
  e tri -- on -- far __ d’A -- ver -- no,
  d’A -- ver -- no.
}
