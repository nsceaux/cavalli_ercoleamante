\score {
  <<
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix3 \includeNotes "voix" \clef "alto"
      >> \keepWithTag #'voix3 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix4 \includeNotes "voix" \clef "alto"
      >> \keepWithTag #'voix4 \includeLyrics "paroles"
    >>
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix8 \includeNotes "voix" \clef "alto"
      >> \keepWithTag #'voix8 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix9 \includeNotes "voix" \clef "alto"
      >> \keepWithTag #'voix9 \includeLyrics "paroles"
    >>
  >>
  \layout { }
}
