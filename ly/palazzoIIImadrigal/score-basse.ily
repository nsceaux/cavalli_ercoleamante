\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \keepWithTag #'voix5 \includeNotes "voix"
    >> \keepWithTag #'voix5 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'voix10 \includeNotes "voix"
    >> \keepWithTag #'voix10 \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
    >>
  >>
  \layout { }
}
