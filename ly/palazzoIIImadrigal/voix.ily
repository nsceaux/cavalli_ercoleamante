<<
  \tag #'(voix1 basse) {
    \clef "vsoprano" mib''4 mib''8 mib'' re''4 re'' |
    r2 r8 re'' re'' re'' |
    re''16[\melisma mi'' re'' do''] si'[ do'' si' la'] sol'2~ |
    sol'4\melismaEnd sol' sol'2 |
    la'8 la' la' la' la'16[\melisma sib' la' sol'] fa'[ mi' re' do'] |
    sol'2\melismaEnd r8 re'' re'' mi'' |
    fa''4 re'' mi''2 |
    re''1 |
    r4 r8 re'' re'' re'' re''16[\melisma mi'' re'' do''] |
    sib'[ do'' sib' la'] sol'4\melismaEnd r4 sol' |
    sol'2 do'' |
    r re'' |
    re''1 |
    si'\breve*1/2\fermata |
  }
  \tag #'voix2 {
    \clef "vsoprano" do''4 do''8 do'' sib'4 sib' |
    R1*2 |
    r8 do'' do'' do'' do''16[\melisma sib' do'' re''] mi''[ do'' re'' mi''] |
    fa''2\melismaEnd r8 do'' do'' do'' |
    sib'16[\melisma la' sib' do''] re''[ do'' re'' mi''] do''2\melismaEnd |
    r8 dod'' la'4.\melisma si'8 dod''4\melismaEnd |
    la'1 |
    r2 r8 sib' sib' sib' |
    sib'16[\melisma la' sib' do''] re''2\melismaEnd re''4 |
    do''2 sol' |
    r4 re'' sib'8[\melisma do''] re''[ do''16 sib'] |
    la'1\melismaEnd |
    sol'\breve*1/2\fermata |
  }
  \tag #'voix3 {
    \clef "valto" do'4 do'8 do' re'4 re' |
    R1 |
    r8 sol' sol' sol' sol'16[\melisma la' sol' fa'] mi'[ fa' mi' re'] |
    do'2.\melismaEnd do'4 |
    do'2 fa' |
    r4 re' re' re' |
    la' fa' mi'8[\melisma fa'16 sol'] la'4\melismaEnd |
    fad' re' re' re' |
    re'1 |
    r2 r8 sol' sol' sol' |
    sol'16[\melisma la' sol' fa'] mib'[ fa' mib' re'] do'4\melismaEnd mib'! |
    re'4.\melisma mi'16[ \ficta fad'] sol'2~ |
    sol'4 fad'8[ mi'] re'2\melismaEnd |
    re'\breve*1/2\fermata |
  }
  \tag #'voix4 {
    \clef "vtenor" sol4 sol8 sol sol4 sol |
    r8 re' re' re' re'16[\melisma mi' re' do'] sib[ do' sib la] |
    sol2\melismaEnd sol |
    do4.\melisma re8\melismaEnd mi2 |
    r8 fa la fa do'2 |
    r8 sol sol sol sol16[\melisma fa sol la] sib4\melismaEnd |
    r4 la la2 |
    re r8 fad fad fad |
    \ficta fad16[\melisma mi fad sol] la[ fad sol la] sib2~\melismaEnd |
    sib4 sib sib2 |
    sol8 do' do' do' mib'16[\melisma fa' mib' re'] do'[ re' do' sib] |
    la4\melismaEnd re' re'2\melisma |
    re1\melismaEnd |
    re\breve*1/2\fermata |
  }
  \tag #'voix5 {
    \clef "vbasse" do4 do8 do sol,4 sol, |
    R1*3 |
    r8 fa fa fa fa16[\melisma sol fa mi] re[ do sib, la,] |
    sol,2\melismaEnd sib, |
    la, la,8 la la la |
    la16[\melisma sib la sol] fad[ sol fad mi] re4\melismaEnd r8 re |
    re re re16[\melisma mi re do] sib,[ do sib, la,] sol,4~ |
    sol,4\melismaEnd sol, sol2 |
    do1 |
    r2 re |
    re1 |
    sol,\breve*1/2\fermata |
  }
  
  \tag #'voix6 {
    \clef "vsoprano" r2 re''4 re''8 re'' |
    re''4 re'' r2 |
    R1 |
    r2 r8 do'' do'' do'' |
    do''16[\melisma re'' do'' sib'] la'[ sib' la' sol'] fa'2\melismaEnd |
    r8 sib' sib' sib' sib'16[\melisma do'' sib' la'] sol'4\melismaEnd |
    r4 la' la' la'8 la' |
    la' la' la'16[\melisma sib' la' sol'] \ficta fad'[ mi' re' do'] re'4~ |
    re'8\melismaEnd re'' re'' re'' re''16[\melisma mi'' re'' do''] sib'[ do'' sib' la'] |
    sol'2.\melismaEnd sol'4 |
    sol'2 sol' |
    r4 la' sib'8[\melisma la' sib' do''] |
    re''1\melismaEnd |
    re''\breve*1/2\fermata |
  }
  \tag #'voix7 {
    \clef "vsoprano" r2 sib'4 sib'8 sib' |
    la'4 la' r2 |
    R1*2 |
    r8 fa' fa' fa' fa'16[\melisma mi' fa' sol'] la'[ fa' sol' la'] |
    sib'2\melismaEnd r8 sib' sib' sib' |
    fa'16[\melisma mi' fa' sol'] la'8\melismaEnd la' la'[\melisma sol'16 fa'] mi'4\melismaEnd |
    fad'8 fad' fad' fad' fad'16[\melisma sol' fad' mi'] re'[ mi' re' do'] |
    re'2\melismaEnd r8 re'' re'' re'' |
    re''16[\melisma mi'' re'' do''] sib'[ do'' sib' la'] sol'4\melismaEnd re'' |
    mib''2. mib''4 |
    r8 re'' re'' re'' re''8[ do''] sib'[ la'16 sol']\melisma |
    la'2\melismaEnd re' |
    re'\breve*1/2\fermata |
  }
  \tag #'voix8 {
    \clef "valto" r2 sol'4 sol'8 sol' |
    fad'4 fad' r2 |
    r2 r8 sol' sol' sol' |
    sol'16[\melisma la' sol' fa'] mi'[ fa' mi' re'] do'2~ |
    do'4\melismaEnd la do'2 |
    sol8 sol' sol' sol' sol'16[\melisma la' sol' fa'] mi'[ fa' mi' re'] |
    dod'4\melismaEnd la8 mi'2 dod'8\melisma |
    re'2\melismaEnd r4 re' |
    re' re' re'2 |
    r4 r8 sol' sol' sol' sol'16[\melisma la' sol' fa'] |
    \ficta mib'[ fa' mib' re'] do'2\melismaEnd sol'4 |
    fad'2 re'4 sol' |
    sol'2\melisma fad'\melismaEnd |
    sol'!\breve*1/2\fermata |
  }
  \tag #'voix9 {
    \clef "vtenor" r2 re'4 re'8 re' |
    re'4 re' r2 |
    R1 |
    r8 mi' mi' mi' mi'16[\melisma fa' mi' re'] do'[ re' do' sib] |
    la2.\melismaEnd la4 |
    re2 sol4 sol |
    la2 la |
    r8 la la la la16[\melisma sib la sol] fad[ sol fad mi] |
    re2\melismaEnd r8 sol sol sol |
    sol16[\melisma fa sol la] sib[ la sib do'] re'2\melismaEnd |
    r2 r8 do' do' do' |
    do'[\melisma sib] la4\melismaEnd sol2 |
    la1 |
    si\breve*1/2\fermata |
  }
  \tag #'voix10 {
    \clef "vbasse" r2 sol4 sol8 sol |
    re4 re r2 |
    R1 |
    r8 do do do do16[\melisma re do sib,] la,[ sib, la, sol,] |
    fa,2.\melismaEnd fa,4 |
    sol,2 sol, |
    r4 la, la,2 |
    re r8 re re re |
    re16[\melisma mi re do] sib,[ do sib, la,] sol,2~ |
    sol,\melismaEnd sol, |
    do1 |
    re2 sol, |
    re1 |
    sol,\breve*1/2\fermata |
  }
>>
