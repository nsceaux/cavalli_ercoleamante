\clef "basse" re1 |
re |
re |
re |
sol, |
mib |
do2 re |
sol,~ sol, |
<< \original mi1 \pygmalion { mib2 mi2 } >> |
fa2 fad |
sol mib |
\ficta mib fa |
sib, fa~ |
fa4 mi fa2 |
la sib4 la |
sol do re sol, |
sol2 mib |
re1 |
sol,~ |
sol,2 sib |
la1 |
la | \allowPageTurn
re'2 sib~ |
sib1 |
la |
fad~ |
fad |
sol |
fa4 mib re2 |
re' do' |
sib la~ |
la fa8 sol la4 |
re2~ re |
sib,1 |
sol,2 fa, |
fa,1 |
sib,~ |
sib,~ |
sib, |
mib |
<< \original mib1 \pygmalion { mib2 re } >> |
do2 sib, |
la,2. sib,4 |
do2~ do |
la,4 sib, do2 |
fa,1 |
