\ffclef "vbas-dessus" <>^\markup\character Iole
r4 r8 la' fad'2 |
r8 re'' la' la' la'4 la'8 la' |
la'4 la'8 la' sib' do'' do'' do'' |
do'' do'' do'' do''16 do'' do''8 do'' do'' do''16 re'' |
sib'4 sib' sib' sib'8 sib' |
sol' fad' sol'4 sol'8 la' la' la' |
do'' sib' sol'2 fad'4( |
sol'2)
\ffclef "vtenor" <>^\markup\character Hyllo
r8 re' sib mib' |
do' do' r4 r do'8 do' |
la4 la r re'8 re' |
sib4 sib r8 mib' re' do' |
sib sib r4 r2 |
r r8 la8 do'4 |
sol4 sol8 fa la4 la |
fa'8 mib' mib'16 mib' mib' re' re'8 re'16 fa' do' do' do' re' |
mib'8 mib' sol' do'16 sib sol8 sol r4 |
\ffclef "vbasse" <>^\markup\character Ercole
sib4 sol r8 sib sol la |
fad fad r re la la la sib |
sol sol r re re re sib, sib, |
sol, sol, r4 sib8 sib16 sib do'8 re' |
la la r4
\ffclef "vtenor" <>^\markup\character Hyllo
dod'8 dod' r dod'16 la |
mi'8 mi' r16 mi' mi' re' mi'8 mi' r re' |
fa' fa' r16 fa' fa' mi' sol'8 sol' r4 |
sol'8 sol' re' re'16 re' re'8 re' re' dod' |
mi'8 mi' r dod' dod' dod' dod' re' |
re'8. re'16 re'8 re' la la16 la la la la sib |
do'8 do'16 do' do' do' do' sib do'8 do' r16 do' re' la |
sib4 sib sib8 sib sib do'16 re' |
la8 la16 re' sol8. do'16 la8 la r4 |
fa'16 re' fa' mi' mi'8 mi'16 mi' mi'8 mi'16 mi' mi'8 fa' |
sol'16 sol' fa' mi' re'8 re'16 mi' dod'8 dod' r mi' |
dod'16 dod' dod' dod' la8. la16 la8 re' la la |
\ffclef "vbas-dessus" <>^\markup\character Iole
r2 r8 re'' la' sib' |
sib'4 sib' r fa'' |
sib'8. sib'16 sib'8 mi''! do'' do'' r4 |
r8 do''16 do'' la'8 la'16 do'' fa'8 fa' r4 |
\ffclef "vbasse" <>^\markup\character Ercole
r4 r8 re fa4 fa8 fa |
fa mib fa fa r4 fa |
fa8 fa fa fa fa fa fa fa16 sol |
mib4 mib r2 |
sol8 sol r4 sol8 sol sol sol |
la la la sib sib sib16 sib sib8 la |
do'4 do' la la8 sol |
sol4 sol
\ffclef "vtenor" <>^\markup\character Hyllo
r8 mi' do' do' |
r8 fa'16 mib' re'8 do'16 sib la8 sol sol la |
fa4 fa r2 |
