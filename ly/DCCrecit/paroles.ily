Ma qual? ma co -- me io sen -- to
spun -- ta -- re en -- tro il mio pet -- to
per te im -- prov -- vi -- so, e in -- vo -- lon -- ta -- rio af -- fet -- to
on -- de forz’ è ch’io t’a -- mi
e ch’a -- mor mio ti chia -- mi. __

Ohi -- mè, ch’as -- col -- to!
E non so -- gno? e son des -- to? e non già stol -- to?
Co -- sì can -- gia -- si I -- o -- le?
Fra -- gil fem -- mi -- ne -- a fe -- de;
ben mer -- ta i tra -- di -- men -- ti un, che ti cre -- de.

Hyl -- lo, di che t’of -- fen -- di?
Che sen -- so ha tal lin -- guag -- gio?
Non mal l’in -- te -- se il Pag -- gio
a -- mi tu dun -- que Io -- le?

I -- o per un’ em -- pia
in -- gra -- ta al pa -- dre, al mon -- do, al ciel sper -- giu -- ra,
che sof -- fris -- si nel cor d’a -- mor l’ar -- su -- ra?
Per u -- na sì mu -- ta -- bi -- le, ch’a un trat -- to
con su -- bi -- to con -- ten -- to
al -- la mia ge -- ni -- tri -- ce, a De -- ia -- ni -- ra
te -- cò a far sì gran tor -- to ohi -- mè cos -- pi -- ra?
Ver -- si ver -- si pria sul mio ca -- po i -- ra -- to Gio -- ve
tut -- ti i ful -- mi -- ni suo -- i,
et il più ne -- ro ba -- ra -- tro m’in -- go -- i.

O me in -- fe -- li -- ce, o mi -- se -- ra, che fe -- i?
Uc -- ci -- de -- te -- mi, oh De -- i.

Fin’ ho -- ra à te d’Eu -- ty -- ro
ne men di De -- ia -- ni -- ra un -- qua non cal -- se.
Par -- ti, e rin -- gra -- zia il ciel; che ben ti val -- se,
che d’es -- ser mi -- te og -- gi dis -- po -- si.

A di -- o:
an -- drò mor -- te a cer -- car per quel -- le bal -- ce.
