\clef "basse" sol,2 ~ sol,4. do8 |
re4 sol, r2 |
do2 fa |
re1~ |
re |
re2 la,4 re, |
sol,2 sol,4 fad, |
sol,1 |
sol, |
mi,2 re, |
la,1 |
la,2 sold,~ |
sold,1 |
la,2 re4 mi |
la,1 |
sol,2 fa,~ |
fa, mi, |
mi, re, |
do, do |
sol,2. fad,4 |
sol,1 |
do | \allowPageTurn
si,2 do |
re sol, |
\original {
  re1~ |
  re2 la, |
  mi1 |
  do |
  sol |
  do'2 si |
  la sol |
  fa mi |
  re8 do sol4 do2 |
}
sol,1~ |
sol, |
fad,~ |
fad,~ |
fad,2 sol,~ |
sol,1 |
la, |
<<
  \pygmalion { re2 }
  \original {
    re2. do4 |
    sib,1 |
    sib,2. sol,4 |
    fa,1~ |
    fa,2 fad, |
    sol, do4 dod |
    re2 sol, |
  }
>>

