\tag #'(voix3 basse) {
  Sve -- glia -- ti sve -- glia -- ti Al -- ci -- de, e mi -- ra.
  
  E do -- ve, o bel -- la?
  Do -- ve? ah qui pur di nuo -- vo
  te -- me -- ra -- rio im -- por -- tu -- no io ti ri -- tro -- vo?
  Ed a qual fi -- ne im -- pu -- gni
  fer -- ro mi -- ci -- dial? per tor la vi -- ta
  a chi s’in -- gius -- ta -- men -- te a te la die -- de?
  Ah se co -- tan -- to ec -- ce -- de
  tuo scel -- le -- ra -- to ar -- dir, giust’ è la vo -- glia,
  che quel vi -- ver in -- gra -- to,
  ch’a tor -- to a te fu da -- to
  o -- ra a ra -- gio -- ne io \tag #'voix3 { to -- glia. }
  \tag #'basse { to - }
}
\tag #'(voix1 basse) {
  Ohi -- mè, ohi -- mè, s’a -- mo -- re
  nul -- la in te puo -- te, ar -- res -- ta.
}
\tag #'(voix2 basse) {
  Ah ge -- ni -- to -- re.
}
\tag #'(voix3 basse) {
  E con sì dol -- ce no -- me an -- cor mi chia -- mi?
}
\tag #'(voix2 basse) {
  Non cre -- der già, ch’io più di vi -- ta bra -- mi
  che per mia mi -- glior sor -- te
  non so più de -- si -- ar al -- tro,
  \pygmalion {
    che mor -- te.
  }
  \original {
    che mor -- te,
    ma sol di par -- ri -- ci -- da
    l’in -- gius -- to in -- fa -- me ti -- to -- lo ri -- fiu -- to,
    e s’eb -- bi di ciò mai so -- lo un pen -- sie -- ro
    so -- vra l’a -- ni -- ma mi -- a,
    qual or sciol -- ta el -- la si -- a,
    o -- gni mar -- tir più fie -- ro,
    che chiu -- da A -- ver -- no in sé, gran -- di -- ni gran -- di -- ni Plu -- to.
  }

  Al -- ci -- de, ah ch’io fui quel -- la
  per ven -- di -- car Eu -- ty -- ro,
  e per sot -- trar -- mi à le tue in -- si -- die, io quel -- la,
  che so -- la di tra -- fig -- ger -- ti ten -- ta -- i.
  \original {
    Quin -- di è, che s’Hyl -- lo uc -- ci -- di,
    com’ es -- send’ i -- o so -- la ca -- gion, ch’ei mo -- ra,
    di me stes -- sa fa -- rò gius -- ti -- zia, e or o -- ra
    mor -- ta qui mi ve -- dra -- i. __
  }
}
