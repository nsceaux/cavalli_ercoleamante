<<
  \tag #'(voix1 basse) {
    <<
      \tag #'basse { s1*13 | s2.. \ffclef "vbas-dessus" }
      \tag #'voix1 { \ffclef "vbas-dessus" R1*13 | r2 r4 r8 }
    >> <>^\markup\character Iole mi''8 |
    do'' mi'' do'' si' si' si' si' si'16 la' |
    si'4 si'8 do'' la' la' \tag #'voix1 { r4 | R1*15 }
  }
  \tag #'(voix2 basse) {
    <<
      \tag #'basse { s1*15 | s2. \ffclef "vtenor" }
      \tag #'voix2 { \ffclef "vtenor" R1*15 | r2 r4 }
    >> <>^\markup\character Hyllo re'4 |
    r la8 sol sol4 <<
      \tag #'basse { sol8 s | s1. \ffclef "vtenor" }
      \tag #'voix2 { sol4 | R1 | r2 }
    >> <>^\markup\character Hyllo r8 mi' do' si |
    re'4 si si8 la la si |
    sol4 sol8 si si si si do' |
    do'4 do'8 do' do' do' do' si |
    re'2 mi'4 do'8 si |
    do'4. re'8\melisma si4\melismaEnd <<
      \pygmalion { r4 | }
      \original {
        r8 re' |
        la la la la la4 la8 la |
        la la la si do'8. do'16 do'8 si |
        si4 si r sol |
        do' do'8 do' do' do' do' do'16 si |
        re'4 re' sol'8 fa' fa' fa'16 mi' |
        mi'4 mi'8 sol' re'4 re'8 re'16 mi' |
        do'4 do' mi'8 mi'16 mi' mi'8 mi' |
        la8 la16 do' la la la sol sol4 do'8 do'16 do' |
        fa'8 fa'16 mi' do'8. si16( do'4) r4 |
      }
    >>
    \ffclef "vbas-dessus" <>^\markup\character Iole
    r4 re'' sib' sib' |
    r do''2 la'8 la' |
    la'4 la'8 la' la' la' la' sol' |
    la'4 la' r8 la' la' la' |
    la' la' sib' do'' sib'4 sib'8 sib' |
    sol'4 sol'8 sol' sol' sol' sol' sib' |
    fa'8. fa'16 fa'8 la' fa'4( mi') |
    re'4 <<
      \pygmalion { r4 }
      \original {
        r8 re'' la' la' la' la'16 sib' |
        sib'4 sib' r8 sib' sib' sib' |
        fa'4 fa' fa'8 fa'16 sol' sol'8 la' |
        la'4 la' r do''8 do'' |
        do'' do''16 do'' do''8 re'' re'' re'' r re'' |
        mib'' mib'' sib' lab' lab'4 lab'8 sol' |
        sol'4. fad'8( sol'2) |
      }
    >>
  }
  \tag #'(voix3 basse) {
    <<
      \original {
        \clef "vtenor" <>^\markup\character Mercurio
        re'8 si16 si re'8 si16 si si8 si r mi' |
        la8. la16( sol4) r2 |
      }
      \pygmalion {
        \clef "vtenor" <>^\markup\character Mercurio
        re'8 si16 si sol'8 re'16 re' si8 si r mi' |
        la8. la16( sol4) r2 |
      }
    >>
    \ffclef "vbasse" <>^\markup\character Ercole
    r8 do' sol sol16 la fa8 fa la fa |
    r4 re' r8 la la sol |
    fa4 fa fa8 fa fa fa16 fa |
    re8 re re do16 si, la,4 re |
    r16 si si re' si8 si16 la la8 la la16 la la si |
    sol4 r r8 sol si la |
    si si r si si si si dod' |
    dod' dod'16 dod' dod'8 si re' re' r4 |
    r do'4 do'8 do' do' si |
    do' do'16 do' do' do' do' si si4 mi8 mi16 mi |
    si16 si si si si8 si16 la si8 si16 si si si si do' |
    la8 la la la16 la fa8 sol16 la mi8 <<
      \tag #'basse { s8 | s1*2 | s2.. \ffclef "vbasse" }
      \tag #'voix3 { mi8 | R1*2 | r2 r4 r8 }
    >> <>^\markup\character Ercole do8 |
    do do do do fa fa16 fa fa8 mi |
    sol sol r4 \tag #'voix3 { r2 | R1*12 }
  }
>>
