\score {
  \new ChoirStaff \with { \haraKiriFirst } <<
    \new Staff \withLyrics <<
      \global \keepWithTag #'voix1 \includeNotes "voix"
    >> \keepWithTag #'voix1 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'voix2 \includeNotes "voix"
    >> \keepWithTag #'voix2 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'voix3 \includeNotes "voix"
    >> \keepWithTag #'voix3 \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*3\pageBreak
        s1*4\break s1*4\break s1*2\break s1*4\pageBreak
        s1*4\break s1*4 s2 \bar "" \break s2 s1*4\break s1*3 \pageBreak
        s1*5\break s1*4\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
