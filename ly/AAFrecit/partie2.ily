\clef "viole2" R1*14 |
r2 mi' mi'2. mi'4 |
la2. la4 do'2 do' |
re'1 do'\fermata |
R1*9 R2.*12 R1*12 |
r2 mi' mi'2. mi'4 |
la2. la4 do'2 do' re'1 do'\fermata |
R1*3 R2.*8 R1*10 |
