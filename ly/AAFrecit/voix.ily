\clef "vsoprano" <>^\markup\character Cinzia
r4 r8 do'' mi''4 mi'' |
mi''4 do''8 do'' do''2 |
do''4 do''8 do''8 do''4 do''8 re''8 |
mi''2 mi''8 sol'' mi'' fa'' |
fa''2 fa'' |
r4 fa''8 do'' fa''4 do''8 do'' |
do'' do'' do''4. do''8 fa'' mi'' |
mi''2 mi''4 mi''8 mi'' |
mi''4 re''8 mi'' do''2 |
do''4 do''8 si' re''4 re''8 mi'' |
do''4 do''4. do''8 do'' do'' |
do''4 fa''2 mi''8 re'' |
mi''2( re'') do''1\fermata |
R\breve*3 |
r4 r8 mi'' do''4 do''8 si' |
do''4. do''8 do'' do'' do'' do'' |
do''4 do'' r do'' |
do''8 do'' do'' do'' do'' do'' do'' do''16 re'' |
sib'4 sib' sib'8 sib' sib' sib'16 do'' |
re''4 re''8 re'' sib'4 sib'8 la' |
la'4 la' la'8 la' la' la'16 la' |
re''4. re''8 do'' sol' sib'4( |
la'1) |
sol'4 re'' mi'' |
re''4. do''8 si'4 |
mi'' mi'' do'' |
re''4. re''8 do'' re'' |
sib'4 sib'8 sib' do'' sol' |
la'4 la' do''8 do'' |
fa''4 fa'' fa''8 mib'' |
re''[ do''] sib'[ la'] sol'[ fa'] |
la'4( sol'2) |
fa'4 do'' re''~ |
re'' re''( mi'') |
mi'' mi'' fa''8[ sol''] |
dod''8 dod'' dod'' dod'' dod''4 dod''8 dod'' |
dod''8 dod'' dod'' re'' mi''4 mi''8 mi'' |
mi'' mi'' mi'' mi'' mi'' mi'' mi'' mi''16 mi'' |
mi''8 re'' mi'' mi'' mi''4 fa''8 sol'' |
fa''4 fa''8 re'' fa''8. mi''16 re''8 sol'' |
mi''8. re''16 do''8 fa'' re'' re'' r sol'' |
la''8. sol''16 fa''8 fa'' sol'' sol'' mi'' mi'' |
fa'' fa'' re''16([ mi'']) fa''([ re'']) sol''4 do''16[ re''] mi''[ do''] |
fa''[\melisma mi'' fa'' mi''] re''[ re'' mi'' fa''] sol''[ si' do'' re''] mi''4\melismaEnd |
mi''8 do'' fa'' fa'' r si' mi'' re'' |
mi''4\melisma fa''8[ sol''] re''2\melismaEnd do''1\fermata |
R1*6 |
r4 mi'' si'8 si' si' si' |
sold' sold' sold' sold' sold'4 sold'8 la' |
si'4 si'8 mi' si' si' re'' mi'' |
do''2. |
do''2 mi''4 |
do''2 si'4 |
re''4 re''8([ sold']) la'4 |
si'2 si'4 |
r la' la' |
la' si'8([ do'' re'' mi'']) |
re''8[ do''] si'2 |
la'4 r8 la' do'' do'' do'' re'' |
sib'8. sib'16 sib'8 sib' sib' sib' sib' la' |
la'4. la'8 la'2 |
fa'8 fa' fa' fa'16 sol' la'2 |
fa'4 la'8 si' do''4 do''8 do'' |
do''8. do''16 re''8 mi'' fa''[\melisma mi''16 re''] do''8[ re''] |
mi''4\melismaEnd mi''8 sol'' fa''2 |
mi''4. sol''8 fa''2 |
mi''4. mi''16[\melisma fa''] sol''8\melismaEnd fa''16([ mi''] re''4) |
do''1\fermata |
