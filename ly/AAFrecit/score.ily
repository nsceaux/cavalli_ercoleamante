\score {
  <<
    \new StaffGroup \with { \haraKiriFirst } <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
      \new Staff << \global \includeNotes "partie1" >>
      \new Staff << \global \includeNotes "partie2" >>
    >>
    \new ChoirStaff \with { \haraKiriFirst } <<
      \new Staff \withLyrics <<
        \global \includeNotes "voix"
      >> \includeLyrics "paroles"
      \new Staff <<
        \global \includeNotes "basse"
        \includeFigures "chiffres"
        \origLayout {
          s1*5\break s1*4\pageBreak
          s1*5\break s1*6\break s1*4\pageBreak
          s1*3\break s1*2 s2.*4\break s2.*6\break s2.*2 s1*2\break s1*3\pageBreak
          s1*2 s2 \bar "" \break s2 s1*4\break
          s1*6 s1*2\break s1 s2.*3\break s2.*5 s1\pageBreak
          s1*3 s2 \bar "" \break s2 s1*3\break
        }
        \modVersion {
          s1*14 \ru#2 { s1\noBreak }
        }
      >>
    >>
  >>
  \layout { }
  \midi { }
}
