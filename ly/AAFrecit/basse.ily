\clef "basse" do1 |
do~ |
do~ |
do |
fa, |
fa, |
fa, |
do~ |
do |
la,2 sol, |
mi,1 |
fa,2 sol,4 fa, |
sol,1 do\fermata |
r2 do' la2. mi4 |
fa2. re4 mi2 do |
sol1 do\fermata |
do1~ |
do |
do~ |
do |
sol,~ |
sol, |
re |
sib,2 do |
re1 |
sol,2 mi4 |
fa re sol |
do8 re do sib, la,4 |
sib,2 do4 |
re2 mi4 |
fa2 mi4 |
re2 la,4 |
sib,2. |
sib,4 do2 |
fa, fa4 |
sol2 sol4 |
la sib2 |
la1~ | \allowPageTurn
la~ |
la~ |
la |
re2~ re8 la sib8. la16 |
sol8 do' la8. sol16 fa8 sib sol4 |
r8 do' re'8. do'16 sib8 sib do' do' |
la la sib4 sol16 la sib sol la4 |
fa16 sol la fa sol la sol fa mi fa mi re do re do si, |
la, sib, la, sol, fa, sol, la, fa, sol,4 do, |
sol,1 do\fermata |
r2 do' la2. mi4 |
fa2. re4 mi2 do sol1 do\fermata |
mi1~ |
mi |
mi |
la,2.~ |
la,2 la,4 |
la2 sol4 |
fad fa2 |
mi4. fa8 mi re |
do2. |
re |
re4 mi2 |
la,2 la |
sol1 |
fa~ |
fa2~ fa8. sol16 fa8. mi16 |
re2 do8. re16 do8. sib,16 |
la,4 sol, fa,8 sol, la, si, |
do re mi do re mi fa sol |
la si do' mi fa sol la si |
do' sol do'4~ do' si |
do'1\fermata |
