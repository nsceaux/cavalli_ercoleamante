\newBookPart#'()
\act "Atto Secondo"
\scene "Scena Prima" "Scena I"
\sceneDescription\markup\center-column {
  \wordwrap-center {
    [La scena si cangia in un gran cortile del palazzo reale.]
  }
  \wordwrap-center { Hyllo, e Iole. }
}
%% 2-1
\pieceToc "Sinf[onia]"
\includeScore "CAAsinfonia"
%% 2-2
\pieceToc\markup\wordwrap {
  Iole, Hyllo: \italic { Amor ardor più rari }
}
\includeScore "CABduo"
%% 2-3
\pieceToc\markup\wordwrap {
  Iole, Hyllo: \italic { Pure alfine il rispetto }
}
\includeScore "CACrecit"
%% 2-4
\pieceToc\markup\wordwrap {
  Iole, Hyllo: \italic { Gare d’affetto ardenti }
}
\includeScore "CADduo"

\scene "Scena Seconda" "Scena II"
\sceneDescription\markup\wordwrap-center { Paggio, Iole, e Hyllo. }
%% 2-5
\pieceToc\markup\wordwrap {
  Paggio, Iole, Hyllo: \italic { Ercole a dirti invia, ch’altro non bada }
}
\includeScore "CBArecit"
%% 2-6
\pieceToc\markup\wordwrap {
  Hyllo, Iole: \italic { Chi può vivere un sol istante }
}
\includeScore "CBBduo"

\scene "Scena Terza" "Scena III"
\sceneDescription\markup\wordwrap-center { Paggio. }
%% 2-7
\pieceToc\markup\wordwrap {
  Paggio: \italic { E che cosa è quest’amore? }
}
\includeScore "CCArecit"

\scene "Scena Quarta" "Scena IV"
\sceneDescription\markup\wordwrap-center { Deianira, Licco, Paggio. }
%% 2-8
\pieceToc\markup\wordwrap {
  Licco, Paggio: \italic { Buon dì gentil fanciullo }
}
\includeScore "CDArecit"
%% 2-9
\pieceToc\markup\wordwrap {
  Paggio, Deianira, Licco,: \italic { Sei tu qualche indovino? }
}
\includeScore "CDBrecit"

\scene "Scena Quinta" "Scena V"
\sceneDescription\markup\wordwrap-center { Deianira, Licco. }
%% 2-10
\pieceToc\markup\wordwrap {
  Deianira: \italic { Misera, ohimè, ch’ascolto }
}
\includeScore "CEArecit"
%% 2-11
\pieceToc\markup\wordwrap {
  Deianira: \italic { Ahi ch’amarezza }
}
\includeScore "CEBaria"
%% 2-12
\pieceToc\markup\wordwrap {
  Licco, Deianira: \italic { Ah fu sempre in amor stolto consiglio }
}
\includeScore "CECrecit"

\scene "Scena Sesta" "Scena VI"
\sceneDescription\markup\center-column {
  \wordwrap-center {
    La scena si cangia nella grotta del Sonno.
  }
  \wordwrap-center {
    Pasithea, il Sonno, coro d'Aure e Ruscelli.
  }
}
%% 2-13
\pieceToc\markup\wordwrap {
  Pasithea: \italic { Mormorate }
}
\includeScore "CFAaria"
%% 2-14
\pieceToc\markup\wordwrap {
  Coro: \italic { Dormi, dormi, o Sonno dormi }
}
\includeScore "CFBcoro"

\scene "Scena Settima" "Scene VII"
\sceneDescription\markup\center-column {
  \wordwrap-center { [Cala Giunone dal cielo.] }
  \wordwrap-center { Li sudetti e Giunone. }
}
%% 2-15
\pieceToc\markup\wordwrap {
  Pasithea, Giunone: \italic { O dèa sublime dèa }
}
\includeScore "CGArecit"
%% 2-16
\pieceToc "Ritor[nello]"
\includeScore "CGBritornello"
%% 2-17
\pieceToc\markup\wordwrap {
  Giunone: \italic { Dell’amorose pene }
}
\includeScore "CGCaria"
%% 2-18
\pieceToc\markup\wordwrap {
  Tutti: \italic { Le rugiade più preziose }
}
\includeScore "CGDtutti"
%% 2-19
\pieceToc\markup\wordwrap {
  Pasithea: \italic { Vanne, e fa breve dimora }
}
\includeScore "CGEaria"
%% 2-20
\pieceToc\markup\wordwrap {
  Tutti: \italic { Le rugiade più preziose }
}
\reIncludeScore "CGDtutti" "CGFtutti"
\actEnd\markup\wordwrap-center {
  Qui va il Balletto \concat { 3 \super o }
  e Finisce l’Atto \concat { 2 \super o }
}
