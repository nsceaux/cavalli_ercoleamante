\clef "vsoprano" <>^\markup\character Pasithea
do''4 fa' r do''8 do'' |
re''4 re''8 mib'' do''4. si'8( do''1) |
r4 sol' la' |
sib' do''2 re'' sol'4 |
sib'2 la'4 r re'' do'' mib''2 re''4~ |
re'' re''4. do''8 re''2 re''4 |
r sol'4. fa'8 |
mi'4 do''4. sib'8 la'4 re''2 |
do''4 sol'( la') sib'( la'2) |
sol'4 sib'4. la'8 sol'4 do''2 |
la'4 do''2 fa'2( sol'4) |
sol'4 sol'4. sol'8 |
do''4 do''4. do''8 |
fa''2 fa''4 re'' do''( re'') |
sib'2. la'4 do''2 |
do''2. do''~ |
do''4\melisma re''4. mi''8 fa''4 mib''4. fa''8 |
re''4\melismaEnd do''2~ do''4 sib' la' |
sol'2. fa'1*3/4\fermata |
