Van -- ne, e fa bre -- ve di -- mo -- ra,
che s’il tuo tar -- dar no -- io -- so
ad ogn’ un tan -- to è pe -- no -- so,
che sa -- rà che sa -- rà per chi t’a -- do -- ra?
E A -- mor ha ben la glo -- ria
di sa -- per di sa -- per nel Son -- no an -- co -- ra
te -- ner des -- ta la me -- mo -- ria.
