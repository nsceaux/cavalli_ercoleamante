\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout { s1*3 s2.*6\break s2.*11\pageBreak s2.*10\break }
    >>
  >>
  \layout { }
  \midi { }
}
