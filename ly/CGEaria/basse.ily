\clef "basse" fa2. fa4 |
re4. do8 sol2 do1 |
r4 do' la |
sol la2 sib do'4 |
re' re' do' sib sib la sol sol fa |
mib2. re4 re4. do8 |
si,2 si,4 |
do2 do4 re2 re4 |
mib2 do4 re2. |
sol,2 sol4 mi2 do4 |
fa2 mi4 re2. |
do4 do' sib |
la2 sol4 |
fa la2 sib4 la fa |
sol re mi fa fa mi |
fa mi4. re8 do4 sib,4. do8 |
la,4 sol,2 fa,4 la,2 |
sib,4 do2 la,4 sib,2 |
do2. fa,1*3/4\fermata |
