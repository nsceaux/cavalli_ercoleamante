\clef "dessus" r4 la' re'' |
do'' fa''8 sol'' la''4 re'' mi''2 |
fa''4. do''8 re''4 mi''2 fa''4 |
fa'' fa''4. mi''8 |
fa''2. r2*3/2 |
R2.*9 |
r2*3/2 r4 la' re'' |
do'' fa''8 sol'' la''4 re'' mi''2 |
fa''4. do''8 re''4 mi''2 fa''4 |
fa'' fa''4. mi''8 |
fa''2. |
<<
  \original {
    R2.*21 |
    r2*3/2 r4 la' re'' |
    do'' fa''8 sol'' la''4 re'' mi''2 |
    fa''4. do''8 re''4 mi''8 fa'' sol''4 fa'' |
    fa'' fa''4. mi''8 fa''2. |
    R2.*14 |
  }
  \pygmalion R2.*13
>>
r2*3/2 r4 la' re'' |
do'' fa''8 sol'' la''4 re'' mi''2 |
fa''4. do''8 re''4 mi''2 fa''4 |
fa'' fa''4. mi''8 fa''2. |
\original {
  R2.*14 |
  r2*3/2 r4 la' re'' |
  do'' fa''8 sol'' la''4 re'' mi''2 |
  fa''4. do''8 re''4 mi''2 fa''4 |
  fa'' fa''4. mi''8 fa''1*3/4\fermata |
}
R2.*22 |
r2*3/2 r4 la' re'' |
do'' fa''8 sol'' la''4 re'' mi''2 |
fa''4. do''8 re''4 mi''2 fa''4 |
fa'' fa''4. mi''8 fa''1*3/4\fermata |
