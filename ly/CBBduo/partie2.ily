\clef "viole2" r4 fa' re' |
fa'2 fa'4 re' do'2 |
do'4 la4. re'8 sol2 la4 |
sol do'2 |
do'2. r2*3/2 |
R2.*9 |
r2*3/2 r4 fa' re' |
fa'2 fa'4 re' do'2 |
do'4 la re' sol2 la4 |
re' do'2 |
do'2. |
<<
  \original {
    R2.*21 |
    r2*3/2 r4 fa' re' |
    fa'2 fa'4 re' do'2 |
    do'4 la re' sol2 la4 |
    re' do'2 do'2. |
    R2.*14 |
  }
  \pygmalion R2.*13
>>
r2*3/2 r4 fa' re' |
fa'2 fa'4 re' do'2 |
do'4 la re' sol2 la4 |
re' do'2 do'2. |
\original {
  R2.*14 |
  r2*3/2 r4 fa' re' |
  fa'2 fa'4 re' do'2 |
  do'4 la re' sol2 la4 |
  re' do'2 do'1*3/4\fermata |
}
R2.*22 |
r2*3/2 r4 fa' re' |
fa'2 fa'4 re' do'2 |
do'4 la re' sol2 la4 |
re' do'2 do'1*3/4\fermata |
