\clef "viole1" r4 do'' sib' |
do''4. sib'8 la'4 sol' sol'2 |
fa' fa'4 sol'2 fa'4 |
sib' sol'4. do''8 |
la'2. r2*3/2 |
R2.*9 |
r2*3/2 r4 do'' sib' |
do''4. sib'8 la'4 sol' sol'2 |
fa' fa'4 sol'2 fa'4 |
sib' sol'4. do''8 |
la'2. |
<<
  \original {
    R2.*21 |
    r2*3/2 r4 do'' sib' |
    do''4. sib'8 la'4 sol' sol'2 |
    fa'2 fa'4 sol' mi' fa' |
    sib' sol'2 la'2. |
    R2.*14 |
  }
  \pygmalion R2.*13
>>
r2*3/2 r4 do'' sib' |
do''4. sib'8 la'4 sol' sol'2 |
fa'2 fa'4 sol'2 fa'4 |
sib' sol'2 la'2. |
\original {
  R2.*14 |
  r2*3/2 r4 do'' sib' |
  do''4. sib'8 la'4 sol' sol'2 |
  fa' fa'4 sol'2 fa'4 |
  sib' sol'2 la'1*3/4\fermata |
}
R2.*22 |
r2*3/2 r4 do'' sib' |
do''4. sib'8 la'4 sol' sol'2 |
fa' fa'4 sol'2 fa'4 |
sib' sol'2 la'1*3/4\fermata |
