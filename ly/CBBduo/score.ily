\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
      \new Staff << \global \includeNotes "partie1" >>
      \new Staff << \global \includeNotes "partie2" >>
      \new Staff << \global \includeNotes "basse1" >>
    >>
    \new Staff \withLyrics <<
      \global \keepWithTag #'iole \includeNotes "voix"
    >> \keepWithTag #'iole \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'hyllo \includeNotes "voix"
    >> \keepWithTag #'hyllo \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2.*12\pageBreak
        s2.*12\pageBreak
        s2.*11\pageBreak
        s2.*11\pageBreak
        s2.*14\pageBreak
        s2.*12\pageBreak
        s2.*15\pageBreak
        s2.*11\pageBreak
        s2.*10\break s2.*8\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
