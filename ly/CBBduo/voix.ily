<<
  \tag #'(hyllo basse) {
    \clef "vtenor" <>^\markup\character Hyllo
    R2.*3 |
    r4 do' fa' mi'4. re'8 do'4 |
    re' sol2 |
    la( si4) do' do' do' |
    fa'4. fa'8 mi'4 re' do'4. re'8 |
    mi'4 mi'8[ re' mi' fa'] re'4 sol sol |
    sol2 sol4 |
    la la( si) do' sol la |
    sib4. do'8 sib4 la sol( la) |
    la( sol2) fa r4 |
    R2.*5 |
    \original {
      do'4 do' do' |
      re'4. re'8 do'4 sib sol2 |
      la4. sib8 do'4 do' re'2 |
      mi'4. fa'8 sol'4 |
      do'2. do' |
      re'4 re'2 do'2. |
      r4 r8 do' sib la |
      sib4 sol2 |
      la4( si2) do'4 fa'2 |
      r4 re' do' sib2 la4~ |
      la fa2 sol4 do' sol |
      sib la sol |
      do'2.~ |
      do'2~ do'8 sib la2. |
      re'4 sol2 fa\fermata r4 |
      R2.*6 |
    }
  }
  \tag #'iole { \clef "vsoprano" R2.*24 \original R2.*30 }
>>
<<
  \tag #'(iole basse) {
    \tag #'basse \ffclef "vsoprano"
    r4 do''( fa'') mi''4. re''8 do''4 |
    re'' sol' do'' la'4. sib'8 do''4 |
    do'' re'' mi'' fa''4. mi''8 mi''4 |
    re''4 re''4. do''8 do''2. |
    r4 r do'' |
    sib'4. la'8 sol'4 la' la' si' |
    do''2. |
    re''4 do'' sib' la'4. sib'8 do''4 |
    re'' sol'4. fa'8 fa'2.\fermata |
    R2.*6 |
    \original {
      r4 do''( fa'') mi''4. re''8 do''4 |
      re'' sol' do'' la'4. sib'8 do''4 |
      do'' re'' mi'' fa''4. mi''8 mi''4 |
      re'' re''4. do''8 do''2. |
      r4 r do'' |
      sib'4. la'8 sol'4 la' la' si' |
      do''2. |
      re''4 do'' sib' la'4. sib'8 do''4 |
      re'' sol'4. fa'8 fa'2.\fermata |
      R2.*6 |
    }
  }
  \tag #'hyllo { R2.*22 \original R2.*22 }
>>
<<
  \tag #'(iole basse) {
    la'4 la' la' |
    sol'4. sol'8 la'4 sib' sol'2 |
    la'4. si'8 do''4 do'' si'!2 |
    do''4. do''8 sol'4 r do''2 |
    do''2. do''4 do''4. si'8( |
    do''2.) |
    r4 r8 do'' sib' la' |
    sib'4 sol'2 la'4( fa'2) |
    do''4 fa''2 r4 re'' do'' |
    sib'2 la'4~ |
    la' fa'2 mi'4 do'' sol' |
    sib' sol'8[ la'] sib'[ do''] la'4.\melisma sib'8 do''4 |
    sib'4 do''4.\melismaEnd sib'8 la'2 r4 |
    re''4 sol'2 fa'1*3/4\fermata |
    R2.*6 |
  }
  \tag #'hyllo {
    do'4 do' do' |
    re'4. re'8 do'4 re' mi'2 |
    fa'4. fa'8 sol'4 fa' fa'2 |
    mi'4. mi'8 re'4 fa'2 mi'4 |
    la4 la8([ sib] do'4) re' re'2 |
    do'2. |
    R2. |
    r2*3/2 r4 r8 fa' mi' re' |
    mi'4 do'2 re'4 sib sol~ |
    sol sib do' |
    la si( do') do'2 do'4 |
    r4 sol' re' fa' do'8[ re'] mib'[ fa'] |
    sol'4.\melisma fa'8 mi'4 fa'4.\melismaEnd mi'8 re'4 |
    fa'4 fa'4. mi'8( fa'1*3/4) |
    R2.*6 |
  }
>>
