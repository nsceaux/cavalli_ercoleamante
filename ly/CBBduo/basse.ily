\clef "basse" r4 fa sib |
la4. sol8 fa4 sol do2 |
fa4. mi8 re4 do4. sib,8 la,4 |
sib, do2 |
fa,2. mi, |
la,2 fa,4 sol, mi,2~ |
mi,4 fa,2 sol,2. |
r4 mi mi |
fa re2 do do4 |
re2 mi4 fa sib,2 |
sib,4 do2 fa,4 fa sib |
la4. sol8 fa4 sol do2 |
fa4. mi8 re4 do4. sib,8 la,4 |
sib, do2 |
<<
  \original {
    fa,2 fa4 |
    sib2 la4 sol do2 |
    fa mi4 fa re2 |
    do sib,4 |
    la,4. sib,8 do4 fa,4. sol,8 la,4 |
    fa, sol,2 do4. fa8 mi re |
    mi4 do2 |
    re4 mi2 |
    fa4 re2 do4 la,2 |
    sib,2 do4 sol, sol do |
    fa re2 do2. |
    sol, |
    r4 fa do |
    mib re do re4. do8 sib,4 |
    sib,4 do2 fa,4 fa sib |
    la4. sol8 fa4 sol do2 |
    fa4. mi8 re4 do4. sib,8 la,4 |
    sib, do2 fa2. |
    r4
  }
  \pygmalion fa,4
>> fa,8 sol, la, sib, do4. re8 mi4 |
fa( fa) mi fa2. |
mi4 fa mi re2 do4 |
fa4 sol2 do2 fa4 |
mi4. re8 do4 |
re re mi fa4. mi8 re4 |
do4. sib,8 la,4 |
sib,2 do4 re2 la,4 |
sib, do2 fa,4 fa sib |
la4. sol8 fa4 sol do2 |
fa4. mi8 re4 do4. sib,8 la,4 |
sib, do2 fa2. |
\original {
  r4 fa,8 sol, la, sib, do4. re8 mi4 |
  fa( fa) mi fa2. |
  mi4 fa mi re2 do4 |
  fa sol2 do fa4 |
  mi4. re8 do4 |
  re re mi fa4. mi8 re4 |
  do4. sib,8 la,4 |
  sib,2 do4 re2 la,4 |
  sib, do2 fa,4 fa sib |
  la4. sol8 fa4 sol do2 |
  fa4. mi8 re4 do4. sib,8 la,4 |
  sib, do2 fa,1*3/4\fermata |
}
fa2 fa4 |
sib2 la4 sol do2 |
fa mi4 fa re2 |
do sib,4 la,4. sib,8 do4 |
fa,4. sol,8 la,4 fa, sol,2 |
do4. fa8 mi re |
mi4 do2 |
re4 mi2 fa4 re2 |
do4 la,2 sib, do4 |
sol, sol do |
fa re2 do2. |
sol, r4 fa do |
mib4. re8 do4 re4. do8 sib,4 |
sib, do2 fa,4 fa sib |
la4. sol8 fa4 sol do2 |
fa4. mi8 re4 do4. sib,8 la,4 |
sib, do2 fa,1*3/4\fermata |
