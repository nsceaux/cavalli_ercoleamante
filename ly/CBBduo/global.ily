\key fa \major \midiTempo#160
\digitTime\time 3/4 s2.
\measure 6/4 s1.*2
\measure 3/4 s2.
\measure 6/4 s1.*3
\measure 3/4 s2.
\measure 6/4 s1.*5
\measure 3/4 s2.*2
<<
  \original {
    \measure 6/4 s1.*2
    \measure 3/4 s2.
    \measure 6/4 s1.*2
    \measure 3/4 s2.*2
    \measure 6/4 s1.*3
    \measure 3/4 s2.*2
    \measure 6/4 s1.*9
  }
  \pygmalion {
    s2.
    \measure 6/4 s1.*3
  }
>>
\measure 3/4 s2.
\measure 6/4 s1.
\measure 3/4 s2.
\measure 6/4 s1.*5
\original {
  s1.*4
  \measure 3/4 s2.
  \measure 6/4 s1.
  \measure 3/4 s2.
  \measure 6/4 s1.*5 \bar "|."
}
\digitTime\time 3/4 s2.
\measure 6/4 s1.*4
\measure 3/4 s2.*2
\measure 6/4 s1.*2
\measure 3/4 s2.
\measure 6/4 s1.*7 \bar "|."
