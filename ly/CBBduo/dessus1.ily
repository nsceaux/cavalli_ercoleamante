\clef "dessus" r4 fa'' sol'' |
la''4. sib''8 do'''4 sib'' sol''2 |
la''2 si''4 do'''2. |
sol''4 sol''4. fa''8 |
fa''2. r2*3/2 |
R2.*9 |
r2*3/2 r4 fa'' sol'' |
la''4. sib''8 do'''4 sib'' sol''2 |
la''2 si''4 do'''2. |
sol''4 sol''4. fa''8 
fa''2. |
<<
  \original {
    R2.*21 |
    r2*3/2 r4 fa'' sol'' |
    la''4. sib''8 do'''4 sib'' sol''2 |
    la'' si''4 do'''2. |
    sol''4 sol''4. fa''8 fa''2. |
    R2.*14
  }
  \pygmalion R2.*13
>>
r2*3/2 r4 fa'' sol'' |
la''4. sib''8 do'''4 sib'' sol''2 |
la''2 si''4 do'''2. |
sol''4 sol''4. fa''8 fa''2. |
\original {
  R2.*14 |
  r2*3/2 r4 fa'' sol'' |
  la''4. sib''8 do'''4 sib'' sol''2 |
  la''2 si''4 do'''2. |
  sol''4 sol''4. fa''8 fa''1*3/4\fermata |
}
R2.*22 |
r2*3/2 r4 fa'' sol'' |
la''4. sib''8 do'''4 sib'' sol''2 |
la''2 si''4 do'''2. |
sol''4 sol''4. fa''8 fa''1*3/4\fermata |
