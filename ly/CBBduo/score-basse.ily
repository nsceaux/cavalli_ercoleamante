\score {
  \new ChoirStaff <<
    \new Staff \with { \haraKiri } << \global \includeNotes "basse1" >>
    \new Staff \with { \tinyStaff } \withLyrics <<
      \global \keepWithTag #'basse \includeNotes "voix"
    >> { \keepWithTag #'basse \includeLyrics "paroles" \set fontSize = -2 }
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
    >>
  >>
  \layout { }
}
