\tag #'(hyllo basse) {
  Chi può vi -- ve -- re un sol is -- tan -- te
  lun -- ge lun -- ge dal bel -- lo che l’in -- va -- ghì,
  di -- ca pur, ch’in lui mo -- rì
  o -- gni pre -- gio di ve -- ro a -- man -- te;
  \original {
    d’a -- mo -- re il fo -- co
    per o -- gni po -- co
    ch’in -- tie -- pi -- dis -- ca -- si ghiac -- cio di -- vie -- ne,
    e le di lui ca -- te -- ne
    più stret -- ta -- men -- te av -- vol -- te
    o -- gni po -- co, che ce -- da -- no, son sciol -- te.
  }
}
\tag #'(iole basse) {
  O glo -- ri -- a
  d’a -- mor più no -- bi -- le
  con fe -- de im -- mo -- bi -- le
  sempr’ ar -- de più;
  me -- mo -- ri -- a
  mai non vi fu,
  che la vit -- to -- ri -- a
  man -- cas -- si tu.
  \original {
    Si scio -- glio -- no
    qual hor gl’ins -- ta -- bi -- li
    rei più dan -- na -- bi -- li
    A -- mor non ha.
    Lo spo -- glio -- no
    di de -- i -- tà
    poi -- ché gli to -- glio -- no
    l’e -- ter -- ni -- tà.
  }
}

D’a -- mo -- re il fo -- co
per o -- gni po -- co
ch’in -- tie -- pi -- dis -- ca -- si ghiac -- cio
\tag #'hyllo { ghiac -- cio } di -- vie -- ne,
e le di lui ca -- te -- ne
più stret -- ta -- men -- te av -- vol -- te
o -- gni po -- co, che ce -- da -- no, son sciol -- te.
