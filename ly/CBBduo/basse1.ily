\clef "basse" r4 fa sib |
la4. sol8 fa4 sol do2 |
fa4. mi8 re4 do4. sib,8 la,4 |
sib, do2 |
fa,2. r2*3/2 |
R2.*9 |
r2*3/2 r4 fa sib |
la4. sol8 fa4 sol do2 |
fa4. mi8 re4 do4. sib,8 la,4 |
sib, do2 |
fa,2. |
<<
  \original {
    R2.*21 |
    r2*3/2 r4 fa sib |
    la4. sol8 fa4 sol do2 |
    fa4. mi8 re4 do4. sib,8 la,4 |
    sib, do2 fa,2. |
    R2.*14 |
  }
  \pygmalion R2.*13
>>
r2*3/2 r4 fa sib |
la4. sol8 fa4 sol do2 |
fa4. mi8 re4 do4. sib,8 la,4 |
sib, do2 fa,2. |
\original {
  R2.*14 |
  r2*3/2 r4 fa sib |
  la4. sol8 fa4 sol do2 |
  fa4. mi8 re4 do4. sib,8 la,4 |
  sib, do2 fa,1*3/4\fermata |
}
R2.*22 |
r2*3/2 r4 fa sib |
la4. sol8 fa4 sol do2 |
fa4. mi8 re4 do4. sib,8 la,4 |
sib, do2 fa,1*3/4\fermata |
