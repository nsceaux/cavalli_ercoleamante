\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket \with {
      \override VerticalAxisGroup.remove-empty = #(eqv? #t (ly:get-option 'pygmalion))
    } <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff <<
        \pygmalion\notemode {
          s1*25 s2.*9 s1.*4
          \override MultiMeasureRest.transparent = ##t
          \noHaraKiri R1.
        }
        \global \includeNotes "dessus2"
      >>
      \new Staff << \global \includeNotes "partie1" >>
      \new Staff << \global \includeNotes "partie2" >>
      \new Staff << \global \keepWithTag #'basse \includeNotes "basse" >>
    >>
    \new ChoirStaff \with { \haraKiriFirst } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'vsoprano \includeNotes "voix"
      >> \keepWithTag #'vsoprano \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'valto \includeNotes "voix"
      >> \keepWithTag #'valto \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vtenor \includeNotes "voix"
      >> \keepWithTag #'vtenor \includeLyrics "paroles"
      \new Staff \withLyrics <<
        { s1*25 s2.*9 \noHaraKiri }
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new Staff \with { \haraKiri } \withLyrics <<
      \global \keepWithTag #'eutyro \includeNotes "voix"
    >> \keepWithTag #'eutyro \includeLyrics "paroles"
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*10\pageBreak
        s1*7\pageBreak
        s1*7\pageBreak
        s1 s2.*6\pageBreak
        s2.*3\pageBreak
        s2.*10\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
