\score {
  <<
    \new Staff \with { \tinyStaff \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'basse \includeNotes "voix"
    >> \keepWithTag #'basse { \set fontSize = #-2 \includeLyrics "paroles" }
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'vsoprano \includeNotes "voix"
    >> \keepWithTag #'vsoprano { \includeLyrics "paroles" }
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'valto \includeNotes "voix" \clef "treble"
    >> \keepWithTag #'valto { \includeLyrics "paroles" }
    \new GrandStaff \with { \haraKiriFirst } <<
      \new Staff <<
        $(if (*instrument-name*)
             (make-music 'ContextSpeccedMusic
               'context-type 'GrandStaff
               'element (make-music 'PropertySet
                          'value (make-large-markup (*instrument-name*))
                          'symbol 'instrumentName))
             (make-music 'Music))
        $(or (*score-extra-music*) (make-music 'Music))
        \global \includeNotes "dessus1"
      >>
      \new Staff << \global \includeNotes "dessus2" >>
    >>
  >>
  \layout {
    system-count = #(*system-count*)
    indent = #(if (*instrument-name*)
                  largeindent
                  (or (*score-indent*) smallindent))
  }
}
