\clef "dessus" r2 fa'' re'' fa'' do''1 |
r2 do''' la'' do''' |
sib'' r8 sib'' fa'' sol'' |
la'' do''' sib'' la'' sib'' sib'' sib''8. la''16 sib''1 |
R1*4 |
r2 fa'' |
re'' fa'' do''1 |
R1*3 |
r2 do''' la'' do''' |
sib'' r8 sib'' fa'' sol'' |
la'' do''' sib'' la'' sib'' sib'' sib''8. la''16 sib''1 |
R1*2 |
r2*3/4 r8 sol'' mib'' |
fa'' do''4 si'8 si' si' |
mib''4 sol''8 fa''4 fa''8 |
re'' mib''4 r8 sol'' re'' |
mib'' do''4 r8 sol'' sol'' |
mib'' fa''4 re''8 re'' sol'' |
do''4 fa''8 fa''4 fa''8 |
sol''4 sol''8 do'' re'' fa'' |
do''4. re'' |
s1.*4 |
s2. r4 sib'' sol'' |
la'' la'' fa'' sol'' mib''2 |
fa''4 fa'' fa''8 sol'' la''2 la''4 |
sib''2 fa''4 sol'' sol'' sol'' |
la'' fa'' sib'' fa''2. fa''1*3/4\fermata |
