\clef "viole1" r2 fa' fa' fa' fa'1 |
r2 fa' fa' fa' |
fa' r8 fa' la' mi' |
fa' do' re' mib' fa' sol' fa'4 fa'1 |
R1*4 |
r2 fa' |
fa' fa' fa'1 | \allowPageTurn
R1*3 |
r2 fa' fa' fa' |
fa' r8 fa' la' mi' |
fa' do' re' mib' fa' sol' fa'4 fa'1\fermata |
R1*2 |
r8 sol' lab' fa' sol'4 |
fa'8 lab' lab' sol'4 sol'8 |
sol'4 sol'8 lab'4 lab'8 |
sol' sol' sol' sol'4. |
sol'8 sol' lab' fa' sol' sib' |
lab' lab'4 sol'8 sol' do' |
fa'4 do''8 sib'4 fa'8 |
mib'4 sol'8 fa' fa' fa' |
fa'4. fa' |
