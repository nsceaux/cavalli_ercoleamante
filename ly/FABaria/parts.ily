\piecePartSpecs
#`((dessus #:score "score-dessus" #:music , #{ s1*25 s2.*9\break #})
   (parties #:score "score-parties" #:music , #{ s1*25 s2.*9\break #})
   (violes #:score "score-violes" #:music , #{ s1*25 s2.*9\break #})
   (basse #:score "score-basse" #:music , #{ s1*25 s2.*9\break #}))
