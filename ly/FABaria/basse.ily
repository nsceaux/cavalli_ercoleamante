\clef "basse" r2 sib, re sib, fa1 |
r2 fa la fa |
sib r8 sib la sol |
fa mib re do sib, mib fa4 sib,1-\tag #'basse-continue -\fermata |
<<
  \tag #'basse { R1*4 r2 }
  \tag #'basse-continue {
    sib,1~ |
    sib, |
    sib, |
    sib,2 mib |
    sib,
  }
>> sib,2 |
re sib, fa1 |
<<
  \tag #'basse { R1*3 }
  \tag #'basse-continue {
    fa1~ |
    fa |
    la2 do'4 fa |
  }
>>
r2 fa la fa |
sib r8 sib la sol |
fa mib re do sib, mib fa4 sib,1 |
<<
  \tag #'basse { R1*2 }
  \tag #'basse-continue {
    sib,1 |
    mib |
  }
>>
r8 do' lab sib sol4 |
<<
  \tag #'basse { lab8 lab fa }
  \tag #'basse-continue { lab4 fa8 }
>> sol4 sol8 |
<<
  \tag #'basse { do'4 }
  \tag #'basse-continue { mib4 }
>> mib8 fa4 fa8 |
sol mib do sol4. |
<<
  \tag #'basse {
    do4. r8 sib sol |
    lab fa4 sol8 sol mib |
    fa4 fa8 sib4 re8 |
  }
  \tag #'basse-continue {
    do8 do' lab sib sol4 |
    lab8 fa4 sol mib8 |
    fa4 fa8 re4 re8 |
  }
>>
mib4 mib8 fa re sib, |
fa4. sib, |
<<
  \tag #'basse-continue \sugNotes {
    \clef "alto" r2*3/2 r4 do'' la' |
    sib' sol'2 la'4 \clef "tenor" fa' re' |
    mib' do'2 re'4 re' sib |
    do'2 la4 sib2 sib4 |
    do' la fa do'2. |
    fa4 \clef "bass" fa re mib do2 |
    re4 re sib, fa2 re4 |
    sol2 re4 mib2 mib4 |
    fa re sib, fa2. sib,1*3/4\fermata |
  }
>>