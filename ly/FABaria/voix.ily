<<
  \tag #'(eutyro basse) {
    \clef "vbasse" <>^\markup\character Eutyro
    R1*8 |
    r4 sib8 sib sib4 sib8 sib |
    sib4 sib8 sib sib sib sib sib |
    sib sib sib sib sib4 sib8 la |
    sib sib lab sib sol sol sol fa16 mib |
    sib8 sib r4 r2 |
    R1*2 |
    r8 fa la4 r8 la do' do' |
    do'8 do'16 do' do'8 sib la la fa fa16 sol |
    la la la sib do'8 sib do'4 fa |
    R1*5 |
    r4 sib8 sib sol4 lab8 sib |
    mib4 mib r2 |
    r8 do' lab sib sol4 |
    lab8 lab fa sol4 sol8 |
    do'4 mib8 fa4 fa8 |
    sol mib do sol4. |
    do r8 sib sol |
    lab fa4 sol8 sol mib |
    fa4 fa8 sib4 re8 |
    mib4 mib8 fa re sib, |
    fa4. sib, |
    R2.*19
  }
  \tag #'vsoprano {
    \clef "vsoprano" R1*25 R2.*9 |
    <>^\markup\character Coro
    r4 fa'' re'' mib'' do''2 |
    re''4 re'' sib' do''2 re''4 |
    sib'2 do''4 la'2 sib'4 |
    sol' sol' la' re'' sib' sib' |
    sol' do'' la' sol'2. |
    la'4 do'' re'' sib' do''2 |
    la'4 la' re'' do''2 re''4 |
    sib'2 sib'4 sol'2 do''4 |
    la' sib' sib' sib'2 la'4( sib'1*3/4)\fermata |
  }
  \tag #'valto {
    \clef "valto" R1*25 R2.*9
    r2*3/2 r4 do'' la' |
    sib' sol'2 la'4 la' fa' |
    sol'2 la'4 fa'2 sol'4 |
    mi'2 fa'4 re' re' sol' |
    mi' do' fa' fa'2 mi'4( |
    fa') la' sib' sol' la'2 |
    fa'4 fa' sib' la'2 la'4 |
    sol'2 fa'4 mib'2 sol'4 |
    fa' fa' fa' fa'2. fa'1*3/4\fermata |
  }
  \tag #'vtenor {
    \clef "vtenor" R1*25 R2.*9
    R2.*2 |
    r2*3/2 r4 fa' re' |
    mib' do'2 re'4 re' sib |
    do'2 la4 sib2 sib4 |
    do' la fa do'2. |
    fa4 fa' fa' mib' mib'2 |
    re'4 re' fa' fa'2 fa'4 |
    re'2 re'4 sib2 mib'4 |
    do' re' re' do'2. re'1*3/4\fermata |
  }
  \tag #'vbasse {
    \clef "vbasse" R1*25 R2.*9
    R2.*10 |
    r4 fa re mib do2 |
    re4 re sib, fa2 re4 |
    sol2 re4 mib2 mib4 |
    fa re sib, fa2. sib,1*3/4\fermata |
  }
>>
