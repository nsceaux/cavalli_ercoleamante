\score {
  \new ChoirStaff \with { \haraKiriFirst } <<
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'vtenor \includeNotes "voix" \clef "tenor"
    >> \keepWithTag #'vtenor { \includeLyrics "paroles" }
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'vbasse \includeNotes "voix"
    >> \keepWithTag #'vbasse { \includeLyrics "paroles" }
    \new Staff << \global \keepWithTag #'basse \includeNotes "basse" >>
    \new Staff \with { \tinyStaff \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'basse \includeNotes "voix"
    >> \keepWithTag #'basse { \set fontSize = #-2 \includeLyrics "paroles" }
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
    >>
  >>
  \layout { }
}
