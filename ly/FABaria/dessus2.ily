\clef "dessus" r2 re'' sib' re'' la'1 |
r2 la'' fa'' la'' |
re'' r8 re'' do'' sib' |
fa'' sol'' fa'' mib'' re'' mib'' do''8. re''16 re''1 |
R1*4 |
r2 re'' |
sib' re'' la'1 |
R1*3 |
r2 la'' fa'' la'' |
re'' r8 re'' do'' sib' |
fa'' sol'' fa'' mib'' re'' do'' do''8. re''16 re''1\fermata |
R1*2 |
r8 mib'' do'' re'' sib'4 |
do''8 fa'' fa'' re''4 re''8 |
do''4 do''8 re''4 re''8 |
si' do'' do'' do''4 si'!8 |
do'' mib'' do'' re'' sib'4 |
do''8 do''4 sib'8 sib' mib'' |
la'4 la'8 re''4 sib'8 |
sib'4 do''8 la' sib' sib' |
sib'4 la'8 sib'4. |
s1.*5 |
r4 fa'' sib'' sib'' la''2 |
re''4 re'' re''8 mi'' fa''2 fa''4 |
sol''2 sib''4 mib'' mib'' sib' |
fa'' sib' fa'' do''4. re''8 mib'' fa'' re''1*3/4\fermata |
