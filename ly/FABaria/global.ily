\key fa \major \beginMark "Sinf[onia]"
\time 4/4 \midiTempo#160
\measure 3/1 s1*3
\measure 2/1 s1*2
\measure 4/4 s1
\measure 2/1 s1*2
\measure 4/4 \midiTempo#80 s1*5
\measure 2/1 s1*2
\measure 4/4 s1*3
\measure 2/1 \midiTempo#160 s1*2
\measure 4/4 s1
\measure 2/1 s1*2
\measure 4/4 \midiTempo#80 s1*2
\time 6/8 \midiTempo#80 s2.*9 \bar "|."
\time 6/4 \midiTempo#160 s1.*8
\measure 9/4 s2.*3 \bar "|."