\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket \with {
      \override VerticalAxisGroup.remove-empty = #(eqv? #t (ly:get-option 'pygmalion))
    } <<
      \new Staff <<
        \global \includeNotes "partie1"
        $(or (*score-extra-music*) (make-music 'Music))
      >>
      \new Staff << \global \includeNotes "partie2" >>
      \new Staff << \global \keepWithTag #'basse \includeNotes "basse" >>
    >>

    \new Staff \withLyrics <<
      \global \keepWithTag #'valto \includeNotes "voix" \clef "alto"
    >> \keepWithTag #'valto \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'vtenor \includeNotes "voix" \clef "alto"
    >> \keepWithTag #'vtenor \includeLyrics "paroles"
    \new Staff \withLyrics <<
      { s1*25 s2.*9 s2.\noHaraKiri }
      \global \keepWithTag #'vbasse \includeNotes "voix"
    >> \keepWithTag #'vbasse \includeLyrics "paroles"

    \new Staff \with { \tinyStaff \haraKiri } \withLyrics <<
      \global \keepWithTag #'eutyro \includeNotes "voix"
    >> \keepWithTag #'eutyro { \set fontSize = #-2 \includeLyrics "paroles" }
    
    \new Staff << \global \keepWithTag #'basse-continue \includeNotes "basse" >>
  >>
  \layout {
    system-count = #(*system-count*)
    indent = #(if (*instrument-name*)
                  largeindent
                  (or (*score-indent*) smallindent))
  }
}
