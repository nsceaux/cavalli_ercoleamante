\tag #'(eutyro basse) {
  Co -- me so -- lo ad un gri -- do,
  che giun -- to a pe -- na d’A -- che -- ron -- te al li -- to
  for -- mai, vi ra -- du -- na -- te a -- ni -- me ar -- di -- te?
  Sù sù, co -- sì pur con -- tro il co -- mun ne -- mi -- co
  vos -- tro fu -- ro -- re al -- la mia rab -- bia u -- ni -- te,
  che più dun -- que s’as -- pet -- ta?
  
  Pe -- ra pe -- ra mo -- ra il cru -- del, sù sù sù sù ven -- det -- ta ven -- det -- ta.
  Pe -- ra pe -- ra mo -- ra il cru -- del, sù sù sù sù ven -- det -- ta ven -- det -- ta.
}
\tag #'(vsoprano valto vtenor) {
  Pe -- ra pe -- ra mo -- ra il cru -- del, sù sù
  \tag #'(vsoprano valto) { sù sù ven -- det -- ta }
  \tag #'vsoprano { ven -- det -- ta }
  ven -- det -- ta ven -- det -- ta.
}
\tag #'(vsoprano valto vtenor vbasse) {
  Pe -- ra pe -- ra mo -- ra il cru -- del, sù sù sù sù ven -- det -- ta ven -- det -- ta.
}
