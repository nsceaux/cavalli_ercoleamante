\clef "viole2" r2 sib sib sib do'1 |
r2 do' do' do' |
re' r8 re' fa' sib |
do' sol sib do' re' do' do'8. sib16 sib1 |
R1*4 |
r2 sib |
sib sib do'1 |
R1*3 |
r2 do' do' do' |
re' r8 re' fa' sib |
do' sol sib do' re' mib' do'8. sib16 sib1 |
R1*2 |
r8 mib' fa' re' mib'4 |
do'8 do' re' re'4 re'8 |
mib'4 do'8 fa'4 re'8 |
re' do' do' re'4. |
do'8 do' fa' re' re' mib' |
do' do' fa' sib sib sol |
do'4 do'8 re'4 re'8 |
sol4 mib'8 do' sib sib |
do'4. sib |
