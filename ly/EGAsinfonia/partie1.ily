\transpose do #(if (eqv? #t (ly:get-option 'pygmalion))
                   (ly:make-pitch -2 4)
                   (ly:make-pitch -1 0)) {
\clef "viole1" do'1 |
do' do'\breve\fermata |
sol'1 sol' |
sol'\breve\fermata |
sol'2 lab' sol' sol' |
sol'\breve sol'\fermata |
}
