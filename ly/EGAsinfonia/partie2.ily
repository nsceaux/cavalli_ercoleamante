\transpose do #(if (eqv? #t (ly:get-option 'pygmalion))
                   (ly:make-pitch -2 4)
                   (ly:make-pitch -1 0)) {
\clef "viole2" sol1 |
fa sol\breve\fermata |
re'1 do' |
re'\breve\fermata |
do'2 re' re' sol |
re'\breve do'\fermata |
}
