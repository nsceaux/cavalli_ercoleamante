\transpose do #(if (eqv? #t (ly:get-option 'pygmalion))
                   (ly:make-pitch -2 4)
                   (ly:make-pitch -1 0)) {
\clef "dessus" sol'1 |
lab' sol'\breve\fermata |
re''1 mib'' |
re''\breve\fermata |
sol''2 fa'' re'' mib'' |
re''\breve mi''\breve\fermata |
}
