\transpose do #(if (eqv? #t (ly:get-option 'pygmalion))
                   (ly:make-pitch -2 4)
                   (ly:make-pitch -1 0)) {
\clef "basse" do1 |
fa, do\breve |
sol1 do |
sol\breve |
mib2 fa sol do |
sol\breve do1*2\fermata |
}
