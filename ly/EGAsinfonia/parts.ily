\piecePartSpecs
#`((dessus)
   (parties)
   (violes)
   (basse #:score-template "score-basse-continue")
   (silence #:on-the-fly-markup , #{ \markup\tacet #}))
