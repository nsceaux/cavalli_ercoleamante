\transpose do #(if (eqv? #t (ly:get-option 'pygmalion))
                   (ly:make-pitch -2 4)
                   (ly:make-pitch -1 0)) {
\clef "dessus" mib'1 |
fa'1. mi'4 re' mi'1\fermata |
si'1 do''~ |
do''2 si'4 la' si'!1\fermata |
mib''2 re'' si' do''~ |
do'' si'4 la' si'!1 do''\breve\fermata |
}
