\score {
  <<
    \new StaffGroup <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
      \original <<
        \new Staff << \global \includeNotes "partie1" >>
        \new Staff << \global \includeNotes "partie2" >>
        \new Staff << \global \keepWithTag #'basse \includeNotes "basse" >>
      >>
    >>
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix1 \includeNotes "voix"
      >> \keepWithTag #'voix1 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix2 \includeNotes "voix"
      >> \keepWithTag #'voix2 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix3 \includeNotes "voix"
      >> \keepWithTag #'voix3 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix4 \includeNotes "voix"
      >> \keepWithTag #'voix4 \includeLyrics "paroles"
    >>
    \new Staff <<
      \global \keepWithTag #'basse-continue-conducteur \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2.*13\pageBreak
        s2.*12\pageBreak
        s2.*12\pageBreak
        s2.*3 s1*3\pageBreak
        s2.*13\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
