\clef "basse"
\tag #'(basse-continue basse-continue-conducteur) {
  do2 do4 |
  sol4. fad8 sol4 la si2 |
  do'2.~ do'4 si2 |
  la2. sol |
  <>^"Tutti" do4. si,8 do4 re mi2 fa2. fa4 mi2 |
  re2. la |
  <>^"[B.C.]" do'4 si2 la do'4 |
  sol2 sol4 re do re |
  mi re mi do re4. do8 si,2 si,4 |
  mi do do re2 do re |
  <>^"Tutti" sol,4. fad,8 sol,4 la, si,2 |
  do2.~ do4 si,2 |
  la,2. sol,4 sol2~ |
  sol4 fa fa fa mi2 |
  re2. do |
  sol4 fa sol la sol la fa sol4. fa8 mi2 mi4 |
  la4 fa2 sol fa sol |
  do2.\fermata do4 |
  fa2 r4 re |
  la2 sold |
  <<
    \tag #'basse-continue {
      la4 r r |
      la mi fa dod re2 |
      la, sold,4 la, si,2 |
      do4. si,8 la,4 mi2 mi4 |
      la2 sold4 la2 mi4 |
      la2 sold4 la2 mi4 |
      fa dod2 re4. re8 la,4 |
      mi2 la,4 do'2 si4 do'2 sol4 do'2 si4 |
      do'2 sol4 la mi2 |
      fa4. fa8 do4 sol2 do4 do'2 sol4 |
      la4. la8 fa4 mi |
      sol1 do\fermata |
    }
    \tag #'basse-continue-conducteur \sugNotes {
      la4 r r |
      la mi fa dod re2 |
      la, sold,4 la, si,2 |
      do4. si,8 la,4 mi2 mi4 |
      la2 sold4 la2 mi4 |
      la2 sold4 la2 mi4 |
      fa dod2 re4. re8 la,4 |
      mi2 la,4 do'2 si4 do'2 sol4 do'2 si4 |
      do'2 sol4 la mi2 |
      fa4. fa8 do4 sol2 do4 do'2 sol4 |
      la4. la8 fa4 mi |
      sol1 do\fermata |
    }
  >>
}

