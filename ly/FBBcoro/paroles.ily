\tag #'(voix1 basse) {
  \tag #'basse {
    Pro -- nu -- ba, è cas -- ta dè-
  }
  Pro -- nu -- ba, è cas -- ta dè -- a
  è cas -- ta dè -- a
  \tag #'basse {
    l’al -- me de’ nuo -- vi spo-
  }
  l’al -- me de’ nuo -- vi spo -- si
  con lac -- ci av -- ven -- tu -- ro -- si
  an -- no -- da, e be -- a.
  Pro -- nu -- ba, è cas -- ta dè -- a
  l’al -- me de’ nuo -- vi spo -- si
  con lac -- ci av -- ven -- tu -- ro -- si
  an -- no -- da, an -- no -- da, e be -- a. __
  
}
\tag #'voix2 {
  Pro -- nu -- ba, è cas -- ta dè -- a
  è cas -- ta è cas -- ta dè -- a
  l’al -- me de’ nuo -- vi spo -- si
  con lac -- ci av -- ven -- tu -- ro -- si
  an -- no -- da an -- no -- da, e be -- a.
  Pro -- nu -- ba, è cas -- ta dè -- a è cas -- ta dè -- a
  l’al -- me de’ nuo -- vi spo -- si
  con lac -- ci av -- ven -- tu -- ro -- si
  an -- no -- da, an -- no -- da, e be -- a.
}
\tag #'voix3 {
  Pro -- nu -- ba, è cas -- ta dè -- a
  è cas -- ta è cas -- ta dè -- a
  l’al -- me de’ nuo -- vi spo -- si
  con lac -- ci av -- ven -- tu -- ro -- si
  an -- no -- da an -- no -- da an -- no -- da, e be -- a.
  Pro -- nu -- ba, è cas -- ta dè -- a
  l’al -- me de’ nuo -- vi spo -- si
  con lac -- ci av -- ven -- tu -- ro -- si
  an -- no -- da, an -- no -- da, e be -- a.
}
\tag #'voix4 {
  Pro -- nu -- ba, è cas -- ta dè -- a
  Pro -- nu -- ba, è cas -- ta dè -- a
  l’al -- me de’ nuo -- vi spo -- si
  con lac -- ci av -- ven -- tu -- ro -- si
  an -- no -- da, an -- no -- da, an -- no -- da, e be -- a.
}

E quie -- ta, e gio -- con -- da
da’ da’ da’ lor nes -- to -- rea vi -- ta,
e gl’am -- ples -- si fe -- con -- da
con pro -- ge -- nie
con pro -- ge -- nie
con pro -- ge -- nie in -- fi -- ni -- ta
con pro -- ge -- nie
con pro -- ge -- nie
con pro -- ge -- nie in -- fi -- ni -- ta
con pro -- ge -- nie in -- fi -- ni -- ta.
