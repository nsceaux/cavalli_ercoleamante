\clef "viole2" re4 |
fa2 sol8 la sib4 la re' |
la do' sol la2 sol8 do' |
sib do' re' sol sol la fad4 re mi |
fa4 do sol8 fad la4 re8 sib la8. sol16 |
sol2 re4 re1*3/4\fermata |
