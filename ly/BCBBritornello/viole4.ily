\clef "basse" sib,4 |
la, do sol, re2 fa4 |
mi8 fad sol la sib4 mi la, re~ |
re8 mi sol4 mi8 la, la,4 sib,2 |
sib,4 la,2 re sol8 fa |
mib4 re la, si,1*3/4\fermata |
