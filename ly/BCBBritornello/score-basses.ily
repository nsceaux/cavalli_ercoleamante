\score {
  \new StaffGroup <<
    \new Staff << \global \includeNotes "viole4" >>
    \new Staff << \global \includeNotes "basse" >>
  >>
  \layout { indent = 0 }
}
