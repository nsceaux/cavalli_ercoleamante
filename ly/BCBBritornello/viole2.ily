\clef "viole1" sol4 |
la la4. fad8 fad4. sol8 la si |
do' re' mib'4 re'8 dod' dod'4. re'8 mi' fad' |
sol' sol si4 dod' re'2 sol4 |
re8 mi fa sol la4 sol fad8 sol do' sib |
la sib sol4 fad sol1*3/4\fermata |
