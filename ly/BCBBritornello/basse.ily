\clef "basse" sol,4 |
fa,2 mib,4 re,2 re4 |
do2 sib,4 la,2 sib,8 la, |
sol,2 la,4 re,2 sol,4 |
fa,2 mib,4 re,4. re8 mib re |
do4 re re, sol,1*3/4\fermata |
