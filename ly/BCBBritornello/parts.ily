\piecePartSpecs
#`((violes #:score "score-violes")
   (basse #:score "score-basses")
   (silence #:on-the-fly-markup , #{ \markup { Ritornello: tacet } #}))
