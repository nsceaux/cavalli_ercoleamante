\score {
  \new StaffGroup <<
    \new Staff << \global \includeNotes "viole1" >>
    \new Staff << \global \includeNotes "viole2" >>
    \new Staff << \global \includeNotes "viole3" >>
    \new Staff << \global \includeNotes "viole4" >>
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
    >>
  >>
  \layout {
    indent = \noindent
    system-count = 1
  }
  \midi { }
}
