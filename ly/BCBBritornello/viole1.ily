\clef "viole1" sib8 do' |
re'4 do' do'8 re' re'4 re' fa'8 sol' |
la'4 sol' sol'8 sol' sol'4 fa' sol'8. la'16 |
sib'4 fa' mi'8 fa' re'2 sib8 do' |
re'4 do' do'8 do' do'4 sib do'8 re' |
mib'4 sib la8 sib sol1*3/4\fermata |
