\clef "viole2" r4 r fa |
fa2. fa |
fa2 fa4 sib2 fa4 |
fa2 sol4 do'2. |
R2.*5 |
r4 r do' |
do'2. do' |
do'2 do'4 fa'2 do'4 |
do'2 re'4 |
sol'2. r2*3/2 |
R2.*3 |
re'4 re' la sib2 re'4 |
sib2 re'4 sib2 re'4 |
sib4 sol2 |
la r4 mi' |
do'8 do' re' re' sib4 do' |
la sib sol la8 la |
la la re'4 sib la8 re' la'2 mi' fad'1 |
R2.*2 |
r2*3/2 r4 do' do' |
mi'2 la4 re' re'2 |
do' do'4 r2*3/2 |
r r4 re' re' |
fad'2 si4 mi' mi'2 |
re'2 re'4 r la' fa' |
sol' re' mi' do'2 re'4 |
do'2 do'4 |
re'2 r4 la |
re2 sol do'1 |
r4 do'2 do'4 fa'2 fa' |
r4 fa'2 fa'8 fa' fa'2 fa' |
sib2. do' |
sib sol |
re'2 sib4 do'2 do'4 sol' re' sol' |
do' re' mib' sib fa'2 |
fa' re' |
sol2. do'4 do'1 |
fa2. sol |
fa sib |
la2 re'4 sol sib sol |
re' la re' sol la sib |
sol do'2 do'1*3/4\fermata |
