\clef "viole1" r4 r do' |
do'2. do' |
fa' fa' |
fa' fa' |
R2.*5 |
r4 r sol' |
sol'2. sol' |
do'' do'' |
do'' |
do'' r2*3/2 |
R2.*3 |
fad'4 fad' fad' re'2 la4 |
re'2 la4 re'2 la4 |
re'2. |
re'2 r4 sol' |
la'8 la' fa' fa' sol'4 mi' |
fa' re' mi' do'8 do' |
re' re' fa'4 re' la'8 sol' fa'4 re' la'2 la'1 |
R2.*2 | \allowPageTurn
r2*3/2 r4 fa' fa' |
do'2 fa'4 fa' sol'2 |
sol' sol'4 r2*3/2 |
r r4 sol' sol' |
re'2 sol'4 sol' la'2 |
la'2 la'4 r do'' do'' |
sib'2 sib'4 la'2 la'4 |
do''2 fa'4 |
sib'2 r4 do' |
si2. si4 sol1 |
r4 mi'2 mi'4 do'2 do' |
r4 la'2 la'8 fa' sib'2 sib' |
fa'2. fa' |
fa' sol' |
sib'2 sib'4 la' sol' la' sib' re'' sib' |
la' fa' sol' mib' do'2 |
re' fa' |
mib'2. sol'4 fa'1 |
do'2. do' |
do' re' |
fa'2 fa'4 mi' re' mi' |
fa' la' fa' do' do' re' |
sol' sol'2 \sug fa'1*3/4\fermata |
