\clef "dessus" r4 r la' |
la'4. sol'8 la' sib' do''4 la' do'' |
re''2 do''4 re''2 do''4 |
re''2 mi''4 fa''2. |
R2.*5 |
r4 r mi'' |
mi''4. re''8 mi'' fa'' sol''4 mi'' sol'' |
la''2 sol''4 la''2 sol''4 |
la''2 si''4 |
do'''2. r2*3/2 |
R2.*3 |
re''4 re'' do'' sib'2 la'4 |
sib'2 la'4 sib'2 la'4 |
sib'2 do''4 |
la'2 sol''4 mi''8 mi'' |
fa'' fa'' re''4 mi'' do'' |
re'' sib'8 sib' do'' do'' la'4 |
la'' fa'' sol'' dod''8 re'' fa''2 mi'' re''1 |
R2.*2 | \allowPageTurn
r2*3/2 r4 do'' do'' |
do''2 do''4 re'' re''2 |
mi'' mi''4 r2*3/2 |
r r4 re'' re'' |
re''2 re''4 mi'' mi''2 |
fa''2 fa''4 r la'' la'' |
sol''2 mi''4 fa''2 do''4 |
mi''4. re''8 do''4 |
sol''2 r4 la' |
sol'2. sol'4 sol'1 |
r4 do''2 do''8 do'' do''2 do'' |
r4 fa''2 fa''8 fa'' fa''2 fa'' |
fa''4 mi'' re'' do'' sib' do'' |
re'' do'' re'' mib'' re'' mib'' |
fa''2 fa''4 r2*3/2 sib''4 la'' sol'' |
fa'' re'' sol'' sol'' fa''2 |
re''4 fa''2 fa''8 fa'' |
sol''2 sol''4 sol''8 sol'' la''2 la'' |
do''4 sib' la' sol' fa' sol' |
la' sol' la' sib' la' sib' |
do''2. r2*3/2 |
fa''4 mi'' re'' do'' la' re'' |
re'' do''2 la'1*3/4\fermata |
