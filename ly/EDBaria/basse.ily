\clef "basse" fa,2 fa,4 |
fa,2. fa,~ |
fa,~ fa,2 fa,4 |
sib,2. fa, |
fa, |
fa,~ fa, |
fa,~ fa, |
do2 do4 |
do2. do~ |
do~ do |
fa |
do la2 sib4 |
la2 sib4 sol2 fad4 |
sol2 sol4 |
re2. re~ |
re~ re |
sol, |
re2 si4 do' |
la sib sol la |
fa sol mi fa |
re2 sol la1 re | \allowPageTurn
r4 sib sib la2 sib4 |
sol do'2 fa4 fa fa |
mi2 fa4 re sol2 |
do do'4 sib2 do'4 |
la re2 sol4 sol sol |
fad2 sol4 mi la2 |
re2 re'4 do'2 la4 |
sib2 sol4 la2 fa4 |
do'2 la4 |
sib2. la,4 |
si,1 do |
mi fa |
la sib |
sib,2. fa |
re do |
sib, fa sol |
la4 sib mib mib fa2 |
sib,2 re |
mib2. mi4 fa1 |
fa,2. do |
la, sol, |
fa, do |
re mi4 fa sib, |
sib, do2 fa,1*3/4\fermata |
