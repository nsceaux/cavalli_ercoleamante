\tag #'(couplet1 basse) {
  A -- man -- ti che tra pe -- ne
  ogn’ hor gri -- da -- te ohi -- mè ohi -- mè:
  per -- ché bra -- ma -- te di mo -- rir, per -- ché, per -- ché?
  Ah non ne -- ga -- te mai mai mai
  Ah non ne -- ga -- te mai mai mai fe -- de al -- la spe -- ne.
  Per chi vi -- ve il ciel gi -- ra,
  e non sem -- pre un sos -- pi -- ra,
  an -- zi lie -- to lie -- to lie -- to è tal’ or chi mes -- to chi mes -- to fu,
  ma per chi mo -- re
  ma per chi mo -- re il ciel non gi -- ra non gi -- ra non gi -- ra non gi -- ra più.
  ma per chi mo -- re per chi mo -- re
  il ciel non gi -- ra non gi -- ra non gi -- ra non gi -- ra più.
}
\original \tag #'couplet2 {
  \override Lyrics.LyricText.font-shape = #'italic
  [O stol -- ti, ov’ è il ris -- to -- ro
  nel mo -- rir poi? dov’ è? dov’ è?
  E che val più di vos -- tra vi -- ta, e che? e che?
  Ah non si può dar mai mai mai
  Ah non si può dar mai mai mai più gran te -- so -- ro.
  E sian pur _ buo -- ne o fel -- le
  sti -- le al par can -- gian le stel -- le
  né può sem -- pre sem -- pre sem -- pre il des -- tin _ gi -- re all’ in gi -- ù]
}
