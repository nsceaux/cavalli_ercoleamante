\clef "vbasse" <>^\markup\character Nettuno
R2.*5 |
r2*3/2 r4 r fa |
fa4. mi8 fa sol |
la4 fa la sib2 la4 |
sib2 la4 sib2 la4 |
do'2 r4 |
R2.*5 |
do'4 do' sib la2 sib4 |
la2 sib4 sol2 fad4 |
sol2 sol4 |
re'2 r4 r2*3/2 |
R2.*3 |
r4 re' si8 si do' do' |
la4 sib sol la |
fa8 fa sol sol mi4 fa |
re2 sol4 la8 sib la1 re |
r4 sib sib la2 sib4 |
sol do'2 fa fa4 |
R2.*2 |
r4 do' do' sib2 do'4 |
la re'2 sol sol4 |
R2.*2 |
r4 re' re' do'2 la4 |
sib2 sol4 la4. sol8 fa4 |
do'2 la4 |
sib2 sib4 r8 la, |
si,2. si,8[ do] do1 |
r4 mi r mi8 fa fa2 fa |
r4 la r la8 sib sib2 sib |
R2.*4 |
sib4 la sol fa mib fa sol fa sol |
la sib mib mib fa2 |
sib,4 re r re8 re |
mib2 mib4 mi8 fa fa2 fa |
R2.*4 |
fa4 mi re do sib, do |
re do re mi fa sib, |
sib, do2 fa,2.\fermata |
