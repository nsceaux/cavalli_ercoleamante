\key fa \major
\digitTime\time 3/4 \midiTempo#200 s2.
\measure 6/4 s1.*3
\measure 3/4 s2.
\measure 6/4 s1.*2
\measure 3/4 s2.
\measure 6/4 s1.*2
\measure 3/4 s2.
\measure 6/4 s1.*2
\measure 3/4 s2.
\measure 6/4 s1.*2
\measure 3/4 s2.
\time 4/4 s1*3
\measure 3/1 s1*3
\digitTime\time 3/4 \measure 6/4 s1.*8
\measure 3/4 s2.
\time 4/4 s1
\measure 2/1 s1*6
\digitTime\time 3/4 \measure 6/4 s1.*2
\measure 9/4 s2.*3
\measure 6/4 s1.
\time 4/4 s1
\measure 2/1 s1*2
\digitTime\time 3/4 \measure 6/4 s1.*5 \bar "|."
