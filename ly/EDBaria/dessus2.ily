\clef "dessus" r4 r fa' |
fa'4. mi'8 fa' sol' la'4 fa' la' |
sib'2 la'4 sib'2 la'4 |
sib'2 do''4 la'2. |
R2.*5 |
r4 r do'' |
do''4. si'8 do'' re'' mi''4 do'' mi'' |
fa''2 mi''4 fa''2 mi''4 |
fa''2 sol''4 |
mi''2. r2*3/2 |
R2.*3 |
la'4 la' la' sol'2 fad'4 |
sol'2 fad'4 sol'2 fad'4 |
sol'2 la'4 |
fad'2 re''4 do''8 do'' |
do'' do'' sib'4 sib' la' |
la'' sol''8 sol'' sol'' sol'' fa''4 |
fa'' la'' mi'' fa''8 sol'' dod''4 re''2 dod''4 re''1 |
R2.*2 |
r2*3/2 r4 la' la' |
sol'2 la'4 la' si'2 |
do''2 do''4 r2*3/2 |
r r4 sib' sib' |
la'2 si'4 si' dod''2 |
re''2 re''4 r mi'' fa'' |
re''2 sol'4 do'' fa'2 |
sol' la'4 |
re''2 r4 fa' |
re'2. re'4 mi'1 |
r4 sol'2 sol'8 sol' la'2 la' |
r4 do''2 do''8 do'' re''2 re'' |
re''4 do'' sib' la' sol' la' |
sib' la' sib' do'' sib' do'' |
re''2 re''4 fa'' sol'' fa'' mib'' fa'' mib'' |
do'' sib'2 sib'4 la'2 |
sib'4 sib'2 sib'8 sib' |
sib'2 sib'4 do''8 do'' do''2 do'' |
la'4 sol' fa' mi' re' mi' |
fa' mi' fa' sol' fa' sol' |
la'2 la'4 do'' re'' do'' |
sib' do'' sib' sol' fa' fa' |
sol' mi'2 fa'1*3/4\fermata |
