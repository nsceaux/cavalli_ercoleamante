\clef "dessus" mi'2 mi'4 mi'2 mi'4 |
mi'2\fermata r4 |
si'2 si'4 |
mi'' sol''4. la''8 |
fad''2. |
fad''4 si'2 |
si'2 si'4 |
red''2 red''4 |
mi''4 dod''2 re''4 dod''!2 si'4 re'' dod''! |
si'2. la'2 re''4 |
mi'' sol'' re'' |
la'' mi'' re'' mi'' re''2 |
re''2. |
r4 sol' la' si'4. re''8 do'' si' |
mi''2 fad''4 |
sol'' sol''8 sol'' sol'' sol'' fad''2. |
si'4. si'8 si'4 |
si'2 si'4 r2*3/2 do''4 do'' do'' |
dod''2. r4 re'' re'' |
red''2. |
do''4 do'' do'' do''2 do''4 |
do'' si'2 |
si'4 mi''4. mi''8 re''4 sol''4. sol''8 |
fad''4 la''4. la''8 sol''2. |
la''4 red'' mi'' mi''2 red''4 mi''1*3/4\fermata |
