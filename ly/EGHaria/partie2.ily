\clef "viole2" mi2 mi4 mi2 mi4 |
mi2\fermata r4 |
si2 si4 |
sol sol mi |
si2. |
si |
si2 si4 |
la2 la4 |
mi'2. si4 dod' fad' fad' si dod' |
re'2. la4 re' fad' |
r do' re' |
la sol2 sol'4 re'2 |
re'4 re' fad' |
si2. si4 si2 |
do'4 la2 |
mi'2. dod' |
fad'4. fad'8 mi'4 |
si2 si4 r2*3/2 mi'4 mi' mi' |
la'2. r4 re' re' |
si2. |
mi'4 mi' sol' do'2 do'4 |
mi' si2 |
si4 si4. si8 re'4 re'4. re'8 |
fad'4 mi'4. mi'8 sol'2. |
mi'4 fad' mi' si2. si1*3/4\fermata |
