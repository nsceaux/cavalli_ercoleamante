\clef "viole1" si2 si4 si2 si4 |
si2\fermata r4 |
mi'2 mi'4 |
mi' mi'2 |
fad'2. |
fad' |
mi'2 mi'4 |
la'2 la'4 |
sol'2. re'4 fad' dod' re' fad' fad' |
sol'2. mi'4 fad' re' |
sol'2 fad'4 |
mi'2 sol'4 mi' la'2 |
si'4 sol' re' |
mi'2. fad'4. fad'8 fad' re' |
mi'4 do'2 |
sol'4 si'8 si' si' si' la'2. |
red'4. red'8 sol'4 |
fad'2 fad'4 r2*3/2 la'4 la' la' |
la'2. r4 si' si' |
sol'2. |
sol'4 sol' mi' la'2 la'4 |
la' mi'4. fad'8 |
sol'4 sol'4. sol'8 si'4 si'4. sol'8 |
la'4 la'4. la'8 si'2. |
la'4 la' do'' fad'2. sold'1*3/4\fermata |
