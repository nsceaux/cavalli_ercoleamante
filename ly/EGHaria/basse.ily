\clef "basse" mi,2 mi,4 mi,2 mi,4 |
mi,2\fermata r4 |
<<
  \tag #'basse { mi2 mi4 | }
  \tag #'basse-continue { mi2. | }
>>
mi4 do2 |
si,2. |
si |
<<
  \tag #'basse {
    sol2 sol4 |
    fad2 fad4 |
    mi2 mi4
  }
  \tag #'basse-continue {
    sol2. |
    fad |
    mi
  }
>> fad2. si,4 si la |
sol2. fad |
mi2 re4 |
do2 si,4 do re2 |
sol,4 sol fad |
<<
  \tag #'basse {
    mi2 mi4 si,2 si4 |
    la2 la4 |
  }
  \tag #'basse-continue {
    mi2. si |
    la |
  }
>>
sol2. la |
si2 mi4 |
si2. <<
  \tag #'basse {
    r2*3/2 la4 la la |
    la,2. r4 si si |
    si,2. |
    do'4 do' do' la2 la4 |
  }
  \tag #'basse-continue {
    sol2. la |
    la si |
    si |
    do' la |
  }
>>
la4 si2 |
mi4 mi4. mi8 si4 sol4. sol8 |
re'4 la4. la8 mi'2. |
la4 la la si2. mi1*3/4\fermata |
