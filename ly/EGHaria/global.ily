\key la \minor \midiTempo#120
\beginMark "Sinf[onia]" \tempo "Douxemans"
\digitTime\time 3/4 \measure 6/4 s1.
\measure 3/4 s2. \original \bar ":||:" \pygmalion \bar ":|." s2.*6
\measure 9/4 s2.*3
\measure 6/4 s1.
\measure 3/4 s2.
\measure 6/4 s1.
\measure 3/4 s2.
\measure 6/4 s1.
\measure 3/4 s2.
\measure 6/4 s1.
\measure 3/4 s2.
\measure 9/4 s2.*3
\measure 6/4 s1.
\measure 3/4 s2.
\measure 6/4 s1.
\measure 3/4 s2.
\beginMark "Ritor[nello]" \measure 6/4 s1.*2
\measure 9/4 s2.*3 \bar "|."

