\tag #'(couplet1 basse) {
  Hyl -- lo il mio be -- ne è mor -- to? al -- tro che pian -- ti
  vuol da me vuol da me tal do -- lo -- re: __
  e -- gli sol per mio a -- mo -- re
  dis -- pe -- ra -- to s’uc -- ci -- se, ed io fra tan -- ti
  se -- gni del -- la sua fé sem -- pre sem -- pre più chia -- ri
  fia ch’a mo -- rir a mo -- rir dal -- la sua fe -- de im -- pa -- ri. __
}
\original\tag #'couplet2 {
  \override Lyrics.LyricText.font-shape = #'italic
  [Trop -- po io pre -- gia -- i la vi -- ta, ed or m’av -- veg -- gio
  quan -- _ to il _ mo -- rir più va -- le; __
  ques -- ta spo -- glia mor -- ta -- le
  sco -- po è sol di sven -- tu -- re, e de -- gno seg -- gio
  d’Amor so -- no gli e -- li -- sei, ov’ ei ov’ ei più splen -- de
  né ti -- ran -- nia ti -- ran -- nia, né duo -- lo al -- cun l’of -- fen -- de.]
}
