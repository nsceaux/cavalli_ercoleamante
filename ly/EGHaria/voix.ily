\clef "vsoprano" <>^\markup\character Iole
R2.*3 |
mi''2 mi''4 |
do''8 si' si'4 si'8 la' |
si'2 si'4 |
r4 red'' red''8 mi'' |
mi''4 mi'' mi''8 red'' |
fad''2 fad''8 mi'' |
sol''2 dod''8 re'' si'2 lad'4( si'2.) |
r4 si' dod'' re''2 la'8 si' |
do''4 do'' si'8 si' |
mi''4 sol'4. la'8 si'4( la'2) |
sol' r4 |
r4 r8 mi'' re'' dod'' re''2 re''4 |
do''4 do''8 do'' do'' si' |
si'2. dod''2 dod''4 |
red''4. red''8 mi''4 |
fad''2 fad''4 si' si' si' do''2. |
r4 dod'' dod'' re''2. |
red''4 red'' red'' |
mi''2. mi'' |
do''4 sol'4. fad'8( |
mi'2.) r2*3/2 |
R2.*5 |
