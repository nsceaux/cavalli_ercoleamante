\clef "dessus" sol'2 sol'4 sol'2 sol'4 |
sol'2\fermata r4 |
sol''2 sol''4 |
sol'' mi''4. fad''8 |
red''2. |
red''4 fad'' fad''8 sol'' |
sol''2 sol''4 |
la''2 la''8 si'' |
si''2 sol''4 fad''2. fad'' |
r4 re'' mi'' fad'' la''2 |
sol''4 mi'' si'' |
do'''2 sol''4~ sol'' sol''4. fad''8 |
sol''4 si'' la'' |
sol''2. r4 r8 fad'' fad'' sold'' |
la''2 la''4 |
mi''4 mi''8 mi'' mi'' mi'' la''2. |
fad''4. fad''8 sol''4 |
red''2 red''4 r2*3/2 mi''4 mi'' mi'' |
fad''2. r4 fad'' fad'' |
sol''2. |
sol''4 sol'' sol'' la''2 la''4 |
mi'' mi''4. red''8 |
mi''4 sol''4. sol''8 fad''4 si''4. si''8 |
la''4 do'''4. do'''8 si''2. |
do'''4 fad'' sol'' fad''2. mi''1*3/4\fermata |
