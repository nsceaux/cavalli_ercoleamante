\piecePartSpecs
#(if (eqv? #t (ly:get-option 'pygmalion))
     '((parties)
       (violes)
       (basse #:score "score-basse")
       (silence #:on-the-fly-markup , #{ \markup\tacet #}))
     '((parties)
       (basse)
       (silence #:on-the-fly-markup , #{ \markup\tacet #})))
