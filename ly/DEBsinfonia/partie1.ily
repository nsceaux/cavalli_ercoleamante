\clef "viole1" fa'2 la'4 do'' sol'1\fermata |
sol'2 re' sol' fa' |
sol'1 la'\fermata |
fa'2 fa' sol'1\fermata |
do'2 do' re'1\fermata |
r4 sol'2 sol'4 fa'2. sol'4 |
do'2 fa' fa'1 fa'\fermata |
