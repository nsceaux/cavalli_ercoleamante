\clef "dessus" fa''2 fad'' sol''1 |
re''2 re'' mib'' fa'' |
fa'' mi'' fa''1\fermata |
do''2 re'' mib''1\fermata |
sol'2 la' sib'1\fermata |
re''2 mib'' fa'' re'' |
do'' re'' mib''1 re''\fermata |
