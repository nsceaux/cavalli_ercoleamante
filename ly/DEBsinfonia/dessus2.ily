\clef "dessus" re''2 do'' sib'1\fermata |
sib'2 si' do'' re'' |
re'' sib' do''1\fermata |
la'2 si' do''1\fermata |
mi'2 fad' sol'1\fermata |
sib'2 do'' re'' sib' |
la' sib'1 la'2 sib'1\fermata |
