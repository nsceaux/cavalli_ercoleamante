\clef "basse" sib,2 la, sol,1\fermata |
sol2 fa mib re |
sol1 fa\fermata |
fa2 re do1 |
do2 la, sol,1\fermata |
sol2 mib re2. mib4 |
fa2 sib, fa1 sib,1\fermata |
