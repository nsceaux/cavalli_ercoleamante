\clef "viole2" sib2 do' re'1\fermata |
re'2 fa' do' sib |
sib sol do'1\fermata |
do'2 fa do'1\fermata |
sol2 do sol1\fermata |
r4 re' sol2 sib2. sib4 |
fa'2 re' do'1 re'\fermata |
