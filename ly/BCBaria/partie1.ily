\clef "viole1" R4*43 |
re'8 re' |
fa'4 fa' do''8 sib' sib'4 la' la'8 la' |
do''4 do'' sol'8 sib' mi'4 la' sol'8 fa' |
sib'4 sib' mi'8 la' la'2 re'8 mi' |
fa'4 fa' do''8 la' la'4 re' sol'8 fa' |
mib'4 sol' re'8 re' re'1\fermata |
R1*12 |
sib'4 sib' la' re' |
re' sol' mi' fa' |
sol' la' fa'8 do'16 re' mi' fa' sol' la' |
sib'2 r4 sol' |
sol' re' re'2 |
