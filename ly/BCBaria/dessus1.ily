\clef "dessus" R4*43 |
sib'8. do''16 |
re''4 do'' do''8 re'' re''4 re'' fa''8. sol''16 |
la''4 sol'' sol''8 sol'' sol''4 fa'' sol''8. la''16 |
sib''4 fa'' mi''8 fa'' re''2 sib'8 do'' |
re''4 do'' do''8 do'' do''4 sib' do''8 re'' |
mib''4 sib' la'8 sib' sol'1\fermata |
R1*12 |
r8 sib''16 la'' sol'' fa'' mib'' re'' do'' do'' sib' do'' la'8. la'16 |
sol'2 r8 la''16 sol'' fa'' mi'' re'' do'' |
si'8 dod''16 re'' re''8. dod''16 re''2 |
r8 la'16 sib' do'' re'' mi'' fa'' sol''8 sib''16 la'' sol'' fa'' mib'' re'' |
do'' do'' sib' do'' la'8. la'16 sol'2 |
