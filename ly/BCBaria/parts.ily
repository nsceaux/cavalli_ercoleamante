\piecePartSpecs
#`((dessus #:score-template "score-dessus-voix"
           #:music ,#{ s4*43\break s4*32\break s1*12 \break #})
   (parties #:score-template "score-parties-voix"
           #:music ,#{ s4*43\break s4*32\break s1*12 \break #})
   (basse #:score "score-basse"))
