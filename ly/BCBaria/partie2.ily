\clef "viole2" R4*43 |
sib8 sib |
la4 do' sol8 sol re'4 re' re'8 fa' |
mi'4 do' re'8 re' la'4 la8 fa' sib8 do' |
re'4 re' la'8 mi' fad'2 sib8 sib |
la4 do' sol8 sol re'4 re' sol8 re' |
sol4 mib' la8 re' si1\fermata |
R1*12 |
re'4 re' mib' la |
sib re' la' la' |
re'8 si mi' la la2 |
r8 fa16 sol la sib do' re' sib4 re' |
la la si2 |
