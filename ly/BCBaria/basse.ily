\clef "basse"
sol,2 sol4 |
fa2 mib4 |
re2 do4 |
re2. |
sol,2 sol,4 |
re2 do4 |
sib,2 la,4 |
sol,2 re4 |
mib fa2 |
sib, sol4 |
fa2 mib4 |
re2 do4 |
re2. sol,1\fermata |
sol4 |
fa2 mib4 re2 re'4 |
do'2 sib4 la2 sib8 la |
sol2 la4 re2 sol4 |
fa2 mib4 re2 mib!8 re |
do2 re4 sol,1\fermata |
fad1~ |
fad~ |
fad2 sol |
mi1 re |
re'2 si~ |
si1 |
do' |
do~ |
do2 re~ |
re mib |
do4 re sol,4 r |
sol,4 sib, do re |
sol,4. sol8 la4 re |
sol8 sol, la,4 re do |
sib, la, sol, sib, |
do re sol,2 |
