\score {
  \new StaffGroupNoBar \with { \haraKiriFirst } <<
    \new StaffGroupNoBracket <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
      \new Staff << \global \includeNotes "partie1" >>
      \new Staff << \global \includeNotes "partie2" >>
      \new Staff << \global \includeNotes "basse1" >>
    >>
    \new Staff \withLyricsB <<
      \global \includeNotes "voix"
    >>
    \keepWithTag #'stroffa1 \includeLyrics "paroles"
    \keepWithTag #'stroffa2 \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        %% stroffa
        s2.*5\break s2.*5\break s2.*3 s1\pageBreak
        %% ritornello
        s4 s2.*9 s1\break
        s1*5\break s1*3\pageBreak
        s1*4\break
      }
      \modVersion { s4*43\break s4*32\break s1*12\break }
    >>
  >>
  \layout { }
  \midi { }
}
