\tag #'(stroffa1 basse) {
  Ma in a -- mor ciò ch’al -- tri fu -- ra
  più d’a -- mor gio -- ia non è
  e un’ in -- si -- pi -- da ven -- tu -- ra
  ciò ch’e -- gli in do -- no, o ver pie -- tà non diè.
  In a -- mor ciò ch’al -- tri fu -- ra
  più d’a -- mor gio -- ia non è.
}
\tag #'stroffa2 {
  Se non vien da gra -- ta ar -- su -- ra
  vo -- lon -- ta -- ria all’ al -- trui fé
  can -- gia af -- fat -- to di na -- tu -- ra
  co -- me con -- di -- ta d’o -- dio o -- gni mer -- _ cé.
}
\tag #'(stroffa1 basse) {
  Ma che più con in -- u -- ti -- li la -- men -- ti
  il tem -- po scar -- so al -- la di -- fe -- sa io per -- do?
  Su su por -- ta -- te -- mi por -- ta -- te -- mi o ven -- ti
  al -- la grot -- ta del Son -- no, e d’au -- re in -- fes -- te
  cor -- teg -- gia -- to il mio tron ver -- si per tut -- to
  pom -- pe del mio fu -- ror fiam -- me, e tem -- pes -- te.
}
