\score {
  \new ChoirStaff \with { \haraKiriFirst } <<
    \new Staff \with { \tinyStaff } \withLyrics <<
      \global \includeNotes "voix"
    >> { \keepWithTag #'basse \includeLyrics "paroles" \set fontSize = -2 }
    \new Staff << \global \includeNotes "basse1" >>
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      { s4*43\break s4*32\break s1*12\break }
    >>
  >>
  \layout { }
}
