\clef "dessus" R4*43 |
sol'8 sol' |
la'4 la' sol'8 la' fad'4 fad' re''8 re'' |
mi''4 mi'' re''8 mi'' dod''4 dod'' mi''8 fa'' |
sol''4 re'' dod''8 re'' re''2 sol'8 sol' |
la'4 la' sol'8 sol' fad'4 fad' la'8 sib' |
do''4 sol' fad'8 sol' sol'1\fermata |
R1*12 |
r8 re''16 do'' sib' la' sol' fa' mi'8 fa'16 sol' sol'8. fad'16 |
sol'8 sol'16 la' sib' do'' re'' mi'' dod''8 fa''16 mi'' re'' \ficta do'' sib' la' |
sol'16 sol'' fa'' sol'' mi''8. mi''16 re''8 mi'16 fa' sol' la' sib' do'' |
re''2 r8 re''16 do'' sib' la' sol' fa' |
mi'8 fad'16 sol' sol'8. fad'16 sol'2 |
