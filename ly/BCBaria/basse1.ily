\clef "basse" R4*43 R4*32
R1*12 |
sol,4 sib, do re |
sol8 sib16 la sol la sib sol la4 re |
sol8 sol, la,4 re8 la16 sol fa mi re do |
sib,8 fa16 mi re do sib, la, sol,4 sib, |
do re sol,2 |
