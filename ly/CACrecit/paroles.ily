Pu -- re al -- fi -- ne il ris -- pet -- to
di fi -- glio al ge -- ni -- tor fia ch’in te can -- gi
sì a -- mo -- ro -- so lin -- guag -- gio.

Che più to -- sto il tuo af -- fet -- to
non ren -- da anch’ e -- gli al for -- te Al -- ci -- de o -- mag -- gio.

Ah che for -- zar un co -- re
non puo -- te al -- tri ch’a -- mo -- re.

E di ri -- va -- le il ti -- to -- lo o -- di -- o -- so
qua -- lun -- que al -- tro bel no -- me,
che con -- cor -- ra con lui, ren -- de o -- zi -- o -- so;
u -- na sol vi -- ta il ge -- ni -- tor mi die -- de,
e per te, che mia vi -- ta
mol -- to più ca -- ra se -- i
mil -- le vi -- te mil -- le vi -- te da -- re -- i. __

E per te sol mio ben,
all’ em -- pi -- o~u -- sur -- pa -- tor con -- ten -- ta i’ ce -- do
il re -- gno, il mon -- do tut -- to, e te e te sol chie -- do.
