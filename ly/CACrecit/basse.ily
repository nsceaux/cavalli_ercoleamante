\clef "basse" fa,1 |
fa, |
fa,~ |
fa,2 sib, |
sib,1 |
mib~ |
mib |
re2~ re |
fad sol~ |
sol la |
re la,4. sib,8 |
do2 fa, |
fa1~ |
fa |
re8 mib fa4 sib,2 |
mib1~ |
mib2 re |
re1~ |
re2 sol |
do re4 do |
re2 sol, |
sol1 |
mi |
mi |
fa2 << \original { sib,4 do } \pygmalion { sib,4. do8 } >> |
re4 la, sib,2 |
la,4 sib, do2 fa,1\fermata |
