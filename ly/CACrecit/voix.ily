\ffclef "vsoprano" <>^\markup\character Iole
r4 la'8 do'' la'4 la' |
la'8 la' la' la'16 la' la'8 la' la' sib' |
do''4 do''8 do''16 do'' do''8 do'' do'' do'' |
do''4 do''8 re'' sib'4 sib' |
\ffclef "vtenor" <>^\markup\character Hyllo
r4 sib8 re' sib4 sib8 sib |
sol4 sol8 sol la sib do' do' |
r do' do' do' do'4 do'8 do' |
la4 la
\ffclef "vsoprano" <>^\markup\character Iole
r4 re'' |
r8 la'16 sib' do''8 re'' sib' sib' sib' la' |
sol'4 fa'8 fa' re' re' r4 |
\ffclef "vtenor" <>^\markup\character Hyllo
r8 la la sib |
do' do' r re' |
la8. sol16 sol8 sol16 la fa8 fa r4 |
r la8 la la4 la8 sib |
do' do' do' do' do'4 do'8 sib |
re'4 re'8 do'16 re' sib8 sib r4 |
r8 sol sol sol sol4 sol8 la |
la la la do' la4 la |
fad8 fad fad fad16 sol la4 la |
do'8 do'16 re' mib'8 re' sib sib sol' fa' |
mib' re' do' sib la4 sib8 do' |
sib4. la8( sol2) |
\ffclef "vsoprano" <>^\markup\character Iole
r8 sib' sib' la' sib'4 r8 re'' |
sol'4 sol'8 sol' do'' do'' do'' re'' |
sib'4. sib'8 sib'4 sib'8 la' |
la' la' r do'' re'' re'' r16 do'' mi'' re'' |
fa''8 fa'' r do'' re''4 r |
r8 do'' sib' la' la'4( sol') fa'1\fermata |
