\clef "treble" <>^"Harpe" do''4. sib'8 do''4. sib'8 |
la'4. sib'8 do''4. re''8 |
do''4. sib'8 la'4. do''8 |
si' sol' do''4 do''4. si'8 |
do''4. do''8 do''4. fa'8 |
sol'4 r8 fa' mi' fa'4 mi'8 |
fa'2 fa'4. sol'8 |
la' sib'4 la'8 sib'4. la'8 |
sib'4. do''8 re''4. do''16 sib' |
la'4. la'8 sib' la' sib' do'' |
re''4. do''8 re''4. do''16 sib' |
la'4. la'8 sol'4 fa' |
mi'4. fa'8 mi' re'4 dod'8 |
re'4. re'8 re''4. re''8 |
sib'4. sib'8 do'' sib'4 la'8 |
sib'4. la'8 sol' fa'4 sol'8 |
sol'4. sol'8 sol'4. fad'8 |
sol'4. fa'8 mi' do' do''4 |
do''4. sib'8 la' sib'8 la'4 |
sib'4. re''8 do''4 sib' |
la'4. sol'8 fa'4 fa' |
sib'4. la'8 sol' fa'4 mi'8 |
fa'1 |
