\score {
  <<
    \new PianoStaff <<
      \new Staff << \global \includeNotes "harpe1" >>
      \new Staff << \global \includeNotes "basse" >>
    >>
    \new PianoStaff <<
      \new Staff << \global \includeNotes "harpe2" >>
      \new Staff << \global \includeNotes "basse" >>
    >>
  >>
  \layout { }
  \midi { }
}
