\clef "viole1" fa' fa'4. sol'8 fa' mi' |
fa' do' fa' re' mi' fa' mi' re' |
fa'4 do'8 re'16 mi' fa'4. la'8 |
sol'4 fa'8 mi' re'4. re'8 |
mi' fa' sol' la' sol' la' sol' fa' |
mi' re' do' sol' sol'4. sol8 |
do' re' do' re' do' re' do' re'16 mi' |
fa'4. do'8 fa' sol' fa' mib' |
re' mib' re' fa' re'4 re'8 mi' |
fad'8 sol' re' fad' sol' re' sol' fa' |
fa'4 re'8 fa'4 fa'8 sol'4 |
fa'4. fa'8 sol' mi' la' fa' |
sib' la' sol' sib' la'4 mi'8. mi'16 |
fad'8 sol' la' fad' sol' la' sol' fa' |
sol' fa' mib' fa' mib' fa' mib' do' |
re' mib' fa' re' mib' do' re' sib |
mib' re' do' mib' re'4 mib'8 re' |
si do' re' si do' fa' mi'4 |
fa'8 mi' fa' sol' fa'4 sol'8 fa' |
re'8 mi' fa'4 mi'8 fa' re' mi' |
fa'4. sib8 do'4. la8 |
re'4 sib8 re' mi' fa' re' do' |
do'1 |
