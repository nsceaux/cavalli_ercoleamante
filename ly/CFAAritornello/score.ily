\score {
  <<
    \new StaffGroup <<
      \new Staff << \global \includeNotes "viole0" >>
      \new Staff << \global \includeNotes "viole1" >>
      \new Staff << \global \includeNotes "viole2" >>
    >>
    \new PianoStaff <<
      \new Staff << \global \includeNotes "harpe1" >>
      \new Staff << \global \includeNotes "basse" >>
    >>
    \new PianoStaff <<
      \new Staff << \global \includeNotes "harpe2" >>
      \new Staff <<
        \global \includeNotes "basse"
        \origLayout {
          s1*5\break s1*4\pageBreak
          s1*5\break s1*5\pageBreak
        }
      >>
    >>
  >>
  \layout { }
  \midi { }
}
