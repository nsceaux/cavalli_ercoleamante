\piecePartSpecs
#'((violes #:score-template "score-violes")
   (basse #:score "score-basse")
   (silence #:on-the-fly-markup , #{ \markup { Ritornello: \tacet } #}))
