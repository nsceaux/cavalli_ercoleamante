\clef "viole2" la8 sib do' re' do' re' do' sol |
la sol re' sol sol2 |
do'8 fa4 sib8 do' sib do' fa' |
re' do'4 do'8 sol la sol4 |
sol do' do'4. si8 |
sol8. do'16 la8 re' sol fa la do' |
la fa4 re8 fa8. sol16 la8 sib |
do' sib do'4 re'8 sib4 do'8 |
fa4 sol8 la sib4 sib8 sol |
re'8 do' la re' re'8. sib16 mib'8. re'16 |
re'8 do' sib do' sib do' re' mi' |
do' sib do' la re' do'4 re'8 |
re' dod' re'4 dod'8 la sib! la |
la4. la8 sib re' sol sib |
sol sib4 sib8 sol fa sol fa |
fa4. fa8 sol la sib do' |
la sib sol la sib sol sib la |
sol4. re'8 sol fa sol mi |
la4. do'8 do' re' sol do' |
sib4. la8 sol fa sol4 |
la8 sib do' re' do' sib la do' |
fa2 do'8. do'16 sib8 sol |
la1 |
