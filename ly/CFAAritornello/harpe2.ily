\clef "dessus" <>^"Harpe" r8 sib do' r r re' do' r |
r do' re' r r fa' mi' r |
r re' do' r r sib do' r |
r mi' fa' mi' re'4. re'8 |
mi' fa' sol' la' sol' la' sol' r |
r re' do' r r fa sol4 |
fa8 sib do' r r re' do' r |
r sib do' r r mib' fa' mib' |
re' mib' re' r r do' re' r |
r do' re' r r do' sib la |
r do' sib r r do' re' r |
r sib do' r r do'4 re'8~ |
re' dod' re'4 r8 la sib la |
r mi' fad' r r la' sol' r |
r fa' mib' r r fa' mib' do' |
re' mib' fa' r r la sib r |
la sib sol r r sib la4 |
r8 do' re' r r fa' mi' sol' |
r mi' fa' r r re' sol' fa' |
r mi' fa' r r fa' re' mi' |
fa' sib do' r r sib la do' |
sib re' fa'4 r8 do' re' do' |
la1 |
