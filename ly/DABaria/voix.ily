\clef "vbas-dessus" R1*10 |
<>^\markup\character Venere
sib'4 do'' re'' fa'' |
sib' do'' fa' sib' |
do''4. re''8 sib'2 |
fa'4 sol' la' do'' |
fa' sol' la' sib' |
do'' re'' sol' la' |
sol'4. la'8 fa'4 do''8 sib' |
la'4 fa''8 mib'' re'' do'' sib' la' |
sol' sol' la' do'' fad'4 sol' |
sib' la'8[ sib'] sol'4 sib'8 do'' |
re''4 mib'' re'' si' |
do''8 re'' mib'' fa'' sol''4 do''8[ re''] |
mib''4 re''8[ mib''] do''4 do''8. sol'16 |
la'4 fa''8. do''16 re''4 do''8 sib' |
fa''2 fa''4 la'8 sib' |
sol' la' sib' do'' la'4 sib'8[ do''] |
re''[ do''] re''[ mib''] do''2 sib'1\fermata |
R1*10 |
