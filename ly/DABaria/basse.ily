\clef "basse"
\setMusic #'ritornello {
  sib la sol re |
  mib do re sib, mib! fa sib,2 |
  fa4 mi re la, |
  sib, sol, fa, sol, |
  la, sib, do la, sib, do fa,2 |
  sib4 la sol re |
  mib do re sib, |
  mib fa sib,2 |
}
\keepWithTag #'() \ritornello
sib4 la sol re |
mib do re sib, |
mib fa sib,2 |
fa4 mi re la, |
sib, sol, fa, sol, |
la, sib, do la, |
sib, do fa,2 |
fa8 mib re4 sib8 la sol fa |
mib re do4 re sib, |
do re sol,2 |
sol8 fa mib4 fa sol |
do8 si, do re mib4 fa |
sol sol, do8 re mi do |
fa8 sol la fa sib sib la sol |
fa sol fa mib re mib fa re |
mib fa sol mib fa mib re do |
sib, la, sol, mib, fa,2 sib,1\fermata |
\ritornello
