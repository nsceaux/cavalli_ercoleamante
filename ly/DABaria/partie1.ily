\clef "viole1"
\setMusic #'ritornello {
  sib' fa' sib' sib' |
  sib' do'' la' sib' sol' fa' fa'2 |
  fa'4 sol' la' do'' |
  sib' sib' fa' sib' |
  la' fa' do'' do'' sib' sol' fa'2 |
  fa'4 fa' sib' sib' |
  sib' do'' la' sib' |
  sol' fa' fa'2 |
}
\keepWithTag #'() \ritornello
R1*18 |
\ritornello
