Sol s’in -- ar -- can gli e -- mis -- fe -- ri
per stu -- por
che tro -- var l’in -- fer -- no io spe -- ri
più cor -- te -- se og -- gi, ch’A -- mor,
ma per me ma per me fin dal -- la cu -- na
fu ge -- lo -- so ei del suo im -- per,
e vi sof -- fre di for -- tu -- na
il ti -- ran -- ni -- co vo -- ler,
che ti -- mor che ti -- mor non gli ar -- re -- ca,
com -- pa -- gni -- a nel re -- gnar pur che sia cie -- ca.
