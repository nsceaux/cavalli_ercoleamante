\actn "Prologo"
\sceneDescription\markup\justify {
  [La scena rappresenta ne’ lati montagne di scogli su li quali si
  vedono giacenti 14 fiumi, che bagnano i regni e le provincie che
  sono o furono sotto la dominazione della corona di Francia. Nella
  prospettiva si vede il mare, e nell’aria Cinzia che discende in una
  gran macchina rappresentante il di lei cielo.]
}
%% 0-1
\pieceToc "Sinf[onia]"
\includeScore "AAAsinfonia"
%% 0-2
\pieceToc\markup\wordwrap {
  Coro di Fiumi, Tevare: \italic { Qual concorso indovino }
}
\includeScore "AABcoro"
\includeScore "AACrecit"
\includeScore "AADcoro"
%% 0-3
\pieceToc "Sinf[onia]"
\includeScore "AAEsinfonia"
%% 0-4
\pieceToc\markup\wordwrap {
  Cinzia: \italic { Ed ecco o Gallia invitta }
}
\includeScore "AAFrecit"
\newBookPart#'(full-rehearsal)
%% 0-5
\pieceToc\markup\wordwrap {
  Coro di Fiumi: \italic { Dopo belliche noie }
}
\includeScore "AAGcoro"
\newBookPart#'(full-rehearsal)
%% 0-6
\pieceToc\markup\wordwrap {
  Cinzia, coro di Fiumi: \italic { Ma voi che più tardate inclite Dee? }
}
\includeScore "AAHrecit"
\includeScore "AAIcoro"
\newBookPart#'(full-rehearsal)
%% 0-7
\pieceToc\markup\wordwrap {
  Coro di Fiumi: \italic { Oh Gallia fortunata }
}
\includeScore "AAJcoro"
\actEnd Fine del Prologo
