\clef "viole1" mi'1 |
mi'2. la'4 |
fa'1 |
fa' |
mi' |
mi' mi'2 fa' |
sol'2. re'4 re'2 dod' |
re'1 red' |
red'2. mi'4 red'!1 |
red'4 fad'2 fad'4 sol'1 |
mi' |
mi'1 |
mi'2 do' re'1 |
r4 mi' sol'2 fad'1 |
sol'2. re'4 mi'1 |
fad'2. la'4 |
red'1 |
mi' |
la'2 fad' |
sold'1\fermata |
