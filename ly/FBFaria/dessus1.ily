\clef "dessus" dod''1 |
dod''2. dod''4 |
re''1 |
re'' |
mi'' |
mi''2 dod''4 re'' mi''2 re'' |
mib''2. sib'4 sib'2 la' |
la'1 \ficta si'! |
si'2. la'4 si'1 |
si'4 red''2 red''4 mi''1 |
si' |
do'' |
do'' si' |
r4 si' dod''2 re''1 |
re''2. re''4 do''1 |
do''2. do''4 |
si'1 |
si' |
do''4 mi''2 red''4 |
mi''1\fermata |
