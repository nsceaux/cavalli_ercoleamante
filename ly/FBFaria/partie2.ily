\clef "viole2" la1 |
la2. la4 |
re1 |
re |
si |
mi2 la la la |
sol2. sol4 re2 la |
la1. fad2 |
si2. mi4 si1 |
si2. si4 si1 |
mi |
sol |
la sol |
r4 sol sol2 la1 |
sol2. sol4 sol1 |
do'2. fad4 |
si1 |
mi |
mi4 do' fad2 |
mi1\fermata |
