\clef "vbasse" <>^\markup\character Ercole
r2 mi4 r |
r mi mi mi8 fa |
fa4 fa r fa |
fa8 fa fa mi fa2 |
r mi4 mi8 mi |
dod4 dod r2 sol4 sol8 la fa4 fa |
r4 mib mib re re2 r |
r fad r4 si2 fad!8 fad |
fad2 fad4 mi fad!2 fad |
si4. la8 la4 la8 si sol2 sol |
r4 si sol sol |
mi2 mi4 mi |
mi mi mi mi8 re re2 re4 re |
mi4 mi r8 mi mi fad fad4 fad r2 |
r4 sol sol sol mi mi r2 |
r4 la la la8 la |
fad4 fad r2 |
si4. mi8 mi2 |
do'4 do'8 si si4 si |
R1 |
