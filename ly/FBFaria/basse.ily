\clef "basse" <<
  \tag #'basse {
    la,1 |
    la, |
    sol, |
    sol, |
  }
  \tag #'basse-continue {
    la,1~ |
    la, |
    sol,~ |
    sol, |
  }
>>
sold, |
la,2. si,4 dod2 re |
<<
  \tag #'basse {
    sol,1 sol,2 la, |
    re1 si, |
    si,2 do si,1 |
    si,2. si,4
  }
  \tag #'basse-continue {
    sol,1. la,2 |
    re,1 si,~ |
    si,2 do si,1 |
    si,  
  }
>> mi1 |
mi |
do |
do <<
  \tag #'basse {
    sol,1 |
    r4 sol, sol,2 re1 |
    si,2. si,4 do1 |
    la,2. la,4 |
  }
  \tag #'basse-continue {
    sol,1~ |
    sol, re |
    si, do |
    la, |
  }
>>
si, |
sol, |
la,2 si, |
mi,1\fermata |
