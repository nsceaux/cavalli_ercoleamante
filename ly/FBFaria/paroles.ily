Ma l’a -- tro -- ce mia do -- glia
im -- per -- ver -- san -- do ogn’ or po -- chi res -- pi -- ri
po -- chi res -- pi -- ri
mi las -- cia più, deh deh s’il mo -- ri -- re è for -- za,
ar -- da -- si la mia spo -- glia
né del -- la ter -- ra, i di cui fi -- gli uc -- ci -- si
s’es -- pon -- ga ad un ri -- fiu -- to:
a di -- o, cie -- lo, a di -- o I -- o -- le, ec -- co -- mi ec -- co -- mi Plu -- to.
