\clef "dessus" la'1 |
la'2. mi'4 |
sib'1 |
sib' |
si'! |
la' la'2 la' |
sib'2. sol'4 fa'2 mi' |
fad'1 fad' |
fad'2. do''4 fad'!1 |
fad'4 si'2 si'4 si'1 |
sol' |
sol' |
sol'2 mi'4 fad' sol'1 |
r4 sol' mi'2 la'1 |
si'2. si'4 sol'1 |
la'2. fad'4 |
fad'1 |
sol' |
mi'2 si' |
si'1\fermata |
