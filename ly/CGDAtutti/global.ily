\key fa \major \midiTempo#160
\beginMark "A 5" \time 4/4 s1*5
\digitTime\time 3/4 \measure 6/4 s1.
\measure 3/4 s2.
\measure 6/4 s1.*6
\measure 3/4 s2.*2
\measure 9/4 s2.*3
\measure 6/4 s1.
\measure 9/4 s2.*3 \bar "|."
