\score {
  \new ChoirStaff <<
    \new Staff \with {
      instrumentName = \markup\character Giunone
      shortInstrumentName = \markup\character Giu.
    } \withLyrics <<
      \global \keepWithTag #'giunone \includeNotes "voix"
    >> \keepWithTag #'giunone \includeLyrics "paroles"
    \new Staff \with {
      instrumentName = \markup\character Pasithea
      shortInstrumentName = \markup\character Pas.
    } \withLyrics <<
      \global \keepWithTag #'pasithea \includeNotes "voix"
    >> \keepWithTag #'pasithea \includeLyrics "paroles"
    \new Staff \with {
      instrumentName = \markup { \smallCaps Aura \concat { P \super a } }
      shortInstrumentName = \markup\character Au 1
    } \withLyrics <<
      \global \keepWithTag #'aura1 \includeNotes "voix"
    >> \keepWithTag #'aura1 \includeLyrics "paroles"
    \new Staff \with {
      instrumentName = \markup\character Aura 2
      shortInstrumentName = \markup\character Au 2
    } \withLyrics <<
      \global \keepWithTag #'aura2 \includeNotes "voix"
    >> \keepWithTag #'aura2 \includeLyrics "paroles"
    \new Staff \with {
      instrumentName = \markup\character Ruscello
      shortInstrumentName = \markup\character Rus.
    } \withLyrics <<
      \global \keepWithTag #'ruscello \includeNotes "voix"
    >> \keepWithTag #'ruscello \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout { s1*5 s2.*3\pageBreak s2.*13\pageBreak }
      \modVersion { s1*5\break }
    >>
  >>
  \layout {
    indent = \largeindent
    short-indent = 10\mm
  }
  \midi { }
}
