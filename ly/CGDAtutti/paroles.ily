Le ru -- gia -- de più pre -- zio -- se
tuoi pa -- pa -- ve -- ri ogn’ or ba -- gni -- no,
\tag #'(giunone basse pasithea aura1) {
  e per tut -- to gi -- gli, e ro -- se
  co’ lor a -- li -- ti t’ac -- com -- pa -- gni -- no,
}
\tag #'(giunone basse pasithea aura2 ruscello) {
  e per tut -- to gi -- gli, e ro -- se
}
co’ lor a -- li -- ti
co’ lor a -- li -- ti
\tag #'(pasithea aura1 aura2) {
  co’ lor a -- li -- ti
}
\tag #'(aura1) {
  t’ac -- com -- pa -- gni -- no
}
t’ac -- com -- pa -- gni -- no.
