\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \keepWithTag #'aura1 \includeNotes "voix" \clef "alto"
    >> \keepWithTag #'aura1 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'aura2 \includeNotes "voix" \clef "alto"
    >> \keepWithTag #'aura2 \includeLyrics "paroles"
  >>
  \layout { }
}
