\clef "basse" r4 fa do' la |
sib sol la2 |
la4 fa sol4. mi8 |
fa2 sol |
do1 |
r2*3/2 \clef "alto" r4 do' do' |
fa'2 fa'4 |
re'2 re'4 sol'2 sol'4 |
mib'2 mib'4 fa'2 mib'4 |
re'2 do'4 sib re' mib' |
\clef "bass" fa2. sib |
sol do' |
la, re |
sib, |
mib2 sib,4 |
do2. la, sib,2 sib,4 |
do2 sib,4 la,2 sol,4 |
fa, la, sib, do2. fa,1*3/4\fermata |
