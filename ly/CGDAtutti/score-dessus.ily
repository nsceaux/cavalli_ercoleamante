\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \keepWithTag #'giunone \includeNotes "voix"
    >> \keepWithTag #'giunone \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'pasithea \includeNotes "voix"
    >> \keepWithTag #'pasithea \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'aura1 \includeNotes "voix" \clef "treble"
    >> \keepWithTag #'aura1 \includeLyrics "paroles"
  >>
  \layout { }
}
