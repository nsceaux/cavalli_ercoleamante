Sal -- va, sal -- va, Net -- tu -- no, ah sal -- va
quel trop -- po ar -- di -- to gio -- va -- ne, e sov -- vien -- ti,
che t’ac -- quis -- tò non fa -- vo -- re -- vol gri -- do
il ne -- ga -- to soc -- cor -- so
all’ a -- mo -- ro -- so nuo -- ta -- tor d’A -- bi -- do.
Sal -- va -- lo, sal -- va -- lo, o dio tri -- for -- me,
che d’Er -- co -- le co -- mun nos -- tro ne -- mi -- co
all’ al -- ma in -- vi -- pe -- ri -- ta
far non si può da noi più grand’ ol -- trag -- gio
che di sal -- va -- re il di lui fi -- glio in vi -- ta; __
\original {
  poi che l’in -- i -- quo pa -- dre,
  che qual ri -- val ge -- lo -- so
  la mor -- te sol di quel mes -- chi -- no a -- go -- gna,
  ve -- den -- do -- lo da noi ri -- dot -- to il -- le -- so,
  dop -- pia ne ri -- trar -- rà sma -- nia, e ver -- go -- gna.
}
Ah __ tu non m’o -- di? o vi ri -- pu -- gni a -- dun -- que?
In quest’ on -- de ver me già sì cor -- te -- si
quell’ an -- ti -- ca bon -- tà del tut -- to è spen -- ta?

Ec -- co -- ti, o dèa con -- ten -- ta;
che nul -- la al tuo vo -- ler ne -- gar poss’ i -- o;
né fu mia ne -- gli -- gen -- za
ma ben sua re -- ni -- ten -- za il tar -- dar mi -- o;
ne cre -- do, ne cre -- do un -- qua più av -- ven -- ne,
che dall’ or -- ri -- bil go -- la
dal -- la vo -- ra -- ce, e non mai sa -- zia di -- te
fos -- ser ri -- tor -- ti a for -- za
con -- tro lor vo -- glia i mi -- se -- ri mor -- ta -- li
com’ hor suc -- ce -- de in ques -- to, ò, ò for -- sen -- na -- to,
e chi ren -- de al tuo gus -- to
di sì a -- ma -- bil sa -- por l’es -- tre -- mo fa -- to?

D’un a -- mor dis -- pe -- ra -- to
al -- la tan -- ta -- lea se -- te
il net -- ta -- re più gra -- to
è sol l’on -- da di Le -- te. __

Oh sem -- pli -- cet -- to sem -- pli -- cet -- to as -- col -- ta,
ciò, che per suo di -- let -- to,
can -- tò Glau -- co tal -- vol -- ta.
