\clef "vbas-dessus" <>^\markup\character Giunone
re''8 sib' re'' re''16 fa'' re''8 re'' fa''4 |
sib'4 sib'8 do'' do'' do'' do'' do'' |
la'8. la'16 la'4 la'8 fa' do''4 |
do''8 do'' do'' do'' do''4. fa''8 |
fa''8 mib'' mib'' re'' mib''! mib'' mib'' mib'' |
\ficta mib''4 mib''8 re'' re''4 re''8 re'' |
re'' re'' re'' re'' re'' do'' do'' do''16[ re''32 \ficta mib''] |
do''2 sib' |
do''8. do''16 do''4 fa''8 fa''16 fa'' fa''8 sol'' |
mib''4 mib''8 mib'' mib''8. mib''16 mib''8 mib'' |
mib''4 mib''8 mib''16 re'' mib''!4 mib''8 mib'' |
do''8 do''16 re'' re''8 re'' si'8 si' sol'4 |
si'8 si' si' do'' re'' re'' re'' mib'' |
do''8 do''16 do'' do''8 do'' do'' do'' do'' do'' |
fa''4 fa''8 mib'' do''4. si'8( |
do''4) <<
  \pygmalion r4
  \original {
    r8 re'' re'' re'' re'' fa'' |
    sib'4 sib'8 sib' sib' sib' sib' sib' |
    sib' sib'16 sib' sib'8 do'' re'' re'' re'' re'' |
    sib'4 sib'8 la' do''4 do''8 do'' |
    do''8. do''16 do''8 do'' la' la' la' sol' |
    sol'4 sol' do''8 do''16 do'' do''8 re'' |
    re''4 re''8 mi''16 fa'' sol'8. sol'16( fa'4) |
  }
>>
r8 do''( mib''2) do''8 si' |
re'' re'' r si' re'' re'' re'' do'' |
mib'' mib'' r4 r2 |
r4 fa''8 fa'' do''4 do''8 do'' |
do'' do'' do'' sib' do''4 do'' |
do''8 do'' do'' do''16 re'' mib''8 fa'' do'' sib' |
re'' re'' r4
\ffclef "vbasse" <>^\markup\character Nettuno
sib8. sib16 sib8 la |
sol4 re8[ mib] fa2 |
sib,4 r8 sib la sib sol sib |
fa4. la8 sib4. la8 |
do'2 fa4 r8 fa |
mib fa re mib do4 do8 do' |
sib do' lab sib sol4 sol8 sol |
lab[ sol] lab[ fa] sol2 |
do4 r8 do fa4 fa8 fa |
fa re mib fa sib,4 sib,8 sib, |
sib, do re sib, mib4 mib8 fa |
mib[ fa] sol[ mib] lab8 lab lab lab |
sol4. lab8 sib4 sib |
si4 si8 si do'[ si!] do'[ sol] |
lab4 lab fa mi8 fa |
sol4 sol8 sol re mib fa re |
mib4 mib8 mib do4. do8 |
re4 mib fa2 |
sib,1 |
\original {
  r4 sib2 |
  r4 do' lab8 sol |
  sol2 sol4 |
  sol8 fa mib4 fa8 sol |
  do4 do do8 re |
  mib4 re mib |
  si,4. re8 mib4 |
  sol4 re re |
  \ffclef "vtenor" <>^\markup\character Hyllo
  r4 la8 la la4 la8 la |
  re'4 re'8 la la la do' re' |
  sib4 sib8 sib sib8. sib16 sib8 sib |
  mib'2 mib'4 do'8 do' |
  fad4 fad8 sib sol4. fad8( sol1)\fermata |
  \ffclef "vbasse" <>^\markup\character Nettuno
  r4 sol2 fa8 mi |
  fa fa fa sol mi4 mi |
  r do sol sol |
  sol r sol8 sol sol fa |
  sol4 sol8 sol sol4 sol8 sol16 la |
  fa4 fa r2 |
}
