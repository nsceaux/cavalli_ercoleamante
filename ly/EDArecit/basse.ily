\clef "basse" sib,1~ |
sib,2 mi, |
fa,1 |
fa,~ |
fa,2 do~ |
do re~ |
re mib |
fa sib, |
fa,1 |
do |
do~ |
do2 sol~ |
sol1 |
mib |
lab4 fa sol2 |
do \original {
  sib, |
  sib,1 |
  sib, |
  sol,2 fa,~ |
  fa,1 |
  do2 la, |
  sib, do4 fa, |
}
do1 |
sol |
do' |
la |
la |
la |
sib2 sib4. la8 |
sol4 re fa2 |
sib,4. sib8 la4 sol |
fa la sib la |
do'2 fa |
mib4 re do4. do'8 |
sib4 lab sol2 |
lab4 fa sol2 |
do4. do8 fa2 |
fa8 re mib fa sib,2~ |
sib,8 do re sib, mib2 |
mib8 fa sol mib lab2 |
sol4. lab8 sib2 |
si do'4 sol |
lab2 fa |
sol re |
mib do4. do8 |
re4 mib fa2 |
sib,1 |
\original {
  sib2. |
  do'4 fa2 |
  sol2. |
  sol, |
  do2 do8 re |
  mib4 re do |
  si,2 do4~ |
  do re2~ |
  re1~ |
  re2 fad |
  sol1 |
  do~ |
  do2 re sol,1\fermata |
  sol,~ |
  sol,2 do |
  do1 |
  do |
  do |
  fa |
}
