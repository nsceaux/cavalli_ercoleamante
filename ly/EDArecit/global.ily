\key fa \major
\time 4/4 \midiTempo#100
s1*15 <<
  \pygmalion { \measure 2/4 s2 \measure 4/4 s1*25 }
  \original { s1*32 }
>>
\original {
  \digitTime\time 3/4 \midiTempo#160 s2.*8
  \time 4/4 \midiTempo#100 s1*4 \measure 2/1 s1*2 \bar "|."
  \measure 4/4 s1*6
}
\bar "|."
