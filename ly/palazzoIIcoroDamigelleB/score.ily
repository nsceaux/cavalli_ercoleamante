\score {
  <<
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix1 \includeNotes "voix"
      >> \keepWithTag #'voix1 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix2 \includeNotes "voix"
      >> \keepWithTag #'voix2 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix3 \includeNotes "voix"
      >> \keepWithTag #'voix3 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix4 \includeNotes "voix"
      >> \keepWithTag #'voix4 \includeLyrics "paroles"
    >>
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix5 \includeNotes "voix"
      >> \keepWithTag #'voix5 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix6 \includeNotes "voix"
      >> \keepWithTag #'voix6 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix7 \includeNotes "voix"
      >> \keepWithTag #'voix7 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix8 \includeNotes "voix"
      >> \keepWithTag #'voix8 \includeLyrics "paroles"
    >>
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*2\pageBreak
        s1*4\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
