Via
\tag #'(voix5 voix6 voix7 voix8) {
  di qua va -- da o -- gni cu -- ra,
}
che le gio -- ie in -- tor -- bi -- dò;
\tag #'(voix1 voix2 voix3 voix4) {
  con la bel -- va, o -- gni pau -- ra
  pur al fin si di -- le -- guò.
}
\tag #'(voix5 voix6 voix7 voix8) {
  pur al fin si di -- le -- guò
  con la bel -- va, o -- gni pau -- ra
  pur al fin si di -- le -- guò.
}
pur al fin si di -- le -- guò
pur al fin si di -- le -- guò.
