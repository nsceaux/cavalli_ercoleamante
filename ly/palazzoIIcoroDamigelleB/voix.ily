<<
  \tag #'voix1 {
    \clef "vsoprano" do'' r r2 |
    r4 fa''8 do'' mi'' do'' re'' re'' |
    do''4 mi''8 fa'' sol'' sol'' fa'' fa'' |
    mi'' fa'' mi'' mi'' re'' re'' re''8. re''16 |
    do''4 r r2 |
    R1 |
    r4 la'8 sib' do'' sib' la' sib' |
    sol'4 r r2 |
    r4 fa''8 do'' re'' fa'' fa'' mi'' |
    fa''4\fermata
  }
  \tag #'voix2 {
    \clef "vsoprano" la'4 r r2 |
    r4 do''8 la' do'' mi'' sol''8. sol''16 |
    mi''4 do''8 re'' mi'' mi'' fa'' do'' |
    do'' do'' do'' do'' do'' do'' \ficta si' si' |
    do''4 r r2 |
    R1 |
    r4 re''8 re'' do'' mib'' re'' re'' |
    sib'4 r r2 |
    r4 do''8 la' sib' do'' do'' do'' |
    do''4\fermata
  }
  \tag #'voix3 {
    \clef "vsoprano" fa'4 r r2 |
    r4 la'8 do'' do'' sol' sol' sol' |
    sol'4 sol'8 sol' do'' do'' la' la' |
    sol' fa' sol' sol' la' la' sol' sol' |
    sol'4 r r2 |
    R1 |
    r4 fa'8 fa' sol' sol' sol' fad' |
    sol'4 r r2 |
    r4 la'8 fa' fa' la' do'' sol' |
    la'4\fermata
  }
  \tag #'voix4 {
    \clef "valto" fa'4 r r2 |
    r4 fa8 fa do' do' sol sol |
    do'4 do'8 do' do' do' fa' fa' |
    do' re' mi' mi' fa' do' sol' sol' |
    do'4 r r2 |
    R1 |
    r4 re'8 re' mib' do' re' re' |
    sol4 r r2 |
    r4 fa'8 fa' mi' re' do' mi' |
    fa'4\fermata
  }
  
  \tag #'voix5 {
    \clef "vsoprano" r4 fa''8. do''16 re''8 re'' do'' sib' |
    la' la' la' fa' sol' do'' do'' si' |
    do''4 r r2 |
    r4 do''8 do'' la' la' si' si' |
    do''4 sib'8 do'' re'' re'' re'' dod'' |
    re'' do'' do'' do'' sib' la' la' sib' |
    sol'4 r r2 |
    r4 la'8 sib' do'' sib' la' la' |
    sol'4 do''8 do'' sib' la' sol' do'' |
    do''4\fermata
  }
  \tag #'voix6 {
    \clef "vsoprano" r4 do''8. la'16 sib'8 sib' la' sol' |
    fa' fa' do'' do'' do'' mi'' re'' re'' |
    mi''4 r r2 |
    r4 sol'8 sol' fa' la' re'' sol' |
    sol'4 sol'8 sol' sol' sol'' fa'' mi'' |
    fa'' mi'' mi'' re'' re'' mi'' re'' re'' |
    re''4 r r2 |
    r4 re''8 re'' do'' mi'' re'' re'' |
    sib'4 la'8 la' fa'8. do''16 do''8 sol' |
    la'4\fermata
  }
  \tag #'voix7 {
    \clef "vsoprano" r4 la'8. fa'16 fa'8 fa' fa' mi' |
    fa' fa' do'' do'' do'' sol' re'' re'' |
    sol'4 r r2 |
    r4 mi'8 do' do'' fa' re' re' |
    mi'4 re'8 mi' fa' fa' mi' mi' |
    la' sol' la' la' sol' sol' sol' fad' |
    sol'4 r r2 |
    r4 fa'8 fa' sol' sol' sol' fad' |
    sol'4 fa'8 fa' fa' fa' sol' sol' |
    fa'4\fermata
  }
  \tag #'voix8 {
    \clef "valto" r4 fa'8. fa'16 sib8 sib do' do' |
    fa fa fa' fa' mi' do' sol' sol' |
    do'4 r r2 |
    r4 do'8 do' fa fa sol sol |
    do'4 sol8 la sib sib la la |
    re' mi' fad' fad' sol' do' re' re' |
    sol4 r r2 |
    r4 re'8 re' mib' do' re' re' |
    sol4 la8 la sib sol do' do' |
    fa4\fermata
  }
>>