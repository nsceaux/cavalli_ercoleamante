\score {
  <<
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix1 \includeNotes "voix"
      >> \keepWithTag #'voix1 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix2 \includeNotes "voix"
      >> \keepWithTag #'voix2 \includeLyrics "paroles"
    >>
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix5 \includeNotes "voix"
      >> \keepWithTag #'voix5 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix6 \includeNotes "voix"
      >> \keepWithTag #'voix6 \includeLyrics "paroles"
    >>
  >>
  \layout { }
}
