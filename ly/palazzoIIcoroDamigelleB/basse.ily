\clef "basse" fa2 sib,4 do |
fa, fa do sol, |
do2~ do4 fa, |
do2 fa,4 sol, |
do sol, sib, la, |
re2 sol,8 do re4 |
sol,2 do4 re |
sol,2 do4 re |
sol,4 la, sib,8 fa, do4 |
fa,4\fermata
