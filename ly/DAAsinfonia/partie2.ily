\clef "viole2" fa'1 mib'2 fa'4 re' |
sol'2 r4 re' fa'2 fa' |
fa'2. re'4 sol' do'2 fa'4 |
sol'1 fa'\fermata |
r2 do' sib do'4 la re'1 |
r2 re' mib' fa'4 re' |
sol'2 r4 sol' mib'2 re' |
re'2. sol'4 do'2 sib do'1 sib\fermata |
