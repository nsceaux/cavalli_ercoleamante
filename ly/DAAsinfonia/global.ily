\key fa \major \midiTempo#240
\time 4/4 \measure 2/1 s\breve*4
\measure 3/1 s1*3
\measure 2/1 s\breve*2
\measure 4/1 s\breve*2 \bar "|."
