\clef "dessus" fa''1 sib''2 fa'' |
sol'' r4 sol'' fa''2 fa'' |
fa''4 mi''8 re'' do''4 re'' mi''2 fa''~ |
fa''4 mi''8 re'' mi''2 fa''1\fermata |
do''2 fa''1 do''2 re''1 |
fa''2 sib''1 fa''2 |
sol'' r4 sol'' sib''2 re'' |
fa''2. mib''8 re'' do''2 re'' do''1 re''\fermata |
