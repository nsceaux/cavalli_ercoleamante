\clef "dessus" re''1 sol''2 re'' |
mib'' r4 re'' re''2 do'' |
do'' do'''4 sib''8 la'' sol''2 la'' |
sol''1 la''\fermata |
r2 la' re'' la' sib'1 |
r2 re'' sol'' re'' |
mib'' r4 mib'' sol''2 fa'' |
sib''4 la''8 sol'' fa''4 sol'' la''2 sib''2. la''8 sol'' la''2 sib''1\fermata |
