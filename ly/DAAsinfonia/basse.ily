\clef "basse" sib1 sol2 sib |
mib r4 sol re2 fa |
la,2. sib,4 do2 fa, |
do1 fa,\fermata |
r2 fa re fa sib,1 |
r2 sib sol sib |
mib r4 do' sol2 sib |
re2. mib4 fa2 sib, fa1 sib,\fermata |
