\clef "viole1" sib'1 sib'2 sib' |
sib' r4 sib' sib'2 la' |
la'2. sol'8 fa' do''2 do'' |
do''1 do''\fermata |
r2 fa' fa' fa' fa'1 |
r2 sib' sib' sib' |
sib' r4 do'' sib'2 sib' |
sib'2. sib'4 fa'2 fa' fa'1 fa'\fermata |
