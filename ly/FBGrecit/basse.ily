\clef "basse" do1~ |
do |
do |
fa |
mi4. fa8 sol4 fa |
sol2 do |
la,1~ |
la, |
sold, |
la, |
re,2 mi, |
la,2. sol,4 |
fa, fa mi re |
do4. si,8 do4 si, |
la, re sol, mi, |
fa, sol, la, si, |
do la, sol, mi, |
fa, mi, fa, mi, |
fa, mi,8 re, do,4 mi, |
fa, sol, la, sol,8 fa, |
mi,2 fa,4 sol, do1\fermata |
\original {
  do1~ |
  do |
  do |
  do |
  do fa, |
  fa2 mi |
  re do |
  sib, sol,~ |
  sol, la, |
  re,1 |
  fad, |
  fad,~ |
  fad, |
  sol, |
  mi, |
  fa, mi,~ mi, |
}
mi |
la |
fa |
sib, |
la,2. sib,4 |
<<
  \original {
    do1 fa, |
    do1~ |
    do2 dod |
    re2. do4 |
  }
  \pygmalion {
    do2 fa, |
    do1 |
    sol,2 dod |
    re do |
  }
>>
sib,2. la,4 |
sol,2 fa, |
mi,2 re,4 sol, |
<<
  \original { la,1 re | }
  \pygmalion { la,2 re | }
>>
do1 |
do |
do |
fa,~ |
fa, |
do |
