Che di -- te, che di -- te? Il mio non fu ri -- me -- dio tar -- do,
ma un po -- co più (ch’io non cre -- dea) ga -- gliar -- do.
Pur cias -- cu -- na di voi di già ri -- mi -- ra
il pe -- no -- so des -- tin per sé fi -- ni -- to
d’un a -- man -- te im -- por -- tun, d’un reo ma -- ri -- to.
E non pian -- ge -- te già,
che co -- mun -- que ch’av -- ven -- ga a un sag -- gio co -- re
dar non si può qui giù sor -- te mi -- glio -- re,
che di vi -- ve -- re in pa -- ce, e li -- ber -- tà, __ e li -- ber -- tà.

\original {
  Qual tra pe -- ri -- gli es -- tre -- mi
  di stre -- pi -- to -- se, et hor -- ri -- de ru -- vi -- ne
  un ch’è sal -- va -- to a sor -- te
  stu -- pi -- do res -- ta, sì ri -- ma -- si an -- ch’i -- o
  sen -- za mo -- to, né vo -- ce; ah per -- ché per -- ché dun -- que
  Hyl -- lo il mio ca -- ro ben, per -- ché per -- ché mo -- ri -- o?
}

Ah Nes -- so mi tra -- dì, deh ti per -- do -- ni
o Lic -- co il ciel l’in -- vo -- lon -- ta -- rio er -- ro -- re;
a do -- lor su do -- lo -- re
e -- gual -- men -- te in -- fi -- ni -- to
più re -- sis -- ter non so, mos -- tra -- mi mos -- tra -- mi oh mor -- te
e del fi -- glio la trac -- cia, e del con -- sor -- te.
Ma che? l’om -- bra del fi -- glio
ec -- co ch’ad in -- con -- trar -- mi
ver me rie -- de pie -- to -- sa.
