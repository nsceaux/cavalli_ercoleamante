\key do \major
\time 4/4 \midiTempo#100 s1*20 \measure 2/1 s1*2 \bar "|."
\original {
  \measure 4/4 s1*4 \measure 2/1 s1*2 \measure 4/4 s1*10 \measure 3/1 s1*3 \bar "|."
}
\measure 4/4 s1*5
<<
  \original { \measure 2/1 s1*2 }
  \pygmalion s1
>>
\measure 4/4 s1*6
<<
  \original { \measure 2/1 s1*2 }
  \pygmalion s1
>>
\measure 4/4 s1*6 \bar "|."
