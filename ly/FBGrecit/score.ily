\score {
  \new StaffGroupNoBar <<
    \pygmalion\new StaffGroupNoBracket \with { \haraKiriFirst } <<
      \new Staff << \global \includeNotes "viole0" >>
      \new Staff << \global \includeNotes "viole1" >>
      \new Staff << \global \includeNotes "viole2" >>
    >>
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*5\break s1*4\break s1*4\break s1*5\break s1*4\pageBreak
        s1*4\break s1*6\break s1*9\break s1*5\break s1*5\pageBreak
        s1*6\break
      }
      \pygmalion\modVersion {
        s1*11\break s1*11\break %s1*19\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
