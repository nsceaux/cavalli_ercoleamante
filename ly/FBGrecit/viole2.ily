\clef "viole2" R1*11 |
r2 r8 mi si sol |
do' la re'4 sol8 si re' re |
sol4 r8 sol do' la re' sol |
do'4 la r8 re' sol do' |
fa4 r8 sol' mi' do' sol sol' |
mi' sol' do' re' si4 r8 do' |
la4 r8 do' la4 r8 do' |
la4 r8 si mi4 r8 sol |
do' la do' si mi' mi si la |
do' sib la4 r8 sol sol4 sol1\fermata |
\original {
  sol1~ |
  sol |
  mi |
  mi2. sol4 |
  sol2. sol4 la2. si4 |
  do' re' mi'2 |
  fa' r4 do |
  re1~ |
  re4 sol mi la |
  fad1 |
  re |
  re~ |
  re |
  re2. re'4 |
  sol2. mi4 |
  do la, la2 mi4 mi' si la si1 |
}
R1*19 |
