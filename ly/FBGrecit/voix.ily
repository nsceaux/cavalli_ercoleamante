\clef "valto" <>^\markup\character Licco
r4 r8 sol' mi'4 mi'8 sol' |
mi'4 mi' r2 |
r8 mi' sol' sol' sol' sol' sol' la' |
fa'4 fa' r8 fa' do' re' |
mi' mi' mi' re' re'4. fa'8 |
re'2 do' |
<<
  \original {
    r4 do'8 do' do'4 do'8 do' |
    do' do' do' re' mi'4 mi' |
  }
  \pygmalion {
    r4 mi'8 mi' mi'4 mi'8 mi' |
    mi' mi' mi' re' mi'4 mi' |
  }
>>
mi'8 mi' mi' mi'16 mi' si8 si re' mi' |
do'4 do' do'8 do' do' re'16 mi' |
fa'8 fa' fa' mi' mi'4 mi' |
r4 r8 la mi' mi' mi' mi' |
la4 la'8 la' sol'4 fa'8 sol' |
mi'4 mi'8 re' mi'[ fa'] re'[ mi'] |
do'[ si do' re'] si4 sol' |
fa'8 fa' mi' re' do'4 re' |
mi'8[ re'] mi'[ fad'] sol'4 sol' |
do'4 do' do' do'8 do' |
do'2 do'4 do'8[ re'] |
mi'[ fa'] re'[ mi'] do'8.[\melisma re'16 mi'8. fa'16] |
sol'4\melismaEnd <<
  \original { la'8[ sol'] fa'[ mi'] re'[ mi'] do'1\fermata | }
  \pygmalion { la'16[ sol' fa' mi'] la'8[ do''] do''8. si'16( do''1)  }
>>
\original {
  \ffclef "vsoprano" <>^\markup\character Iole
  sol'2 sol'8 sol' sol' fa' |
  sol'4 sol' r8 sol' sol' sol' |
  sol'4 sol'8 sol' sol'8. sol'16 sol'8 sol' |
  mi'4 mi' mi'8 mi'16 mi' mi'8 fa' |
  sol'4 sol' do'' do''8 do'' la'4 la' r2 |
  la'4. sol'8 sol'4 sol'8 la' |
  fa'4 fa' r mi'8 fa' |
  re'4 re' r r8 fa' |
  mi'4 mi' r2 |
  r4 la'( re''2) |
  r4 re'' la'2 |
  r4 re''8 la' do''4 do'' |
  do'' do'' do''8 do'' do'' do'' |
  si'4 re'' sol'2 |
  r4 mi'' do'' sold' |
  la'2.\melisma si'4 do''2. re''4\melismaEnd si'1 |
}
\ffclef "vsoprano" <>^\markup\character Deianira
mi''2 re''8 re'' re'' do'' |
<< \original la'4 \pygmalion do''4 >> r r2 |
r4 do''2 fa'8 fa' |
fa'4 fa'8 fa' sol'4 sol'8 fa' |
la' do'' sib' la' la'4 la'8 sol' |
<<
  \original { la'2( sol') fa'1 | }
  \pygmalion { la'4( sol') fa'2 | }
>>
r4 sol'8 do'' sib'4 sib'8 la' |
sib' sib' sib' sib' sib'4 sib'8 la' |
la'4 la' la'8 la' la' la'16 la' |
re'2 re''8 do''16 do'' do''8 do''16 re'' |
sib'4 sib' la'8 la' la' la'16 sib' |
sol'4 sol' r8 fa'' dod'' re'' |
\override AccidentalSuggestion.avoid-slur = #'outside
<<
  \original { re''2. \ficta dod''4( re''1) | }
  \pygmalion { re''4. \ficta dod''8( re''2) | }
>>

r8 mi'' do''4 r2 |
sol'4 sol'8 sol' sol'4 sol' |
do'' do'' r8 do'' do'' do'' |
la'4 la' r la' |
si'2 si'4 si'8 do'' |
do''4 do'' r2 |
