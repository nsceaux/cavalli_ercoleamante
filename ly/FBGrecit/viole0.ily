\clef "viole0" R1*22 |
\original {
  mi'1~ |
  mi' |
  do' |
  r2 do'4. re'8 |
  mi'2 mi' fa'1~ |
  fa'2 dod'4. dod'8 |
  re'2 mi'4 r |
  r2 sib4 r |
  r4 re'2 dod'4 |
  re'1 |
  re' |
  re'~ |
  re' |
  re' |
  do'2 mi'~ |
  mi'4 dod' re' si la2 la' sold'1 |
}
R1*19 |
