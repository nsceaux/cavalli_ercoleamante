\clef "viole1" R1*11 |
r8 la mi' do' la4 r8 do' |
fa' fa r si mi' mi r si |
mi' do' la si mi4 r8 si |
mi' sol'4 fad'8 sol' sol r mi' |
la re' si4 r8 fa' re' si |
sol4 r8 la re' si sol4 |
r8 fa sol4 r8 fa sol4 |
r8 do' sol4 r8 do' sol do' |
la re'4 sol8 do' sol'4 re'8 |
sol sol' fa' mi' fa' mi'4 re'8 do'1\fermata |
\original {
  do'1~ |
  do' |
  sol |
  sol |
  do'~ do'4 do' la2~ |
  la4 si sol2 |
  la1 |
  fa2 sol4 r |
  r4 sib la2~ |
  la1 |
  la |
  la~ |
  la |
  si2. si4 |
  mi' do'2 do'4 |
  la2. re'4 do'2 mi' mi'1 |
}
R1*19 |
