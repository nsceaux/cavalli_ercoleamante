\clef "dessus" fa''1 do''2 fa'' |
fa''4 mib'' re''2 mib''!4 re'' re''2~ |
re'' do'' re''2. fad''4 |
sol''2 la'' sib''2. la''4 |
sol''2 fa'' mib''1 |
re''2. fa''4 fa''2 sol'' |
fa''2. fa''4 re''2 do'' |
re''4 mib'' fa''2 sol''4 la'' sib'' do''' |
la'' fa'' sib''1 la''2 sib''1\fermata |
\original {
  r4 r fa'' |
  sol'' do''2 fa''4. mib''8 re''4 |
  mib'' do''2 sib'8 do'' re'' mib'' fa'' sol'' |
  mib'' do'' fa''4. mib''8 |
  re''8 do'' re''4 mib'' |
  mib'' re''4.\trill do''16 re'' |
  mib''4. do''8 re'' mib'' |
  fa''4. mib''16 re'' do''8 fa'' |
  re'' sol'' mib''4. re''8 |
  do''4 re''2 |
  do''4 fa''4. mib''8 |
  re''4. re''8 sol''4 |
  mib''4 fa''4.\trill mib''16 fa'' |
  sol''2 r4 |
  r r fa'' |
  re'' mib''2 |
  do''8 re''16 mib'' fa''2 |
  sol''4 fa''4. mib''8 |
  re''2. |
  r4 fa'' fa'' fa'' |
  mib'' fa'' re''4. mib''8 |
  fa''4 fa'' sol'' fa'' |
  sol'' la'' sib'' la''8 sol'' |
  fa''4 do''' sib'' la'' |
  sol''4. la''8 la''2 |
  r4 fa'' fa'' sol'' |
  mib'' do'' re''4. do''8 |
  sib'4 sib'' sol'' do''' |
  la''4. sol''8 sol''2 |
  r4 do''' la'' fa'' |
  sol'' la'' sib'' la''8 sol'' |
  fa''4 fa'' sol'' re'' |
  mib'' do'' re''4. mib''8 |
  fa''4 sib'' sol''8 la'' sib''4 |
  la''4. sib''8 sib''2 |
}
