\clef "basse" sib1 sib2 la |
sib2. la4 sol2 fa |
mib1 re2. re4 |
sol2 fad sol2. \ficta fa4 |
mib2 re do1 |
sib,2. sib,4 fa2 mi |
fa2. fa4 sib2 la |
sib2. la4 sol2 mib |
fa\breve sib,1\fermata |
\original {
  R2.*16 |
  r4 r fa |
  sol do2 |
  fa4. mib8 re4 |
  mib fa2 |
  sib,2. |
  r4 sib la fa |
  sol la sib la8 sol |
  fa4 re mib re |
  do fa sib,4. do8 |
  re4 la, sib, fa, |
  do do, fa,2 |
  r4 fa re mib |
  do fa, sib,4. la,8 |
  sol,4 sol, do mib |
  re re, sol,2 |
  r4 sol la sib |
  mib fa sol4. la8 |
  sib4 re mib sol |
  mib fa re4. do8 |
  sib,4 re mib sol |
  fa fa, sib,2 |
}
