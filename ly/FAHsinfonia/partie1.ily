\clef "viole1" fa'1 mib'2 fa' |
fa'2. fa'4 sol'2 re' |
sib sol re'2. re'4 |
re'2 la' sol'2. re'4 |
mib'2 fa' sol' mib'! |
fa'2. fa'4 fa'2 do' |
fa'2. fa'4 fa'2 la' |
fa'2. fa'4 sib2 mib'~ |
mib' re' fa'1 fa'\fermata |
\original {
  R2.*9 |
  r4 r fa' |
  sol' do'2 |
  fa'4. mib'8 re'4 |
  mib' do'2 |
  sib8 do' re' fa' mib' re' |
  do'4 re'4.\trill do'16 re' |
  mib'4. fa'8 sol' mib' |
  fa'2 lab'4 |
  sol' sol'2 |
  fa' fa'4 |
  mib'8 re' do'2 |
  sib2. |
  r4 sib' fa' la' |
  sol' fa' sib'4. sib'8 |
  do''4 sib' sol' re' |
  sol' fa' fa'2 |
  fa'4 fa' re'8 mi' fa'4 |
  sol'4. sol'8 fa'2 |
  r4 la' la' sol' |
  sol' fa' fa'2 |
  sib'4 re'' do'' sol' |
  re''4. do''8 si'2 |
  r4 sol' do'' sib' |
  sib' la' sol'4. do'8 |
  fa'4 sib' sol' sol' |
  sol' fa' fa'4. do'8 |
  fa'4 re' sol' sol' |
  do'4. fa'8 fa'2 |
}
