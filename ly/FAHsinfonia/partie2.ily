\clef "viole2" re'1 do'2 do' |
re'2. la4 sib2 fa |
sib do' la2. la4 |
sol4 sib la2 re'2. la4 |
do'2 re'4 sib do'1 |
fa2. sib4 la2 sol |
do'2. do'4 re'2 fa' |
re'4 do' sib2 re' sol4 sol' |
fa'2. fa4 do'1 sib\fermata |
\original {
  R2.*11 |
  r4 r sib |
  do' fa2 |
  sib4. lab8 sol4 |
  lab fa2 |
  mib8 fa sol lab sib do' |
  lab fa lab4. lab8 |
  sib4 sol do' |
  do'2 sib4 |
  sib fa'2 |
  fa'2. |
  r4 fa' la' re' |
  mib' do' re'4. sib8 |
  fa'4 fa' mib' fa' |
  do'4. la8 re'2 |
  re'4 do' sol la8 do' |
  sol4. do'8 do'2 |
  r4 fa' fa' mib'8 sol |
  do'4 do' sib re' |
  sib sib do' do'8 mib' |
  la4 re' re'2 |
  r4 re' do' re' |
  sol' fa' re'4. la8 |
  re'4 fa' mib' re' |
  do'4 do' sib4. sol8 |
  sib4 sib do' sib |
  fa'4. do'8 re'2 |
}
