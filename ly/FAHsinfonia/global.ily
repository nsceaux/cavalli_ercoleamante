\key fa \major
\time 4/4 \midiTempo#240 \measure 2/1 s1*16 \measure 3/1 s1*3
\pygmalion \bar "|."
\original {
  \bar ".|:"
  \digitTime\time 3/4 \midiTempo#160 s2.
  \measure 6/4 s1.*2
  \measure 3/4 s2.*16 \bar ":|.|:"
  \time 4/4 s1*6 \bar ":|.|:" s1*10 \bar ":|."
}
