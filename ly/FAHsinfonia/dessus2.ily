\clef "dessus" sib'1 mib''2 do'' |
sib'2. do''4 sib'2 la' |
sol'1 fad'2. la'4 |
sib'2 re'' re''4 do'' sib' re'' |
sol' la' sib'1 la'2 |
sib'2. re''4 do''2 do'' |
la'2. la'4 sib'2 fa'' |
sib'4 do'' re''2. re''4 sol''2 |
do''2 re'' do''1 re''\fermata |
\original {
  R2.*3 |
  r1*3/4 r4 r sib' |
  do'' fa'2 |
  sib'4. lab'8 sol'4 |
  lab'4 fa'2 |
  mib'8 fa' sol' lab' sib' do'' |
  lab' fa' lab'4. lab'8 |
  sib'4 sol' do''8 sib' |
  la'4. la'8 sib'4 |
  sol' la'4.\trill sol'16 la' |
  sib'2 sib'4 |
  lab'4 lab' fa' |
  sib'8 mib'' re'' do'' sib' sol' |
  do''4. sib'8 do'' re'' |
  sib'4 do''4. sib'8 |
  la'4. sib'16 do'' re''8 sib' |
  sib''4 la''4. sib''8 |
  sib''2. |
  r4 re'' do'' re'' |
  sib' do'' fa'4. sol'8 |
  la'4 re'' sib' sib' |
  mib'' do'' re'' do''8 sib' |
  la'4 la'' sol'' fa'' |
  mi''4. fa''8 fa''2 |
  r4 do'' re'' sib' |
  do'' la' sib'4. do''8 |
  re''4 sol'' mi''8 fa'' sol''4 |
  fad''4. sol''8 sol''2 |
  r4 sol'' fa'' re'' |
  mib'' do'' sol'' fa''8 mib'' |
  re''4 re'' sib' sib' |
  do'' la' sib'4. do''8 |
  re''4 fa'' mib'' re'' |
  fa''4. mib''8 re''2 |
}
