\score {
  \new StaffGroup <<
    \new Staff <<
      <>^\markup\italic {
        Questa sinf[onia] si fà per aspettare la mutatione della scena
      }
      \global \includeNotes "dessus1"
    >>
    \new Staff << \global \includeNotes "dessus2" >>
    \new Staff << \global \includeNotes "partie1" >>
    \new Staff << \global \includeNotes "partie2" >>
    \new Staff <<
      \global \includeNotes "basse"
      \origLayout {
        s1*14\break s1*5 s2.*6\pageBreak
        s2.*8\break s2.*7\pageBreak
        s1*8\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
