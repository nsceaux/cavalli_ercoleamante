\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \keepWithTag #'voix2 \includeNotes "voix" \clef "alto"
    >> \keepWithTag #'voix2 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'voix3 \includeNotes "voix" \clef "alto"
    >> \keepWithTag #'voix3 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'voix4 \includeNotes "voix"
    >> \keepWithTag #'voix4 \includeLyrics "paroles"
    \new Staff <<
      \global \keepWithTag #'basse-continue-conducteur \includeNotes "basse"
    >>
  >>
  \layout { }
}
