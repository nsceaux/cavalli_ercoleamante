<<
  \tag #'(voix1 basse) {
    <<
      \tag #'basse { s2.*3 \ffclef "vsoprano" }
      \tag #'voix1 { \clef "vsoprano" R2.*3 | }
    >>
    sol'4. fad'8 sol'4 la' si'2 |
    do''2.~ do''4\melisma si'2 |
    la'4\melismaEnd sol' sol' fa' mi'2 la'2.
    <<
      \tag #'basse { sol'4 s2 | s2.*2 | s4 \ffclef "vsoprano" }
      \tag #'voix1 { sol'2. | R2.*2 | r4 }
    >> re''2~ re''4 do'' do'' |
    do'' si'2 la'4 la' re'' |
    si'2 si'4 do'' si'4.( do''8) re''4 re'' re'' |
    si'4\melisma do''4. si'8 la'4\melismaEnd la' si'2 la'4 la' |
    si'4. la'8 si'4 la' sol'2 |
    do''2.\melisma fad'4 sol'2\melismaEnd |
    la'4 do''2~ do''4 si' si' |
    si' la'2 sol'4 sol' mi' |
    la' re' re' sol' sol'2 |
    sol'4\melisma la' si' do''4.\melismaEnd si'8( la'4) re'' si'\melisma la'8[ si'] do''4\melismaEnd do'' mi'' |
    do''4\melisma re''4. do''8 si'4\melismaEnd si'4 do''2 do''4. si'8( |
    do''1)\fermata |
  }
  \tag #'(voix2 basse) {
    \clef "valto" do'4. si8 do'4 |
    re' mi'2 <<
      \tag #'basse {
        fa'2. |
        s2.*7 s4
        \ffclef "valto"
      }
      \tag #'voix2 {
        fa'2.~ |
        fa'4\melisma mi'2 re'4 sol'2~ |
        sol'4 fad'2\melismaEnd sol'2 sol'4 |
        mi' mi' mi' re' do'2 do'4( re'2) mi'4
      }
    >> sol'2~ |
    sol'4 fa' fa' fa' mi'2 |
    re'4 \tag #'voix2 {
      re'4 sol'4 mi'2 mi'4 |
      mi' re'( mi') fad' mi' fad' |
      sol'4\melisma fad'4. sol'8 mi'4 fad'4. mi'8 re'4\melismaEnd re' si' |
      sol'\melisma la'4. sol'8 fad'4\melismaEnd fad' sol'2 sol'4 fad'! |
      sol'4. la'8 sol'4 fa' re'2 |
      mi'2. re'2 sol'4 |
      mi'4 fad'2 sol' sol'4 |
      r2*3/2 r4 sol'2~ |
      sol'4 fa' fa' fa' mi'2 |
      re'4 re' sol' mi' mi' mi' la' sol'2 sol'4 sol' si' |
      la'4 la' la' sol'2 la' sol' |
      sol'1\fermata |
    }
  }
  \tag #'voix3 {
    \clef "vtenor" R2. |
    sol4. fad8 sol4 la si2 |
    do'2.~ do'4\melisma si2 |
    la2.\melismaEnd sol2 sol4 |
    do'2 do'4 la sol2 la( si4) do'2. |
    r4 re'2~ re'4 do' do' |
    do'4 si2 la4 la do' |
    sol2 sol4 re' do'( re') |
    mi' re' mi' do'\melisma re'4. do'8 si4\melismaEnd si si |
    mi' do' do' re'2 do' re' |
    sol2. r2*3/2 |
    sol4. fad8 sol4 la si2 |
    do'2. re' |
    r4 re'2~ re'4 do' do' |
    do' si2 do'4 do' do' |
    si la re'( do') mi'( do') fa' re' re' mi' mi' sol' |
    mi'4\melisma fa'4. mi'8 re'4\melismaEnd re' fa'2 re' |
    mi'1\fermata |
  }
  \tag #'voix4 {
    \clef "vbasse" R2.*7 |
    do4. si,8 do4 re mi2 fa2.~ fa4\melisma mi2 |
    re2.\melismaEnd la |
    R2.*10 |
    sol,4. fad,8 sol,4 la, si,2 |
    do2.~ do4\melisma si,2 |
    la,2.\melismaEnd sol,4 sol2~ |
    sol4 fa fa fa mi2 |
    re4 re re do2 do4 |
    sol fa( sol) la sol la fa\melisma sol4. fa8 mi4\melismaEnd mi mi |
    la fa fa sol2 fa sol |
    do1\fermata |
  }
>>
