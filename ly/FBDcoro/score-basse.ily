\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \keepWithTag #'voix4 \includeNotes "voix"
    >> \keepWithTag #'voix4 \includeLyrics "paroles"
    \new Staff <<
      \global \keepWithTag #'basse-continue-conducteur \includeNotes "basse"
      \includeFigures "chiffres"
    >>
  >>
  \layout { }
}
