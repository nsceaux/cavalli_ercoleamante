\key la \minor \midiTempo#160
\digitTime\time 3/4 s2.
\measure 6/4 s1.*3
\measure 12/4 s1.*2
\measure 6/4 s1.*3
\measure 9/4 s2.*6
\measure 6/4 s1.*5
\measure 12/4 s1.*2
\measure 9/4 s2.*3
\time 4/4 s1 \bar "|."
