\clef "treble" R2.*7 |
do''4. do''8 sol''4 re''8 la' mi''2 do''8 si' la'4 fa'' do'' mi''2 |
la'2. r2*3/2 |
R2.*10 |
re''4. fad''8 re''4 la'' re'' sol'' |
sol''2 do''4 re'' sol''2 |
do''2. sol'' |
r2*3/2 r4 do''2~ |
do''4 re'' fa'' do''' sol'' sol'' |
sol'' re''2 la''4 mi'' mi'' fa''8 re'' sol''2 do''' sol''4 |
la''4 re''2 sol'' do'' sol'' |
sol''1\fermata |
