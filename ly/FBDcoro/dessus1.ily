\clef "treble" R2.*7 |
mi''4. re''8 mi''4 fa'' sol''2 fa'' re''4 sol''2 sol'4 |
re''2. r2*3/2 |
R2.*10 |
sol''4. re''8 sol''4 do'' si'2 |
mi''4. re''8 mi''4 fad'' re''2 |
la''2. re'' |
r4 re''2~ re''4 mi'' sol'' |
la'' si''2 sol''4 do''' do'' |
re'' la'' sol'' do''' sol'' do''' la'' re'' mi''8 fa'' sol''4 mi'' mi'' |
mi''4 la''2 re'' la'' re''4 sol'' |
mi''1\fermata |
