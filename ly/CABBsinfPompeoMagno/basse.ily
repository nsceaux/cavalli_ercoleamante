\clef "basse" fa2. |
mi re |
do4. re8 mi4 fa sol2 |
do'4 sib la sib4. la8 sib sol |
la2 la4 sol4. fa8 sol mi |
fa4. sol8 la4 sib do' do |
fa1*3/4\fermata |
