\clef "viole2" do'2 la4 |
do'2 do'4 la2 fa4 |
do'2. do'4 sol' re' |
mi' re' do' re'8 mi' fa'4 sol' |
do'2 do'4 re'2 re'8 sol' |
do'2. re'4 do'2 |
do'1*3/4\fermata |
