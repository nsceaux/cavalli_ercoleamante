\clef "viole1" fa'2 la'4 |
sol'2 sol'4 fa'2 re'4 |
mi'4. fa'8 sol'4 fa' re' sol' |
sol'2 r4 r2*3/2 |
la'4 sol' fa' sol'4. la'8 sol' do' |
fa'2. sib'4 sol'4. sol'8 |
la'1*3/4\fermata |
