\clef "dessus" la''4 sol'' fa'' |
sol''4. fa''8 sol'' mi'' fa''4. mi''8 fa'' re'' |
sol''4. fa''8 mi''4 la'' re''4. do''8 |
do''2. r2*3/2 |
do'''4 sib'' la'' sib''4. la''8 sib'' sol'' |
la''4. sol''8 fa''4 sol'' sol''4. fa''8 |
fa''1*3/4\fermata |
