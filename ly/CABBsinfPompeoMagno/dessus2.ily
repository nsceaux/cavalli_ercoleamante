\clef "dessus" fa''4 mi'' re'' |
mi''4. re''8 mi'' do'' re''4. do''8 re'' si' |
mi''4. re''8 do''4 do'' do''4. si'8 |
do''2 r4 re''4 do'' sib' |
do''4. re''8 fa'' fa'' re''4. fa''8 re'' mi'' |
do''4. sib'8 la' fa' fa''4 fa''4. mi''8 |
fa''1*3/4\fermata |
