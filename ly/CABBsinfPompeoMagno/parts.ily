\piecePartSpecs
#`((dessus #:system-count 1 #:indent 0)
   (parties #:system-count 1 #:indent 0)
   (violes #:system-count 1 #:indent 0)
   (basse #:score "score-basse")
   (silence #:on-the-fly-markup , #{ \markup\tacet #}))
