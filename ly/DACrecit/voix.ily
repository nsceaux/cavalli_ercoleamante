\clef "vbasse" <>^\markup\character Ercole
r4 sib8 sib sib4 sib8 sib |
sol sol sol sol sol4 sol8 sol |
do' sol sol fa la4 la |
r8 la fa4 r8 do' do' do' |
la la la la la4 sol8 la |
fa fa fa fa fa4 mi8 fa |
re re r re fa fa r4 |
fa8 fa16 fa la8 la r la la sol |
la4 la la8 la la la16 si |
si4 si si8 si16 si si8 re' |
si4 si8 do'16 re' sol4 sol |
\ffclef "vbas-dessus" <>^\markup\character Venere
<<
  \original { r4 mi'8 mi' mi'4 mi'8 fa' | }
  \pygmalion { r4 do''8 do'' do''4 do''8 do'' | }
>>
sol'8 sol' sol' fa' sol' sol'16 sol' la'8 sib' |
la'4 r8 la' la' la' la' sol' |
sol' sol' sol' sol' sol'4 sol'8 la' |
sib'4 sib'8 sib' sib' sib' sib' la' |
la'2 sol'4 sol'8 sol' |
la'2( sol') fa'1 |
r4 sol'8 mi' sol'4 mi' |
sol'4 sol'8 sol' do''4 do'' |
do'' do''8 re'' re''4 re'' |
re''8. re''16 re''8 re''16 <<
  \original { mi''16 do''4 do'' | }
  \pygmalion { do''16 mi''4 mi'' | }
>>
r8 do'' do'' do'' sol'4 sol'8 sol' |
sol' sol' sol' la' fa'4 fa'8 fa' |
fa'8 fa'16 fa' fa'8 mi' sol'8. sol'16 sol'8 sol' |
do''4 do''8 do'' do'' do''16 do'' do''8 sol' |
la'2 la'4 <<
  \original { fa'8 mi' | mi'2( re') do'1 | }
  \pygmalion { si'8 do'' | do''2. si'4( do''1) | }
>>
r8 <<
  \original { re'8 re' re' }
  \pygmalion { sol'8 sol' sol' }
>> sol'4. sol'8 |
sol'8 sol' sol' fad' la'4 la' |
re''8 la' la' la'16 la' re'8 re' la' sib' |
do''4 do''8 sib' <<
  \original { sol'4 sol' | }
  \pygmalion { sib'4 sib' | }
>>
r8 sol' re' re' re'4 re'8 re' |
re'8. re'16 re'8 re' re' re'16 re' re'8 do' |
re' re' re' mi' mi'8. mi'16 mi'8 fad' |
fad'4 fad' r fad' |
fad'8. fad'16 fad'8 fad' fad' fad' fad' mi' |
sol'4 sol' r8 sol' sol' sol' |
sol' sol' sol' sol' sol'4 sol'8 fad' |
la'4 la' r8 la' la' la' |
la'4 la'8 la' la' sib' do'' re'' |
sib'2 sib' |
sib'4. do''8 re''4 |
sol' la'2 |
fad'4 fad' sol' |
mi'4\melisma la'8[ sib' sol' la'] |
fad'4\melismaEnd fad' sib' |
sol'\melisma do''8[ re'' sib' do''] |
la'4 re''8[ mib'' do'' re''] |
sib'4\melismaEnd sib' re'' |
mib''2 si'4 |
do''2 fad'4 |
sib'( la'2) sol'1*3/4 |
\pygmalion { R2.*4 }
\ffclef "vbasse" <>^\markup\character Ercole
la4 la r fa |
la la8 la do'4 do'8 do' |
mi8. mi16 mi8 mi mi2 |
r4 sol8 sib mi4 mi8 fa |
fa4 fa r la8 la |
la4 la8 sol la4 la8 la |
si4. si8 si si si si |
si4 si8 do' do'4 do'8 do |
sol8 sol sol sol16 sol sol8 sol sol fa |
la4 la fa8 fa fa fa16 fa |
re4. re8 mi4 mi8 sol sol,1 do |
r8 do fa4 r8 fa mib fa |
re4 re sib la |
sol fa8 sol mib4 mib |
do do r8 do re mib |
re re re8. do16 sib,4 la,8 sol, |
re2 sol,4. sol,8 |
do4 sol2 mi8 do |
fa4 fa8 fa fa sol la fa |
sib4 sib sib8 sib16 lab sol8 fa |
mib8 mib16 mib re8 do sib, sib, la, sol, |
fa,4 mib, fa,2 sib,1\fermata | \allowPageTurn
\ffclef "vbas-dessus" <>^\markup\character Venere
re''8 sib' re'' do'' do''4 do''8 re'' |
sib'4 sib' r2 |
R1 |
r2 r8 fa'' mib'' re'' |
do''4 sib'8 do'' re''4 do''8[ re''] |
sib' mib'' re'' do'' sib'4 la'8 sib' |
do''[ sib'] la'[ sib'] sol'2 |
r8 do'' sib' la' sol'4 fa'8 sol' |
la'4 sol'8[ la'] fa'2 |
r8 fa'' mib'' re'' do''4 sib'8 do'' |
re''4 do''8[ re''] sib'2~ |
sib'4 sib'8[ do''] re''4 do''8[ re''] sib'1 |
re''4 do''4. re''8 |
sib'4 sol' la' |
sib'8[ la' sib' sol'] do''[ la'] |
re''[\melisma do'' re'' sol'] do''[ la'] |
sib'[ la' sol' la' sib' do''] |
re''4\melismaEnd re''8 la' sib' do'' |
re''4 mi''2 |
fa''4 re''8.[\melisma mi''16 re''8. do''16] |
si'4 mi''8.[ fa''16 mi''8. re''16] |
dod''4 fa''8.[ sol''16 fa''8. mi''16] |
re''4\melismaEnd sol''8[ fa''] mi''[ re''] |
mi''4 la' re''8 re'' |
re''4 mi'' fa'' |
re''2 dod''4( re''2.) |

r4 re''8 fa'' re'' re'' re'' fa'' |
sib'4 sib'8 la' do''4 do'' |
r8 do'' fa'' do'' re'' la' |
sib' fa' sol' do'' la' re''16[\melisma do''] |
sib'8[ la'16 sib'] sol'8[ do''16 sib'] la'8[ sol'16 la'] |
fa'2( sol'4)\melismaEnd sol'2 do''4 |
do''2. r4 r fa'' |
fa''2. |
sib'4 sib' sib' |
sib'2.~ |
sib'2\melisma do''8[ sib'] |
lab'2 sib'8[ lab'] sol'2.\melismaEnd |
r4 la'4. sib'8 sib'2. |
sib'4 mi''!4. fa''8 fa''2. fa'' |
r4 fa''4. mib''8 |
re''4 do'' sib' |
do''2. sib'~ |
sib'~ sib' |
R2. |
re''8 sib' re'' re'' la'4 la'8 la' |
la' la' la' la' la'4 la'8 sib' |
sol'4 sol' r2 |
\original {
  sib'8 sib'16 sib' sib'8 la' do''4 do'' |
  sib'8 sib' sib' sib'16 la' la'4 la' |
  r4 fad'8 fad' fad'4 fad'8 fad' |
  fad'4 fad' la' fad'8 sol' |
  sol'4 sol' r8 la' la' la' |
  do''4 do'' r8 re'' re'' mib'' |
  mib''4 mib''8 sib' sol'4. fad'8( sol'1) |
}
r4 r8 fa' fa' fa' fa'4 |
fa'8 fa'16 fa' fa'8 mib' sol' sol' r sol' |
sol'4 sol' la' la'8 sib' |
sib'4 sib'8 sib' re'' re'' re'' mi'' |
fa'' do'' fa''16[\melisma mib'' re'' do''] sib'8\melismaEnd mib'' do''16[ sib' do'' re''] sib'1\fermata |
