\clef "viole1" R1*44 R2.*16 R1*27 |
r4 r8 sib' la'4 sol'8 fa' |
mib'4 re'8 do' re' do' sib4 |
fa'8 do' mib'4 re' fa'~ |
fa'8 mib' fa' do' mib' re' la fa |
do' sol re'4 si sol~ |
sol8 do' fa4 sib fa8 sib |
la fa do'8. la16 la8 sib do'4~ |
do' sol'4. do'8 re' do' |
sib8 fa' do' fa' re'4 sol'~ |
sol'8 sol re' do' fa'4. re'8 re'1 |
re'4 la8 sib do' la |
sib4 do'4. re'8 |
sib4 mib'8 sib fa'4 |
sib4 fa'8 re' do' re' |
sib2 sol4 |
re'2 sol4 |
re'4. do'8 sib do' |
la4 re'2 |
re'4. re'8 sol'8. fa'16 |
mi'4 la'4. sol'8 |
fa'4 sib'8 la' sol' fa' |
la'4. mi'8 fa'4 |
re' sol'4. fa'8 |
mi'2 mi'4 fad'2. |
