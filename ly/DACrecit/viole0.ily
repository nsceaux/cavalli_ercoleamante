\clef "viole0" R1*44 R2.*16 R1*27 |
r2 r8 do'' sib' la' |
sol' la' sib' la' sib' la' sol'4 |
la' sol'8. la'16 sib'4. la'8 |
sib' la' sib' do'' sol' sib' mib' fa' |
sol'4. fad'8 sol' re' mi' fa' |
sol' la' fa'4 re'8 mi' do' re' |
do' fa'4 mi'8 fa'4. sib'8 |
la' fa' sib'4. la'8 sib' sol' |
fa'8 sib'4 la'8 sib' fa'[ mib' re'] |
mib' sol' fa' sib'4 la'16 sol' la'4 sib'1 |
fa'4 fad'4. fad'8 |
sol'2 r4 |
sol'4. mi'8 la' sol' |
fa' sol' la' sib' fad'4 |
sol'8 fa' mib' fa' sol' la' |
fad'4. fad'8 sol' la' |
sib'4 sib'4. la'8 |
la'2 la'4 |
re'' si'4. si'8 |
mi''4 dod''4. dod''8 |
re''2 re''4 |
dod''2 la'4 |
si' dod'' re'' |
mi''2. re'' |
