\clef "basse" sib1 |
sol mi2 fa |
fa1~ |
fa |
fa |
re~ |
re2~ re |
re1~ |
re~ |
re2 do |
do1 |
do |
fa, |
do |
sol, |
la,2 sib, |
do1 fa, |
do |
do |
<< \original do2 \pygmalion la, >> sol,~ |
sol, do |
do1~ |
do2 <<
  \original { fa,2~ | fa, mi, | }
  \pygmalion { fa,2 | re, do, | }
>>
mi,1 |
<<
  \original { fa,1 | sol, do | }
  \pygmalion { fa,2 fa | sol sol, do1 | }
>>
sol,~ |
sol,2 fad,~ |
fad,1~ |
fad,2 sol,~ |
sol,1~ |
sol, |
sol, |
re |
re |
mi2~ mi~ |
mi1 |
fad~ |
fad |
sol |
sol2 fa4 |
mib do2 |
re si,4 |
do2. |
re |
mi |
fad |
sol |
do2 re4 |
mib2 re4 |
do re2 <<
  \original { sol,1*3/4\fermata | }
  \pygmalion {
    sol,2 si4 |
    do'2 re'4 |
    mib'2 re'4 |
    do' re' re |
    sol,1*3/4\fermata | \allowPageTurn
  }
>>
fa1~ |
fa |
sol~ |
sol |
fa |
fa |
re~ |
re2 do |
do1 |
la, |
si,2 do4 mi, sol,1 do |
fa,1 |
sib,2 sib4 la |
sol re mib2~ |
mib do |
re sib,4 sol, |
re2 sol, |
do1 |
fa |
sib |
mib2 sib, |
fa,4 \ficta mib, fa,2 sib,1\fermata |
sib,2. la,4 |
sib,1 |
r8 sib la sol fa4 sol8 re |
mib4 fa sib,8 do re mib |
fa4 sol8 mib fa2 |
sib8 do' sib la sol fa mib re |
do4 re sol,8 fa mi re |
do4 re sib, la,8 sib, |
do2 fa8 sib la sol |
fa4 sol mib re8 mib |
fa4 fa, sib,8 la, sol, fa, |
mib,4 re,8 mib, fa,2 sib,1 | \allowPageTurn
sib4 la2 |
sol fad4 |
sol2 la4 |
sib2 la4 |
sol2 mib4 |
re2 do4 |
sib, sol,2 |
fa,2. |
sol, |
la, |
sib, |
la,2 fa,4 |
sol,2. |
sol,4 la,2 re2. |

sib1 |
sol2 mi |
fa4. fa8 sib fa |
sol re mib do fa re |
sol fa mi do fa mi |
re2. do~ |
do2 fa4 fa2.~ |
fa2 sib4 |
sib2. |
mib4 mib mib |
mib2.~ |
mib4 re2 mib mib8 re |
do2. sib,4 sib4. la8 |
sol2. fa4 fa4. mib8 re4 do sib, |
fa2. |
sol4 la sib |
fa2. sib,4 sib4. la8 |
sol4 fa mib fa2. |
sib,\fermata |
sol,1 |
fad,1 |
sol, |
\original {
  sol,2 do~ |
  do re |
  do1 |
  sib,2 la, |
  sol, fa, |
  mib, re, |
  do, re, sol,1 |
}
sib,1~ |
sib,2 mib~ |
mib1 |
sib,2 sol, |
fa,4. fa8 sol mib fa fa, sib,1\fermata |
