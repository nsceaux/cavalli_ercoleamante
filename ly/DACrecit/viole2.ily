\clef "viole2" R1*44 R2.*16 R1*27 |
r2 r4 r8 re' |
sib do' fa'2 fa'4 |
do'8 fa sib4 fa'4. re'8 |
re' do' re' fa' sib4 do'8 sib |
mib'4 la8 re'4 la8 do' re' |
mib'4 re' sol la8 sol |
fa do' do'4. re'8 fa'4~ |
fa'8 do' mib' fa' do' sol' fa' mib' |
re'4 fa'8 fa4 fa8 sib la |
sol mib sib4 fa2 fa1 |
sib4 do'2 |
re'4 la2 |
re'8 do' sib4 do' |
re'2 la4 |
re' sib mib' |
la sib4. la8 |
sol4 sib2 |
do'4 la4. la8 |
si4 sol4. sol8 |
dod'4 la4. la8 |
re'2 sib4 |
mi'2 re'4 |
sol2 sib4 |
sib la2 la2. |
