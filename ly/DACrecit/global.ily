\set Score.currentBarNumber = 9 \bar ""
\key fa \major \midiTempo#160
\time 4/4 s1*11
\time 4/4 s1*6
\measure 2/1 s1*2
\measure 4/4 s1*9
\measure 2/1 s1*2
\measure 4/4 s1*14
\digitTime\time 3/4 s2.*10 \measure 6/4 s2.*2
\pygmalion { \measure 3/4 s2.*4 }
\bar "|."
\time 4/4 s1*10
\measure 3/1 s1*3
\measure 4/4 s1*10
\measure 2/1 s1*2 \bar "|."
\measure 4/4 s1*2
\beginMark "Aria" \time 4/4 s1*9 \measure 2/1 s1*2
\digitTime\time 3/4 s2.*13 \measure 6/4 s2.*2

\time 4/4 s1*2
\digitTime\time 3/4 s2.*3
\measure 6/4 s2.*4
\measure 3/4 s2.*4
\measure 6/4 s2.*4
\measure 9/4 s2.*3
\measure 3/4 s2.*2
\measure 6/4 s2.*4
\measure 3/4 s2.
\time 4/4 s1*3
\original { s1*6 \measure 2/1 s1*2 }
\measure 4/4 s1*4
\measure 2/1 s1*2
\bar "|."
