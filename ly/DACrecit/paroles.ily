E per me can -- gi o dè -- a
le de -- li -- zie del ciel con ques -- to suo -- lo
ed or per -- ché non man -- da
la pa -- lu -- de Ler -- ne -- à
e la sel -- va Ne -- me -- à
nov’ i -- dre, al -- tri le -- o -- ni a far qui me -- co
glo -- ri -- o -- si con -- tras -- ti,
on -- de a te for -- mi o dèa gra -- ti o -- lo -- caus -- ti?

Pur ch’io giun -- ga a can -- giar nel cru -- do se -- no
d’I -- o -- le il cor, e te lo ren -- da a -- man -- te
ne trar -- rò tal pia -- ce -- re,
che fia d’o -- gni o -- pra mia pre -- mio bas -- tan -- te,
mi -- ra mi -- ra quest’ è la ver -- ga on -- de fa Cir -- ce
ma -- gi -- che me -- ra -- vi -- glie;
al di cui mo -- to ub -- bi -- dien -- ti an -- cel -- le
per pat -- to in -- al -- te -- ra -- bi -- le son tut -- te
di li -- di A -- che -- ron -- te -- i l’a -- ni -- me fel -- le.
Or in vir -- tù di sì po -- ten -- te ste -- lo
do -- ve toc -- co la ter -- ra
nas -- ce -- rà seg -- gio er -- bo -- so in cui ri -- pos -- te,
da spi -- ri -- ti la -- sci -- vi a ciò cos -- tret -- ti
le man -- dra -- go -- re os -- ce -- ne
di pal -- li -- do co -- lor la Li -- dia pie -- tra
e d’a -- mo -- ro -- se ron -- di -- nel -- le i co -- ri
fa -- ran ch’I -- o -- le al -- lor, ch’in lui s’as -- si -- da
can -- gi per te il suo sde -- gno
in dol -- ci in dol -- ci in dol -- ci dol -- ci a -- mo -- ri.

Di -- va ad o -- pre sì ra -- re
in -- so -- li -- to tre -- mor tut -- to tut -- to mi scuo -- te,
e poi ch’es -- ser non puo -- te
ti -- mor da me non co -- no -- sciu -- to an -- co -- ra
forz’ è che si -- a per in -- spi -- rar su -- per -- no
di fu -- tu -- ro gio -- ir pre -- sa -- gio in -- ter -- no.
Ma pur nel pen -- sier mi -- o sce -- man sce -- man di pre -- gio
quel -- li, ch’a me pro -- met -- ti
sos -- pi -- ra -- ti di -- let -- ti,
quall’ hor las -- so m’av -- veg -- gio
ch’a far miei dì gio -- con -- di
trat -- te non fian tai gio -- ie
dal mar d’a -- mor, ma da gli sti -- gi fon -- di.

O di o di ques -- ta can -- zo -- ne
Pur che tu go -- da
ch’im -- por -- ta a te?
Che sia per fro -- da
o per mer -- cé?
Pur che tu go -- da
ch’im -- por -- ta a te?
Pur che tu go -- da
ch’im -- por -- ta a te __
ch’im -- por -- ta a te?

Ch’al -- tro è l’a -- ma -- re?
Ch’un guer -- reg -- gia -- re,
o -- ve in tri -- on -- fo e -- gual lie -- ti sen’ van -- no
il va -- lor, e l’in -- gan -- no;

in -- fe -- li -- ce in -- fe -- li -- ce non sa -- i?
Che nel gran re -- gno del mio fi -- glio ar -- cie -- ro
non v’è non v’è tol -- to il pe -- nar __
nul -- la nul -- la nul -- la nul -- la nul -- la nul -- la di ve -- ro. __
Pren -- di pren -- di il crin, che for -- tu -- na
per mia man t’of -- fre in do -- no.
\original {
  Tor -- bi -- do ri -- vo an -- co -- ra
  spe -- gne se -- te in -- fi -- ni -- ta,
  e per lan -- gui -- da in -- e -- dia un che si mo -- ra
  non sce -- glie i ci -- bi a sos -- te -- ner -- si in vi -- ta: __
  ma
}
\pygmalion { Ma }
men -- tre a te gius -- ta ra -- gion m’in -- vo -- la
se d’al -- tro uo -- po ti si -- a
Mer -- cu -- rio in -- vie -- rò, che rat -- to vo -- la.

