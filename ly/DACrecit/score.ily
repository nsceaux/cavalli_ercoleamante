\score {
  \new StaffGroupNoBar <<
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \original\origLayout {
        s1*4\break s1*4\break s1*4\pageBreak
        s1*4\break s1*7\break s1*4\break s1*6\break s1*4\pageBreak
        s1*5\break s1*2 s2.*6\break s2.*6 s1*2\break s1*5\break s1*6\pageBreak
        s1*5 s2 \bar "" \break s2 s1*4\break s1*4\break s1*5\break s1*6\pageBreak
        s2.*8\break s2.*7 s1\break s1 s2.*5\break s2.*13\break s2.*7 s1\pageBreak
        s1*5\break s1*6\break
      }
      \pygmalion\modVersion {
        s1*44 s2.*16\break s1*25\break
      }
    >>
  >>
  \layout { indent = \noindent }
  \midi { }
}
