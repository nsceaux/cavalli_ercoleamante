\clef "viole2" r4 r sol' |
re'4. si8 do'4 sol' sol' re' |
mi'4. mi'8 si4 |
do' re' do' |
do'2 do'4 sol'2 sol'4 |
do'4. mi'8 si4 mi' mi' mi' |
do'4. do'8 sol'4 do' do' fa' |
re'2. do'1\fermata |
