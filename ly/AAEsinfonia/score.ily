\score {
  \new StaffGroup <<
    \new Staff << \global \includeNotes "dessus1" >>
    \new Staff << \global \includeNotes "dessus2" >>
    \new Staff << \global \includeNotes "partie1" >>
    \new Staff << \global \includeNotes "partie2" >>
    \new Staff <<
      \global \includeNotes "basse"
      \origLayout { s2.*5\break }
    >>
  >>
  \layout {
    indent = \noindent
    system-count = 1
  }
  \midi { }
}
