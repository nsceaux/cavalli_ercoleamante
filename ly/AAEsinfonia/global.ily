\key do \major \midiTempo#160
\digitTime\time 3/4 s2.
\measure 6/4 s1.
\measure 3/4 s2.*2
\measure 6/4 s1.*3
\measure 7/4 s4*7 \bar "|."
