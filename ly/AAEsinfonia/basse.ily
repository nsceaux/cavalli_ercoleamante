\clef "basse" r4 r do'4 |
si4. si8 la4 sol sol fa |
mi4. mi8 re4 |
do si, do |
la,2. sol,2 sol4 |
mi4. mi8 re4 do do do' |
la4. la8 sol4 fa mi fa |
sol2. do1\fermata |
