\clef "dessus" r4 r mi'' |
re''4. re''8 do''4 si' si' la' |
si'4. do''8 re''4 |
sol' si' sol' |
do''2. si'2 si'4 |
do''4. do''8 re''4 mi'' mi'' do'' |
do''4. re''8 mi''4 fa'' do'' do'' |
do'' si'4. do''8 do''1\fermata |
