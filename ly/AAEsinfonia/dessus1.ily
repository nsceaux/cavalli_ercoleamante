\clef "dessus" r4 r sol'' |
sol''4. sol''8 fad''4 sol'' sol'' la'' |
sol''4. sol''8 fa''4 |
mi'' re'' sol'' |
sol''4 fad''4. sol''8 sol''2 re''4 |
mi''4. mi''8 fa''4 sol'' sol'' mi'' |
fa''4. fa''8 sol''4 la'' sol'' la'' |
re'' re''4. mi''8 mi''1\fermata |
