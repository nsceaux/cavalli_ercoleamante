\clef "viole1" r4 r do'' |
sol'4. sol'8 la'4 re' re' fa' |
sol'4. sol'8 re'4 |
mi' sol' mi' |
la'2 la'4 re'2 re'4 |
sol'4. sol'8 fa'4 do'' do'' sol' |
la'4. la'8 sib'4 la' mi' do' |
sol'2. sol'1\fermata |
