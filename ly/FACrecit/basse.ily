\clef "basse"
<<
  \tag #'basse-continue {
    sib2. |
    r4 la fa |
    sib2. |
    re4 mib2 fa2. |
    sib,2~ sib, |
    la,2. sib,4 |
    do2 fa,~ |
    fa,1 |
    do |
    mib2. fa4 |
    sol2 lab4. fa8 sol2 do |
    <<
      \original {
        la,2. |
        sib,4 do2 |
        fa,4 fa8 mib re do |
        sib,4. do8 re4 |
        mib fa2 |
        sol do |
        re sol,~ |
        sol, mi,~ |
        mi, fa,~ |
        fa, la, |
        sib, mib |
        fa1 |
        sib,2 si,4 |
      }
      \pygmalion { si,2 sol,4 | }
    >>
    do2 fa4 |
    sol4 lab8 fa sol4 |
    do4. re8 mi do |
    fa4. fa8 \ficta mib fa |
    re4. re8 do sib, |
    do4. re8 do sib, |
    <<
      \original {
        la,4. sib,8 la, sol, |
        fa,4. sol,8 la, fa, |
        sib,2 do4 fa,2. |
        fa,1 sib, |
      }
      \pygmalion {
        la,8 sol, la, sib, la, sol, |
        fa, mi, fa, sol, la, fa, |
        sib, la, sib, sol, do do, fa,2. |
        la,1 sib, |
      }
    >>
    r2*3/4 r8 do' la |
    sib sol4 la8 la fa |
    sol4 mi8 fa4 fa8 |
    sib,4. do |
    fa,8 fa re mib do4 |
    re8 sib,4 do8 do' la |
    sib8 sol4 lab8 lab fa |
    sol4 sol8 re mib4 |
    fa4. sib, |
    %%
    \clef "alto" r2*3/2 r4 do'' la' |
    sib' sol'2 la'4 \clef "tenor" fa' re' |
    mib' do'2 re'4 re' sib |
    do'2 la4 sib2 sib4 |
    do' la fa do'2. |
    fa4 \clef "bass" fa re mib do2 |
    re4 re sib, fa2 re4 |
    sol2 re4 mib2 mib4 |
    fa re sib, fa2. sib,1*3/4\fermata |
  }
>>
