\tag #'(clerica basse) {
  Pe -- ra pe -- ra mo -- ra mo -- ra l’in -- de -- gno
  di cui più scel -- le -- ra -- to un -- qua non vis -- se,
  che del troi -- a -- no ec -- ci -- dio an -- cor fu -- man -- te
  non mai sa -- zio di san -- gue
  i miei po -- ve -- ri fi -- gli, e me tra -- fis -- se, __
  \original {
    o bel -- la glo -- ria in ve -- ro
    o bel -- la glo -- ria in ve -- ro
    d’un uc -- ci -- sor di mos -- tri,
    im -- pie -- ga -- re il va -- lo -- re
    con cui d’ha -- ver si van -- ta
    sos -- te -- nu -- te le stel -- le
    con -- tro te -- ne -- ri par -- ti, e ma -- dre im -- bel -- le.
  }
  Ah ver’ un chios -- tro
  più fie -- ro mos -- tro
  di lui non ha.
  E se il cru -- de -- le
  per nos -- tro uf -- fi -- cio hog -- gi ca -- drà
  mai sa -- cri -- fi -- zio
  più gra -- to al ciel
  al -- tri fe’, né mai fa -- rà.
  Che più dun -- que s’as -- pet -- ta?
  
  Pe -- ra pe -- ra mo -- ra il cru -- del, sù sù sù sù ven -- det -- ta ven -- det -- ta.
  Pe -- ra pe -- ra mo -- ra il cru -- del, sù sù sù sù ven -- det -- ta ven -- det -- ta.
}
\tag #'(vsoprano valto vtenor) {
  Pe -- ra pe -- ra mo -- ra il cru -- del, sù sù
  \tag #'(vsoprano valto) { sù sù ven -- det -- ta }
  \tag #'vsoprano { ven -- det -- ta }
  ven -- det -- ta ven -- det -- ta.
}
\tag #'(vsoprano valto vtenor vbasse) {
  Pe -- ra pe -- ra mo -- ra il cru -- del, sù sù sù sù ven -- det -- ta ven -- det -- ta.
}
