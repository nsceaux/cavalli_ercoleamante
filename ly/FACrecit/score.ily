\score {
  \new StaffGroupNoBar \with { \haraKiriFirst } <<
    \new StaffGroupNoBracket <<
      \new Staff <<
        {
          s2.*5 s1*8 \original { s2.*5 s1*7 } s2.*11 s1*2 s2.*9
          s1. \override MultiMeasureRest.transparent = ##t 
          \noHaraKiri R1.*7
        }
        \global \includeNotes "dessus1"
      >>
      \new Staff <<
        {
          s2.*5 s1*8 \original { s2.*5 s1*7 } s2.*11 s1*2 s2.*9
          s1. \override MultiMeasureRest.transparent = ##t 
          \noHaraKiri R1.*7
        }
        \global \includeNotes "dessus2"
      >>
      \new Staff <<
        \original {
          s2.*5 s1*8 \original { s2.*5 s1*7 } s2.*11 s1*2 s2.*9
          s1. \override MultiMeasureRest.transparent = ##t 
          \noHaraKiri R1.*7
        }
        \global \includeNotes "partie1"
      >>
      \new Staff <<
        \original {
          s2.*5 s1*8 \original { s2.*5 s1*7 } s2.*11 s1*2 s2.*9
          s1. \override MultiMeasureRest.transparent = ##t 
          \noHaraKiri R1.*7
        }
        \global \includeNotes "partie2"
      >>
      \new Staff <<
        \original {
          s2.*5 s1*8 \original { s2.*5 s1*7 } s2.*11 s1*2 s2.*9
          s1. \override MultiMeasureRest.transparent = ##t 
          \noHaraKiri R1.*7
        }
        \global \keepWithTag #'basse \includeNotes "basse"
      >>
    >>
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'vsoprano \includeNotes "voix"
      >> \keepWithTag #'vsoprano \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'valto \includeNotes "voix"
      >> \keepWithTag #'valto \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vtenor \includeNotes "voix"
      >> \keepWithTag #'vtenor \includeLyrics "paroles"
      \new Staff \withLyrics <<
        { s2.*5 s1*8 \original { s2.*5 s1*7 } s2.*11 s1*2 s2.*9\break
          s2\noHaraKiri }
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new Staff \withLyrics <<
      \global \keepWithTag #'clerica \includeNotes "voix"
    >> \keepWithTag #'clerica \includeLyrics "paroles"
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2.*5 s1*2\break s1*4\break s1*2 s2.*5\break
        s1*4\break s1*3 s2.*2\pageBreak
        s2.*6\break s2.*3 s1*2 s2.*2\break s2.*7\break s1.*5\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
