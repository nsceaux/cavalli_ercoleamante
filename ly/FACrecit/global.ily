\key fa \major
\digitTime\time 3/4 \midiTempo#160 s2.*3
\measure 6/4 s1.
\time 4/4 \midiTempo#80 s1*6
\measure 2/1 s1*2
\original {
  \digitTime\time 3/4 \midiTempo#160 s2.*5
  \time 4/4 \midiTempo#80 s1*7
}
\digitTime\time 3/4 \midiTempo#160 s2.*9
\measure 6/4 s1.
\time 4/4 \measure 2/1 s1*2
\time 6/8 \midiTempo#80 s2.*9 \bar "|."
\time 6/4 \midiTempo#160 s1.*8 \measure 9/4 s2.*3 \bar "|."
