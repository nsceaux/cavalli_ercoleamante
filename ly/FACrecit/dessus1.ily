\clef "dessus" R2.*5 R1*8 \original { R2.*5 R1*7 } R2.*11 R1*2 R2.*9
\skip1.*4 |
s2. r4 sib'' sol'' |
la'' la'' fa'' sol'' mib''2 |
fa''4 fa'' fa''8 sol'' la''2 la''4 |
sib''2 fa''4 sol'' sol'' sol'' |
la'' fa'' sib'' fa''2. fa''1*3/4\fermata |
