\piecePartSpecs
#`((dessus #:score "score-dessus"
           #:music , #{ s2.*5 s1*8 \original { s2.*5 s1*7 } s2.*11 s1*2 s2.*9\break #})
   (parties #:score "score-parties"
            #:music , #{ s2.*5 s1*8 \original { s2.*5 s1*7 } s2.*11 s1*2 s2.*9\break #})
   (violes #:score "score-violes"
            #:music , #{ s2.*5 s1*8 \original { s2.*5 s1*7 } s2.*11 s1*2 s2.*9\break #})
   (basse #:score "score-basse"
           #:music , #{ s2.*5 s1*8 \original { s2.*5 s1*7 } s2.*11 s1*2 s2.*9\break #}))
