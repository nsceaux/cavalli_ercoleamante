\score {
  <<
    \new ChoirStaff \with { \haraKiriFirst } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'valto \includeNotes "voix" \clef "alto"
      >> \keepWithTag #'valto { \includeLyrics "paroles" }
      \new Staff \with { \haraKiriFirst } \withLyrics <<
        \global \keepWithTag #'vtenor \includeNotes "voix" \clef "alto"
      >> \keepWithTag #'vtenor { \includeLyrics "paroles" }
      \new Staff \withLyrics <<
        { s2.*5 s1*8 \original { s2.*5 s1*7 } s2.*11 s1*2 s2.*9\break
          s2\noHaraKiri }
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new ChoirStaff <<
      \new Staff \with { \tinyStaff \haraKiriFirst } \withLyrics <<
        \global \keepWithTag #'basse \includeNotes "voix"
      >> \keepWithTag #'basse { \set fontSize = #-2 \includeLyrics "paroles" }
      \new Staff << \global \includeNotes "basse" >>
    >>
  >>
  \layout {
    system-count = #(*system-count*)
    indent = #(if (*instrument-name*)
                  largeindent
                  (or (*score-indent*) smallindent))
  }
}
