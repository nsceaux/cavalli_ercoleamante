\score {
  \new ChoirStaff \with { \haraKiriFirst } <<
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      { s2.*5 s1*8 s2.*5 s1*7 s2.*11 s1*2 s2.*9 s2 \noHaraKiri }
      \global \keepWithTag #'vbasse \includeNotes "voix"
    >> \keepWithTag #'vbasse { \includeLyrics "paroles" }
    \new Staff \with { \tinyStaff \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'basse \includeNotes "voix"
    >> \keepWithTag #'basse { \set fontSize = #-2 \includeLyrics "paroles" }
    \new Staff <<
      $(or (*score-extra-music*) (make-music 'Music))
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
    >>
  >>
  \layout { }
}
