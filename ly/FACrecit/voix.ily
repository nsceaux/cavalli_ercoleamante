<<
  \tag #'(clerica basse) {
    \clef "vsoprano" <>^\markup\character Clerica Regina
    r4 re'' sib' |
    fa'' fa'' r |
    r re''4. mib''8 |
    fa''4 mib'' re'' mib''2. |
    re''4 r8 sib' re'' re'' re'' mi'' |
    fa''4 fa'' do'' re''8 do'' |
    sib'2 la'4 r8 la' |
    la' la' la' sib' do'' do''16 do'' do''8 re'' |
    mib''8 mib'' mib'' mib'' mib''4 mib''8 fa'' |
    sol''4 sol'' mib''8 re'' re'' re''16 re'' |
    si'4 si'8 re'' fa'' si' do''2 si'!4( do''2) |
    \original {
      r4 do''2 |
      sib'8 la' sol'4 sol'8 la' |
      fa'4 fa' r |
      r fa''2 |
      mib''8 re'' do''4 do''8 re'' |
      sib'4 sib'8 re'' do'' sib' do'' re'' |
      sib'4( la') sol'4 sib'8 sib' |
      sib'4 sib'8 la' do''4 do''8 do'' |
      do'' do'' do'' do'' do'' do'' do'' do'' |
      fa''4 fa''8 fa'' fa''4 fa'' |
      re''8 re'' re'' re''16 do'' do''4 do'' |
      r8 do'' reb'' reb'' sib'4 sib' |
    }
    r8 re''( sol''4) sol''8 fa'' |
    mib''4 mib''8 mib'' re'' do'' |
    si'4 do''8 fa'' mib'' re'' |
    do''4 r8 do'' sib' do'' |
    la'4. la'8 sol' la' |
    sib'4 sib'8 sib' do'' re'' |
    sol'4 r8 sol' la' sib' |
    do''4 do''8 do'' re'' mi'' |
    fa''2 mib''8 re'' |
    re''4 r8 sib' sol' la' fa'2. |
    r4 fa''8 fa'' fa''4 fa''8 << \original sol'' \pygmalion do'' >> re''4 re'' r2 |
    r8 fa'' re'' mib'' do''4 |
    re''8 re'' sib' do''4 re''8 |
    sib'4 do''8 la'4 la'8 |
    re'' sib' la' la'( sol'4) |
    fa'4. r8 sol'' mib'' |
    fa'' re''4 mib''8 mib'' do'' |
    re''4 mib''8 do''4 re''8 |
    sib'4 sib'8 fa'' mib'' re'' |
    re''( do''4) sib'4. |
  }
  \tag #'vsoprano {
    \clef "vsoprano" R2.*5 R1*8 \original { R2.*5 R1*7 } R2.*11 R1*2 R2.*9
    <>^\markup\character Coro
    r4 fa'' re'' mib'' do''2 |
    re''4 re'' sib' do''2 re''4 |
    sib'2 do''4 la'2 sib'4 |
    sol' sol' la' re'' sib' sib' |
    sol' do'' la' sol'2. |
    la'4 do'' re'' sib' do''2 |
    la'4 la' re'' do''2 re''4 |
    sib'2 sib'4 sol'2 do''4 |
    la' sib' sib' sib'2 la'4( sib'1*3/4)\fermata |
  }
  \tag #'valto {
    \clef "valto" R2.*5 R1*8 \original { R2.*5 R1*7 } R2.*11 R1*2 R2.*9
    r2*3/2 r4 do'' la' |
    sib' sol'2 la'4 la' fa' |
    sol'2 la'4 fa'2 sol'4 |
    mi'2 fa'4 re' re' sol' |
    mi' do' fa' fa'2 mi'4( |
    fa') la' sib' sol' la'2 |
    fa'4 fa' sib' la'2 la'4 |
    sol'2 fa'4 mib'2 sol'4 |
    fa' fa' fa' fa'2. fa'1*3/4\fermata |
  }
  \tag #'vtenor {
    \clef "vtenor" R2.*5 R1*8 \original { R2.*5 R1*7 } R2.*11 R1*2 R2.*9
    R2.*2 |
    r2*3/2 r4 fa' re' |
    mib' do'2 re'4 re' sib |
    do'2 la4 sib2 sib4 |
    do' la fa do'2. |
    fa4 fa' fa' mib' mib'2 |
    re'4 re' fa' fa'2 fa'4 |
    re'2 re'4 sib2 mib'4 |
    do' re' re' do'2. re'1*3/4\fermata |
  }
  \tag #'vbasse {
    \clef "vbasse" R2.*5 R1*8 \original { R2.*5 R1*7 } R2.*11 R1*2 R2.*9
    R2.*10 |
    r4 fa re mib do2 |
    re4 re sib, fa2 re4 |
    sol2 re4 mib2 mib4 |
    fa re sib, fa2. sib,1*3/4\fermata |
  }
>>
