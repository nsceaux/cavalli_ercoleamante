<<
  \tag #'voix1 {
    \clef "vsoprano" fad''1. re'' |
    re''1 re''2 re''1 re''2 |
    re''1 re''2 re''1 re''2 |
    mi''1 mi''2 re''1 re''2 |
    re''1 re''2 re''1 re''2 |
    re''1 re''2 re''1 re''2 |
    mi''1 mi''2 re''1 re''2 |
    re''2. si'4 mi''1 re''\fermata |
  }
  \tag #'voix2 {
    \clef "vsoprano" re''1. re'' |
    la'1 la'2 la'1 fad'2 |
    la'1 la'2 la'1 la'2 |
    si'1 la'2 la'1 la'2 |
    la'1 la'2 la'1 fad'2 |
    la'1 la'2 la'1 fad'2 |
    si'1 mi'2 sol'1 la'2 |
    si'2. re''4 la'1 la'\fermata |
  }
  \tag #'voix3 {
    \clef "valto" la'1. fad' |
    fad'1 mi'2 re'1 re'2 |
    fad'1 mi'2 re'1 fad'2 |
    mi'1 mi'2 fad'1 fad'2 |
    fad'1 mi'2 re'1 re'2 |
    fad'1 mi'2 re'1 re'2 |
    \hideNotes mi'1 mi'2 re'1 re'2 \unHideNotes |
    sol'2. sol'4 mi'1 fad'\fermata |
  }
  \tag #'voix4 {
    \clef "vtenor" re'1. re' |
    re'1 re'2 re'1 la2 |
    re'1 re'2 re'1 re'2 |
    re'1 dod'2 re'1 re'2 |
    re'1 re'2 re'1 la2 |
    re'1 re'2 re'1 la2 |
    si1 la2 re'1 re'2 |
    re'2. re'4 re'2 dod'( re'1)\fermata |
  }
  \tag #'voix5 {
    \clef "vtenor" re'1. la |
    la1 la2 la1 re'2 |
    la1 la2 la1 fad2 |
    si1 la2 la1 la2 |
    la1 la2 la1 re'2 |
    la1 la2 la1 re'2 |
    re'1 dod'2 si1 la2 |
    sol2 si la1 la\fermata |
  }
  \tag #'voix6 {
    \clef "vbasse" re'1. re |
    re1 mi2 fad1 re2 |
    re1 mi2 fad1 re2 |
    sol1 la2 re1 re2 |
    re1 mi2 fad1 re2 |
    re1 mi2 fad1 re2 |
    sol1 la2 si1 fad2 |
    sol2. sol4 la1 re\fermata |
  }
>>