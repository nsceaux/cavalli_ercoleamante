\clef "basse" fa1~ |
fa2. mi4 |
fa2~ fa |
re do |
do1 |
fa,2 sol, |
do1 |
la, |
sol,2 mib |
re4~ re~ re2 |
sol,~ sol, |
<<
  \original { sol1 | fa | fa2 sib | sol1 | }
  \pygmalion {
    sol2 sol, | fa, fa |
    la,4 fa, sib,2 |
    sol,1 |
  }
>>
do1 |
re2~ re |
sol,1 |
