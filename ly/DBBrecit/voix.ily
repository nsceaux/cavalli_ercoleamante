\ffclef "vbas-dessus" <>^\markup\character Paggio
r4 r8 la' la' la' la' sol' |
la'4 la'8 sol' sol' sol' sol' la' |
fa'4 fa'
\ffclef "vbasse" <>^\markup\character Ercole
r4 fa |
re8 re re mi do4 do |
\ffclef "vbas-dessus" <>^\markup\character Paggio
r8 sol' sol' sol' do''4 do''8 do'' |
do'' do'' la' do'' sol' sol' r4 |
\ffclef "vbasse" <>^\markup\character Ercole
r4 r8 mi mi mi mi re |
fad fad r la fad!4 r8 sib |
sol4 sol r8 sol sol fad |
la la
\ffclef "vbas-dessus" <>^\markup\character Paggio
r8 la' la' la' do'' re'' |
sib'4 sib'
\ffclef "vbasse" <>^\markup\character Ercole
sib8 sol r4 |
sib8 sib16 sib sib8 sib sib sib sib sib16 la |
do'8 do' r fa la la la sib |
do' do' do' sib re' re' r4 |
r sol sol8 sol sol sol |
mib mib mib mib mib4 mib8 re |
re4 re
\ffclef "vbas-dessus" <>^\markup\character Paggio
re''8. la'16 la'8 sib' |
sol'4 sol' r2 |
