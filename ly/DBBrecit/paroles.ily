Sa -- rà com’ hai dis -- pos -- to
I -- o -- le qui ben tos -- to.

E do -- ve la tro -- vas -- ti?

Nel cor -- til re -- gio a fa -- vel -- lar d’a -- mo -- re.

A fa -- vel -- lar d’a -- mo -- re? con chi? deh dil -- lo,
dell’ a -- mor mi -- o?

Dell’ a -- mor suo con Hyl -- lo.

Co -- me? Dun -- que il mio fi -- glio
mio ri -- va -- le di -- ven -- ne?
A tal te -- me -- ri -- tà sa -- reb -- be ei giun -- to?
Tu non hai ben com -- pre -- so
sem -- pli -- cet -- to gar -- zo -- ne.

Ec -- co -- li à pun -- to.
