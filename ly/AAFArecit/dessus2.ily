\clef "dessus" R1*14 |
r2 mi'' mi''2. mi''4 |
do''2. re''4 do''2 do''~ |
do''4 si'8 la' si'2 do''1\fermata |
