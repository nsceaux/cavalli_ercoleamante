\clef "vsoprano" <>^\markup\character Cinzia
r4 r8 do'' mi''4 mi'' |
mi''4 do''8 si' do''2 |
do''4 do''8 do''8 do''4 do''8 re''8 |
mi''2 mi''8 sol'' mi'' fa'' |
fa''2 fa'' |
r4 fa''8 do'' fa''4 do''8 do'' |
do'' do'' do''4. do''8 fa'' mi'' |
mi''2 mi''4 mi''8 mi'' |
mi''4 re''8 mi'' do''2 |
do''4 do''8 si' re''4 re''8 mi'' |
do''4 do''4. do''8 do'' do'' |
do''4 fa''2 mi''8 re'' |
mi''2( re'') do''1\fermata |
R\breve*3 |
