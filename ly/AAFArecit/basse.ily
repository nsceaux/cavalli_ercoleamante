\clef "basse" do1 |
do~ |
do~ |
do |
fa, |
fa, |
fa, |
do~ |
do4 si, la,2 |
la,2 sol, |
mi,1 |
fa,2 sol,4 fa, |
sol,1 do\fermata |
r2 do' la2. mi4 |
fa2. re4 mi2 do |
sol1 do\fermata |
