\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*3\pageBreak
        s1*5\break s1*5\break s1*5\break s1*4\break s1*2\pageBreak
        s2.*11\break s2.*4 s1*5\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
