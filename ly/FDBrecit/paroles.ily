Co -- sì de -- pos -- ti al -- fin gl’hu -- ma -- ni af -- fet -- ti
co -- sì l’al -- ma pur -- ga -- ta
d’o -- gni re -- a ge -- lo -- sia
ciò che qui giù sde -- gnò, las -- sù las -- sù de -- si -- a.
Quin -- di am -- mor -- za -- ti an -- ch’io gl’an -- ti -- chi sde -- gni
per il vos -- tro go -- de -- re:
a me sì glo -- ri -- o -- so
con -- sen -- tii, ch’e -- gli go -- da in su le sfe -- re
un be -- a -- to ri -- po -- so,
on -- de a com -- pi -- re o -- gni de -- sio ce -- les -- te
sol de’ vos -- tri hi -- me -- nei
sol sol de’ vos -- tri hi -- me -- nei man -- can le fes -- te. __
Su su su dun -- que a i giu -- bi -- li
a -- ni -- me nu -- bi -- li
e fe -- li -- cis -- si -- mi
i miei dol -- cis -- si -- mi
no -- di in -- so -- lu -- bi -- li
al par d’a -- mor __ v’al -- lac -- ci -- no,
e nel -- le vos -- tre des -- tre i cor i co -- ri al -- lac -- ci -- no.
\original {
  Se a pro d’un ve -- ro a -- mo -- rei il gius -- to Gio -- ve
  me -- ra -- vi -- glie non fa,
  a che ri -- ser -- be -- rà sue mag -- gior pro -- ve?
}
