\clef "basse" sol,1~ |
sol,2 do |
do1~ |
do2 sol,~ |
sol, re |
si, do re1 sol, |
re~ |
re2 la, |
la,1~ |
la, |
mi1~ |
mi2 re~ |
re1 mi la, |
la,~ |
la, |
fad, |
sold, |
la,2 si8 la sol fad16 sol |
mi4 fad8 sol la2 re1 |
re2. |
re4 la2 si~ si8 la |
sold2. la2~ la8 \ficta sol |
fad2. sol2~ sol8 fad |
mi2. fad2~ fad8 mi |
red2. mi |
fad sol |
mi |
mi4 fad2 |
si,1 |
sold2 la |
re mi4. re8 |
mi1 la,\fermata |
\original {
  la,1~ |
  la, |
  la, |
  fad, |
  sol,2 fad, |
}
