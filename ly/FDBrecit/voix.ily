\clef "vsoprano" <>^\markup\character Giunone
r4 r8 re'' si' si' si' do'' |
re'' re'' re'' mi'' do''4 do''8 mi'' |
do''4 do''8 do''16 do'' do''8 do'' do'' do'' |
do''4 do''8 si' re''4 re''8 re'' |
sol' sol' sol' fad' la'4 r8 la' |
re''4 r8 si' mi''4 do''8[ si'] la'1 sol' |
la'4 la' re''8 re'' re'' re'' |
la' la' la' si' do''4 do'' |
r mi''8 mi'' mi''4 re''8 mi'' |
do''4 do''8 mi'' la' la' la' sold' |
si' si' mi' mi' si'4 si'8 do'' |
re'' re'' re'' mi'' fa'' fa'' re'' si' |
sold'4 la'8 si' do''2( si'1) la' |
dod''4 dod'' r dod''8 dod'' |
dod''4 dod'' dod''8 dod''16 dod'' dod''8 re'' |
re''4 re'' re''8 la' la' si'16 do'' |
si'8 si' re''4 mi''8 si' si' dod''16 re'' |
dod''8 dod'' fad''[ mi'']\melisma re''8[ dod''16 re''] si'4~ |
si'8\melismaEnd dod'' re''4 re''4. dod''8( re''1) |
la'4 re'' r |
re'' mi'' fad'' re''4. dod''8 si'4 |
re''4. mi''8 si'4 dod''4. si'8 la'4 |
fad'' re'' dod'' si'4. lad'8 si'4 |
mi'' \ficta dod'' mi'' lad'4. sold'8 lad'4 |
si' si' si' si'4. si'8 si'4 |
re'' re'' re'' re''\melisma dod''8[ re'' mi'' fad''] |
sol''4\melismaEnd si'8([ dod''] re''4) |
mi''8([ re''] dod''4.) si'8 |
si'2 r4 re'' |
re''8 re'' re'' dod'' dod''4 dod''8 mi'' |
fad''4 r8 si' dod'' sold' la'4~ |
la'2. sold'4 la'1\fermata |
\original {
  r4 la' mi''8 mi'' mi'' re'' |
  mi''4 mi'' r8 mi'' mi'' mi'' |
  mi'' mi'' mi'' mi'' mi''4 mi''8 fad'' |
  re''4 r8 fad'' re'' re'' re'' re'' |
  si' si' si' mi'' dod'' dod'' r4 |
}
