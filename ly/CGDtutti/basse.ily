\clef "basse" r4 fa do' la |
sib sol la2 |
la4 fa sol4. mi8 |
fa2 sol |
do1 |
\clef "alto" r2*3/2 r4 do' do' |
fa'2 fa'4 |
re'2 re'4 sol'2 sol'4 |
mib'2 mib'4 fa'2 mib'4 |
re'2 do'4 sib re' mib' |
\clef "basse" fa2. sib |
sol do' |
la re' |
sib |
mib'2 sib4 |
do'2. la sib2 sib4 |
do'2 sib4 la2 sol4 |
fa la sib do'2. fa1*3/4\fermata |
