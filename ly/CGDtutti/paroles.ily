Le ru -- gia -- de più pre -- zio -- se
tuoi pa -- pa -- ve -- ri ogn’ or ba -- gni -- no,
\tag #'(giunone basse pasithea aura2) {
  e per tut -- to gi -- gli, e ro -- se
  co’ lor a -- li -- ti t’ac -- com -- pa -- gni -- no,
}
e per tut -- to gi -- gli, e ro -- se
co’ lor a -- li -- ti
\tag #'(giunone basse aura2) { co’ lor a -- li -- ti }
\tag #'(aura1 ruscello) {
  co’ lor a -- li -- ti co’ lor a -- li -- ti
}
t’ac -- com -- pa -- gni -- no.
