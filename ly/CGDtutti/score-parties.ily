\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \keepWithTag #'giunone \includeNotes "voix"
    >> \keepWithTag #'giunone \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'pasithea \includeNotes "voix"
    >> \keepWithTag #'pasithea \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'aura1 \includeNotes "voix"
    >> \keepWithTag #'aura1 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'aura2 \includeNotes "voix" \clef "alto"
    >> \keepWithTag #'aura2 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'ruscello \includeNotes "voix" \clef "alto"
    >> \keepWithTag #'ruscello \includeLyrics "paroles"
    %\new Staff << \global \includeNotes "basse" >>
  >>
  \layout { }
}
