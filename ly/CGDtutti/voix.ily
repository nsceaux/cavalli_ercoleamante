<<
  \tag #'(giunone basse) {
    \clef "vsoprano" r4 fa''8 fa'' mi''4 fa'' |
    re'' mi'' do''2 |
    do''4 fa''8 fa'' re''4 re''8 mi'' |
    do''4 fa'' re''4. re''8 |
    do''1 |
    R1. |
    r4 do'' do'' |
    fa''2 fa''4 re''2 re''4 |
    mib''2 mib''4 r do'' do'' |
    re''2 mib''4 fa'' mib'' re'' |
    do''2 do''4 sib'2. |
    R2.*2 |
    r4 do'' do'' fa''2 fa''4 |
    re''2 re''4 |
    sol''2 sol''4 |
    r4 do'' do'' fa''2 fa''4 re'' re'' re'' |
    mi''2 re''4 do'' re'' mi'' |
    fa''2.~ fa''2 mi''4 fa''1*3/4\fermata |
  }
  \tag #'pasithea {
    \clef "vsoprano" r4 do''8 do'' do''4 do'' |
    sib' sib' la'2 |
    la'4 do''8 do'' si'4 si'8 do'' |
    la'4 do'' do''4. si'8 |
    do''1 |
    r4 sol' sol' do''2 do''4 |
    la'2 la'4 |
    re''2 re''4 r sib' sib' |
    do''2\melisma sib'4 la'2\melismaEnd sol'4 |
    fa'4 sib' do'' re''\melisma do'' sib'~ |
    sib'2\melismaEnd la'4 sib'2. |
    R2.*2 |
    r2*3/2 r4 fa' fa' |
    sib'2 sib'4 |
    sol'2 sol'4 |
    do''2 do''4 r do'' do'' sib'2 sib'4 |
    sol' la' sib' do''2.~ |
    do''2\melisma sib'8[ la'] sol'2\melismaEnd sol'4 la'1*3/4\fermata |
  }
  \tag #'aura1 {
    \clef "vsoprano" r4 la'8 la' sol'4 la' |
    fa' sol' mi'2 |
    mi'4 la'8 la' sol'4 sol'8 sol' |
    fa'4 la' sol'4. sol'8 |
    sol'1 |
    R2.*9 |
    r2*3/2 r4 fa' fa' |
    sib'2 sib'4 sol'2 sol'4 |
    do''2 do''4 r la' la' |
    re''2 re''4 |
    sib'2. |
    r4 sol' sol' la'2 la'4 fa' fa' fa' |
    do''2\melisma sol'4 la'2\melismaEnd mi'4 |
    la' la' re'' do''2 do''4 do''1*3/4\fermata |
  }
  \tag #'aura2 {
    \clef "valto" r4 do'8 do' mi'4 do' |
    re' sib do'2 |
    do'4 do'8 la re'4 re'8 do' |
    do'4 la re'4. re'8 |
    mi'1 |
    r2*3/2 r4 do' do' |
    fa'2 fa'4 |
    re'2 re'4 sol'2 sol'4 |
    r mib' mib' fa'2\melisma mib'4 |
    re'2\melismaEnd do'4 sib re' mib' |
    fa'2 fa'4 sib re' re' |
    sol'2 sol'4 mi'2 mi'4 |
    la'2 la'4 r2*3/2 |
    r4 fa' fa' |
    sol'2 sol'4 |
    mib'2. r4 do' do' re'2 re'4 |
    do' do' re' fa'2\melisma sol'4 |
    do' do' re' sol'2\melismaEnd sol'4 fa'1*3/4\fermata |
  }
  \tag #'ruscello {
    \clef "vtenor" r4 fa8 fa do'4 la |
    sib sol la2 |
    la4 fa8 fa sol4 sol8 mi |
    fa4 fa sol4. sol8 |
    do'1 |
    R2.*9 |
    r4 fa fa sib2 sib4 |
    sol2 sol4 do'2 do'4 |
    r4 la la re'2 re'4 |
    sib sib sib |
    mib'2 sib4 |
    do'2. r2*3/2 r4 sib sib |
    do'2\melisma sib4 la2\melismaEnd sol4 |
    fa la sib do'2 do'4 fa1*3/4\fermata |
  }
>>
