\clef "vbas-dessus" <>^\markup\character Venere
r4 r8 fa' fa' fa' fa'4 |
fa'8 fa'16 fa' fa'8 mib' sol' sol' r sol' |
sol'4 sol' la' la'8 sib' |
sib'4 sib'8 sib' re'' re'' re'' mi'' |
fa'' do'' fa''16[\melisma mib'' re'' do''] sib'8\melismaEnd mib'' do''16[ sib' do'' re''] sib'1\fermata |

