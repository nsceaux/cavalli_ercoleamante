\clef "dessus" fa''4. mi''8 re''4. do''8 |
sib'4 sib' sol'4. sol'8 |
la'4 do'' do''4. si'8 do''1 |
r2 do'''4. sol''8 |
la''4. mi''8 fa''4. do''8 |
re''4 fa'' mi''4. fa''8 |
re''8 mi'' fa''4 fa''4. mi''8 fa''1\fermata |
