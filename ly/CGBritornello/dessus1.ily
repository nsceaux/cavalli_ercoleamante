\clef "dessus" la''4. sol''8 fa''4. mi''8 |
re''4 fa'' mi''4. mi''8 |
re''4 mi'' fa''4. mi''8 mi''2 sol''4. re''8 |
mi''4. si'8 do''4. mi''8 |
la'4. sol'8 re''4. do''8 |
fa''4 la'' sol''4. la''8 |
fa''4 la'' sol''4. la''8 la''1\fermata |
