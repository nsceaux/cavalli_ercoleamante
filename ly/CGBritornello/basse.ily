\clef "basse" fa4. do8 re4. la,8 |
sib,4 re do4. do8 |
fa4 mi re2 do1 |
do'4. sol8 la4. mi8 |
fa4. do8 re4. la,8 |
sib,4 fa, do4. la,8 |
sib,4 fa, do2 fa,1\fermata |
