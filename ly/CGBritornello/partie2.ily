\clef "viole2" do'4. do'8 la4. la8 |
re'4 re' sol4. do'8 |
re'4 sol re'2 sol1 |
r2 la'4. sol'8 fa'4. sol'8 re'4. fa'8 |
sib4 do' do'4. do'8 |
re'4 do' do'2 do'1\fermata |
