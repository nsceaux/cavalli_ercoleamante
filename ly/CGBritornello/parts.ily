\piecePartSpecs
#`((dessus)
   (parties)
   (violes #:score "score-violes")
   (basse #:score-template "score-basse-continue")
   (silence #:on-the-fly-markup , #{ \markup\tacet #}))
