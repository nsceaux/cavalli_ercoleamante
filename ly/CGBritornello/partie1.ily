\clef "viole1" fa'4. sol'8 re'4. fa'8 |
fa'4 fa' do''4. do''8 |
fa'4 sol' la' fa' sol'1 |
sol'4. sol'8 mi'4. do'8 |
do'4. mi'8 la'4. la'8 |
fa'4 fa' sol'4. fa'8 fa'4 fa' sol'2 fa'1\fermata |
