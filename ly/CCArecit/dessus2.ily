\clef "dessus" re''8 re'' |
do''4 sib' sib' la' |
sib'8 do'' re'' sib' mib'' re'' do'' sib' |
fa''4 do'' r8 fa'' mib'' re'' |
do''4. fa'8 sol'4 fa' |
fa'2 r |
r4 r8 re' sol'4 r |
R1 |
r4 r8 do' re' mib' fa'4 |
R1 |
r4 r8 re'16 mi' fad'8 re' r4 |
r sol'16 la' sib'8 r4 fad'16 sol' la'8 |
r4 sol'16 la' sib' do'' re''4. sib'8 |
R1*2 |
r2 r4 r8 sol' |
re' mib' fa' do' r2 |
r r8 fa' re'4 |
R1 |
r8 fa' do'16 re' mib'8 re' do' r4 |
R1*4 |
r4 re''8 si'16 do'' re''8 sol' r4 |
R1*3 |
r4 sib'8 sib' sib'4 re''8 do'' |
sib'4. la'8 sib' fa' sib' la' |
sol'4 fa' sol' fa' |
fa'4 r re'2~ |
re'4 do' do'2 |
r r4 r8 fa' |
re'4. do'8 do'2~ |
do'4 r r r8 do'' |
sib'4 r8 do' do'4 r |
R1 |
r2 r4 do' |
re'4 r r2 |
R1*6 |
r2 r4 re''8 re'' |
do''4 sib' sib'4. la'8 |
sib'16 la' sib' do'' re''8 sib' mib'' re'' do'' sib' |
fa''4 do'' r16 re'' sol'' fa'' mib'' re'' do'' sib' |
fa''4. mib''8 re'' do''16 sib' la'4 |
sib'1\fermata |
