\score {
  \new StaffGroupNoBar \with { \haraKiriFirst } <<
    \pygmalion\new StaffGroupNoBracket <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
      \new Staff << \global \includeNotes "partie1" >>
      \new Staff << \global \includeNotes "partie2" >>
    >>
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \original\origLayout {
        s1*5\break s1*6\break s1*5\break s1*5\break s1*7\pageBreak
        s1*5\break s1*5\break
      }
      \pygmalion\origLayout {
        s4 s1*4\pageBreak
        s1*4\break s1*5\pageBreak
        s1*4\break s1*4\pageBreak
        s1*4\break s1*5\pageBreak
        s1*4\break s1*4\pageBreak
        s1*4\break s1*4\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
