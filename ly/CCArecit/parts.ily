\piecePartSpecs
#(if (eqv? #t (ly:get-option 'pygmalion))
     '((dessus #:score-template "score-dessus-voix")
       (parties #:score-template "score-parties-voix")
       (basse #:score-template "score-voix"))
     '((basse #:score-template "score-voix")))
