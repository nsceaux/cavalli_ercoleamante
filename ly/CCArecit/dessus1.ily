\clef "dessus" fa''8 fa'' |
mib''4 re'' do''8 re'' mib'' do'' |
re'' mib'' fa'' re'' sol'' fa'' mib'' re'' |
do''4 fa''8 mib'' re''4 do''8 sib' |
la'4. sib'8 do'' sib' sib' la' |
sib'2 r |
r8 sib' fa'4 r2 |
R1 |
r8 do' fa'4 r2 |
R1 |
r4 la'16 sol' fad' mi' re'4 r8 la' |
sol'16 la' sib'8 r4 la'16 sib' do''8 r4 |
r8 sol'16 la' sib' do'' re''4 sol'8 r4 |
R1*2 |
r2 r8 re' re' mib' |
fa' sol' la'4 r2 |
r8 sol' mi'4 r2 |
R1 |
r4 fa'8 sol' la'4 r |
R1*4 |
r8 re'' si'16 do'' re''8 sol'4 r | \allowPageTurn
R1*3 |
r4 fa''8 fa'' sol''4 fa'' |
mib'' do'' re''8 fa'' mib'' re'' |
do''4 sib' sib' la' |
sib'4 r r fa'~ |
fa'8 sol' mi'4 fa'2 |
r r8 sol' fa'16 sol' lab'8 |
sol'4 re' mib'2~ |
mib'4 r r8 sol' la'4 |
r8 re' mib'4 r2 |
R1 |
r2 r8 re' mib'4 |
re'4 r r2 |
R1*6 |
r2 r4 fa''8 fa'' |
mib''4 re'' do''16 sib' do'' re'' mib''8 do'' |
re''16 do'' re'' mib'' fa''8 re'' sol'' fa'' mib'' re'' |
do'' re''16 mib'' fa''8 mib'' re''16 fa'' mib'' re'' do'' sib' la' sol' |
la' fa' sol' la' sib' do'' re'' mib'' fa''8 mib''16 re'' do''8. do''16 |
re''1\fermata |
