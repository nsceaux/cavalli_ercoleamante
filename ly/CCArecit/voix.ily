\clef "vsoprano"
\pygmalion { r4 R1*4 }
<>^\markup\character Paggio
r8 sib' sib' fa' sol'4 fa'8. mib'16 |
re'2 re'4 fa'8 fa' |
sol'4 la' sol'4. do''8 |
la'2 la'4 sib'8 la' |
sol'4 fad' sol' la' |
fad'2 fad'4 sol'8 la' |
sib'4 sol' sol'4. fad'8 |
sol'2 sol'4 sib'8 re'' |
sol'4 sib' sib'4. do''8 |
re''8 do'' sib' la' sol'4 sib' |
sib'4. la'8 sib'2 |
sib'4 fa'8 fa' sol'4 la' |
sib'4. do''8 la' la' la' sib' |
do''4 re'' sib'4. la'8 |
la'4 la' r la'8 sib' |
do''4 do'' do''4. si'8 |
do'' do'' sib' la' sol'4 fad' |
sol'4. la'8 fad' fad' sol' la' |
sib'4 sol' sol'4. fad'8 |
sol'2 sol'4 sib'8 re'' |
sol'4 sib' sib'4. do''8 |
re'' do'' sib' la' sol'4 sib' |
sib'4. la'8 sib'2 |
sib' r |
R1*2 |
r4 fa'8 fa' fa'4 fa' |
sib' sib'8 la' la'4 la' |
r4 la'8 sib' sol'4 do'' |
si'4. do''8 do''4. do''8 |
do''4 sib'8 la' sol'4 fad' |
sol' la' fad'4. fad'8 |
fad'4 sol'8 la' re'4 sol' |
sol'4. fad'8 sol'4 sol' |
r4 sib'8 la' sol'4 sib' |
mib'4. re'8 re' do' re' \ficta mi' |
fa'4 fa' fa'4. sol'8 |
la' sol' la' sib' do''4 re'' |
do''4. sib'8 la' fa' sol' la' |
sib' do'' re''4 do''2 |
re''8 do'' sib' la' sol' la' sib'4~ |
sib' la' sib' sib' |
\pygmalion R1*5
