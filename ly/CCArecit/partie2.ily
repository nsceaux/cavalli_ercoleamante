\clef "viole2" sib4 |
sol re' sol do' |
sib2 sib4. sol8 |
do'4 do' re'4. sol8 |
do'4. sib8 mib' sol' do'4 |
re'2 sol4 la |
sib2 re'4 r8 la |
sib4 do'2 do'4 |
do' la2 re'8 do' |
sib4 re'8 do' sib sol mib'4 |
re'2 la4. fad8 |
sol4 sib la4. la8 |
sol re' sib!2 sib4 |
sib4. sib8 sol4 la |
re'4 fa' sol' fa'8 re' |
sol'4 fa'8 mib' re'2 |
re'4 do' sib do' |
re' do'2 sib4 |
fa' re' sol' do' |
la16 sib do'8 fa4. sol8 la fa |
sol4 la sol8 sol' re'4 |
sol sib4. do'8 la re' |
sib8 sol do'4 la8 do' sib la |
sol4 do'8 sib la4 re' |
re'2 sib!4. sib8 |
sib4. fa8 sol4 la |
re'4 fa' sol' fa' |
fa'8 mib' re' do' re' sib fa'4 |
sib fa' mib' fa' |
sib8 sol do'4 sib4. re'8 |
sol4 re' do' fa' |
sib2 re'4 do' |
sib sol la4. la8 |
fa4 la do'8 sib lab do' |
sol2 sol4. la8 |
mib'4 sib8 do' sib do' la4 |
r8 sib do'4 r8 fad la4 |
re4 mib fa8 sib sol la |
sib do' sib la sol2 |
sol'4 fa' mib' sib~ |
sib la sib2 |
sib4 do' re' do' |
fa' mi'8 re' do'4 fa' |
do'4. sol8 la sib sol4 |
fa sib4. sib8 la4 |
sib4 sib do' sib |
sol do' sib sib |
sol4 re' sol do' |
sib2 sib16 la sib do' re'8 sib |
do'4 r16 la sib do' fa4 sib |
la8 sib16 do' re'8 la fa re'16 do' do'4 |
sib1\fermata |
