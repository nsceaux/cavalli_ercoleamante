E che co -- sa è quest’ a -- mo -- re?
Di cui par -- lan tan -- to in cor -- te,
e can -- zon di mil -- le sor -- te
di lui can -- ta -- no a tutt’ ho -- re.
E -- gli è qual -- che ciur -- ma -- do -- re
e -- gli è qual -- che ciur -- ma -- do -- re
poi ch’a quel, che sen -- to di -- re
sen -- za pun -- to in -- ten -- der co -- me
men -- tre a stil -- le dà il gio -- i -- re
el pe -- nar dis -- pen -- sa a so -- me,
fas -- si il mon -- do a -- do -- ra -- to -- re
e -- gli è qual -- che ciur -- ma -- do -- re
e -- gli è qual -- che ciur -- ma -- do -- re.
Di ve -- der -- lo eb -- bi gran bra -- me
ma poi sep -- pi, ch’è im -- pos -- si -- bi -- le,
ch’e -- gli sia già mai vi -- si -- bi -- le
per -- ché sem -- pre è con le da -- me,
e che que -- ste al fin -- ger dot -- te
se lo ten -- ga -- no ce -- la -- to,
co -- me s’ei stes -- se ap -- piat -- ta -- to
den -- tro le cim -- me -- rie grot -- te
den -- tro le cim -- me -- rie grot -- te.
