\clef "viole1" do''4 sol' do' sol'2. |
mi'4 si re' sol2. |
la4 do' re' sol si do' re'2. |
do'4 fa' re' la' sol'8 fa' mi'4 |
sol'4 fa' re' sol'4. fa'8 mi'4 |
la' sol'2 sol'1*3/4\fermata |
