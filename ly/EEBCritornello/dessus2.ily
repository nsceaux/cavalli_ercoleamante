\clef "dessus" mi''4 re'' do'' si'2. |
mi''4 mi'' re'' mi'' re''8 do'' si'4 |
la' sol' fa' mi' sol' fad' sol'2. |
sol'4 la' si' do''2. |
si'4 do'' re'' mi'' mi''8 fa'' sol''4 |
la'' re''4. re''8 mi''1*3/4\fermata |
