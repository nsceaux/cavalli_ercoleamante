\set Score.currentBarNumber = 41 \bar ""
\key do \major \midiTempo#160
\beginMark "Ritor[nello]" \digitTime\time 3/4 \measure 6/4 s1.*2
\measure 9/4 s2.*3
\measure 6/4 s1.*3 \bar "|."
