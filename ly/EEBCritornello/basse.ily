\clef "basse" do'4 si la sol2. |
la4 sol fa mi2. |
fa4 mi re do si, la, sol,2. |
mi,4 fa, sol, la,2. |
sol,4 la, si, do4. re8 mi4 |
fa sol2 do1*3/4\fermata |
