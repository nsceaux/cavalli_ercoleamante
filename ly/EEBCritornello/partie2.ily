\clef "viole2" sol'4 re' la' re'2. |
la'4 mi' la mi'2. |
do'4 mi' si mi re do sol2. |
sol4 re' si mi'4. re'8 do'4 |
mi' la sol do'2. |
fa'8 mi' re'4. re'8 do'1*3/4\fermata |
