\clef "dessus" sol''4 sol'' fad'' sol'' fad''8 mi'' re''4 |
do'' si' la' sol'2. |
do''4 do'' si' do'' sol' la' si'2. |
mi''4 re'' sol'' mi'' fa''8 sol'' la''4 |
mi'' fa'' sol'' do'' si'8 la' sol' mi'' |
do''4 do''4. si'8 do''1*3/4\fermata |
