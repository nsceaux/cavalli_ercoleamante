\clef "viole1" sib'2. sol'4 fa'2 sib' la'1\fermata |
la'2. sib'4 do''2 sib'4 re'' do''1 do''\fermata |
sib'2. fa'4 sib'2 lab' sol'1\fermata |
sol'2. re'4 sol' mib' fa' sol' |
fa'\breve fa'1\fermata |
R2.*8 |
r4 r sib |
fa' re'2 mib'4 fa'2 |
sib4. do'8 re' mi' fa'4. sol'8 fa' mib' |
re'4 sol'2 fa'4 fa'2 |
sol'2 sol'4 fa' do'2 sol' sol'4 |
do'2 do'4 re'2 fa'4 |
fa' fa'2 sol'4 fa'2 |
fa' sib'4 fa'2 fa'4 |
do' fa'2 fa'1*3/4\fermata |
