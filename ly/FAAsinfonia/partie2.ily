\clef "viole2" re'2. sol4 re'2 sol do'1\fermata |
fa'2. re'4 do'2 re' sol'1 fa'\fermata |
fa'2. do'4 sol' sol re'2 sib1\fermata |
sib2. fa'4 do'4. la8 re'4 mib' |
do'2 sib do'1 sib\fermata |
R2.*11 |
r2*3/2 r4 r fa |
sib4 sol2 lab4 sib2 |
mib4. fa8 sol la sib4 la2 sol2. |
fa2 fa4 sib2 sib4 |
do' re'2 do'4 do'2 |
sib4 re'2 do' sib4 |
sol do'2 sib1*3/4\fermata |
