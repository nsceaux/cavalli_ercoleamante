\score {
  \new StaffGroup <<
    \new Staff << \global \includeNotes "dessus1" >>
    \new Staff << \global \includeNotes "dessus2" >>
    \new Staff << \global \includeNotes "partie1" >>
    \new Staff << \global \includeNotes "partie2" >>
    \new Staff <<
      \global \includeNotes "basse"
      \origLayout {
        s1*15\pageBreak
        s2.*13\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
