\clef "basse" sib,2. do4 re2 mi fa1\fermata |
fa2. sol4 la2 sib do'1 fa\fermata |
sib2. la4 sol2 fa mib1\fermata |
mib2. re4 do2 sib, |
fa\breve sib,1\fermata |
R2.*18 |
r2*3/2 r4 r sib, |
fa re2 mib4 fa2 |
sib,4. do8 re mi fa4. mib8 re4 |
mib fa2 sib,1*3/4\fermata |
