\clef "dessus" re''2. do''4 sib'2 sol'4 mi'' do''1\fermata |
la''2. sol''4 fa''2 fa''4 fa''2 mi''8 re'' mi''2 fa''1\fermata |
re''2. do''4 mib''2 fa'' sol''1\fermata |
mib''2. sib'4 mib'' do'' re'' sib' |
do''2 re'' do''1 re''\fermata |
R2.*4 |
r4 r fa' |
sib' sol'2 lab'4 sib'2 |
mib'4. fa'8 sol' la' |
sib'4. do''8 sib' do'' |
la'4 sib'2 sol'4 la'2 |
sib'4. re''8 do'' sib' la'4. sib'8 do'' la' |
sib'4 sib'2 do''4 sib'2 |
sib'4. la'8 sib' do'' sib'4 do''2 sib'4 re'' sib' |
la'2 la'4 sib'2 sib'4 |
la' sib'2 sib'4 la'2 |
sib' sib'4 la' sib'8 do'' re''4 |
do''8 sib' sib'4. la'8 sib'1*3/4\fermata |
