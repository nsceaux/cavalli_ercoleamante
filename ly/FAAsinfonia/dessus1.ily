\clef "dessus" fa''2. mib''4 re''2 sol'' fa''1\fermata |
do'''2. sib''4 la''2 sol'' sol''1 la''\fermata |
fa''2. fa''4 sol''2 re'' mib''1\fermata |
sol''2. fa''4 sol'' la'' sib'' sol'' |
la'' fa'' sib''1 la''2 sib''1\fermata |
r4 r sib' |
fa'' re''2 mib''4 fa''2 |
sib'4. do''8 re'' mi'' |
fa''4. sol''8 fa'' mib'' |
re''4 mib''2 fa''4 fa''4. mib''!16 fa'' |
sol''4. fa''8 mib'' fa'' |
re''4. mib''8 re'' mib'' |
do''4 fa''2 mib''4 do''2 |
re''4. fa''8 mi'' re'' do''4. sib'8 la' do'' |
re''4 mib''2 mib''4 re''2 |
mib''4. re''8 mib'' fa'' re''4 fa''2~ fa''4 sib'8 do'' re'' \ficta mi'' |
fa''4 mib''2 re'' re''4 |
do'' fa''2 mib''4 do''2 |
re''4. mi''8 fa'' sol'' do''4 re''8 mib'' fa''4 |
sol'' fa''4. mib''8 re''1*3/4\fermata |
