\piecePartSpecs
#`((dessus)
   (violes #:music , #{
\addQuote "FAAbasse" { \includeNotes "dessus1" }
s1*15
\cueDuringWithClef "FAAbasse" #CENTER "treble" {
  \mmRestDown
  s2.*8
  \mmRestCenter
}
          #})
   (parties #:music , #{
\addQuote "FAAbasse" { \includeNotes "dessus1" }
s1*15
\cueDuringWithClef "FAAbasse" #CENTER "treble" {
  \mmRestDown
  s2.*8
  \mmRestCenter
}
          #})
   (basse #:music , #{
\addQuote "FAAbasse" { \includeNotes "dessus1" }
s1*15
\cueDuringWithClef "FAAbasse" #CENTER "treble" {
  \mmRestDown
  s2.*19
  \mmRestCenter
}
          #})
   (silence #:on-the-fly-markup , #{ \markup\tacet #}))
