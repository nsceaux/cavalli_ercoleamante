\key fa \major
\time 4/4 \midiTempo#240
\measure 3/1 s1*3
\measure 4/1 s1*4
\measure 3/1 s1*3
\measure 2/1 s1*2
\measure 3/1 s1*3 \bar "|."
\segnoMark \digitTime\time 3/4 \midiTempo#160 s2.
\measure 6/4 s1.
\measure 3/4 s2.*2
\measure 6/4 s1.
\measure 3/4 s2.*2
\measure 6/4 s1.*3
\measure 9/4 s2.*3
\measure 6/4 s1.*4 \bar ":|."
\endMarkSmall\markup\right-column {
  Un austre fois
  \smaller\line { e poi segue la sinf[onia] }
}
