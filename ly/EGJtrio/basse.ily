\clef "basse"
\tag #'basse-continue <>_\markup\tiny { [Source : partie d’une autre main] }
r4 r do' |
si2 la4 sol fa mi |
re2. do |
<<
  \tag #'basse {
    \clef "tenor" sol2. re |
    la si |
    do'2 re'4 mi'2. |
    mi'2 mi'8 re' |
    do'2 si4 |
    la2 sol'4 |
    fa'2 mi'4 |
    re'2 do'4 |
    si2 la4 |
    mi2. la2 la'4 |
    sol'2. |
    fa' |
    mi' |
    la2 la4 |
    si2. |
    do' |
    fa'4 sol' fa' sol'2. |
    \clef "bass"
  }
  \tag #'basse-continue {
    sol2. re |
    la2 la,4 si,2. |
    do2 re4 mi2. |
    mi2 mi8 re |
    do2 si,4 |
    la, la sol |
    fa2 mi4 |
    re2 do4 |
    si,2 la,4 |
    mi2. la,2 la4 |
    sol2. |
    fa |
    mi |
    la,2 la,4 |
    si,2. |
    do2 do4 |
    fa sol fa sol2. |
  }
>>
do2 la,4 sib,2 sol,4 |
la,2 fa,4 sol,2 mi,4 fa, sol,2 |
do do'4 si2 la4 |
sol fa mi re2. |
do sol | \pygmalion\allowPageTurn
re la |
<<
  \tag #'basse {
    si2. do'2 re'4 |
    \clef "tenor" mi'2. mi'2 mi'8 re' |
    do'2 si4 |
    la2 sol'4 fa'2 mi'4 |
    re'2 do'4 si2 la4 |
    mi2. la |
    sol2 sol'4 fa'2 fa'4 |
    mi'2 mi4 la2 la4 |
    si2. |
    do'2 do'4 |
    fa' sol' fa' sol'2. |
    \clef "bass"
  }
  \tag #'basse-continue {
    si2 si,4 do2 re4 |
    mi2. mi2 mi8 re |
    do2 si,4 |
    la, la sol fa2 mi4 |
    re2 do4 si,2 la,4 |
    mi2. la,2 la4 |
    sol2 sol4 fa2 fa4 |
    mi2. la,2 la,4 |
    si,2 si,4 |
    do2 do4 |
    fa sol fa sol2. |
  }
>>
do2 la,4 sib,2 sol,4 la,2 fa,4 |
sol,2 mi,4 fa, sol,2 do1*3/4\fermata |
