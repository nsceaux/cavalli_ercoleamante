\clef "dessus" r4 r la'' |
sol''2 fa''4 mi'' re'' do'' |
do''2 si'4 do''2. |
r2*3/2 r4 fa''2 |
r4 la''2 r4 sol''4. sol''8 |
la''4 la'' si'' si''2. |
si''2 sold''8 sold'' |
la''4. la''8 si'' sold'' |
do'''4 fa'' sol''8 sol'' |
re''2 mi''8 mi'' |
fa''2 la''8 la'' |
si''4 si'' do''' |
r4 r re''8 do'' do''2. |
r4 r si''8 la'' |
la''2 la''8 sold'' |
sold''2 sold''8 sold'' |
la''4. la''8 mi'' do''' |
si''4 si'' si''8 si'' |
sol''2 do'''8 do''' |
do'''4 si'' la'' sol''2. |
sol''8 sol'' fa'' mi'' fa''4 r8 fa'' mi'' re'' mi''4 |
r8 mi'' re'' do'' re''4 r8 re'' do'' si' do'' sol'' fa''[ mi''] re''4. mi''8 |
mi''2 la''4 sol''2 fa''4 |
mi'' re'' do'' do''2 si'4 |
do''2. r2*3/2 |
r4 fa''2 r4 la''2 |
r4 sol''4. sol''8 la''4 la'' si'' |
si''2. si''2 sold''8 sold'' |
la''4. la''8 si'' sold'' |
do'''4 fa'' sol''8 sol'' re''2 mi''8 mi'' |
fa''2 la''8 la'' si''4 si'' do''' |
r r re''8 do'' do''4 do'' fa'' |
sol''4 sol'' si''8 la'' la''4 la'' la''8 si'' |
sold''4 sold'' sold''8 sold'' la''4. la''8 mi'' do''' |
si''4 si''8 si'' si'' si'' |
sol''4 sol'' do'''8 do''' |
do'''4 si'' la'' sol''2. |
r8 sol'' fa'' mi'' fa''4 r8 fa'' mi'' re'' mi''4 r8 mi'' re'' do'' re''4 |
r8 re'' do'' si' do'' sol'' fa'' mi'' re''4. mi''8 mi''1*3/4\fermata |
