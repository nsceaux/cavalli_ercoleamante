<<
  \tag #'(iole basse) {
    \clef "vsoprano" <>^\markup\character Iole
    R2.*3 |
    r2*3/2 r4 do'' do'' |
    do'' si' si' si' la'2 |
    r4 mi''2 r4 re''4. re''8 |
    do''4 do'' do''8[ si'] si'2. |
    si'2 si'8 si' |
    mi''4. mi''8 re'' mi'' |
    fa''4 fa'' mi''8 mi'' |
    re''2 do''8 do'' |
    sib'2 la'8 la' |
    re''4 re''4. mi''8 |
    do''4( si'2) la' fa''8 mi'' |
    mi''2. |
    R2. |
    r4 r re''8 do'' |
    do''2. |
    r4 r fa''8 mi'' |
    mi''2.~ |
    mi''4 re'' mib'' mib''( re''2) |
    do''2. r2*3/2 |
    R2.*7 |
    r4 do'' do'' do'' si' si' |
    si' la'2 r4 mi''2 |
    r4 re''4. re''8 do''2 si'4 |
    si'2. si'2 si'8 si' |
    mi''4 mi'' re'' |
    fa'' fa'' mi''8 mi'' re''4 re'' do''8 do'' |
    sib'4 sib' la' re'' re'' r8 mi'' |
    do''4( si'2) la' fa''8 mi'' |
    mi''4 mi'' mi'' mi'' re'' re''8 re'' |
    re''4 re''4. do''8 do''2 do''4 |
    r8 re'' re'' sol'' fa'' mi'' |
    mi''4 mi''8 mi'' mi'' mi'' |
    mi''4 re''( mib'') mib''( re''2) |
    do''1*3/4\fermata r2*3/2 r |
    R2.*3 |
  }
  \tag #'deianira {
    \clef "vsoprano" <>^\markup\character Deianira |
    R2.*3 |
    r4 fa' fa' fa' mi' mi' |
    mi' re'2 r4 re''2 |
    r4 do''4. do''8 si'2 r8 si' |
    la'4 la' la'8[ sold'] sold'2. |
    sold'2 r4 |
    R2. |
    r4 r mi'8 mi' |
    la'4. la'8 sol' la' |
    fa'4 fa' mi'8 mi' |
    fad'4 sold' la' |
    la'2 sold'4( la'2.) |
    r4 r mi'8 mi' |
    la'4. la'8 sold' la' |
    si'4 si' r |
    r r la'8 sol' |
    sol'2. |
    r4 r sol'8 sol' |
    la'4 si' do'' do''2 si'4( |
    do''2.) r2*3/2 |
    R2.*5 |
    r2*3/2 r4 fa' fa' |
    fa' mi' mi' mi' re'2 |
    r4 re''2~ re''4 do''4. do''8 |
    si'2 si'4 la'2 la'8[ sold'] |
    sold'2. sold' |
    R2. |
    r4 r mi'8 mi' la'4 la' sol' |
    fa' fa' mi'8 mi' fad'4 sold' la' |
    la'2 sold'4( la'2.) |
    r4 r mi'8 mi' la'4 la'4. sold'8 |
    si'4 si' si'8 la' la'4 la' la' |
    la' sol'8 sol' sol' sol' |
    sol' sol' do''4 do''8 sol' |
    la' la' si' sol' do''4~ do''2 si'4( |
    do''1*3/4)\fermata r1*3/4 r |
    R2.*3 |
  }
  \tag #'licco {
    \clef "valto" <>^\markup\character Licco
    R2.*5 |
    r4 sol' sol' sol' fa' fa' |
    fa' mi'2 r4 si4. si8 |
    do'4 do' re' mi'2. |
    mi'2 mi'8 re' |
    do'4. do'8 si do' |
    la4 la sol'8 sol' |
    fa'2 r4 |
    r r do'8 do' |
    si4 si la |
    mi'2. la |
    r4 r sol'8 sol' |
    fa'2 fa'8 mi' |
    mi'2. |
    r4 r la8 la |
    si4. si8 si do' |
    do'4 do' do'8 do' |
    fa'4 sol' fa' sol'2. |
    do' r2*3/2 |
    R2.*7 |
    r2*3/2 r4 sol' sol' |
    sol' fa' fa' fa' mi'2 |
    si4 si4. si8 do'2 re'4 |
    mi'2. mi'2 mi'8 re' |
    do'4 do' si |
    la la sol'8 sol' fa'4 fa' r |
    r r do'8 do' si4 si la |
    mi'2. la |
    R2.*2 |
    r2*3/2 r4 r8 la la la |
    si4 si8 si si si |
    do'4 do'8 do' do' do' |
    fa'4 sol'( fa') sol'2. |
    do'1*3/4\fermata r r |
    R2.*3 |
  }
>>