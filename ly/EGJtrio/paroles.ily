\tag #'(iole basse) {
  U -- na stil -- la di spe -- ne
  oh oh che mar di dol -- cez -- za!
  per un’ a -- ni -- ma av -- vez -- za
  a lan -- guir a lan -- guir sem -- pre sem -- pre in pe -- ne
  a lan -- guir a lan -- guir a lan -- guir __ sem -- pre in pe -- ne.
  U -- na stil -- la di spe -- ne,
  ben -- ché tal’ or men -- ti -- ta
  nel -- le già fred -- de ve -- ne
  ri -- con -- du -- ce
  ri -- con -- du -- ce la vi -- ta la vi -- ta
  ri -- con -- du -- ce la vi -- ta
  nel -- le già fred -- de ve -- ne
  e per stu -- pen -- da pro -- va
  fin con l’in -- gan -- no __ gio -- va.
}
\tag #'deianira {
  U -- na stil -- la di spe -- ne
  oh oh che mar che mar di dol -- cez -- za!
  per un’ a -- ni -- ma av -- vez -- za
  a lan -- guir sem -- pre in pe -- ne __
  per un’ a -- ni -- ma av -- vez -- za
  a lan -- guir a lan -- guir sem -- pre in pe -- ne. __
  U -- na stil -- la di spe -- ne,
  ben -- ché tal’ or tal’ or men -- ti -- ta
  nel -- le già fred -- de ve -- ne
  ri -- con -- du -- ce la vi -- ta __
  nel -- le già fred -- de ve -- ne
  ri -- con -- du -- ce la vi -- ta
  e per stu -- pen -- da pro -- va
  fin con l’in -- gan -- no __ gio -- va. __
}
\tag #'licco {
  U -- na stil -- la di spe -- ne
  oh che mar di dol -- cez -- za!
  per un’ a -- ni -- ma av -- vez -- za
  a lan -- guir a lan -- guir sem -- pre in pe -- ne
  a lan -- guir a lan -- guir
  per un’ a -- ni -- ma av -- vez -- za
  a lan -- guir sem -- pre in pe -- ne.
  U -- na stil -- la di spe -- ne,
  ben -- ché tal’ or men -- ti -- ta
  nel -- le già fred -- de ve -- ne
  ri -- con -- du -- ce
  ri -- con -- du -- ce la vi -- ta
  e per stu -- pen -- da stu -- pen -- da pro -- va
  fin con l’in -- gan -- no __ gio -- va.
}
