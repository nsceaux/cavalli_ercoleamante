\clef "dessus" r4 r mi'' |
re''2 do''4 si' la' sol' |
fa'2. sol' |
r2*3/2 r4 la''2 |
r4 do'''2 r4 si''4. si''8 |
mi''4 mi'' fad'' mi''2. |
mi''2 mi''8 si' |
do''4. do''8 re''4 |
do''4 do'' si'8 si' |
la'2 do''8 do'' |
re''2 mi''8 mi'' |
re''4 re'' do''~ |
do''8 re'' mi''2 mi'' do''8 si' |
si'2. |
r4 r la'8 la' |
mi''4. mi''8 re'' mi'' |
do''4 la' re''8 do'' |
re''2 re''8 do'' |
do''2 mi''8 mi'' |
la''4 sol'' do''8 la' do''4 re''2 |
mi''2 r8 do'' re''2 r8 sib' |
do''2 r8 la' si'2 r8 sol' la'[ do''] do''4. si'8 |
do''2 mi''4 re''2 do''4 |
si' la' sol' fa'2 fa'4 |
sol'2. r2*3/2 |
r4 la''2 r4 do'''2 |
r4 si''4. si''8 mi''4 mi'' fa'' |
mi''2. mi''2 mi''8 si' |
do''4. do''8 re''4 |
do'' do'' si'8 si' la'2 do''8 do'' |
re''2 mi''8 mi'' re''4 re'' do''~ |
do''8 re'' mi''2 mi'' do''8 si' |
si'4 si' si' do'' re'' re''8 re'' |
mi''4 si' mi'' mi''4. mi''8 mi'' do'' |
re''4 re''8 re'' re'' re'' |
do''4 do'' mi''8 mi'' |
la''4 sol'' do''~ do''8 si'16 la' re''2 |
mi''2 r8 do'' re''2 r8 sib' do''2 r8 la' |
si'2 r8 sol' la' do'' do''4. si'8 do''1*3/4\fermata |
