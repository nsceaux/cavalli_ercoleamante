\score {
  <<
    \new StaffGroup <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
      \new Staff << \global \includeNotes "partie1" >>
      \new Staff << \global \includeNotes "partie2" >>
    >>
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'iole \includeNotes "voix"
      >> \keepWithTag #'iole \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'deianira \includeNotes "voix"
      >> \keepWithTag #'deianira \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'licco \includeNotes "voix"
      >> \keepWithTag #'licco \includeLyrics "paroles"
    >>
    \new StaffGroup <<
      \new Staff <<
        \global \keepWithTag #'basse \includeNotes "basse"
        \includeFigures "chiffres"
      >>
      \new Staff <<
        \global \keepWithTag #'basse-continue \includeNotes "basse"
        \origLayout {
          s2.*12\pageBreak
          s2.*8\pageBreak
          s2.*9\pageBreak
          s2.*13\pageBreak
          s2.*9\pageBreak
          s2.*8\pageBreak
        }
      >>
    >>
  >>
  \layout { }
  \midi { }
}
