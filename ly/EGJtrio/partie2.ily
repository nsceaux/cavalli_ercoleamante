\clef "viole2" r4 r do' |
re'2 fa'4 sol' re' mi' |
la2 fa4 do'2. |
r2*3/2 r4 re'2 |
la2 la4 si2 si4 |
do'2 re'4 mi'2. |
mi'2 mi'8 re' |
do'4. do'8 si4 |
la la r |
r r mi'8 mi' |
re'2 do'4 |
si si la |
mi' mi2 la2. |
r4 r sol'8 sol' |
fa'2 fa'8 mi' |
mi'2 mi8 mi |
la4. si8 do' la |
si4 si si8 si |
do'2 do'8 do' |
fa'4 sol' fa' sol' sol2 |
do'2 do'4 sib2 sib4 |
la2 la4 sol2 sol8 do' la4 re'2 |
do'2 do'4 re'2 fa'4 |
sol' re' mi' la2 fa4 |
do'2. r2*3/2 |
re'2 re4 la2 la4 |
si2 si4 do'2 re'4 |
mi'2. mi'2 mi'8 re' |
do'4. do'8 si4 |
la2 r4 r r mi'8 mi' |
re'2 do'4 si si la |
mi' mi2 la2. |
R2.*2 |
r4 r mi8 mi la4. si8 do' la |
si2 si4 |
do'2 do'4 |
fa'4 sol' fa' sol' sol2 |
do'2 do'4 sib2 sib4 la2 la4 |
sol2 sol8 do' la4 re'2 \sug mi'1*3/4\fermata |
