\score {
  <<
    \new StaffGroup <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
      \new Staff << \global \includeNotes "partie1" >>
      \new Staff << \global \includeNotes "partie2" >>
    >>
    \new Staff \with { \tinyStaff \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'basse \includeNotes "voix"
    >> \keepWithTag #'basse { \set fontSize = #-2 \includeLyrics "paroles" }
    \new StaffGroup <<
      \new Staff <<
        \global \keepWithTag #'basse \includeNotes "basse"
        \includeFigures "chiffres"
      >>
      \new Staff <<
        \global \keepWithTag #'basse-continue \includeNotes "basse"
      >>
    >>
  >>
  \layout { }
}
