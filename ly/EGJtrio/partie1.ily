\clef "viole1" r4 r mi' |
sol'2 la'4 mi' fa' do' |
re'2. sol |
r2*3/2 r4 la'2 |
r4 mi'2 r4 sol'4. sol'8 |
mi'4 la' re'' sold'2. |
sold'2 sold'8 sold' |
mi'4. mi'8 sold'4 |
la' la' sol' |
fa'2 r4 |
r r do''8 do'' |
fad'4 re' la'~ |
la'8 sol'16 la' si'4 si8 mi' do'2 la'8 sol' |
sol'2 sol'8 sol' |
re''4. re''8 do'' re'' |
si'4 mi' si'8 la' |
la'2 la'8 mi' |
sol'4. sol'8 fa' sol' |
sol'4 sol' sol'8 do'' |
do''4 sol' la'8 do'' sol'2. |
sol'4. sol'8 la'4 fa'4. fa'8 sol'4 |
mi'4. mi'8 fa'4 re'4. re'8 mi'4 do' sol'2 |
sol' mi'4 sol'2 la'4 |
fa' fa' do' re'2. |
sol r2*3/2 |
r4 la'2 r4 mi'2 |
r4 sol'4. sol'8 mi'4 la' re'' |
sold'2. sold'2 si'8 re'' |
la'4 fa' sold' |
la' la' sol' fa'2 r4 |
r r do''8 do'' fad'4 re' la'~ |
la'8 sold'16 la' si'4 si8 mi' do'2 la'8 \ficta sol' |
sol'4 sol' sol' fa' fa' fa'8 fa' |
mi'4 mi' mi' mi' la2 |
re'2 re'4 |
mi'2 sol'8 do'' |
do''4 sol' la'8 do'' sol'2. |
sol'4. sol'8 la'4 fa'4. fa'8 sol'4 mi'4. mi'8 fa'4 |
re'4. re'8 mi'4 do' sol'2 sol'1*3/4\fermata |
