\key do \major \midiTempo#160
\beginMark "Ritor[nello]" \digitTime\time 3/4 s2.
\measure 6/4 s1.*5
\measure 3/4 s2.*6
\measure 6/4 s1.
\measure 3/4 s2.*6
\measure 6/4 s1.
\beginMark "Ritor[nello]" s1.
\measure 9/4 s2.*3
\measure 6/4 s1.*6
\measure 3/4 s2.
\measure 6/4 s1.*5
\measure 3/4 s2.*2
\measure 6/4 s1.
\beginMark "Ritor[nello]" \measure 9/4 s2.*6 \bar "|."
