\score {
  \new Staff \withLyrics <<
    \global \keepWithTag #'voix3 \includeNotes "voix" \clef "alto"
  >> \keepWithTag #'voix3 \includeLyrics "paroles"
  \layout { }
}
