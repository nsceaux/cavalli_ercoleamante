\clef "basse" r4 re8 re sol4 fa |
sib4. sib8 la2 |
r4 re8 mi fa4 sib, |
do2 fa, |
fa mi re |
mi re do re1. |
do1. do1 re2 |
mi re mi |
mi1 fad2 sol1. |
mi1. fa2 mi fa |
sol1. do |
re1 mi2 fa mi fa |
fa1 sol2 la1. |
fa1. sol2 fa sol |
la1. re\fermata |
