<<
  \tag #'(voix1 basse) {
    \clef "vsoprano" r4 la'8 re'' sib'4 do'' |
    re''4. mi''8 dod''4 la' |
    r fa'8 sol' la'4 sol' |
    sol'4. fa'8 fa'2 |
    do''2 do'' re'' |
    do'' do'' do'' do''1 si'2 |
    do''1 do''2 mi''1 re''2 |
    do''2 si' do'' |
    do''1 re''2 re''1 re''2 |
    mi''1 mi''2 re'' do'' re'' |
    re''1 mi''2 do''1. |
    fa''1 mi''2 re'' dod'' re'' |
    re''1 mi''2 mi''1 mi''2 |
    fa''1 fa''2 mi'' re'' mi'' |
    mi''1 mi''2 re''1.\fermata |
  }
  \tag #'voix2 {
    \clef "vsoprano" r4 fa'8 fa' sol'4 la' |
    la'4. sol'8 la'4 la' |
    r la'8 sol' fa'4 fa' |
    fa'4. mi'8 fa'2 |
    la'2 sol' fa' |
    sol' fa' mi' fa'1 fa'2 |
    mi'1 mi'2 mi'1 fa'2 |
    sol' fa' sol' |
    sol'1 la'2 si'1 si'2 |
    sol'1 sol'2 la' do'' do'' |
    do''1 si'2 do''1. |
    la'1 sol'2 la' sol' la' |
    la'1 \ficta si'2 dod''1 dod''2 |
    la'1 la'2 sib' re'' re'' |
    re''1 dod''2 re''1.\fermata |
  }
  \tag #'voix3 {
    \clef "valto" r4 re'8 re' sol'4 fa' |
    sib'4. sib'8 la'4 la' |
    r re'8 mi' fa'4 sib |
    do'4. do'8 fa'2 |
    fa' mi' re' |
    mi' re' do' re'1 re'2 |
    do'1 do'2 do'1 re'2 |
    mi' re' mi' |
    mi'1 fad'2 sol'1 sol'2 |
    mi'1 mi'2 fa' mi' fa' |
    sol'1 sol'2 do'1. |
    re'1 mi'2 fa' mi' fa' |
    fa'1 sol'2 la'1 la'2 |
    fa'1 fa'2 sol' fa' sol' |
    la'1 la'2 re'1.\fermata |
  }
>>