\clef "dessus" mi''1 |
mi''2 do'' |
mi''2. mi''4 mi''1 |
mi''2. mi''4 |
fa''1 |
sol''2. mi''4 |
la'1 |
re''2 sol'4 do'' |
\once\tieDashed si'1~ |
si'2. si'4 mi''1 |
\once\tieDashed mi''~ |
mi''~ |
mi''~ |
mi'' | \allowPageTurn
do''1 fa''2. re''4 |
re''2 si'~ |
si' la' |
do'' si'~ |
si'4 sol' do''2 |
sol'2 do''4 re''8 la' |
do''4
mib''8 fa'' re''2 do''1 |
\tieDashed mi''~ |
mi'' |
mi''~ |
mi'' |
mi'' | \tieSolid
fa''2 re'' |
r4 la' la'4. sold'8 la'1\fermata |
