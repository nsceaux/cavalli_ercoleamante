\clef "dessus" la'1 |
la'2 la' |
si'2. si'4 do''1 |
la'2. la'4 |
la'1 |
do''2. do''4 |
do''1 |
si'2 do''4 sol' |
\once\tieDashed sol'1~ |
sol'2. sol'4 sol'1 |
do''2 sol' |
\once\tieDashed sol'1~ |
sol'~ |
sol' |
fa'1 la'2. la'4 |
sol'2 sol'~ |
sol'4 sol'' mi''2 |
la'' sol''~ |
sol'' sol'' |
mi'' fa''4 la'' |
sol''4. la''8 sol''2 mi''1 |
\tieDashed si'1~ |
si' |
si'~ |
si' | \tieSolid
la'2. do''4 |
re''2 si'4 la' |
do''2 si' dod''1\fermata |
