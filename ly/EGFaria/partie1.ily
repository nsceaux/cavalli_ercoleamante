\clef "viole1" do'1 |
do'2 la |
mi'2. mi'4 mi'1 |
do'2. mi'4 |
re'1 |
do'2. do'4 |
re'1 |
re'2 mi'4 do' |
\once\tieDashed re'1~ |
re'2. re'4 sol1 |
mi'2 do' |
\once\tieDashed do'1~ |
do'~ |
do' |
do' re'2. la4 |
si2 \tieDashed sol~ |
sol do'~ |
do' \tieSolid re'~ |
re' do'~ |
do' do'4 fa' |
mi'4. do'8 sol'2 sol'1 |
\once\tieDashed mi'~ |
mi' |
mi' |
sold' |
mi' |
re'2 fa'~ |
fa'4 la mi'2 mi'1\fermata |
