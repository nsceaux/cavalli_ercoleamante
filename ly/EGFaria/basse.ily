\clef "basse"
<<
  \tag #'basse {
    la,1 |
    la,2 la, |
    sold,2. sold,4 la,1 |
    la,2. la,4 |
    fa,1 |
    mi, |
    re,1 |
    re,2 do, |
    sol,1~ |
    sol,2. sol,4 mi,1 |
    \tieDashed mi,1~ |
    mi,~ | \tieSolid
    mi,~ |
    mi, |
    fa,1 fa,2. fa,4 |
    sol,1~ |
    sol,2 la,~ |
    la, si,~ |
    si, do~ |
    do2 fa,4. fa,8 |
    sol,4. fa,8 sol,2 do1 |
    \tieDashed sold,1~ |
    sold, |
    sold,~ |
    sold, | \tieSolid
    la, |
    re~ |
    re4 re mi2 la,1\fermata |
  }
  \tag #'basse-continue {
    la,1 |
    la,1 |
    sold, la, |
    la, |
    fa, |
    mi, |
    re,1~ |
    re,2 do, |
    sol,1~ |
    sol, mi, |
    mi,1~ |
    mi,~ |
    \once\tieDashed mi,~ |
    mi, |
    fa, fa, |
    sol,1~ |
    sol,2 la,~ |
    la, si,~ |
    si, do~ |
    do fa, |
    sol,4. fa,8 sol,2 do1 |
    sold,~ |
    sold, |
    sold,~ |
    sold, |
    la, |
    re~ |
    re4 re mi2 la,1\fermata |
  }
>>
