\clef "vbas-dessus" <>^\markup\character Iole
do''4 do''8 si' do''4 do''8 do'' |
do'' do'' do'' re'' mi''4 mi'' |
si'4. si'8 si' si' si' do'' la'2 la' |
do''8 do'' do'' do''16 do'' do''8 do'' do'' si' |
re'' re'' re'' re'' re''4 re''8 do'' |
mi''4. mi''8 mi''4 r8 do'' |
fa''4 fa'' r fa''8 fa'' |
fa''4 fa''8 mi'' mi'' mi'' mi'' re'' |
re''4 re''8 re'' re'' re'' re'' re'' |
re'' re'' re'' re'' re''4 re''8 mi'' do''4 do'' r2 |
sol''4 do''8 do'' do'' si' do''4 |
do''8 do'' do'' re'' sib'4. sib'8 |
sib' la' sib'4 sib'4. sib'8 |
sib'4 sib'8 sib' sib' sib' sib' la' |
la'2 la' re''4 re'' r fa'' |
si'4 si'8 si' mi'' mi'' mi'' mi'' |
mi''4 mi''8 mi'' do''4 do''8 do'' |
fa'' fa'' fa'' fa'' re''4 re''8 re'' |
re'' re'' re'' do'' mi''4 mi''8 mi'' |
do''8. sib'16 sib'8 la' la'4 la'8 re'' |
mi'' si' do''2 si'4( do''1) |
si'4 si'8 mi'' si'4 si'8 mi'' |
si' si' si' la' si'4. si'8 |
si'4 si'8 si' si' si' si' si' |
mi'4 mi'8 mi' si' si' si' la' |
do''4 do'' do''8 do'' do'' do''16 si' |
si'8 si' r re'' sold' sold' r do'' |
la' la' r4 r2 r1\fermata |
