\clef "viole2" la1 |
la2 mi |
si2. mi4 la1 |
la2. la4 |
la1 |
sol2. sol4 |
fa1 |
fa2 sol4 sol |
\once\tieDashed sol1~ |
sol2. sol4 do'1 |
do'2 mi' |
mi'1 |
mi'2 mi |
mi1 |
la la2. re'4 |
re'2 mi'~ |
mi' mi' |
la4 fa sol2~ |
sol mi~ |
mi la4 re' |
sol2 re'4 sol sol1 |
\tieDashed si~ |
si |
mi~ |
mi | \tieSolid
la |
si2 re' |
r4 mi' si2 la1\fermata |
