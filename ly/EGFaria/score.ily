\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket \with { \haraKiri } <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
      \new Staff << \global \includeNotes "partie1" >>
      \new Staff << \global \includeNotes "partie2" >>
      \new Staff <<
        \global \keepWithTag #'basse \includeNotes "basse"
      >>
    >>
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*2\pageBreak
        s1*5\pageBreak
        s1*5\pageBreak
        s1*6\pageBreak
        s1*4\pageBreak
        s1*6\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
