Ben re -- sis -- te -- a l’av -- ver -- so mio vo -- le -- re
d’Er -- co -- le à le pre -- ghie -- re,
e al -- la for -- za di lui pur fat -- ta hav -- re -- i
re -- sis -- ten -- za in -- vin -- ci -- bi -- le, ma d’Hyl -- lo,
d’Hyl -- lo a te già non men, ch’a me sì ca -- ro,
che del -- le nos -- tre of -- fe -- se
non fu com -- pli -- ce ma -- i:
an -- zi che ne sof -- fer -- se
al par di noi con a -- mo -- ro -- sa, e im -- men -- sa
com -- pas -- sio -- ne il duo -- lo,
d’Hyl -- lo, ohi -- mè, di lui so -- lo
il pe -- ri -- glio mor -- ta -- le
m’as -- trin -- se a con -- sen -- ti -- re
all’ a -- bor -- ri -- te noz -- ze,
com’ u -- ni -- co ri -- pa -- ro al suo mo -- ri -- re: __
dun -- que per -- do -- na, per -- do -- na, o ge -- ni -- tor, l’in -- ten -- to
di ques -- te sa -- cre pom -- pe
ch’A -- mor, che non ha leg -- ge
o -- gni leg -- ge a sua vo -- glia o scio -- glie, o rom -- pe.
