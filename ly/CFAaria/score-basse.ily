\score {
  \new ChoirStaff \with { \haraKiriFirst } <<
    \new Staff \with { \tinyStaff \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'basse \includeNotes "voix"
    >> \keepWithTag #'basse { \set fontSize = #-2 \includeLyrics "paroles" }
    \new GrandStaff <<
      \new Staff << \global \includeNotes "harpe1" >>
      \new Staff <<
        \global \includeNotes "basse"
        { s1*23 \startHaraKiri\break s2.*23 \stopHaraKiri\break }
      >>
    >>
    \new GrandStaff <<
      \new Staff << \global \includeNotes "harpe2" >>
      \new Staff <<
        \global \includeNotes "basse"
        \includeFigures "chiffres"
      >>
    >>
  >>
  \layout {
    system-count = #(*system-count*)
    indent = #(if (*instrument-name*)
                  largeindent
                  (or (*score-indent*) smallindent))
  }
}
