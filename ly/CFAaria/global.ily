\key fa \major \midiTempo#160
\time 4/4 s1*21 \measure 2/1 s1*2
\digitTime\time 3/4 s2.*2
\measure 6/4 s1.
\measure 3/4 s2.
\measure 6/4 s1.*9 \bar "||"
\time 4/4 s1*21 \measure 2/1 s1*2 \bar "|."
