\clef "viole1"
\setMusic#'couplet {
  do'4. re'8 do'4. sib8 |
  la4 re' do'2 |
  do'4. sib8 la2 |
  re'4 do'2 si4 |
  do'4. si8 do'4. fa8 |
  sol2. sol4 |
  fa2 fa4 la8 sib |
  do'4. do'8 re' do' sib la |
  sib4. do'8 re'4. la8 |
  la2 r4 re'~ |
  re'8 fa' mi'4 re'4. do'8 |
  do'4. do'8 re' do'4 re'8 |
  re'4. mi'8 mi' fa' mi'8. re'16 |
  re'4. la8 sib re' sol4~ |
  sol8 sol do'4 sol8 sib4 fa8 |
  fa4. sib8 sib la re' sib |
  do'4. mib'8 la sib la8. sol16 |
  sol2. do'4 |
  do'4. sib8 la fa sol la |
  sib4. re'8 do'4 re'8 do' |
  la sib do' re' do' sib la do' |
  sib do' fa sib sol fa sol mi fa1\fermata |
}
\keepWithTag#'() \couplet
R2.*23 |
\couplet
