Mor -- mo -- ra -- te o fiu -- mi -- cel -- li,
sus -- sur -- ra -- te o ven -- ti -- cel -- li,
e col vos -- tro sus -- sur -- ro, e mor -- mo -- ri -- o
dol -- ci in -- can -- ti dell’ o -- bli -- o,
ch’o -- gni cu -- ra fu -- gar pon -- no
lu -- sin -- ga -- te al son -- no il Son -- no
ch’o -- gni cu -- ra fu -- gar pon -- no
lu -- sin -- ga -- te al son -- no il Son -- no
lu -- sin -- ga -- te lu -- sin -- ga -- te al son -- no il Son -- no.

Chi da ver a -- ma
vie più il di -- let -- to
del ca -- ro og -- get -- to
che’l pro -- prio bra -- ma,
quind’ è ch’io po -- si
la not -- t’el di -- e
le con -- ten -- tez -- ze mi -- e
del con -- sor -- te gen -- til ne’ bei ri -- po -- si.

Mor -- mo -- ra -- te o fiu -- mi -- cel -- li,
sus -- sur -- ra -- te o ven -- ti -- cel -- li,
e col vos -- tro sus -- sur -- ro, e mor -- mo -- ri -- o
dol -- ci in -- can -- ti dell’ o -- bli -- o,
ch’o -- gni cu -- ra fu -- gar pon -- no
lu -- sin -- ga -- te al son -- no il Son -- no
ch’o -- gni cu -- ra fu -- gar pon -- no
lu -- sin -- ga -- te al son -- no il Son -- no
lu -- sin -- ga -- te lu -- sin -- ga -- te al son -- no il Son -- no.
