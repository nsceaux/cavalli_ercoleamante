\clef "dessus" <>^"Harpe"
\setMusic#'couplet {
  do''4 r8 sol' la'4 r8 sib' |
  do''4 r8 sol' mi'4 r8 mi' |
  fa'4 r8 sib' do''4 r8 la' |
  sol'4 r8 fa' do''4 r8 sol' |
  mi'4 r8 fa' sol'4 r8 si' |
  do''4 r8 sol' do''4 r8 sol' |
  la'4 r8 fa' la'4 r8 sol' |
  do''4 r8 la' re''4 r8 mib'' |
  re''4 r8 fa'' re''4 r8 la' |
  fad'4 r8 fad' sol'4 r8 do'' |
  sib' do'' re''4 r8 do'' sib' sol' |
  la'4 r8 do'' sib'4 r8 sib' |
  re''4 r8 re'' la'4 r8 la' |
  fad'4 r8 fad' sol'4 r8 re'' |
  sib'4 r8 re'' do''4 r8 do'' |
  sib'4 r8 fa' mib'4 r8 sol' |
  mib'4 r8 mib' re'4 r8 re'' |
  si' sol' re'' si' do''4 r8 do'' |
  la'4 r8 do'' fa'4 r8 do'' |
  sib'4 r8 re'' do''4 r8 do''~ |
  do'' sib' la' sol' la' sib' do''4 |
  fa'4. sib'4 la'8 sol'4 fa'1\fermata |
}
\keepWithTag#'() \couplet
R2.*23 |
\couplet
