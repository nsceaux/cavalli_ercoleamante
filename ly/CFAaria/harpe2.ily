\clef "dessus" <>^"Harpe"
\setMusic#'couplet {
  r8 sol' do'' r r sol' la' r |
  r sol' do'' r r fa' sol' r |
  r sol' la' r r sol' do'' r |
  r do'' la' r r la' sol'4 |
  r8 la' sol' r r la' sol' r |
  r si' do'' r r sib' la' r |
  r fa' do'' r r sol' do'' r |
  r do'' re'' mib'' sib' do'' re'' r |
  r8 la' sib' r r do'' sib' r |
  r la' re'4 r8 re'' sib' la' |
  sol'4 r8 do'' sib'4 r8 sol' |
  do'' sib' la' r r sol' fa' r |
  r do'' sib' r r re'' sib' r |
  r mi' re' r r la' sib' r |
  r re'' mib'' r r sib' sol' r |
  r la' sib' r r do' re' r |
  r8 re' sol'4 r8 sib' la' re' |
  r re'' si' sol' r fa' mi' r |
  r do'' la'4 r8 sib' sol' r |
  r la' sib' r r fa' sib' la' |
  r re'' do'' sib' do''2 |
  r8 fa' re' fa' mi' fa' re' do' do'1\fermata |
}
\keepWithTag#'() \couplet
R2.*23 |
\couplet
