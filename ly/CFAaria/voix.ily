\clef "vsoprano" <>^\markup\character Pasithea
fa'8[ mi'] fa'[ sol'] fa'[ mi'] fa'[ sol'] |
la'[ sol'] la'[ sib'] do''2 |
do''4 re''8 mi'' la'[ si'] do''[ re''] |
si'[ sol'] do''[ re''] mi''[\melisma fa''16 mi''] re''4\melismaEnd |
do''2 r |
sol'4 sol' sol' la'8 sib' |
do''[ re''] do''[ re''] do''[ re''] do''[ sib'] |
la'4 la' sib'8[ la'] sib'[ do''] |
re''[ mib''] re''[ do''] sib'[ la'] sol'[ do''] |
la'4 la' sib'8[ la'] sib'[ do''] |
re''[ do''] sib'[ do''] re''[ do''] re''[ mi''] |
fa''4 fa'' re''8[ mi''] fa''[ re''] |
sol''[ fa''] mi''[ re''] dod''[ re''] mi''[ fa''] |
re''4 re'' sib'8[ la'] sol'[ fa'] |
sol'[ fa'] mib'[ fa'] sol'[ fa'] sol'[ la'] |
sib'4 sib' sol'8[ la'] sib'[ sol'] |
do''[ sib'] la'[ sol'] fad'[ sol'] la'[ sib'] |
sol'4 sol' mi'8[ fa'] sol'[ mi'] |
la'4 la' la'8[ sib'] do''[ la'] |
re''[ do''] sib'[ la'] sol'[ fa'] sol'[ la'] fa'4 fa' r2 |
R1*2 |
do''4 do'' do'' |
sib' sol'2 |
la'4 la' la' sol'4. fa'8\melisma mi'4\melismaEnd |
fa' sol' fa' |
mi' fa' sol' la' fa'( mi') |
re'2. do' |
do'4 sol' la' sib' la' sol' |
la' sib'2 do''4 sib' la' |
sib' do''2 re''4 do''( re'') |
sib'2. la'4 la' do'' |
sol' la'4. sib'8 do''2. |
sib' la'2 sol'4~ |
sol' sol'2 fa'1*3/4\fermata |
fa'8[ mi'] fa'[ sol'] fa'[ mi'] fa'[ sol'] |
la'[ sol'] la'[ sib'] do''2 |
do''4 re''8 mi'' la'8[ si'] do''[ re''] |
si'[ sol'] do''[ re''] mi''[\melisma fa''16 mi''] re''4\melismaEnd |
do''2 r |
sol'4 sol' sol' la'8 sib' |
do''[ re''] do''[ re''] do''[ re''] do''[ sib'] |
la'4 la' sib'8[ la'] sib'[ do''] |
re''[ mib''] re''[ do''] sib'[ la'] sol'[ do''] |
la'4 la' sib'8[ la'] sib'[ do''] |
re''[ do''] sib'[ do''] re''[ do''] re''[ mi''] |
fa''4 fa'' re''8[ mi''] fa''[ re''] |
sol''[ fa''] mi''[ re''] dod''[ re''] mi''[ fa''] |
re''4 re'' sib'8[ la'] sol'[ fa'] |
sol'[ fa'] mib'[ fa'] sol'[ fa'] sol'[ la'] |
sib'4 sib' sol'8[ la'] sib'[ sol'] |
do''[ sib'] la'[ sol'] fad'[ sol'] la'[ sib'] |
sol'4 sol' mi'8[ fa'] sol'[ mi'] |
la'4 la' la'8[ sib'] do''[ la'] |
re''[ do''] sib'[ la'] sol'[ fa'] sol'[ la'] |
fa'4 fa' r2 |
R1*2 |
