\clef "viole0"
\setMusic#'couplet {
  la'8 sol' fa' re' la' sol' fa' mi' |
  fa' do'' la' sol' sol'2 |
  la'8 sol' fa' sol' do'4 fa'~ |
  fa'8 mi' fa' la' sol' re' sol'4~ |
  sol'8 fa' mi' re' mi'4. fa'8 |
  mi' sol' fa' re' mi' re' mi' re' |
  fa'2 fa'4. mi'8 |
  fa'4. sol'8 fa' mib' fa'4 |
  fa'4 sol'8 la' sib' fa' sol' mib' |
  re'4. la'8 sol' re' sol' fad' |
  sol' la' sib'4 fa'4. do''8 |
  la' sol' fa'4 sol' la'8 fa' |
  sol'4. sib'8 la'4 sol'8 mi' |
  fad' sol' la'4 re' mib'8 fa' |
  mib' fa' sol'4 do'8 fa' mib' do' |
  re'4 fa' sol'8 fa'4 sol'8 |
  la'4. sib'8 la' sol'4 fad'8 |
  sol'2. sol'4 |
  fa'4. sol'8 fa'4 mib'8 do' |
  re' mib' fa'4 mib'8 fa'4 mib'8 |
  fa'8 mi' fa' sol' fa' sol' la'4 |
  sib'8 la' sib' re'' sol' do'' sib' sol' la'1\fermata | \allowPageTurn
}
\keepWithTag#'() \couplet
R2.*23 |
\couplet
