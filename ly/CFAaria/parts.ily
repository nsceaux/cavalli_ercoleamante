\piecePartSpecs
#(if (eqv? #t (ly:get-option 'pygmalion))
     '((violes #:score-template "score-violes-voix")
       (basse #:score "score-basse"))
     '((basse #:score-template "score-voix")))
