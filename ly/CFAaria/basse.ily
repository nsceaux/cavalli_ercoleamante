\clef "basse" fa8 sol la sib la sib la sol |
fa mi fa sol do re do sib, |
la, sib, la, sol, fa, sol, la, fa, |
sol, mi, la, fa, sol, fa, sol,4 |
do8 re mi fa mi fa mi re |
do sol, la, si, do re do \ficta sib, |
la, sib, la, sib, la, sib, la, sol, |
fa sol fa mib re mib re do |
sib, do sib, la, sol, la, sib, do |
re mi fad re sol \ficta fad? sol la |
sib la sol la sib la sib do' |
fa sol la fa sib do' la sib |
sol la sib sol la fa sol la |
re mi fad re sol \ficta fa mib re |
mib re do re mib! re mib fa |
sib, do re sib, mib fa re mib |
do re mib do re sib, do re |
sol, la, si, sol, do re mi do |
fa do re mi fa re mib fa |
sib, do re sib, do la, sib, do |
fa sol la sib la sol fa mib |
re do re sib, do la, sib, do fa,1\fermata |
fa2 fa4 |
sol do'2 |
fa4 fa fa mi re do~ |
do si,2 |
do mi,4 fa,2. |
sol, do |
do'4 sib la sol2 do'4 |
la4 sol2 fa fa4 |
re do2 sib,4 la,2 |
sol,2. fa,4 fa2 |
mi4 re2 do4 la do' |
sol la4. sib8 do'4 la sib |
sib, do2 fa,1*3/4\fermata | \allowPageTurn
fa8 sol la sib la sib la sol |
fa mi fa sol do re do sib, |
la, sib, la, sol, fa, sol, la, fa, |
sol, mi, la, fa, sol, fa, sol,4 |
do8 re mi fa mi fa mi re |
do sol, la, si, do re do \ficta sib, |
la, sib, la, sib, la, sib, la, sol, |
fa sol fa mib re mib re do |
sib, do sib, la, sol, la, sib, do |
re mi fad re sol fad! sol la |
sib la sol la sib la sib do' |
fa sol la fa sib do' la sib |
sol la sib sol la fa sol la |
re mi fad re sol \ficta fa mib re |
mib re do re mib! re mib fa |
sib, do re sib, mib fa re mib |
do re mib do re sib, do re |
sol, la, si, sol, do re mi do |
fa do re mi fa re mib fa |
sib, do re sib, do la, sib, do |
fa sol la sib la sol fa mib |
re do re sib, do la, sib, do fa,1\fermata |
