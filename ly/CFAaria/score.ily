\score {
  <<
    \original\new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \includeNotes "voix"
      >> \includeLyrics "paroles"
      \new Staff <<
        \global \includeNotes "basse"
        \includeFigures "chiffres"
        \origLayout {
          s1*5\break s1*6\pageBreak
          s1*6\break s1*6\break s2.*11\break s2.*12\break s1*5\pageBreak
          s1*6\break s1*6\break
        }
      >>
    >>
    \pygmalion\new StaffGroupNoBar \with { \haraKiriFirst } <<
      \new StaffGroupNoBracket <<
        \new Staff << \global \includeNotes "viole0" >>
        \new Staff << \global \includeNotes "viole1" >>
      >>
      \new Staff \withLyrics <<
        \global \includeNotes "voix"
      >> \includeLyrics "paroles"
      \new GrandStaff <<
        \new Staff << \global \includeNotes "harpe1" >>
        \new Staff <<
          \global \includeNotes "basse"
          { s1*23 \startHaraKiri\break s2.*23 \stopHaraKiri\break }
        >>
      >>
      \new GrandStaff <<
        \new Staff << \global \includeNotes "harpe2" >>
        \new Staff <<
          \global \includeNotes "basse"
          \includeFigures "chiffres"
        >>
      >>
    >>
  >>
  \layout { }
  \midi { }
}
