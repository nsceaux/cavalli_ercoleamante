\score {
  \new StaffGroupNoBar \with { \haraKiriFirst } <<
    \new StaffGroupNoBracket <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
      \new Staff << \global \includeNotes "partie1" >>
      \new Staff << \global \includeNotes "partie2" >>
      \new Staff << \global \includeNotes "basse1" >>
    >>
    \pygmalion\new StaffGroupNoBracket <<
      \new Staff << \global \includeNotes "viole0" >>
      \new Staff << \global \includeNotes "viole1" >>
      \new Staff << \global \includeNotes "viole2" >>
    >>
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \original\origLayout {
        s2.*10\pageBreak
        s2.*11\pageBreak
        s2.*14\pageBreak
        s2.*8\pageBreak
        s2.*9\pageBreak
        s2.*11\pageBreak
        s2.*7 s1*4\break s1*4 s1.*4\pageBreak
        s1.*4\break s2.*11\pageBreak
      }
      \pygmalion\modVersion {
        s2.*70\break s1*8\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
