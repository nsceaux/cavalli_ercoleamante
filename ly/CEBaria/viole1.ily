\clef "viole1" R2.*70 R1*8 |
r4 si si do'2. |
r2. r4 re' re' |
mib'2. r4 fa' fa' |
mib'2 mib'4 mib'2 re'4 |
mib'2. r4 mib' mib' |
mib'2 lab4 do'2 fa'4 |
re' sol' sol' sol4. sol'8 fa' mib' |
re'2 mib'4 mib' re'2 |
do'2. |
re' r4 do' do' |
sol'2. r4 re' re' |
sol2. r |
r4 r la sib2. |
r4 la sol sol re2 |
r4 re'2 do'4 mib' mib' sol'2. |
r4 r do' mib'2. |
r4 la re' sol2. |
r4 mib'2 |
mib'2. r4 re' mib' |
sol'2 sol4 sol1*3/4\fermata |
