\clef "basse" R2. |
r4 sib2 r4 lab4. lab8 |
sol4 sol2 r4 r fa |
mib mib mib re do2 |
re2. sib, do |
re sol, |
do' sib |
lab sol |
fa sol4 do2 |
sol2. mib |
fa |
sol |
do4 do4. do8 fa2. |
r4 sib,4. sib,8 mib4 mib4. mib8 |
fa4 re2 mib4 fa2 |
sib,2. r4 sib4. sib8 |
mib2. r4 fa4. fa8 |
sol4 mib2 fa4 sol2 do1*3/4\fermata |
r2*3/2 r4 sib2 |
lab2. |
sol |
fa |
mib re4 do2 re2. |
sib,2. do2 do4 |
re2. sol, |
r4 do'2 |
sib2. |
lab sol fa |
sol4 do2 |
sol2. |
mib2. fa2 fa4 |
sol2. do4 do4. do8 |
fa2. r4 sib,4. sib,8 mib2. |
fa4 re2 mib4 fa2 |
sib,2. r4 sib4. sib8 |
mib2. r4 fa4. fa8 |
sol4 mib2 fa4 sol2 do1*3/4\fermata |
R1*8 R1.*8 |
R2. |
r4 sib2 lab2. |
sol r4 r fa |
mib mib mib re do2 |
re2. sib, |
do re |
sol, do' sib |
lab sol |
fa sol4 do2 |
sol2. |
mib fa |
sol do1*3/4\fermata |
