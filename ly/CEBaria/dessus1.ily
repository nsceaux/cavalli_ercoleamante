\clef "dessus" R2. |
r4 sol''2 r4 fa''4. fa''8 |
fa''4 mib''2 r4 r re'' |
re'' do'' do'' re'' mib''2 |
la'2. sol''2 sol''8 fa'' mi''4 fad'' sol'' |
sol''4 fad''4. sol''8 sol''2. |
r4 lab''2 r4 sol''4. sol''8 |
sol''4 fa''2 r4 r mib'' |
mib'' mib'' re'' re'' mib''2 |
re''4 sol''2 r4 mib''4. re''8 |
do''4 fa'' mib'' |
do''4 \ficta si'4. do''8 |
do''4 do''4. sib'8 la'2. |
r4 sib'4. la'8 sol'4 do''4. sib'8 |
la'4 re''4. do''8 sib'4 la'4. sib'8 |
sib'4 fa''4. mib''8 re''2. |
r4 mib''4. re''8 do''4 fa''4. mib''8 |
re''4 do''4. re''8 mib''4 re''4. mib''8 do''1*3/4\fermata |
r2*3/2 r4 sol''2 |
r4 fa'' fa'' |
fa'' mib''8 mib'' mib'' mib'' |
fa''4 re''2 |
re''4. do''8 re''4 re'' mib''2 la'2. |
sol''2 sol''8 fa'' mi''4 fad'' sol'' |
sol'' \ficta fad''4. sol''8 sol''2. |
r4 lab''2 |
r4 sol'' sol'' |
sol'' fa''8 fa'' fa'' fa'' fa''4 mib''2 mib''4. re''8 re''4 |
re''4 mib''2 |
re''4 sol''2 |
r4 mib''4. re''8 do''4 re''4. mib''8 |
do''4 si'4. do''8 do''4 do''4. \ficta sib'8 |
la'2. r4 sib'4. la'8 sol'4 do''4. sib'8 |
la'4 re''4. do''8 sib'4 la'4. sib'8 |
sib'4 fa''4. mib''8 re''2. |
r4 mib''4. re''8 do''4 fa''4. mib''8 |
re''4 do''4. re''8 mib''4 re''4. mib''8 do''1*3/4\fermata |
R1*8 R1.*8 |
R2. |
r4 sol''2 r4 fa''4. fa''8 |
fa''4 mib''2 r4 r re'' |
re'' do'' do'' re'' mib''2 |
la'2. sol''2 sol''8 fa'' |
mi''4 fad'' sol'' sol'' fad''!4. sol''8 |
sol''2. r4 lab''2 r4 sol'' sol'' |
sol'' fa''2 r4 r mib'' |
mib'' re'' re'' re'' mib''!2 |
re''4 sol''2 |
r4 \ficta mib''4. re''8 do''4 fa'' mib''! |
do'' si'4. do''8 do''1*3/4\fermata |
