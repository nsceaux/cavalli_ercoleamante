\piecePartSpecs
#(if (eqv? #t (ly:get-option 'pygmalion))
     '((dessus #:score-template "score-dessus-voix")
       (parties #:score-template "score-parties-voix")
       (violes #:score "score-violes")
       (basse #:score "score-basse"))
     '((dessus #:score-template "score-dessus-voix")
       (parties #:score-template "score-parties-voix")
       (basse #:score "score-basse")))
