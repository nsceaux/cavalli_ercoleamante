\clef "basse" do'2. |
sib lab |
sol fa |
mib re4 do2 |
re2. sib, do2 do4 |
re2. sol, |
do' sib |
lab sol |
fa sol4 do2 |
sol2. mib |
fa2 fa4 |
sol2. |
do fa |
sib, mib |
fa4 re2 mib4 fa2 |
sib,2. sib |
mib fa |
sol4 mib2 fa4 sol2 do1*3/4\fermata |
do'2. sib |
lab |
sol |
fa |
mib re4 do2 re2. |
sib,2. do |
re sol, |
do' |
sib |
lab sol fa |
sol4 do2 |
sol2. |
mib fa |
sol do1*3/4\fermata |
fa2. sib, mib |
fa4 re2 mib4 fa2 |
sib,2. sib |
mib fa |
sol4 mib2 fa4 sol2 do1*3/4\fermata |
do2 do'4 sib |
lab2 sol |
mib fa4 sol |
lab2 sol~ |
sol do4 re |
mib fa sib,2~ |
sib,4 do re do |
re2 sol,~ |
sol,4 sol sol mib2. |
r4 fa fa re2. |
r4 mib mib lab2 sib4 |
do'2 lab4 sib2. |
mib4 mib mib do2. |
r4 lab lab fa2. |
sol4 sol sol mib2 fa4 |
sol2 fa4 sol2. |
do2. |
sib lab |
sol fa |
mib re4 do2 |
re2. sib, |
do re |
sol, do' sib |
lab sol |
fa sol4 do2 |
sol2. |
mib fa |
sol do1*3/4\fermata |
