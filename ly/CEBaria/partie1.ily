\clef "viole1" R2. |
r4 sol'2 r4 do'4. do'8 |
re'4 sib2 r4 r fa' |
sib do' sol re' sol do' |
la fad2 sib4 re' sol' sol' do' mib' |
la re'2 re'2. |
r4 mib'2 r4 sol'4. sol'8 |
lab'4 lab'2 r4 r sol' |
mib' fa' fa' re' do'2 |
re'2. sol'4 sol'4. sol'8 |
do'4 re' lab' |
mib' sol'2 |
sol'4 sol'4. sol'8 fa'2. |
r4 fa'4. fa'8 mib'4 sol'4. sol'8 |
do'4 fa'2 sib4 fa'2 |
fa'2. r4 fa'4. fa'8 |
mib'2. r4 lab'4. lab'8 |
re''4 sol'2 lab'4 re' sol' sol'1*3/4\fermata |
r2*3/2 r4 sol'2 |
r4 do' do' |
re' mib'8 sib sib sib |
do'4 re'2 |
sib4. do'8 do'4 la do' mib' re'2. |
sib'4 sol'2 sol' mib'4 |
sib re'2 re'2. |
r4 do'2 |
r4 sol' sol' |
lab'2 lab'4 r r8 sol' sol' sol' mib'4 fa' fa' |
re' do'2 |
re'2. |
sol'4 sol'4. sol'8 do'4 re' lab' |
mib' sol'2 sol'4 sol'4. sol'8 |
fa'2. r4 fa'4. fa'8 mib'4 sol'4. sol'8 |
do'4 fa'2 sib4 fa'2 |
fa'2. r4 fa'4. fa'8 |
mib'2. r4 la'4. la'8 |
re'4 sol'2 lab'4 re' sol' sol'1*3/4\fermata | \allowPageTurn
R1*8 |
R1.*8 |
R2. |
r4 sol'2 r4 do'4. do'8 |
re'4 sib2 r4 r fa' |
sib do' sol re' sol do' |
la fad2 sib4 re' sol' |
sol' do' mib' la re'2 |
re'2. r4 mib'2 r4 sol'4. sol'8 |
lab'4 lab'2 r4 r sol' |
mib' fa' fa' re' do'2 |
re'2. |
sol'4 sol'4. sol'8 do'4 re' lab' |
mib' sol'2 sol'1*3/4\fermata |
