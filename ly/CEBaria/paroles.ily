Ahi ch’a -- ma -- rez -- za
me -- schi -- na me -- schi -- na me
è la cer -- tez -- za
di rot -- ta fé!
Ahi ch’a -- ma -- rez -- za
me -- schi -- na me -- schi -- na me
è la cer -- tez -- za
di rot -- ta fé!

Ahi co -- me, ohi -- mè,
la ge -- lo -- si -- a
di fu -- rie di fu -- rie l’E -- re -- bo im -- po -- ve -- rì.
E l’al -- ma mi -- a
ne ri -- em -- pì.
Ahi co -- me, ohi -- mè,
la ge -- lo -- si -- a
di fu -- rie di fu -- rie l’E -- re -- bo im -- po -- ve -- rì.
E l’al -- ma mi -- a
ne ri -- em -- pì.

S’in a -- mor si rad -- dop -- pias -- se -- ro
tut -- ti i guai, tut -- ti i tor -- men -- ti,
e ch’in lui so -- lo man -- cas -- se -- ro
i sos -- pet -- ti, e i tra -- di -- men -- ti
fo -- ra a -- mor fo -- ra a -- mor tut -- ta tut -- ta dol -- cez -- za
fo -- ra a -- mor fo -- ra a -- mor tut -- ta tut -- ta dol -- cez -- za. __

Ahi ch’a -- ma -- rez -- za
me -- schi -- na me -- schi -- na me
è la cer -- tez -- za
di rot -- ta fé!
Ahi ch’a -- ma -- rez -- za
me -- schi -- na me -- schi -- na me
è la cer -- tez -- za
di rot -- ta fé!
