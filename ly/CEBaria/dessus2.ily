\clef "dessus" R2. |
r4 sib'2 r4 do''4. do''8 |
sib'4 sib'2 r4 r lab' |
sol' sol' sol' fad' sol'2 |
re''2. re''2 re''8 re'' do''4 do'' sol' |
re''4 re''2 si'2. |
r4 do''2 r4 re''4. re''8 |
do''4 do''2 r4 r sib' |
la' la' la' sol' sol'2 |
sol'2. mib''4 sol''4. sol''8 |
fa''4 re'' do'' |
sol''4 sol''4. fa''8 |
mi''2. r4 fa''4. mib''8 |
re''2. r4 mib''4. re''8 |
do''4 sib'4. do''8 re''4 do''4. re''8 |
sib'2. r4 sib'4. la'8 |
sol'2. r4 re''4. do''8 |
si'4 mib''4. re''8 do''4 si'!4. do''8 do''1*3/4\fermata |
r2*3/2 r4 sib'2 |
r4 do'' do'' |
sol'4. sol'8 sol' sol' |
lab'4 fa'2 |
sib'4. sol'8 sol'4 fad' sol'2 re''2. |
re''2 re''8 re'' do''4 do'' sol' |
re'' re''2 si'2. |
r4 do''2 |
r4 re'' sib' |
r4 r8 do'' do'' do'' do''4 sib'2 do''4. sib'8 la'4 |
sol'4 sol'2 |
sol'2. |
mib''4 sol''4. sol''8 fa''4 fa''4. do''8 |
sol''4 sol''4. fa''8 mi''2. |
r4 fa''4. mib''8 re''2. r4 mib''!4. re''8 |
do''4 sib'4. do''8 re''4 do''4. re''8 |
sib'2. r4 sib'4. la'8 |
sol'2. r4 re''4. do''8 |
si'4 mib''4. re''8 do''4 si'!4. do''8 do''1*3/4\fermata |
R1*8 R1.*8 |
R2. |
r4 sib'2 r4 do''4. do''8 |
sib'4 sib'2 r4 r lab' |
sol' sol' sol' fad' sol'2 |
re''2. re''2 re''8 re'' |
do''4 do'' sol' re'' re''2 |
si'2. r4 do''2 r4 re''4. re''8 |
do''4 do''2 r4 r sib' |
la' la' la' sol' sol'2 |
sol'2. |
mib''4 sol''4. sol''8 fa''4 re'' do'' |
sol'' sol''4. fa''8 mib''1*3/4\fermata |
