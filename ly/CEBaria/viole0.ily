\clef "viole0" R2.*70 R1*8 |
r2. r4 mib' mib' |
fa'2. r |
r4 sol' sol' fa'2. |
r r4 fa' fa' |
sol'2. r4 do'' do'' |
lab' fa' fa' fa'2.~ |
fa'4 re' re' mib'2 do'4 |
sol'2 lab'4 sol'2. |
sol'2.~ |
sol' fa' |
sib'2 sol'4 re' fa'2 |
sol'4 mib'2 r4 sol' sol' |
re'2.~ re' |
r4 r sol' sol' re'2~ |
re'4 sol'2 lab'2. r4 sib' sib' |
mib'4 lab' fa' sol'2. |
r4 fa' fa' fa' mib'2 |
r4 sol'2 |
do''2 sol'4 do' fa'4. do'8 |
do''4 re'' sol' sol'1*3/4\fermata |
