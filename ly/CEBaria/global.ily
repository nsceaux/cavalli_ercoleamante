\key re \minor \tempo "Tou douxemans"
\digitTime\time 3/4 \midiTempo#160 s2.
\measure 6/4 s1.*3
\measure 9/4 s2.*3
\measure 6/4 s1.*5
\measure 3/4 s2.*2
\beginMark "Ritor[nello]" \measure 6/4 s1.*5
\measure 9/4 s2.*3
\measure 6/4 s1.
\measure 3/4 s2.*3
\measure 9/4 s2.*3
\measure 6/4 s1.*2
\measure 3/4 s2.*2
\measure 9/4 s2.*3
\measure 3/4 s2.*2
\measure 6/4 s1. s2. \beginMark "Ritor[nello]" s2.
\measure 9/4 s2.*3
\measure 6/4 s1.*3
\measure 9/4 s2.*3 \bar "||"
\time 4/4 \midiTempo#100 s1*8
\time 6/4 \midiTempo#160 s1.*8
\digitTime\time 3/4 \tempo "Douxemans" s2.
\measure 6/4 s1.*5
\measure 9/4 s2.*3
\measure 6/4 s1.*2
\measure 3/4 s2.
\measure 6/4 s1.*2 \bar "|."


