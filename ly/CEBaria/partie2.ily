\clef "viole2" R2. |
r4 re'2 r4 fa'4. fa'8 |
sol'4 sol'2 r4 r re' |
mib'4. mib'8 mib'4 la la mib'! |
re' la2 re'4 sib sol do' la fa |
re' la re sol2. |
r4 do'2 r4 sib4. sib8 |
do'4 do'2 r4 r sib |
do' re' re' si sol2 |
si2. do'4 do'4. sol8 |
la4 fa do' |
sol re' sol |
do' do'4. do'8 do'2. |
r4 re'4. re'8 sib4 do'4. do'8 |
fa'4 sib2 sol4 do'2 |
re'2. r4 re'4. re'8 |
sib2. r4 fa'4. do'8 |
sol'4 do'2 do'4 sol' re' mi'1*3/4\fermata |
r2*3/2 r4 re'2 |
r4 fa' fa' |
sol'4. mib'8 mib' mib' |
fa'4 fa'2 |
sol'4. sol'8 sol4 re' sol sol' fad'2. |
re'2 sol4 do'2 sol4 |
re' la re sol2. |
r4 mib'2 |
r4 re' re' |
re'8 do' fa'2 r4 r8 mib' mib' mib' la2 re'4 |
si4 sol2 |
si2. |
do'4 do'4. sol8 la4 fa do' |
sol re' sol do' do'4. do'8 |
do'2. r4 re'4. re'8 sib4 do'4. do'8 |
fa'4 sib2 sol4 do'2 |
re'2. r4 re'4. re'8 |
sib2. r4 fa'4. do'8 |
sol'4 do'2 do'4 sol' re' mi'1*3/4\fermata |
R1*8
R1.*8 |
R2. |
r4 re'2 r4 fa'4. fa'8 |
sol'4 sol'2 r4 r re' |
mib' mib' mib' la la mib'! |
re' la2 re'4 sib sol |
do' la la re' la re |
sol2. r4 do'2 r4 sib4. sib8 |
do'4 do'2 r4 r sib |
do' re' re' si sol2 |
si2. |
do'4 do'4. sol8 la4 fa do' |
sol re' sol do'1*3/4\fermata |
