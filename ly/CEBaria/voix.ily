\clef "vsoprano" <>^\markup\character Deianira
r4 mib''2 |
r4 re''4. re''8 re''4 do''2 |
r4 r sib' sib' lab'2 |
r4 r sol' la' do''2 |
fad'4 re''2 r4 sib'4. la'8 sol'4 la'4. sib'8 |
sib'4 la'2 sol'4 sol''2 |
r4 mib''4. mib''8 mib''4 re''2 |
r4 r do'' do'' sib'2 |
r4 r la' si' do''2 |
si'4 mib''2 r4 do''4. \ficta sib'8 |
la'4 si'4. do''8 |
mib''4 re''2 |
do''2. r2*3/2 |
R2.*11 |
r4 mib''2 r4 re'' re'' |
re''( do''8) do'' do'' do'' |
do''4 sib'4. sib'8 |
sib'4 lab'8 do'' lab' sol' |
sol'4. sol'8 sol' sol' la'4 do''2 fad'4 re''2 |
r4 sib'4. la'8 sol'4 la'4. sib'8 |
sib'4 la'( sol') sol' sol''2 |
r4 mib'' mib'' |
\override AccidentalSuggestion.avoid-slur = #'outside
\ficta mib''( re''8) re'' re'' re'' |
re''4 do''4. do''8 do''4 sib'8 sib' sib' la' la'4. la'8 la' sib' |
si'4 do''2 |
si'4 mib''2 |
r4 do''4. sib'8 la'4 si'4. do''8 |
mib''4 re''( do'') do''1*3/4\fermata |
R2.*12 |
r4 sol'8 sol' sol'4 sol' |
do'' do''8[ re''] sib'4. la'8 |
sol'4 sol'8 sib' lab'4 sol' |
mib' fa'8[ sol'] sol'2 |
sol'4 si'8 si' do''4 \ficta sib' |
sib' la' sib'4. do''8 |
re''4 la'8 do'' fad'4 sib' |
sol'4. fad'8 sol'2 |
sol'2. r4 do'' do'' |
la'2. r4 sib' sib' |
sol'2. do''2 sib'4 |
lab'4. lab'8 sol'4 fa'2. |
mib'2. r4 mib'' mib'' |
do''2. r4 re'' re'' |
si'2. sol'2 la'4 |
si'4. si'8 do''4 do''2 si'!4( |
do'') mib''2 |
r4 re''4. re''8 re''4 do''2 |
r4 r sib' sib' lab'2 |
r4 r sol' la' do''2 |
fad'4 re''2 r4 sib'4. la'8 |
sol'4 la'4. sib'8 sib'4 la'( sib') |
sol'4 sol''2 r4 mib'' mib'' mib'' re''2 |
r4 r do'' do'' sib'2 |
r4 r la' si' do''2 |
si'4 mib''2 |
r4 do''4. sib'8 la'4 si'4. do''8 |
mib''4 re''( do'') do''1*3/4\fermata |
