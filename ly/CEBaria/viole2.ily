\clef "viole2"  R2.*70 R1*8 |
R1. |
r4 la la sib2. |
r4 do' do' do'2 re'4 |
r do' do' sib2. |
sib4 sol sol lab2.~ |
lab4 do' do' lab lab lab |
sol si si do'2 la4 |
sol2 do'4 sol2. |
sol2. |
r4 r sib do' lab2 |
r4 sib4. sol8 lab4 fa2 |
r4 sol do' la mib' la |
r4 la re' sol2. |
r4 r do' la2 re'4 |
r4 sib2 mib' do'4 re' sol'2 |
r4 do' lab sib sol2 |
la2 fa4 r2. |
r4 do'2 |
sol do' fa4 lab |
sol2 do'4 do'1*3/4\fermata |
