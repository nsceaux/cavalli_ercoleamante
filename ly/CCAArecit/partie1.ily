\clef "viole1" re'4 |
mib' fa' sol' fa' |
fa'2 mib'4 sol' |
fa'4 la' sib' fa'8 sol' |
la'4 la8 re' do'4. do'8 |
sib2 r |
sib4 fa' mib' do' |
re' sib2 do'8 re' |
mi'4 fa'4. fa'8 mi'4 |
fa' do' fa'4. fa8 |
sol4 la sol do' |
la2 re'4 mib' |
re' sol re'4. do'8 |
si4 re'4. mib'8 fa'4 |
sol' fa' mib' do' |
fa'8 mib' re' do' sib4. sib8 |
do'4. do'8 sib2 |
sib4 fa' re' fa' |
sib sol la sol |
do' fa' mi'4. fa'8 |
fa'4. do'8 fa4 do'8 re' |
mib'4. mib'8 re'4. sol'8 |
mi'4 fa' sol' la' |
sol'8 do' mib'4 re' mib'8 do' |
re'4 mib' re'4. la8 |
si4 sol4. re'8 fa'4 |
sol' fa' mib'8 re' do'4 |
fa'8 mib' re' do' sib4. sib8 |
do'4 fa' fa'8 re'4 sib8 |
re'4. re'8 sol'4 re' |
sol' fa' fa' sib |
mib' sib mib' do' |
re'2 r |
re'4 fa'4 sol'4. do'8 |
fa'4 do'~ do'8 fa' do' re' |
mib'4. re'8 sol'2 |
sol'4. fad'8 sol' la' fad'4 |
sol' re' sol mib' |
la re' la4. la8 |
sib sol mib'4 re'2~ |
re'4 sol4 sib4. re'8 |
sol'4 fa' mib' fa'~ |
fa'8 mib'8 re' do' re'4 fa' |
sib4. mib'8 do'4 sib |
fa' re' mi'8 fa' sol'4 |
fa'8 re' mib' do' do' mib' fa' sol' |
fa'2 re'4 fa' |
mib'8 fa' sol' fa' mib' do' fa'4 |
re' re' r re'4 |
mib' fa' sol' fa' |
fa'2 mib'4 sol' |
fa'4 la' sib' fa'8 sol' |
la'4 la8 re' do'4. do'8 |
sib1\fermata |
