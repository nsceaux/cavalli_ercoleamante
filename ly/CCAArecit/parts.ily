\piecePartSpecs
#(if (eqv? #t (ly:get-option 'pygmalion))
     '((dessus #:score-template "score-dessus-voix")
       (parties #:score-template "score-parties-voix")
       (violes #:score-template "score-parties-violes-voix")
       (basse #:score "score-basse"))
     '((basse #:score-template "score-voix")))
