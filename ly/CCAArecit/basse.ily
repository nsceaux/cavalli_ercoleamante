\clef "basse" sib,4 |
do re mib fa |
sib,2 mib |
fa4 la, sib,8 do re mib |
fa4. re8 mib do fa fa, |
sib,2 r |
sib,4 re mib fa |
sib,2 sib4 la |
sol fa do' do |
fa2 fa4 re |
mib re mib do |
re2. do4 |
sib, do re re, |
sol,2. re4 |
mib re do fa |
sib, re mib re |
mib fa sib,2~ |
sib,4 la, sib, la, |
sol, do fa, sol, |
la, sib, do2 |
fa,2. fa4 |
mib fa sol sol, |
do re mib re |
mib do re do |
sib, do re re, |
sol,2. re4 |
mib re do fa |
sib, re mib re |
fa fa, sib,2~ |
sib,4 re8 re mib4 re |
mib fa sib8 la sol fa |
mib4 re mib fa |
sib,2 r | \allowPageTurn
sib,4 la,4 sol, do |
fa,2~ fa,4 fa |
mib fa sol sol, |
do2~ do4 re |
mib re mib do |
re2~ re4 do |
sib, do re re, |
sol,2~ sol,4 re |
mib re do fa, |
sib,2~ sib,4 la, |
sol, do fa, sol, |
la, sib, do2 |
fa4 mib re4. mib8 |
fa4 fa, sib, re |
mib4. re8 mib4 fa |
sib,2 r4 sib,4 |
do re mib fa |
sib,2 mib |
fa4 la, sib,8 do re mib |
fa4. re8 mib do fa fa, |
sib,1\fermata |

