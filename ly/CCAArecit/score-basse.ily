\score {
  \new StaffGroupNoBar \with { \haraKiriFirst } <<
    \pygmalion\new StaffGroupNoBracket <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
      \new Staff << \global \includeNotes "partie1" >>
      \new Staff << \global \includeNotes "partie2" >>
    >>
    \new Staff \with { \tinyStaff \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'basse \includeNotes "voix"
    >> \keepWithTag #'basse { \set fontSize = #-2 \includeLyrics "paroles" }
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \pygmalion\origLayout {
        s4 s1*5\pageBreak
        s1*4\break s1*5\pageBreak
        s1*4\break s1*4\pageBreak
        s1*4\break s1*5\pageBreak
        s1*4\break s1*4\pageBreak
        s1*4\break s1*4\pageBreak
      }
    >>
  >>
  \layout { }
}
