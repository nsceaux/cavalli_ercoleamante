\clef "viole1" r2 r4 mi'4 |
fa'2 r4 do' re' sol' sol'2 |
r4 la' mi' sol' |
do'2 r4 mi' |
la2 mi' mi'1\fermata |
R2.*20 |
re'1 re'2 fad' |
si4 mi' la4. si8 |
si2 si4. si8 |
sol2 la |
si red'8 dod' red'!4 mi' red' |
si2 si4 |
do' si do' |
re' mi' re' |
r4 do'2~ |
do'8 la si4 re' |
si mi'2 |
r4 re' do' |
la re' la'2~ |
la'4. mi'8 mi'2 |
mi'4 la'2 mi'4 |
fa'2 r4 do' re' sol' sol'2 |
r4 la' mi' sol' |
do'2 r4 mi' |
la2 mi' mi'1\fermata |
