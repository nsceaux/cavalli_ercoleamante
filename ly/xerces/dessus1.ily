\clef "dessus" r4 fa'' mi''2 |
r4 re'' do''2 si'4 do''4 do''4. si'8 |
do''4 la'' sol''2 |
r4 fa'' mi''2 |
re''4 do'' si'2 la'1\fermata |
R2. |
r4 si'4. si'8 do''4 la'2 |
sold'2 la'4 la'2 sold'!4 |
r r do''8 re'' |
mi''4 la'' la'8 mi' |
si'4 mi''2 |
mi''2. |
mi''2 red''4 |
mi''2. |
r4 sol''4. sol''8 la''2 mi''4 |
la' re'' fad'' |
re'' re''8 do'' si'4 |
sol' do''4. si'8 |
la'4 re''2 |
mi''8 re'' mi''4 r |
la'8 \ficta fad' sol'4 r |
re''2. |
si'1 |
si'2 fad'' |
sol''4 mi'' fad''2~ |
fad''2 r4 sol''8 red'' |
mi''2 do''4. mi''8 |
red''2 fad''8 mi'' red''!4 dod'' red'' |
mi''2. |
r4 r do''8 si' |
la'4 sold' fad' |
mi'' mi' mi'' |
re'' si' fad' |
si' mi'' do''~ |
do'' re'' mi'' |
re''2. do''4 |
si'2 mi'' |
r4 fa'' mi''2 |
r4 re'' do''2 si'4 do'' do''4. si'8 |
do''4 la'' sol''2 |
r4 fa'' mi''2 |
re''4 do'' si'2 la'1\fermata |
