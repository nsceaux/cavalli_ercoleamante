\clef "viole2" r2 r4 sol'4 |
re'2 r4 sol si sol sol2 |
sol4 do'2 re'4 |
mi' la r si |
re' la2 si4 dod'1\fermata |
R2.*20 |
si1 |
si2 la |
sol4. sol'8 fad'2~ |
fad'4 fad' mi' si |
mi1 |
fad2. si4 fad'2 |
mi' mi'4 |
mi'2 mi'4 |
re'8 do' si la si4~ |
si mi la |
re fad si |
mi sold do' |
la2. |
re'2. la4 |
la2 si |
do'2. sol'4 |
re'2 r4 sol si sol sol2 |
sol4 do'2 re'4 |
mi' la r si |
re' la2 si4 dod'1\fermata |
