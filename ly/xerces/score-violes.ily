\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket \with { \haraKiri
    } <<
      \new Staff << \global \includeNotes "partie1" >>
      \new Staff << \global \includeNotes "partie2" >>
    >>
    \new Staff \with { \tinyStaff \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'basse \includeNotes "voix"
    >> \keepWithTag #'basse { \set fontSize = #-2 \includeLyrics "paroles" }
    \new Staff << \global \includeNotes "basse" >>
  >>
  \layout { }
}
