\tag #'(stroffa1 basse) {
  Lu -- ci mi -- e, voi che mi -- ra -- ste
  quel bel sol __ che m’ab -- ba -- gliò,
  voi che sem -- pli -- ci cer -- ca -- ste
  il crin d’or __ che mi le -- gò,
  voi che del mio pe -- nar __ la col -- pa a -- vet -- te
  di do -- ver la -- gri -- mar __ non vi do -- le -- te. __
}
\original\tag #'stroffa2 {
  Oc -- chi mie -- i voi che go -- de -- ste
  lo splen -- do __ d’u -- na bel -- tà,
  ch’a mi -- rar -- la par ce -- le -- ste,
  ma in -- fer -- na -- le al duol che dà,
}
