\clef "dessus" r2 r4 si' |
la'2 r4 sol' fa' mi' re'2 |
mi' r4 re'' |
do''2 r4 si' |
la' la' la'4. sold'8 la'1\fermata |
R2. |
r4 sol'4. sol'8 la'4 re''2 |
mi'' fa''4 mi''2 si'4 |
r r sol'8 si' |
do''4 do''2 |
fad'4 si'2 |
do''8 si' do''4 r |
si'8 la' si'4 r |
si'2. |
r4 do''4. re''8 mi''2 sol''4 |
la'' fad'' la'' |
sol''2 re''8 re'' |
mi''4 mi''2 |
re''4 sol'2 |
sol'2. |
sol'2 fad'4 |
sol'2. |
sol'1 |
sol'2 la' |
si'4 sol' la'2 |
fad'4 si si'2~ |
si' la' |
fad'2. fad'!2 fad'8 fad' |
si'4 la' si' |
mi'2. |
r4 r re''8 do'' |
si'4 sold' do'' |
la' re'' si' |
sold'2 mi'4 |
r la' mi' |
fa'2. la'4 |
mi''4 re''8 do'' si'2 |
la' r4 si' |
la'2 r4 sol' fa' mi' re'2 |
mi' r4 re'' |
do''2 r4 si' |
la' la' la'4. sold'8 la'1\fermata |
