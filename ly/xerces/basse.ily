\clef "basse" la,2. sol4 |
fa2 r4 mi re do sol2 |
do2. si4 |
la2. sol4 |
fa re mi2 la,1\fermata |
la,2. |
la4 sol2 fa2. |
mi2 re4 mi2.~ |
mi2 mi8 re |
do4 la,2 |
si,4 sol,2 |
la,2. |
si, |
mi, |
r4 do'4. si8 la2 sol4 |
fad2. |
sol2 sol8 fa |
mi4 do2 |
re4 si,2 |
do2. |
re |
sol, |
sol1~ |
sol2 fad |
mi red~ |
red2 mi4 mi8 si, |
do2~ do |
si,2. si,2 si8 la |
sold4 fad sold |
la2 la,4 |
re2. |
mi2 do4 |
re2. |
mi |
r4 fa do |
re2. red4 |
mi1 |
la,2. sol4 |
fa2 r4 mi re do sol2 |
do2 r4 si |
la2 r4 sol |
fa re mi2 la,1\fermata |
