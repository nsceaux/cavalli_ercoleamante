\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket \with { \haraKiri
    } <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
      \new Staff << \global \includeNotes "partie1" >>
      \new Staff << \global \includeNotes "partie2" >>
    >>
    \new Staff \withLyricsB <<
      \global \includeNotes "voix"
    >>
    \keepWithTag #'stroffa1 \includeLyrics "paroles"
    \keepWithTag #'stroffa2 \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*7\break s2.*7\pageBreak
        s2.*8\break s2.*5 s1*3\pageBreak
        s1*2 s2.*6\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
