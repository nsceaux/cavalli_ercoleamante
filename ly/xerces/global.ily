\key do \major
\beginMark "Ritor[nello]"
\time 4/4 \midiTempo#140 s1
\measure 2/1 s1*2
\measure 4/4 s1*2
\measure 2/1 s1*2 \bar "|."
\original\segnoMark
\digitTime\time 3/4 s2.
\measure 6/4 s1.*2
\measure 3/4 s2.*6
\measure 6/4 s1.
\measure 3/4 s2.*7
\time 4/4 s1*5
\digitTime\time 3/4 \measure 6/4 s1.
\measure 3/4 s2.*7
\time 4/4 s1*2 s4 \beginMark "Ritor[nello]" s2.
\measure 2/1 s1*2
\measure 4/4 s1*2
\measure 2/1 s1*2 \bar "|."
