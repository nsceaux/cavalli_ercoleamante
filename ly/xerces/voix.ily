\clef "vsoprano" R1*7 |
r4 mi''4. fa''8 |
fa''4 mi''2 r4 re''2 |
r4 do''4. si'8 do''4 si' mi''8 fad'' |
sol''2.~ |
sol''4\melisma fad''4.( mi''8 |
red''4)\melismaEnd sol''8([ fad''] sol''4) |
r sol''8([ fad''] sol''4) |
r sol''8([ fad''] sol''4) |
mi''4 sol''4. fad''8 |
mi''2 re''4 do''2 si'4 |
re'' la' re''8 do'' |
si'2.~ |
si'4\melisma la'4. sol'8 |
fad'4\melismaEnd si'8([ la'] si'4) |
r4 do''8[\melisma si'] do''4\melismaEnd |
r4 do''8[\melisma si'] do''4\melismaEnd |
si'2. |
re''2 re''8 re'' re'' re'' |
re''2( red''4)\melisma mi''8[ red''!] |
mi''4 do''8[ si'] do''4 la'8[ sol'] |
la'4 si'8[ fad'] sol'2~ |
sol'4.\melismaEnd sol'8 fad'4 fad'8 mi' |
si'2. si' |
r4 r mi''8 re'' |
do''4 re'' mi'' |
fa''\melisma re'' si' |
sold' do'' la' |
fad'\melismaEnd si'2 |
r4 do'' sold' |
la'2.~ |
la'4\melisma sib'( la'2)~ |
la'2.\melismaEnd sold'4( |
la'1)\fermata |
R1*6 |
