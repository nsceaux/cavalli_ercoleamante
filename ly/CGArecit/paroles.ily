O Dè -- a su -- bli -- me Dè -- a,
e qual nuo -- vo de -- si -- o
a quest’ u -- mi -- le al -- ber -- go og -- gi ti me -- na?

Ze -- lo dell’ o -- nor mi -- o
e del -- la fe -- de al -- tru -- i a me già sa -- cra, e da sa -- crar -- si, a cu -- i
e fro -- di, e vi -- o -- len -- ze al -- tri pre -- pa -- ra,
on -- de per fa -- re a ciò scher -- mo in -- no -- cen -- te
sol sol per u -- na brev’ ho -- ra
di con -- dur me -- co il Son -- no uo -- po mi fo -- ra.

Ohi -- mè di Gio -- ve all’ i -- re di nuo -- vo es -- por -- re
o -- gni mio ben vor -- ra -- i?
No, no, no, no, ciò non fia più ma -- i.

Non te -- mer non te -- mer Pa -- si -- the -- a,
che so -- lo è mio pen -- sie -- ro
di va -- ler -- mi di lui con men che nu -- mi
di già sog -- get -- ti al di lui pi -- gro im -- pe -- ro.

E di ciò m’as -- si -- cu -- ri?

S’an -- cor vuoi che te’l giu -- ri
sul ger -- ma -- no di lui lo sti -- gio Le -- te.

Bas -- ta Giu -- no: qui -- e -- te
son già le mie vo -- glie al tuo de -- sir sov -- ra -- no.

Por -- gi -- lo dun -- que a me, di -- va, pian pia -- no…
