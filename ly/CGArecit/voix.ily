\ffclef "vsoprano" <>^\markup\character Pasithea
r4 do'' la' la' |
r8 la' do'' sol' la'4 la' |
r la'8 la' la'4 la'8 sib' |
do''4 do''8 do'' do'' do'' do'' do'' |
do''4 do'' do'' do''8 re'' |
sib'4 sib' r2 |
\ffclef "vsoprano" <>^\markup\character Giunone
fad'4 fad' r8 la' fad' fad' |
fad'4 fad'8 fad' fad' fad' fad' fad' |
fad' fad' fad' sol' sol' sol'16 sol' sol'8 la' |
la'4 la'8 re' la' la' r la' |
do'' do'' do''8 do''16 re'' sib'8 sib' sib' sib'16 la' |
la'2 sol' |
la'4 la'8 la' la' la' la'4 |
la' la'8 sib' do''4 do'' |
r sib' sib'8 sib'16 sib' sib'8 sib' |
sib' la' la' la' la'4 la'8 la' |
la'4 sol' sol' sol'8 la' |
fa'4 fa'
\ffclef "vsoprano" <>^\markup\character Pasithea
r8 do'' fa'4 |
r8 do'' fa'' mib'' mib''4 re'' |
r8 re'' re'' re'' re''4 do'' |
do''8 do''16 do'' do''8 sib' re'' re'' r re'' |
sib' re'' sib'4 sib'8 sib' do'' re'' |
la' la' r4
\ffclef "vsoprano" <>^\markup\character Giunone
r re'8 re' |
fad'4 fad'8 fad' la'4 la'8 sol' |
la' la' r re'' la' la' la' la' |
la' la' la' la' la'4 la'8 sib' |
do'' do'' do'' re'' sib'4 sib'8 sib' |
sib'8 sib' sol'4 sol'8 sol' sol' sol' |
sol'4 sol'8 la' la'2 sol'1 |
\ffclef "vsoprano" <>^\markup\character Pasithea
r8 sib'16 sib' sol'8 sol'16 fad' la'8 la' r4 |
\ffclef "vsoprano" <>^\markup\character Giunone
re''8 re'' la' sib'16 do'' sib' sib' sib' sib' sib'8 sib'16 sib' |
sol'8 sol' do'' do'' la' la' r4 |
\ffclef "vsoprano" <>^\markup\character Pasithea
r4 la'8 fa' fa' fa' r mi' |
sol' sol' sol' do''16 sol' la'8 la'16 la' la'8 la' |
si'8 do'' do''8. si'16( do''2) |
\ffclef "vsoprano" <>^\markup\character Giunone
do''8 sol'16 sol' sol'8 la' sib'2 |
do''4 la'8 re'' sib'4.( la'8) la'1\fermata |
