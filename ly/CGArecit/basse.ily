\clef "basse" fa1 |
fa4 mi fa2 |
fa1~ |
fa~ |
fa |
sib, |
la,~ |
la,~ |
la,2 sol, |
fad,1~ |
fad,2 sol,4. do8 |
re2 sol, |
fa,1~ |
fa, |
re |
do |
sib,2 do |
fa,2~ fa, |
fa,1 |
fa, |
fa,2 sib, |
sol,1 |
la,2 re~ |
re1 |
re |
re~ |
re2 sol,~ |
sol,2 do~ |
do re sol,1 |
sol,2 re |
re sol |
mi fa |
fa,1 |
mi,2 fa, |
sol,8 fa, sol,4 do2 |
do sol, |
la,4. sib,8 do2 fa,1\fermata |
