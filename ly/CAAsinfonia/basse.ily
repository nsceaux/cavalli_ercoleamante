\clef "basse"
fa1 re2 la, |
re1 do2. sib,4 |
la,2 mi, sol,1 do\fermata |
r2 do' la mi |
fa1 do\fermata |
r2 fa2 re la, sib,1 |
fa,2 fa4 mib re do si,2 |
do\breve fa,1\fermata |
R2.*13 |
r4 r do la, fa,2 |
do4. sib,8 la,4 |
re re2 sib,2 do4 |
fa,4. sol,8 la,4 sib, do2 |
fa,2. |
r2 fa |
re la, sib,1 fa,2 fa4 mib |
re do si,2 do\breve fa,1\fermata |
