\clef "viole2"
do'1 sib2 la4 fa' |
re'1 sol2 do'~ |
do' do' re'4 do' re'2 do'1\fermata |
r2 do' do' do' |
do' fa' sol'1\fermata |
r2 fa' fa' fa' re'1 |
do'4 sib la do' fa'2 re' |
do'\breve do'1\fermata |
R2.*11 |
r4 r do' la fa2 |
do' do'4 do' do'2 |
do'4. do'8 mi'4 |
re' fa'2 re'4. fa'8 do'4 |
do'2. fa'4 do'2 |
do'2. |
r2 fa' |
fa' fa' re'1 do'4 sib la do' |
fa'2 re' do'\breve do'1\fermata |
