\score {
  \new StaffGroup <<
    \new Staff << \global \includeNotes "dessus1" >>
    \new Staff << \global \includeNotes "dessus2" >>
    \new Staff << \global \includeNotes "partie1" >>
    \new Staff << \global \includeNotes "partie2" >>
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout { s1*19\pageBreak s2.*16\break }
    >>
  >>
  \layout { system-count = 6 }
  \midi { }
}
