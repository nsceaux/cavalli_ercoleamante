\clef "viole1"
fa'1 fa'2 la' |
la' fa' sol'2. sol'4 |
la'2 sol' sol'1 sol'\fermata |
r2 sol' fa' mi' |
la'2. si'4 do''1\fermata |
r2 do'' sib' la' fa'1 |
fa'2. sol'4 sib'2 sol' |
sol' fa' sol'1 fa'\fermata |
R2. |
r2*3/2 r4 r fa' |
mi' do'2 fa'4. mi'8 re'4 |
do' fa'2 re' sol'4 |
do'4. re'8 mi'4 fa' sol'2 |
do'2. r2*3/2 |
r4 r mi' fa' la'2 |
mi' sol'4 fa' fa'2 |
sol'4. sol'8 do'4 |
fa' la'2 sol' sol'4 |
fa'2. re'4 sol'4. la'8 |
la'2. |
r2 do'' |
sib' la' fa'1 fa'2. sol'4 |
sib'2 sol' sol' fa' sol'1 fa'\fermata |
