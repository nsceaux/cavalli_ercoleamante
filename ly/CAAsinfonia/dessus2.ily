\clef "dessus"
do''1 re''2 fa'' |
fa''1 mi''2. re''4 |
do''2 do'' si'8 sol' do''2 si'4 do''1\fermata |
r2 mi'' fa'' sol'' |
fa''4 mi''8 re'' do''4 re'' mi''1\fermata |
r2 fa'' fa'' la'' re''2. mi''4 |
fa''4 mi''8 re'' do''2 re''2. re''4 |
mi''4 do'' fa''1 mi''2 fa''1\fermata |
r4 r do'' |
la' fa'2 do''4. sib'8 la'4 |
sol' do''2 la' si'4 |
do'' la'2 re''4. do''8 si'4 |
mi''4. re''8 do''4 do'' do''4. si'8 |
do''2. r2*3/2 |
r4 r sol' la' do''2 |
sol' mi''4 fa'' la''2 |
mi''4. sol''8 la''4 |
fa'' re''2 sol''4. fa''8 mi''4 |
la''4. sol''8 fa''4 fa'' fa''4. mi''8 |
fa''2. |
r2 fa'' |
fa'' la'' re''2. mi''4 fa'' mi''8 re'' do''4 re'' |
re''2. re''4 mi'' do'' fa''1 mi''2 fa''1\fermata |

