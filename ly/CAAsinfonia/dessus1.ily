\clef "dessus"
la''1 sib''2 do''' |
do''' si'' do''' sol'' |
fa'' sol''2. fa''8 mi'' re''2 mi''1\fermata |
r2 sol'' la'' do''' |
la''1 sol''\fermata |
r2 la'' sib'' do''' sib''8 la'' sol'' fa'' sol''2 |
la''2. sol''4 fa''2 sol'' |
do'''4 sib'' la'' sib'' sol''1 la''\fermata |
R2.*3 |
r2*3/2 r4 r fa'' |
mi'' do''2 fa''4. mi''8 re''4 |
sol''4. fa''8 mi''4 la'' re''4. do''8 |
do''2 sol''4 mi'' do''2 |
sol''2. r2*3/2 |
r4 r sol'' la'' do'''2 |
sol''2 do'''4 la'' fa''2 sib''4. la''8 sol''4 |
do'''4. sib''8 la''4 sol'' sol''4. fa''8 |
fa''2. |
r2 la'' |
sib'' do''' sib''4 la''8 sol'' fa''4 sol'' la''2. sol''4 |
fa''2 sol'' do'''4 sib'' la'' sib'' sol''1 la''\fermata |

