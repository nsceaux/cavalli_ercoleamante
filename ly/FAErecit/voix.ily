<<
  \pygmalion\tag #'(bussiride basse) {
    \clef "petrucci-c3/bass" <>^\markup\character-text Bussiride re d’Egitto
    r4 fa re |
    sol sol r |
    r lab4. sol8 fa4 mib fa |
    sol( sol,2) |
    do4. do8 do do do re |
    mi mi fa sol mi4 mi8 fa |
    fa4 fa fa8 fa fa fa16 sol |
    mib4 mib r8 mib mib mib |
    \ficta mib4 mib8 mib mib8. mib16 mib8 re |
    re re re re sol4 sol8 sol |
    mi4. do8 fa4 <<
      \original {
        fa4 |
        fa2. mi4( fa1) |
        r4 la re |
        sol sol8 sol sol fad |
        fad8. fad16 fad4 r8 la |
        re4 re8 re re re re mib |
        mib4 mib8 mib mib mib mib re |
        fa fa fa fa sib4 sib8 fa |
        sol4. sol8 la sib sol sol |
        R1 |
        sib4 sol sol mib4. mib8 mib4 |
        lab fa fa re4. re8 re4 |
        mib mib mib si,2. |
        do4 do do re4. re8 re4 |
        mib mib mib fa4. fa8 fa4 |
        lab re4. mib8 do2. |
      }
      \pygmalion { sib,4 | do2 fa, | }
    >>
    r4 fa8 fa fa4 fa8 fa |
    <<
      \original {
        re4 re r2 |
        r4 sib8 sib sib4 sib8 sib sol4 sol r2 |
      }
      \pygmalion {
        sib,4 sib, r4 sib8 sib |
        sib4 sib8 sib mib4 mib |
      }
    >>
    r4 sib sol lab fa2 |
    sol4 sol mib fa2 sol4 |
    mib2 fa4 re2 fa4 |
    sib4 la << \original { sol la( sol2) } \pygmalion { sib4 do'2. } >> |
    fa2. r4 sol mib |
    fa re2 r4 do' lab |
    sib sol2 lab4 lab fa |
    sol2 lab4 fa2 sol4 |
    mib2 mib4 |
    sib4 sol sib~ |
    sib sib4. la8( sib2.) |
    R2.*19
  }
  \original\tag #'(bussiride basse) {
    \clef "vhaute-contre" <>^\markup\character-text Bussiride re d’Egitto
    r4 fa' re' |
    sol' sol' r |
    r lab'4. sol'8 fa'4 mib' re' |
    mib'( re'2) |
    do'4. do'8 do' do' do' re' |
    mi' mi' fa' sol' mi'4 mi'8 fa' |
    fa'4 fa' fa'8 fa' fa' fa'16 sol' |
    mib'4 mib' r8 mib' mib' mib' |
    \ficta mib'4 mib'8 mib' mib'8. mib'16 mib'8 re' |
    re' re' re' re' sol'4 sol'8 sol' |
    mi'4. do'8 fa'4 fa' |
    fa'2. mi'4( fa'1) |
    r4 la' re' |
    sol' sol'8 sol' sol' fad' |
    fad'8. fad'16 fad'4 r8 la' |
    re'4 re'8 re' re' re' re' mib' |
    mib'4 mib'8 mib' mib' mib' mib' re' |
    fa' fa' fa' fa' sib'4 sib'8 fa' |
    sol'4. sol'8 la' sib' sol' sol' |
    R1 |
    sib'4 sol' sol' mib'4. mib'8 mib'4 |
    lab' fa' fa' re'4. re'8 re'4 |
    mib' mib' mib' si2. |
    do'4 do' do' re'4. re'8 re'4 |
    mib' mib' mib' fa'4. fa'8 fa'4 |
    lab' re'4. mib'8 do'2. |
    r4 fa'8 fa' fa'4 fa'8 fa' |
    re'4 re' r2 |
    r4 sib'8 sib' sib'4 sib'8 sib' sol'4 sol' r2 |
    r4 sib' sol' lab' fa'2 |
    sol'4 sol' mib' fa'2 sol'4 |
    mib'2 fa'4 re'2 fa'4 |
    sib'4 la' sol' la'( sol'2) |
    fa'2. r4 sol' mib' |
    fa' re'2 r4 do'' lab' |
    sib' sol'2 lab'4 lab' fa' |
    sol'2 lab'4 fa'2 sol'4 |
    mib'2 mib'4 |
    sib'4 sol' sib'~ |
    sib' sib'4. la'8( sib'2.) |
    R2.*19
  }
  \tag #'vsoprano {
    \clef "vsoprano" R2.*5 \pygmalion R1*8 \original { R1*9 R2.*3 R1*5 R1.*6 R1 } R1*3 R1.*10
    <>^\markup\character Coro
    r4 fa'' re'' mib'' do''2 |
    re''4 re'' sib' do''2 re''4 |
    sib'2 do''4 la'2 sib'4 |
    sol' sol' la' re'' sib' sib' |
    sol' do'' la' sol'2. |
    la'4 do'' re'' sib' do''2 |
    la'4 la' re'' do''2 re''4 |
    sib'2 sib'4 sol'2 do''4 |
    la' sib' sib' sib'2 la'4( sib'1*3/4)\fermata |
  }
  \tag #'valto {
    \clef "valto" R2.*5 \pygmalion R1*8 \original { R1*9 R2.*3 R1*5 R1.*6 R1 } R1*3 R1.*10
    r2*3/2 r4 do'' la' |
    sib' sol'2 la'4 la' fa' |
    sol'2 la'4 fa'2 sol'4 |
    mi'2 fa'4 re' re' sol' |
    mi' do' fa' fa'2 mi'4( |
    fa') la' sib' sol' la'2 |
    fa'4 fa' sib' la'2 la'4 |
    sol'2 fa'4 mib'2 sol'4 |
    fa' fa' fa' fa'2. fa'1*3/4\fermata |
  }
  \tag #'vtenor {
    \clef "vtenor" R2.*5 \pygmalion R1*8 \original { R1*9 R2.*3 R1*5 R1.*6 R1 } R1*3 R1.*10
    R2.*2 |
    r2*3/2 r4 fa' re' |
    mib' do'2 re'4 re' sib |
    do'2 la4 sib2 sib4 |
    do' la fa do'2. |
    fa4 fa' fa' mib' mib'2 |
    re'4 re' fa' fa'2 fa'4 |
    re'2 re'4 sib2 mib'4 |
    do' re' re' do'2. re'1*3/4\fermata |
  }
  \tag #'vbasse {
    \clef "vbasse" R2.*5 \pygmalion R1*8 \original { R1*9 R2.*3 R1*5 R1.*6 R1 } R1*3 R1.*10
    R2.*10 |
    r4 fa re mib do2 |
    re4 re sib, fa2 re4 |
    sol2 re4 mib2 mib4 |
    fa re sib, fa2. sib,1*3/4\fermata |
  }
>>
