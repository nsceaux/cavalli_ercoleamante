\key fa \major
\digitTime\time 3/4 \midiTempo#160 s2.*2
\measure 6/4 s1. \measure 3/4 s2.
\time 4/4 \midiTempo#80
\pygmalion s1*8
\original {
  s1*7 \measure 2/1 s1*2
  \digitTime\time 3/4 s2.*3
  \time 4/4 \midiTempo#80 s1*5
  \time 6/4 \midiTempo#160 s1.*6
}
\time 4/4 << \original { s1*2 \measure 2/1 s1*2 } \pygmalion s1*3 >>
\time 6/4 s1.*8 \measure 3/4 s2.*2 \measure 6/4 s1. \bar "|."
\time 6/4 s1.*8 \measure 9/4 s2.*3 \bar "|."
