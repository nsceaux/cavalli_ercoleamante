\piecePartSpecs
#`((dessus #:score "score-dessus"
           #:music , #{ s2.*5 s1*8 \original { s1 s2.*3 s1*5 s1.*6 s1 } s1*3 s1.*10\break #})
   (parties #:score "score-parties"
           #:music , #{ s2.*5 s1*8 \original { s1 s2.*3 s1*5 s1.*6 s1 } s1*3 s1.*10\break #})
   (violes #:score "score-violes"
           #:music , #{ s2.*5 s1*8 \original { s1 s2.*3 s1*5 s1.*6 s1 } s1*3 s1.*10\break #})
   (basse #:score "score-basse"
           #:music , #{ s2.*5 s1*8 \original { s1 s2.*3 s1*5 s1.*6 s1 } s1*3 s1.*10\break #}))
