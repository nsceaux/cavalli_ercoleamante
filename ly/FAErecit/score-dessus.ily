\score {
  <<
    \new Staff \with { \tinyStaff \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'basse \includeNotes "voix"
    >> \keepWithTag #'basse { \set fontSize = #-2 \includeLyrics "paroles" }
    \new ChoirStaff <<
      \new Staff \with { \haraKiriFirst } \withLyrics <<
        \global \keepWithTag #'vsoprano \includeNotes "voix"
      >> \keepWithTag #'vsoprano { \includeLyrics "paroles" }
      \new Staff \with { \haraKiriFirst } \withLyrics <<
        \global \keepWithTag #'valto \includeNotes "voix" \clef "treble"
      >> \keepWithTag #'valto { \includeLyrics "paroles" }
    >>
    \new GrandStaff \with { \haraKiriFirst } <<
      \new Staff <<
        $(if (*instrument-name*)
             (make-music 'ContextSpeccedMusic
               'context-type 'GrandStaff
               'element (make-music 'PropertySet
                          'value (make-large-markup (*instrument-name*))
                          'symbol 'instrumentName))
             (make-music 'Music))
        $(or (*score-extra-music*) (make-music 'Music))
        { s2.*5 s1*8 \original { s1 s2.*3 s1*5 s1.*6 s1 } s1*3 s1.*10
          s1. \override MultiMeasureRest.transparent = ##t 
          \noHaraKiri R1.*5 }
        \global \includeNotes "dessus1"
      >>
      \new Staff <<
        { s2.*5 s1*8 \original { s1 s2.*3 s1*5 s1.*6 s1 } s1*3 s1.*10
          s1. \override MultiMeasureRest.transparent = ##t 
          \noHaraKiri R1.*6 }
        \global \includeNotes "dessus2" >>
    >>
  >>
  \layout {
    system-count = #(*system-count*)
    indent = #(if (*instrument-name*)
                  largeindent
                  (or (*score-indent*) smallindent))
  }
}
