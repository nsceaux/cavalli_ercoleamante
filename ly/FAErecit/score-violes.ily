\score {
  \new StaffGroupNoBar \with { \haraKiriFirst } <<
    \new Staff \withLyrics <<
      \global \keepWithTag #'valto \includeNotes "voix" \clef "alto"
    >> \keepWithTag #'valto \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'vtenor \includeNotes "voix" \clef "alto"
    >> \keepWithTag #'vtenor \includeLyrics "paroles"
    \new Staff \withLyrics <<
      { s2.*5 s1*8 \original { s1 s2.*3 s1*5 s1.*6 s1 } s1*3 s1.*10\break
        s2\noHaraKiri }
      \global \keepWithTag #'vbasse \includeNotes "voix"
    >> \keepWithTag #'vbasse \includeLyrics "paroles"
    \new Staff \with { \tinyStaff } \withLyrics <<
      \global \keepWithTag #'bussiride \includeNotes "voix"
    >> \keepWithTag #'bussiride { \set fontSize = -2 \includeLyrics "paroles" }
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
    >>
  >>
  \layout { }
}
