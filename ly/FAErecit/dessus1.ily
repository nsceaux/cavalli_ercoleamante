\clef "dessus" R2.*5 \pygmalion R1*8 \original { R1*9 R2.*3 R1*5 R1.*6 R1 } R1*3 R1.*10
\allowPageTurn
s1.*4 |
s2. r4 sib'' sol'' |
la'' la'' fa'' sol'' mib''2 |
fa''4 fa'' fa''8 sol'' la''2 la''4 |
sib''2 fa''4 sol'' sol'' sol'' |
la'' fa'' sib'' fa''2. fa''1*3/4\fermata |
