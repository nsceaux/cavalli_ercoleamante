\clef "basse"
<<
  \tag #'basse-continue {
    sib,2. |
    <<
      \original {
        r4 sib sol |
        do'2. fa |
        sol |
      }
      \pygmalion {
        r4 si, sol, |
        do2. fa, |
        sol, |
      }
    >>
    do1~ |
    do |
    <<
      \original { la,1~ | la,~ | la, | }
      \pygmalion {
        fa,1~ |
        fa,~ |
        fa,2 la, |
      }
    >>
    sib,1 |
    do2 la,4 sib, |
    <<
      \original {
        do1 fa, |
        fa2. |
        mib |
        re |
        si,1 |
        do1 |
        re |
        mib2 do |
        re sol, |
        mib2. lab |
        fa sol |
        do sol |
        mib fa |
        sol lab |
        fa4 sol2 do2. |
        sib,1~ |
        sib, |
        sib, mib |
      }
      \pygmalion {
        do2 fa, |
        fa,1 |
        sib,2 re,~ |
        re,2 mib,2 |
      }
    >>
    mib2. r4 fa re |
    mib do2 re4 re sib, |
    do2 la,4 sib,2 la,4 |
    sol, fa, sib, do2. |
    fa,4 fa re mib do2 |
    r4 sib sol lab fa2 |
    sol4 mib2 fa4 re2 |
    mib do4 re2 sib,4 |
    do2 do4 |
    re4 \ficta mib2 |
    fa2. sib,1*3/4\fermata | \allowPageTurn
    %%
    \clef "alto" r2*3/2 r4 do'' la' |
    sib' sol'2 la'4 \clef "tenor" fa' re' |
    mib' do'2 re'4 re' sib |
    do'2 la4 sib2 sib4 |
    do' la fa do'2. |
    fa4 \clef "bass" fa re mib do2 |
    re4 re sib, fa2 re4 |
    sol2 re4 mib2 mib4 |
    fa re sib, fa2. sib,1*3/4\fermata |
  }
>>