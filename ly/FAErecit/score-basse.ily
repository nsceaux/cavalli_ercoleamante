\score {
  \new ChoirStaff \with { \haraKiriFirst } <<
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      { s2.*5 s1*8 \original { s1 s2.*3 s1*5 s1.*6 s1 } s1*3 s1.*10 s2 \noHaraKiri }
      \global \keepWithTag #'vbasse \includeNotes "voix"
    >> \keepWithTag #'vbasse { \includeLyrics "paroles" }
    \new Staff \with { \tinyStaff \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'basse \includeNotes "voix"
    >> \keepWithTag #'basse { \set fontSize = #-2 \includeLyrics "paroles" }
    \new Staff <<
      $(or (*score-extra-music*) (make-music 'Music))
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
    >>
  >>
  \layout { }
}
