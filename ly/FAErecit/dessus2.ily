\clef "dessus" R2.*5 \pygmalion R1*8 \original { R1*9 R2.*3 R1*5 R1.*6 R1 } R1*3 R1.*10
s1.*5 |
r4 fa'' sib'' sib'' la''2 |
re''4 re'' re''8 mi'' fa''2 fa''4 |
sol''2 sib''4 mib'' mib'' sib' |
fa'' sib' fa'' do''4. re''8 mib'' fa'' re''1*3/4\fermata |
