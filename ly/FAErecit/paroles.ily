\tag #'(bussiride basse) {
  Pe -- ra pe -- ra mo -- ra mo -- ra l’i -- ni -- quo,
  che dell’ e -- te -- reo Gio -- ve,
  in -- gra -- tis -- si -- mo al pa -- ri,
  ch’in le -- git -- ti -- mo fi -- glio,
  di sa -- cer -- do -- te, e vit -- ti -- me più de -- gne,
  con sa -- cri -- le -- ge man spo -- gliò l’al -- ta -- ri.
  \original {
    Pe -- ra pe -- ra l’a -- bo -- mi -- ne -- vo -- le; ma pe -- ra
    del -- la più cru -- da mor -- te,
    che per e -- sem -- pio e -- ter -- no,
    in -- ven -- tar pos -- sa mai l’i -- ra -- to in -- fer -- no.
    Quan -- ti mai stra -- ti -- i,
    nei ne -- gri spa -- ti -- i,
    Plu -- to a -- du -- nò
    tut -- ti s’u -- nis -- chi -- no,
    et as -- sa -- lis -- chi -- no,
    chi ne sve -- nò:
  }
  che più dun -- que s’as -- pet -- ta?
  Che più dun -- que s’as -- pet -- ta?
  
  Pe -- ra pe -- ra mo -- ra il cru -- del,
  sù sù sù sù ven -- det -- ta ven -- det -- ta.
  Pe -- ra pe -- ra
  pe -- ra pe -- ra mo -- ra il cru -- del,
  sù sù sù sù ven -- det -- ta ven -- det -- ta. __
}
\tag #'(vsoprano valto vtenor) {
  Pe -- ra pe -- ra mo -- ra il cru -- del, sù sù
  \tag #'(vsoprano valto) { sù sù ven -- det -- ta }
  \tag #'vsoprano { ven -- det -- ta }
  ven -- det -- ta ven -- det -- ta.
}
\tag #'(vsoprano valto vtenor vbasse) {
  Pe -- ra pe -- ra mo -- ra il cru -- del, sù sù sù sù ven -- det -- ta ven -- det -- ta.
}
