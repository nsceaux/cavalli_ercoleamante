\key re \minor \midiTempo#120
\digitTime\time 3/4 s2.*9
\time 4/4 s1*3
\time 12/8 <<
  \original { s1.*3 \measure 10/4 s4*10 \bar "|." }
  \pygmalion { s1.*4 }
>>
\beginMark "Sinf[onia]" \time 12/8 s1.*4 \bar "|."
