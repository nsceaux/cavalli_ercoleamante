\clef "viole1" R2.*9 R1*3 << \original { R1.*3 R4*10 } \pygmalion R1.*4 >> |
r8 sib' la' sol' fa' mi' re' fa' sol' la' sol' fa' |
mi'4 la'8 mi' fa' re' la' la' sol' fa' fa' la' |
sib' fa' sol' do' la do' re' sib do' la re' la |
mib'4 sol'8 re'4. re'2. |
