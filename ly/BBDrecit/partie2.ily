\clef "viole2" R2.*9 R1*3 << \original { R1.*3 R4*10 } \pygmalion R1.*4 >> |
r8 sib do' re' do' do' la re' sol re'4 re'8 |
la4 la8 sib4 sib8 la4. la8 sib do' |
re' re' mi' fa' re' fa' sol' do' sol re' la re' |
la sib mib' la4. si2.\fermata |
