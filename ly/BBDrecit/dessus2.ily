\clef "dessus" R2.*9 R1*3 << \original { R1.*3 R4*10 } \pygmalion R1.*4 >> |
r8 re'' do'' sib' la' sol' fa' fa'' mi'' re'' do'' sib' |
la' dod'' re'' mi'' re''4 re'' dod''!8 re'' fa'' mi'' |
re'' do'' sib' la' sol' la' sib' do'' do'' re'' fad'' sol'' |
la'' sol''4 sol'' fad''8 sol''2. |
