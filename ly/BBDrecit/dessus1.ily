\clef "dessus" R2.*9 R1*3 << \original { R1.*3 R4*10 } \pygmalion R1.*4 >> |
r2*3/2 r8 la'' sol'' fa'' mi'' re'' |
dod'' mi'' fa'' sol'' fa'' sol'' mi''4. re'' |
r4. r8 re'' do'' sib' la' sol' fad' la' sib' |
do'' sib' do'' la'4. sol'2. |
