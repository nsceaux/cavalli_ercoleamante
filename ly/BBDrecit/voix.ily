\clef "vbasse" <>^\markup\character Ercole
r4 r sol8. sol16 |
fa4 fa8 sol mib fa |
re4 re8 re do re |
sib,8. sib,16 sib,8 do re mi |
fa2 do'8 do' |
sib4 sib8 do' la sib |
sol4 sol8 sol fa sol |
mib4 mib8 mib fa sol |
re4 r r |
r4 si2 si8 si |
si si si2 si8 si |
si4 si8 do' do'4 do' |
r8 do' sib la sol fa mi re mi fa sol la |
sol4 sol8 la fa sol la4. re |
r4. r8 sol fa mib re do si, la, si, |
do8 re mib re4 re8 mib! sib, do re4. <<
  \original { sol,1\fermata | R1.*4 | }
  \pygmalion { sol,2. r2*3/2 | R1.*3 | }
>>
