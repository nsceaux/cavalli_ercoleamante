In -- fe -- li -- ce, e di -- spe -- ra -- to
men -- tre mes -- tis -- si -- mo
vo not -- te, e dì,
qual di be -- ne i -- na -- spet -- ta -- to
rag -- gio pu -- ris -- si -- mo
m’ap -- pa -- rì?

Ah che s’ac -- ce -- so un cor
av -- vien mai che di -- spe -- ri,
non sa co -- me in a -- mor
con so -- vra -- no po -- ter for -- tu -- na im -- pe -- ri,
non sa co -- me in a -- mor
con so -- vra -- no po -- ter for -- tu -- na im -- pe -- ri.
