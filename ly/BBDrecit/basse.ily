\clef "basse" sol2 sol4 |
fa2 mib4 |
re2 do4 |
sib,2. |
fa2 do'4 |
sib2 la4 |
sol2 fa4 |
mib2 do4 |
re2. |
si1~ |
si |
si2 do' |
r8 do' sib la sol fa mi4. fa |
sol4 sol8 la fa sol la4. re8 re'[ do'] |
sib la sol fad sol \ficta fa mib re do si,4. |
do re mib8 sib, do re4. <<
  \original { sol,1\fermata | r8 sol, la, }
  \pygmalion { sol, sol,[ la,] }
>> sib,8 do do re re mi fa sol sol |
la4 la8 sol4 sol8 la4. re8 re'[ do'] |
sib la sol fa sib la sol fa mib re4 re8 |
do4 do8 re4. sol,2. |
