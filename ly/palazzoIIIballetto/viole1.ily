\clef "dessus2" do''4 |
mi''4. re''8 do''4 si' |
la'4. si'8 do''2 |
re''8 do'' do''2 si'4 |
do''2 mi''4 mi'' |
re'' re'8 mi' fa' sol' la' si' |
do''2 re'' |
mi''4. re''8 re''2 |
re''2. si'4 |
sol' do''4. si'8 la'4 |
r8 re' sol'4. la'8 si' do'' |
re'' la' do''2 si'4 |
do''2
