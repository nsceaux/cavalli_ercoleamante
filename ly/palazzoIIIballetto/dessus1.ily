\clef "dessus" mi''4 |
sol''4. fa''8 mi''4 re'' |
do''8 si' do'' re'' mi''4 sol'' |
fa'' mi'' re''8 do'' re'' mi'' |
do''2 do''4 do'' |
si'4. do''8 re''4 fa'' |
mi''8 re'' mi'' fa'' sol''4 sol'' |
mi''8 fad'' la''4 sol''4. fad''8 |
sol''2 sol''4 re'' |
mi''4. fa''8 sol''4 mi''8 re'' |
re'' do'' re'' mi'' re''4 sol'' |
fa'' mi'' re''8 do'' re'' mi'' |
do''2
