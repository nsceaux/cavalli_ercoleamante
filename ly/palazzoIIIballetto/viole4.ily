\clef "viole2" sol4 |
sol2 sol8 la si4 |
do'2 sol4. do'8 |
la si do'4. si16 la sol4 |
sol2 sol4 sol |
sol8 la si la16 sol re'4 re |
la2 sol |
sol r4 re~ |
re sol4. la8 si re' |
do' re' mi'2 la4 |
si2 sol4 re'~ |
re'8 re sol2 sol4 |
sol2
