\clef "basse" do4 |
do4. re8 mi fa sol4 |
la sol8 fa mi4 do |
fa fa, sol,2 |
do do4 do |
sol fa8 mi re2 |
do2 si,4 sol, |
do2 re |
sol, si, |
do4. re8 mi4 fa |
sol2 si,4. sol,8 |
re4 mi8 fa sol4 sol, |
do2
