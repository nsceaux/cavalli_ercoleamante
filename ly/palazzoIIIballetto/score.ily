\score {
  \new StaffGroup <<
    \new Staff << \global \includeNotes "dessus1" >>
    \new Staff << \global \includeNotes "dessus2" >>
    \new Staff << \global \includeNotes "viole1" >>
    \new Staff << \global \includeNotes "viole2" >>
    \new Staff << \global \includeNotes "viole3" >>
    \new Staff << \global \includeNotes "viole4" >>
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout { s4 s1*5\pageBreak }
    >>
  >>
  \layout { }
  \midi { }
}
