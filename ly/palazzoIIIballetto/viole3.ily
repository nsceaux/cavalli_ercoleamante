\clef "viole2" do'4 |
do'2 re'4 sol |
r la~ la8 mi mi'4 |
re'2. re'4 |
mi'2 do'4 do' |
re'2. la4~ |
la8 si do' si16 la re'2 |
do'4 sol re'4. do'8 |
si4. la8 sol4. fa8 |
mi do do'2 re'4~ |
re' si2 sol4 |
la r8 la re'4 re' |
mi'2
