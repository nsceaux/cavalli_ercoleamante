\clef "dessus" sol''4 |
mi''4. fa''8 sol''4. mi''8 |
la''2 sol''4. mi''8 |
la''4. la''8 sol''2 |
sol'' sol''4 sol'' |
sol''2 fa''4. sol''8 |
la''4 la' si'8 do'' re'' si' |
do''4. si'8 la' sol' la' si' |
sol'2 re'' |
sol''4. fa''8 mi''4 re''8 do'' |
si' la' si' do'' si'4 si'' |
la'' sol'' sol''2 |
sol''
