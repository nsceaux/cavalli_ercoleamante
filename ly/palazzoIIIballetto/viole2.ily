\clef "viole1" sol'4 |
sol'2 sol'4. sol'8 |
mi'8 re' mi' fa' sol'4. sol'8 |
la'4 la re' sol' |
sol'2 sol'4 sol' |
sol'2 la' |
la' sol' |
sol'4 mi' la' re' |
re'4. do'8 si do' re' si |
mi' fa' sol'4. mi'8 la'4 |
re'4. do'8 re'4. mi'8 |
fa'4 sol' sol'2 |
sol'
