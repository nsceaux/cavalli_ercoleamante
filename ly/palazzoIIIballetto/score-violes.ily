\score {
  \new StaffGroup <<
    \new Staff << \global \includeNotes "viole1" >>
    \new Staff << \global \includeNotes "viole2" >>
    \new Staff << \global \includeNotes "viole3" >>
    \new Staff << \global \includeNotes "viole4" >>
  >>
  \layout { }
}
