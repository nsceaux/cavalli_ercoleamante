\key do \major \midiTempo#240
\beginMark "A 6"
\customTime\markup\vcenter {
  \musicglyph#"timesig.C44"
  \override#'(baseline-skip . 0) \column { \musicglyph#"three" \musicglyph#"two" }
}
\measure 3/2 s1.
\measure 6/2 s1.*2
\measure 3/2 s1.*2
\measure 6/2 s1.*2
\measure 3/2 s1.
\measure 6/2 s1.*4
\measure 3/2 s1.*4
\measure 6/2 s1.*6 \bar "|."
