Vo -- la pre -- ci -- pi -- te
al som -- mo cul -- mi -- ne à dea fe -- del
sprez -- za col ful -- mi -- ne
l’an -- gel bri -- ci -- pi -- te
del dio cru -- del
bat -- ta -- si, vin -- ca -- si, spe -- gna -- si il ciel
\tag #'(voix2 voix4 voix6) { bat -- ta -- si, }
vin -- ca -- si, spe -- gna -- si il ciel.
