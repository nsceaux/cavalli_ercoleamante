<<
  \tag #'voix1 {
    \clef "vsoprano" R1. |
    sol''4( fa'') mi''( re'') do''2 si'2. re''4 la'2 |
    re'' do'' re'' |
    re''2. do''4 si'2 |
    mi'' re''2. re''4 re''1. |
    r2 re''4\melisma do'' si' la'8[ sol'] |
    fad'2\melismaEnd sol' fad' si'2. si'4 si'2 |
    mi'' si' do'' si'2. si'4 si'2 |
    re'' dod''2. dod''4 |
    si'1. |
    R1. |
    mi''2. mi''4 mi''2 |
    re''2. re''4 re''2 re''2. re''4 re''2 |
    do''1. mi''2. mi''4 mi''2 |
    red''2. dod''4 red''2 mi''1*3/2\fermata |
  }
  \tag #'voix2 {
    \clef "vsoprano" R1. |
    re''2 sol' la' mi''2. si'4 do''2 |
    sol' la' la' |
    si'2. do''4 re''2 |
    do'' la'2. la'4 si'1. |
    r2 si'4\melisma la' sol' fa'8[ mi'] |
    la'2\melismaEnd mi'' re'' re''2. re''4 re''2 |
    do'' re'' la' re''2. re''4 mi''2 |
    si' si' lad' |
    si'1. |
    sold'2. sold'4 sold'2 |
    la'2. la'4 la'2 |
    la'2. la'4 la'2 si'1. |
    sol'2. sol'4 sol'2 do''2. do''4 do''2 |
    si'2. si'4 si'2 si'1*3/2\fermata |
  }
  \tag #'voix3 {
    \clef "vhaute-contre" R1. |
    sol'2 sol'2. fad'4 sol'2. fad'!4 mi'2 |
    re' mi' fa' |
    sol'2. sol'4 sol'2 |
    sol' sol'2. fad'4 sol'1. |
    R1. |
    la'2 mi' r4 la' sol'2. sol'4 sol'2 |
    sol' sol' fad' sol'2. sol'4 sol'2 |
    fad' sol' fad' |
    fad'1. |
    R1. |
    mi'2. mi'4 mi'2 |
    fad'2. fad'4 fad'2 re'2. re'4 re'2 |
    mi'1. la'2. la'4 la'2 |
    r2 fad'4. mi'8 fad'2 sold'1*3/2\fermata |
  }
  \tag #'voix4 {
    \clef "vtenor" mi4\melisma fad sol la si do' |
    re'2\melismaEnd do' do' sol2. si4 mi2 |
    sol do' la |
    re'2. re'4 re'2 |
    sol re'2. re'4 re'1.R1. |
    re'4\melisma do' si la8[ sol]\melismaEnd la4 re' si2. do'4 re'2 |
    mi' re' do' re'4. do'8 si1 |
    fad'2 mi' dod' |
    red'1. |
    si2. si4 si2 |
    dod'2. dod'4 dod'2 |
    re'2. la4 re'2 si1. |
    do'2. do'4 do'2 do'2. do'4 mi'2 |
    si2. si4 si2 si1*3/2\fermata |
  }
  \tag #'voix5 {
    \clef "vtenor" r2 mi'\melisma re'4 do' |
    si2\melismaEnd mi'2. la4 si4. la8 sol2 r |
    re la4( sol) fad( re) |
    sol2. sol4 sol2 |
    mi la2. la4 sol1. |
    sol4\melisma la si do' re' do'8[ si] |
    la2\melismaEnd sol re' re'4. do'8 si2 r |
    do' sol la si2. la4 sol2 |
    si si fad |
    fad1. |
    R1. |
    la2. la4 la2 |
    re2. re4 re2 sol2. sol4 sol2 |
    sol1. la2. la4 do'2 |
    fad2. fad4 fad2 mi1*3/2\fermata |
  }
  \tag #'voix6 {
    \clef "vbasse" r2 mi4\melisma fad sol la |
    si2\melismaEnd do' la mi2. re4 do2 |
    si, la, re |
    sol,2. la,4 si,2 |
    do re2. re4 sol,1. |
    r2 sol,4\melisma la, si, do |
    re2\melismaEnd mi fad sol2. la4 si2 |
    do' si la sol2. fad4 mi2 |
    re mi fad |
    si,1. |
    mi2. mi4 mi2 |
    la,2. la,4 la,2 |
    re2. re4 re2 sol,1. |
    do2. do4 do2 la,2. la,4 la,2 |
    si,2. si,4 si,2 mi,1*3/2\fermata |
  }
>>
