Per le piag -- ge su -- per -- be
ri -- splen -- de ac -- col -- ta o -- gni
\tag #'(voix1 basse voix4) { bel -- tà }
\tag #'(voix2 voix3) { bel -- tà __ }
su i fio -- ri,
\tag #'(voix1 basse voix2) {
  ri -- de o -- gni fior su l’her -- be,
}
\tag #'(voix3 voix4 basse) {
  dan -- za ogn’ her -- ba su i pra -- ti
}
al -- lo scher -- zar __ di
\tag #'(voix1 basse voix2 voix4) { zef -- fi -- ret -- ti a -- la -- ti }
\tag #'voix3 { zef -- fi -- ret -- ti a -- la -- ti __ }
al -- lo scher -- zar __ di
\tag #'(voix1 basse voix2 voix4) { zef -- fi -- ret -- ti a -- la -- ti. }
\tag #'voix3 { zef -- fi -- ret -- ti a -- la -- ti. __ }
