\beginMark "A 4"
\key fa \major \midiTempo#140
\digitTime\time 3/4 s2.*8
\measure 6/4 s1.
\measure 3/4 s2.*26
\measure 6/4 s1.
\measure 3/4 s2.*8
\measure 6/4 s1. \bar "|."

