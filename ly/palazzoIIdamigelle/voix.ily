<<
  \tag #'(voix1 basse) {
    \clef "vsoprano" r4 do''4. do''8 |
    re''4 mi''4. mi''8 |
    fa''2 fa''4 |
    R2. |
    fa''4 re''4.\melisma mi''16[ fa''] |
    sol''4. fa''8[ mi'' re''] |
    do''4. sib'8 la'[ sib'16 do''] |
    re''2\melismaEnd do''4 |
    sib'2. la' |
    R2. |
    re''4 re'' re'' |
    mi'' fa''2 |
    mi''2. |
    re'' |
    fa''4 fa''8[ mi''] fa''[ sol''] |
    mi''4 mi''8[ re'' mi'' fa''] |
    re''4\melisma re''8[ do'' re'' mi''] |
    do''4 re''8[ do'' sib' la'] |
    sol'[ fa' sol' la' sib' do''] |
    la'2\melismaEnd fa'4 |
    <<
      \tag #'basse { s2.*8 }
      \tag #'voix1 { R2.*8 }
    >>
    sol'4 re'' re'' |
    re''4.\melisma do''8[ re'' mi''] |
    re''[ mi'' re'' do'' sib' la'] |
    sib'[ la' sol' la' sib' do''] |
    re''2.\melismaEnd |
    re''4 mib''4. mib''8 |
    re''2 re''4 |
    re''2. re'' |
    re''4 sib' re'' |
    sol'8[\melisma fa' mi' fa' sol' \sug mi'] |
    fa'[ mi' fa' sol' la' fa'] |
    fa''[ sol'' fa'' mi'' re'' do''] |
    re''[ mi'' re'' do'' sib' la'] |
    sol'2.\melismaEnd |
    la'4 la'4. la'8 |
    re''2 re''4 |
    do''2. do''\breve*3/8\fermata |
  }
  \tag #'voix2 {
    \clef "vsoprano" r4 la'4. la'8 |
    si'4 do''4. do''8 |
    do''2 do''4 |
    R2.*2 |
    sol'4 mi'4.\melisma fa'16[ sol'] |
    la'4. sol'8[ fa' mi'] |
    re'2\melismaEnd fa'4 |
    re'2. fa' |
    do''4 do'' do'' |
    sib'8[\melisma la' sib' do'' re'' sib'!] |
    dod''4\melismaEnd re''2 |
    re'' \once\slurDashed dod''4( |
    re''2.) |
    re''4 re''8[ do''] re''[ mi''] |
    do''4 do''8[ sib' do'' re''] |
    sib'4\melisma sib'8[ la' sib' do''] |
    la'4 sib'8[ la' sol' fa'] |
    mi'[ re' mi' fa' sol' la'] |
    fa'4. sol'8[ la' sib'] |
    do''2.\melismaEnd |
    do'' |
    R2.*6 |
    sol'4 la' la' |
    sib'4.\melisma la'8[ sib' do''] |
    sib'[ do'' sib' do'' re'' mi''] |
    re''[ do'' sib' do'' sib' la'] |
    sol'2.\melismaEnd |
    sib'4 do''4. do''8 |
    do''2 sib'4 |
    la'2. sib' |
    R2.*2 |
    fa''4 do'' fa'' |
    re''8[\melisma mi'' re'' do'' sib' la'] |
    sib'[ do'' sib' la' sol' fa'] |
    do''2.\melismaEnd |
    do''4 do''4. do''8 |
    sib'2 la'4 |
    sol'2. la'\breve*3/8\fermata |
  }
  \tag #'(voix3 basse) {
    <<
      \tag #'basse s2.*21
      \tag #'voix3 {
        \clef "vsoprano" r4 fa'4. fa'8 |
        fa'4 sol'4. sol'8 |
        la'2 la'4 |
        do''4 la'4.\melisma sib'16[ do''] |
        re''4. do''8[ sib' la'] |
        sol'4. la'16[ sib'] do''4~ |
        do''8[ sib'] la'4. sol'8 |
        fa'2\melismaEnd fa'4 |
        sol'2. do'' |
        la'4 la' la' |
        sol'2.~ |
        sol'4 la'2 |
        la'2. |
        la' |
        R2.*6 |
      }
    >>
    la'4 la'8[ sol' la' fa'] |
    sol'4 sol'8[ fa'] sol'[ mi'] |
    fa'[\melisma mi' fa' sol' la' sib'] |
    do''[ re'' do'' sib' la' sol'] |
    la'[ sib' la' sol' fa' mi'] |
    re'4 re''8[ do'' sib' la'] |
    sol'[ fa'] sol'2\melismaEnd |
    fa'2. |
    \tag #'voix3 {
      sol'4 sol' fad' |
      sol'8[\melisma la' sib' do'' sib' la'] |
      sol'4. la'8[ sol' fa'] |
      sol'4. fa'8[ sol' la'] |
      sib'2.\melismaEnd |
      sol'4 sol'4. sol'8 |
      fad'4\melisma re'\melismaEnd sol' |
      sol'2 fad'4\melisma sol'2.\melismaEnd |
      R2. |
      do''4 sol' do'' |
      la'8[\melisma sol' la' sib' do'' la'] |
      sib'4 sib8[ do' re' mi'] |
      fa'[ mi' fa' re' mi' fa'] |
      mi'2.\melismaEnd |
      do'4 do'4. do'8 |
      re'2 fa'4 |
      fa'2 mi'4\melisma fa'\breve*3/8\fermata\melismaEnd |
    }
  }
  \tag #'voix4 {
    \clef "vsoprano" r4 fa'4. fa'8 |
    re'4 do'4. do'8 |
    fa'2 fa'4 |
    r4 fa' re'4~ |
    re'8[\melisma mi'16 fa'] sol'4. fa'8 |
    mi'[ re'] do'4. re'16[ mi'] |
    fa'4. sol'8 la'4 |
    sib'2\melismaEnd la'4 |
    sol'2. fa' |
    fa'4 fad' fad' |
    sol'2 fa'4 |
    mi'\melisma re'8[ mi' fa' sol'] |
    la'2.\melismaEnd re' |
    R2.*6 |
    fa'4 fa'8[ mi' fa' re'] |
    mi'4 mi'8[ re'] mi'[ do'] |
    re'[\melisma do' re' mi' fa' re'] |
    mi'[ re' mi' do' re' mi'] |
    fa'[ sol' fa' mi' re' do'] |
    sib4. do'8[ re' mi'] |
    fa'2 mi'4\melismaEnd |
    fa'2. |
    mib'4 re' re' |
    sol'8[\melisma fa' sol' la' sol' fa'] |
    sol'2.~ |
    sol'~ |
    sol'\melismaEnd |
    sol'4 do'4. do'8 |
    re'2 re'4 |
    re'2. sol' |
    sol' |
    mi' |
    fa' |
    sib~ |
    sib( |
    do'2.) |
    fa'4 fa'4. fa'8 |
    sib2 sib4 |
    do'2. fa'\breve*3/8\fermata |
  }
>>