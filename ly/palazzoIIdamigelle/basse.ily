\clef "basse" r4 fa2 |
re4 do2 |
fa,2. |
fa2 re4~ |
re sol2 |
do2. |
fa |
sib, |
sol, fa, |
fa4 re2 |
sol2 fa4 |
mi re2 |
la,2. |
re |
sib, |
do |
re |
fa4 sib,2 |
do2. |
fa |
fa |
do |
re |
do |
fa |
sib, |
do |
fa, |
mib4 re2 |
sol2. |
sol |
sol |
sol |
sol4 do2 |
re2. |
re sol, |
sol |
mi |
fa |
sib,~ |
sib, |
do |
fa |
sib, |
do fa,\breve*3/8\fermata |
