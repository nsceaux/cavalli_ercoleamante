\clef "basse" <<
  \original { sib,1 | sib, | la, | }
  \pygmalion { sib,2 sib | la sol | fa la, | }
>>
sol,2 mib, |
re,1 |
re |
dod |
dod2 re~ |
re4 sib, << \original sib,! \pygmalion la, >> la, |
fa,1 |
sol,~ |
sol,4 la, re,2 |
\original {
  la,1 sold, |
  sold, |
  la, |
  la, |
  fa, |
  re,2 la, |
  mi,4 re, mi,2 la,1\fermata |
}
<<
  \original { la1 | fa2 mi | }
  \pygmalion { la,1 | fa,2 mi, | }
>>
mi1 |
mi |
la,~ |
la,4 fa, mi,2 |
mi do~ |
do si,~ |
si, la, |
sold,4 la, si,2 mi,1 |
do~ |
do |
do2 fa, |
<<
  \original { fa,1~ | fa,2 }
  \pygmalion { fa,1 | re,2 }
>> do,4 do,8 re, |
mi,4 fa, sol, fa, sol,1 do |
la, |
sol, |
fa, |
mi,~ |
mi,2 re, |
re do~ |
do si, |
la,1~ |
la,4 la sol mi |
do2~ do4 re8 mi |
fa2 re~ re mi la,1\fermata |
la,1 |
la, |
sol,2 fa, |
mi,1 |
mi |
mi2 la,~ |
la, si, |
do4 la,8 sold, si,2 mi1 |
do1 |
do2 sol, |
sol,1 |
mi,2. fa,4 |
sol,2 do |
fa,1~ |
fa, |
fa,~ |
fa, |
fa,2 do |
sol, sol,~ |
sol,1 |
re, |
sol, |
do |
re sol, |
la2 la4 |
sold2. |
la4. sol8 fa4 |
mi2 mi4 |
do4 si,2 |
la, sol,4 |
fa,2 mi,4 re,2. |
do, |
sol,4. sol8 re mi |
fa2. |
mi4. re8 do si, |
la,4. sol,8 fa, mi, |
re,4. mi,8 fa,4 |
re, mi,2 la,1*3/4\fermata |
