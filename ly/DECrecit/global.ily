%% Iole
\key fa \major \time 4/4 \midiTempo#80 s1*5 \bar "|."
%% Giunone
\key do \major s1*7 \original { s1*7 \measure 2/1 s1*2 }
\bar "|."
%% Iole
\measure 4/4 s1*9
\measure 2/1 s1*2
\measure 4/4 s1*5
\measure 3/1 s1*3
\measure 4/4 s1*10
\measure 3/1 s1*3 \bar "|."
%% Giunone
\measure 4/4 s1*7
\measure 2/1 s1*2
\measure 4/4 s1*15
\measure 2/1 s1*2
\digitTime\time 3/4 \midiTempo#160 s2.*6
\measure 6/4 s1.
\measure 3/4 s2.*6
\measure 6/4 s1. \bar "|."
