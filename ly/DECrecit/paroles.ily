E qua -- le in -- as -- pet -- ta -- to
son -- no pro -- di -- gi -- o -- so
pre -- ve -- nen -- do I -- me -- ne -- o le -- ga il mio spo -- so?

I -- o -- le, I -- o -- le, ah sor -- gi
sor -- gi ra -- pi -- da, fug -- gi, e t’al -- lon -- ta -- na
dall’ in -- can -- ta -- to seg -- gio, e a me t’ap -- pres -- sa
che di ben tos -- to ri -- sa -- nar -- ti è d’uo -- po
dal ma -- gi -- co ve -- le -- no,
ond’ hai l’a -- ni -- ma op -- pres -- sa:
\original {
  pren -- di, pren -- di, fiu -- ta quest’ er -- be,
  che ne gli or -- ti fil -- li -- ri -- di rac -- col -- si,
  il cui me -- di -- co o -- do -- re,
  che le ma -- lie di -- le -- gua,
  ti sa -- ne -- rà ad un trat -- to
  dà -- le tar -- ta -- re e in -- fet -- ti -- o -- ni il co -- re. __
}

O Di -- va, o Dè -- a, dà qua -- li
or -- ri -- di pre -- ci -- pi -- zi
d’in -- fe -- del -- tà, d’in -- i -- qui -- tà ri -- sor -- go?
Ohi -- mè! di qua -- li er -- ro -- ri
rea, quan -- tun -- que in -- no -- cen -- te o -- ra mi scor -- go! __
Pu -- re il mio pri -- mo, e sol gra -- di -- to fo -- co,
ch’in me pa -- re -- va es -- tin -- to
men -- tre il cor mi ral -- lu -- ma,
con u -- su -- ra di fiam -- me
più che mai mi con -- su -- ma.
Ma che pro? s’Hyl -- lo in -- tan -- to
l’u -- ni -- co mio te -- so -- ro
sen -- za mia col -- pa
sen -- za mia col -- pa a ra -- gion me -- co i -- ra -- to,
a ra -- gion da me fug -- ge, e a tor -- to io mo -- ro. __

Ah per -- ché per -- di Io -- le
in su -- per -- flue que -- re -- le
tem -- po sì pre -- ci -- o -- so, Hyl -- lo non lun -- ge
per mio con -- si -- glio in un cis -- pu -- glio as -- co -- so
tut -- to gua -- ta, et as -- col -- ta. __ Ar -- ma più tos -- to
ar -- ma ar -- ma fi -- glia la ma -- no
di ques -- to a -- cu -- to ac -- cia -- ro,
ch’a -- bi -- le a pe -- ne -- tra -- re o -- gni ri -- pa -- ro
per me tem -- prò Vul -- ca -- no
e men -- tre im -- pri -- gio -- na -- to
da i le -- ga -- mi del Son -- no i più te -- na -- ci
sta quel mos -- tro sì cru -- do
d’o -- gni di -- fe -- sa i -- gnu -- do,
van -- ne, van -- ne, e ven -- di -- ca ar -- di -- ta
con la mor -- te di lui
le mie of -- fe -- se, e i tuoi dan -- ni,
ch’al -- tro scam -- po non ha d’Hyl -- lo la vi -- ta.
Van -- ne, e poi -- ché spe -- di -- ta al ciel’ io tor -- no
ad ov -- vi -- ar in ciò l’i -- re di Gio -- ve __
fa’ ch’io vi giun -- ga
fa’ ch’io vi giun -- ga il crin de lau -- ri a -- dor -- no. __
