\tag #'(licco basse) {
  Buon dì buon dì gen -- til fan -- ciul -- lo.
}
\tag #'(paggio basse) {
  E buo -- na not -- te.
}
\tag #'(licco basse) {
  Ma do -- ve in tan -- ta fret -- ta?
}
\tag #'(paggio basse) {
  A far da gran mes -- sag -- gio.
}
\tag #'(licco basse) {
  As -- col -- ta as -- col -- ta un po -- co, as -- pet -- ta;
  che so qual pos -- sa ha -- ver fac -- cen -- de un Pag -- gio.
}
\tag #'(paggio basse) {
  E che tu sai? ch’I -- o -- le
  ad Er -- co -- le…
}
\tag #'(licco basse) {
  T’in -- vi -- a.
}
\tag #'(paggio basse) {
  Sì af -- fé sì af -- fé m’in -- vi -- a…
}
\tag #'(licco basse) {
  A dir -- gli.
}
\tag #'(paggio basse) {
  È ve -- ro è ve -- ro a dir -- gli…
  Ch’al giar -- di -- no de’ fio -- ri
}
\tag #'(licco basse) {
  El -- la
}
\tag #'(paggio basse) {
  El -- la si ren -- de -- rà com’ ei com’ ei de -- si -- a. __
}
\tag #'licco {
  El -- la si ren -- de -- rà com’ ei de -- si -- a. __
}
