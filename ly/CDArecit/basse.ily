\clef "basse" sib,1 |
sib,2 do |
fa,1 |
fa,2 do~ |
do4 sol, do2 |
do1 |
fa2 re |
mib2. fa4 |
sib,1 |
sib, |
fa, |
fa2 re~ |
re mib~ |
mib fa~ |
fa sol |
mib fa8 mib fa4 |
sib,1 |
