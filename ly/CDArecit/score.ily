\score {
  \new ChoirStaff <<
    \new Staff \with {
      instrumentName = \markup\character Paggio
      shortInstrumentName = \markup\character Pag.
    } \withLyrics <<
      \global \keepWithTag #'paggio \includeNotes "voix"
    >> \keepWithTag #'paggio \includeLyrics "paroles"
    \new Staff \with {
      instrumentName = \markup\character Licco
      shortInstrumentName = \markup\character Lic.
    } \withLyrics <<
      \global \keepWithTag #'licco \includeNotes "voix"
    >> \keepWithTag #'licco \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*3\pageBreak
        s1*3 s2 \bar "" \break s2 s1*3\break s1*4\pageBreak
        s1*3\break
      }
    >>
  >>
  \layout {
    indent = \largeindent
    short-indent = 10\mm
  }
  \midi { }
}
