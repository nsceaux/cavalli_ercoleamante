<<
  \tag #'(paggio basse) {
    <<
      \tag #'basse { s1 s2 \ffclef "vsoprano" <>^\markup\character Paggio }
      \tag #'paggio { \clef "vsoprano" R1 r2 }
    >> r8 la' sol' la' |
    fa'4 fa' <<
      \tag #'basse { s2 s2.. \ffclef "vsoprano" <>^\markup\character Paggio }
      \tag #'paggio { r2 | r r4 r8 }
    >> sol'8 |
    do''8 si' si'8. do''16 do''4 <<
      \tag #'basse {
        do''8 s |
        s1*3 \ffclef "vsoprano" <>^\markup\character Paggio
      }
      \tag #'paggio { do''4 | R1*3 | }
    >>
    r8 re'' do'' re'' sib'4. la'8 |
    sib'4 sib'8 fa' sol'8. sol'16 <<
      \tag #'basse {
        sol'8 s s4
        \ffclef "vsoprano" <>^\markup\character Paggio
      }
      \tag #'paggio { sol'4 | r4 }
    >> r8 do''16 do'' la'8 do''16 do'' la'8 sol' |
    la' la' <<
      \tag #'basse { s2 \ffclef "vsoprano" <>^\markup\character Paggio }
      \tag #'paggio {
        r4 r 
      }
    >> r8 re'' |
    sib' sib' re''16 sib' sib' sib' sol'8 sol' r4 |
    sol'8 sol' sol' sol'16 la' la'8 la' <<
      \tag #'basse { s4 \ffclef "vsoprano" <>^\markup\character Paggio }
      \tag #'paggio { r4 }
    >>
    la'8 la'16 la' la'8. sib'16 sib'4 r8 sib' |
    sol'4 r8 sol' la' sib' sib'8. la'16( |
    sib'1) |
  }
  \tag #'(licco basse) {
    \clef "valto" \tag #'basse <>^\markup\character Licco
    r8 fa' re' fa' re' re' re' do' |
    re'4 re' <<
      \tag #'basse { s1 \ffclef "valto" <>^\markup\character Licco }
      \tag #'licco { r2 | r }
    >> r4 do' |
    fa'8 fa' fa' sol' mib'4 <<
      \tag #'basse {
        mib'8 s | s2..
        \ffclef "valto" <>^\markup\character Licco
      }
      \tag #'licco { mib'4 | r2 r4 r8 }
    >> sol'8 |
    mi' mi'16 sol' mi'8 re' mi'4 mi'8 la' |
    fa'4 fa'8 do' fa' fa' fa' fa'16 sol' |
    mib'8 re' do' re' sib sib r4 |
    <<
      \tag #'basse {
        s1 s2.. 
        \ffclef "valto" <>^\markup\character Licco
      }
      \tag #'licco { R1 | r2 r4 r8 }
    >> mi'8 |
    fa' fa' <<
      \tag #'basse { s1 \ffclef "valto" <>^\markup\character Licco }
      \tag #'licco { r4 r2 | r4 }
    >> r8 do' re' re' <<
      \tag #'basse {
        s4 s1 s2.
        \ffclef "valto" <>^\markup\character Licco
      }
      \tag #'licco { r4 | R1 | r2 r4 }
    >> do'8 do' |
    <<
      \tag #'basse { s1*3 \ffclef "valto" <>^\markup\character Licco }
      \tag #'licco {
        r2 re'8 re'16 re' re'8. mib'16 |
        mib'4 r8 mib' do' re' do'8. do'16( |
        sib1) |
      }
    >>
  }
>>
