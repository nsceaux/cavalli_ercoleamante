\clef "viole1"
\transpose do #(if (eqv? #t (ly:get-option 'pygmalion))
                   (ly:make-pitch -1 1)
                   (ly:make-pitch -1 0))
{
r4 r sib' |
sol'2 la'4 fa'2 fa'4 |
sol' mib' sol' fa' sol' re' |
r fa' do' |
re'2. mib'4 do' sib |
fa'2 fa'4 re' sib2~ |
sib4 do' re' re' do' re' |
re'2. do'2 fa'4 |
fa'2 sib4 r sib4. la8 |
la4 do' fa' |
fa'4. mib'8 re'4 mib'2 mib'4 |
re' fa' mib' mib'2. |
re'4 do' sib fa' do' fa' |
sib mib' sib fa'2.~ |
fa'2 sib'4 sol'2 la'4 fa'2 fa'4 |
sol' mib' sol' fa' sol' re' r fa' do' |
re'2 re'4 sib do' re' |
sol'4 do' fa'2 re' |
r4 la' la' fa' re' fa' |
fa'2 mi'4 fa'2. |
sol'2 fa'4 sib'2 la'4 |
sol'2 la'4 la' sol'2 |
la'2 fa'4 sol' mib'2 |
do'4 fa' mib' re'2. |
do'2 sol4 la sib do' |
re' mib' fa' mib'2. |
do'4 la la re' sib2~ |
sib4 do' sib fa' do'4. re'8 |
la4 fa'2 mib'4 mib' re' |
sol'2. |
do'4 fa' fa' fa'2 mib'4 |
fa' sol' do'2 re'4 mib' |
fa' re'2 fa' fa'4 |
sol' fa' mi' fa' re' do' |
fa' sol'2 fa'2. |
sib2 re'4 sol sol sol |
do' fa sol la sib la |
sol2 sib la4 sol |
fa la2 sib4. do'8 re'4 mi' fa' mi' |
re'2. sol4 mib'2 |
mib'4 re'2 re'4 do' si |
sol'2. fa'4. fa'8 mib'2 re'4. re'8 |
mi'!4 do' mi' la re' do' |
sib fa'4. fa'8 sol'4 mib'2~ |
mib'4 re' mib' fa'2 re'4 |
do'4. do'8 sib4 la fa'4. do'8 |
mib'2 sib4~ |
sib fa' do' re'2 sib'4 |
sol'2 la'4 fa'2 fa'4 |
sol' mib' sol' fa' sol' re' |
r4 fa' do' \original { re'1*3/4\fermata | }
}
\pygmalion {
  mi'4 mi'2 |
  fa'4 la' fa' sol' si' sol' |
  la'2 la4 do'8 re' la4 do' |
  re'2 si4 do' mi' sol' |
  fa'2 la'4 si' sol' si' |
  do'' la' do''~ do'' do' do' |
  re'2 sol'8 fa' mi' re' do'4 mi' |
  fa'2 la'4 re' re' sol' |
  la'2 la4 r do'2~ |
  do' si4 do' sol'2 |
  fa'2 fa'4 sol'2 do''4 |
  la'2 la'4 do'' la' sol' |
  sol'2.~ sol'2 do''4 |
  do'' la'2 re'4 mi' sol' |
  do'2 la4 do'2. |
  re'2. do'2 sol'4 |
  fa'2 la'4 sol'2 si'4 |
  do'' la'2 sol'4 do''2 |
  do''4 si'4. do''8 do''2 sol'4 |
  fa'2 fa'4 sol'2 sol'4 |
  la'2 la'4 si' la' sol' |
  sol'2. sol'2 sol'4 |
  fa'4. sol'8 la'4 si' sol'2 |
  la'2 mi'4~ mi' do'2 |
  re' sol4 sol'2 mi'4 |
  do'2 re'4 re'2 sol'4 |
  fa'4 mi'2 mi'4 la' sol' |
  sol'2. sol'2 la'4 |
  la' la si si mi'2 |
  sol'4 la' la'~ la' la'2 |
  re'4 sol'2 sol'2. |
  la' si'4 sol'2 |
  do''2. sol'4 la' fa' |
  sol'2. sol'2 do''4 |
  la'2 si'4 sol'2 sol'4 |
  la' fa' la' mi' la' mi' |
  r sol'2 sol'2 sol'4 |
  fa' la' re'~ re' mi'4. do'8 |
  fa'8 sol' la' si' do''4~ do'' do' sol' |
  sol'2 re'4 mi'2. |
}

