\clef "dessus"
\transpose do #(if (eqv? #t (ly:get-option 'pygmalion))
                   (ly:make-pitch -1 1)
                   (ly:make-pitch -1 0))
{
r2*3/2 |
r4 sol'' mib'' fa''8 mib'' re'' do'' re''4 |
mib'' sib' do'' re'' sib'2~ |
sib'4 la'2 |
sib'2. r2*3/2 |
R1.*4 |
r4 do'' la' |
re''2. r2*3/2 |
r r4 mib''8 re'' do''4 |
fa''2. r2*3/2 |
R1. |
r2*3/2 r4 sol'' mib'' fa''8 mib'' re'' do'' re''4 |
mib'' sib' do'' re'' sib'2~ sib'4 la'2 |
sib'2. r2*3/2 |
r r4 fa'' re'' |
R1. |
r2*3/2 r4 do'' fa'' |
mi'' do''2 r2*3/2 |
R2.*21 |
r4 re'' sib' do'' sib' la' |
do''2 sol'4 r2*3/2 |
r r4 re'' do'' |
sib'2. r2*3/2 |
R2.*25 |
r4 sol'' mib'' fa''8 mib'' re'' do'' re''4 |
mib'' sib' do'' re'' sib'2~ |
sib'2 la'4 
} do''4 sol'' do''' |
la''8 sol'' fa''4 la'' sol''8 fa'' mi''4 sol'' |
fa''4 re'' mi''4. re''8 do''2 |
si'4 re'' sol'' mi''8 re'' do''2 |
r4 do'' fa'' re''8 do'' si'4 mi'' |
do'' mi'' la'' sol''8 fa'' mi'' re'' mi''4 |
re''4 si'2 do''4 sol''8 fa'' mi'' re'' |
do''2 re''8 do'' si' la' sol'4 sol''8 fa'' |
mi'' re'' do''4 si'8 la' sol' fa' mi' re' mi'4 |
re'2 re''4 do''8 re'' mi'' fa'' sol''4 |
la'' fa'' la'' sol'' mi''2 |
fa''4 do'' re'' mi'' re'' do'' |
do''2 si'4 do''8 si' do'' re'' mi''4 |
fa'' do'' re'' si' sol' sol'' |
la''8 sol'' fa''4 la'' sol''8 la'' fa'' sol'' mi'' fa'' |
re''2. mi''4 do''2 |
la'4 la'' fa'' sol''2 mi''4 |
fa'' do''4. re''8 mi''4. fa''8 sol''4 |
sol''4. sol''8 fa''4 mi'' do''2 |
la' re''4 si'2 mi''4 |
do'' re'' mi''~ mi'' re'' do''~ |
do'' si' re'' mi''8 sol'' sol'' fa'' mi'' re'' |
do'' la'' la'' sol'' fa'' mi'' re'' sol'' sol'' fa'' mi'' re'' |
do'' fa'' fa'' mi'' re'' do'' si' si' la' sol' fa' mi' |
re' mi' fa' sol' la' si' do'' mi'' mi'' re'' do'' si' |
la' sol' la' si' do'' re'' si' la' si' do'' re'' mi'' |
do'' re'' mi'' re'' mi'' fa'' sol''4 fa'' mi'' |
re''2 re''4 mi''2 mi''4 |
fa''2 re''4 sol''2 sol'4 |
do''2 do''4 la' re'' do'' |
si'4 do'' re'' mi''4. fa''8 sol''4 |
fa''4. sol''8 la''4 sol'' do''' si'' |
do'''4. si''8 la''4 sol'' fa''4. mi''8 |
re''4 sol''2~ sol''4 sol''8 fa'' mi'' sol'' |
fa''4 do'' re'' mi''8 re'' mi'' fa'' sol''4 |
r la''8 sol'' fa'' la'' sol'' fa'' mi'' re'' mi''4 |
re'' sol''8 fa'' mi'' re'' mi''2 mi''4 |
fa''2 fa''4 re'' sol'' la''8 sol'' |
la'' sol'' fa'' sol'' la'' fa'' sol'' fa'' mi'' re'' mi''4 |
mi'' re''2 do''2. |
