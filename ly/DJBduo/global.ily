\original \key fa \major
\pygmalion \key do \major
\midiTempo#200
\digitTime\time 3/4 s2.
\measure 6/4 s1.*2
\measure 3/4 s2.
\measure 6/4 s1.*5
\measure 3/4 s2.
\measure 6/4 s1.*4
\measure 9/4 s2.*6
\measure 6/4 s1.*13
\measure 3/4 s2.
\measure 6/4 s1.*8
\measure 9/4 s2.*3
\measure 6/4 s1.*2
\measure 9/4 s2.*3
\measure 6/4 s1.*4
\measure 3/4 s2.
<<
  \original { \measure 6/4 s1.*4 }
  \pygmalion { \measure 6/4 s1.*43 }
>> \bar "|."
