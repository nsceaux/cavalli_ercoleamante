\transpose do #(if (eqv? #t (ly:get-option 'pygmalion))
                   (ly:make-pitch -1 1)
                   (ly:make-pitch -1 0))
<<
  %% Paggio
  \tag #'(paggio basse) {
    <<
      \tag #'basse {
        s2.*9 s2
        \ffclef "vbas-dessus" <>^\markup\character Paggio
      }
      \tag #'paggio {
        \ffclef "vbas-dessus" <>^\markup\character Paggio
        R2.*8 |
        r2*3/2 r4 r
      }
    >> re''4 |
    sol' la'( sib') sib' la'( sib') |
    sib'2 sib'4 la'2 la'4 |
    sib' sol'( la') la'( sol'2) |
    fa'2. r4 r sol' do''2 la'4 |
    re''2 sol'4 do''2. |
    <<
      \tag #'basse {
        s2.*2 \ffclef "vbas-dessus" <>^\markup\character Paggio
      }
      \tag #'paggio { R2.*2 | }
    >>
    re''4 do'' sib' sib'2 la'4( |
    sib'2.) r1*3/2 |
    R2.*3 |
    <<
      \tag #'basse {
        s2.*4 s4 \ffclef "vbas-dessus" <>^\markup\character Paggio
      }
      \tag #'paggio { R2.*4 r4 }
    >> do''4 do'' re''2 do''4 |
    sib' sol'2 la'2. |
    <<
      \tag #'basse {
        sol'4 s2 s2.*3 s4
        \ffclef "vbas-dessus" <>^\markup\character Paggio
      }
      \tag #'paggio { sol'2 r4 r2*3/2 | R2.*2 | r4 }
    >> la'4 sib' sol'2 sol'4 |
    la' si' do'' do''2 si'!4( |
    do''2.) la'4 sol' la' |
    sib'2 sib'4 sol' fa' sol' |
    la'2 la'4 r sib' la' |
    sol'2 sol'4 r la' sol' |
    <<
      \tag #'basse {
        fa'4 s2 s2.*2 s4 \ffclef "vbas-dessus" <>^\markup\character Paggio
      }
      \tag #'paggio {
        fa'2 fa'4 r2*3/2 |
        R2. |
        r4
      }
    >> la'4 la' re'' re'' do'' |
    sib' do''2 do'' do''4( |
    sib') sib'2 la'2. |
    sol' do''4 sib' la' |
    sol'2 sol'4( la'2.) |
    r4 sib' la' sol'2 sol'4 |
    la' la' sol' fa'2 fa'4 |
    r sol' fa' mi'2 r4 |
    fa'4 fa' mi' <<
      \tag #'basse {
        re'4 s2 s2.*3 s4 \ffclef "vbas-dessus" <>^\markup\character Paggio
      }
      \tag #'paggio {
        re'2 re'4 r2*3/2 |
        R2.*2 |
        r4
      }
    >> la' re'' si' la' si' |
    do'' si' do'' re''4. re''8 do''2 do''4. si'!8( |
    do''4) do'' sib' la'2 r4 |
    sib'4 sib' la' <<
      \tag #'basse {
        sol'4 s2 |
        s2. s4 \ffclef "vbas-dessus" <>^\markup\character Paggio
      }
      \tag #'paggio {
        sol'2 sol'4 |
        r2*3/2 r4
      }
    >> fa'4 sib' |
    sol' fa' sol' la' la' la' |
    sib'4. do''8 sib'4~ |
    sib' sib'4. la'8( sib'2.)\fermata |
    R2.*6 |
  }
  %% Licco
  \tag #'(licco basse) {
    \ffclef "valto" <>^\markup\character Licco
    R2.*6 |
    r4 r fa' sib do'( re') |
    re' do'( re') <<
      \tag #'basse {
        sib2 s4 | s2.*11
        \ffclef "valto" <>^\markup\character Licco
      }
      \tag #'licco {
        sib2. |
        r2*3/2 r4 r fa' |
        re'2. r4 r fa' |
        re' mi'( fa') fa'2 mi'4( |
        fa'2) do'4 |
        fa'2 sib4 mib'2 do'4 |
        fa'2 r4 r2*3/2 |
      }
    >>
    fa'4 mib' re' do'2 do'4 |
    <<
      \tag #'basse { s2.*8 \ffclef "valto" <>^\markup\character Licco }
      \tag #'licco {
        fa'4 mib' re' do'2. |
        sib r1*3/2 |
        R2.*3 |
      }
    >>
    r4 fa' fa' sol'2 fa'4 |
    mib' do'2 re'2. |
    <<
      \tag #'basse {
        do'4 s2 s2.*3 s4
        \ffclef "valto" <>^\markup\character Licco
      }
      \tag #'licco {
        do'2. r2*3/2 |
        R2.*2 |
        r4
      }
    >> mi'4 fa' re'2 re'4 |
    re' mi' fa' fa'2 mi'4( |
    <<
      \tag #'basse {
        fa'4) s2 s2.*11 s4
        \ffclef "valto" <>^\markup\character Licco
      }
      \tag #'licco {
        fa'2.) r2*3/2 |
        R2.*2 |
        mi'4 re' mi' fa'2 fa'4 |
        re' do' re' mib'2 mib'4 |
        r fa' mib' re'2 re'4 |
        r mib' re' do'2 do'4 |
        r4
      }
    >> re'4 re' sol' sol' fa' |
    mib' re'2 |
    <<
      \tag #'basse {
        do'4 s2 s2.*16 s4
        \ffclef "valto" <>^\markup\character Licco
      }
      \tag #'licco {
        do'2 do'4( sib2) r4 |
        r2*3/2 r4 fa'2 |
        re'2. do' |
        r2*3/2 do'4 re' fa' |
        fa'2 mi'4( fa') fa' mib' |
        re'2 re'4 mib' mib' re' |
        do'2 do'4 r re' do' |
        sib2 r4 do' do' sib |
        la2 la4 r
      }
    >> re'4 sol' mi' re' mi' |
    fad' mi' fad' sol'4. la'8 sol'4~ |
    sol' <<
      \tag #'basse {
        s2 s2.*7 s4
        \ffclef "valto" <>^\markup\character Licco
      }
      \tag #'licco {
        sol'4. fad'8( sol'2.) |
        r1*3/2 r4 sol' fa' |
        mi'2 r4 fa' fa' mib' |
        re'2 re'4 r
      }
    >> sib4 mib' |
    do' sib do' re'
    \tag #'licco {
      re'4 fa' |
      mib'4. mib'8 re'4 do' do' fa' |
      mib'4. mib'8 re'4~ |
      re' do'4. do'8( sib2.)\fermata |
      R2.*6 |
    }
  }
>>