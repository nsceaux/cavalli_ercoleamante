\clef "dessus"
\transpose do #(if (eqv? #t (ly:get-option 'pygmalion))
                   (ly:make-pitch -1 1)
                   (ly:make-pitch -1 0))
{
r4 r re'' |
sib'2 do''4 la'2 sib'4 |
r sol'' mib'' fa''8 mib'' re'' do'' re''4 |
do''2. |
sib' r2*3/2 |
R1.*3 |
r2*3/2 r4 r do'' |
la'2 do''4 |
sib'2. r2*3/2 |
r mib''8 re'' do''4 fa'' |
sib'2. r2*3/2 |
R1. |
r4 r re'' sib'2 do''4 la'2 sib'4 |
r sol'' mib'' fa''8 mib'' re'' do'' re''4 do''2. |
sib'2. r2*3/2 |
r re''4 sib' r |
R1. |
r4 r do'' la'2 la'4 |
do'' sol' la' r2*3/2 |
R2.*21 |
r4 fa' sol' la' sol' fa' |
mi' sol'2 r2*3/2 |
r4 do'' sib' la'2. |
R2.*25 |
r2*3/2 r4 r re'' |
sib'2 do''4 la'2 sib'4 |
r sol'' mib'' fa''8 mib'' re'' do'' re''4 |
do''2 do''4
} do''2 mi''4 |
do''2 fa''8 mi'' re''4 sol'' do''' |
la''8 sol'' fa''4 la'' sol''8 fa'' mi'' re'' mi''4 |
re'' si' re'' do'' sol' do'' |
la'8 sol' fa'2 r4 re'' sol'' |
mi''8 re'' do''4 fa'' mi''8 re'' do''2 |
si'4 sol''8 fa'' mi'' re'' mi''4. re''8 do'' si' |
la'4 la''8 sol'' fa'' mi'' re'' do'' si' la' si'4 |
do'' fa''8 mi'' re'' do'' si'4 la' sol'~ |
sol' sol''2~ sol'' mi''4 |
fa'' do'' re'' si' sol' sol'' |
la'' fa'' la'' sol'' fa'' mi'' |
mi'' re''2 mi''8 re'' mi'' fa'' sol''4 |
la''8 sol'' fa''4 la'' sol''8 fa'' mi'' re'' do''4 |
mi''4 do'' re'' mi''8 fa'' re'' mi'' do'' re'' |
si'2 do''4 do''2 mi''4 |
fa'' do''4. re''8 mi''4. fa''8 sol''4 |
la''4. sol''8 fa''4 sol'' fa''4. mi''8 |
mi''4 re''2 do'' mi''4~ |
mi'' re'' fa''~ fa'' mi'' sol''~ |
sol'' fa'' la'' sol'' fa'' mi'' |
mi'' re''4. sol''8 sol'' fa'' mi'' re'' do'' la'' |
la'' sol'' fa'' mi'' re'' sol'' sol'' fa'' mi'' re'' do'' si' |
la' re'' re'' do'' si' la' sol' sol'' fa'' mi'' re'' do'' |
si' sol'' sol'' fa'' mi'' re'' mi'' do'' do'' si' la' sol' |
fa' mi' fa' sol' la' fad' sol' fad' sol' la' si' sol' |
la' si' do'' si' do'' re'' mi''4 re'' do''~ |
do'' si'2 do'' do''4 |
do''2 sol'4 si' sol' sol'' |
mi''2 mi''4 fa''2 mi''4 |
re''4 mi'' sol''4. fa''8 mi'' re'' do''4 |
do'' re''2 re''4 mi''2 |
mi''2. do''~ |
do''4 re''8 do'' si' re'' do''2. |
r4 la''8 sol'' fa'' la'' sol'' fa'' mi'' re'' mi''4 |
fa'' do'' re'' mi''8 re'' do'' si' do''4 |
si'8 do'' re''4 do''8 si' do''4 sol'' do'''8 si'' |
la'' sol'' fa'' sol'' la'' fa'' sol'' fa'' mi'' fa'' sol'' mi'' |
fa''4 do''4. re''8 mi'' re'' do'' si' do''4 |
do''2 si'4 do''2. |

