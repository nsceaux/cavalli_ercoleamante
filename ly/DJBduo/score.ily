\score {
  \new StaffGroupNoBar <<
    \pygmalion\new StaffGroupNoBracket <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
      \new Staff << \global \includeNotes "partie1" >>
      \new Staff << \global \includeNotes "partie2" >>
    >>
    \new Staff \with { \haraKiri } \withLyrics <<
      \global \keepWithTag #'paggio \includeNotes "voix"
    >> \keepWithTag #'paggio \includeLyrics "paroles"
    \new Staff \with { \haraKiri } \withLyrics <<
      \global \keepWithTag #'licco \includeNotes "voix"
    >> \keepWithTag #'licco \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \original\origLayout {
        s2.*17\break s2.*20\break s2.*18\pageBreak
        s2.*17\break s2.*16\break
      }
      \pygmalion\origLayout {
        s2.*6\break s2.*8\pageBreak
        s2.*9\break s2.*10\pageBreak
        s2.*10\break s2.*10\pageBreak
        s2.*9\break s2.*10\pageBreak
        s2.*9\break s2.*9\pageBreak
        s2.*5\break s1.*4\break s1.*4\pageBreak
        s1.*4\break s1.*4\pageBreak
        s1.*4\break s1.*4\pageBreak
        s1.*4\break s1.*4\pageBreak
        s1.*4\break s1.*4\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
