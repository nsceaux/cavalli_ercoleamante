\clef "basse"
\transpose do #(if (eqv? #t (ly:get-option 'pygmalion))
                   (ly:make-pitch -1 1)
                   (ly:make-pitch -1 0))
{
r4 r sib, |
mib2 do4 fa2 re4 |
sol2. re4 mib sib, |
fa2. |
sib, mib |
fa sib, |
mib fa |
sib, fa |
sib, do |
fa,2 fa4 |
re2 sol4 mib2 fa4 |
re2 mib4 do2. |
re2 mib4 fa2. |
re4 mib2 fa2. |
sib,2 sib,4 mib2 do4 fa2 re4 |
sol2. re4 mib sib, fa2. |
sib,2 sib,4 mib2 re4 |
do fa2 sib,2. |
fa2 fa4 sib2 la4 |
sol do'2 fa2. |
do'2 la4 sib2. |
sib4 sol fa do'2. |
fa2 re4 mib2. |
fa4 re do sol2. |
do fa |
sib, mib4. re8 do4 |
fa2. sol |
mib fa |
re mib2 fa4 |
sol mib2 |
fa2. sib,2 do4 |
re mib2 fa2. |
sib,2. fa |
do' la4 sib fa |
do'2. fa |
sol2 fa4 mib2. |
fa2 mib4 re2. |
mib2 re4 do2. |
re2 do4 sib,2. do |
re mib4 do2 |
do4 re2 sol fa4 |
mib2. fa fa4 sol2 |
do2. fa |
sib, mib |
fa re |
mib fa2 fa4 |
sol mib2 |
\ficta mib4 fa2 sib,2 sib,4 |
mib2 do4 fa2 re4 |
sol2. re4 mib sib, |
fa2.
}
<<
  \original { sib,1*3/4\fermata | }
  \pygmalion {
    do2 do4 |
    fa2 re4 sol2 mi4 |
    la2. mi4 fa do |
    sol2. do2 do4 |
    fa2 re4 sol2 mi4 |
    la2. mi4 fa do |
    sol2. do2 do4 |
    fa2 re4 sol2 mi4 |
    la2. mi4 fa do |
    sol2. do2 do4 |
    fa2 re4 sol2 mi4 |
    la2. mi4 fa do |
    sol2. do2 do4 |
    fa2 re4 sol2 mi4 |
    la2. mi4 fa do |
    sol2. do2 do4 |
    fa2 re4 sol2 mi4 |
    la2. mi4 fa do |
    sol2. do2 do4 |
    fa2 re4 sol2 mi4 |
    la2. mi4 fa do |
    sol2. do2 do4 |
    fa2 re4 sol2 mi4 |
    la2. mi4 fa do |
    sol2. do2 do4 |
    fa2 re4 sol2 mi4 |
    la2. mi4 fa do |
    sol8 la sol mi fa sol do do' do' si la sol |
    fa mi re do si, la, sol, sol sol fa mi re |
    do si, la, la la sol fa mi fa re mi fa |
    sol sol, sol, la, si, sol, do do do re mi do |
    fa re re mi fa re sol mi mi fa sol mi |
    la la, la, si, do re mi do fa mi re do |
    sol2 sol,4 do2 do4 |
    fa2 re4 sol2 mi4 |
    la2. mi4 fa do |
    sol2. do2 do4 |
    fa2 re4 sol2 mi4 |
    la2. mi4 fa do |
    sol2. do |
  }
>>
