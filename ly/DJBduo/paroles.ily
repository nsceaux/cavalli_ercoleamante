\tag #'paggio {
  A -- mor, ch’ha sen -- no in sé,
  va già va già d’ac -- cor -- do,
  ch’il più con -- ten -- to è in te
  ch’il più ba -- lor -- do. __
  O -- gni dol -- ce, che puoi da -- re
  è d’as -- sen -- tio al -- tro sci -- rop -- po __
  e le tue gio -- ie gio -- ie più ca -- re
  o son fal -- se,
  o son fal -- se,
  o son fal -- se, o cos -- tan trop -- po
  o cos -- tan
  o cos -- tan trop -- po
  o son fal -- se, o cos -- tan trop -- po:
  e co -- sì in si -- mil fro -- de
  lie -- to è più chi men ve -- de, e cre -- de, e go -- de
  e co -- sì in si -- mil fro -- de
  lie -- to è più chi men ve -- de, e cre -- de, e __ go -- de. __
}
\tag #'licco {
  A -- mor, chi ha sen -- no in sé,
  va già va già d’ac -- cor -- do, __
  ch’il più con -- ten -- to è in te
  ch’il più ba -- lor -- do
  ch’il più ba -- lor -- do.
  O -- gni dol -- ce, che puoi da -- re
  è d’as -- sen -- tio al -- tro sci -- rop -- po __
  e le tue gio -- ie gio -- ie più ca -- re
  o son fal -- se,
  o son fal -- se,
  o son fal -- se, o cos -- tan trop -- po
  o cos -- tan
  o cos -- tan trop -- po __
  o son fal -- se, o cos -- tan trop -- po:
  e co -- sì in si -- mil fro -- de
  lie -- to è più chi men ve -- de, e cre -- de, e __ go -- de
  e co -- sì in si -- mil fro -- de
  lie -- to è più chi men ve -- de, e cre -- de, e go -- de,
  e cre -- de, e go -- de. __
}
\tag #'basse {
  A -- mor, chi ha sen -- no in sé,

  A -- mor, ch’ha sen -- no in sé,
  va già va già d’ac -- cor -- do,
  ch’il più con -- ten -- to è in te

  ch’il più ba -- lor -- do

  ch’il più ba -- lor -- do. __

  O -- gni dol -- ce, che puoi da -- re

  O -- gni dol -- ce, che puoi da -- re

  è d’as -- sen -- tio al -- tro sci -- rop -- po __

  è d’as -- sen -- tio al -- tro sci -- rop -- po __
  e le tue gio -- ie gio -- ie più ca -- re
  o son fal -- se,
  o son fal-

  o son fal -- se, o cos -- tan trop-

  o son fal -- se, o cos -- tan trop -- po
  o cos -- tan
  o cos -- tan trop -- po
  o son fal -- se, o cos -- tan trop -- po:
  e co -- sì in si -- mil fro-

  lie -- to è più chi men ve -- de, e cre -- de, e __

  lie -- to è più chi men ve -- de, e cre -- de, e go -- de
  e co -- sì in si -- mil fro-

  lie -- to è più chi men ve-

  lie -- to è più chi men ve -- de, e cre -- de, e __ go -- de. __
}
