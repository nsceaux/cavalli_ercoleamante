\clef "viole2"
\transpose do #(if (eqv? #t (ly:get-option 'pygmalion))
                   (ly:make-pitch -1 1)
                   (ly:make-pitch -1 0))
{
r4 r fa' |
mib' mib'2 do'4 fa sib |
sib2.~ sib8 do' sol4 sib |
do'2 fa4 |
fa4 sib2 sol4 la sib |
sib la2 sib8 do' re' mib' fa'4 |
mib'2 sib4 fa fa'2 |
r4 sib sol la4. sib8 do'4 |
re'4 sib re' do'2. |
do'4 la2 |
sib sib4 sol do'2 |
sib4 re' do' sib la2 |
sib2.~ sib4 la4. sib8 |
fa4 sol2 fa4 fa' do' |
re'2 fa'4 mib' mib'2 do'4 fa sib |
sib2.~ sib8 do' sol4 sib do'2 fa4 |
fa2. sol4 la sib |
sib2 la4 sib2. |
sib4 do' fa' re' fa' do' |
sol'2 do'4 do'2. |
r4 mi' do' fa'2 fa'4 |
fa' mi' re' fa'2 do'4 |
do'2 sib do' |
la4 fa sol r sol' re' |
mi' fa' do' do'2 fa4 |
fa2. sol4 la sib |
la re' do' sib sol2~ |
sol2 sib la |
fa4 sib sib sib do' la |
sib2 sol4 |
la2 do'4 re'2 sol4 |
sib2 la4 la2. |
sib2 sol4 do'2 do'4 |
mi' re' do'2 fa'4 fa' |
sol'2 do' la4 la |
re' sol la sib do' sib |
la2 sib4 fa2. |
sib4 mib fa sol do'2 |
r4 fa do' sol sib4. la8 sol2 la4~ |
la sol sib~ sib do' sib~ |
sib la2 sol4 re'2 |
do'4 re' mib' la2 do' sol |
sol4 la sol fa la la |
re' re' do' sib2 do'4 |
la fa2 sib fa4 |
sol4 la sib fa8 sol la sib do'4 |
sib4. sol8 sol4~ |
sol do' fa fa2 fa'4 |
mib' mib'2 do'4 fa sib |
sib2.~ sib8 do' sol4 sib |
do'2 fa4 \original { fa1*3/4\fermata | }
}
\pygmalion {
  sol4 do'2 |
  do' re'4 re'2 mi'4 |
  fa'2 mi'4 sol' do' sol' |
  sol'2 sol'4 sol'2 mi'4 |
  do' fa' re' re'2 mi'4 |
  mi'2 fa'4 sol' la' sol' |
  sol'2.~ sol'2 sol'4 |
  la'8 sol' fa' mi' fa'4 sol'2. |
  do'2 mi'4 mi' la do' |
  re'2 sol'4 mi'2 do'4 |
  do'' la'2 re'4 mi' sol' |
  do'2 fa'4 do' fa sol |
  sol2 re'4 do'2 sol'4 |
  fa'2 fa'4 sol'2. |
  fa'2 fa'4 sol' la' sol' |
  sol'2. sol'2 do'4 |
  do' fa'2 re'4 sol'2 |
  fa' do'4 do'' la' sol' |
  sol'2. sol'2 do'4~ |
  do' la si8 do' re'4 si do'8 re' |
  mi'4 re' do' sol' re' sol |
  sol2. sol2 do'4 |
  do'4 la re' re' si mi' |
  fa'8 sol' la'4 la mi' la sol |
  sol2 re'4 do' sol2 |
  la la4 si2 do'4 |
  do'2. si4 la do'~ |
  do'8 do' re' mi' re'4 do'2. |
  fa'2 sol'4~ sol' si do'8 re' |
  mi'2 do'4 do' fa' do' |
  sol' re'2 do'2. |
  do'4 fa'2 re'4 sol8 la si sol |
  la si do' re' mi'4~ mi' do'2 |
  re'8 do' si do' re'4 mi'2 sol'4 |
  do' fa'2 re'4 sol do' |
  do'2 la4 do' la sol |
  sol' re' sol sol do'2 |
  do'4 la2 si mi'4 |
  do'2 fa'4 mi' la sol |
  sol sol2 sol2. |
}

