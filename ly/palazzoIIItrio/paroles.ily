\tag #'(voix1 basse) {
  Se con pla -- ci -- di sguar -- di
  fil -- li mo -- stra pie -- tà, __
  fil -- li mo -- stra __ pie -- tà,
  io __ be -- ne -- di -- co i dar -- di,
  ché sa -- et -- te più dol -- ci a -- mor non ha.
  Io __ be -- ne -- di -- co i dar -- di,
  ché sa -- et -- te più dol -- ci a -- mor non ha
  a -- mor __ non ha a -- mor __ a -- mor __ non ha.
  
  Ma non pe -- rò
  ma non pe -- rò mi pen -- to
  del mio lun -- go tor -- men -- to,
  se sde -- gna -- ti gli gi -- ra, __
  ché son bel -- li quei lu -- mi an -- co nell’ i -- ra
  an -- co nell’ i -- ra __
  ché son bel -- li quei lu -- mi an -- co nell’ i -- ra
  ché son bel -- li quei lu -- mi an -- co an -- co nell’ i -- ra.
}
\tag #'voix2 {
  Se con pla -- ci -- di sguar -- di
  fil -- li mo -- stra pie -- tà,
  fil -- li fil -- li mo -- stra pie -- tà,
  io be -- ne -- di -- co i dar -- di,
  ché sa -- et -- te più dol -- ci a -- mor non ha.
  Io __ be -- ne -- di -- co i dar -- di,
  ché sa -- et -- te più dol -- ci a -- mor non ha
  a -- mor __ non ha a -- mor __ a -- mor non ha.
  
  Ma non pe -- rò
  ma non pe -- rò mi pen -- to __
  del mio lun -- go tor -- men -- to, __
  se sde -- gna -- ti gli gi -- ra __ gli gi -- ra,
  ché son bel -- li quei lu -- mi an -- co nell’ i -- ra __
  an -- co nell’ i -- ra __
  ché son bel -- li quei lu -- mi quei lu -- mi an -- co nell’ i -- ra __
  ché son bel -- li quei lu -- mi
  ché son bel -- li quei lu -- mi an -- co an -- co nell’ i -- ra. __
}
\tag #'voix3 {
  Se con pla -- ci -- di sguar -- di
  fil -- li mo -- stra pie -- tà, __
  io io __ be -- ne -- di -- co i dar -- di,
  ché sa -- et -- te più dol -- ci a -- mor non ha.
  Io __ be -- ne -- di -- co i dar -- di, i dar -- di,
  ché sa -- et -- te più dol -- ci a -- mor non ha
  a -- mor __ non ha a -- mor __ a -- mor non ha.
  
  Ma non pe -- rò
  ma non pe -- rò mi pen -- to
  del mio lun -- go tor -- men -- to,
  se sde -- gna -- ti gli gi -- ra gli gi -- ra,
  ché son bel -- li quei lu -- mi quei lu -- mi __ an -- co nell’ i -- ra
  an -- co nell’ i -- ra
  ché son bel -- li quei lu -- mi quei lu -- mi an -- co nell’ i -- ra
  ché son bel -- li quei lu -- mi an -- co nell’ i -- ra.
}
