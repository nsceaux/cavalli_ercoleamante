\clef "basse" re1. |
re |
\blackNotation { la1 re } |
sol1. |
sol |
re' |
do'1 la2 |
sib1. |
la |
\blackNotation { sol1 fa mib } |
re1. |
re |
sol |
fa~ |
fa2 mi1 |
fa1. |
sol |
sol |
do |
do2 fa sol |
lab sol fa |
sol1. |
do |
fa2 sol1 |
\blackNotation { do'2 sib1 } |
la1. |
\blackNotation { sol1 fa mib } |
re2 sib1 |
sib2 la1 |
\blackNotation { sib1 mib fa } |
sib,1. |
fa |
sol |
re |
mib2 do1 |
\blackNotation { re1 sib,\breve do\breve. } |
re1 do2 |
re1. |
sol, |
\blackNotation { sol,1 do re } |
sol,1. |
\blackNotation { re1 sol la } |
sib1 sol2 |
la1. |
re2 re1 |
mib1. |
fa |
sib,2 sib sol |
la1. |
sol |
fa |
sol |
do1 re2 |
mi1. |
fa |
do1 do'2 |
do'1 si2 |
\blackNotation { do'2 mi1 } |
fa1 fad2 |
sol1. |
do |
fa2 re1 |
mi1. |
la, |
la |
re |
la |
la2 sold1 |
la1. |
fad |
sol |
la |
la |
re |
re |
sib |
la |
sol |
fa |
mib |
re sib, |
do |
re |
re |
sol, |
