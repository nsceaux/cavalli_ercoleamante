\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \keepWithTag #'voix1 \includeNotes "voix"
    >> \keepWithTag #'voix1 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'voix2 \includeNotes "voix"
    >> \keepWithTag #'voix2 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'voix3 \includeNotes "voix"
    >> \keepWithTag #'voix3 \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1.*2 s1*2 s1.\break s1.*7\pageBreak
        s1.*8\break s1.*6\break s1.*2 s1*3 s1.*2\pageBreak
        s1.*7\break s1*6 s1.*3\break s1.*8\pageBreak
        s1.*6\break s1.*5\break s1.*6\pageBreak
        s1.*5\break s1.*7\break s1.*6\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}