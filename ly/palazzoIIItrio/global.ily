\key re \minor
\beginMark "A 3" \midiTempo#240
\time 3/2 s1.*2
\measure 2/1 s1*2
\measure 3/2 s1.*6
\measure 3/1 s1*3
\measure 3/2 s1.*16
\measure 3/1 s1*3
\measure 3/2 s1.*2
\measure 3/1 s1*3
\measure 3/2 s1.*5
\measure 6/1 s1*6
\measure 3/2 s1.*3 \bar ":|."
\measure 3/1 s1*3
\measure 3/2 s1.
\measure 3/1 s1*3
\measure 3/2 s1.*38
\measure 3/1 s1*3
\measure 3/2 s1.*4 \bar "|."


