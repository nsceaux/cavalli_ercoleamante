\whiteNoteHeadsOn
<<
  \tag #'(voix1 basse) {
    \clef "vsoprano" \whiteNoteHeadsOn r2 re'' re'' |
    re''2. re''8*2 re''2 |
    do'' do'' do''8*2[\melisma sib' do'' la'] |
    sib'1\melismaEnd sib'2 |
    sib'2. sib'8*2 sib'2 |
    la'1.~ |
    la' |
    r2 sib'8*2[\melisma la' sib' sol'] |
    do''1\melismaEnd la'2 |
    sib'8*2[ la' sib' do''] re''[ sib' do'' re''] do''[ re'' mib'' do''] |
    re''1. r2 fa''1~ |
    fa''2 mi'' mi'' |
    fa''1. |
    sol'' |
    do''2.\melisma re''16*2[ mi''] fa''2~ |
    fa'' mib''8*2[ re'' mib'' do''] |
    re''1.\melismaEnd |
    do''2 mib''2. mib''8*2 |
    \ficta mib''2 re'' mib'' |
    fa''1 mib''2 |
    re''1 re''2 |
    do''1. |
    R1. |
    r2 sol'1~ |
    sol'2 fad' fad' |
    \blackNotation { sol'1 la' do'' } |
    la'2 re''2. re''8*2 |
    do''2 do'' do'' |
    \blackNotation { sib'1 sib' } sib'2. la'8*2 |
    sib'2 r4 fa''8*2[ \ficta mib'' re''] |
    do''[\melisma sib' do'' mib'' re'' do''] |
    sib'[ la' sib' re'' do'' sib'] |
    la'2. sol'8*2[ la' sib'] |
    do''1\melismaEnd sib'2 |
    \blackNotation { la'1 re''\breve~ } re''2\melisma do''8*2([ sib']) la'[ sol' la' sib'] do''2. sib'8*2 |
    la'1\melismaEnd sib'2 |
    do''8*2[\melisma sib'] la'2.\melismaEnd sib'8*2 |
    sol'1. |
    \blackNotation { sol'1 sol' fad' } |
    sol'1. |
    \blackNotation { re''1 re'' dod'' } |
    re''1 mi''2 |
    mi''1. |
    re''2 sib' sib' |
    sib' do'' do'' |
    do''1. |
    sib' |
    R1. |
    r2 sib'8*2[ la'] sib'[ sol'] |
    la'2 si' do'' |
    do''1 si'2( |
    do''1.) |
    R1. |
    r2 do'' re'' |
    mi'' mi'' mi'' |
    fa''1. |
    \blackNotation { mi''2 re''1 } |
    re''1 re''2 |
    re''1. |
    do''2 mi''!1~ |
    mi''2 re'' do'' |
    do''2( si'1) |
    la'1.~ |
    la' |
    r2 la' si' |
    dod'' dod'' dod'' |
    re''1. |
    dod''2 la'1~ |
    la'2\melisma sib'8*2[ do''] re''2~ |
    re'' mi''8*2[ fa''] sol''2~ |
    sol''\melismaEnd mi'' fa'' |
    mi''1. |
    re'' |
    R1. |
    r2 re' mi' |
    fad' fad' fad' |
    sol'1.\melisma |
    la'1 sib'8*2[ la'] |
    sol'[ la' sib' do'' re'' do'']\melismaEnd |
    re''2 \blackNotation { fa''1. re''1 } |
    r2 \blackNotation { do''1 | la' sib'2 | }
    la'1. |
    sol' |
  }
  \tag #'voix2 {
    \clef "vsoprano" r2 re' mi' |
    fa'2. fa'8*2 sol'2 |
    la' la' la'8*2[\melisma sol' la' fad'] |
    sol'1\melismaEnd sol'2 |
    sol'2. sol'8*2 sol'2 |
    fa' fa'8*2[\melisma mi' fa' re'] |
    mi'1.\melismaEnd |
    re'2 \blackNotation { sol'1~ | sol'2 fad'1 | sol'1 la' sol' | }
    fad'1. |
    R1. |
    sib'1. |
    r2 la' la' |
    si' do''1 |
    la'1.( |
    sol')~ |
    sol' |
    sol'2 do''2. do''8*2 |
    do''2 si' do'' |
    re''\melisma si'\melismaEnd do'' |
    do''1 si'2 |
    do'' mib''1~ |
    mib''2 re'' re'' |
    \blackNotation { mib''2 re''1 } |
    do''2~ do''2.\melisma \whiteNoteHeadsOff re''8[ do''] |
    sib'8*2[ la' sib' do''] re''[ re' mi' fa'] \whiteNoteHeadsOn sol'2.( la'8*2)\melismaEnd |
    fa'2 fa''2. fa''8*2 |
    mib''2 mib'' mib'' |
    \blackNotation re''1 do''2.\melisma \whiteNoteHeadsOff re''8[ \ficta mib'']\melismaEnd do''2. \whiteNoteHeadsOn do''8*2 |
    sib'2 r4 re''8*2[ do'' sib'] |
    la'[\melisma sol' la' do'' sib' la'] |
    sol'[ fa' sol' sib' la' sol'] |
    fad'2. mi'8*2[ fad' sol'] |
    la'1\melismaEnd sol'2 |
    \blackNotation { fad'1 sib'\breve~ } sib'2\melisma la'8*2[ sib'] do''[ re'' do'' sib'] la'2. sol'8*2 |
    fad'1\melismaEnd sol'2 |
    sol'1 r4 fad'8*2 |
    sol'1. |
    sol'2( la') sib'( do'') la'2.\melisma sib'8*2\melismaEnd |
    sol'1. |
    re''2( mi'') fa''( sol'') mi''( fa'') |
    re''1 re''2 |
    re''1 dod''2\melisma |
    re''\melismaEnd fa' fa' |
    sol'2 la' sib' |
    sib'1 la'2( |
    sib') re''8*2([ do'']) re''([ sib']) |
    do''2 fa' fa'' |
    fa''1 mi''2( |
    fa''1) mib''2 |
    re''1. |
    do''2 mi' fa' |
    sol' sol' sol' |
    la'1\melisma sib'2 |
    do''1. |
    re''\melismaEnd |
    \blackNotation { sol'2 do''1 } |
    do''1 do''2 |
    do''1 si'2( |
    do'') do''1~ |
    do''2 si' la' |
    la'1 sold'2( |
    la') dod' re' |
    mi' mi' mi' |
    fa'1. |
    mi'1 la'2 |
    si'1. |
    mi' |
    la' |
    si' |
    \blackNotation { dod''2( re''1) } |
    re''1 dod''2( |
    re'') re' mi' |
    fad' fad' fad' |
    sol'1.( |
    la') |
    sib'2 re'' mi'' |
    fa'' fa'' fa'' |
    sol''\melisma sol'8*2[ la' sib' sol']\melismaEnd |
    la'1 r sib' |
    \blackNotation { sol'2 la'1 } |
    fad'1 sol'2 |
    sol'1 fad'2\melisma |
    sol'1.\melismaEnd |
  }
  \tag #'voix3 {
    \clef "vtenor" R1*5 |
    r2 sol la |
    sib2. sib8*2 do'2 |
    re'1 re'2 |
    r do'8*2[ sib do' la]\melisma |
    sib1.\melismaEnd |
    la |
    \blackNotation { sol1 fa mib } |
    re1.~ |
    re |
    sol |
    r2 fa1~ |
    fa2 mi mi |
    fa la1 |
    si2\melisma \blackNotation { do'1~ | do'2 si1\melismaEnd | }
    do'2 do2. re8*2 |
    mib2 fa sol |
    lab\melisma sol\melismaEnd fa |
    sol1 sol2 |
    do do'1~ |
    do'2 si si |
    \blackNotation { do'2 sib1 } |
    la1. |
    \blackNotation { sol1 fa mib } |
    re2 sib2. sib8*2 |
    sib2 la la |
    \blackNotation { sib1 mib } fa2. fa8*2 |
    sib,1. |
    fa |
    sol( |
    re) |
    mib2( \blackNotation { do1) | re1 sib,\breve\melisma do\breve.( | }
    re1)\melismaEnd mi2 |
    re1 re2 |
    sol1. |
    \blackNotation { sib1 do re } |
    sol1. |
    \blackNotation { re1 sol la } |
    sib1 sol2 |
    la1. |
    re2 re re |
    mib mib mib |
    fa1. |
    sib,2 sib8*2([ la]) sib([ sol]) |
    la2 la la |
    sol1. |
    fa1 fa2 |
    sol1. |
    do2 do re |
    mi mi mi |
    fa1. |
    do1 do'2 |
    do'1 si2( |
    \blackNotation { do'2) mi1 } |
    fa1 fad2 |
    sol1. |
    do2 do2.\melisma re16*2[ mi] |
    fa2\melismaEnd re re |
    mi1. |
    la,2 la \ficta si |
    dod' dod' dod' |
    re'1. |
    la1 la2 |
    sold1. |
    la |
    fad |
    sol |
    la |
    la |
    \blackNotation { re2 re'1 } |
    do'1. |
    sib |
    la |
    sol |
    fa( |
    mib) |
    re1. sib, |
    do |
    re |
    re |
    sol |
  }
>>
