\clef "viole1" R1*17 |
r2 mib' |
reb'1~ |
reb'2 mib' |
sib1 |
do'2 reb' |
do'4 sol la2 |
sol2. |
do'4 fa' re' |
do' re'8 mib' fa' sol' |
lab'2 fa'4 |
do' sol' re' mib'2 mib'4 |
re' sib do'~ |
do' re' fa' |
sol'2.~ sol'4 fad'2 |
sol'2.~ sol'4 fad'2 |
sol'2. |
re'4 sib re' |
mib'2. |
fa'2 sol'4 do' fa'2 |
fa'4 re'4. fa'8 fa' lab' do''4 sib' |
lab'8 sol' fa'4 sol' |
fa'2 mi'4 |
fa' fa'4. sol'8 |
mi'4. re'8 mi'4 |
do' r sib |
fa'2 mi'4 fa'8 mib' reb' do' sib lab |
do'2 do'4 si re' do'8 sol' |
mib' fa' sol'4 re' |
do'2 do''4~ |
do'' si'2 |
do''2. do''2 la'4 |
sol'4. sib'8 lab'4 |
sol'2. sol' |
fa'2~ fa'8 mi' |
fa'4 sol'2 |
fa'4 do' reb' |
mib' re'8 fa' do'4 |
si do'2 |
sol2 do'4 |
do'2 si4 |
do'2. |
R1*6 |
