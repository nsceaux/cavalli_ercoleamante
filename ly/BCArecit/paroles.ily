E vuol dun -- que ci -- pri -- gna,
per far con -- tro di me gl’ul -- ti -- mi sfor -- zi
de’ più pun -- gen -- ti ol -- trag -- gi,
fa -- vo -- rir chi le vo -- glie heb -- be sì in -- te -- se
ad of -- fen -- der -- mi ogn’ o -- ra,
che ne gl’im -- pu -- ri suoi prin -- ci -- pi an -- co -- ra
pri -- ma d’es -- ser m’of -- fe -- se?
Chi pria di spi -- rar l’au -- re
spi -- rò de -- sio di dan -- neg -- giar -- mi, e do -- po
ha -- ver dal pet -- to mi -- o
trat -- ti i pri -- mi a -- li -- men -- ti al vi -- ver su -- o,
con in -- gra -- ta in -- so -- len -- za
d’uc -- ci -- der -- mi ten -- tan -- do o -- sò o -- sò fe -- rir -- mi?
Ah ch’in -- te -- si in -- te -- si i di -- se -- gni
ma non sia ch’a di -- sfar -- li al -- tri m’in -- se -- gni.
Di re -- ci -- pro -- co af -- fet -- to
ar -- don Hyl -- lo, e I -- o -- le,
e sol per mio dis -- pet -- to
l’i -- ni -- qua Dea non vuo -- le,
ch’I -- me -- neo li con -- giun -- ga? an -- zi an -- zi pro -- cu -- ra
per mio scor -- no mag -- gio -- re,
ch’il no -- do ma -- ri -- ta -- le on -- d’è ris -- tret -- to
Er -- co -- le a De -- ia -- ni -- ra al -- fin al -- fin si rom -- pa;
a ciò ch’I -- o -- le a que -- sti
del di lei ge -- ni -- to -- re em -- pio o -- mi -- ci -- da
con mo -- stru -- o -- si am -- ples -- si og -- gi og -- gi s’in -- ne -- sti.
E con qual ar -- te oh Di -- o? con ar -- ti in -- de -- gne
d’ogn’ a -- ni -- ma più vil non che di -- vi -- na. __
