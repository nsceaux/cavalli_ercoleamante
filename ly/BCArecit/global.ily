\key re \minor
\time 4/4 \midiTempo#120 s1*23
\digitTime\time 3/4 s2.*4
\measure 6/4 s1.
\measure 3/4 s2.*2
\measure 6/4 s1.*2
\measure 3/4 s2.*3
\measure 6/4 s1.*2
\measure 3/4 s2.*5
\measure 6/4 s1.*2
\measure 3/4 s2.*3
\measure 6/4 s1.
\measure 3/4 s2.
\measure 6/4 s1.
\pygmalion { \measure 3/4 s2.*8 \bar "|." }
\time 4/4 s1*4
\measure 2/1 s\breve \bar "|."


