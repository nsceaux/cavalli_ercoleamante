\clef "basse" do1 |
do~ |
do |
si,1 |
do~ |
do |
sib,~ |
sib,2 lab,~ |
lab, sol, |
sol,1~ |
sol, |
<<
  \original { do1 | }
  \pygmalion { do4 sol, do,2 | }
>>
do1 |
do |
<<
  \original { sib,1 | }
  \pygmalion { sib,2 sib,4 sib,4 | }
>>
lab,1~ |
lab, |
sol,2 sol |
fa1~ |
fa2 mib |
reb1 |
do2 sib, |
do fa, |
do2. |
do'4 sib2 |
lab4 sol2 |
fa2. |
fa4 sol2 do do'4 |
sib2 la4 |
sol2 fa4 |
mib2. re2 do4 |
sib, do2 re2. |
sol, |
sol2 fa4 |
mib2. |
re2 mib4 fa2. |
sib,2 sib4 lab2 sol4 |
fa2 sib,4 |
do2. |
reb |
do2 sib,4 |
<<
  \original { lab,2 sib,4~ | }
  \pygmalion { lab,4 r sib,~ | }
>>
sib, do2 fa,2. |
do2 do4 sol2 la8 si |
do'4 sib2 |
lab2. |
sol |
mi fa |
sol4 mib fa |
sol2. do |
\pygmalion {
  lab4 sib do'8 sol |
  lab4 sol mi! |
  fa mib reb |
  do sib, lab, |
  sol, lab,2 |
  sol,4 mib, fa, |
  sol,2. |
  do |
}
sib1 |
sol2 fa~ |
fa re~ |
re mib |
fa1 sib,\fermata |
