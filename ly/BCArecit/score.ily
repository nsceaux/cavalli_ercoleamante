\score {
  \new StaffGroupNoBar <<
    \pygmalion\new StaffGroupNoBracket \with { \haraKiriFirst } <<
      \new Staff << \global \includeNotes "viole0" >>
      \new Staff << \global \includeNotes "viole1" >>
      \new Staff << \global \includeNotes "viole2" >>
    >>
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*3\break s1*4\break s1*4\break s1*4\break s1*4\pageBreak
        s1*4\break s2.*7\break s2.*7\break s2.*7\break s2.*7\pageBreak
        s2.*8\break
      }
      \pygmalion\modVersion {
        s1*17\break
        s1*6 s2.*44\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
