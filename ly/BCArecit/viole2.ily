\clef "viole2" R1*17 |
r2 sib |
lab1~ |
lab2 do' |
fa2. fa4 |
sol4 lab2 sol4 |
lab8. sib16 do'4 do'2 |
mib'2. |
mib'4 re'4. sol'8 |
fa'4 sol' sol |
do'2 lab4~ |
lab sol2 sol do'4 |
re'2 la4 |
re' sol la |
sol sib la re'2. |
sol2 mi8 fad sol4 la2 |
sol re'4 |
sib sol lab |
sib2.~ |
sib4 la do'~ do' do' re' |
re'4 fa'2 do'4. do'8 sol'4 |
do' lab fa |
lab8 sib do'2 |
fa2 sib4 |
sol4. fa8 sol4 |
fa4 r8 lab sol fa |
sib4. lab16 sol do'4~ do' fa2 |
sol4 do' la re'2 la8 re' |
do'4 re' sol' |
fa'2 fa'4 |
re' sol'2 |
sol'2. la'4 fa' re' |
mib'8 si do'4. fa'8 |
mib' fa' sol'4 re' do'2. |
do'4 reb' do'~ |
do'8 re' mi'4 do'8 sol |
lab4 sol fa |
lab8 mib sol re fa4 |
re4 do2 |
re4 mib lab |
sol2 re4 |
do2. |
R1*6 |
