\clef "vsoprano" <>^\markup\character Giunone
r4 mib''8 mib'' do''4 do''8 si' |
do''4 do''8 do'' do''4 do''8 do''16 do'' |
lab'4 lab'8 lab'16 sol' lab'!4 lab'8 lab' |
lab' lab' lab' sol' sol'4 sol' |
sol'8 sol' sol' sol'16 fa' sol'8 sol' sol' sol'16 la' |
la'8 la' la' la' la'4 la'8 sib' |
sib'4 sib'8 sib' sib' sib' sib' sib' |
sib' sib' sib' do'' do'' do'' do'' do'' |
do''4 do''8 si' re''4 re'' |
r4 si' si'8 si' si' do'' |
re'' re''16 re'' re''8 re'' re'' re'' re'' do'' |
mib''4 mib''8 sol'' mib''4 mib''8 mib'' |
mib'' mib'' mib'' re'' mib'' mib'' mib'' mib'' |
mib''4 mib''8 mib'' mib'' mib'' mib'' re'' |
fa''4 fa'' sol''8 sol'' sol'' sol''16 sol'' |
do''4 do''8 do'' do'' do'' re'' mib'' |
fa''4 fa''8 fa'' do'' fa'' do'' si' |
re''4 re'' r mib'' |
r fa'' reb'' reb''8 fa'' |
reb''4 reb''8 do'' do''4 do'' |
r4 sib'8 reb'' sib'4 sib'8 sib' |
sib'4 lab' reb'' sib'8 do'' |
lab'4. sol'8( fa'2) |
r4 r sol'8 lab' |
lab'4. sol'8 sol'4 |
do'' si' re''8 mib'' |
fa''4 mib'' do''8 re'' |
mib''4( re''2) do'' mib''4 |
re''4. do''8 do'' do'' |
do''4 sib'8 re'' la' sib' |
do''4 sol'( la') sib' la' sib'8 do'' |
re''4 sol' la' sib'( la'2) |
sol'2. |
sib'8 sol' sib'4 lab'8 sol' |
sol'4 sol' sol'8 la' |
sib'4 do''4. re''8 sib'2 la'4( |
sib'2) sib'4 fa'4. fa'8 fa' sol' |
lab'4 lab'8 lab' sib' do'' |
lab'4 sol'2 |
fa'4 fa'8 fa' fa' mi' |
sol'4 sol' sol' |
do'' r8 fa' sol' lab' |
lab'4( sol'2) fa'4. lab'!8 sib' do'' |
sol'4 sol' la' si' si' do''8 re'' |
mib''4 re''4. mib''8 |
fa''4 fa''8 do'' si' do'' |
re''4 re''8 sol' la' si' |
do''4 sib'( do'') la' la' re''8 la' |
si'4 sol' do''8[ re''] |
mib''4( re''2) do''2. |
\pygmalion R2.*8 |
r8 fa'' fa'' fa'' re''4 re''8 fa'' |
sib'4.( do''8) do''2 |
r8 do'' fa'' do'' re''4 re''8 fa'' |
sib'8. lab'16 lab'8 sol' sol'4. do''8 |
re''8 la' sib'2 la'4( sib'1)\fermata |
