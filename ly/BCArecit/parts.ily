\piecePartSpecs
#(if (eqv? #t (ly:get-option 'pygmalion))
     '((violes #:score-template "score-violes-voix")
        (basse #:score-template "score-basse-continue-voix"))
     '((basse #:score-template "score-voix")))
