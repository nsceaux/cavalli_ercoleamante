\clef "viole0" R1*17 |
r2 sol' |
lab'1~ |
lab'2 sol' |
fa'2. fa'4 |
mi' fa'2 sol'4 |
fa' mi' fa'2 |
do''2.~ |
do''4 re''8 sib' re'' mib'' |
fa''4 re''4. do''8 |
do''2. |
do''2 si'4 do''2 sol'4~ |
sol' sol'4. fad'8 |
sol'2 re''4 |
mib'' sib' do'' re''2.~ |
re''4 sib' la' re''4. do''8 si' la' |
si'2. |
sol'4 mib' fa' |
sol'8 lab' sib'4 sib'8 do'' |
re'' mib'' fa''4 mib''8 fa'' mib'' re'' do''4. do''8 |
re''8 sib' sib' re'' fa''4~ fa'' fa'' mi'' |
fa''8 mib'' reb''4. mib''8 |
do''2.~ |
do''4 sib'2 |
do''2 do''4 |
fa' r reb'' |
reb'' do''2 lab'4. lab'8 sol' fa' |
mib'4 sol' fad' sol'2. |
sol'8 la' sib'4. sol'8 |
do''4. do''8 re'' mib'' |
fa'' mib'' re''4. re''8 |
sol''2. fa''2 fa''8 mib'' |
mib''4. re''8 do'' si' |
do''2 si'4 do''2 do''8 sol' |
lab'4 sol'2 |
do''4 sib'4. do''8 |
reb'' lab' do'' sol' sib'4 |
lab' sib'8 lab' sol' fa' |
sol'4. re'8 fa'4 |
si4 do'4. re'8 |
mib'4 re'2 |
do'2. | \allowPageTurn
R1*6 |
