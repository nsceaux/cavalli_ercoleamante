OUTPUT_DIR=out
DELIVERY_DIR=delivery
DELIVERY_DIR_PYGMALION=~/Dropbox/Pygmalion/ErcoleAmante
NENUVAR_LIB_PATH=$$(pwd)/../nenuvar-lib
LILYPOND_CMD=lilypond -I$$(pwd) -I$(NENUVAR_LIB_PATH) \
  --loglevel=WARN -ddelete-intermediate-files \
  -dno-protected-scheme-parsing
PROJECT=Cavalli_ErcoleAmante

# Conducteur
conducteur:
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT) main.ly
.PHONY: conducteur
# Dessus
dessus:
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)-dessus -dpart=dessus part.ly
.PHONY: dessus
# Parties
parties:
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)-parties -dpart=parties part.ly
.PHONY: parties
# Basse
basse:
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)-basse -dpart=basse part.ly
.PHONY: basse

delivery:
	@mkdir -p $(DELIVERY_DIR)/
	@if [ -e $(OUTPUT_DIR)/$(PROJECT).pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT).pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-dessus.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-dessus.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-parties.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-parties.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-basse.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-basse.pdf $(DELIVERY_DIR)/; fi
.PHONY: delivery

clean:
	@rm -f $(OUTPUT_DIR)/$(PROJECT)-* $(OUTPUT_DIR)/$(PROJECT).*
.PHONY: clean

parts: dessus parties basse
.PHONY: parts

all: check parts conducteur delivery clean
.PHONY: all

###
### Version Pygmalion
###
# Conducteur
conducteur-pygmalion:
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)-pygmalion -dpygmalion main-pygmalion.ly
.PHONY: conducteur-pygmalion

parts-pygmalion:
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)-pygmalion-dessus -dpygmalion -dpart=dessus part-pygmalion.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)-pygmalion-altos -dpygmalion -dpart=parties part-pygmalion.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)-pygmalion-violes -dpygmalion -dpart=violes part-pygmalion.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)-pygmalion-basse -dpygmalion -dpart=basse part-pygmalion.ly
.PHONY: parts-pygmalion

annexes-pygmalion:
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)-pygmalion-annexes -dpygmalion annexes-pygmalion.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)-pygmalion-annexes-dessus -dpygmalion -dpart=dessus annexes-pygmalion.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)-pygmalion-annexes-altos -dpygmalion -dpart=parties annexes-pygmalion.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)-pygmalion-annexes-violes -dpygmalion -dpart=violes annexes-pygmalion.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)-pygmalion-annexes-basse -dpygmalion -dpart=basse annexes-pygmalion.ly
.PHONY: annexes-pygmalion

pygmalion: check parts-pygmalion conducteur-pygmalion annexes-pygmalion delivery-pygmalion clean
.PHONY: pygmalion

delivery-pygmalion:
	@mkdir -p $(DELIVERY_DIR_PYGMALION)/
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-pygmalion.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-pygmalion.pdf $(DELIVERY_DIR_PYGMALION)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-pygmalion-dessus.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-pygmalion-dessus.pdf $(DELIVERY_DIR_PYGMALION)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-pygmalion-altos.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-pygmalion-altos.pdf $(DELIVERY_DIR_PYGMALION)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-pygmalion-violes.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-pygmalion-violes.pdf $(DELIVERY_DIR_PYGMALION)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-pygmalion-basse.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-pygmalion-basse.pdf $(DELIVERY_DIR_PYGMALION)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-pygmalion-annexes.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-pygmalion-annexes.pdf $(DELIVERY_DIR_PYGMALION)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-pygmalion-annexes-dessus.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-pygmalion-annexes-dessus.pdf $(DELIVERY_DIR_PYGMALION)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-pygmalion-annexes-altos.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-pygmalion-annexes-altos.pdf $(DELIVERY_DIR_PYGMALION)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-pygmalion-annexes-violes.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-pygmalion-annexes-violes.pdf $(DELIVERY_DIR_PYGMALION)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-pygmalion-annexes-basse.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-pygmalion-annexes-basse.pdf $(DELIVERY_DIR_PYGMALION)/; fi
.PHONY: delivery-pygmalion

check:
	@if [ ! -d $(NENUVAR_LIB_PATH) ]; then \
	  echo "Please install nenuvar-lib in parent directory:"; \
	  echo " cd .. && git clone https://github.com/nsceaux/nenuvar-lib.git"; \
	  false; \
	fi
.PHONY: check
