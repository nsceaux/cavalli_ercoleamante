\livretAct ATTO SECONDO
\livretScene SCENA PRIMA
\livretDescAtt\center-column {
  \justify {
    La scena si cangia in un gran cortile del palazzo reale.
  }
  Hyllo, e Iole.
}  
\livretPers Hyllo e Iole
\livretRef#'CABduo
%# Amor ardor più rari
%# accesi mai non ha,
%# che quelli onde del pari
%# le nostre alme disfà
%# d'avverso ciel le lampe
%# contro di lui si sforzino,
%# ch'in vece, che l'amorzino,
%# l'arricchiran di vampe.
\livretPers Iole
\livretRef#'CACrecit
%# Pure alfine il rispetto
%# di figlio al genitor fia ch'in te cangi
%# sì amoroso linguaggio.
\livretPers Hyllo
%# Che più tosto il tuo affetto
%# non renda anch'egli al forte Alcide omaggio.
\livretPers Iole
%# Ah che forzar un core
%# no 'l puote altri che amore.
\livretPers Hyllo
%# E di rivale il titolo odioso
%# qualunque altro bel nome,
%# che concorra con lui, rende ozioso;
%# una sol vita il genitor mi diede,
%# e per te, che mia vita
%# molto più cara sei
%# mille vite darei.
\livretPers Iole
%# E per te sol mio ben,
%# all'empio usurpator contenta i' cedo
%# il regno, e 'l mondo tutto, e te sol chiedo.
\livretPers Hyllo e Iole
\livretRef#'CADduo
%# Gare d'affetto ardenti
%# deh non cedete a i guai,
%# e nel goder non vi stancate mai,
%# che de' vostri argumenti
%# nell'uguaglianza sol tutta si sta
%# l'amorosa felicità.

\livretScene SCENA SECONDA
\livretDescAtt\wordwrap-center { Paggio, Iole, e Hyllo. }
\livretPers Paggio
\livretRef#'CBArecit
%# Ercole a dirti invia, ch'altro non bada,
%# che di saper, se nel giardin de' fiori
%# di condurti a diporto oggi t'aggrada.
\livretPers Iole
%# Come fia, che ciò nieghi?
%# D'un che sovra di me le stelle alzaro
%# son comandi anco i prieghi.
\livretPers Hyllo
%# Ahi qual torbido, e amaro
%# velen presaga gelosia m'appresta,
%# di cui solo il timor già mi funesta.
\livretPers Iole
%# Non temere Hyllo caro:
%# che non potrà mai violenza ardita
%# togliermi a te, senza a me tor la vita.
\livretPers Hyllo
%# E quando anche in tal guisa
%# ogn'un meco ti perda amato bene,
%# qual miglior sorte avrò, che cangiar pene?
\livretPers Iole
%# Da sì grave timor l'alma disvezza,
%# che quanto Ercol per me palesa affetto,
%# tant'ha rispetto, ed io per te fermezza.
%# Torna, digli, ch'io vado: Hyllo vien meco.
\livretPers Hyllo
%# E quando io non son teco?
%# Se dovunque il mio piè giri, o la mente
%# t'adoro ogn'or presente.
\livretRef#'CBBduo
%# Chi può vivere un sol istante
%# lunge dal bello che l'invaghì,
%# dica pur, ch'in lui morì
%# ogni pregio di vero amante;
%# d'amore il foco
%# per ogni poco
%# ch'intiepidiscasi ghiaccio diviene,
%# e le di lui catene
%# più strettamente avvolte
%# ogni poco, che cedano, son sciolte.
\livretPers Iole
%# O gloria
%# d'amor più nobile
%# con fede immobile
%# sempr'arde più;
%# memoria
%# non mai vi fu,
%# che la vittoria
%# mancassi tu.
%# Si sciogliono
%# qual or gl'instabili
%# rei più dannabili
%# Amor non ha.
%# Lo spogliono
%# di deità
%# poiché gli togliono
%# l'eternità.

\livretScene SCENA TERZA
\livretPers Paggio
\livretRef #'CCArecit
%# E che cosa è quest'amore?
%# Di cui parlan tanto in corte,
%# e canzon di mille sorte
%# di lui cantano a tutt'ore.
%# Egli è qualche ciurmadore
%# poi che a quel, che sento dire
%# (senza punto intender come)
%# mentre a stille dà il gioire
%# e il penar dispensa a some,
%# fassi il mondo adoratore
%# egli è qualche ciurmadore.
%# Di vederlo ebbi gran brame
%# ma poi seppi, ch'è impossibile,
%# ch'egli sia già mai visibile
%# perché sempre è con le dame,
%# e che queste al finger dotte
%# se lo tengano celato,
%# come s'ei stesse appiattato
%# dentro le cimmerie grotte.

\livretScene SCENA QUARTA
\livretDescAtt\wordwrap-center { Deianira, Licco, Paggio. }
\livretPers Licco
\livretRef#'CDArecit
%# Buon dì gentil fanciullo.
\livretPers Paggio
%# E buona notte.
\livretPers Licco
%# Ma dove in tanta fretta?
\livretPers Paggio
%# A far da gran messaggio.
\livretPers Licco
%# Ascolta un poco, aspetta;
%# che so qual possa aver faccende un Paggio.
\livretPers Paggio
%# E che tu sai? ch'Iole  
%#- ad Ercole…
\livretPers Licco
%#= T'invia.
\livretPers Paggio
%# Sì affé m'invia…
\livretPers Licco
%#- A dirgli.
\livretPers Paggio
%#= È vero a dirgli…
\livretPers Licco e Paggio
%# Ch'al giardino de' fiori
%# ella si renderà com'ei desia.
\livretPers Paggio
\livretRef#'CDBrecit
%# Sei tu qualche indovino?
\livretPers Licco
%# E ben famoso,
%# ch'in simil guisa a me nulla è nascoso.
\livretPers Deianira
%# Ah crudo, ah disleale,
%# ah traditore, ingrato,
%# ah scellerato, ed empio
%# dell'amor coniugale
%# tra noi tanto giurato.

%# Qui dunque hai scelto il luogo a farne scempio?  
%# Ah Deianira ogni ristor dispera,
%# ch'a morir di dolor sei destinata.
\livretPers Paggio
%# Che? cotesta straniera
%# anch'essa è innamorata?
\livretPers Licco
%# Così mi dice, ma d'amor ben vero,
%# come saggio io non credo,
%# ch'a gli uomini, poco, ed alle donne un zero.
\livretPers Paggio
%# Basta per questa corte ogn'or volare
%# si vede un sì gran numero d'amori,
%# che non abbiamo a fare,
%# che ne vengan di fuori.
%# Ama Hyllo Iole riamato, e l'ama
%# Ercole assai malvisto, ama Nicandro
%# Licori, e questa Oreste, e Oreste Olinda,
%# e Olinda, e Celia scaltre
%# aman le gemme, e l'oro,
%# e Niso, ed Alidoro aman cent'altre.
\livretPers Licco
%# E perché ha in odio Iole
%#- Ercole?
\livretPers Paggio
%#- Perché uccise Eutyro.
\livretPers Licco
%#= Ed ama
%# il figlio poi di chi gli uccise il padre?
%# Ha la pianta in orrore, ed ama il frutto?
%# Che vuoi giocar ch'io so
%# la ragion che di ciò
%# ella in sé covane?
%# Un d'essi è troppo adulto, e l'altro è giovane
\livretPers Paggio
%# Fin da principio Iole ardea per Hyllo
%# onde per compiacerla
%# le già date promesse
%# delle nozze di lei ritolse Eutyro
%# ad Ercole, ch'al fin sì mal soffrillo,
%# ch'una tal dalla figlia opra gradita
%# all'infelice re costò la vita.
%# E tu, ch'il tutto sai
%# non sai, ch'Ercol' m'attende? e ch'egli è amante?
%# E che fra quanti mai
%# ardono al mondo d'amorosa fiamma
%# non v'è di pazienza una sol dramma.

\livretScene SCENA QUINTA
\livretDescAtt\wordwrap-center {
  Deianira, Licco.
}
\livretPers Deianira
\livretRef#'CEArecit
%# Misera, ohimè, ch'ascolto.
%# Non so, se più gelosa
%# esser dèa come madre, o come sposa;
%# che comune è il periglio
%# alla mia fede coniugale, e al figlio;
%# almen con soffrir l'uno
%# schivar l'altro potessi: oh dio qual sorte
%# prefisse iniquo fato a i miei natali:
%# ch'io soffra a doppio i mali,
%# né per schivarne alcun basti mia morte.
%# O presagi funesti:
%# Ercol spirti non ha, se non feroci,
%# e non serian già questi
%# i di lui primi parricidi atroci.
%# Come mal mi lasciai
%# strascinar da' miei guai
%# a queste eubee contrade,
%# ove il destin mi fabbricò l'inferno:
%# ora, ahi lassa, discerno
%# quanto meglio era entro le patrie mura
%# di Calidonia sospirar piangendo
%# miei dubbi oltraggi, che con duol più orrendo
%# esserne qui sicura.
\livretRef#'CEBaria
%# Ahi ch'amarezza
%# meschina me
%# è la certezza
%# di rotta fé!
%# Ahi come, ohimè,
%# la gelosia
%# di furie l'Erebo impoverì.
%# E l'alma mia
%# ne riempì.
%# S'in amor si raddoppiassero
%# tutti i guai, tutti i tormenti,
%# e ch'in lui solo mancassero
%# i sospetti, e i tradimenti
%# fora amor tutta dolcezza.
\livretPers Licco
\livretRef#'CECrecit
%# Ah fu sempre in amor stolto consiglio
%# il cercar di sapere
%# punto di più, che quel basta a godere;
%# copron l'indiche balze
%# sotto aspetto villan viscere d'oro;
%# ma ben contrario affatto
%# l'amoroso terreno
%# sotto una superficie preziosa
%# sol cattiva materia ha in sé nascosa.
%# Onde chi vuole in lui
%# gir scavando tal'or con mesta prova
%# più s'inoltra a cercar peggio ritrova;
%# ben lo dicea, che noi sariam venuti
%# a incontrar pene, e rischi:
%# ah che d'Ercole irato
%# qualche stral ben rotato
%# parmi sentir, ch'intorno a me già fischi.
\livretPers Deianira
%# Ah Licco il cor ti manca, ohimè, che fia
%# di me senza il tuo aiuto?
\livretPers Licco
%# Ah Deianira:
%# dunque, dunque tu temi?
%# Io non ho già paura.
\livretPers Deianira
%# E in tanto tremi.
\livretPers Licco
%# Ma ve'; poiché nel mondo
%# ogni cosa ha misura;
%# forz'è che l'abbia ancor la mia bravura
%# e siccome tra quelle,
%# che fè nemico ciel senza danari
%# chi ha quattro soldi è ricco:
%# così per bravo io solamente spicco
%# fra tutti quanti li poltron miei pari.
\livretPers Deianira
%# Dunque che far dovrem?
\livretPers Licco
%# N'han già cangiati
%# in guisa tal questi abiti villani,
%# che se guardinghi andremo
%# ad altro non potrà, ch'alla favella
%# Ercole riconoscerne: per tanto
%# avvertir ne conviene
%# che qualche beffa, o crocchio
%# (grazie, ch'alli stranier versa ogni corte)
%# non c'irriti a parlare, e di tal sorte
%# farem la guerra all'occhio.

\livretScene SCENA SESTA
\livretDescAtt\center-column {
  \justify { La scena si cangia nella grotta del Sonno. }
  \wordwrap-center {
    Pasithea, il Sonno, coro d'Aure e Ruscelli.
  }
}
\livretPers Pasithea
\livretRef#'CFAaria
%# Mormorate
%# o fiumicelli,
%# sussurrate
%# o venticelli,
%# e col vostro sussurro, e mormorio
%# dolci incanti dell'oblio,
%# ch'ogni cura fugar ponno
%# lusingate al sonno il Sonno.
%# Chi da ver ama
%# vie più il diletto
%# del caro oggetto
%# che 'l proprio brama,
%# quind'è ch'io posi
%# la notte, e 'l die
%# le contentezze mie
%# del consorte gentil ne' bei riposi.
\livretPers Coro
\livretRef#'CFBcoro
%# Dormi, dormi, o Sonno dormi
%# fra le braccia a Pasithea
%# ninfa aver non ti potea
%# più d'affetti a' tuoi conformi:
%# dormi, dormi o Sonno dormi.
%# Dormi, dormi o Sonno dormi
%# sovra a te gli amori istessi
%# lente movano le piume;
%# e al tuo cor placido nume,
%# gelosia mai non appressi
%# de' suoi rei sospetti i stormi
%# dormi, dormi o Sonno dormi.

\livretScene SCENA SETTIMA
\livretDescAtt\center-column {
  \wordwrap-center { Cala Giunone dal cielo. }
  \wordwrap-center {
    Giunone, Pasithea, il Sonno, coro d'Aure e Ruscelli.
  }
}  
\livretPers Pasithea
\livretRef#'CGArecit
%# O dèa sublime dèa,
%# e qual nuovo desio
%# a quest'umile albergo oggi ti mena?
\livretPers Giunone
%# Zelo dell'onor mio
%# e della fede altrui
%# a me già sacra, e da sacrarsi, a cui
%# e frodi, e violenze altri prepara,
%# onde per fare a ciò schermo innocente
%# sol per una breve ora
%# di condur meco il Sonno uopo mi fora.
\livretPers Pasithea
%# Ohimè di nuovo esporre
%# di Giove all'ire ogni mio ben vorrai?
%# No, ciò non fia più mai.
\livretPers Giunone
%# Non temer Pasithea,
%# che solo è mio pensiero
%# di valermi di lui con men che numi
%# di già soggetti al di lui pigro impero.
\livretPers Pasithea
%# E di ciò m'assicuri?
\livretPers Giunone
%# S'ancor vuoi che te 'l giuri
%# sul germano di lui lo stigio Lete.
\livretPers Pasithea
%# Basta Giuno: quiete
%# son già le mie voglie al tuo desir sovrano.
\livretPers Giunone
%# Porgilo dunque a me, diva, pian piano…
\livretDidasPPage\justify {
  Giunone prende nel suo carro il Sonno e parte.
}
\livretRef#'CGCaria
%# Dell'amorose pene  
%# sospirato ristoro,
%# vital dolce tesoro,
%# ch'il mondo più che Cerere mantiene
%# dal neghittoso speco
%# soffri di venir meco,
%# ch'Amore oggi dispone
%# contro l'empia insolenza
%# di straniera potenza
%# della sua libertà farti campione.

\livretPers Tutti
\livretRef#'CGDtutti
%# Le rugiade più preziose
%# tuoi papaveri ogn'or bagnino,
%# e per tutto gigli, e rose
%# co' lor aliti t'accompagnino.
\livretPers Pasithea
\livretRef#'CGEaria
%# Vanne, e fa breve dimora,
%# che s'il tuo tardar noioso
%# ad ogn'un tanto è penoso,
%# che sarà per chi t'adora?
%# E Amore ha ben la gloria
%# di saper nel Sonno ancora
%# tener desta la memoria.
\livretDidasPPage\justify {
  Li Sogni giacenti per la grotta formano sognando la 3ª danza per
  fine del 2º atto.
}
\sep
